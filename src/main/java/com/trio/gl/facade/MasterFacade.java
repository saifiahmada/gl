package com.trio.gl.facade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.trio.gl.dao.HondaH000DealersDao;
import com.trio.gl.dao.TrioBukubesarDao;
import com.trio.gl.dao.TrioDesignNeracaAkhirAktivaDao;
import com.trio.gl.dao.TrioDesignNeracaAkhirDao;
import com.trio.gl.dao.TrioDesignNeracaAkhirPasivaDao;
import com.trio.gl.dao.TrioDesignRugiLabaDao;
import com.trio.gl.dao.TrioDetailBpkbDao;
import com.trio.gl.dao.TrioDetailStnkDao;
import com.trio.gl.dao.TrioHeaderBpkbDao;
import com.trio.gl.dao.TrioHeaderStnkDao;
import com.trio.gl.dao.TrioJurnalDao;
import com.trio.gl.dao.TrioKumpulDao;
import com.trio.gl.dao.TrioLabaDitahanDao;
import com.trio.gl.dao.TrioLapBukubesarDao;
import com.trio.gl.dao.TrioLapNeracaAktivaDao;
import com.trio.gl.dao.TrioLapNeracaDao;
import com.trio.gl.dao.TrioLapNeracaPasivaDao;
import com.trio.gl.dao.TrioLapRlDao;
import com.trio.gl.dao.TrioMstPerusahaanDao;
import com.trio.gl.dao.TrioMstaccessroleDao;
import com.trio.gl.dao.TrioMstcabangDao;
import com.trio.gl.dao.TrioMstkonfigcabangDao;
import com.trio.gl.dao.TrioMstkonfigurasiDao;
import com.trio.gl.dao.TrioMstmenuDao;
import com.trio.gl.dao.TrioMstperkiraanDao;
import com.trio.gl.dao.TrioMstroleDao;
import com.trio.gl.dao.TrioMstrunnumDao;
import com.trio.gl.dao.TrioMstsaldoperkiraanDao;
import com.trio.gl.dao.TrioMstsaldosubperkiraanDao;
import com.trio.gl.dao.TrioMstsubperkiraanDao;
import com.trio.gl.dao.TrioMstuserDao;
import com.trio.gl.dao.TrioMstuserroleDao;
import com.trio.gl.dao.TrioNeracaCobaDao;
import com.trio.gl.dao.TrioRmsNeracaAktivaDao;
import com.trio.gl.dao.TrioRmsNeracaAktivaSubDao;
import com.trio.gl.dao.TrioRmsNeracaPasivaDao;
import com.trio.gl.dao.TrioRmsNeracaPasivaSubDao;
import com.trio.gl.dao.TrioRumus2Dao;
import com.trio.gl.dao.TrioRumusDao;
import com.trio.gl.dao.TrioStokCabangDao;
import com.trio.gl.dao.TrioStokIntransitDao;
import com.trio.gl.dao.TrioStokTerimaDao;
import com.trio.gl.dao.TrioStokUnitTempDao;
import com.trio.gl.dao.TrioStokunitDao;
import com.trio.gl.dao.TrioTarikoracleDao;
import com.trio.gl.dao.TrioTarikunitDao;
import com.trio.gl.dao.TrioTerimaDao;
import com.trio.gl.helper.dao.TrioStokUnitHelperDao;

@Component
public class MasterFacade {
	
	@Autowired
	protected TrioMstaccessroleDao trioMstaccessroleDao;
	
	@Autowired
	protected TrioMstmenuDao trioMstmenuDao;
	
	@Autowired
	protected TrioMstroleDao trioMstroleDao;
	
	@Autowired
	protected TrioMstuserDao trioMstuserDao;
	
	@Autowired
	protected TrioMstuserroleDao trioMstuserroleDao;
	
	@Autowired
	protected TrioMstcabangDao trioMstcabangDao;
	
	@Autowired
	protected TrioMstperkiraanDao trioMstperkiraanDao;
	
	@Autowired
	protected TrioMstsaldosubperkiraanDao trioMstsaldosubperkiraanDao;
	
	@Autowired
	protected TrioMstsubperkiraanDao trioMstsubperkiraanDao;
	
	@Autowired
	protected TrioJurnalDao trioJurnalDao;
	
	@Autowired
	protected TrioMstrunnumDao trioMstrunnumDao;
	
	@Autowired
	protected TrioMstkonfigurasiDao trioMstkonfigurasiDao;
	
	@Autowired
	protected TrioStokunitDao trioStokunitDao;
	
	@Autowired
	protected HondaH000DealersDao hondaH000DealersDao;
	
	@Autowired
	protected TrioTarikunitDao trioTarikunitDao;
	
	@Autowired
	protected TrioTarikoracleDao trioTarikoracleDao;
	
	@Autowired
	protected TrioMstkonfigcabangDao trioMstkonfigcabangDao;
	
	@Autowired
	protected TrioLapBukubesarDao trioLapBukubesarDao;
	
	@Autowired
	protected TrioMstsaldoperkiraanDao trioMstsaldoperkiraanDao;
	
	@Autowired
	protected TrioStokUnitTempDao trioStokUnitTempDao;
	
	@Autowired
	protected TrioStokUnitHelperDao trioStokUnitHelperDao;
	
	@Autowired
	protected TrioHeaderStnkDao trioHeaderStnkDao;
	
	@Autowired
	protected TrioDetailStnkDao trioDetailStnkDao;
	
	@Autowired
	protected TrioHeaderBpkbDao trioHeaderBpkbDao;
	
	@Autowired
	protected TrioDetailBpkbDao trioDetailBpkbDao;
	
	@Autowired
	protected TrioKumpulDao trioKumpulDao;
	
	@Autowired
	protected TrioMstPerusahaanDao trioMstPerusahaanDao;
	
	@Autowired
	protected TrioLabaDitahanDao trioLabaDitahanDao;
	
	@Autowired
	protected TrioDesignNeracaAkhirDao trioDesignNeracaAkhirDao;
	
	@Autowired
	protected TrioDesignNeracaAkhirAktivaDao trioDesignNeracaAkhirAktivaDao;
	
	@Autowired
	protected TrioDesignNeracaAkhirPasivaDao trioDesignNeracaAkhirPasivaDao;
	
	@Autowired
	protected TrioLapNeracaDao trioLapNeracaDao;
	
	@Autowired
	protected TrioLapNeracaAktivaDao trioLapNeracaAktivaDao;
	
	@Autowired
	protected TrioLapNeracaPasivaDao trioLapNeracaPasivaDao;
	
	@Autowired
	protected TrioRmsNeracaAktivaDao trioRmsNeracaAktivaDao;
	
	@Autowired
	protected TrioRmsNeracaAktivaSubDao trioRmsNeracaAktivaSubDao;
	
	@Autowired
	protected TrioRmsNeracaPasivaDao trioRmsNeracaPasivaDao;
	
	@Autowired
	protected TrioRmsNeracaPasivaSubDao trioRmsNeracaPasivaSubDao;
	
	@Autowired
	protected TrioStokTerimaDao trioStokTerimaDao;
	
	@Autowired
	protected TrioStokCabangDao trioStokCabangDao;
	
	@Autowired
	protected TrioNeracaCobaDao trioNeracaCobaDao;
	
	@Autowired
	protected TrioStokIntransitDao trioStokIntransitDao;
	
	@Autowired
	protected TrioTerimaDao trioTerimaDao;
	
	@Autowired
	protected TrioLapRlDao trioLapRlDao;
	
	@Autowired
	protected TrioDesignRugiLabaDao trioDesignRugiLabaDao;

	@Autowired
	protected TrioRumusDao trioRumusDao;
	
	@Autowired
	protected TrioRumus2Dao trioRumus2Dao;
	
	@Autowired
	protected TrioBukubesarDao trioBukubesarDao;
	
	public TrioBukubesarDao getTrioBukubesarDao() {
		return trioBukubesarDao;
	}

	public void setTrioBukubesarDao(TrioBukubesarDao trioBukubesarDao) {
		this.trioBukubesarDao = trioBukubesarDao;
	}

	public TrioTerimaDao getTrioTerimaDao() {
		return trioTerimaDao;
	}

	public void setTrioTerimaDao(TrioTerimaDao trioTerimaDao) {
		this.trioTerimaDao = trioTerimaDao;
	}

	public TrioMstsaldoperkiraanDao getTrioMstsaldoperkiraanDao() {
		return trioMstsaldoperkiraanDao;
	}

	public void setTrioMstsaldoperkiraanDao(
			TrioMstsaldoperkiraanDao trioMstsaldoperkiraanDao) {
		this.trioMstsaldoperkiraanDao = trioMstsaldoperkiraanDao;
	}

	public TrioLapBukubesarDao getTrioLapBukubesarDao() {
		return trioLapBukubesarDao;
	}

	public void setTrioLapBukubesarDao(TrioLapBukubesarDao trioLapBukubesarDao) {
		this.trioLapBukubesarDao = trioLapBukubesarDao;
	}

	public TrioMstkonfigcabangDao getTrioMstkonfigcabangDao() {
		return trioMstkonfigcabangDao;
	}

	public void setTrioMstkonfigcabangDao(
			TrioMstkonfigcabangDao trioMstkonfigcabangDao) {
		this.trioMstkonfigcabangDao = trioMstkonfigcabangDao;
	}

	public TrioTarikoracleDao getTrioTarikoracleDao() {
		return trioTarikoracleDao;
	}

	public void setTrioTarikoracleDao(TrioTarikoracleDao trioTarikoracleDao) {
		this.trioTarikoracleDao = trioTarikoracleDao;
	}

	public TrioTarikunitDao getTrioTarikunitDao() {
		return trioTarikunitDao;
	}

	public void setTrioTarikunitDao(TrioTarikunitDao trioTarikunitDao) {
		this.trioTarikunitDao = trioTarikunitDao;
	}

	public HondaH000DealersDao getHondaH000DealersDao() {
		return hondaH000DealersDao;
	}

	public void setHondaH000DealersDao(HondaH000DealersDao hondaH000DealersDao) {
		this.hondaH000DealersDao = hondaH000DealersDao;
	}

	public TrioStokunitDao getTrioStokunitDao() {
		return trioStokunitDao;
	}

	public void setTrioStokunitDao(TrioStokunitDao trioStokunitDao) {
		this.trioStokunitDao = trioStokunitDao;
	}

	public TrioMstkonfigurasiDao getTrioMstkonfigurasiDao() {
		return trioMstkonfigurasiDao;
	}

	public void setTrioMstkonfigurasiDao(TrioMstkonfigurasiDao trioMstkonfigurasiDao) {
		this.trioMstkonfigurasiDao = trioMstkonfigurasiDao;
	}

	public TrioMstrunnumDao getTrioMstrunnumDao() {
		return trioMstrunnumDao;
	}

	public void setTrioMstrunnumDao(TrioMstrunnumDao trioMstrunnumDao) {
		this.trioMstrunnumDao = trioMstrunnumDao;
	}

	public TrioJurnalDao getTrioJurnalDao() {
		return trioJurnalDao;
	}

	public void setTrioJurnalDao(TrioJurnalDao trioJurnalDao) {
		this.trioJurnalDao = trioJurnalDao;
	}

	public TrioMstsubperkiraanDao getTrioMstsubperkiraanDao() {
		return trioMstsubperkiraanDao;
	}

	public void setTrioMstsubperkiraanDao(
			TrioMstsubperkiraanDao trioMstsubperkiraanDao) {
		this.trioMstsubperkiraanDao = trioMstsubperkiraanDao;
	}

	public TrioMstperkiraanDao getTrioMstperkiraanDao() {
		return trioMstperkiraanDao;
	}

	public void setTrioMstperkiraanDao(TrioMstperkiraanDao trioMstperkiraanDao) {
		this.trioMstperkiraanDao = trioMstperkiraanDao;
	}

	public TrioMstcabangDao getTrioMstcabangDao() {
		return trioMstcabangDao;
	}

	public void setTrioMstcabangDao(TrioMstcabangDao trioMstcabangDao) {
		this.trioMstcabangDao = trioMstcabangDao;
	}

	public TrioMstuserroleDao getTrioMstuserroleDao() {
		return trioMstuserroleDao;
	}

	public void setTrioMstuserroleDao(TrioMstuserroleDao trioMstuserroleDao) {
		this.trioMstuserroleDao = trioMstuserroleDao;
	}

	public TrioMstaccessroleDao getTrioMstaccessroleDao() {
		return trioMstaccessroleDao;
	}

	public void setTrioMstaccessroleDao(TrioMstaccessroleDao trioMstaccessroleDao) {
		this.trioMstaccessroleDao = trioMstaccessroleDao;
	}

	public TrioMstmenuDao getTrioMstmenuDao() {
		return trioMstmenuDao;
	}

	public void setTrioMstmenuDao(TrioMstmenuDao trioMstmenuDao) {
		this.trioMstmenuDao = trioMstmenuDao;
	}

	public TrioMstroleDao getTrioMstroleDao() {
		return trioMstroleDao;
	}

	public void setTrioMstroleDao(TrioMstroleDao trioMstroleDao) {
		this.trioMstroleDao = trioMstroleDao;
	}

	public TrioMstuserDao getTrioMstuserDao() {
		return trioMstuserDao;
	}

	public void setTrioMstuserDao(TrioMstuserDao trioMstuserDao) {
		this.trioMstuserDao = trioMstuserDao;
	}

	public TrioStokUnitTempDao getTrioStokUnitTempDao() {
		return trioStokUnitTempDao;
	}

	public void setTrioStokUnitTempDao(TrioStokUnitTempDao trioStokUnitTempDao) {
		this.trioStokUnitTempDao = trioStokUnitTempDao;
	}

	public TrioStokUnitHelperDao getTrioStokUnitHelperDao() {
		return trioStokUnitHelperDao;
	}

	public void setTrioStokUnitHelperDao(TrioStokUnitHelperDao trioStokUnitHelperDao) {
		this.trioStokUnitHelperDao = trioStokUnitHelperDao;
	}

	public TrioHeaderStnkDao getTrioHeaderStnkDao() {
		return trioHeaderStnkDao;
	}

	public void setTrioHeaderStnkDao(TrioHeaderStnkDao trioHeaderStnkDao) {
		this.trioHeaderStnkDao = trioHeaderStnkDao;
	}

	public TrioDetailStnkDao getTrioDetailStnkDao() {
		return trioDetailStnkDao;
	}

	public void setTrioDetailStnkDao(TrioDetailStnkDao trioDetailStnkDao) {
		this.trioDetailStnkDao = trioDetailStnkDao;
	}

	public TrioHeaderBpkbDao getTrioHeaderBpkbDao() {
		return trioHeaderBpkbDao;
	}

	public void setTrioHeaderBpkbDao(TrioHeaderBpkbDao trioHeaderBpkbDao) {
		this.trioHeaderBpkbDao = trioHeaderBpkbDao;
	}

	public TrioDetailBpkbDao getTrioDetailBpkbDao() {
		return trioDetailBpkbDao;
	}

	public void setTrioDetailBpkbDao(TrioDetailBpkbDao trioDetailBpkbDao) {
		this.trioDetailBpkbDao = trioDetailBpkbDao;
	}

	public TrioKumpulDao getTrioKumpulDao() {
		return trioKumpulDao;
	}

	public void setTrioKumpulDao(TrioKumpulDao trioKumpulDao) {
		this.trioKumpulDao = trioKumpulDao;
	}

	public TrioMstPerusahaanDao getTrioMstPerusahaanDao() {
		return trioMstPerusahaanDao;
	}

	public void setTrioMstPerusahaanDao(TrioMstPerusahaanDao trioMstPerusahaanDao) {
		this.trioMstPerusahaanDao = trioMstPerusahaanDao;
	}

	public TrioLabaDitahanDao getTrioLabaDitahanDao() {
		return trioLabaDitahanDao;
	}

	public void setTrioLabaDitahanDao(TrioLabaDitahanDao trioLabaDitahanDao) {
		this.trioLabaDitahanDao = trioLabaDitahanDao;
	}

	public TrioDesignNeracaAkhirDao getTrioDesignNeracaAkhirDao() {
		return trioDesignNeracaAkhirDao;
	}

	public void setTrioDesignNeracaAkhirDao(
			TrioDesignNeracaAkhirDao trioDesignNeracaAkhirDao) {
		this.trioDesignNeracaAkhirDao = trioDesignNeracaAkhirDao;
	}

	public TrioDesignNeracaAkhirAktivaDao getTrioDesignNeracaAkhirAktivaDao() {
		return trioDesignNeracaAkhirAktivaDao;
	}

	public void setTrioDesignNeracaAkhirAktivaDao(
			TrioDesignNeracaAkhirAktivaDao trioDesignNeracaAkhirAktivaDao) {
		this.trioDesignNeracaAkhirAktivaDao = trioDesignNeracaAkhirAktivaDao;
	}

	public TrioDesignNeracaAkhirPasivaDao getTrioDesignNeracaAkhirPasivaDao() {
		return trioDesignNeracaAkhirPasivaDao;
	}

	public void setTrioDesignNeracaAkhirPasivaDao(
			TrioDesignNeracaAkhirPasivaDao trioDesignNeracaAkhirPasivaDao) {
		this.trioDesignNeracaAkhirPasivaDao = trioDesignNeracaAkhirPasivaDao;
	}

	public TrioLapNeracaDao getTrioLapNeracaDao() {
		return trioLapNeracaDao;
	}

	public void setTrioLapNeracaDao(TrioLapNeracaDao trioLapNeracaDao) {
		this.trioLapNeracaDao = trioLapNeracaDao;
	}

	public TrioLapNeracaAktivaDao getTrioLapNeracaAktivaDao() {
		return trioLapNeracaAktivaDao;
	}

	public void setTrioLapNeracaAktivaDao(
			TrioLapNeracaAktivaDao trioLapNeracaAktivaDao) {
		this.trioLapNeracaAktivaDao = trioLapNeracaAktivaDao;
	}

	public TrioRmsNeracaAktivaDao getTrioRmsNeracaAktivaDao() {
		return trioRmsNeracaAktivaDao;
	}
	
	public TrioLapNeracaPasivaDao getTrioLapNeracaPasivaDao() {
		return trioLapNeracaPasivaDao;
	}

	public void setTrioLapNeracaPasivaDao(
			TrioLapNeracaPasivaDao trioLapNeracaPasivaDao) {
		this.trioLapNeracaPasivaDao = trioLapNeracaPasivaDao;
	}

	public void setTrioRmsNeracaAktivaDao(
			TrioRmsNeracaAktivaDao trioRmsNeracaAktivaDao) {
		this.trioRmsNeracaAktivaDao = trioRmsNeracaAktivaDao;
	}

	public TrioRmsNeracaAktivaSubDao getTrioRmsNeracaAktivaSubDao() {
		return trioRmsNeracaAktivaSubDao;
	}

	public void setTrioRmsNeracaAktivaSubDao(
			TrioRmsNeracaAktivaSubDao trioRmsNeracaAktivaSubDao) {
		this.trioRmsNeracaAktivaSubDao = trioRmsNeracaAktivaSubDao;
	}

	public TrioRmsNeracaPasivaDao getTrioRmsNeracaPasivaDao() {
		return trioRmsNeracaPasivaDao;
	}

	public void setTrioRmsNeracaPasivaDao(
			TrioRmsNeracaPasivaDao trioRmsNeracaPasivaDao) {
		this.trioRmsNeracaPasivaDao = trioRmsNeracaPasivaDao;
	}

	public TrioRmsNeracaPasivaSubDao getTrioRmsNeracaPasivaSubDao() {
		return trioRmsNeracaPasivaSubDao;
	}

	public void setTrioRmsNeracaPasivaSubDao(
			TrioRmsNeracaPasivaSubDao trioRmsNeracaPasivaSubDao) {
		this.trioRmsNeracaPasivaSubDao = trioRmsNeracaPasivaSubDao;
	}

	public TrioMstsaldosubperkiraanDao getTrioMstsaldosubperkiraanDao() {
		return trioMstsaldosubperkiraanDao;
	}

	public void setTrioMstsaldosubperkiraanDao(
			TrioMstsaldosubperkiraanDao trioMstsaldosubperkiraanDao) {
		this.trioMstsaldosubperkiraanDao = trioMstsaldosubperkiraanDao;
	}

	public TrioStokTerimaDao getTrioStokTerimaDao() {
		return trioStokTerimaDao;
	}

	public void setTrioStokTerimaDao(TrioStokTerimaDao trioStokTerimaDao) {
		this.trioStokTerimaDao = trioStokTerimaDao;
	}

	public TrioStokCabangDao getTrioStokCabangDao() {
		return trioStokCabangDao;
	}

	public void setTrioStokCabangDao(TrioStokCabangDao trioStokCabangDao) {
		this.trioStokCabangDao = trioStokCabangDao;
	}

	public TrioNeracaCobaDao getTrioNeracaCobaDao() {
		return trioNeracaCobaDao;
	}

	public void setTrioNeracaCobaDao(TrioNeracaCobaDao trioNeracaCobaDao) {
		this.trioNeracaCobaDao = trioNeracaCobaDao;
	}

	public TrioStokIntransitDao getTrioStokIntransitDao() {
		return trioStokIntransitDao;
	}

	public void setTrioStokIntransitDao(TrioStokIntransitDao trioStokIntransitDao) {
		this.trioStokIntransitDao = trioStokIntransitDao;
	}

	public TrioLapRlDao getTrioLapRlDao() {
		return trioLapRlDao;
	}

	public void setTrioLapRlDao(TrioLapRlDao trioLapRlDao) {
		this.trioLapRlDao = trioLapRlDao;
	}

	public TrioDesignRugiLabaDao getTrioDesignRugiLabaDao() {
		return trioDesignRugiLabaDao;
	}

	public void setTrioDesignRugiLabaDao(TrioDesignRugiLabaDao trioDesignRugiLabaDao) {
		this.trioDesignRugiLabaDao = trioDesignRugiLabaDao;
	}

	public TrioRumusDao getTrioRumusDao() {
		return trioRumusDao;
	}

	public void setTrioRumusDao(TrioRumusDao trioRumusDao) {
		this.trioRumusDao = trioRumusDao;
	}

	public TrioRumus2Dao getTrioRumus2Dao() {
		return trioRumus2Dao;
	}

	public void setTrioRumus2Dao(TrioRumus2Dao trioRumus2Dao) {
		this.trioRumus2Dao = trioRumus2Dao;
	}
	
}
