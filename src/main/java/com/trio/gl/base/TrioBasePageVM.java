package com.trio.gl.base;

import java.sql.Connection;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zkplus.spring.SpringUtil;

import com.trio.gl.facade.MasterFacade;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;

/*
 * Created by Saifi Ramli $
 * 
 * Tue Sep 25 21:03:40 PM @ kos Bandaneira
 */

public class TrioBasePageVM  {
	
	/**
	 * 
	 */
	
	private static String userSession;
	
	private static String cabangSession;
	
	private MasterFacade mf;
	
	public MasterFacade getMasterFacade() {
		MasterFacade mf = (MasterFacade) SpringUtil.getBean("masterFacade");
		return mf;
	}

	public static String getUserSession() {
		String username = (String) Sessions.getCurrent().getAttribute("username");
		userSession = username;
		return userSession;
	}

	public static void setUserSession(String userSession) {
		TrioBasePageVM.userSession = userSession;
	}
	
	public Connection getReportConnection(){
		
		DataSource ds = (DataSource) SpringUtil.getBean("dataSource");
		Connection con = DataSourceUtils.getConnection(ds);
		return con;
	}

	public static String getCabangSession() {
		cabangSession = (String) Sessions.getCurrent().getAttribute("cabang");
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(cabangSession));
		return cabangSession;
	}

	public static void setCabangSession(String cabangSession) {
		TrioBasePageVM.cabangSession = cabangSession;
	}

}
