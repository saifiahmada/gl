package com.trio.gl.base;

import java.sql.Connection;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zkplus.spring.SpringUtil;

import com.trio.gl.facade.MasterFacade;

/*
 * Created by Saifi Ramli $
 * 
 * Tue Sep 25 21:03:40 PM @ kos Bandaneira
 */

@SuppressWarnings("rawtypes")
public class TrioBasePage extends GenericForwardComposer {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static String userSession;
	
	private static String cabangSession;
	
	private MasterFacade mf;
	
	public MasterFacade getMasterFacade() {
		MasterFacade mf = (MasterFacade) SpringUtil.getBean("masterFacade");
		return mf;
	}

	public static String getUserSession() {
		String username = (String) Sessions.getCurrent().getAttribute("username");
		userSession = username;
		return userSession;
	}

	public static void setUserSession(String userSession) {
		TrioBasePage.userSession = userSession;
	}
	
	public static String getCabangSession() {
		cabangSession = (String) Sessions.getCurrent().getAttribute("cabang"); 
		return cabangSession;
	}

	public static void setCabangSession(String cabangSession) {
		TrioBasePage.cabangSession = cabangSession;
	}

	public Connection getReportConnection(){
		
		DataSource ds = (DataSource) SpringUtil.getBean("dataSource");
		Connection con = DataSourceUtils.getConnection(ds);
		return con;
	}

}
