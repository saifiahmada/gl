package com.trio.gl.popup;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;

import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioDesignNeracaAkhirPasiva;
import com.trio.gl.bean.TrioLapNeracaPasiva;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;

/**
 * @author Gusti Arya 4:26:22 PM Nov 7, 2013
 */

public class PopupSaveFilePasivaVM extends TrioBasePageVM{
	
	private Date periode;

	public Date getPeriode() {
		if(periode==null)
			periode = new Date();
		return periode;
	}

	@NotifyChange("periode")
	public void setPeriode(Date periode) {
		this.periode = periode;
	}

	@Command
	public void save(){
		DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);
		SimpleDateFormat sdf = new SimpleDateFormat("MM");
		String periode = sdf.format(getPeriode());
		
		List<TrioLapNeracaPasiva> lists = getMasterFacade().getTrioLapNeracaPasivaDao().findByPeriode(periode);
		if(lists.size() > 0){
			Messagebox.show("Periode Neraca Akhir Sudah Ada");
			return;
		}else{
			List<TrioDesignNeracaAkhirPasiva> listDsg = getMasterFacade().getTrioDesignNeracaAkhirPasivaDao().findAll();
			List<TrioLapNeracaPasiva> listNeracaToSave = new ArrayList<TrioLapNeracaPasiva>();
			
			for(TrioDesignNeracaAkhirPasiva dsg : listDsg){
				TrioLapNeracaPasiva nrcPasiva = new TrioLapNeracaPasiva();
				nrcPasiva.setPeriode(periode);
				nrcPasiva.setIdPerkiraan(dsg.getIdPerkiraan());
				nrcPasiva.setKdJd(dsg.getKdJd());
				nrcPasiva.setNamaPerkiraan(dsg.getNamaPerkiraan());
				nrcPasiva.setKet(dsg.getKet());
				nrcPasiva.setKet1(dsg.getKet1());
				nrcPasiva.setLevel1(dsg.getLevel1());
				nrcPasiva.setLevel2(dsg.getLevel2());
				nrcPasiva.setNilai(dsg.getNilai());
				listNeracaToSave.add(nrcPasiva);
			}
			
			getMasterFacade().getTrioLapNeracaPasivaDao().saveFromCollection(listNeracaToSave, getUserSession());
			Messagebox.show("Proses Selesai");
		}
	}
}
