package com.trio.gl.popup;

import java.math.BigDecimal;
import java.util.List;

import org.hibernate.NonUniqueObjectException;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zul.ListModelList;

import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioRmsNeracaPasivaSub;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;

/**
 * @author Gusti Arya 9:39:05 AM Nov 11, 2013
 */

public class PopupRumusTotalPasivaSubVM extends TrioBasePageVM{

	private int index;
	private TrioRmsNeracaPasivaSub selectedRumusSub;
	private ListModelList<TrioRmsNeracaPasivaSub> listModelRumusSub;


	public TrioRmsNeracaPasivaSub getSelectedRumusSub() {
		return selectedRumusSub;
	}

	public void setSelectedRumusSub(TrioRmsNeracaPasivaSub selectedRumusSub) {
		this.selectedRumusSub = selectedRumusSub;
	}

	public ListModelList<TrioRmsNeracaPasivaSub> getListModelRumusSub() {
		DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);
		if(listModelRumusSub == null){
			listModelRumusSub = new ListModelList<TrioRmsNeracaPasivaSub>();
			List<TrioRmsNeracaPasivaSub> list = getMasterFacade()
					.getTrioRmsNeracaPasivaSubDao().findAll();
			listModelRumusSub.addAll(list);
		}
		return listModelRumusSub;
	}

	public void setListModelRumusSub(
			ListModelList<TrioRmsNeracaPasivaSub> listModelRumusSub) {
		this.listModelRumusSub = listModelRumusSub;
	}

	/*
	 * Logic Code Start Here
	 */

	@Command
	public void onSelectList(){
		this.index = getListModelRumusSub().indexOf(getSelectedRumusSub());
	}

	@Command
	@NotifyChange("listModelRumusSub")
	public void newRecord(){
		TrioRmsNeracaPasivaSub rmsSub = new TrioRmsNeracaPasivaSub("", "", "", "", new BigDecimal(0));
		getListModelRumusSub().add(rmsSub);
	}

	@Command
	@NotifyChange("listModelRumusSub")
	public void insertRecord(){
		TrioRmsNeracaPasivaSub rmsSub = new TrioRmsNeracaPasivaSub("", "", "", "", new BigDecimal(0));
		getListModelRumusSub().add(index, rmsSub);
	}

	@Command
	@NotifyChange("listModelRumusSub")
	public void deleteRecord(){
		getListModelRumusSub().remove(getSelectedRumusSub());
	}

	@Command
	@NotifyChange("listModelRumusSub")
	public void save(){
		try {
			DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);
			getMasterFacade().getTrioRmsNeracaPasivaSubDao().saveByCollection(listModelRumusSub, getUserSession());
			Messagebox.show("Berhasil Simpan Rumus Sub-Total Pasiva");
		} catch (NonUniqueObjectException e) {
			Messagebox.show("Kd.Hasil Tidak Boleh Sama");
		}
	}
}
