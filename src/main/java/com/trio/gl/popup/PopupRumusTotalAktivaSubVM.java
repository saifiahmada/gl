package com.trio.gl.popup;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.NonUniqueObjectException;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zul.ListModelList;

import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioRmsNeracaAktivaSub;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;

/**
 * @author Gusti Arya 9:56:10 AM Nov 4, 2013
 */

public class PopupRumusTotalAktivaSubVM extends TrioBasePageVM{

	private int index;
	private ListModelList<TrioRmsNeracaAktivaSub> listModelRumusSub;
	private TrioRmsNeracaAktivaSub selectedRumusSub;

	public ListModelList<TrioRmsNeracaAktivaSub> getListModelRumusSub() {
		DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);
		if(listModelRumusSub == null){
			listModelRumusSub = new ListModelList<TrioRmsNeracaAktivaSub>();
			List<TrioRmsNeracaAktivaSub> lists = new ArrayList<TrioRmsNeracaAktivaSub>();
			lists = getMasterFacade().getTrioRmsNeracaAktivaSubDao().findAll();
			listModelRumusSub.addAll(lists);
		}

		return listModelRumusSub;
	}

	public void setListModelRumusSub(
			ListModelList<TrioRmsNeracaAktivaSub> listModelRumusSub) {
		this.listModelRumusSub = listModelRumusSub;
	}

	public TrioRmsNeracaAktivaSub getSelectedRumusSub() {
		return selectedRumusSub;
	}

	public void setSelectedRumusSub(TrioRmsNeracaAktivaSub selectedRumusSub) {
		this.selectedRumusSub = selectedRumusSub;
	}

	@Command
	@NotifyChange("listModelRumusSub")
	public void newRecord(){
		TrioRmsNeracaAktivaSub trioRmsNeracaAktivaSub = new TrioRmsNeracaAktivaSub("", "", "", "", new BigDecimal(0));
		getListModelRumusSub().add(trioRmsNeracaAktivaSub);
	}
	
	@Command
	@NotifyChange("listModelRumusSub")
	public void insertRecord(){
		TrioRmsNeracaAktivaSub trioRmsNeracaAktivaSub = new TrioRmsNeracaAktivaSub("", "", "", "", new BigDecimal(0));
		getListModelRumusSub().add(index, trioRmsNeracaAktivaSub);
	}
	
	@Command
	@NotifyChange("listModelRumusSub")
	public void deleteRecord(){
		getListModelRumusSub().remove(getSelectedRumusSub());
	}
	
	@Command
	@NotifyChange("listModelRumusSub")
	public void save(){
		try {
			DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);
			getMasterFacade().getTrioRmsNeracaAktivaSubDao().saveByList(getListModelRumusSub(), getUserSession());
			Messagebox.show("Berhasil Simpan Rumus Sub-Total Aktiva");
		} catch (NonUniqueObjectException e) {
			Messagebox.show("Kd. Hasil Tidak Boleh Sama");
		}
	}
	
	@Command
	public void onSelectList(){
		index = getListModelRumusSub().indexOf(getSelectedRumusSub());
	}
	
	
}
