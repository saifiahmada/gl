package com.trio.gl.popup;
import java.math.BigDecimal;
import java.util.List;

import org.hibernate.NonUniqueObjectException;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zul.ListModelList;

import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioRumus;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;

/**
 * @author Gusti Arya 1:57:19 PM Dec 14, 2013
 */

public class PopupRumus1RugiLabaVM extends TrioBasePageVM {
	
	private int index;
	private TrioRumus selectedItem;
	ListModelList<TrioRumus> listModelRumus;
	
	public TrioRumus getSelectedItem() {
		return selectedItem;
	}

	public void setSelectedItem(TrioRumus selectedItem) {
		this.selectedItem = selectedItem;
	}

	public ListModelList<TrioRumus> getListModelRumus() {
		DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);
		if(listModelRumus == null){
			listModelRumus = new ListModelList<TrioRumus>();
			List<TrioRumus> lists = getMasterFacade().getTrioRumusDao().findAll();
			listModelRumus.addAll(lists);
		}
		
		return listModelRumus;
	}

	public void setListModelRumus(ListModelList<TrioRumus> listModelRumus) {
		this.listModelRumus = listModelRumus;
	}
	
	@Command
	public void onSelectList(){
		index = getListModelRumus().indexOf(getSelectedItem());
	}
	
	@Command
	@NotifyChange("listModelRumus")
	public void newRecord(){
		TrioRumus rumus = new TrioRumus("", "", "", ' ', new BigDecimal(0));
		getListModelRumus().add(rumus);
	}
	
	@Command
	@NotifyChange("listModelRumus")
	public void insertRecord(){
		TrioRumus rumus = new TrioRumus("", "", "", ' ', new BigDecimal(0));
		getListModelRumus().add(index, rumus);
	}
	
	@Command
	public void deleteRecord(){
		getListModelRumus().remove(getSelectedItem());
	}
	
	@Command
	@NotifyChange("listModelRumus")
	public void save(){
		try {
			getMasterFacade().getTrioRumusDao().saveByList(getListModelRumus(), getUserSession());
			Messagebox.show("Berhasil Simpan Rumus");
		} catch (NonUniqueObjectException e) {
			e.printStackTrace();
			Messagebox.show("Terdapat Kode Perkiraan Yang Sama");
		}
	}
}
