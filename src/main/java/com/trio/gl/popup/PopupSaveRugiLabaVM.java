package com.trio.gl.popup;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.zkoss.bind.annotation.Command;
import org.zkoss.zhtml.Li;
import org.zkoss.zul.Messagebox;

import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioDesignRugiLaba;
import com.trio.gl.bean.TrioLapRl;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;

public class PopupSaveRugiLabaVM extends TrioBasePageVM {
	
	private Date bulan;
	private boolean cb;

	public Date getBulan() {
		if(bulan==null)
			bulan = new Date();
		return bulan;
	}

	public void setBulan(Date bulan) {
		this.bulan = bulan;
	}

	public boolean isCb() {
		return cb;
	}

	public void setCb(boolean cb) {
		this.cb = cb;
	}
	
	@Command
	public void simpan(){
		DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);
		SimpleDateFormat sdf = new SimpleDateFormat("MM");
		String periode = sdf.format(getBulan());
		
		if(isCb()){
			periode = "00";
		}
		
		List<TrioLapRl> listPeriode = getMasterFacade().getTrioLapRlDao().findByPeriode(periode);
		
		if(listPeriode.size() > 0){
			Messagebox.show("Periode Rugi Laba Sudah Ada");
			return;
		}else{
			List<TrioDesignRugiLaba> listDesign = getMasterFacade().getTrioDesignRugiLabaDao().rlSave();
			List<TrioLapRl> listRlToSave = new ArrayList<TrioLapRl>();
			
			for(TrioDesignRugiLaba dsg : listDesign){
				TrioLapRl lapRl = new TrioLapRl();
				
				lapRl.setPeriode(periode);
				lapRl.setIdPerkiraan(dsg.getIdPerkiraan());
				lapRl.setKdJd(dsg.getKdJD());
				lapRl.setTglRl(dsg.getTglRl());
				lapRl.setNamaPerkiraan(dsg.getNamaPerkiraan());
				lapRl.setKet(dsg.getKet());
				lapRl.setKet2(dsg.getKet2());
				lapRl.setLevel1(dsg.getLevel1());
				lapRl.setLevel2(dsg.getLevel2());
				lapRl.setLevel3(dsg.getLevel3());
				lapRl.setLevel4(dsg.getLevel4());
				
				listRlToSave.add(lapRl);
					
			}
			
			getMasterFacade().getTrioLapRlDao().saveByList(listRlToSave);
			Messagebox.show("Proses Selesai");
		}
	}
}
