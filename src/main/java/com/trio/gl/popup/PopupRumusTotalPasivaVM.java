package com.trio.gl.popup;

import java.math.BigDecimal;
import java.util.List;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zul.ListModelList;

import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioRmsNeracaPasiva;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;

/**
 * @author Gusti Arya 4:18:37 PM Nov 8, 2013
 */

public class PopupRumusTotalPasivaVM extends TrioBasePageVM {

	private int index;
	private String strIdPerk;
	private TrioRmsNeracaPasiva selectedPasiva;
	private ListModelList<TrioRmsNeracaPasiva> listModelRumus;

	public ListModelList<TrioRmsNeracaPasiva> getListModelRumus() {
		DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);
		if(listModelRumus == null){
			listModelRumus = new ListModelList<TrioRmsNeracaPasiva>();
			List<TrioRmsNeracaPasiva> lists = getMasterFacade().getTrioRmsNeracaPasivaDao().findAll();
			listModelRumus.addAll(lists);
		}
		return listModelRumus;
	}

	public void setListModelRumus(ListModelList<TrioRmsNeracaPasiva> listModelRumus) {
		this.listModelRumus = listModelRumus;
	}

	public TrioRmsNeracaPasiva getSelectedPasiva() {
		return selectedPasiva;
	}

	public void setSelectedPasiva(TrioRmsNeracaPasiva selectedPasiva) {
		this.selectedPasiva = selectedPasiva;
	}
	
	public String getStrIdPerk() {
		return strIdPerk;
	}

	public void setStrIdPerk(String strIdPerk) {
		this.strIdPerk = strIdPerk;
	}


	/* 
	 * start of logic code
	 * */
	
	
	@Command
	public void onSelectList(){
		this.index = getListModelRumus().indexOf(getSelectedPasiva());
	}
	
	@Command
	@NotifyChange("listModelRumus")
	public void newRecord(){
		TrioRmsNeracaPasiva trioRmsNeracaPasiva = new TrioRmsNeracaPasiva("", "", "", "", new BigDecimal(0));
		getListModelRumus().add(trioRmsNeracaPasiva);
	}

	@Command
	@NotifyChange("listModelRumus")
	public void insertRecord(){
		TrioRmsNeracaPasiva rms = new TrioRmsNeracaPasiva("", "", "", "", new BigDecimal(0));
		getListModelRumus().add(index, rms);
	}
	
	@Command
	@NotifyChange("listModelRumus")
	public void deleteRecord(){
		getListModelRumus().remove(getSelectedPasiva());
	}
	
	@Command
	@NotifyChange("listModelRumus")
	public void onChangeText(){
		String namaPerk = getMasterFacade().getTrioMstperkiraanDao().findNamaById(getStrIdPerk());
		getSelectedPasiva().setNamaPerkiraan(namaPerk);
	}
	
	@Command
	public void save(){
		try {
			DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);
			getMasterFacade().getTrioRmsNeracaPasivaDao().saveByList(getListModelRumus(), getUserSession());
			Messagebox.show("Berhasil Simpan Rumus Neraca Pasiva");
		} catch (Exception e) {
			Messagebox.show("Terdapat Id Perkiraan Yang Sama");
		}
	}
}
