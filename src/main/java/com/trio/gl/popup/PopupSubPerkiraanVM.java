package com.trio.gl.popup;

import org.zkoss.bind.annotation.Command;
import org.zkoss.zul.ListModelList;

import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioMstsubperkiraan;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;
import com.trio.gl.viewmodel.rpt.RptBukuBesarPemPerTanggalVM;

public class PopupSubPerkiraanVM extends TrioBasePageVM {
	
	
	private RptBukuBesarPemPerTanggalVM rptBukuBesarPemPerTanggalVM;
	private TrioMstsubperkiraan current;
	private ListModelList<TrioMstsubperkiraan> listModel;
	
	public TrioMstsubperkiraan getCurrent() {
		return current;
	}

	public void setCurrent(TrioMstsubperkiraan current) {
		this.current = current;
	}

	public ListModelList<TrioMstsubperkiraan> getListModel() {
		DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);
		if(listModel==null){
			listModel = new ListModelList<TrioMstsubperkiraan>();
			listModel.addAll(getMasterFacade().getTrioMstsubperkiraanDao().findAll());
		}
		return listModel;
	}
	
	public void setListModel(ListModelList<TrioMstsubperkiraan> listModel) {
		this.listModel = listModel;
	}
	
	@Command
	public void pilih(){
		System.out.println("pilih");
	}
}
