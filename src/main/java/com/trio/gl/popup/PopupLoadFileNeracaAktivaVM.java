package com.trio.gl.popup;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.Binder;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Component;

import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioDesignNeracaAkhirAktiva;
import com.trio.gl.bean.TrioDesignNeracaAkhirPasiva;
import com.trio.gl.bean.TrioLapNeraca;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;

/**
 * @author Gusti Arya 10:56:16 AM Oct 26, 2013
 */

public class PopupLoadFileNeracaAktivaVM extends TrioBasePageVM{

	private Date periode;

	public Date getPeriode() {
		if(periode == null)
			periode = new Date();
		return periode;
	}

	public void setPeriode(Date periode) {
		this.periode = periode;
	}


	@Command
	public void tarikNeraca(@ContextParam(ContextType.VIEW) Component view ){
		DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);
		
		SimpleDateFormat sdf = new SimpleDateFormat("MM");
		String month = sdf.format(getPeriode());
		System.out.println("month  = " + month);
		
		List<TrioLapNeraca> listsNeraca = getMasterFacade().getTrioLapNeracaDao().findByPeriode(month);
		if(listsNeraca.size() > 0){
			simpanAktiva(listsNeraca);
			simpanPasiva(listsNeraca);
			sendList(view);
			
		}else{
			Messagebox.show("Data Tidak Ditemukan");
			return;
		}
	}

	private void simpanAktiva(List<TrioLapNeraca> listNeraca){
		
		List<TrioDesignNeracaAkhirAktiva> listDsgNrcAkhirAktiva =
				getMasterFacade().getTrioDesignNeracaAkhirAktivaDao().findAll();
		for(TrioDesignNeracaAkhirAktiva dsg : listDsgNrcAkhirAktiva){
			String idPerkDesignNeraca = dsg.getIdPerkiraan();
			for(TrioLapNeraca nrc : listNeraca){
				String idPerkLapNeraca = nrc.getTrioLapNeracaPK().getIdPerkiraan();
				
				if(idPerkDesignNeraca.equalsIgnoreCase(idPerkLapNeraca)){
					//if 1
					if((nrc.getSlAkhD().compareTo(BigDecimal.ZERO) != 0) && (nrc.getSlAkhK().compareTo(BigDecimal.ZERO) == 0)){
						if(dsg.getKet().equalsIgnoreCase("1")){
							dsg.setLevel1(nrc.getSlAkhD());
							dsg.setNilai(nrc.getSlAkhD());
						}
						if(dsg.getKet().equalsIgnoreCase("2")){
							dsg.setLevel2(nrc.getSlAkhD());
							dsg.setNilai(nrc.getSlAkhD());
						}
					}
					
					//if 2
					if((nrc.getSlAkhK().compareTo(BigDecimal.ZERO)!= 0) && (nrc.getSlAkhD().compareTo(BigDecimal.ZERO)==0)){
						if(dsg.getKet().equalsIgnoreCase("1")){
							dsg.setLevel1(nrc.getSlAkhK().multiply(new BigDecimal(-1)));
							dsg.setNilai(nrc.getSlAkhK().multiply(new BigDecimal(-1)));
						}
						if(dsg.getKet().equalsIgnoreCase("2")){
							dsg.setLevel2(nrc.getSlAkhK());
							dsg.setNilai(nrc.getSlAkhK());
						}
					}
					
					//if3
					if((nrc.getSlAkhD().compareTo(BigDecimal.ZERO)!= 0) && (nrc.getSlAkhK().compareTo(BigDecimal.ZERO)!=0)){
						BigDecimal vrs1 = nrc.getSlAkhD().subtract(nrc.getSlAkhK());
						if(dsg.getKet().equalsIgnoreCase("1")){
							dsg.setLevel1(vrs1);
							dsg.setNilai(vrs1);
						}
						if(dsg.getKet().equalsIgnoreCase("2")){
							dsg.setLevel2(vrs1);
							dsg.setNilai(vrs1);
						}
					}
					
				}
			}
			
			getMasterFacade().getTrioDesignNeracaAkhirAktivaDao().update(dsg, getUserSession());
		}
	}

	private void simpanPasiva(List<TrioLapNeraca> listNeraca){
		List<TrioDesignNeracaAkhirPasiva> listDsgNrcAkhirPasiva = 
				getMasterFacade().getTrioDesignNeracaAkhirPasivaDao().findAll();
		for(TrioDesignNeracaAkhirPasiva dsg : listDsgNrcAkhirPasiva){
			String idPerkDesignNeraca = dsg.getIdPerkiraan();
			for(TrioLapNeraca nrc : listNeraca){
				String idPerkLapNeraca = nrc.getTrioLapNeracaPK().getIdPerkiraan();
				if(idPerkDesignNeraca.equalsIgnoreCase(idPerkLapNeraca)){
					//if 1
					if((nrc.getSlAkhD().compareTo(BigDecimal.ZERO) != 0) && (nrc.getSlAkhK().compareTo(BigDecimal.ZERO) == 0)){
						if(dsg.getKet().equalsIgnoreCase("1")){
							dsg.setLevel1(nrc.getSlAkhD().multiply(new BigDecimal(-1)));
							dsg.setNilai(nrc.getSlAkhD().multiply(new BigDecimal(-1)));
						}
						
						if(dsg.getKet().equalsIgnoreCase("2")){
							dsg.setLevel2(nrc.getSlAkhD());
							dsg.setNilai(nrc.getSlAkhD());
						}
					}
					
					//if2
					if((nrc.getSlAkhK().compareTo(BigDecimal.ZERO) != 0) && (nrc.getSlAkhD().compareTo(BigDecimal.ZERO)==0)){
						if(dsg.getKet().equalsIgnoreCase("1")){
							dsg.setLevel1(nrc.getSlAkhK());
							dsg.setNilai(nrc.getSlAkhK());
						}
						
						if(dsg.getKet().equalsIgnoreCase("2")){
							dsg.setLevel2(nrc.getSlAkhK());
							dsg.setNilai(nrc.getSlAkhK());
						}
					}
					
					//if3
					if((nrc.getSlAkhD().compareTo(BigDecimal.ZERO)!= 0) && (nrc.getSlAkhK().compareTo(BigDecimal.ZERO)!= 0)){
						BigDecimal vrs1 = nrc.getSlAkhD().subtract(nrc.getSlAkhK());
						if(dsg.getKet().equalsIgnoreCase("1")){
							dsg.setLevel1(vrs1);
							dsg.setNilai(vrs1);
						}
						
						if(dsg.getKet().equalsIgnoreCase("2")){
							dsg.setLevel2(vrs1);
							dsg.setNilai(vrs1);
						}
					}
				}
			}
			
			getMasterFacade().getTrioDesignNeracaAkhirPasivaDao().update(dsg, getUserSession());
		}
	}
	
	//mengirim hasil popupLoadFile ke layar main aplikasi
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void sendList(@ContextParam(ContextType.VIEW) Component view){
		List<TrioDesignNeracaAkhirAktiva> listAktiva =
				getMasterFacade().getTrioDesignNeracaAkhirAktivaDao().findAll();
		List<TrioDesignNeracaAkhirPasiva> listPasiva = 
				getMasterFacade().getTrioDesignNeracaAkhirPasivaDao().findAll();
		Map param = new HashMap();
		param.put("aktivaModel", listAktiva);
		param.put("pasivaModel", listPasiva);
		Binder bind = (Binder) view.getParent().getAttribute("binder");
		if(bind == null)
			return;
		bind.postCommand("sendListModel", param);
		view.detach();
	}
}
