package com.trio.gl.popup;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;

import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioDesignRugiLaba;
import com.trio.gl.bean.TrioLabaDitahan;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;

/**
 * @author Gusti Arya 2:58:01 PM Dec 17, 2013
 */

public class PopupLabaDitahanRlVM extends TrioBasePageVM {

	private String kdJudul;
	private String kdLaba;
	private BigDecimal nilaiLaba;
	
	public String getKdJudul() {
		return kdJudul;
	}
	public void setKdJudul(String kdJudul) {
		this.kdJudul = kdJudul;
	}
	public String getKdLaba() {
		return kdLaba;
	}
	public void setKdLaba(String kdLaba) {
		this.kdLaba = kdLaba;
	}
	public BigDecimal getNilaiLaba() {
		return nilaiLaba;
	}
	public void setNilaiLaba(BigDecimal nilaiLaba) {
		this.nilaiLaba = nilaiLaba;
	}

	@Command
	public void save(){
		DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);
		List<TrioDesignRugiLaba> listRl = getMasterFacade().getTrioDesignRugiLabaDao().findAll();
		List<String> kdJdList = new ArrayList<String>();
		for(TrioDesignRugiLaba current : listRl){
			kdJdList.add(current.getKdJD());
		}
		if(!kdJdList.contains(getKdJudul())){
			Messagebox.show("Kode Judul Tidak Ditemukan");
			return;
		}
		
		List<TrioLabaDitahan> listLabaDitahan = getMasterFacade().getTrioLabaDitahanDao().findAll();
		for(TrioLabaDitahan current : listLabaDitahan){
			if(current.getTrioLabaDitahanPK().getKdLaba().equalsIgnoreCase(getKdLaba())){
				current.getTrioLabaDitahanPK().setKdLaba(getKdLaba());
				current.setTotal(getNilaiLaba());
				getMasterFacade().getTrioLabaDitahanDao().saveOrUpdate(current, getUserSession());
			}else{
				current = new TrioLabaDitahan();
				current.getTrioLabaDitahanPK().setKdDlr("T13");
				current.getTrioLabaDitahanPK().setKdLaba(getKdLaba());
				current.setTotal(getNilaiLaba());
				getMasterFacade().getTrioLabaDitahanDao().saveOrUpdate(current, getUserSession());
			}
		}
		
		Messagebox.show("Proses Selesai");
	}
}
