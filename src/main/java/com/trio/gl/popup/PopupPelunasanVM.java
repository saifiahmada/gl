package com.trio.gl.popup;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.zkoss.bind.Binder;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.ListModelList;

import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioJurnal;
import com.trio.gl.bean.TrioMstperkiraan;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;

/**
 * @author Gusti Arya 3:43:06 PM Sep 12, 2013
 */

public class PopupPelunasanVM extends TrioBasePageVM{

	
	private ListModelList<TrioJurnal> listModel;
	private ListModelList<TrioJurnal> listToPass;
	private List<TrioMstperkiraan> idPerkiraan;
	private TrioMstperkiraan selectedPerkiraan;
	private TrioMstperkiraan selectedPerkiraan2;
	private TrioMstperkiraan selectedPerkiraan3;
	private TrioJurnal selectedJurnal;
	private BigDecimal bd;
	private String search;
	
	public BigDecimal getBd() {
		if(bd == null)
			bd = new BigDecimal(0);
		return bd;
	}

	public void setBd(BigDecimal bd) {
		this.bd = bd;
	}

	public TrioJurnal getSelectedJurnal() {
		return selectedJurnal;
	}

	public void setSelectedJurnal(TrioJurnal selectedJurnal) {
		this.selectedJurnal = selectedJurnal;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public TrioMstperkiraan getSelectedPerkiraan() {
		if(selectedPerkiraan == null)
			selectedPerkiraan = new TrioMstperkiraan();
		return selectedPerkiraan;
	}

	public void setSelectedPerkiraan(TrioMstperkiraan selectedPerkiraan) {
		this.selectedPerkiraan = selectedPerkiraan;
	}
	
	public TrioMstperkiraan getSelectedPerkiraan2() {
		if(selectedPerkiraan2 == null)
			selectedPerkiraan2 = new TrioMstperkiraan();
		return selectedPerkiraan2;
	}

	public void setSelectedPerkiraan2(TrioMstperkiraan selectedPerkiraan2) {
		this.selectedPerkiraan2 = selectedPerkiraan2;
	}
	
	public TrioMstperkiraan getSelectedPerkiraan3() {
		if(selectedPerkiraan3 == null)
			selectedPerkiraan3 = new TrioMstperkiraan();
		return selectedPerkiraan3;
	}

	public void setSelectedPerkiraan3(TrioMstperkiraan selectedPerkiraan3) {
		this.selectedPerkiraan3 = selectedPerkiraan3;
	}

	public List<TrioMstperkiraan> getIdPerkiraan() {
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		return getMasterFacade().getTrioMstperkiraanDao().getIdPerkiraanPiutang();
	}

	public void setIdPerkiraan(List<TrioMstperkiraan> idPerkiraan) {
		this.idPerkiraan = idPerkiraan;
	}
	

	public ListModelList<TrioJurnal> getListModel() {
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		if(listModel == null)
			listModel = new ListModelList<TrioJurnal>();
		return listModel;
	}

	public void setListModel(ListModelList<TrioJurnal> listModel) {
		this.listModel = listModel;
	}
	
	public ListModelList<TrioJurnal> getListToPass() {
		return listToPass;
	}

	public void setListToPass(ListModelList<TrioJurnal> listToPass) {
		this.listToPass = listToPass;
	}

	@NotifyChange("listModel")
	@Command
	public void show(){
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		
		String idPerk = getSelectedPerkiraan().getTrioMstperkiraanPK().getIdPerkiraan();
		String idPerk2 = getSelectedPerkiraan2().getTrioMstperkiraanPK().getIdPerkiraan();
		String idPerk3  = getSelectedPerkiraan3().getTrioMstperkiraanPK().getIdPerkiraan();
		
		System.out.println("idPerk = " + idPerk);
		System.out.println("idPerk2 = " + idPerk2);
		System.out.println("idPerk3 = " + idPerk3);
		
		ListModelList<TrioJurnal> iList = new ListModelList<TrioJurnal>();
		iList.addAll(getMasterFacade().getTrioJurnalDao().getNotLunasJurnal(idPerk,idPerk2, idPerk3));
		iList.setMultiple(true);
		
		setListModel(iList);
	}
	
	@Command("pilih")
	@NotifyChange({"bd","listModel"})
	public void onPilih(){
		
		BigDecimal bd = new BigDecimal(0);
		Set<TrioJurnal> set = getListModel().getSelection();
		if(set.size() >= 0){
			Clients.showNotification("Jurnal dipilih : " + set.size(), "info", null, "top_right", 2000);
		}
		
		for(TrioJurnal current : set){
			
			if(current.getDebet().compareTo(BigDecimal.ZERO)==0){
				bd = bd.subtract(current.getKredit());
			}else{
				bd = bd.add(current.getDebet());
			}
			setBd(bd);
		}
		
		if(set.size() == 0){
			setBd(new BigDecimal(0));
		}
		
		ListModelList<TrioJurnal> listPass = new ListModelList<TrioJurnal>();
		listPass.addAll(set);
		setListToPass(listPass);
	}
	
	@Command
	@NotifyChange("listModel")
	public void search(){
		ListModelList<TrioJurnal> cekLists = new ListModelList<TrioJurnal>();
		cekLists = getListModel();
		
		Iterator<TrioJurnal> iterate = cekLists.iterator();
		while(iterate.hasNext()){
			TrioJurnal tj = iterate.next();
			if(!tj.getKet().contains(search)){
				iterate.remove();
			}
		}
		
		if(search.length() <= 0){
			show();
		}
	}
	
	//pass list ke form trnJurnalMemorial
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Command
	public void simpan(@ContextParam(ContextType.VIEW) Component view){
		Map param = new HashMap();
		param.put("iModel", getListToPass());
		Binder bind = (Binder) view.getParent().getAttribute("binder");
		if (bind == null) return;
		bind.postCommand("sendListModel", param);
		view.detach();
	}
	
	@Command
	public void exit(@ContextParam(ContextType.VIEW) Component view){
		view.detach();
	}
}
