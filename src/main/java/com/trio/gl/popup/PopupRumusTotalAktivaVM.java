package com.trio.gl.popup;

import java.math.BigDecimal;
import java.util.List;

import org.hibernate.NonUniqueObjectException;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zul.ListModelList;

import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioRmsNeracaAktiva;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;

/**
 * @author Gusti Arya 11:41:15 AM Oct 31, 2013
 */

public class PopupRumusTotalAktivaVM extends TrioBasePageVM{

	private int index;
	private String strIdPerk;
	private TrioRmsNeracaAktiva selectedRumus;
	private ListModelList<TrioRmsNeracaAktiva> listModelRumus;

	public ListModelList<TrioRmsNeracaAktiva> getListModelRumus() {
		DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);
		if(listModelRumus == null){
			listModelRumus = new ListModelList<TrioRmsNeracaAktiva>();
			List<TrioRmsNeracaAktiva> listRumus = getMasterFacade().getTrioRmsNeracaAktivaDao().findAll();
			listModelRumus.addAll(listRumus);
		}
		return listModelRumus;
	}

	public void setListModelRumus(ListModelList<TrioRmsNeracaAktiva> listModelRumus) {
		this.listModelRumus = listModelRumus;
	}

	public TrioRmsNeracaAktiva getSelectedRumus() {
		return selectedRumus;
	}

	public void setSelectedRumus(TrioRmsNeracaAktiva selectedRumus) {
		this.selectedRumus = selectedRumus;
	}

	public String getStrIdPerk() {
		return strIdPerk;
	}

	public void setStrIdPerk(String strIdPerk) {
		this.strIdPerk = strIdPerk;
	}

	@Command
	@NotifyChange("listModelRumus")
	public void newRecord(){
		TrioRmsNeracaAktiva trioRmsNeracaAktiva = new TrioRmsNeracaAktiva("", "", "", "", new BigDecimal(0));
		getListModelRumus().add(trioRmsNeracaAktiva);

	}

	@Command
	@NotifyChange("listModelRumus")
	public void insertRecord(){
		TrioRmsNeracaAktiva rms = new TrioRmsNeracaAktiva("", "", "", "", new BigDecimal(0));
		getListModelRumus().add(index, rms);
	}

	@Command
	@NotifyChange("listModelRumus")
	public void deleteRecord(){
		getListModelRumus().remove(getSelectedRumus());
	}	

	@Command
	public void onSelectList(){
		index = getListModelRumus().indexOf(getSelectedRumus());
	}

	@Command
	@NotifyChange({"listModelRumus"})
	public void onChangeText(){
		String namaPerk = getMasterFacade().getTrioMstperkiraanDao().findNamaById(getStrIdPerk());
		getSelectedRumus().setNamaPerkiraan(namaPerk);
	}
	
	@Command
	@NotifyChange("listModelRumus")
	public void save(){
		try {
			DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);
			getMasterFacade().getTrioRmsNeracaAktivaDao().saveByList(getListModelRumus(), getUserSession());
			Messagebox.show("Berhasil Simpan Rumus Neraca Aktiva");
		} catch (NonUniqueObjectException e) {
			Messagebox.show("Terdapat Id Perkiraan Yang Sama");
		}
	}
	
}
