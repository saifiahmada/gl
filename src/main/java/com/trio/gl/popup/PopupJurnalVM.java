package com.trio.gl.popup;

import java.util.HashMap;
import java.util.Map;

import org.zkoss.bind.Binder;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.ListModelList;

import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioJurnal;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;

public class PopupJurnalVM extends TrioBasePageVM {

	private TrioJurnal current;
	private ListModelList<TrioJurnal> listModel;

	public TrioJurnal getCurrent() {
		return current;
	}

	public void setCurrent(TrioJurnal current) {
		this.current = current;
	}

	public ListModelList<TrioJurnal> getListModel() {
//		DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		if (listModel == null){
			listModel = new ListModelList<TrioJurnal>();
			listModel.addAll(getMasterFacade().getTrioJurnalDao().getListJurnalMemorial());
		}
		return listModel;
	}

	public void setListModel(ListModelList<TrioJurnal> listModel) {
		this.listModel = listModel;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Command("pilih")
	public void pilih(@ContextParam (ContextType.VIEW) Component view){
		Map param = new HashMap();
		param.put("idJurnal", current);
		Binder bind = (Binder) view.getParent().getAttribute("binder");
		if (bind == null) return;
		bind.postCommand("sendIdJurnal", param);
		view.detach();
	}
}
