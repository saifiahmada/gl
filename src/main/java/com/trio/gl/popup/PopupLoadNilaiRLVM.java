package com.trio.gl.popup;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.Binder;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.zk.ui.Component;

import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioDesignRugiLaba;
import com.trio.gl.bean.TrioLapNeraca;

public class PopupLoadNilaiRLVM extends TrioBasePageVM{

	private Date bulan;
	private boolean checked;

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public Date getBulan() {
		if(bulan==null){
			bulan = new Date();
		}
		return bulan;
	}

	public void setBulan(Date bulan) {
		this.bulan = bulan;
	}

	@Command
	public void proses(@ContextParam(ContextType.VIEW) Component view){
		//pertahun is checked
		if(isChecked()){
			tarikPerTahun();
		}
		//pertahun not checked	
		else{
			tarikPerBulan();
		}


		List<TrioDesignRugiLaba> listRl = getMasterFacade().getTrioDesignRugiLabaDao().findAll();
		List<TrioDesignRugiLaba> listRlToSave = new ArrayList<TrioDesignRugiLaba>();
		for(TrioDesignRugiLaba current : listRl){
			current.setTmp1(current.getLevel1());
			current.setTmp2(current.getLevel2());
			current.setTmp3(current.getLevel3());
			current.setTmp4(current.getLevel4());
			current.setTmpGroup(current.getNamaPerkiraan());
			listRlToSave.add(current);
		}
		getMasterFacade().getTrioDesignRugiLabaDao().updateByList(listRlToSave, getUserSession());

		sendList(view);
	}

	public void tarikPerBulan(){
		SimpleDateFormat sdf = new SimpleDateFormat("MM");
		String bulan = sdf.format(getBulan());
		List<TrioLapNeraca> listNrc = getMasterFacade().getTrioLapNeracaDao().findByPeriode(bulan);
		List<TrioDesignRugiLaba> listRugiLaba = getMasterFacade().getTrioDesignRugiLabaDao().findAll();
		List<TrioDesignRugiLaba> listRlToSave = new ArrayList<TrioDesignRugiLaba>();

		for(TrioLapNeraca nrc : listNrc){
			for(TrioDesignRugiLaba rl :listRugiLaba){

				if(rl.getIdPerkiraan().equalsIgnoreCase(nrc.getTrioLapNeracaPK().getIdPerkiraan())){

					if((nrc.getMtsD().compareTo(new BigDecimal(0)) != 0) && (nrc.getMtsK().compareTo(new BigDecimal(0))!=0)){
						BigDecimal vrs = nrc.getMtsD().subtract(nrc.getMtsK());

						if(vrs.compareTo(new BigDecimal(0)) == -1){
							vrs = vrs.multiply(new BigDecimal(-1));
						}

						if(rl.getKet().equalsIgnoreCase("1")){
							rl.setLevel1(vrs);
							rl.setNilai(vrs);
						}

						if(rl.getKet().equalsIgnoreCase("2")){
							rl.setLevel2(vrs);
							rl.setNilai(vrs);
						}

						if(rl.getKet().equalsIgnoreCase("3")){
							rl.setLevel3(vrs);
							rl.setNilai(vrs);
						}

						if(rl.getKet().equalsIgnoreCase("4")){
							rl.setLevel4(vrs);
							rl.setNilai(vrs);
						}
					}

					if((nrc.getMtsD().compareTo(new BigDecimal(0)) != 0) && (nrc.getMtsK().compareTo(new BigDecimal(0)) == 0)){
						if(rl.getKet().equalsIgnoreCase("1")){
							rl.setLevel1(nrc.getMtsD());
							rl.setNilai(nrc.getMtsD());
						}

						if(rl.getKet().equalsIgnoreCase("2")){
							rl.setLevel2(nrc.getMtsD());
							rl.setNilai(nrc.getMtsD());
						}

						if(rl.getKet().equalsIgnoreCase("3")){
							rl.setLevel3(nrc.getMtsD());
							rl.setNilai(nrc.getMtsD());
						}

						if(rl.getKet().equalsIgnoreCase("4")){
							rl.setLevel4(nrc.getMtsD());
							rl.setNilai(nrc.getMtsD());
						}

					}

					if((nrc.getMtsK().compareTo(new BigDecimal(0)) != 0) && (nrc.getMtsD().compareTo(new BigDecimal(0)) == 0)){

						if(rl.getKet().equalsIgnoreCase("1")){
							rl.setLevel1(nrc.getMtsK());
							rl.setNilai(nrc.getMtsK());
						}

						if(rl.getKet().equalsIgnoreCase("2")){
							rl.setLevel2(nrc.getMtsK());
							rl.setNilai(nrc.getMtsK());
						}

						if(rl.getKet().equalsIgnoreCase("3")){
							rl.setLevel3(nrc.getMtsK());
							rl.setNilai(nrc.getMtsK());
						}

						if(rl.getKet().equalsIgnoreCase("4")){
							rl.setLevel4(nrc.getMtsK());
							rl.setNilai(nrc.getMtsK());
						}
					}
				}
				listRlToSave.add(rl);
			}
		}

		getMasterFacade().getTrioDesignRugiLabaDao().updateByList(listRlToSave, getUserSession());
	}


	public void tarikPerTahun(){
		List<TrioDesignRugiLaba> listRugiLaba = getMasterFacade().getTrioDesignRugiLabaDao().findAll();
		List<TrioLapNeraca> listQLapNrc = getMasterFacade().getTrioLapNeracaDao().findBySumRl();
		List<TrioDesignRugiLaba> listToSave = new ArrayList<TrioDesignRugiLaba>();

		for(TrioLapNeraca nrc : listQLapNrc){
			for(TrioDesignRugiLaba rl : listRugiLaba){

				if(rl.getIdPerkiraan().equalsIgnoreCase(nrc.getTrioLapNeracaPK().getIdPerkiraan())){
					if(nrc.getMtsD().compareTo(new BigDecimal(0))!=0 && nrc.getMtsK().compareTo(new BigDecimal(0))!=0){
						BigDecimal vrs = nrc.getMtsD().subtract(nrc.getMtsK());

						if(vrs.compareTo(new BigDecimal(0)) == -1){
							vrs = vrs.multiply(new BigDecimal(-1));
						}

						if (rl.getKet().equalsIgnoreCase("1")) {
							rl.setLevel1(vrs);
							rl.setNilai(vrs);
						}

						if (rl.getKet().equalsIgnoreCase("2")) {
							rl.setLevel2(vrs);
							rl.setNilai(vrs);
						}

						if (rl.getKet().equalsIgnoreCase("3")) {
							rl.setLevel3(vrs);
							rl.setNilai(vrs);
						}

						if (rl.getKet().equalsIgnoreCase("4")) {
							rl.setLevel4(vrs);
							rl.setNilai(vrs);
						}
					}

					if(nrc.getMtsD().compareTo(new BigDecimal(0))!=0 && nrc.getMtsK().compareTo(new BigDecimal(0))==0){
						if(rl.getKet().equalsIgnoreCase("1")){
							rl.setLevel1(nrc.getMtsD());
							rl.setNilai(nrc.getMtsD());
						}

						if(rl.getKet().equalsIgnoreCase("2")){
							rl.setLevel2(nrc.getMtsD());
							rl.setNilai(nrc.getMtsD());
						}

						if(rl.getKet().equalsIgnoreCase("3")){
							rl.setLevel3(nrc.getMtsD());
							rl.setNilai(nrc.getMtsD());
						}

						if(rl.getKet().equalsIgnoreCase("4")){
							rl.setLevel4(nrc.getMtsD());
							rl.setNilai(nrc.getMtsD());
						}

					}

					if(nrc.getMtsK().compareTo(new BigDecimal(0))!=0 && nrc.getMtsD().compareTo(new BigDecimal(0))==0){

						if(rl.getKet().equalsIgnoreCase("1")){
							rl.setLevel1(nrc.getMtsK());
							rl.setNilai(nrc.getMtsD());
						}

						if(rl.getKet().equalsIgnoreCase("2")){
							rl.setLevel2(nrc.getMtsK());
							rl.setNilai(nrc.getMtsD());
						}

						if(rl.getKet().equalsIgnoreCase("3")){
							rl.setLevel2(nrc.getMtsK());
							rl.setNilai(nrc.getMtsD());
						}

						if(rl.getKet().equalsIgnoreCase("4")){
							rl.setLevel4(nrc.getMtsK());
							rl.setNilai(nrc.getMtsD());
						}
					}
				}

				listToSave.add(rl);
			}
		}
		getMasterFacade().getTrioDesignRugiLabaDao().updateByList(listToSave, getUserSession());
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void sendList(@ContextParam(ContextType.VIEW) Component view){
		List<TrioDesignRugiLaba> listRugiLaba = getMasterFacade().getTrioDesignRugiLabaDao().findAll();
		Map param = new HashMap();
		param.put("rugiLabaModel", listRugiLaba);
		Binder bind = (Binder) view.getParent().getAttribute("binder");
		if(bind == null)
			return;
		bind.postCommand("sendListModel", param);
		view.detach();
	}
}
