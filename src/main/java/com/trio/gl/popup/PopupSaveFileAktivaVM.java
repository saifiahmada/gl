package com.trio.gl.popup;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;

import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioDesignNeracaAkhirAktiva;
import com.trio.gl.bean.TrioLapNeracaAktiva;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;

/**
 * @author Gusti Arya 11:42:10 AM Oct 30, 2013
 */

public class PopupSaveFileAktivaVM extends TrioBasePageVM{

	private Date periode;

	public Date getPeriode() {
		if(periode==null)
			periode = new Date();
		return periode;
	}

	@NotifyChange("periode")
	public void setPeriode(Date periode) {
		this.periode = periode;
	}

	@Command
	public void save(){
		DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);
		SimpleDateFormat sdf = new SimpleDateFormat("MM");
		String strPeriode = sdf.format(getPeriode());

		List<TrioLapNeracaAktiva> lists = getMasterFacade().getTrioLapNeracaAktivaDao().findByPeriode(strPeriode);
		System.out.println("size = " + lists.size());
		if(lists.size() > 0){
			Messagebox.show("Periode Neraca Akhir Sudah Ada");
			return;
		}else{
			List<TrioDesignNeracaAkhirAktiva> listDsgNrcAktiva = 
					getMasterFacade().getTrioDesignNeracaAkhirAktivaDao().findAll();
			List<TrioLapNeracaAktiva> listToSave = new ArrayList<TrioLapNeracaAktiva>();
			
			for(TrioDesignNeracaAkhirAktiva dsg : listDsgNrcAktiva){
				TrioLapNeracaAktiva nrcAktiva = new TrioLapNeracaAktiva();
				nrcAktiva.setPeriode(strPeriode);
				nrcAktiva.setIdPerkiraan(dsg.getIdPerkiraan());
				nrcAktiva.setKdJd(dsg.getKdJd());
				nrcAktiva.setNamaPerkiraan(dsg.getNamaPerkiraan());
				nrcAktiva.setKet(dsg.getKet());
				nrcAktiva.setKet1(dsg.getKet1());
				nrcAktiva.setLevel1(dsg.getLevel1());
				nrcAktiva.setLevel2(dsg.getLevel2());
				nrcAktiva.setNilai(dsg.getNilai());
				listToSave.add(nrcAktiva);
			}
			
			getMasterFacade().getTrioLapNeracaAktivaDao().saveFromCollection(listToSave, getUserSession());
			Messagebox.show("Proses Selesai");
		}
	}
}
