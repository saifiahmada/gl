package com.trio.gl.popup;

import java.math.BigDecimal;
import java.util.List;

import org.hibernate.NonUniqueObjectException;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zul.ListModelList;

import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioRumus2;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;

/**
 * @author Gusti Arya 2:18:15 PM Dec 17, 2013
 */

public class PopupRumus2RugiLabaVM extends TrioBasePageVM {

	private int index;
	private ListModelList<TrioRumus2> listModelRumus2;
	private TrioRumus2 selectedItem;


	public ListModelList<TrioRumus2> getListModelRumus2() {
		DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);
		if(listModelRumus2 == null){
			listModelRumus2 = new ListModelList<TrioRumus2>();
			List<TrioRumus2> lists = getMasterFacade().getTrioRumus2Dao().findAll();
			listModelRumus2.addAll(lists);
		}
		return listModelRumus2;
	}

	public void setListModelRumus2(ListModelList<TrioRumus2> listModelRumus2) {
		this.listModelRumus2 = listModelRumus2;
	}

	public TrioRumus2 getSelectedItem() {
		return selectedItem;
	}

	public void setSelectedItem(TrioRumus2 selectedItem) {
		this.selectedItem = selectedItem;
	}

	@Command
	public void onSelectList(){
		index = getListModelRumus2().indexOf(getSelectedItem());
		System.out.println(index);
	}

	@Command
	@NotifyChange("listModelRumus2")
	public void newRecord(){
		TrioRumus2 rms = new TrioRumus2("", "", "", ' ' , new BigDecimal(0));
		getListModelRumus2().add(rms);

	}

	@Command
	@NotifyChange("listModelRumus2")
	public void insertRecord(){
		TrioRumus2 rms = new TrioRumus2("", "", "", ' ' , new BigDecimal(0));
		getListModelRumus2().add(index, rms);
	}

	@Command
	@NotifyChange("listModelRumus2")
	public void deleteRecord(){
		getListModelRumus2().remove(getSelectedItem());
	}

	@Command
	@NotifyChange("listModelRumus2")
	public void save(){
		try {
			getMasterFacade().getTrioRumus2Dao().saveByList(getListModelRumus2(), getUserSession());
			Messagebox.show("Berhasil Simpan Rumus 2");
		} catch (NonUniqueObjectException e) {
			e.printStackTrace();
			Messagebox.show("Terdapat Kode Perkiraan yang sama");
		}
	}

}
