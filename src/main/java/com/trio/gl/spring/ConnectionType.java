package com.trio.gl.spring;

/** @author Saifi Ahmada Feb 22, 2013 3:51:50 PM  **/

public enum ConnectionType {
	
	PERINTIS("PERINTIS"), KOLONEL("KOLONEL"), DUANAGA("DUANAGA"),DEFAULT("DEFAULT"),
	ORACLEUNIT("ORACLEUNIT"), ORACLEPART("ORACLEPART") ;

    private String letter;
    private ConnectionType(String letter) {
        this.letter = letter;
    }

    public static ConnectionType setType(String letter) {
        for (ConnectionType s : values() ){
            if (s.letter.equals(letter)) return s;
        }
        return null;
    }
	
}
