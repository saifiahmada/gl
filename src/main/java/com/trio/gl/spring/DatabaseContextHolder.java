package com.trio.gl.spring;

import org.springframework.util.Assert;

/** @author Saifi Ahmada Feb 22, 2013 3:51:14 PM  **/

public class DatabaseContextHolder {

	private static final ThreadLocal<ConnectionType> contextHolder = new ThreadLocal<ConnectionType>();
	
	public static void setConnectionType(ConnectionType connectionType){
		Assert.notNull(connectionType, "ConnectionType ra iso null to le");
		contextHolder.set(connectionType);
	}
	
	public static ConnectionType getConnectionType(){
		return contextHolder.get();
	}
	
	public static void clearConnectionType(){
		contextHolder.remove();
	}
	
	
}

