package com.trio.gl.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;


/** @author Saifi Ahmada Jul 11, 2013 10:26:23 AM  **/
@Embeddable
public class TrioStokIntransitPK implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Column(name="NO_FAKTUR", length=22)
	private String noFaktur;
	
	@Column(name="PART_NUM", length=20)
	private String partNum;
	
	public TrioStokIntransitPK() {
	}
	
	public TrioStokIntransitPK(String noFaktur, String partNum) {
		this.noFaktur = noFaktur;
		this.partNum = partNum; 
		
	}

	public String getNoFaktur() {
		return noFaktur;
	}

	public void setNoFaktur(String noFaktur) {
		this.noFaktur = noFaktur;
	}

	public String getPartNum() {
		return partNum;
	}

	public void setPartNum(String partNum) {
		this.partNum = partNum;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((noFaktur == null) ? 0 : noFaktur.hashCode());
		result = prime * result + ((partNum == null) ? 0 : partNum.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioStokIntransitPK other = (TrioStokIntransitPK) obj;
		if (noFaktur == null) {
			if (other.noFaktur != null)
				return false;
		} else if (!noFaktur.equals(other.noFaktur))
			return false;
		if (partNum == null) {
			if (other.partNum != null)
				return false;
		} else if (!partNum.equals(other.partNum))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioStokIntransitPK [noFaktur=" + noFaktur + ", partNum="
				+ partNum + "]";
	}

}

