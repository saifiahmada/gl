package com.trio.gl.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @author Gusti Arya 9:44:16 AM Sep 30, 2013
 */

@Embeddable
public class TrioLabaDitahanPK implements Serializable{
	
	private static final long serialVersionUID = 1L;
	

	@Column(name="KD_DLR", nullable=false, length=3)
	private String kdDlr;
	
	@Column(name="KD_LABA", nullable=false, length=4)
	private String kdLaba;

	public String getKdDlr() {
		return kdDlr;
	}

	public void setKdDlr(String kdDlr) {
		this.kdDlr = kdDlr;
	}

	public String getKdLaba() {
		return kdLaba;
	}

	public void setKdLaba(String kdLaba) {
		this.kdLaba = kdLaba;
	}
	
	public TrioLabaDitahanPK() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((kdDlr == null) ? 0 : kdDlr.hashCode());
		result = prime * result + ((kdLaba == null) ? 0 : kdLaba.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioLabaDitahanPK other = (TrioLabaDitahanPK) obj;
		if (kdDlr == null) {
			if (other.kdDlr != null)
				return false;
		} else if (!kdDlr.equals(other.kdDlr))
			return false;
		if (kdLaba == null) {
			if (other.kdLaba != null)
				return false;
		} else if (!kdLaba.equals(other.kdLaba))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioLabaDitahanPK [kdDlr=" + kdDlr + ", kdLaba=" + kdLaba + "]";
	}
	
}
