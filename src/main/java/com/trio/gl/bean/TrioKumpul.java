package com.trio.gl.bean;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.trio.gl.usertrail.TrioEntityUserTrail;

/**
 * @author Gusti Arya 3:52:44 PM Sep 3, 2013
 */

@Entity
@Table(name="TRIO_KUMPUL")
public class TrioKumpul extends TrioEntityUserTrail implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private TrioKumpulPK trioKumpulPK;
	
	@Column(name="TOTAL", length=12, precision=12, scale=0)
	private BigDecimal total;

	
	
	public TrioKumpul() {
		if(trioKumpulPK == null)
		trioKumpulPK = new TrioKumpulPK();
	}
	

	public TrioKumpulPK getTrioKumpulPK() {
		return trioKumpulPK;
	}

	public void setTrioKumpulPK(TrioKumpulPK trioKumpulPK) {
		this.trioKumpulPK = trioKumpulPK;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((trioKumpulPK == null) ? 0 : trioKumpulPK.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioKumpul other = (TrioKumpul) obj;
		if (trioKumpulPK == null) {
			if (other.trioKumpulPK != null)
				return false;
		} else if (!trioKumpulPK.equals(other.trioKumpulPK))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioKumpul [trioKumpulPK=" + trioKumpulPK + "]";
	}
	

}
