package com.trio.gl.bean;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.trio.gl.usertrail.TrioEntityUserTrail;

/**
 * @author Gusti Arya 9:37:35 AM Nov 4, 2013
 */

@Entity
@Table(name="TRIO_RMS_NERACA_AKTIVA_SUB")
public class TrioRmsNeracaAktivaSub extends TrioEntityUserTrail{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="KD_HSL", length=3)
	private String kdHsl;
	
	@Column(name="HASIL",length=3)
	private String hasil;
	
	@Column(name="NAMA_PERKIRAAN")
	private String namaPerkiraan;
	
	@Column(name="JENIS",length=1)
	private String jenis;
	
	@Column(name="NILAI", precision=12, scale=0)
	private BigDecimal nilai;

	public TrioRmsNeracaAktivaSub() {
		// TODO Auto-generated constructor stub
	}
	
	public TrioRmsNeracaAktivaSub(String hasil, BigDecimal nilai){
		this.hasil = hasil;
		this.nilai = nilai;
	}
	
	public TrioRmsNeracaAktivaSub(String kdHsl, String hasil,
			String namaPerkiraan, String jenis, BigDecimal nilai) {
		super();
		this.kdHsl = kdHsl;
		this.hasil = hasil;
		this.namaPerkiraan = namaPerkiraan;
		this.jenis = jenis;
		this.nilai = nilai;
	}

	public String getKdHsl() {
		return kdHsl;
	}

	public void setKdHsl(String kdHsl) {
		this.kdHsl = kdHsl;
	}

	public String getHasil() {
		return hasil;
	}

	public void setHasil(String hasil) {
		this.hasil = hasil;
	}

	public String getNamaPerkiraan() {
		return namaPerkiraan;
	}

	public void setNamaPerkiraan(String namaPerkiraan) {
		this.namaPerkiraan = namaPerkiraan;
	}

	public String getJenis() {
		return jenis;
	}

	public void setJenis(String jenis) {
		this.jenis = jenis;
	}

	public BigDecimal getNilai() {
		return nilai;
	}

	public void setNilai(BigDecimal nilai) {
		this.nilai = nilai;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((kdHsl == null) ? 0 : kdHsl.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioRmsNeracaAktivaSub other = (TrioRmsNeracaAktivaSub) obj;
		if (kdHsl == null) {
			if (other.kdHsl != null)
				return false;
		} else if (!kdHsl.equals(other.kdHsl))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioRmsNeracaAktivaSub [hasil=" + hasil + "]";
	}
	
}
