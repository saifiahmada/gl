package com.trio.gl.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/** @author Saifi Ahmada Apr 3, 2013 11:51:21 AM  **/

@Embeddable
public class TrioMstkonfigcabangPK implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Column(name="KD_DLR", length=3, nullable=false)
	private String kdDlr;
	@Column(name="CONTEXT", length=4, nullable=false)
	private String context;
	
	public TrioMstkonfigcabangPK() {
	
	}
	
	public TrioMstkonfigcabangPK(String kdDlr, String context) {
		this.kdDlr = kdDlr;
		this.context = context;
	}

	public String getKdDlr() {
		return kdDlr;
	}

	public void setKdDlr(String kdDlr) {
		this.kdDlr = kdDlr;
	}

	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((context == null) ? 0 : context.hashCode());
		result = prime * result + ((kdDlr == null) ? 0 : kdDlr.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioMstkonfigcabangPK other = (TrioMstkonfigcabangPK) obj;
		if (context == null) {
			if (other.context != null)
				return false;
		} else if (!context.equals(other.context))
			return false;
		if (kdDlr == null) {
			if (other.kdDlr != null)
				return false;
		} else if (!kdDlr.equals(other.kdDlr))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioMstkonfigcabangPK [kdDlr=" + kdDlr + ", context=" + context
				+ "]";
	}
	
}

