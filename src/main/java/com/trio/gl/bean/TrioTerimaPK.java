package com.trio.gl.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/** @author Saifi Ahmada Sep 27, 2013 9:33:22 AM  **/
@Embeddable
public class TrioTerimaPK implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Column(name="NO_LKH", length=16, nullable=false)
	private String noLKH;//NO_LKH	varchar(16)
	
	@Column(name="NO_FAK", length=17)
	private String noFak; //NO_FAK	varchar(17)
	
	public TrioTerimaPK() {
		
	}
	
	public TrioTerimaPK(String noLKH, String noFak) {
		this.noLKH = noLKH;
		this.noFak = noFak;
	}

	public String getNoLKH() {
		return noLKH;
	}

	public void setNoLKH(String noLKH) {
		this.noLKH = noLKH;
	}

	public String getNoFak() {
		return noFak;
	}

	public void setNoFak(String noFak) {
		this.noFak = noFak;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((noFak == null) ? 0 : noFak.hashCode());
		result = prime * result + ((noLKH == null) ? 0 : noLKH.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioTerimaPK other = (TrioTerimaPK) obj;
		if (noFak == null) {
			if (other.noFak != null)
				return false;
		} else if (!noFak.equals(other.noFak))
			return false;
		if (noLKH == null) {
			if (other.noLKH != null)
				return false;
		} else if (!noLKH.equals(other.noLKH))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioTerimaPK [noLKH=" + noLKH + ", noFak=" + noFak + "]";
	}
	
}

