package com.trio.gl.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.trio.gl.usertrail.TrioEntityUserTrail;

/** @author Saifi Ahmada Apr 12, 2013 9:57:09 PM  **/
@Entity
@Table(name="TRIO_LAP_BUKUBESAR")
public class TrioLapBukubesar extends TrioEntityUserTrail implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private TrioLapBukubesarPK trioLapBukubesarPK;
	
	@Column(name="TANGGAL")
	@Temporal(TemporalType.DATE)
	private Date tanggal;
	@Column(name="DEBET", precision=17, scale=2)
	private BigDecimal debet;
	@Column(name="KREDIT", precision=17,scale=2)
	private BigDecimal kredit;
	@Column(name="SALDO_AWAL", precision=17,scale=2)
	private BigDecimal saldoAwal;
	@Column(name="SALDO_AKHIR", precision=17,scale=2)
	private BigDecimal saldoAkhir;
	@Column(name="KET_AWAL",length=1)
	private String ketAwal;
	@Column(name="KET", length=100)
	private String ket;
	@Column(name="TIPE", length=1)
	private String tipe;
	@Column(name="PERIODE", length=6)
	private String periode;
	
	@ManyToOne
	@JoinColumn(name="ID_PERKIRAAN", referencedColumnName="ID_PERKIRAAN", insertable=false, updatable=false)
	private TrioMstperkiraan trioMstperkiraan;
	
	public TrioMstperkiraan getTrioMstperkiraan() {
		return trioMstperkiraan;
	}

	public void setTrioMstperkiraan(TrioMstperkiraan trioMstperkiraan) {
		this.trioMstperkiraan = trioMstperkiraan;
	}

	public TrioLapBukubesar() {
		this.trioLapBukubesarPK = new TrioLapBukubesarPK();
		this.trioMstperkiraan = new TrioMstperkiraan();
	}
	
	public TrioLapBukubesar(String idJurnal, String idPerkiraan, String idSub ) {
		this.trioLapBukubesarPK = new TrioLapBukubesarPK(idJurnal, idPerkiraan, idSub);
		this.trioMstperkiraan = new TrioMstperkiraan();
	}
	
	public TrioLapBukubesar(String idPerkiraan, String idSub, BigDecimal saldoAwal){
		this.trioLapBukubesarPK = new TrioLapBukubesarPK("", idPerkiraan, idSub);
		this.saldoAwal = saldoAwal;
	}

	public TrioLapBukubesarPK getTrioLapBukubesarPK() {
		return trioLapBukubesarPK;
	}

	public void setTrioLapBukubesarPK(TrioLapBukubesarPK trioLapBukubesarPK) {
		this.trioLapBukubesarPK = trioLapBukubesarPK;
	}

	public Date getTanggal() {
		return tanggal;
	}

	public void setTanggal(Date tanggal) {
		this.tanggal = tanggal;
	}

	public BigDecimal getDebet() {
		return debet;
	}

	public void setDebet(BigDecimal debet) {
		this.debet = debet;
	}

	public BigDecimal getKredit() {
		return kredit;
	}

	public void setKredit(BigDecimal kredit) {
		this.kredit = kredit;
	}

	public BigDecimal getSaldoAwal() {
		return saldoAwal;
	}

	public void setSaldoAwal(BigDecimal saldoAwal) {
		this.saldoAwal = saldoAwal;
	}

	public BigDecimal getSaldoAkhir() {
		return saldoAkhir;
	}

	public void setSaldoAkhir(BigDecimal saldoAkhir) {
		this.saldoAkhir = saldoAkhir;
	}

	public String getKetAwal() {
		return ketAwal;
	}

	public void setKetAwal(String ketAwal) {
		this.ketAwal = ketAwal;
	}

	public String getKet() {
		return ket;
	}

	public void setKet(String ket) {
		this.ket = ket;
	}

	public String getTipe() {
		return tipe;
	}

	public void setTipe(String tipe) {
		this.tipe = tipe;
	}

	public String getPeriode() {
		return periode;
	}

	public void setPeriode(String periode) {
		this.periode = periode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((trioLapBukubesarPK == null) ? 0 : trioLapBukubesarPK
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioLapBukubesar other = (TrioLapBukubesar) obj;
		if (trioLapBukubesarPK == null) {
			if (other.trioLapBukubesarPK != null)
				return false;
		} else if (!trioLapBukubesarPK.equals(other.trioLapBukubesarPK))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioLapBukubesar [trioLapBukubesarPK=" + trioLapBukubesarPK
				+ "]";
	}
	

}

