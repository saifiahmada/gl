package com.trio.gl.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @author Gusti Arya 3:53:27 PM Sep 3, 2013
 */

@Embeddable
public class TrioKumpulPK implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Column(name="ID_PERKIRAAN", length=6,nullable=false)
	private String idPerkiraan;
	
	@Column(name="ID_SUB", length=5, nullable=false)
	private String idSub;
	
  
	public TrioKumpulPK() {
		// TODO Auto-generated constructor stub
	}
	
	public String getIdPerkiraan() {
		return idPerkiraan;
	}

	public void setIdPerkiraan(String idPerkiraan) {
		this.idPerkiraan = idPerkiraan;
	}

	public String getIdSub() {
		return idSub;
	}

	public void setIdSub(String idSub) {
		this.idSub = idSub;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((idPerkiraan == null) ? 0 : idPerkiraan.hashCode());
		result = prime * result + ((idSub == null) ? 0 : idSub.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioKumpulPK other = (TrioKumpulPK) obj;
		if (idPerkiraan == null) {
			if (other.idPerkiraan != null)
				return false;
		} else if (!idPerkiraan.equals(other.idPerkiraan))
			return false;
		if (idSub == null) {
			if (other.idSub != null)
				return false;
		} else if (!idSub.equals(other.idSub))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioKumpulPK [idPerkiraan=" + idPerkiraan + ", idSub=" + idSub
				+ "]";
	}

}
