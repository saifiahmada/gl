package com.trio.gl.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.trio.gl.usertrail.TrioEntityUserTrail;

/** @author Saifi Ahmada Mar 13, 2013 12:12:43 PM  **/

@Entity
@Table(name="TRIO_JURNAL")
public class TrioJurnal extends TrioEntityUserTrail implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private TrioJurnalPK trioJurnalPK;
	
	@Column(name="TGL_JURNAL")
	@Temporal(TemporalType.DATE)
	private Date tglJurnal;
	@Column(name="NAMA_PERKIRAAN", length=35)
	private String namaPerkiraan;
	@Column(name="KET", length=100)
	private String ket;
	@Column(name="KET2", length=15)
	private String ket2;
	@Column(name="DEBET", precision=18, scale=0)
	private BigDecimal debet;
	@Column(name="KREDIT", precision=18, scale=0)
	private BigDecimal kredit;
	@Column(name="BLC", length=1)
	private String blc;
	@Column(name="TIPE", length=1)
	private String tipe;
	@Column(name="LUNAS", length=1)
	private String lunas;
	@Column(name="TGL_LUNAS")
	@Temporal(TemporalType.DATE)
	private Date tglLunas;
	@Column(name="NO_LUNAS", length=11)
	private String noLunas;
	@Column(name="FLAG", length=1)
	private int flag;
	@Column(name="NO_REFF",length=30)
	private String noReff;
	@Column(name="NO_MESIN", length=13)
	private String noMesin;
	
	public TrioJurnal() {
		this.trioJurnalPK = new TrioJurnalPK();
	}
	
	
	public TrioJurnal(String idJurnal, String idPerkiraan, String idSub) {
		this.trioJurnalPK = new TrioJurnalPK(idJurnal, idPerkiraan, idSub);
	}
	
	public TrioJurnal(BigDecimal debet){
		this.trioJurnalPK = new TrioJurnalPK();
		this.debet = debet;
	}
	
	public TrioJurnal(String idJurnal,BigDecimal debet,BigDecimal kredit){
		this.trioJurnalPK = new TrioJurnalPK(idJurnal, "","");
		this.debet = debet;
		this.kredit = kredit;
	}

	public TrioJurnalPK getTrioJurnalPK() {
		return trioJurnalPK;
	}

	public void setTrioJurnalPK(TrioJurnalPK trioJurnalPK) {
		this.trioJurnalPK = trioJurnalPK;
	}

	public Date getTglJurnal() {
		return tglJurnal;
	}

	public void setTglJurnal(Date tglJurnal) {
		this.tglJurnal = tglJurnal;
	}
	
	public String getKet() {
		return ket;
	}

	public void setKet(String ket) {
		this.ket = ket;
	}

	public String getKet2() {
		return ket2;
	}

	public void setKet2(String ket2) {
		this.ket2 = ket2;
	}

	public BigDecimal getDebet() {
		return debet;
	}

	public void setDebet(BigDecimal debet) {
		this.debet = debet;
	}

	public BigDecimal getKredit() {
		return kredit;
	}

	public void setKredit(BigDecimal kredit) {
		this.kredit = kredit;
	}

	public String getBlc() {
		return blc;
	}

	public void setBlc(String blc) {
		this.blc = blc;
	}

	public String getTipe() {
		return tipe;
	}

	public void setTipe(String tipe) {
		this.tipe = tipe;
	}

	public String getLunas() {
		return lunas;
	}

	public void setLunas(String lunas) {
		this.lunas = lunas;
	}

	public Date getTglLunas() {
		return tglLunas;
	}

	public void setTglLunas(Date tglLunas) {
		this.tglLunas = tglLunas;
	}

	public String getNoLunas() {
		return noLunas;
	}

	public void setNoLunas(String noLunas) {
		this.noLunas = noLunas;
	}

	public int getFlag() {
		return flag;
	}

	public void setFlag(int flag) {
		this.flag = flag;
	}

	public String getNoReff() {
		return noReff;
	}

	public void setNoReff(String noReff) {
		this.noReff = noReff;
	}

	public String getNoMesin() {
		return noMesin;
	}

	public void setNoMesin(String noMesin) {
		this.noMesin = noMesin;
	}

	public String getNamaPerkiraan() {
		return namaPerkiraan;
	}

	public void setNamaPerkiraan(String namaPerkiraan) {
		this.namaPerkiraan = namaPerkiraan;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((trioJurnalPK == null) ? 0 : trioJurnalPK.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioJurnal other = (TrioJurnal) obj;
		if (trioJurnalPK == null) {
			if (other.trioJurnalPK != null)
				return false;
		} else if (!trioJurnalPK.equals(other.trioJurnalPK))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioJurnal [trioJurnalPK=" + trioJurnalPK + "]";
	}
	
}

