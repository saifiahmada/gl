package com.trio.gl.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.trio.gl.usertrail.TrioEntityUserTrail;

@Entity
@Table(name="TRIO_DESIGN_RUGI_LABA")
public class TrioDesignRugiLaba extends TrioEntityUserTrail implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="BARIS")
	private Long baris;
	
	@Column(name="ID_PERKIRAAN", length=6)
	private String idPerkiraan;
	
	@Column(name="KD_JD", length=3)
	private String kdJD;
	
	@Column(name="TGL_RL")
	@Temporal(TemporalType.DATE)
	private Date tglRl;
	
	@Column(name="NAMA_PERKIRAAN", length=35)
	private String namaPerkiraan;
	
	@Column(name="KET", length=1)
	private String ket;
	
	@Column(name="KET2", length=1)
	private String ket2;
	
	@Column(name="LEVEL1", precision=17, scale=2)
	private BigDecimal level1;
	
	@Column(name="LEVEL2", precision=17, scale=2)
	private BigDecimal level2;
	
	@Column(name="LEVEL3", precision=17, scale=2)
	private BigDecimal level3;
	
	@Column(name="LEVEL4", precision=17, scale=2)
	private BigDecimal level4;
	
	@Column(name="NILAI", precision=17, scale=2)
	private BigDecimal nilai;
	
	@Column(name="KD_GROUP", length=3)
	private String kdGroup;
	
	@Column(name="HDR_GROUP", precision=1, scale=0)
	private BigDecimal hdrGroup;
	
	@Column(name="SHOW_DTL", precision=1, scale=0)
	private BigDecimal showDtl;
	
	@Column(name="TMP1", precision=15, scale=0)
	private BigDecimal tmp1;
	
	@Column(name="TMP2", precision=15, scale=0)
	private BigDecimal tmp2;
	
	@Column(name="TMP3", precision=15, scale=0)
	private BigDecimal tmp3;
	
	@Column(name="TMP4", precision=15, scale=0)
	private BigDecimal tmp4;
	
	@Column(name="SHOWS", length=1)
	private String shows;
	
	@Column(name="KET_GROUP", length=100)
	private String ketGroup;
	
	@Column(name="TMP_GROUP", length=100)
	private String tmpGroup;
	
	public TrioDesignRugiLaba() {
		// TODO Auto-generated constructor stub
	}
	
	public Long getBaris() {
		return baris;
	}

	public void setBaris(Long baris) {
		this.baris = baris;
	}

	public String getIdPerkiraan() {
		return idPerkiraan;
	}

	public void setIdPerkiraan(String idPerkiraan) {
		this.idPerkiraan = idPerkiraan;
	}

	public String getKdJD() {
		return kdJD;
	}

	public void setKdJD(String kdJD) {
		this.kdJD = kdJD;
	}

	public Date getTglRl() {
		return tglRl;
	}

	public void setTglRl(Date tglRl) {
		this.tglRl = tglRl;
	}

	public String getNamaPerkiraan() {
		return namaPerkiraan;
	}

	public void setNamaPerkiraan(String namaPerkiraan) {
		this.namaPerkiraan = namaPerkiraan;
	}

	public String getKet() {
		return ket;
	}

	public void setKet(String ket) {
		this.ket = ket;
	}

	public String getKet2() {
		return ket2;
	}

	public void setKet2(String ket2) {
		this.ket2 = ket2;
	}

	public BigDecimal getLevel1() {
		return level1;
	}

	public void setLevel1(BigDecimal level1) {
		this.level1 = level1;
	}

	public BigDecimal getLevel2() {
		return level2;
	}

	public void setLevel2(BigDecimal level2) {
		this.level2 = level2;
	}

	public BigDecimal getLevel3() {
		return level3;
	}

	public void setLevel3(BigDecimal level3) {
		this.level3 = level3;
	}

	public BigDecimal getLevel4() {
		return level4;
	}

	public void setLevel4(BigDecimal level4) {
		this.level4 = level4;
	}

	public BigDecimal getNilai() {
		return nilai;
	}

	public void setNilai(BigDecimal nilai) {
		this.nilai = nilai;
	}

	public String getKdGroup() {
		return kdGroup;
	}

	public void setKdGroup(String kdGroup) {
		this.kdGroup = kdGroup;
	}

	public BigDecimal getHdrGroup() {
		return hdrGroup;
	}

	public void setHdrGroup(BigDecimal hdrGroup) {
		this.hdrGroup = hdrGroup;
	}

	public BigDecimal getShowDtl() {
		return showDtl;
	}

	public void setShowDtl(BigDecimal showDtl) {
		this.showDtl = showDtl;
	}

	public BigDecimal getTmp1() {
		return tmp1;
	}

	public void setTmp1(BigDecimal tmp1) {
		this.tmp1 = tmp1;
	}

	public BigDecimal getTmp2() {
		return tmp2;
	}

	public void setTmp2(BigDecimal tmp2) {
		this.tmp2 = tmp2;
	}

	public BigDecimal getTmp3() {
		return tmp3;
	}

	public void setTmp3(BigDecimal tmp3) {
		this.tmp3 = tmp3;
	}

	public BigDecimal getTmp4() {
		return tmp4;
	}

	public void setTmp4(BigDecimal tmp4) {
		this.tmp4 = tmp4;
	}

	public String getShows() {
		return shows;
	}

	public void setShows(String shows) {
		this.shows = shows;
	}

	public String getKetGroup() {
		return ketGroup;
	}

	public void setKetGroup(String ketGroup) {
		this.ketGroup = ketGroup;
	}

	public String getTmpGroup() {
		return tmpGroup;
	}

	public void setTmpGroup(String tmpGroup) {
		this.tmpGroup = tmpGroup;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((baris == null) ? 0 : baris.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioDesignRugiLaba other = (TrioDesignRugiLaba) obj;
		if (baris == null) {
			if (other.baris != null)
				return false;
		} else if (!baris.equals(other.baris))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioDesignRugiLaba [baris=" + baris + "]";
	}
	
	
}
