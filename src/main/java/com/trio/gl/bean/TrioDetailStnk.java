package com.trio.gl.bean;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.trio.gl.usertrail.TrioEntityUserTrail;

/**
 * @author Gusti Arya 2:41:29 PM Jul 15, 2013
 */

@Entity
@Table(name="TRIO_DETAIL_STNK")
public class TrioDetailStnk extends TrioEntityUserTrail{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private TrioDetailStnkPK trioDetailStnkPK;
	
	@Column(name="NO_RANGKA", length=17)
	private String noRangka;
	
	@Column(name="KD_ITEM", length=6)
	private String kdItem;
	
	@Column(name="NAMA", length=50)
	private String nama;
	
	@Column(name="BIAYA", length=10, precision=10, scale=0)
	private BigDecimal biaya;
	
	@Column(name="CEK", length=1, precision=1, scale=0)
	private BigDecimal cek;
	
	@Column(name="STATUS", length=1)
	private String status;
	
	@Column(name="STNK", length=10, precision=10, scale=0)
	private BigDecimal stnk;
	
	@Column(name="LAIN", length=10, precision=10, scale=0)
	private BigDecimal lain;
	
	@Column(name="K_STNK", length=10, precision=10, scale=0)
	private BigDecimal kStnk;
	
	@Column(name="K_LAIN", length=10, precision=10, scale=0)
	private BigDecimal kLain;

	@Column(name="NO_BUKTI_S", length=16)
	private String noBuktiS;
	
	@Column(name="NO_BUKTI_L", length=16)
	private String noBuktiL;
	
	@Column(name="BBNKB", length=10, precision=10, scale=0)
	private BigDecimal bbnkb;
	
	@Column(name="PKB",length=10, precision=10, scale=0)
	private BigDecimal pkb;
	
	@Column(name="SWDKLLJ", length=10, precision=10, scale=0)
	private BigDecimal swdkllj;
	
	@Column(name="TOT_LAIN", length=10, precision=10, scale=0)
	private  BigDecimal totLain;
	
	@Column(name="BBNKB_R", length=10, precision=10, scale=0)
	private BigDecimal bbnkbR;
	
	@Column(name="PKB_R", length=10, precision=10, scale=0)
	private BigDecimal pkbR;
	
	@Column(name="SWDKLLJ_R", length=10, precision=10, scale=0)
	private BigDecimal swdklljR;
	
	@Column(name="SAMA",length=10, precision=10, scale=0)
	private BigDecimal sama;
	
	@Column(name="TGL_PINJ")
	@Temporal(TemporalType.DATE)
	private Date tglPinj;
	
	@Column(name="TGL_KMBL")
	@Temporal(TemporalType.DATE)
	private Date tglKmbl;
	
	@Column(name="TGL_KRDT")
	@Temporal(TemporalType.DATE)
	private Date tglKrdt;

	@Column(name="NO_FAK")
	private String noFak;
	
	@Column(name="TGL_FAK")
	@Temporal(TemporalType.DATE)
	private Date tglFak;
	
	@ManyToOne
	@JoinColumn(name="NO_MHN", referencedColumnName="NO_MHN", updatable=false, insertable=false)
	private TrioHeaderStnk trioHeaderStnk;

	public TrioDetailStnk() {
		// TODO Auto-generated constructor stub
	}
	

	public TrioDetailStnk(Date tglPinj ,String noMhn,Date tglFak,String noFak, String noMesin,String nama
			, BigDecimal bbnkb, BigDecimal pkb, BigDecimal swdkllj, BigDecimal lain, BigDecimal bbnkbR
			, BigDecimal pkbR, BigDecimal swdklljR, Date tglKrdt) {
		super();
		this.trioDetailStnkPK = new TrioDetailStnkPK(noMesin, noMhn);//Constuctornya kelas PK
		this.tglPinj = tglPinj;
		this.tglFak = tglFak;
		this.noFak = noFak;
		this.nama = nama;
		this.bbnkb = bbnkb;
		this.pkb = pkb;
		this.swdkllj = swdkllj;
		this.lain = lain;
		this.bbnkbR =bbnkbR;
		this.pkbR = pkbR;
		this.swdklljR = swdklljR;
		this.tglKrdt = tglKrdt;
	}


	public TrioDetailStnkPK getTrioDetailStnkPK() {
		return trioDetailStnkPK;
	}

	public void setTrioDetailStnkPK(TrioDetailStnkPK trioDetailStnkPK) {
		this.trioDetailStnkPK = trioDetailStnkPK;
	}

	public String getNoRangka() {
		return noRangka;
	}

	public void setNoRangka(String noRangka) {
		this.noRangka = noRangka;
	}

	public String getKdItem() {
		return kdItem;
	}

	public void setKdItem(String kdItem) {
		this.kdItem = kdItem;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public BigDecimal getBiaya() {
		return biaya;
	}

	public void setBiaya(BigDecimal biaya) {
		this.biaya = biaya;
	}

	public BigDecimal getCek() {
		return cek;
	}

	public void setCek(BigDecimal cek) {
		this.cek = cek;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public BigDecimal getStnk() {
		return stnk;
	}

	public void setStnk(BigDecimal stnk) {
		this.stnk = stnk;
	}

	public BigDecimal getLain() {
		return lain;
	}

	public void setLain(BigDecimal lain) {
		this.lain = lain;
	}

	public BigDecimal getkStnk() {
		return kStnk;
	}

	public void setkStnk(BigDecimal kStnk) {
		this.kStnk = kStnk;
	}

	public BigDecimal getkLain() {
		return kLain;
	}

	public void setkLain(BigDecimal kLain) {
		this.kLain = kLain;
	}

	public String getNoBuktiS() {
		return noBuktiS;
	}

	public void setNoBuktiS(String noBuktiS) {
		this.noBuktiS = noBuktiS;
	}

	public String getNoBuktiL() {
		return noBuktiL;
	}

	public void setNoBuktiL(String noBuktiL) {
		this.noBuktiL = noBuktiL;
	}

	public BigDecimal getBbnkb() {
		return bbnkb;
	}

	public void setBbnkb(BigDecimal bbnkb) {
		this.bbnkb = bbnkb;
	}

	public BigDecimal getPkb() {
		return pkb;
	}

	public void setPkb(BigDecimal pkb) {
		this.pkb = pkb;
	}

	public BigDecimal getSwdkllj() {
		return swdkllj;
	}

	public void setSwdkllj(BigDecimal swdkllj) {
		this.swdkllj = swdkllj;
	}

	public BigDecimal getTotLain() {
		return totLain;
	}

	public void setTotLain(BigDecimal totLain) {
		this.totLain = totLain;
	}

	public BigDecimal getBbnkbR() {
		return bbnkbR;
	}

	public void setBbnkbR(BigDecimal bbnkbR) {
		this.bbnkbR = bbnkbR;
	}

	public BigDecimal getPkbR() {
		return pkbR;
	}

	public void setPkbR(BigDecimal pkbR) {
		this.pkbR = pkbR;
	}

	public BigDecimal getSwdklljR() {
		return swdklljR;
	}

	public void setSwdklljR(BigDecimal swdklljR) {
		this.swdklljR = swdklljR;
	}

	public BigDecimal getSama() {
		return sama;
	}
	
	public void setSama(BigDecimal sama) {
		this.sama = sama;
	}

	public Date getTglPinj() {
		return tglPinj;
	}

	public void setTglPinj(Date tglPinj) {
		this.tglPinj = tglPinj;
	}

	public Date getTglKmbl() {
		return tglKmbl;
	}

	public void setTglKmbl(Date tglKmbl) {
		this.tglKmbl = tglKmbl;
	}

	public Date getTglKrdt() {
		return tglKrdt;
	}

	public void setTglKrdt(Date tglKrdt) {
		this.tglKrdt = tglKrdt;
	}

	public String getNoFak() {
		return noFak;
	}

	public void setNoFak(String noFak) {
		this.noFak = noFak;
	}

	public Date getTglFak() {
		return tglFak;
	}

	public void setTglFak(Date tglFak) {
		this.tglFak = tglFak;
	}

	public TrioHeaderStnk getTrioHeaderStnk() {
		return trioHeaderStnk;
	}

	public void setTrioHeaderStnk(TrioHeaderStnk trioHeaderStnk) {
		this.trioHeaderStnk = trioHeaderStnk;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((trioDetailStnkPK == null) ? 0 : trioDetailStnkPK.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioDetailStnk other = (TrioDetailStnk) obj;
		if (trioDetailStnkPK == null) {
			if (other.trioDetailStnkPK != null)
				return false;
		} else if (!trioDetailStnkPK.equals(other.trioDetailStnkPK))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioDetailStnk [trioDetailStnkPK=" + trioDetailStnkPK + "]";
	}
}
