package com.trio.gl.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/** @author Saifi Ahmada May 23, 2013 3:40:10 PM  **/
@Embeddable
public class TrioLapNeracaAktivaPK implements Serializable {

	private static final long serialVersionUID = 1L;
	@Column(name="ID_PERKIRAAN", length=6)
	private String idPerkiraan;
	@Column(name="KD_JD")
	private String kdJd;
	@Column(name="PERIODE", length=6)
	private String periode;
	
	public TrioLapNeracaAktivaPK() {
	}
	
	public TrioLapNeracaAktivaPK(String idPerkiraan, String kdJd, String periode) {
		this.idPerkiraan = idPerkiraan;
		this.kdJd = kdJd;
		this.periode = periode;
	}

	public String getIdPerkiraan() {
		return idPerkiraan;
	}

	public void setIdPerkiraan(String idPerkiraan) {
		this.idPerkiraan = idPerkiraan;
	}

	public String getKdJd() {
		return kdJd;
	}

	public void setKdJd(String kdJd) {
		this.kdJd = kdJd;
	}

	public String getPeriode() {
		return periode;
	}

	public void setPeriode(String periode) {
		this.periode = periode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((idPerkiraan == null) ? 0 : idPerkiraan.hashCode());
		result = prime * result + ((kdJd == null) ? 0 : kdJd.hashCode());
		result = prime * result + ((periode == null) ? 0 : periode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioLapNeracaAktivaPK other = (TrioLapNeracaAktivaPK) obj;
		if (idPerkiraan == null) {
			if (other.idPerkiraan != null)
				return false;
		} else if (!idPerkiraan.equals(other.idPerkiraan))
			return false;
		if (kdJd == null) {
			if (other.kdJd != null)
				return false;
		} else if (!kdJd.equals(other.kdJd))
			return false;
		if (periode == null) {
			if (other.periode != null)
				return false;
		} else if (!periode.equals(other.periode))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioLapNeracaAktivaPK [idPerkiraan=" + idPerkiraan + ", kdJd="
				+ kdJd + ", periode=" + periode + "]";
	}
	
}

