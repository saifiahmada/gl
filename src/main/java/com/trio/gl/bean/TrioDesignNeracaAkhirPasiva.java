package com.trio.gl.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.trio.gl.usertrail.TrioEntityUserTrail;

/** @author Saifi Ahmada May 23, 2013 10:51:41 AM  **/

@Entity
@Table(name="TRIO_DESIGN_NERACA_AKHIR_PASIVA")
public class TrioDesignNeracaAkhirPasiva extends TrioEntityUserTrail implements Serializable {

	private static final long serialVersionUID = 1L;
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="BARIS")
	private int baris;
	
	@Column(name="ID_PERKIRAAN", length=6)
	private String idPerkiraan;
	@Column(name="KD_JD", length=3)
	private String kdJd;
	@Column(name="TGL_NERACA")
	@Temporal(TemporalType.DATE)
	private Date tglNeraca;
	@Column(name="NAMA_PERKIRAAN", length=35)
	private String namaPerkiraan;
	@Column(name="KET", length = 1)
	private String ket;
	@Column(name="KET1", length=1)
	private String ket1;
	@Column(name="LEVEL1", precision=14, scale=2)
	private BigDecimal level1;
	@Column(name="LEVEL2", precision=14, scale=2)
	private BigDecimal level2;
	@Column(name="NILAI", precision=14, scale=2)
	private BigDecimal nilai;
	
	public TrioDesignNeracaAkhirPasiva() {
	
	}
	

	public TrioDesignNeracaAkhirPasiva(String idPerkiraan, String kdJd,
			Date tglNeraca, String namaPerkiraan, String ket, String ket1,
			BigDecimal level1, BigDecimal level2, BigDecimal nilai) {
		super();
		this.idPerkiraan = idPerkiraan;
		this.kdJd = kdJd;
		this.tglNeraca = tglNeraca;
		this.namaPerkiraan = namaPerkiraan;
		this.ket = ket;
		this.ket1 = ket1;
		this.level1 = level1;
		this.level2 = level2;
		this.nilai = nilai;
	}



	public int getBaris() {
		return baris;
	}

	public void setBaris(int baris) {
		this.baris = baris;
	}

	public String getIdPerkiraan() {
		return idPerkiraan;
	}

	public void setIdPerkiraan(String idPerkiraan) {
		this.idPerkiraan = idPerkiraan;
	}

	public String getKdJd() {
		return kdJd;
	}

	public void setKdJd(String kdJd) {
		this.kdJd = kdJd;
	}

	public Date getTglNeraca() {
		return tglNeraca;
	}

	public void setTglNeraca(Date tglNeraca) {
		this.tglNeraca = tglNeraca;
	}

	public String getNamaPerkiraan() {
		return namaPerkiraan;
	}

	public void setNamaPerkiraan(String namaPerkiraan) {
		this.namaPerkiraan = namaPerkiraan;
	}

	public String getKet() {
		return ket;
	}

	public void setKet(String ket) {
		this.ket = ket;
	}

	public String getKet1() {
		return ket1;
	}

	public void setKet1(String ket1) {
		this.ket1 = ket1;
	}

	public BigDecimal getLevel1() {
		return level1;
	}

	public void setLevel1(BigDecimal level1) {
		this.level1 = level1;
	}

	public BigDecimal getLevel2() {
		return level2;
	}

	public void setLevel2(BigDecimal level2) {
		this.level2 = level2;
	}

	public BigDecimal getNilai() {
		return nilai;
	}

	public void setNilai(BigDecimal nilai) {
		this.nilai = nilai;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + baris;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioDesignNeracaAkhirPasiva other = (TrioDesignNeracaAkhirPasiva) obj;
		if (baris != other.baris)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioDesignNeracaAkhirPasiva [baris=" + baris + "]";
	}
	
}



