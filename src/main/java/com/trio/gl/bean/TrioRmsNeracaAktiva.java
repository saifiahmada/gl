package com.trio.gl.bean;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.trio.gl.usertrail.TrioEntityUserTrail;

/**
 * @author Gusti Arya 9:56:13 AM Oct 31, 2013
 */

@Entity
@Table(name="TRIO_RMS_NERACA_AKTIVA")
public class TrioRmsNeracaAktiva extends TrioEntityUserTrail{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="ID_PERKIRAAN", length=6)
	private String idPerkiraan;
	
	@Column(name="KD_HSL", length=3)
	private String kdHsl;
	
	@Column(name="NAMA_PERKIRAAN", length=35)
	private String namaPerkiraan;
	
	@Column(name="JENIS", length=1)
	private String jenis;
	
	@Column(name="NILAI", precision=12, scale=0)
	private BigDecimal nilai;
	
	public TrioRmsNeracaAktiva() {
		// TODO Auto-generated constructor stub
	}

	public TrioRmsNeracaAktiva(String kdHsl, BigDecimal nilai){
		this.kdHsl = kdHsl;
		this.nilai = nilai;
	}
	
	public TrioRmsNeracaAktiva(String idPerkiraan, String kdHsl,
			String namaPerkiraan, String jenis, BigDecimal nilai) {
		super();
		this.idPerkiraan = idPerkiraan;
		this.kdHsl = kdHsl;
		this.namaPerkiraan = namaPerkiraan;
		this.jenis = jenis;
		this.nilai = nilai;
	}



	public String getIdPerkiraan() {
		return idPerkiraan;
	}

	public void setIdPerkiraan(String idPerkiraan) {
		this.idPerkiraan = idPerkiraan;
	}

	public String getKdHsl() {
		return kdHsl;
	}

	public void setKdHsl(String kdHsl) {
		this.kdHsl = kdHsl;
	}

	public String getNamaPerkiraan() {
		return namaPerkiraan;
	}

	public void setNamaPerkiraan(String namaPerkiraan) {
		this.namaPerkiraan = namaPerkiraan;
	}

	public String getJenis() {
		return jenis;
	}

	public void setJenis(String jenis) {
		this.jenis = jenis;
	}

	public BigDecimal getNilai() {
		return nilai;
	}

	public void setNilai(BigDecimal nilai) {
		this.nilai = nilai;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((idPerkiraan == null) ? 0 : idPerkiraan.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioRmsNeracaAktiva other = (TrioRmsNeracaAktiva) obj;
		if (idPerkiraan == null) {
			if (other.idPerkiraan != null)
				return false;
		} else if (!idPerkiraan.equals(other.idPerkiraan))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioRmsNeracaAktiva [idPerkiraan=" + idPerkiraan + "]";
	}
	

}
