package com.trio.gl.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/** @author Saifi Ahmada Jul 15, 2013 3:53:08 PM  **/
@Entity
public class TrioTarikStok implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private TrioTarikStokPK trioTarikStokPK;
	
	@Column(name="KD_DLR")
	private String kdDlr;
	
	@Column(name="QTY")
	private int qty;
	
	@Column(name="TGL_FAKTUR")
	@Temporal(TemporalType.DATE)
	private Date tglFaktur;
	
	@Column(name="HARGA", length=15)
	private Integer harga;
	
	
	public TrioTarikStok() {
	}
	
	public TrioTarikStok(String noFaktur, String partNum, String status) {
		this.trioTarikStokPK = new TrioTarikStokPK(noFaktur, partNum, status);
	}

	public Integer getHarga() {
		return harga;
	}

	public void setHarga(Integer harga) {
		this.harga = harga;
	}

	public TrioTarikStokPK getTrioTarikStokPK() {
		return trioTarikStokPK;
	}

	public void setTrioTarikStokPK(TrioTarikStokPK trioTarikStokPK) {
		this.trioTarikStokPK = trioTarikStokPK;
	}

	public String getKdDlr() {
		return kdDlr;
	}

	public void setKdDlr(String kdDlr) {
		this.kdDlr = kdDlr;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	public Date getTglFaktur() {
		return tglFaktur;
	}

	public void setTglFaktur(Date tglFaktur) {
		this.tglFaktur = tglFaktur;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((trioTarikStokPK == null) ? 0 : trioTarikStokPK.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioTarikStok other = (TrioTarikStok) obj;
		if (trioTarikStokPK == null) {
			if (other.trioTarikStokPK != null)
				return false;
		} else if (!trioTarikStokPK.equals(other.trioTarikStokPK))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioTarikStok [trioTarikStokPK=" + trioTarikStokPK + "]";
	}
	
}

