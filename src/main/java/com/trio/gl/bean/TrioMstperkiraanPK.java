package com.trio.gl.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/** @author Saifi Ahmada Feb 25, 2013 12:17:46 PM  **/

@Embeddable
public class TrioMstperkiraanPK implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Column(name="ID_PERKIRAAN", length=6, nullable=false)
	private String idPerkiraan;
	
	public TrioMstperkiraanPK() {
	}
	
	public TrioMstperkiraanPK(String idPerkiraan) {
		this.idPerkiraan = idPerkiraan;
	}

	public String getIdPerkiraan() {
		return idPerkiraan;
	}

	public void setIdPerkiraan(String idPerkiraan) {
		this.idPerkiraan = idPerkiraan;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((idPerkiraan == null) ? 0 : idPerkiraan.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioMstperkiraanPK other = (TrioMstperkiraanPK) obj;
		if (idPerkiraan == null) {
			if (other.idPerkiraan != null)
				return false;
		} else if (!idPerkiraan.equals(other.idPerkiraan))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioMstperkiraanPK [idPerkiraan=" + idPerkiraan + "]";
	}
	
}

