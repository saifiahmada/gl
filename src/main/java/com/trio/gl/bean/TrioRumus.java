package com.trio.gl.bean;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.trio.gl.usertrail.TrioEntityUserTrail;

@Entity
@Table(name="TRIO_RUMUS")
public class TrioRumus extends TrioEntityUserTrail implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_PERKIRAAN", length=6)
	private String idPerkiraan;
	
	@Column(name="KD_HSL", length=3)
	private String kdHsl;
	
	@Column(name="NAMA_PERKIRAAN", length=35)
	private String namaPerkiraan;
	
	@Column(name="JENIS")
	private char jenis;
	
	@Column(name="NILAI", precision=14, scale=2)
	private BigDecimal nilai;
	
	public TrioRumus() {
		// TODO Auto-generated constructor stub
	}
	
	public TrioRumus(String kdHsl, BigDecimal nilai) {
		this.kdHsl = kdHsl;
		this.nilai = nilai;
	}

	public TrioRumus(String idPerkiraan, String kdHsl, String namaPerkiraan,
			char jenis, BigDecimal nilai) {
		this.idPerkiraan = idPerkiraan;
		this.kdHsl = kdHsl;
		this.namaPerkiraan = namaPerkiraan;
		this.jenis = jenis;
		this.nilai = nilai;
	}



	public String getIdPerkiraan() {
		return idPerkiraan;
	}

	public void setIdPerkiraan(String idPerkiraan) {
		this.idPerkiraan = idPerkiraan;
	}

	public String getKdHsl() {
		return kdHsl;
	}

	public void setKdHsl(String kdHsl) {
		this.kdHsl = kdHsl;
	}

	public String getNamaPerkiraan() {
		return namaPerkiraan;
	}

	public void setNamaPerkiraan(String namaPerkiraan) {
		this.namaPerkiraan = namaPerkiraan;
	}

	public char getJenis() {
		return jenis;
	}

	public void setJenis(char jenis) {
		this.jenis = jenis;
	}

	public BigDecimal getNilai() {
		return nilai;
	}

	public void setNilai(BigDecimal nilai) {
		this.nilai = nilai;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((idPerkiraan == null) ? 0 : idPerkiraan.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		
		TrioRumus other = (TrioRumus) obj;
		if (idPerkiraan == null) {
			if (other.idPerkiraan != null)
				return false;
		} else if (!idPerkiraan.equals(other.idPerkiraan))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioRumus [idPerkiraan=" + idPerkiraan + "]";
	}
	

}
