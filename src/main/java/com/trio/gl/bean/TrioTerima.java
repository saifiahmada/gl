package com.trio.gl.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.trio.gl.usertrail.TrioEntityUserTrail;

/** @author Saifi Ahmada Aug 28, 2013 4:18:01 PM  **/
@Entity
@Table(name="TRIO_TERIMA")
public class TrioTerima extends TrioEntityUserTrail implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private TrioTerimaPK trioTerimaPK;
	
	@Column(name="NO_BUKTI", length=16)
	private String noBukti;//NO_BUKTI	varchar(16)
	@Column(name="NO_KWT", length=6)
	private String noKWT;//NO_KWT	varchar(6)
	@Column(name="TGL_TRM" )
	@Temporal(TemporalType.DATE)
	private Date tglTrm;//TGL_TRM	date
	@Column(name="JNS", length=3)
	private String jns; //JNS	varchar(3)
	@Column(name="KET", length=1)
	private String ket;//KET	varchar(1)
	@Column(name="NM_KET", length=13)
	private String nmKet;//NM_KET	varchar(13)
	@Column(name="NM_TRAN", length=200)
	private String nmTran;//NM_TRAN	varchar(75)
	@Column(name="JML_TRM", precision=12, scale=0)
	private BigDecimal jmlTrm; //JML_TRM	decimal(12,0)
	@Column(name="DEBET", precision=12, scale=0)
	private BigDecimal debet;//DEBET	decimal(12,0)
	@Column(name="KREDIT", precision=12, scale=0)
	private BigDecimal kredit; //KREDIT	decimal(12,0)
	@Column(name="TUTUP", length=10)
	private String tutup;//TUTUP	varchar(10)
	@Column(name="USER", length=10)
	private String user; //USER	varchar(10)
	@Column(name="TGL_USER")
	@Temporal(TemporalType.DATE)
	private Date tglUser; //TGL_USER	datetime
	@Column(name="BPKB")
	private int BPKB;//BPKB	decimal(1,0)
	@Column(name="KET_ANG")
	private int ketAng; //KET_ANG	decimal(2,0)
	@Column(name="REG", length=1)
	private String reg; //REG	varchar(1)
	@Column(name="NO_MESIN", length=13)
	private String noMesin; //NO_MESIN	varchar(13)
	@Column(name="NO_RANGKA", length=15)
	private String noRangka;//NO_RANGKA	varchar(15)
	@Column(name="KD_PRS", length=3)
	private String kdPrs; //KD_PRS	varchar(3)
	@Column(name="NO_REC", length=30)
	private String noRec; //NO_REC	varchar(30)
	@Column(name="NO_CEK_BG", length=15)
	private String noCekBg; //NO_CEK_BG	varchar(15)
	@Column(name="NAMA_BANK", length=30)
	private String namaBank; //NAMA_BANK	varchar(30)
	@Column(name="TGL_JT")
	@Temporal(TemporalType.DATE)
	private Date tglJT; //TGL_JT	date
	@Column(name="NO_PERK", length=5)
	private String noPerk; //NO_PERK	varchar(5)
	@Column(name="NO_PERK2", length=5)
	private String noPerk2; //NO_PERK2	varchar(5)
	@Column(name="NO_SUB", length=3)
	private String noSub; //NO_SUB	varchar(3)
	@Column(name="KET2", length=16)
	private String ket2; //KET2	varchar(16)
	
	@Column(name="S_AHM", precision=10, scale=0)
	private BigDecimal sAHM; //S_AHM	decimal(10,0)
	@Column(name="S_MD", precision=10, scale=0)
	private BigDecimal sMD; //S_MD	decimal(10,0)
	@Column(name="S_SD", precision=10, scale=0)
	private BigDecimal sSD; //S_SD	decimal(10,0)
	@Column(name="S_FINANCE", precision=10, scale=0)
	private BigDecimal sFinance; //S_FINANCE	decimal(10,0)
	@Column(name="S_BUNGA", precision=10, scale=0)
	private BigDecimal sBunga; //S_BUNGA	decimal(10,0)
	@Column(name="KD_ITEM", length=6)
	private String kdItem; //KD_ITEM	varchar(6)
	@Column(name="HARGA", precision=10, scale=0)
	private BigDecimal harga; //HARGA	decimal(10,0)
	@Column(name="B_STNK", precision=10, scale=0)
	private BigDecimal bSTNK; //B_STNK	decimal(10,0)
	@Column(name="P_STNK", precision=10, scale=0)
	private BigDecimal pSTNK; //P_STNK	decimal(10,0)
	@Column(name="P_BPKB", precision=10, scale=0)
	private BigDecimal pBPKB; //P_BPKB	decimal(10,0)
	@Column(name="NM_CUST", length=50)
	private String nmCust; //NM_CUST	varchar(50)
	@Column(name="BATAL_JUAL", length =1)
	private String batalJual; //BATAL_JUAL	varchar(1)
	@Column(name="KD_BANK", length=3)
	private String kdBank; //KD_BANK	varchar(3)
	@Column(name="KD_BANK2", length=3)
	private String kdBank2; //KD_BANK2	varchar(3)
	@Column(name="NO_BP", length=20)
	private String noBP; //NO_BP	varchar(20)
	@Column(name="ALAMAT", length=100)
	private String alamat; //ALAMAT	varchar(100)
	@Column(name="JK_WAKTU")
	private int jkWaktu; //JK_WAKTU	decimal(10,0)
	@Column(name="ANGSURAN", precision=10, scale=0)
	private BigDecimal angsuran; //ANGSURAN	decimal(10,0)
	@Column(name="NO_SJ", length=17)
	private String noSJ; //NO_SJ	varchar(17)
	@Column(name="KD_POS", length=10)
	private String kdPOS; //KD_POS	varchar(10)
	@Column(name="ITR", length=1)
	private String itr; //ITR	varchar(1)
	@Column(name="NO_LKH2", length=16)
	private String noLKH2; //NO_LKH2	varchar(16)
	@Column(name="TGL_TRM2")
	@Temporal(TemporalType.DATE)
	private Date tglTrm2; //TGL_TRM2	date
	@Column(name="NO_KIRIM", length=16)
	private String noKirim; //NO_KIRIM	varchar(16)
	@Column(name="TGL_KIRIM")
	@Temporal(TemporalType.DATE)
	private Date tglKirim; //TGL_KIRIM	date
	@Column(name="TGL_B")
	@Temporal(TemporalType.DATE)
	private Date tglB; // TGL B
	@Column(name="HADIAH")
	private String hadiah; // HADIAH
	@Column(name="TAMB_MD", precision=10, scale=0)
	private BigDecimal tambMD; // TAMB_MD
	@Column(name="TAMB_SD", precision=10, scale=0)
	private BigDecimal tambSD; // TAMB_SD
	@Column(name="TAMB_FIN", precision=10, scale=0)
	private BigDecimal tambFIN; // TAMB_FIN
	@Column(name="STATUS", length=1)
	private String status; //STATUS	varchar(1)
	
	public TrioTerima() {
	
	}
	
	public TrioTerima(String noLKH, String noFak) {
		this.trioTerimaPK = new TrioTerimaPK(noLKH, noFak); 
	}
	
	public TrioTerima(TrioTerimaPK trioTerimaPK){
		this.trioTerimaPK = trioTerimaPK;
	}

	public TrioTerimaPK getTrioTerimaPK() {
		return trioTerimaPK;
	}

	public void setTrioTerimaPK(TrioTerimaPK trioTerimaPK) {
		this.trioTerimaPK = trioTerimaPK;
	}

	public String getNoBukti() {
		return noBukti;
	}

	public void setNoBukti(String noBukti) {
		this.noBukti = noBukti;
	}

	public String getNoKWT() {
		return noKWT;
	}

	public void setNoKWT(String noKWT) {
		this.noKWT = noKWT;
	}

	public Date getTglTrm() {
		return tglTrm;
	}

	public void setTglTrm(Date tglTrm) {
		this.tglTrm = tglTrm;
	}

	public String getJns() {
		return jns;
	}

	public void setJns(String jns) {
		this.jns = jns;
	}

	public String getKet() {
		return ket;
	}

	public void setKet(String ket) {
		this.ket = ket;
	}

	public String getNmKet() {
		return nmKet;
	}

	public void setNmKet(String nmKet) {
		this.nmKet = nmKet;
	}

	public String getNmTran() {
		return nmTran;
	}

	public void setNmTran(String nmTran) {
		this.nmTran = nmTran;
	}

	public BigDecimal getJmlTrm() {
		return jmlTrm;
	}

	public void setJmlTrm(BigDecimal jmlTrm) {
		this.jmlTrm = jmlTrm;
	}

	public BigDecimal getDebet() {
		return debet;
	}

	public void setDebet(BigDecimal debet) {
		this.debet = debet;
	}

	public BigDecimal getKredit() {
		return kredit;
	}

	public void setKredit(BigDecimal kredit) {
		this.kredit = kredit;
	}

	public String getTutup() {
		return tutup;
	}

	public void setTutup(String tutup) {
		this.tutup = tutup;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public Date getTglUser() {
		return tglUser;
	}

	public void setTglUser(Date tglUser) {
		this.tglUser = tglUser;
	}

	public int getBPKB() {
		return BPKB;
	}

	public void setBPKB(int bPKB) {
		BPKB = bPKB;
	}

	public int getKetAng() {
		return ketAng;
	}

	public void setKetAng(int ketAng) {
		this.ketAng = ketAng;
	}

	public String getReg() {
		return reg;
	}

	public void setReg(String reg) {
		this.reg = reg;
	}

	public String getNoMesin() {
		return noMesin;
	}

	public void setNoMesin(String noMesin) {
		this.noMesin = noMesin;
	}

	public String getNoRangka() {
		return noRangka;
	}

	public void setNoRangka(String noRangka) {
		this.noRangka = noRangka;
	}

	public String getKdPrs() {
		return kdPrs;
	}

	public void setKdPrs(String kdPrs) {
		this.kdPrs = kdPrs;
	}

	public String getNoRec() {
		return noRec;
	}

	public void setNoRec(String noRec) {
		this.noRec = noRec;
	}

	public String getNoCekBg() {
		return noCekBg;
	}

	public void setNoCekBg(String noCekBg) {
		this.noCekBg = noCekBg;
	}

	public String getNamaBank() {
		return namaBank;
	}

	public void setNamaBank(String namaBank) {
		this.namaBank = namaBank;
	}

	public Date getTglJT() {
		return tglJT;
	}

	public void setTglJT(Date tglJT) {
		this.tglJT = tglJT;
	}

	public String getNoPerk() {
		return noPerk;
	}

	public void setNoPerk(String noPerk) {
		this.noPerk = noPerk;
	}

	public String getNoPerk2() {
		return noPerk2;
	}

	public void setNoPerk2(String noPerk2) {
		this.noPerk2 = noPerk2;
	}

	public String getNoSub() {
		return noSub;
	}

	public void setNoSub(String noSub) {
		this.noSub = noSub;
	}

	public String getKet2() {
		return ket2;
	}

	public void setKet2(String ket2) {
		this.ket2 = ket2;
	}

	public BigDecimal getsAHM() {
		return sAHM;
	}

	public void setsAHM(BigDecimal sAHM) {
		this.sAHM = sAHM;
	}

	public BigDecimal getsMD() {
		return sMD;
	}

	public void setsMD(BigDecimal sMD) {
		this.sMD = sMD;
	}

	public BigDecimal getsSD() {
		return sSD;
	}

	public void setsSD(BigDecimal sSD) {
		this.sSD = sSD;
	}

	public BigDecimal getsFinance() {
		return sFinance;
	}

	public void setsFinance(BigDecimal sFinance) {
		this.sFinance = sFinance;
	}

	public BigDecimal getsBunga() {
		return sBunga;
	}

	public void setsBunga(BigDecimal sBunga) {
		this.sBunga = sBunga;
	}

	public String getKdItem() {
		return kdItem;
	}

	public void setKdItem(String kdItem) {
		this.kdItem = kdItem;
	}

	public BigDecimal getHarga() {
		return harga;
	}

	public void setHarga(BigDecimal harga) {
		this.harga = harga;
	}

	public BigDecimal getbSTNK() {
		return bSTNK;
	}

	public void setbSTNK(BigDecimal bSTNK) {
		this.bSTNK = bSTNK;
	}

	public BigDecimal getpSTNK() {
		return pSTNK;
	}

	public void setpSTNK(BigDecimal pSTNK) {
		this.pSTNK = pSTNK;
	}

	public BigDecimal getpBPKB() {
		return pBPKB;
	}

	public void setpBPKB(BigDecimal pBPKB) {
		this.pBPKB = pBPKB;
	}

	public String getNmCust() {
		return nmCust;
	}

	public void setNmCust(String nmCust) {
		this.nmCust = nmCust;
	}

	public String getBatalJual() {
		return batalJual;
	}

	public void setBatalJual(String batalJual) {
		this.batalJual = batalJual;
	}

	public String getKdBank() {
		return kdBank;
	}

	public void setKdBank(String kdBank) {
		this.kdBank = kdBank;
	}

	public String getKdBank2() {
		return kdBank2;
	}

	public void setKdBank2(String kdBank2) {
		this.kdBank2 = kdBank2;
	}

	public String getNoBP() {
		return noBP;
	}

	public void setNoBP(String noBP) {
		this.noBP = noBP;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public int getJkWaktu() {
		return jkWaktu;
	}

	public void setJkWaktu(int jkWaktu) {
		this.jkWaktu = jkWaktu;
	}

	public BigDecimal getAngsuran() {
		return angsuran;
	}

	public void setAngsuran(BigDecimal angsuran) {
		this.angsuran = angsuran;
	}

	public String getNoSJ() {
		return noSJ;
	}

	public void setNoSJ(String noSJ) {
		this.noSJ = noSJ;
	}

	public String getKdPOS() {
		return kdPOS;
	}

	public void setKdPOS(String kdPOS) {
		this.kdPOS = kdPOS;
	}

	public String getItr() {
		return itr;
	}

	public void setItr(String itr) {
		this.itr = itr;
	}

	public String getNoLKH2() {
		return noLKH2;
	}

	public void setNoLKH2(String noLKH2) {
		this.noLKH2 = noLKH2;
	}

	public Date getTglTrm2() {
		return tglTrm2;
	}

	public void setTglTrm2(Date tglTrm2) {
		this.tglTrm2 = tglTrm2;
	}

	public String getNoKirim() {
		return noKirim;
	}

	public void setNoKirim(String noKirim) {
		this.noKirim = noKirim;
	}

	public Date getTglKirim() {
		return tglKirim;
	}

	public void setTglKirim(Date tglKirim) {
		this.tglKirim = tglKirim;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getTglB() {
		return tglB;
	}

	public void setTglB(Date tglB) {
		this.tglB = tglB;
	}

	public String getHadiah() {
		return hadiah;
	}

	public void setHadiah(String hadiah) {
		this.hadiah = hadiah;
	}

	public BigDecimal getTambMD() {
		return tambMD;
	}

	public void setTambMD(BigDecimal tambMD) {
		this.tambMD = tambMD;
	}

	public BigDecimal getTambSD() {
		return tambSD;
	}

	public void setTambSD(BigDecimal tambSD) {
		this.tambSD = tambSD;
	}

	public BigDecimal getTambFIN() {
		return tambFIN;
	}

	public void setTambFIN(BigDecimal tambFIN) {
		this.tambFIN = tambFIN;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((trioTerimaPK == null) ? 0 : trioTerimaPK.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioTerima other = (TrioTerima) obj;
		if (trioTerimaPK == null) {
			if (other.trioTerimaPK != null)
				return false;
		} else if (!trioTerimaPK.equals(other.trioTerimaPK))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioTerima [trioTerimaPK=" + trioTerimaPK + "]";
	}

}

