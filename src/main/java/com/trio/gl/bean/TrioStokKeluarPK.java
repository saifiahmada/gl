package com.trio.gl.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/** @author Saifi Ahmada Jul 19, 2013 4:38:29 PM  **/
@Embeddable
public class TrioStokKeluarPK implements Serializable {

	private static final long serialVersionUID = 1L;
	@Column(name="NO_NOTA", length=20)
	private String noNota;
	@Column(name="PART_NUM", length=20)
	private String partNum;
	
	public TrioStokKeluarPK() {
	}
	
	public TrioStokKeluarPK(String noNota, String partNum) {
		this.noNota = noNota;
		this.partNum = partNum;
	}

	public String getNoNota() {
		return noNota;
	}

	public void setNoNota(String noNota) {
		this.noNota = noNota;
	}

	public String getPartNum() {
		return partNum;
	}

	public void setPartNum(String partNum) {
		this.partNum = partNum;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((noNota == null) ? 0 : noNota.hashCode());
		result = prime * result + ((partNum == null) ? 0 : partNum.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioStokKeluarPK other = (TrioStokKeluarPK) obj;
		if (noNota == null) {
			if (other.noNota != null)
				return false;
		} else if (!noNota.equals(other.noNota))
			return false;
		if (partNum == null) {
			if (other.partNum != null)
				return false;
		} else if (!partNum.equals(other.partNum))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioStokKeluarPK [noNota=" + noNota + ", partNum=" + partNum
				+ "]";
	}

}

