package com.trio.gl.bean;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.trio.gl.usertrail.TrioEntityUserTrail;

/**
 * @author Gusti Arya 9:09:03 AM Sep 30, 2013
 */

@Entity
@Table(name="TRIO_LABA_DITAHAN")
public class TrioLabaDitahan extends TrioEntityUserTrail implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private TrioLabaDitahanPK trioLabaDitahanPK;
	
	@Column(name="KET")
	private String ket;
	
	@Column(name="TOTAL")
	private BigDecimal total;

	public TrioLabaDitahan() {
		this.trioLabaDitahanPK = new TrioLabaDitahanPK();
	}
	
	public TrioLabaDitahanPK getTrioLabaDitahanPK() {
		return trioLabaDitahanPK;
	}

	public void setTrioLabaDitahanPK(TrioLabaDitahanPK trioLabaDitahanPK) {
		this.trioLabaDitahanPK = trioLabaDitahanPK;
	}

	public String getKet() {
		return ket;
	}

	public void setKet(String ket) {
		this.ket = ket;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((trioLabaDitahanPK == null) ? 0 : trioLabaDitahanPK
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioLabaDitahan other = (TrioLabaDitahan) obj;
		if (trioLabaDitahanPK == null) {
			if (other.trioLabaDitahanPK != null)
				return false;
		} else if (!trioLabaDitahanPK.equals(other.trioLabaDitahanPK))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioLabaDitahan [trioLabaDitahanPK=" + trioLabaDitahanPK + "]";
	}
	
	
}
