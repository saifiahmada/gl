package com.trio.gl.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/** @author Saifi Ahmada May 23, 2013 11:21:53 AM  **/
@Embeddable
public class TrioLapNeracaPK implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Column(name="ID_PERKIRAAN", length=6)
	private String idPerkiraan;
	
	@Column(name="ID_SUB", length=5)
	private String idSub;
	
	@Column(name="PERIODE", length=6)
	private String periode;
	
	public TrioLapNeracaPK() {
	}
	
	public TrioLapNeracaPK(String idPerkiraan, String idSub, String periode) {
		this.idPerkiraan = idPerkiraan;
		this.idSub = idSub;
		this.periode = periode;
	}

	public String getIdPerkiraan() {
		return idPerkiraan;
	}

	public void setIdPerkiraan(String idPerkiraan) {
		this.idPerkiraan = idPerkiraan;
	}

	public String getIdSub() {
		return idSub;
	}

	public void setIdSub(String idSub) {
		this.idSub = idSub;
	}

	public String getPeriode() {
		return periode;
	}

	public void setPeriode(String periode) {
		this.periode = periode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((idPerkiraan == null) ? 0 : idPerkiraan.hashCode());
		result = prime * result + ((idSub == null) ? 0 : idSub.hashCode());
		result = prime * result + ((periode == null) ? 0 : periode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioLapNeracaPK other = (TrioLapNeracaPK) obj;
		if (idPerkiraan == null) {
			if (other.idPerkiraan != null)
				return false;
		} else if (!idPerkiraan.equals(other.idPerkiraan))
			return false;
		if (idSub == null) {
			if (other.idSub != null)
				return false;
		} else if (!idSub.equals(other.idSub))
			return false;
		if (periode == null) {
			if (other.periode != null)
				return false;
		} else if (!periode.equals(other.periode))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioLapNeracaPK [idPerkiraan=" + idPerkiraan + ", idSub="
				+ idSub + ", periode=" + periode + "]";
	}

}

