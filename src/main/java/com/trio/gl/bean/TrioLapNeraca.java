package com.trio.gl.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.trio.gl.usertrail.TrioEntityUserTrail;

/** @author Saifi Ahmada May 23, 2013 10:55:00 AM  **/
@Entity
@Table(name="TRIO_LAP_NERACA")
public class TrioLapNeraca extends TrioEntityUserTrail implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private TrioLapNeracaPK trioLapNeracaPK;
	
	@Column(name="TGL_NERACA")
	@Temporal(TemporalType.DATE)
	private Date tglNeraca;
	
	@Column(name="SL_AWAL_D", precision=17, scale=2)
	private BigDecimal slAwalD;
		
	@Column(name="SL_AWAL_K", precision=17, scale=2)
	private BigDecimal slAwalK;
	
	@Column(name="MTS_D", precision=17, scale=2)
	private BigDecimal mtsD;
	
	@Column(name="MTS_K", precision=17, scale=2)
	private BigDecimal mtsK;
	
	@Column(name="SL_AKH_D", precision=17, scale=2)
	private BigDecimal slAkhD;
	
	@Column(name="SL_AKH_K", precision=17, scale=2)
	private BigDecimal slAkhK;
	
	@Column(name="R_L", precision=17, scale=2)
	private BigDecimal rL;
	
	public TrioLapNeraca() {
		
	}
	
	public TrioLapNeraca(String idPerkiraan, BigDecimal slAwalD,
			BigDecimal slAwalK, BigDecimal mtsD, BigDecimal mtsK,
			BigDecimal slAkhD, BigDecimal slAkhK, BigDecimal rL) {
		super();
		this.trioLapNeracaPK = new TrioLapNeracaPK(idPerkiraan, "", "");
		this.slAwalD = slAwalD;
		this.slAwalK = slAwalK;
		this.mtsD = mtsD;
		this.mtsK = mtsK;
		this.slAkhD = slAkhD;
		this.slAkhK = slAkhK;
		this.rL = rL;
	}
	
	public TrioLapNeraca(BigDecimal slAwalD,
			BigDecimal slAwalK, BigDecimal mtsD, BigDecimal mtsK,
			BigDecimal slAkhD, BigDecimal slAkhK, BigDecimal rL) {
		super();
		this.slAwalD = slAwalD;
		this.slAwalK = slAwalK;
		this.mtsD = mtsD;
		this.mtsK = mtsK;
		this.slAkhD = slAkhD;
		this.slAkhK = slAkhK;
		this.rL = rL;
	}

	public TrioLapNeraca(String idPerkiraan, String idSub, String periode) {
		this.trioLapNeracaPK = new TrioLapNeracaPK(idPerkiraan, idSub, periode);
	}

	public TrioLapNeracaPK getTrioLapNeracaPK() {
		return trioLapNeracaPK;
	}

	public void setTrioLapNeracaPK(TrioLapNeracaPK trioLapNeracaPK) {
		this.trioLapNeracaPK = trioLapNeracaPK;
	}

	public Date getTglNeraca() {
		return tglNeraca;
	}

	public void setTglNeraca(Date tglNeraca) {
		this.tglNeraca = tglNeraca;
	}

	public BigDecimal getSlAwalD() {
		return slAwalD;
	}

	public void setSlAwalD(BigDecimal slAwalD) {
		this.slAwalD = slAwalD;
	}

	public BigDecimal getSlAwalK() {
		return slAwalK;
	}

	public void setSlAwalK(BigDecimal slAwalK) {
		this.slAwalK = slAwalK;
	}

	public BigDecimal getMtsD() {
		return mtsD;
	}

	public void setMtsD(BigDecimal mtsD) {
		this.mtsD = mtsD;
	}

	public BigDecimal getMtsK() {
		return mtsK;
	}

	public void setMtsK(BigDecimal mtsK) {
		this.mtsK = mtsK;
	}

	public BigDecimal getSlAkhD() {
		return slAkhD;
	}

	public void setSlAkhD(BigDecimal slAkhD) {
		this.slAkhD = slAkhD;
	}

	public BigDecimal getSlAkhK() {
		return slAkhK;
	}

	public void setSlAkhK(BigDecimal slAkhK) {
		this.slAkhK = slAkhK;
	}

	public BigDecimal getrL() {
		return rL;
	}

	public void setrL(BigDecimal rL) {
		this.rL = rL;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((trioLapNeracaPK == null) ? 0 : trioLapNeracaPK.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioLapNeraca other = (TrioLapNeraca) obj;
		if (trioLapNeracaPK == null) {
			if (other.trioLapNeracaPK != null)
				return false;
		} else if (!trioLapNeracaPK.equals(other.trioLapNeracaPK))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioLapNeraca [trioLapNeracaPK=" + trioLapNeracaPK + "]";
	}
	

}

