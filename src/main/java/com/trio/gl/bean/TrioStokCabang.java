package com.trio.gl.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.trio.gl.usertrail.TrioEntityUserTrail;

/** @author Saifi Ahmada Jul 19, 2013 4:19:56 PM  **/
@Entity
@Table(name="TRIO_STOK_CABANG")
public class TrioStokCabang extends TrioEntityUserTrail implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Column(name="PART_NUM", length=20)
	private String partNum;
	@Column(name="QTY")
	private int qty;
	
	public TrioStokCabang() {
	}
	
	public TrioStokCabang(String partNum) {
		this.partNum = partNum;
	}

	public String getPartNum() {
		return partNum;
	}

	public void setPartNum(String partNum) {
		this.partNum = partNum;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((partNum == null) ? 0 : partNum.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioStokCabang other = (TrioStokCabang) obj;
		if (partNum == null) {
			if (other.partNum != null)
				return false;
		} else if (!partNum.equals(other.partNum))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioStokCabang [partNum=" + partNum + "]";
	}
	
}

