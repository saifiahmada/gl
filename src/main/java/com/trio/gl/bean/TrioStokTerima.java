package com.trio.gl.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.trio.gl.usertrail.TrioEntityUserTrail;

/** @author Saifi Ahmada Jul 18, 2013 3:37:29 PM  **/
@Entity
@Table(name="TRIO_STOK_TERIMA")
public class TrioStokTerima extends TrioEntityUserTrail implements Serializable {

	private static final long serialVersionUID = 1L;
	@EmbeddedId
	private TrioStokTerimaPK trioStokTerimaPK;
	@Column(name="KD_DLR")
	private String kdDlr;
	@Column(name="QTY")
	private int qty;
	@Column(name="TGL_TERIMA")
	@Temporal(TemporalType.DATE)
	private Date tglTerima;
	@Column(name="NO_REF")
	private String noRef;
	@Column(name="STATUS")
	private String status;
	
	public TrioStokTerima() {
	
	}
	
	public TrioStokTerima(String noTerima, String partNum) {
		this.trioStokTerimaPK = new TrioStokTerimaPK(noTerima, partNum);
	}

	public TrioStokTerimaPK getTrioStokTerimaPK() {
		return trioStokTerimaPK;
	}

	public void setTrioStokTerimaPK(TrioStokTerimaPK trioStokTerimaPK) {
		this.trioStokTerimaPK = trioStokTerimaPK;
	}

	public String getKdDlr() {
		return kdDlr;
	}

	public void setKdDlr(String kdDlr) {
		this.kdDlr = kdDlr;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	public Date getTglTerima() {
		return tglTerima;
	}

	public void setTglTerima(Date tglTerima) {
		this.tglTerima = tglTerima;
	}

	public String getNoRef() {
		return noRef;
	}

	public void setNoRef(String noRef) {
		this.noRef = noRef;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((trioStokTerimaPK == null) ? 0 : trioStokTerimaPK.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioStokTerima other = (TrioStokTerima) obj;
		if (trioStokTerimaPK == null) {
			if (other.trioStokTerimaPK != null)
				return false;
		} else if (!trioStokTerimaPK.equals(other.trioStokTerimaPK))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioStokTerima [trioStokTerimaPK=" + trioStokTerimaPK + "]";
	}

}

