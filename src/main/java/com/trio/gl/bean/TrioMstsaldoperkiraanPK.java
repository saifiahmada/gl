package com.trio.gl.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/** @author Saifi Ahmada Apr 16, 2013 9:15:32 PM  **/
@Embeddable
public class TrioMstsaldoperkiraanPK implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Column(name="ID_PERKIRAAN", length=6, nullable=false)
	private String idPerkiraan;
	@Column(name="TAHUN", length=4, nullable=false)
	private String tahun;
	
	public TrioMstsaldoperkiraanPK() {
	
	}
	
	public TrioMstsaldoperkiraanPK(String idPerkiraan, String tahun) {
		this.idPerkiraan = idPerkiraan;
		this.tahun = tahun;
	}

	public String getIdPerkiraan() {
		return idPerkiraan;
	}

	public void setIdPerkiraan(String idPerkiraan) {
		this.idPerkiraan = idPerkiraan;
	}

	public String getTahun() {
		return tahun;
	}

	public void setTahun(String tahun) {
		this.tahun = tahun;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((idPerkiraan == null) ? 0 : idPerkiraan.hashCode());
		result = prime * result + ((tahun == null) ? 0 : tahun.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioMstsaldoperkiraanPK other = (TrioMstsaldoperkiraanPK) obj;
		if (idPerkiraan == null) {
			if (other.idPerkiraan != null)
				return false;
		} else if (!idPerkiraan.equals(other.idPerkiraan))
			return false;
		if (tahun == null) {
			if (other.tahun != null)
				return false;
		} else if (!tahun.equals(other.tahun))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioMstsaldoperkiraanPK [idPerkiraan=" + idPerkiraan
				+ ", tahun=" + tahun + "]";
	}
	
}

