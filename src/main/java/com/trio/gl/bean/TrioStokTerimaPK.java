package com.trio.gl.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/** @author Saifi Ahmada Jul 18, 2013 3:39:16 PM  **/
@Embeddable
public class TrioStokTerimaPK implements Serializable {

	private static final long serialVersionUID = 1L;
	@Column(name="NO_TERIMA", length=20)
	private String noTerima;
	@Column(name="PART_NUM", length=20)
	private String partNum;
	
	public TrioStokTerimaPK() {
	}
	
	public TrioStokTerimaPK(String noTerima, String partNum) {
		this.noTerima = noTerima;
		this.partNum = partNum;
	}

	public String getNoTerima() {
		return noTerima;
	}

	public void setNoTerima(String noTerima) {
		this.noTerima = noTerima;
	}

	public String getPartNum() {
		return partNum;
	}

	public void setPartNum(String partNum) {
		this.partNum = partNum;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((noTerima == null) ? 0 : noTerima.hashCode());
		result = prime * result + ((partNum == null) ? 0 : partNum.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioStokTerimaPK other = (TrioStokTerimaPK) obj;
		if (noTerima == null) {
			if (other.noTerima != null)
				return false;
		} else if (!noTerima.equals(other.noTerima))
			return false;
		if (partNum == null) {
			if (other.partNum != null)
				return false;
		} else if (!partNum.equals(other.partNum))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioStokTerimaPK [noTerima=" + noTerima + ", partNum="
				+ partNum + "]";
	}

}

