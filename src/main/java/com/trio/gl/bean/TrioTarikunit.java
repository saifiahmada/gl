package com.trio.gl.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/** @author Saifi Ahmada Mar 23, 2013 9:53:36 PM  **/

@Entity
public class TrioTarikunit implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Column(name="KD_DLR")
	private String kdDlr;
	
	@Column(name="TGL_SJ")
	@Temporal(TemporalType.DATE)
	private Date tglSj;
	
	@Column(name="NO_SJ")
	private String noSj;
	
	@Column(name="TGL_FAK")
	@Temporal(TemporalType.DATE)
	private Date tglFak;
	
	@Column(name="NO_FAKTUR")
	private String noFaktur;
	
	@Id
	@Column(name="NO_MESIN")
	private String noMesin;
	
	@Column(name="NO_RANGKA")
	private String noRangka;
	
	@Id
	@Column(name="KD_ITEM")
	private String kdItem;
	
	@Column(name="KET_1")
	private String ket1;
	
	@Column(name="HARGA_STD")
	private BigDecimal hargaStd;
	
	@Column(name="DISCOUNT")
	private BigDecimal discount;
	
	public TrioTarikunit() {
	
	}
	
	public TrioTarikunit(String noMesin, String kdItem) {
		this.noMesin = noMesin;
		this.kdItem = kdItem;
	}

	public String getKdDlr() {
		return kdDlr;
	}

	public void setKdDlr(String kdDlr) {
		this.kdDlr = kdDlr;
	}

	public Date getTglSj() {
		return tglSj;
	}

	public void setTglSj(Date tglSj) {
		this.tglSj = tglSj;
	}

	public String getNoSj() {
		return noSj;
	}

	public void setNoSj(String noSj) {
		this.noSj = noSj;
	}

	public Date getTglFak() {
		return tglFak;
	}

	public void setTglFak(Date tglFak) {
		this.tglFak = tglFak;
	}

	

	public String getNoMesin() {
		return noMesin;
	}

	public String getNoFaktur() {
		return noFaktur;
	}

	public void setNoFaktur(String noFaktur) {
		this.noFaktur = noFaktur;
	}

	public void setNoMesin(String noMesin) {
		this.noMesin = noMesin;
	}

	public String getNoRangka() {
		return noRangka;
	}

	public void setNoRangka(String noRangka) {
		this.noRangka = noRangka;
	}

	public String getKdItem() {
		return kdItem;
	}

	public void setKdItem(String kdItem) {
		this.kdItem = kdItem;
	}

	public String getKet1() {
		return ket1;
	}

	public void setKet1(String ket1) {
		this.ket1 = ket1;
	}

	public BigDecimal getHargaStd() {
		return hargaStd;
	}

	public void setHargaStd(BigDecimal hargaStd) {
		this.hargaStd = hargaStd;
	}

	public BigDecimal getDiscount() {
		return discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((kdItem == null) ? 0 : kdItem.hashCode());
		result = prime * result + ((noMesin == null) ? 0 : noMesin.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioTarikunit other = (TrioTarikunit) obj;
		if (kdItem == null) {
			if (other.kdItem != null)
				return false;
		} else if (!kdItem.equals(other.kdItem))
			return false;
		if (noMesin == null) {
			if (other.noMesin != null)
				return false;
		} else if (!noMesin.equals(other.noMesin))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioTarikunit [noMesin=" + noMesin + ", kdItem=" + kdItem + "]";
	}

}

