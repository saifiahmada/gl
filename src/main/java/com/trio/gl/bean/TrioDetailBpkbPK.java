package com.trio.gl.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @author Gusti Arya 11:53:18 AM Jul 17, 2013
 */

@Embeddable
public class TrioDetailBpkbPK implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name="NO_MHN", length=10)
	private String noMhn;
	
	@Column(name="NO_MESIN", length=13)
	private String noMesin;
	
	public TrioDetailBpkbPK() {
		// TODO Auto-generated constructor stub
	}
	
	public TrioDetailBpkbPK(String noMhn, String noMesin) {
		super();
		this.noMhn = noMhn;
		this.noMesin = noMesin;
	}

	public String getNoMhn() {
		return noMhn;
	}

	public void setNoMhn(String noMhn) {
		this.noMhn = noMhn;
	}

	public String getNoMesin() {
		return noMesin;
	}

	public void setNoMesin(String noMesin) {
		this.noMesin = noMesin;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((noMesin == null) ? 0 : noMesin.hashCode());
		result = prime * result + ((noMhn == null) ? 0 : noMhn.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioDetailBpkbPK other = (TrioDetailBpkbPK) obj;
		if (noMesin == null) {
			if (other.noMesin != null)
				return false;
		} else if (!noMesin.equals(other.noMesin))
			return false;
		if (noMhn == null) {
			if (other.noMhn != null)
				return false;
		} else if (!noMhn.equals(other.noMhn))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioDetailBpkbPK [noMhn=" + noMhn + ", noMesin=" + noMesin
				+ "]";
	}
	
}
