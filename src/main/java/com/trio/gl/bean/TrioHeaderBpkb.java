package com.trio.gl.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.trio.gl.usertrail.TrioEntityUserTrail;

/**
 * @author Gusti Arya 10:36:43 AM Jul 17, 2013
 */

@Entity
@Table(name="TRIO_HEADER_BPKB")
public class TrioHeaderBpkb extends TrioEntityUserTrail implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="NO_MHN", unique=true, length=10)
	private String noMhn;
	
	@Column(name="TGL")
	@Temporal(TemporalType.DATE)
	private Date tgl;
	
	@Column(name="JUMLAH", length=10, precision=10, scale=0)
	private BigDecimal jumlah;
	
	@Column(name="QTY", length=10, precision=10, scale=0)
	private BigDecimal qty;
	
	@Column(name="PENGURUS", length=20)
	private String pengurus;
	
	@Column(name="FLAG", length=1)
	private String flag;
	
	@Column(name="JNS_KLR", length=5)
	private String jnsKlr;
	
	@Column(name="BPKB", length=10, precision=10, scale=0)
	private BigDecimal BPKB;
	
	@Column(name="LAIN", length=10, precision=10, scale=0)
	private BigDecimal lain;
	
	@Column(name="K_BPKB", length=10, precision=10, scale=0)
	private BigDecimal kBpkb;
	
	@Column(name="K_LAIN",length=10, precision=10, scale=0)
	private BigDecimal kLain;
	
	@Column(name="STCK", length=10, precision=10, scale=0)
	private BigDecimal stck;
	
	@Column(name="K_STCK", length=10, precision=10, scale=0)
	private BigDecimal kStck;
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy = "trioHeaderBpkb")
	private Set<TrioDetailBpkb> trioDetailBpkbs;
	
	
	public TrioHeaderBpkb() {
		// TODO Auto-generated constructor stub
	}


	public String getNoMhn() {
		return noMhn;
	}


	public void setNoMhn(String noMhn) {
		this.noMhn = noMhn;
	}


	public Date getTgl() {
		return tgl;
	}


	public void setTgl(Date tgl) {
		this.tgl = tgl;
	}


	public BigDecimal getJumlah() {
		return jumlah;
	}


	public void setJumlah(BigDecimal jumlah) {
		this.jumlah = jumlah;
	}


	public BigDecimal getQty() {
		return qty;
	}


	public void setQty(BigDecimal qty) {
		this.qty = qty;
	}


	public String getPengurus() {
		return pengurus;
	}


	public void setPengurus(String pengurus) {
		this.pengurus = pengurus;
	}


	public String getFlag() {
		return flag;
	}


	public void setFlag(String flag) {
		this.flag = flag;
	}


	public String getJnsKlr() {
		return jnsKlr;
	}


	public void setJnsKlr(String jnsKlr) {
		this.jnsKlr = jnsKlr;
	}


	public BigDecimal getBPKB() {
		return BPKB;
	}


	public void setBPKB(BigDecimal bPKB) {
		BPKB = bPKB;
	}


	public BigDecimal getLain() {
		return lain;
	}


	public void setLain(BigDecimal lain) {
		this.lain = lain;
	}


	public BigDecimal getkBpkb() {
		return kBpkb;
	}


	public void setkBpkb(BigDecimal kBpkb) {
		this.kBpkb = kBpkb;
	}


	public BigDecimal getkLain() {
		return kLain;
	}


	public void setkLain(BigDecimal kLain) {
		this.kLain = kLain;
	}


	public BigDecimal getStck() {
		return stck;
	}


	public void setStck(BigDecimal stck) {
		this.stck = stck;
	}


	public BigDecimal getkStck() {
		return kStck;
	}

	public Set<TrioDetailBpkb> getTrioDetailBpkbs() {
		return trioDetailBpkbs;
	}


	public void setTrioDetailBpkbs(Set<TrioDetailBpkb> trioDetailBpkbs) {
		this.trioDetailBpkbs = trioDetailBpkbs;
	}


	public void setkStck(BigDecimal kStck) {
		this.kStck = kStck;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((noMhn == null) ? 0 : noMhn.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioHeaderBpkb other = (TrioHeaderBpkb) obj;
		if (noMhn == null) {
			if (other.noMhn != null)
				return false;
		} else if (!noMhn.equals(other.noMhn))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioHeaderBpkb [noMhn=" + noMhn + "]";
	}
	
	
}
