package com.trio.gl.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/** @author Saifi Ahmada Mar 23, 2013 3:28:01 PM  **/

@Embeddable
public class TrioStokunitPK implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Column(name="NO_MESIN", length=13)
	private String noMesin;
	@Column(name="KD_ITEM", length=6)
	private String kdItem;
	
	public TrioStokunitPK() {
	}
	
	public TrioStokunitPK(String noMesin, String kdItem) {
		this.noMesin = noMesin;
		this.kdItem = kdItem;
	}

	public String getNoMesin() {
		return noMesin;
	}

	public void setNoMesin(String noMesin) {
		this.noMesin = noMesin;
	}

	public String getKdItem() {
		return kdItem;
	}

	public void setKdItem(String kdItem) {
		this.kdItem = kdItem;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((kdItem == null) ? 0 : kdItem.hashCode());
		result = prime * result + ((noMesin == null) ? 0 : noMesin.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioStokunitPK other = (TrioStokunitPK) obj;
		if (kdItem == null) {
			if (other.kdItem != null)
				return false;
		} else if (!kdItem.equals(other.kdItem))
			return false;
		if (noMesin == null) {
			if (other.noMesin != null)
				return false;
		} else if (!noMesin.equals(other.noMesin))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioStokunitPK [noMesin=" + noMesin + ", kdItem=" + kdItem
				+ "]";
	}
	
}

