package com.trio.gl.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.trio.gl.usertrail.TrioEntityUserTrail;

/** @author Saifi Ahmada Apr 3, 2013 11:50:03 AM  **/

@Entity
@Table(name="TRIO_MSTKONFIGCABANG") 
public class TrioMstkonfigcabang extends TrioEntityUserTrail implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private TrioMstkonfigcabangPK trioMstkonfigcabangPK;
	
	@Column(name="NO_CABANG", length=1, nullable=false)
	private String noCabang;
	
	public TrioMstkonfigcabang() {
		this.trioMstkonfigcabangPK = new TrioMstkonfigcabangPK();
	}
	
	public TrioMstkonfigcabang(String kdDlr, String context ) {
		this.trioMstkonfigcabangPK = new TrioMstkonfigcabangPK(kdDlr, context);
	}

	public TrioMstkonfigcabangPK getTrioMstkonfigcabangPK() {
		return trioMstkonfigcabangPK;
	}

	public void setTrioMstkonfigcabangPK(TrioMstkonfigcabangPK trioMstkonfigcabangPK) {
		this.trioMstkonfigcabangPK = trioMstkonfigcabangPK;
	}

	public String getNoCabang() {
		return noCabang;
	}

	public void setNoCabang(String noCabang) {
		this.noCabang = noCabang;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((trioMstkonfigcabangPK == null) ? 0 : trioMstkonfigcabangPK
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioMstkonfigcabang other = (TrioMstkonfigcabang) obj;
		if (trioMstkonfigcabangPK == null) {
			if (other.trioMstkonfigcabangPK != null)
				return false;
		} else if (!trioMstkonfigcabangPK.equals(other.trioMstkonfigcabangPK))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioMstkonfigcabang [trioMstkonfigcabangPK="
				+ trioMstkonfigcabangPK + "]";
	}
	
}

