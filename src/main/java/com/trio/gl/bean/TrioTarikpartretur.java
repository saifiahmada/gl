package com.trio.gl.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/** @author Saifi Ahmada Apr 2, 2013 9:25:44 PM  **/

@Entity
public class TrioTarikpartretur implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Column(name="RETUR_NO")
	private String returNo;
	@Column(name="RETUR_DATE")
	@Temporal(TemporalType.DATE)
	private Date returDate;
	@Column(name="KET")
	private String ket;
	@Column(name="KD_DLR")
	private String kdDlr;
	@Column(name="DLR_NAMA")
	private String dlrNama;
	@Column(name="KD_TRANS")
	private String kdTrans;
	@Column(name="NM_TRAN")
	private String nmTran;
	@Column(name="CABANG")
	private String cabang;
	@Column(name="KEL_BRG")
	private String kelBrg;
	@Column(name="JML_TRM")
	private BigDecimal jmlTrm;
	
	public TrioTarikpartretur() {
	
	}
	
	public TrioTarikpartretur(String returNo) {
		this.returNo = returNo;
	}

	public String getReturNo() {
		return returNo;
	}

	public void setReturNo(String returNo) {
		this.returNo = returNo;
	}

	public Date getReturDate() {
		return returDate;
	}

	public void setReturDate(Date returDate) {
		this.returDate = returDate;
	}

	public String getKet() {
		return ket;
	}

	public void setKet(String ket) {
		this.ket = ket;
	}

	public String getKdDlr() {
		return kdDlr;
	}

	public void setKdDlr(String kdDlr) {
		this.kdDlr = kdDlr;
	}

	public String getDlrNama() {
		return dlrNama;
	}

	public void setDlrNama(String dlrNama) {
		this.dlrNama = dlrNama;
	}

	public String getKdTrans() {
		return kdTrans;
	}

	public void setKdTrans(String kdTrans) {
		this.kdTrans = kdTrans;
	}

	public String getNmTran() {
		return nmTran;
	}

	public void setNmTran(String nmTran) {
		this.nmTran = nmTran;
	}

	public String getCabang() {
		return cabang;
	}

	public void setCabang(String cabang) {
		this.cabang = cabang;
	}

	public String getKelBrg() {
		return kelBrg;
	}

	public void setKelBrg(String kelBrg) {
		this.kelBrg = kelBrg;
	}

	public BigDecimal getJmlTrm() {
		return jmlTrm;
	}

	public void setJmlTrm(BigDecimal jmlTrm) {
		this.jmlTrm = jmlTrm;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((returNo == null) ? 0 : returNo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioTarikpartretur other = (TrioTarikpartretur) obj;
		if (returNo == null) {
			if (other.returNo != null)
				return false;
		} else if (!returNo.equals(other.returNo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioTarikpartretur [returNo=" + returNo + "]";
	}
	
}

