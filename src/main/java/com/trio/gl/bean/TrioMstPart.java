package com.trio.gl.bean;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.trio.gl.usertrail.TrioEntityUserTrail;

/** @author Saifi Ahmada Jul 20, 2013 12:09:02 PM  **/
@Entity
@Table(name="TRIO_MST_PART")
public class TrioMstPart extends TrioEntityUserTrail implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Column(name="PART_NUM", length=20)
	private String partNum;
	@Column(name="HARGA", length=15) 
	private Integer harga;
	
	@Column(name="HARGA_POKOK_RATA", length=15) 
	private BigDecimal hargaPokokRata;
	
	@Column(name="QTY", length=5)
	private int qty;
	
	public TrioMstPart() {
	}
	
	public TrioMstPart(String partNum) {
		this.partNum = partNum;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	public BigDecimal getHargaPokokRata() {
		return hargaPokokRata;
	}

	public void setHargaPokokRata(BigDecimal hargaPokokRata) {
		this.hargaPokokRata = hargaPokokRata;
	}

	public String getPartNum() {
		return partNum;
	}

	public void setPartNum(String partNum) {
		this.partNum = partNum;
	}

	public Integer getHarga() {
		return harga;
	}

	public void setHarga(Integer harga) {
		this.harga = harga;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((partNum == null) ? 0 : partNum.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioMstPart other = (TrioMstPart) obj;
		if (partNum == null) {
			if (other.partNum != null)
				return false;
		} else if (!partNum.equals(other.partNum))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioMstPart [partNum=" + partNum + "]";
	}
	

}

