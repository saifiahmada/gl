package com.trio.gl.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/** @author Saifi Ahmada Apr 2, 2013 3:41:17 PM  **/

@Entity
public class TrioTarikpartsale implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="FAK_NO")
	private String fakNo;
	@Column(name="FAK_DATE")
	@Temporal(TemporalType.DATE)
	private Date fakDate;
	@Column(name="KET")
	private String ket;
	@Column(name="JML_TRM")
	private BigDecimal jmlTrm;
	@Column(name="KD_DLR")
	private String kdDlr;
	@Column(name="DLR_NAMA")
	private String dlrNama;
	@Column(name="NM_TRAN")
	private String nmTran;
	@Column(name="CABANG")
	private String cabang;
	@Column(name="KD_TRANS")
	private String kdTrans;
	@Column(name="KEL_BRG")
	private String kelBrg;
	
	public TrioTarikpartsale() {
	
	}
	
	public TrioTarikpartsale(String fakNo) {
		this.fakNo = fakNo;
	}

	public String getFakNo() {
		return fakNo;
	}

	public void setFakNo(String fakNo) {
		this.fakNo = fakNo;
	}

	public Date getFakDate() {
		return fakDate;
	}

	public void setFakDate(Date fakDate) {
		this.fakDate = fakDate;
	}

	public String getKet() {
		return ket;
	}

	public void setKet(String ket) {
		this.ket = ket;
	}

	public BigDecimal getJmlTrm() {
		return jmlTrm;
	}

	public void setJmlTrm(BigDecimal jmlTrm) {
		this.jmlTrm = jmlTrm;
	}

	public String getKdDlr() {
		return kdDlr;
	}

	public void setKdDlr(String kdDlr) {
		this.kdDlr = kdDlr;
	}

	public String getDlrNama() {
		return dlrNama;
	}

	public void setDlrNama(String dlrNama) {
		this.dlrNama = dlrNama;
	}

	public String getNmTran() {
		return nmTran;
	}

	public void setNmTrn(String nmTran) {
		this.nmTran = nmTran;
	}

	public String getCabang() {
		return cabang;
	}

	public void setCabang(String cabang) {
		this.cabang = cabang;
	}

	public String getKdTrans() {
		return kdTrans;
	}

	public void setKdTrans(String kdTrans) {
		this.kdTrans = kdTrans;
	}

	public String getKelBrg() {
		return kelBrg;
	}

	public void setKelBrg(String kelBrg) {
		this.kelBrg = kelBrg;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fakNo == null) ? 0 : fakNo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioTarikpartsale other = (TrioTarikpartsale) obj;
		if (fakNo == null) {
			if (other.fakNo != null)
				return false;
		} else if (!fakNo.equals(other.fakNo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioTarikpart [fakNo=" + fakNo + "]";
	}
	
}

