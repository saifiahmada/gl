package com.trio.gl.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/** @author Saifi Ahmada Jul 15, 2013 5:11:35 PM  **/
@Embeddable
public class TrioTarikStokPK implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Column(name="NO_FAKTUR")
	private String noFaktur;
	
	@Column(name="PART_NUM")
	private String partNum;
	
	@Column(name="STATUS")
	private String status;
	
	public TrioTarikStokPK() {
	}
	
	public TrioTarikStokPK(String noFaktur, String partNum, String status) {
		this.noFaktur = noFaktur;
		this.partNum = partNum;
		this.status = status;
	}

	public String getNoFaktur() {
		return noFaktur;
	}

	public void setNoFaktur(String noFaktur) {
		this.noFaktur = noFaktur;
	}

	public String getPartNum() {
		return partNum;
	}

	public void setPartNum(String partNum) {
		this.partNum = partNum;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((noFaktur == null) ? 0 : noFaktur.hashCode());
		result = prime * result + ((partNum == null) ? 0 : partNum.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioTarikStokPK other = (TrioTarikStokPK) obj;
		if (noFaktur == null) {
			if (other.noFaktur != null)
				return false;
		} else if (!noFaktur.equals(other.noFaktur))
			return false;
		if (partNum == null) {
			if (other.partNum != null)
				return false;
		} else if (!partNum.equals(other.partNum))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioTarikStokPK [noFaktur=" + noFaktur + ", partNum=" + partNum
				+ ", status=" + status + "]";
	}

}

