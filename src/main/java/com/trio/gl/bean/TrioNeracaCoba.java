package com.trio.gl.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.trio.gl.usertrail.TrioEntityUserTrail;

/** @author Saifi Ahmada Jun 3, 2013 4:30:29 PM  **/
@Entity
@Table(name="TRIO_NERACA_COBA")
public class TrioNeracaCoba extends TrioEntityUserTrail implements Serializable {

	private static final long serialVersionUID = 1L;
	@EmbeddedId
	private TrioNeracaCobaPK trioNeracaCobaPK;
	@Column(name="NAMA_PERKIRAAN", length=35)
	private String namaPerkiraan;
	@Column(name="TGL_NRC")
	@Temporal(TemporalType.DATE)
	private Date tglNrc;
	@Column(name="SL_AWAL_D", precision=18, scale=0)
	private BigDecimal slAwalD;
	@Column(name="SL_AWAL_K", precision=18, scale=0)
	private BigDecimal slAwalK;
	@Column(name="MTS_D", precision=18, scale=0)
	private BigDecimal mtsD;
	@Column(name="MTS_K", precision=18, scale=0)
	private BigDecimal mtsK;
	@Column(name="SL_AKHIR_D", precision=18, scale=0)
	private BigDecimal slAkhirD;
	@Column(name="SL_AKHIR_K", precision=18, scale=0)
	private BigDecimal slAkhirK;
	@Column(name="R_L", precision=18, scale=0)
	private BigDecimal rL;
	
	public TrioNeracaCoba() {
		this.trioNeracaCobaPK = new TrioNeracaCobaPK();
	}
	
	public TrioNeracaCoba(String idPerkiraan , String idSub) {
		this.trioNeracaCobaPK = new TrioNeracaCobaPK(idPerkiraan, idSub);
	}

	public TrioNeracaCobaPK getTrioNeracaCobaPK() {
		return trioNeracaCobaPK;
	}

	public void setTrioNeracaCobaPK(TrioNeracaCobaPK trioNeracaCobaPK) {
		this.trioNeracaCobaPK = trioNeracaCobaPK;
	}

	public String getNamaPerkiraan() {
		return namaPerkiraan;
	}

	public void setNamaPerkiraan(String namaPerkiraan) {
		this.namaPerkiraan = namaPerkiraan;
	}

	public Date getTglNrc() {
		return tglNrc;
	}

	public void setTglNrc(Date tglNrc) {
		this.tglNrc = tglNrc;
	}

	public BigDecimal getSlAwalD() {
		return slAwalD;
	}

	public void setSlAwalD(BigDecimal slAwalD) {
		this.slAwalD = slAwalD;
	}

	public BigDecimal getSlAwalK() {
		return slAwalK;
	}

	public void setSlAwalK(BigDecimal slAwalK) {
		this.slAwalK = slAwalK;
	}

	public BigDecimal getMtsD() {
		return mtsD;
	}

	public void setMtsD(BigDecimal mtsD) {
		this.mtsD = mtsD;
	}

	public BigDecimal getMtsK() {
		return mtsK;
	}

	public void setMtsK(BigDecimal mtsK) {
		this.mtsK = mtsK;
	}

	public BigDecimal getSlAkhirD() {
		return slAkhirD;
	}

	public void setSlAkhirD(BigDecimal slAkhirD) {
		this.slAkhirD = slAkhirD;
	}

	public BigDecimal getSlAkhirK() {
		return slAkhirK;
	}

	public void setSlAkhirK(BigDecimal slAkhirK) {
		this.slAkhirK = slAkhirK;
	}

	public BigDecimal getrL() {
		return rL;
	}

	public void setrL(BigDecimal rL) {
		this.rL = rL;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((trioNeracaCobaPK == null) ? 0 : trioNeracaCobaPK.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioNeracaCoba other = (TrioNeracaCoba) obj;
		if (trioNeracaCobaPK == null) {
			if (other.trioNeracaCobaPK != null)
				return false;
		} else if (!trioNeracaCobaPK.equals(other.trioNeracaCobaPK))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioNeracaCoba [trioNeracaCobaPK=" + trioNeracaCobaPK + "]";
	}
	
}

