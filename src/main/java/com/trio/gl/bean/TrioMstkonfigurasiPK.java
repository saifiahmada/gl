package com.trio.gl.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/** @author Saifi Ahmada Mar 22, 2013 9:06:55 AM  **/

@Embeddable
public class TrioMstkonfigurasiPK implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Column(name="ID_KONFIGURASI", nullable=false, length=3)
	private String idKonfigurasi;
	
	public TrioMstkonfigurasiPK() {
	
	}
	
	public TrioMstkonfigurasiPK(String idKonfigurasi) {
		this.idKonfigurasi = idKonfigurasi;
	}

	public String getIdKonfigurasi() {
		return idKonfigurasi;
	}

	public void setIdKonfigurasi(String idKonfigurasi) {
		this.idKonfigurasi = idKonfigurasi;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((idKonfigurasi == null) ? 0 : idKonfigurasi.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioMstkonfigurasiPK other = (TrioMstkonfigurasiPK) obj;
		if (idKonfigurasi == null) {
			if (other.idKonfigurasi != null)
				return false;
		} else if (!idKonfigurasi.equals(other.idKonfigurasi))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioMstkonfigurasiPK [idKonfigurasi=" + idKonfigurasi + "]";
	}
	
}

