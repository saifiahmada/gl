package com.trio.gl.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.trio.gl.usertrail.TrioEntityUserTrail;

/** @author Saifi Ahmada Mar 23, 2013 3:26:30 PM  **/

@Entity
@Table(name="TRIO_STOKUNIT")
public class TrioStokunit extends TrioEntityUserTrail implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@EmbeddedId
	private TrioStokunitPK trioStokunitPK;
	
	@Column(name="KD_DLR",length=10)
	private String kdDlr;
	@Column(name="KET",length=20)
	private String ket;
	@Column(name="TGL")
	@Temporal(TemporalType.DATE)
	private Date tgl;
	@Column(name="TIPE",length=50)
	private String tipe;
	@Column(name="HARGA",precision=10, scale=2)
	private BigDecimal harga;
	@Column(name="NO_RANGKA",length=15)
	private String noRangka;
	@Column(name="NO_FAK_IN",length=20)
	private String noFakIn;
	@Column(name="NO_SJ_IN",length=25)
	private String noSjIn;
	@Column(name="NO_FAK_OUT",length=20)
	private String noFakOut;
	@Column(name="NO_SJ_OUT",length=20)
	private String noSjOut;
	@Column(name="S_AWAL",precision=10, scale=2)
	private BigDecimal sAwal;
	@Column(name="MASUK",length=3)
	private int masuk;
	@Column(name="KELUAR",length=3)
	private int keluar;
	@Column(name="S_AKHIR",precision=10, scale=2)
	private BigDecimal sAkhir;
	@Column(name="BATAL",length=1)
	private String batal;
	@Column(name="NM_CUST",length=50)
	private String nmCust;
	@Column(name="HARGA_J",precision=10,scale=2)
	private BigDecimal hargaJ;
	@Column(name="DISCOUNT",precision=10,scale=2)
	private BigDecimal discount;
	@Column(name="S_AHM",precision=10,scale=2)
	private BigDecimal sAHM;
	@Column(name="S_MD",precision=10,scale=2)
	private BigDecimal sMD;
	@Column(name="S_SD",precision=10,scale=2)
	private BigDecimal sSD;
	@Column(name="S_FINANCE",precision=10,scale=2)
	private BigDecimal sFinance;
	@Column(name="S_BUNGA",precision=10,scale=2)
	private BigDecimal sBunga;
	@Column(name="B_STNK",precision=10,scale=2)
	private BigDecimal bSTNK;
	@Column(name="P_STNK",precision=10,scale=2)
	private BigDecimal pSTNK;
	@Column(name="P_BPKB",precision=10,scale=2)
	private BigDecimal pBPKB;
	
	public TrioStokunit() {
		this.trioStokunitPK = new TrioStokunitPK();
	}
	
	public TrioStokunit(String noMesin, String kdItem) {
		this.trioStokunitPK = new TrioStokunitPK(noMesin, kdItem);
	}

	public TrioStokunitPK getTrioStokunitPK() {
		return trioStokunitPK;
	}

	public void setTrioStokunitPK(TrioStokunitPK trioStokunitPK) {
		this.trioStokunitPK = trioStokunitPK;
	}

	public String getKdDlr() {
		return kdDlr;
	}

	public void setKdDlr(String kdDlr) {
		this.kdDlr = kdDlr;
	}

	public String getKet() {
		return ket;
	}

	public void setKet(String ket) {
		this.ket = ket;
	}

	public Date getTgl() {
		return tgl;
	}

	public void setTgl(Date tgl) {
		this.tgl = tgl;
	}

	public String getTipe() {
		return tipe;
	}

	public void setTipe(String tipe) {
		this.tipe = tipe;
	}

	public BigDecimal getHarga() {
		return harga;
	}

	public void setHarga(BigDecimal harga) {
		this.harga = harga;
	}

	public String getNoRangka() {
		return noRangka;
	}

	public void setNoRangka(String noRangka) {
		this.noRangka = noRangka;
	}

	public String getNoFakIn() {
		return noFakIn;
	}

	public void setNoFakIn(String noFakIn) {
		this.noFakIn = noFakIn;
	}

	public String getNoSjIn() {
		return noSjIn;
	}

	public void setNoSjIn(String noSjIn) {
		this.noSjIn = noSjIn;
	}

	public String getNoFakOut() {
		return noFakOut;
	}

	public void setNoFakOut(String noFakOut) {
		this.noFakOut = noFakOut;
	}

	public String getNoSjOut() {
		return noSjOut;
	}

	public void setNoSjOut(String noSjOut) {
		this.noSjOut = noSjOut;
	}

	public BigDecimal getsAwal() {
		return sAwal;
	}

	public void setsAwal(BigDecimal sAwal) {
		this.sAwal = sAwal;
	}

	public int getMasuk() {
		return masuk;
	}

	public void setMasuk(int masuk) {
		this.masuk = masuk;
	}

	public int getKeluar() {
		return keluar;
	}

	public void setKeluar(int keluar) {
		this.keluar = keluar;
	}

	public BigDecimal getsAkhir() {
		return sAkhir;
	}

	public void setsAkhir(BigDecimal sAkhir) {
		this.sAkhir = sAkhir;
	}

	public String getBatal() {
		return batal;
	}

	public void setBatal(String batal) {
		this.batal = batal;
	}

	public String getNmCust() {
		return nmCust;
	}

	public void setNmCust(String nmCust) {
		this.nmCust = nmCust;
	}

	public BigDecimal getHargaJ() {
		return hargaJ;
	}

	public void setHargaJ(BigDecimal hargaJ) {
		this.hargaJ = hargaJ;
	}

	public BigDecimal getDiscount() {
		return discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	public BigDecimal getsAHM() {
		return sAHM;
	}

	public void setsAHM(BigDecimal sAHM) {
		this.sAHM = sAHM;
	}

	public BigDecimal getsMD() {
		return sMD;
	}

	public void setsMD(BigDecimal sMD) {
		this.sMD = sMD;
	}

	public BigDecimal getsSD() {
		return sSD;
	}

	public void setsSD(BigDecimal sSD) {
		this.sSD = sSD;
	}

	public BigDecimal getsFinance() {
		return sFinance;
	}

	public void setsFinance(BigDecimal sFinance) {
		this.sFinance = sFinance;
	}

	public BigDecimal getsBunga() {
		return sBunga;
	}

	public void setsBunga(BigDecimal sBunga) {
		this.sBunga = sBunga;
	}

	public BigDecimal getbSTNK() {
		return bSTNK;
	}

	public void setbSTNK(BigDecimal bSTNK) {
		this.bSTNK = bSTNK;
	}

	public BigDecimal getpSTNK() {
		return pSTNK;
	}

	public void setpSTNK(BigDecimal pSTNK) {
		this.pSTNK = pSTNK;
	}

	public BigDecimal getpBPKB() {
		return pBPKB;
	}

	public void setpBPKB(BigDecimal pBPKB) {
		this.pBPKB = pBPKB;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((trioStokunitPK == null) ? 0 : trioStokunitPK.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioStokunit other = (TrioStokunit) obj;
		if (trioStokunitPK == null) {
			if (other.trioStokunitPK != null)
				return false;
		} else if (!trioStokunitPK.equals(other.trioStokunitPK))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioStokunit [trioStokunitPK=" + trioStokunitPK + "]";
	}
	
}

