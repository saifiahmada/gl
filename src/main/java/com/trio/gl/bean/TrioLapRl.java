package com.trio.gl.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.trio.gl.usertrail.TrioEntityUserTrail;

@Entity
@Table(name="TRIO_LAP_RL")
public class TrioLapRl extends TrioEntityUserTrail implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="BARIS")
	private Integer baris;
	
	@Column(name="ID_PERKIRAAN", length=6)
	private String idPerkiraan;
	
	@Column(name="KD_JD", length=3)
	private String kdJd;
	
	@Column(name="PERIODE", length=2)
	private String periode;
	
	@Column(name="TGL_RL")
	@Temporal(TemporalType.TIMESTAMP)
	private Date tglRl;
	
	@Column(name="NAMA_PERKIRAAN", length=35)
	private String namaPerkiraan;
	
	@Column(name="KET", length=1)
	private String ket;
	
	@Column(name="KET2", length = 1)
	private String ket2;
	
	@Column(name="LEVEL_1", precision=12, scale=0)
	private BigDecimal level1;
	
	@Column(name="LEVEL_2", precision=12, scale=0)
	private BigDecimal level2;
	
	@Column(name="LEVEL_3", precision=12, scale=0)
	private BigDecimal level3;
	
	@Column(name="LEVEL_4", precision=12, scale=0)
	private BigDecimal level4;
	
	
	@Column(name="NILAI", precision=12, scale=0)
	private  BigDecimal nilai;

	
	public TrioLapRl() {
		// TODO Auto-generated constructor stub
	}

	public Integer getBaris() {
		return baris;
	}


	public void setBaris(Integer baris) {
		this.baris = baris;
	}


	public String getIdPerkiraan() {
		return idPerkiraan;
	}


	public void setIdPerkiraan(String idPerkiraan) {
		this.idPerkiraan = idPerkiraan;
	}


	public String getKdJd() {
		return kdJd;
	}


	public void setKdJd(String kdJd) {
		this.kdJd = kdJd;
	}


	public String getPeriode() {
		return periode;
	}


	public void setPeriode(String periode) {
		this.periode = periode;
	}


	public Date getTglRl() {
		return tglRl;
	}


	public void setTglRl(Date tglRl) {
		this.tglRl = tglRl;
	}


	public String getNamaPerkiraan() {
		return namaPerkiraan;
	}


	public void setNamaPerkiraan(String namaPerkiraan) {
		this.namaPerkiraan = namaPerkiraan;
	}


	public String getKet() {
		return ket;
	}


	public void setKet(String ket) {
		this.ket = ket;
	}


	public String getKet2() {
		return ket2;
	}


	public void setKet2(String ket2) {
		this.ket2 = ket2;
	}


	public BigDecimal getLevel1() {
		return level1;
	}


	public void setLevel1(BigDecimal level1) {
		this.level1 = level1;
	}


	public BigDecimal getLevel2() {
		return level2;
	}


	public void setLevel2(BigDecimal level2) {
		this.level2 = level2;
	}


	public BigDecimal getLevel3() {
		return level3;
	}


	public void setLevel3(BigDecimal level3) {
		this.level3 = level3;
	}


	public BigDecimal getLevel4() {
		return level4;
	}


	public void setLevel4(BigDecimal level4) {
		this.level4 = level4;
	}


	public BigDecimal getNilai() {
		return nilai;
	}


	public void setNilai(BigDecimal nilai) {
		this.nilai = nilai;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((baris == null) ? 0 : baris.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioLapRl other = (TrioLapRl) obj;
		if (baris == null) {
			if (other.baris != null)
				return false;
		} else if (!baris.equals(other.baris))
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "TrioLapRl [baris=" + baris + "]";
	}
}
