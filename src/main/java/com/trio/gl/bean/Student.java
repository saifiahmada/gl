package com.trio.gl.bean;

import java.io.Serializable;

/** @author Saifi Ahmada Feb 27, 2013 9:45:21 AM  **/

public class Student implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String id;
	
	private String nama;
	
	private String noTelp;
	
	public Student() {
	}
	
	public Student(String id) {
		this.id = id;
	}
	
	public Student(String id, String nama) {
		this.id = id;
		this.nama = nama;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getNoTelp() {
		return noTelp;
	}

	public void setNoTelp(String noTelp) {
		this.noTelp = noTelp;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Student [id=" + id + "]";
	}

}

