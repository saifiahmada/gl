package com.trio.gl.bean;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Id;

/** @author Saifi Ahmada Dec 23, 2013 3:44:45 PM  **/

/*
 * Class ini untuk helper
 * idPerkiraan , debet , kredit
 */
@Entity
public class DKHelper implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private String idPerkiraan;
	
	private BigDecimal debet;
	
	private BigDecimal kredit;
	
	public DKHelper() {
		// TODO Auto-generated constructor stub
	}
	
	public DKHelper(String idPerkiraan) {
		this.idPerkiraan = idPerkiraan;
	}

	public String getIdPerkiraan() {
		return idPerkiraan;
	}

	public void setIdPerkiraan(String idPerkiraan) {
		this.idPerkiraan = idPerkiraan;
	}

	public BigDecimal getDebet() {
		return debet;
	}

	public void setDebet(BigDecimal debet) {
		this.debet = debet;
	}

	public BigDecimal getKredit() {
		return kredit;
	}

	public void setKredit(BigDecimal kredit) {
		this.kredit = kredit;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((idPerkiraan == null) ? 0 : idPerkiraan.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DKHelper other = (DKHelper) obj;
		if (idPerkiraan == null) {
			if (other.idPerkiraan != null)
				return false;
		} else if (!idPerkiraan.equals(other.idPerkiraan))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DKHelper [idPerkiraan=" + idPerkiraan + "]";
	}
	
}
