package com.trio.gl.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.trio.gl.usertrail.TrioEntityUserTrail;

/** @author Saifi Ahmada Apr 12, 2013 9:57:09 PM  **/
@Entity
@Table(name="TRIO_BUKUBESAR")
public class TrioBukubesar extends TrioEntityUserTrail implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private TrioBukubesarPK trioBukubesarPK;
	
	@Column(name="TANGGAL")
	@Temporal(TemporalType.DATE)
	private Date tanggal;
	@Column(name="DEBET", precision=17, scale=2)
	private BigDecimal debet;
	@Column(name="KREDIT", precision=17,scale=2)
	private BigDecimal kredit;
	@Column(name="SALDO_AWAL", precision=17,scale=2)
	private BigDecimal saldoAwal;
	@Column(name="SALDO_AKHIR", precision=17,scale=2)
	private BigDecimal saldoAkhir;
	@Column(name="KET_AWAL",length=1)
	private String ketAwal;
	@Column(name="KET", length=100)
	private String ket;
	@Column(name="TIPE", length=1)
	private String tipe;
	
	@ManyToOne
	@JoinColumn(name="ID_PERKIRAAN", referencedColumnName="ID_PERKIRAAN", insertable=false, updatable=false)
	private TrioMstperkiraan trioMstperkiraan;
	
	public TrioMstperkiraan getTrioMstperkiraan() {
		return trioMstperkiraan;
	}

	public void setTrioMstperkiraan(TrioMstperkiraan trioMstperkiraan) {
		this.trioMstperkiraan = trioMstperkiraan;
	}

	public TrioBukubesar() {
		this.trioBukubesarPK = new TrioBukubesarPK();
		this.trioMstperkiraan = new TrioMstperkiraan();
	}
	
	public TrioBukubesar(String idJurnal, String idPerkiraan, String idSub ) {
		this.trioBukubesarPK = new TrioBukubesarPK(idJurnal, idPerkiraan, idSub);
		this.trioMstperkiraan = new TrioMstperkiraan();
	}

	public TrioBukubesarPK getTrioBukubesarPK() {
		return trioBukubesarPK;
	}

	public void setTrioBukubesarPK(TrioBukubesarPK trioBukubesarPK) {
		this.trioBukubesarPK = trioBukubesarPK;
	}

	public Date getTanggal() {
		return tanggal;
	}

	public void setTanggal(Date tanggal) {
		this.tanggal = tanggal;
	}

	public BigDecimal getDebet() {
		return debet;
	}

	public void setDebet(BigDecimal debet) {
		this.debet = debet;
	}

	public BigDecimal getKredit() {
		return kredit;
	}

	public void setKredit(BigDecimal kredit) {
		this.kredit = kredit;
	}

	public BigDecimal getSaldoAwal() {
		return saldoAwal;
	}

	public void setSaldoAwal(BigDecimal saldoAwal) {
		this.saldoAwal = saldoAwal;
	}

	public BigDecimal getSaldoAkhir() {
		return saldoAkhir;
	}

	public void setSaldoAkhir(BigDecimal saldoAkhir) {
		this.saldoAkhir = saldoAkhir;
	}

	public String getKetAwal() {
		return ketAwal;
	}

	public void setKetAwal(String ketAwal) {
		this.ketAwal = ketAwal;
	}

	public String getKet() {
		return ket;
	}

	public void setKet(String ket) {
		this.ket = ket;
	}

	public String getTipe() {
		return tipe;
	}

	public void setTipe(String tipe) {
		this.tipe = tipe;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((trioBukubesarPK == null) ? 0 : trioBukubesarPK.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioBukubesar other = (TrioBukubesar) obj;
		if (trioBukubesarPK == null) {
			if (other.trioBukubesarPK != null)
				return false;
		} else if (!trioBukubesarPK.equals(other.trioBukubesarPK))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioBukubesar [trioBukubesarPK=" + trioBukubesarPK + "]";
	}

}

