package com.trio.gl.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @author Gusti Arya 9:33:55 AM Jul 16, 2013
 */

@Embeddable
public class TrioDetailStnkPK implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name="NO_MHN", length=10)
	private String noMhn;
	
	@Column(name="NO_MESIN", length=13)
	private String noMesin;
	
	
	public TrioDetailStnkPK() {
		// TODO Auto-generated constructor stub
	}
	
	public TrioDetailStnkPK(String noMesin, String noMhn) {
		super();
		this.noMesin = noMesin;
		this.noMhn = noMhn;
	}

	public String getNoMesin() {
		return noMesin;
	}

	public void setNoMesin(String noMesin) {
		this.noMesin = noMesin;
	}

	public String getNoMhn() {
		return noMhn;
	}

	public void setNoMhn(String noMhn) {
		this.noMhn = noMhn;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((noMesin == null) ? 0 : noMesin.hashCode());
		result = prime * result + ((noMhn == null) ? 0 : noMhn.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioDetailStnkPK other = (TrioDetailStnkPK) obj;
		if (noMesin == null) {
			if (other.noMesin != null)
				return false;
		} else if (!noMesin.equals(other.noMesin))
			return false;
		if (noMhn == null) {
			if (other.noMhn != null)
				return false;
		} else if (!noMhn.equals(other.noMhn))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioDetailStnkPK [noMhn=" + noMhn + ", noMesin=" + noMesin
				+ "]";
	}
	
}
