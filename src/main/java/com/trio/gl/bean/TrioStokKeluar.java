package com.trio.gl.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.trio.gl.usertrail.TrioEntityUserTrail;

/** @author Saifi Ahmada Jul 19, 2013 4:41:22 PM  **/
@Embeddable
@Table(name="TRIO_STOK_KELUAR")
public class TrioStokKeluar extends TrioEntityUserTrail implements Serializable {

	private static final long serialVersionUID = 1L;
	@EmbeddedId
	private TrioStokKeluarPK trioStokKeluarPK;
	@Column(name="KD_DLR", length=6)
	private String kdDlr;
	@Column(name="NO_REF", length=20)
	private String noRef;
	@Column(name="QTY")
	private int qty;
	@Column(name="STATUS", length=1)
	private String status;
	@Column(name="TGL_NOTA")
	@Temporal(TemporalType.DATE)
	private Date tglNota;
	
	public TrioStokKeluar() {
	}
	
	public TrioStokKeluar(TrioStokKeluarPK trioStokKeluarPK) {
		this.trioStokKeluarPK = trioStokKeluarPK;
	}

	public TrioStokKeluarPK getTrioStokKeluarPK() {
		return trioStokKeluarPK;
	}

	public void setTrioStokKeluarPK(TrioStokKeluarPK trioStokKeluarPK) {
		this.trioStokKeluarPK = trioStokKeluarPK;
	}

	public String getKdDlr() {
		return kdDlr;
	}

	public void setKdDlr(String kdDlr) {
		this.kdDlr = kdDlr;
	}

	public String getNoRef() {
		return noRef;
	}

	public void setNoRef(String noRef) {
		this.noRef = noRef;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getTglNota() {
		return tglNota;
	}

	public void setTglNota(Date tglNota) {
		this.tglNota = tglNota;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((trioStokKeluarPK == null) ? 0 : trioStokKeluarPK.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioStokKeluar other = (TrioStokKeluar) obj;
		if (trioStokKeluarPK == null) {
			if (other.trioStokKeluarPK != null)
				return false;
		} else if (!trioStokKeluarPK.equals(other.trioStokKeluarPK))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioStokKeluar [trioStokKeluarPK=" + trioStokKeluarPK + "]";
	}

}

