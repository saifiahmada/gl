package com.trio.gl.bean;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.trio.gl.usertrail.TrioEntityUserTrail;

/** @author Saifi Ahmada Apr 16, 2013 8:53:16 PM  **/
@Entity
@Table(name="TRIO_MSTSALDOPERKIRAAN")
public class TrioMstsaldoperkiraan extends TrioEntityUserTrail implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private TrioMstsaldoperkiraanPK trioMstsaldoperkiraanPK;
	
	@Column(name="BUL_0", precision=17, scale=2)
	private BigDecimal bul0;
	
	@Column(name="KET_0" , length=1)
	private String ket0;
	
	@Column(name="BUL_1", precision=17, scale=2)
	private BigDecimal bul1;
	
	@Column(name="KET_1" , length=1)
	private String ket1;
	
	@Column(name="BUL_2", precision=17, scale=2)
	private BigDecimal bul2;
	
	@Column(name="KET_2" , length=1)
	private String ket2;
	
	@Column(name="BUL_3", precision=17, scale=2)
	private BigDecimal bul3;
	
	@Column(name="KET_3" , length=1)
	private String ket3;
	
	@Column(name="BUL_4", precision=17, scale=2)
	private BigDecimal bul4;
	
	@Column(name="KET_4" , length=1)
	private String ket4;
	
	@Column(name="BUL_5", precision=17, scale=2)
	private BigDecimal bul5;
	
	@Column(name="KET_5" , length=1)
	private String ket5;
	
	@Column(name="BUL_6", precision=17, scale=2)
	private BigDecimal bul6;
	
	@Column(name="KET_6" , length=1)
	private String ket6;
	
	@Column(name="BUL_7", precision=17, scale=2)
	private BigDecimal bul7;
	
	@Column(name="KET_7" , length=1)
	private String ket7;
	
	@Column(name="BUL_8", precision=17, scale=2)
	private BigDecimal bul8;
	
	@Column(name="KET_8" , length=1)
	private String ket8;
	
	@Column(name="BUL_9", precision=17, scale=2)
	private BigDecimal bul9;
	
	@Column(name="KET_9" , length=1)
	private String ket9;
	
	@Column(name="BUL_10", precision=17, scale=2)
	private BigDecimal bul10;
	
	@Column(name="KET_10" , length=1)
	private String ket10;
	
	@Column(name="BUL_11", precision=17, scale=2)
	private BigDecimal bul11;
	
	@Column(name="KET_11" , length=1)
	private String ket11;
	
	@Column(name="BUL_12", precision=17, scale=2)
	private BigDecimal bul12;
	
	@Column(name="KET_12" , length=1)
	private String ket12;
	
	@JoinColumn(name="ID_PERKIRAAN", referencedColumnName="ID_PERKIRAAN", insertable=false, updatable=false)
	@ManyToOne
	private TrioMstperkiraan trioMstperkiraan;
	
	public TrioMstperkiraan getTrioMstperkiraan() {
		return trioMstperkiraan;
	}

	public void setTrioMstperkiraan(TrioMstperkiraan trioMstperkiraan) {
		this.trioMstperkiraan = trioMstperkiraan;
	}

	public TrioMstsaldoperkiraan() {
		this.trioMstsaldoperkiraanPK = new TrioMstsaldoperkiraanPK();
	}
	
	public TrioMstsaldoperkiraan(String idPerkiraan, String tahun) {
		this.trioMstsaldoperkiraanPK = new TrioMstsaldoperkiraanPK(idPerkiraan, tahun);
	}

	public TrioMstsaldoperkiraanPK getTrioMstsaldoperkiraanPK() {
		return trioMstsaldoperkiraanPK;
	}

	public void setTrioMstsaldoperkiraanPK(
			TrioMstsaldoperkiraanPK trioMstsaldoperkiraanPK) {
		this.trioMstsaldoperkiraanPK = trioMstsaldoperkiraanPK;
	}

	public BigDecimal getBul0() {
		return bul0;
	}

	public void setBul0(BigDecimal bul0) {
		this.bul0 = bul0;
	}

	public String getKet0() {
		return ket0;
	}

	public void setKet0(String ket0) {
		this.ket0 = ket0;
	}

	public BigDecimal getBul1() {
		return bul1;
	}

	public void setBul1(BigDecimal bul1) {
		this.bul1 = bul1;
	}

	public String getKet1() {
		return ket1;
	}

	public void setKet1(String ket1) {
		this.ket1 = ket1;
	}

	public BigDecimal getBul2() {
		return bul2;
	}

	public void setBul2(BigDecimal bul2) {
		this.bul2 = bul2;
	}

	public String getKet2() {
		return ket2;
	}

	public void setKet2(String ket2) {
		this.ket2 = ket2;
	}

	public BigDecimal getBul3() {
		return bul3;
	}

	public void setBul3(BigDecimal bul3) {
		this.bul3 = bul3;
	}

	public String getKet3() {
		return ket3;
	}

	public void setKet3(String ket3) {
		this.ket3 = ket3;
	}

	public BigDecimal getBul4() {
		return bul4;
	}

	public void setBul4(BigDecimal bul4) {
		this.bul4 = bul4;
	}

	public String getKet4() {
		return ket4;
	}

	public void setKet4(String ket4) {
		this.ket4 = ket4;
	}

	public BigDecimal getBul5() {
		return bul5;
	}

	public void setBul5(BigDecimal bul5) {
		this.bul5 = bul5;
	}

	public String getKet5() {
		return ket5;
	}

	public void setKet5(String ket5) {
		this.ket5 = ket5;
	}

	public BigDecimal getBul6() {
		return bul6;
	}

	public void setBul6(BigDecimal bul6) {
		this.bul6 = bul6;
	}

	public String getKet6() {
		return ket6;
	}

	public void setKet6(String ket6) {
		this.ket6 = ket6;
	}

	public BigDecimal getBul7() {
		return bul7;
	}

	public void setBul7(BigDecimal bul7) {
		this.bul7 = bul7;
	}

	public String getKet7() {
		return ket7;
	}

	public void setKet7(String ket7) {
		this.ket7 = ket7;
	}

	public BigDecimal getBul8() {
		return bul8;
	}

	public void setBul8(BigDecimal bul8) {
		this.bul8 = bul8;
	}

	public String getKet8() {
		return ket8;
	}

	public void setKet8(String ket8) {
		this.ket8 = ket8;
	}

	public BigDecimal getBul9() {
		return bul9;
	}

	public void setBul9(BigDecimal bul9) {
		this.bul9 = bul9;
	}

	public String getKet9() {
		return ket9;
	}

	public void setKet9(String ket9) {
		this.ket9 = ket9;
	}

	public BigDecimal getBul10() {
		return bul10;
	}

	public void setBul10(BigDecimal bul10) {
		this.bul10 = bul10;
	}

	public String getKet10() {
		return ket10;
	}

	public void setKet10(String ket10) {
		this.ket10 = ket10;
	}

	public BigDecimal getBul11() {
		return bul11;
	}

	public void setBul11(BigDecimal bul11) {
		this.bul11 = bul11;
	}

	public String getKet11() {
		return ket11;
	}

	public void setKet11(String ket11) {
		this.ket11 = ket11;
	}

	public BigDecimal getBul12() {
		return bul12;
	}

	public void setBul12(BigDecimal bul12) {
		this.bul12 = bul12;
	}

	public String getKet12() {
		return ket12;
	}

	public void setKet12(String ket12) {
		this.ket12 = ket12;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((trioMstsaldoperkiraanPK == null) ? 0
						: trioMstsaldoperkiraanPK.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioMstsaldoperkiraan other = (TrioMstsaldoperkiraan) obj;
		if (trioMstsaldoperkiraanPK == null) {
			if (other.trioMstsaldoperkiraanPK != null)
				return false;
		} else if (!trioMstsaldoperkiraanPK
				.equals(other.trioMstsaldoperkiraanPK))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioMstsaldoperkiraan [trioMstsaldoperkiraanPK="
				+ trioMstsaldoperkiraanPK + "]";
	}
	
}

