package com.trio.gl.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/** @author Saifi Ahmada Mar 13, 2013 12:12:57 PM  **/
@Embeddable
public class TrioJurnalPK implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Column(name="ID_JURNAL", length=14, nullable=false)
	private String idJurnal;
	@Column(name="ID_PERKIRAAN", length=6, nullable=false)
	private String idPerkiraan;
	@Column(name="ID_SUB", length=5, nullable=false)
	private String idSub;
	
	public TrioJurnalPK() {
	}
	
	public TrioJurnalPK(String idJurnal, String idPerkiraan, String idSub) {
		super();
		this.idJurnal = idJurnal;
		this.idPerkiraan = idPerkiraan;
		this.idSub = idSub;
	}



	public String getIdJurnal() {
		return idJurnal;
	}

	public void setIdJurnal(String idJurnal) {
		this.idJurnal = idJurnal;
	}

	public String getIdPerkiraan() {
		return idPerkiraan;
	}

	public void setIdPerkiraan(String idPerkiraan) {
		this.idPerkiraan = idPerkiraan;
	}
	
	public String getIdSub() {
		return idSub;
	}

	public void setIdSub(String idSub) {
		this.idSub = idSub;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((idJurnal == null) ? 0 : idJurnal.hashCode());
		result = prime * result
				+ ((idPerkiraan == null) ? 0 : idPerkiraan.hashCode());
		result = prime * result + ((idSub == null) ? 0 : idSub.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioJurnalPK other = (TrioJurnalPK) obj;
		if (idJurnal == null) {
			if (other.idJurnal != null)
				return false;
		} else if (!idJurnal.equals(other.idJurnal))
			return false;
		if (idPerkiraan == null) {
			if (other.idPerkiraan != null)
				return false;
		} else if (!idPerkiraan.equals(other.idPerkiraan))
			return false;
		if (idSub == null) {
			if (other.idSub != null)
				return false;
		} else if (!idSub.equals(other.idSub))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioJurnalPK [idJurnal=" + idJurnal + ", idPerkiraan="
				+ idPerkiraan + ", idSub=" + idSub + "]";
	}

	
}

