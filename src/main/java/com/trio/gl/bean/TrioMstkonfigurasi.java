package com.trio.gl.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.trio.gl.usertrail.TrioEntityUserTrail;

/** @author Saifi Ahmada Mar 22, 2013 9:06:29 AM  **/

@Entity
@Table(name="TRIO_MSTKONFIGURASI")
public class TrioMstkonfigurasi extends TrioEntityUserTrail implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private TrioMstkonfigurasiPK trioMstkonfigurasiPK;
	
	@Column(name="NAMA", length=30, nullable=false)
	private String nama;
	@Column(name="NILAI", length=50, nullable=false)
	private String nilai;
	
	public TrioMstkonfigurasi() {
		this.trioMstkonfigurasiPK = new TrioMstkonfigurasiPK();
	}
	
	public TrioMstkonfigurasi(String idKonfigurasi) {
		this.trioMstkonfigurasiPK = new TrioMstkonfigurasiPK(idKonfigurasi);
	}

	public TrioMstkonfigurasiPK getTrioMstkonfigurasiPK() {
		return trioMstkonfigurasiPK;
	}

	public void setTrioMstkonfigurasiPK(TrioMstkonfigurasiPK trioMstkonfigurasiPK) {
		this.trioMstkonfigurasiPK = trioMstkonfigurasiPK;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getNilai() {
		return nilai;
	}

	public void setNilai(String nilai) {
		this.nilai = nilai;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((trioMstkonfigurasiPK == null) ? 0 : trioMstkonfigurasiPK
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioMstkonfigurasi other = (TrioMstkonfigurasi) obj;
		if (trioMstkonfigurasiPK == null) {
			if (other.trioMstkonfigurasiPK != null)
				return false;
		} else if (!trioMstkonfigurasiPK.equals(other.trioMstkonfigurasiPK))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioMstkonfigurasi [trioMstkonfigurasiPK="
				+ trioMstkonfigurasiPK + "]";
	}

}

