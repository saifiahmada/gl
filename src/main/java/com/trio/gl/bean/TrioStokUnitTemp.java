package com.trio.gl.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.trio.gl.usertrail.TrioEntityUserTrail;

/**
 * @author Gusti Arya 3:11:16 PM Jun 20, 2013
 */

/**
 * Class Sementara untuk tabel TrioStokUnit sampai ada instruksi lebih lanjut
 * Class Aslinya bisa di buka disini {@link TrioStokunit}
 * @author Gusti Arya
 *
 */

@Entity
@Table(name="TRIO_STOKUNIT_TEMP")
public class TrioStokUnitTemp extends TrioEntityUserTrail implements Serializable {

	private static final long serialVersionUID = 1L;


	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long baris;

	@Column(name="NO_MESIN", length=13)
	private String noMesin;
	@Column(name="KD_ITEM", length=6)
	private String kdItem;
	@Column(name="KD_DLR",length=10)
	private String kdDlr;
	@Column(name="KET",length=20)
	private String ket;
	@Column(name="TGL")
	@Temporal(TemporalType.DATE)
	private Date tgl;
	@Column(name="TIPE",length=50)
	private String tipe;
	@Column(name="HARGA",precision=10, scale=2)
	private BigDecimal harga;
	@Column(name="NO_RANGKA",length=15)
	private String noRangka;
	@Column(name="NO_FAK_IN",length=20)
	private String noFakIn;
	@Column(name="NO_SJ_IN",length=25)
	private String noSjIn;
	@Column(name="NO_FAK_OUT",length=20)
	private String noFakOut;
	@Column(name="NO_SJ_OUT",length=20)
	private String noSjOut;
	@Column(name="S_AWAL",precision=10, scale=2)
	private BigDecimal sAwal;
	@Column(name="MASUK",length=3)
	private Integer masuk;
	@Column(name="KELUAR",length=3)
	private Integer keluar;
	@Column(name="S_AKHIR",precision=10, scale=2)
	private BigDecimal sAkhir;
	@Column(name="BATAL",length=1)
	private String batal;
	@Column(name="NM_CUST",length=50)
	private String nmCust;
	@Column(name="HARGA_J",precision=10,scale=2)
	private BigDecimal hargaJ;
	@Column(name="DISCOUNT",precision=10,scale=2)
	private BigDecimal discount;
	@Column(name="S_AHM",precision=10,scale=2)
	private BigDecimal sAHM;
	@Column(name="S_MD",precision=10,scale=2)
	private BigDecimal sMD;
	@Column(name="S_SD",precision=10,scale=2)
	private BigDecimal sSD;
	@Column(name="S_FINANCE",precision=10,scale=2)
	private BigDecimal sFinance;
	@Column(name="S_BUNGA",precision=10,scale=2)
	private BigDecimal sBunga;
	@Column(name="B_STNK",precision=10,scale=2)
	private BigDecimal bSTNK;
	@Column(name="P_STNK",precision=10,scale=2)
	private BigDecimal pSTNK;
	@Column(name="P_BPKB",precision=10,scale=2)
	private BigDecimal pBPKB;

	public TrioStokUnitTemp() {
		// TODO Auto-generated constructor stub
	}

	public TrioStokUnitTemp(String kdDlr){
		super();
		this.kdDlr = kdDlr;
	}

	//VqsAwal di rptStockSmh
	public TrioStokUnitTemp(String kdDlr,String kdItem, String tipe,
			BigDecimal harga, Long sAwal,Integer masuk,Integer keluar 
			,Integer sAkhir) {
		super();
		this.kdDlr = kdDlr;
		this.kdItem = kdItem;
		this.tipe = tipe;
		this.harga = harga;
		this.sAwal = new BigDecimal(sAwal);
		this.masuk = masuk;
		this.keluar = keluar ;
		this.sAkhir = new BigDecimal(sAkhir);
	}
	
	
	//vqTranMasuk di rptStockSmh
	public TrioStokUnitTemp(String kdDlr,String kdItem, String tipe,
			BigDecimal harga,Integer sAwal,Long masuk, Integer keluar
			,Integer sAkhir) {
		super();
		this.kdDlr = kdDlr;
		this.kdItem = kdItem;
		this.tipe = tipe;
		this.harga = harga;
		this.sAwal = new BigDecimal(sAwal);
		this.masuk = new BigDecimal(masuk).intValueExact();
		this.keluar = keluar ;
		this.sAkhir = new BigDecimal(sAkhir);
	}
	
	//vqTrankeluar di rptStockSmh
		public TrioStokUnitTemp(String kdDlr,String kdItem, String tipe,
				BigDecimal harga,Integer sAwal,Integer masuk, Long keluar
				,Integer sAkhir) {
			super();
			this.kdDlr = kdDlr;
			this.kdItem = kdItem;
			this.tipe = tipe;
			this.harga = harga;
			this.sAwal = new BigDecimal(sAwal);
			this.masuk = masuk;
			this.keluar = new BigDecimal(keluar).intValueExact();
			this.sAkhir = new BigDecimal(sAkhir);
		}

	public TrioStokUnitTemp(String noMesin, String kdItem, String kdDlr,
			String ket, Date tgl, String tipe, String noRangka,
			String noFakOut, String noSjOut, String nmCust, BigDecimal hargaJ,
			BigDecimal discount, BigDecimal sAHM, BigDecimal sMD,
			BigDecimal sSD, BigDecimal sFinance, BigDecimal sBunga) {
		super();
		this.noMesin = noMesin;
		this.kdItem = kdItem;
		this.kdDlr = kdDlr;
		this.ket = ket;
		this.tgl = tgl;
		this.tipe = tipe;
		this.noRangka = noRangka;
		this.noFakOut = noFakOut;
		this.noSjOut = noSjOut;
		this.nmCust = nmCust;
		this.hargaJ = hargaJ;
		this.discount = discount;
		this.sAHM = sAHM;
		this.sMD = sMD;
		this.sSD = sSD;
		this.sFinance = sFinance;
		this.sBunga = sBunga;
	}

	public String getNoMesin() {
		return noMesin;
	}
	public void setNoMesin(String noMesin) {
		this.noMesin = noMesin;
	}
	public String getKdItem() {
		return kdItem;
	}
	public void setKdItem(String kdItem) {
		this.kdItem = kdItem;
	}
	public String getKdDlr() {
		return kdDlr;
	}
	public void setKdDlr(String kdDlr) {
		this.kdDlr = kdDlr;
	}
	public String getKet() {
		return ket;
	}
	public void setKet(String ket) {
		this.ket = ket;
	}
	public Date getTgl() {
		return tgl;
	}
	public void setTgl(Date tgl) {
		this.tgl = tgl;
	}
	public String getTipe() {
		return tipe;
	}
	public void setTipe(String tipe) {
		this.tipe = tipe;
	}
	public BigDecimal getHarga() {
		return harga;
	}
	public void setHarga(BigDecimal harga) {
		this.harga = harga;
	}
	public String getNoRangka() {
		return noRangka;
	}
	public void setNoRangka(String noRangka) {
		this.noRangka = noRangka;
	}
	public String getNoFakIn() {
		return noFakIn;
	}
	public void setNoFakIn(String noFakIn) {
		this.noFakIn = noFakIn;
	}
	public String getNoSjIn() {
		return noSjIn;
	}
	public void setNoSjIn(String noSjIn) {
		this.noSjIn = noSjIn;
	}
	public String getNoFakOut() {
		return noFakOut;
	}
	public void setNoFakOut(String noFakOut) {
		this.noFakOut = noFakOut;
	}
	public String getNoSjOut() {
		return noSjOut;
	}
	public void setNoSjOut(String noSjOut) {
		this.noSjOut = noSjOut;
	}
	public BigDecimal getsAwal() {
		return sAwal;
	}
	public void setsAwal(BigDecimal sAwal) {
		this.sAwal = sAwal;
	}
	public Integer getMasuk() {
		return masuk;
	}
	public void setMasuk(Integer masuk) {
		this.masuk = masuk;
	}
	public Integer getKeluar() {
		return keluar;
	}
	public void setKeluar(Integer keluar) {
		this.keluar = keluar;
	}
	public BigDecimal getsAkhir() {
		return sAkhir;
	}
	public void setsAkhir(BigDecimal sAkhir) {
		this.sAkhir = sAkhir;
	}
	public String getBatal() {
		return batal;
	}
	public void setBatal(String batal) {
		this.batal = batal;
	}
	public String getNmCust() {
		return nmCust;
	}
	public void setNmCust(String nmCust) {
		this.nmCust = nmCust;
	}
	public BigDecimal getHargaJ() {
		return hargaJ;
	}
	public void setHargaJ(BigDecimal hargaJ) {
		this.hargaJ = hargaJ;
	}
	public BigDecimal getDiscount() {
		return discount;
	}
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	public BigDecimal getsAHM() {
		return sAHM;
	}
	public void setsAHM(BigDecimal sAHM) {
		this.sAHM = sAHM;
	}
	public BigDecimal getsMD() {
		return sMD;
	}
	public void setsMD(BigDecimal sMD) {
		this.sMD = sMD;
	}
	public BigDecimal getsSD() {
		return sSD;
	}
	public void setsSD(BigDecimal sSD) {
		this.sSD = sSD;
	}
	public BigDecimal getsFinance() {
		return sFinance;
	}
	public void setsFinance(BigDecimal sFinance) {
		this.sFinance = sFinance;
	}
	public BigDecimal getsBunga() {
		return sBunga;
	}
	public void setsBunga(BigDecimal sBunga) {
		this.sBunga = sBunga;
	}
	public BigDecimal getbSTNK() {
		return bSTNK;
	}
	public void setbSTNK(BigDecimal bSTNK) {
		this.bSTNK = bSTNK;
	}
	public BigDecimal getpSTNK() {
		return pSTNK;
	}
	public void setpSTNK(BigDecimal pSTNK) {
		this.pSTNK = pSTNK;
	}
	public BigDecimal getpBPKB() {
		return pBPKB;
	}
	public void setpBPKB(BigDecimal pBPKB) {
		this.pBPKB = pBPKB;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((baris == null) ? 0 : baris.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioStokUnitTemp other = (TrioStokUnitTemp) obj;
		if (baris == null) {
			if (other.baris != null)
				return false;
		} else if (!baris.equals(other.baris))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "TrioStokUnitTemp [baris=" + baris + "]";
	}
}