package com.trio.gl.bean;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.trio.gl.usertrail.TrioEntityUserTrail;

/**
 * @author Gusti Arya 9:51:59 AM Nov 8, 2013
 */

@Entity
@Table(name="TRIO_LAP_NERACA_PASIVA")
public class TrioLapNeracaPasiva extends TrioEntityUserTrail{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="BARIS")
	private int baris;
	
	@Column(name="ID_PERKIRAAN", length=6)
	private String idPerkiraan;
	
	@Column(name="KD_JD", length=3)
	private String kdJd;
	
	@Column(name="PERIODE", length=2)
	private String periode;
	
	@Column(name="NAMA_PERKIRAAN", length=35)
	private String namaPerkiraan;
	
	@Column(name="KET", length=1)
	private String ket;
	
	@Column(name="KET1", length=1)
	private String ket1;
	
	@Column(name="LEVEL_1", precision=14, scale=2)
	private BigDecimal level1;
	
	@Column(name="LEVEL_2", precision=14, scale=2)
	private BigDecimal level2;
	
	@Column(name="NILAI", precision=14, scale=2)
	private BigDecimal nilai;
	
	public TrioLapNeracaPasiva() {
		// TODO Auto-generated constructor stub
	}
	

	public int getBaris() {
		return baris;
	}

	public void setBaris(int baris) {
		this.baris = baris;
	}

	public String getIdPerkiraan() {
		return idPerkiraan;
	}

	public void setIdPerkiraan(String idPerkiraan) {
		this.idPerkiraan = idPerkiraan;
	}

	public String getKdJd() {
		return kdJd;
	}

	public void setKdJd(String kdJd) {
		this.kdJd = kdJd;
	}

	public String getPeriode() {
		return periode;
	}

	public void setPeriode(String periode) {
		this.periode = periode;
	}

	public String getNamaPerkiraan() {
		return namaPerkiraan;
	}

	public void setNamaPerkiraan(String namaPerkiraan) {
		this.namaPerkiraan = namaPerkiraan;
	}

	public String getKet() {
		return ket;
	}

	public void setKet(String ket) {
		this.ket = ket;
	}

	public String getKet1() {
		return ket1;
	}

	public void setKet1(String ket1) {
		this.ket1 = ket1;
	}

	public BigDecimal getLevel1() {
		return level1;
	}

	public void setLevel1(BigDecimal level1) {
		this.level1 = level1;
	}

	public BigDecimal getLevel2() {
		return level2;
	}

	public void setLevel2(BigDecimal level2) {
		this.level2 = level2;
	}

	public BigDecimal getNilai() {
		return nilai;
	}

	public void setNilai(BigDecimal nilai) {
		this.nilai = nilai;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + baris;
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioLapNeracaPasiva other = (TrioLapNeracaPasiva) obj;
		if (baris != other.baris)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioLapNeracaPasiva [baris=" + baris + "]";
	}

}
