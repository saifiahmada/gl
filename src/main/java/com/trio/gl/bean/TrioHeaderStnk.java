package com.trio.gl.bean;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.trio.gl.usertrail.TrioEntityUserTrail;

/**
 * @author Gusti Arya 2:15:25 PM Jul 15, 2013
 */


@Entity
@Table(name="TRIO_HEADER_STNK")
public class TrioHeaderStnk extends TrioEntityUserTrail{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="NO_MHN", length=10, nullable=false)
	private String noMhn;
	
	@Column(name="NM_WIL", length=100)
	private String nmWil;
	
	@Column(name = "TGL")
	@Temporal(TemporalType.DATE)
	private Date tgl;
	
	@Column(name="JUMLAH", length=10, precision=10, scale=0)
	private BigDecimal jumlah;
	
	@Column(name="QTY",length=10, precision=10, scale=0)
	private BigDecimal qty;
	
	@Column(name="PENGURUS", length=20)
	private String pengurus;
	
	@Column(name="FLAG", length=1)
	private String flag;
	
	@Column(name="JNS_KLR", length=5)
	private String  jnsKlr;
	
	@Column(name="STNK", length=10, precision=10, scale=0)
	private BigDecimal stnk;
	
	@Column(name="LAIN", length=10, precision=10, scale=0)
	private BigDecimal lain;
	
	@Column(name="K_STNK", length=10, precision=10, scale=0)
	private BigDecimal kStnk;
	
	@Column(name="K_LAIN", length=10, precision=10, scale=0)
	private BigDecimal kLain;

	@OneToMany(fetch=FetchType.LAZY, mappedBy = "trioHeaderStnk", cascade=CascadeType.ALL)
	private Set<TrioDetailStnk> trioDetailStnkSet;
	
	public TrioHeaderStnk() {
		// TODO Auto-generated constructor stub
	}
	
	public String getNoMhn() {
		return noMhn;
	}

	public void setNoMhn(String noMhn) {
		this.noMhn = noMhn;
	}

	public String getNmWil() {
		return nmWil;
	}

	public void setNmWil(String nmWil) {
		this.nmWil = nmWil;
	}

	public Date getTgl() {
		return tgl;
	}

	public void setTgl(Date tgl) {
		this.tgl = tgl;
	}

	public BigDecimal getJumlah() {
		return jumlah;
	}

	public void setJumlah(BigDecimal jumlah) {
		this.jumlah = jumlah;
	}

	public BigDecimal getQty() {
		return qty;
	}

	public void setQty(BigDecimal qty) {
		this.qty = qty;
	}

	public String getPengurus() {
		return pengurus;
	}

	public void setPengurus(String pengurus) {
		this.pengurus = pengurus;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getJnsKlr() {
		return jnsKlr;
	}

	public void setJnsKlr(String jnsKlr) {
		this.jnsKlr = jnsKlr;
	}

	public BigDecimal getStnk() {
		return stnk;
	}

	public void setStnk(BigDecimal stnk) {
		this.stnk = stnk;
	}

	public BigDecimal getLain() {
		return lain;
	}

	public void setLain(BigDecimal lain) {
		this.lain = lain;
	}

	public BigDecimal getkStnk() {
		return kStnk;
	}

	public void setkStnk(BigDecimal kStnk) {
		this.kStnk = kStnk;
	}

	public BigDecimal getkLain() {
		return kLain;
	}

	public void setkLain(BigDecimal kLain) {
		this.kLain = kLain;
	}
	
	public Set<TrioDetailStnk> getTrioDetailStnkSet() {
		return trioDetailStnkSet;
	}

	public void setTrioDetailStnkSet(Set<TrioDetailStnk> trioDetailStnkSet) {
		this.trioDetailStnkSet = trioDetailStnkSet;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((noMhn == null) ? 0 : noMhn.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioHeaderStnk other = (TrioHeaderStnk) obj;
		if (noMhn == null) {
			if (other.noMhn != null)
				return false;
		} else if (!noMhn.equals(other.noMhn))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioHeaderStnk [noMhn=" + noMhn + "]";
	}
	
}
