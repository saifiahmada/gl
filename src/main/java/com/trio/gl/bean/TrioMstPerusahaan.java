package com.trio.gl.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.trio.gl.usertrail.TrioEntityUserTrail;

/**
 * @author Gusti Arya 3:34:33 PM Sep 9, 2013
 */

@Entity
@Table(name="TRIO_MST_PERUSAHAAN")
public class TrioMstPerusahaan extends TrioEntityUserTrail {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="KD_PERS", length=3, nullable=false)
	private String kdPers;
	
	@Column(name="KD_PERS2", length=3)
	private String kdPers2;
	
	@Column(name="NAMA_PERS", length=35)
	private String namaPers;
	
	@Column(name="ALAMAT_PERS", length=40)
	private String alamatPers;
	
	@Column(name="TELP_1", length=12)
	private String telp1;
	
	@Column(name="TELP_2", length=12)
	private String telp2;
	
	@Column(name="TELP_3", length=12)
	private String telp3;
	
	@Column(name="FAX", length=12)
	private String fax;

	
	public TrioMstPerusahaan() {
		// TODO Auto-generated constructor stub
	}
	
	public String getKdPers() {
		return kdPers;
	}

	public void setKdPers(String kdPers) {
		this.kdPers = kdPers;
	}

	public String getKdPers2() {
		return kdPers2;
	}

	public void setKdPers2(String kdPers2) {
		this.kdPers2 = kdPers2;
	}

	public String getNamaPers() {
		return namaPers;
	}

	public void setNamaPers(String namaPers) {
		this.namaPers = namaPers;
	}

	public String getAlamatPers() {
		return alamatPers;
	}

	public void setAlamatPers(String alamatPers) {
		this.alamatPers = alamatPers;
	}

	public String getTelp1() {
		return telp1;
	}

	public void setTelp1(String telp1) {
		this.telp1 = telp1;
	}

	public String getTelp2() {
		return telp2;
	}

	public void setTelp2(String telp2) {
		this.telp2 = telp2;
	}

	public String getTelp3() {
		return telp3;
	}

	public void setTelp3(String telp3) {
		this.telp3 = telp3;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((kdPers == null) ? 0 : kdPers.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioMstPerusahaan other = (TrioMstPerusahaan) obj;
		if (kdPers == null) {
			if (other.kdPers != null)
				return false;
		} else if (!kdPers.equals(other.kdPers))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioMstPerusahaan [kdPers=" + kdPers + "]";
	}
	
}
