package com.trio.gl.bean;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.trio.gl.usertrail.TrioEntityUserTrail;

/**
 * @author Gusti Arya 11:29:52 AM Jul 17, 2013
 */

@Entity
@Table(name="TRIO_DETAIL_BPKB")
public class TrioDetailBpkb extends TrioEntityUserTrail implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private TrioDetailBpkbPK trioDetailBpkbPK;
	
	@Column(name="NO_RANGKA", length=17)
	private String noRangka;
	
	@Column(name="KD_ITEM", length=6)
	private String kdItem;
	
	@Column(name="NAMA", length=50)
	private String nama;
	
	@Column(name="BIAYA", length=10, precision=10, scale=0)
	private BigDecimal biaya;
	
	@Column(name="CEK",length=1, precision=1, scale=0)
	private BigDecimal cek;
	
	@Column(name="STATUS", length=1)
	private String status;
	
	@Column(name="NO_BUKTI", length=16)
	private String noBukti;
	
	@Column(name="BPKB", length=10, precision=10, scale=0)
	private BigDecimal bpkb;
	
	@Column(name="LAIN", length=10, precision=10, scale=0)
	private BigDecimal lain;
	
	@Column(name="K_BPKB", length=10, precision=10, scale=0)
	private BigDecimal kBpkb;
	
	@Column(name="K_LAIN", length=10, precision=10, scale=0)
	private BigDecimal kLain;
	
	@Column(name="NO_BUKTI_S", length=16)
	private String noBuktiS;
	
	@Column(name="NO_BUKTI_L", length=16)
	private String noBuktiL;
	
	@Column(name="STCK", length=10, precision=10, scale=0)
	private BigDecimal stck;
	
	@Column(name="K_STCK", length=10, precision=10, scale=0)
	private BigDecimal kStck;
	
	@Column(name="B_STCK", length=10, precision=10, scale=0)
	private BigDecimal bStck;
	
	@Column(name="B_LAIN", length=10, precision=10, scale=0)
	private BigDecimal bLain;
	
	@Column(name="B_BPKB", length=10, precision=10, scale=0)
	private BigDecimal bBpkb;
	
	@Column(name="NO_BUKTI_T", length=16)
	private String noBuktiT;
	
	@ManyToOne
	@JoinColumn(name="NO_MHN", referencedColumnName="NO_MHN", insertable=false, updatable=false)
	private TrioHeaderBpkb trioHeaderBpkb;
	
	public TrioDetailBpkb() {
		// TODO Auto-generated constructor stub
	}

	public TrioDetailBpkbPK getTrioDetailBpkbPK() {
		return trioDetailBpkbPK;
	}

	public void setTrioDetailBpkbPK(TrioDetailBpkbPK trioDetailBpkbPK) {
		this.trioDetailBpkbPK = trioDetailBpkbPK;
	}

	public String getNoRangka() {
		return noRangka;
	}

	public void setNoRangka(String noRangka) {
		this.noRangka = noRangka;
	}

	public String getKdItem() {
		return kdItem;
	}

	public void setKdItem(String kdItem) {
		this.kdItem = kdItem;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public BigDecimal getBiaya() {
		return biaya;
	}

	public void setBiaya(BigDecimal biaya) {
		this.biaya = biaya;
	}

	public BigDecimal getCek() {
		return cek;
	}

	public void setCek(BigDecimal cek) {
		this.cek = cek;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getNoBukti() {
		return noBukti;
	}

	public void setNoBukti(String noBukti) {
		this.noBukti = noBukti;
	}

	public BigDecimal getBpkb() {
		return bpkb;
	}

	public void setBpkb(BigDecimal bpkb) {
		this.bpkb = bpkb;
	}

	public BigDecimal getLain() {
		return lain;
	}

	public void setLain(BigDecimal lain) {
		this.lain = lain;
	}

	public BigDecimal getkBpkb() {
		return kBpkb;
	}

	public void setkBpkb(BigDecimal kBpkb) {
		this.kBpkb = kBpkb;
	}

	public BigDecimal getkLain() {
		return kLain;
	}

	public void setkLain(BigDecimal kLain) {
		this.kLain = kLain;
	}

	public String getNoBuktiS() {
		return noBuktiS;
	}

	public void setNoBuktiS(String noBuktiS) {
		this.noBuktiS = noBuktiS;
	}

	public String getNoBuktiL() {
		return noBuktiL;
	}

	public void setNoBuktiL(String noBuktiL) {
		this.noBuktiL = noBuktiL;
	}

	public BigDecimal getStck() {
		return stck;
	}

	public void setStck(BigDecimal stck) {
		this.stck = stck;
	}

	public BigDecimal getkStck() {
		return kStck;
	}

	public void setkStck(BigDecimal kStck) {
		this.kStck = kStck;
	}

	public BigDecimal getbStck() {
		return bStck;
	}

	public void setbStck(BigDecimal bStck) {
		this.bStck = bStck;
	}

	public BigDecimal getbLain() {
		return bLain;
	}

	public void setbLain(BigDecimal bLain) {
		this.bLain = bLain;
	}

	public BigDecimal getbBpkb() {
		return bBpkb;
	}

	public void setbBpkb(BigDecimal bBpkb) {
		this.bBpkb = bBpkb;
	}

	public String getNoBuktiT() {
		return noBuktiT;
	}

	public void setNoBuktiT(String noBuktiT) {
		this.noBuktiT = noBuktiT;
	}
	
	public TrioHeaderBpkb getTrioHeaderBpkb() {
		return trioHeaderBpkb;
	}

	public void setTrioHeaderBpkb(TrioHeaderBpkb trioHeaderBpkb) {
		this.trioHeaderBpkb = trioHeaderBpkb;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((trioDetailBpkbPK == null) ? 0 : trioDetailBpkbPK.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioDetailBpkb other = (TrioDetailBpkb) obj;
		if (trioDetailBpkbPK == null) {
			if (other.trioDetailBpkbPK != null)
				return false;
		} else if (!trioDetailBpkbPK.equals(other.trioDetailBpkbPK))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioDetailBpkb [trioDetailBpkbPK=" + trioDetailBpkbPK + "]";
	}
	
	
}
