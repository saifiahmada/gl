package com.trio.gl.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.trio.gl.usertrail.TrioEntityUserTrail;

/** @author Saifi Ahmada Mar 7, 2013 10:59:13 AM  **/

@Entity
@Table(name="TRIO_MSTSUBPERKIRAAN") 
public class TrioMstsubperkiraan extends TrioEntityUserTrail implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private TrioMstsubperkiraanPK trioMstsubperkiraanPK;
	
	@Column(name="NAMA_SUB",length=35, nullable=false)
	private String namaSub;
	
	@Column(name="KET2", length=15)
	private String ket2;
	
	@Column(name="BUL_SUB0", precision=17, scale=2)
	private BigDecimal bulSub0;
	
	@Column(name="KET_SUB0", length=1)
	private String ketSub0;
	
	@Column(name="BUL_SUB1", precision=17, scale=2)
	private BigDecimal bulSub1;
	
	@Column(name="KET_SUB1", length=1)
	private String ketSub1;
	
	@Column(name="BUL_SUB2", precision=17, scale=2)
	private BigDecimal bulSub2;
	
	@Column(name="KET_SUB2", length=1)
	private String ketSub2;
	
	@Column(name="BUL_SUB3", precision=17, scale=2)
	private BigDecimal bulSub3;
	
	@Column(name="KET_SUB3", length=1)
	private String ketSub3;
	
	@Column(name="BUL_SUB4", precision=17, scale=2)
	private BigDecimal bulSub4;
	
	@Column(name="KET_SUB4", length=1)
	private String ketSub4;
	
	@Column(name="BUL_SUB5", precision=17, scale=2)
	private BigDecimal bulSub5;
	
	@Column(name="KET_SUB5", length=1)
	private String ketSub5;
	
	@Column(name="BUL_SUB6", precision=17, scale=2)
	private BigDecimal bulSub6;
	
	@Column(name="KET_SUB6", length=1)
	private String ketSub6;
	
	@Column(name="BUL_SUB7", precision=17, scale=2)
	private BigDecimal bulSub7;
	
	@Column(name="KET_SUB7", length=1)
	private String ketSub7;
	
	@Column(name="BUL_SUB8", precision=17, scale=2)
	private BigDecimal bulSub8;
	
	@Column(name="KET_SUB8", length=1)
	private String ketSub8;
	
	@Column(name="BUL_SUB9",	precision=17, scale=2)
	private BigDecimal bulSub9;
	
	@Column(name="KET_SUB9", length=1)
	private String ketSub9;
	
	@Column(name="BUL_SUB10",	precision=17, scale=2)
	private BigDecimal bulSub10;
	
	@Column(name="KET_SUB10", length=1)
	private String ketSub10;
	
	@Column(name="BUL_SUB11",	precision=17, scale=2)
	private BigDecimal bulSub11;
	
	@Column(name="KET_SUB11", length=1)
	private String ketSub11;
	
	@Column(name="BUL_SUB12",	precision=17, scale=2)
	private BigDecimal bulSub12;
	
	@Column(name="KET_SUB12", length=1)
	private String ketSub12;
	
	@Column(name="POKOK", precision=17, scale=2)
	private BigDecimal pokok;
	
	@Column(name="BUNGA", precision=17, scale=2)
	private BigDecimal bunga;
	
	@Column(name="TGL_AWAL")
	@Temporal(TemporalType.DATE)
	private Date tglAwal;
	
	@Column(name="TIPE", length=50)
	private String tipe;
	
	@Column(name="UANG_MUKA" ,precision=10, scale=0)
	private BigDecimal uangMuka;
	
	@Column(name="TGL_LUNAS")
	@Temporal(TemporalType.DATE)
	private Date tglLunas;
	
	@Column(name="KET", length=10)
	private String ket;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PERKIRAAN", referencedColumnName="ID_PERKIRAAN", updatable=false, insertable=false)
	private TrioMstperkiraan trioMstperkiraan;
	
	
	public TrioMstsubperkiraan() {
		this.trioMstsubperkiraanPK = new TrioMstsubperkiraanPK();
	}
	
	public TrioMstsubperkiraan(String idSub, String idPerkiraan) {
		this.trioMstsubperkiraanPK = new TrioMstsubperkiraanPK(idPerkiraan, idSub);
	}

	public TrioMstsubperkiraanPK getTrioMstsubperkiraanPK() {
		return trioMstsubperkiraanPK;
	}

	public void setTrioMstsubperkiraanPK(TrioMstsubperkiraanPK trioMstsubperkiraanPK) {
		this.trioMstsubperkiraanPK = trioMstsubperkiraanPK;
	}

	public String getNamaSub() {
		return namaSub;
	}

	public void setNamaSub(String namaSub) {
		this.namaSub = namaSub;
	}

	public String getKet2() {
		return ket2;
	}

	public void setKet2(String ket2) {
		this.ket2 = ket2;
	}

	public BigDecimal getBulSub0() {
		return bulSub0;
	}

	public void setBulSub0(BigDecimal bulSub0) {
		this.bulSub0 = bulSub0;
	}

	public String getKetSub0() {
		return ketSub0;
	}

	public void setKetSub0(String ketSub0) {
		this.ketSub0 = ketSub0;
	}

	public BigDecimal getBulSub1() {
		return bulSub1;
	}

	public void setBulSub1(BigDecimal bulSub1) {
		this.bulSub1 = bulSub1;
	}

	public String getKetSub1() {
		return ketSub1;
	}

	public void setKetSub1(String ketSub1) {
		this.ketSub1 = ketSub1;
	}

	public BigDecimal getBulSub2() {
		return bulSub2;
	}

	public void setBulSub2(BigDecimal bulSub2) {
		this.bulSub2 = bulSub2;
	}

	public String getKetSub2() {
		return ketSub2;
	}

	public void setKetSub2(String ketSub2) {
		this.ketSub2 = ketSub2;
	}

	public BigDecimal getBulSub3() {
		return bulSub3;
	}

	public void setBulSub3(BigDecimal bulSub3) {
		this.bulSub3 = bulSub3;
	}

	public String getKetSub3() {
		return ketSub3;
	}

	public void setKetSub3(String ketSub3) {
		this.ketSub3 = ketSub3;
	}

	public BigDecimal getBulSub4() {
		return bulSub4;
	}

	public void setBulSub4(BigDecimal bulSub4) {
		this.bulSub4 = bulSub4;
	}

	public String getKetSub4() {
		return ketSub4;
	}

	public void setKetSub4(String ketSub4) {
		this.ketSub4 = ketSub4;
	}

	public BigDecimal getBulSub5() {
		return bulSub5;
	}

	public void setBulSub5(BigDecimal bulSub5) {
		this.bulSub5 = bulSub5;
	}

	public String getKetSub5() {
		return ketSub5;
	}

	public void setKetSub5(String ketSub5) {
		this.ketSub5 = ketSub5;
	}

	public BigDecimal getBulSub6() {
		return bulSub6;
	}

	public void setBulSub6(BigDecimal bulSub6) {
		this.bulSub6 = bulSub6;
	}

	public String getKetSub6() {
		return ketSub6;
	}

	public void setKetSub6(String ketSub6) {
		this.ketSub6 = ketSub6;
	}

	public BigDecimal getBulSub7() {
		return bulSub7;
	}

	public void setBulSub7(BigDecimal bulSub7) {
		this.bulSub7 = bulSub7;
	}

	public String getKetSub7() {
		return ketSub7;
	}

	public void setKetSub7(String ketSub7) {
		this.ketSub7 = ketSub7;
	}

	public BigDecimal getBulSub8() {
		return bulSub8;
	}

	public void setBulSub8(BigDecimal bulSub8) {
		this.bulSub8 = bulSub8;
	}

	public String getKetSub8() {
		return ketSub8;
	}

	public void setKetSub8(String ketSub8) {
		this.ketSub8 = ketSub8;
	}

	public BigDecimal getBulSub9() {
		return bulSub9;
	}

	public void setBulSub9(BigDecimal bulSub9) {
		this.bulSub9 = bulSub9;
	}

	public String getKetSub9() {
		return ketSub9;
	}

	public void setKetSub9(String ketSub9) {
		this.ketSub9 = ketSub9;
	}

	public BigDecimal getBulSub10() {
		return bulSub10;
	}

	public void setBulSub10(BigDecimal bulSub10) {
		this.bulSub10 = bulSub10;
	}

	public String getKetSub10() {
		return ketSub10;
	}

	public void setKetSub10(String ketSub10) {
		this.ketSub10 = ketSub10;
	}

	public BigDecimal getBulSub11() {
		return bulSub11;
	}

	public void setBulSub11(BigDecimal bulSub11) {
		this.bulSub11 = bulSub11;
	}

	public String getKetSub11() {
		return ketSub11;
	}

	public void setKetSub11(String ketSub11) {
		this.ketSub11 = ketSub11;
	}

	public BigDecimal getBulSub12() {
		return bulSub12;
	}

	public void setBulSub12(BigDecimal bulSub12) {
		this.bulSub12 = bulSub12;
	}

	public String getKetSub12() {
		return ketSub12;
	}

	public void setKetSub12(String ketSub12) {
		this.ketSub12 = ketSub12;
	}

	public BigDecimal getPokok() {
		return pokok;
	}

	public void setPokok(BigDecimal pokok) {
		this.pokok = pokok;
	}

	public BigDecimal getBunga() {
		return bunga;
	}

	public void setBunga(BigDecimal bunga) {
		this.bunga = bunga;
	}

	public Date getTglAwal() {
		return tglAwal;
	}

	public void setTglAwal(Date tglAwal) {
		this.tglAwal = tglAwal;
	}

	public String getTipe() {
		return tipe;
	}

	public void setTipe(String tipe) {
		this.tipe = tipe;
	}

	public BigDecimal getUangMuka() {
		return uangMuka;
	}

	public void setUangMuka(BigDecimal uangMuka) {
		this.uangMuka = uangMuka;
	}

	public Date getTglLunas() {
		return tglLunas;
	}

	public void setTglLunas(Date tglLunas) {
		this.tglLunas = tglLunas;
	}

	public String getKet() {
		return ket;
	}

	public void setKet(String ket) {
		this.ket = ket;
	}

	public TrioMstperkiraan getTrioMstperkiraan() {
		return trioMstperkiraan;
	}

	public void setTrioMstperkiraan(TrioMstperkiraan trioMstperkiraan) {
		this.trioMstperkiraan = trioMstperkiraan;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((trioMstsubperkiraanPK == null) ? 0 : trioMstsubperkiraanPK
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioMstsubperkiraan other = (TrioMstsubperkiraan) obj;
		if (trioMstsubperkiraanPK == null) {
			if (other.trioMstsubperkiraanPK != null)
				return false;
		} else if (!trioMstsubperkiraanPK.equals(other.trioMstsubperkiraanPK))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioMstsubperkiraan [trioMstsubperkiraanPK="
				+ trioMstsubperkiraanPK + "]";
	}
	
}

