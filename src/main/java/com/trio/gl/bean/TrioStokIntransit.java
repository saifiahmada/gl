package com.trio.gl.bean;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.trio.gl.usertrail.TrioEntityUserTrail;

/** @author Saifi Ahmada Jul 11, 2013 9:41:16 AM  **/
@Entity
@Table(name="TRIO_STOK_INTRANSIT")
public class TrioStokIntransit extends TrioEntityUserTrail  {

	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private TrioStokIntransitPK trioStokIntransitPK;
	@Column(name="KD_DLR")
	private String kdDlr;
	
	@Column(name="QTY")
	private int qty;
	
	@Column(name="TGL_FAKTUR")
	@Temporal(TemporalType.DATE)
	private Date tglFaktur;
	
	@Column(name="NO_REF", length=20)
	private String noRef;
	
	@Column(name="STATUS", length=1)
	private String status;
	
	public TrioStokIntransit() {
	
	}
	
	public TrioStokIntransit(String noFaktur, String partNum) {
		this.trioStokIntransitPK = new TrioStokIntransitPK(noFaktur, partNum);
	}
	
	public String getKdDlr() {
		return kdDlr;
	}

	public void setKdDlr(String kdDlr) {
		this.kdDlr = kdDlr;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	public TrioStokIntransitPK getTrioStokIntransitPK() {
		return trioStokIntransitPK;
	}

	public void setTrioStokIntransitPK(TrioStokIntransitPK trioStokIntransitPK) {
		this.trioStokIntransitPK = trioStokIntransitPK;
	}

	public Date getTglFaktur() {
		return tglFaktur;
	}

	public void setTglFaktur(Date tglFaktur) {
		this.tglFaktur = tglFaktur;
	}

	public String getNoRef() {
		return noRef;
	}

	public void setNoRef(String noRef) {
		this.noRef = noRef;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((trioStokIntransitPK == null) ? 0 : trioStokIntransitPK
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioStokIntransit other = (TrioStokIntransit) obj;
		if (trioStokIntransitPK == null) {
			if (other.trioStokIntransitPK != null)
				return false;
		} else if (!trioStokIntransitPK.equals(other.trioStokIntransitPK))
			return false;
		return true;
	}
	

}

