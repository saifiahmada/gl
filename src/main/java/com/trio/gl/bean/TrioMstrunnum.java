package com.trio.gl.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.trio.gl.usertrail.TrioEntityUserTrail;

/** @author Saifi Ahmada Mar 14, 2013 4:30:14 PM  **/

@Entity
@Table(name="TRIO_MSTRUNNUM")
public class TrioMstrunnum extends TrioEntityUserTrail implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private TrioMstrunnumPK trioMstrunnumPK;
	
	@Column(name="DESCRIPTION", length=30)
	private String description;
	
	@Column(name="RUNNUM", length=11)
	private int runnum;
	
	public TrioMstrunnum() {
		this.trioMstrunnumPK = new TrioMstrunnumPK();
	}
	
	public TrioMstrunnum(String idDoc, String reseter) {
		this.trioMstrunnumPK = new TrioMstrunnumPK(idDoc, reseter);
	}

	public TrioMstrunnumPK getTrioMstrunnumPK() {
		return trioMstrunnumPK;
	}

	public void setTrioMstrunnumPK(TrioMstrunnumPK trioMstrunnumPK) {
		this.trioMstrunnumPK = trioMstrunnumPK;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getRunnum() {
		return runnum;
	}

	public void setRunnum(int runnum) {
		this.runnum = runnum;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((trioMstrunnumPK == null) ? 0 : trioMstrunnumPK.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioMstrunnum other = (TrioMstrunnum) obj;
		if (trioMstrunnumPK == null) {
			if (other.trioMstrunnumPK != null)
				return false;
		} else if (!trioMstrunnumPK.equals(other.trioMstrunnumPK))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioMstrunnum [trioMstrunnumPK=" + trioMstrunnumPK + "]";
	}
	
}

