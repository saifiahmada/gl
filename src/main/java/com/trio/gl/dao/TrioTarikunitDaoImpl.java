package com.trio.gl.dao;

import java.util.List;

import org.hibernate.SQLQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trio.gl.bean.TrioTarikunit;
import com.trio.gl.hibernate.TrioHibernateDaoSupport;

/** @author Saifi Ahmada Mar 23, 2013 10:03:08 PM  **/

@Service
public class TrioTarikunitDaoImpl extends TrioHibernateDaoSupport implements TrioTarikunitDao {

	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<TrioTarikunit> getListUnitOracle(String kdDlr, String tgl) {
		// TODO , masbro
		String sQ = 
					" SELECT "+  
					" 	b.kd_dlr, "+
					" 	a.tgl_sj,  "+
					" 	a.no_sj,  "+
					" 	b.tgl_fak,  "+
					" 	b.no_faktur,  "+
					" 	c.no_mesin,  "+
					" 	c.no_rangka,  "+
					" 	c.kd_item,  "+
					" 	e.ket_1,  "+
					" 	d.harga_std,  "+
					" 	d.discount "+
					" FROM  "+
					" 	honda_h100_sjalans a,  "+
					" 	honda_h100_fakdos b,  "+
					" 	honda_h100_dtlsjalans c,  "+
					" 	honda_h100_dtlfakdos d, "+ 
					" 	honda_h000_items e "+
					" WHERE  "+
					" 	c.kd_item=e.kd_item and  "+
					" 	a.no_sj=c.sjalan_no_sj and "+ 
					" 	a.no_do=b.no_do and  "+
					" 	b.no_do=d.fakdo_no_do and  "+
					" 	c.kd_item=d.kd_item and  "+
					" 	a.status<>'X' and b.status<>'X' "+ 
					" 	and b.kd_dlr= '"+kdDlr+"'  "+
					" 	and	a.tgl_sj= TO_DATE('"+tgl+"','YYYY-MM-DD') ";
		
		System.out.println("Tanggal "+tgl);
		System.out.println("Kd Dlr "+kdDlr);
		System.out.println("Query"+sQ);
		
		SQLQuery sqlQ = getHibernateTemplate().getSessionFactory().getCurrentSession()
						.createSQLQuery(sQ);
		sqlQ.addEntity(TrioTarikunit.class);
		List<TrioTarikunit> list = sqlQ.list();
		
		return list;
	}
	
}

