package com.trio.gl.dao;

import java.util.List;

import com.trio.gl.bean.TrioStokunit;
import com.trio.gl.hibernate.TrioGenericDao;

/** @author Saifi Ahmada Mar 23, 2013 3:49:35 PM  **/

public interface TrioStokunitDao extends TrioGenericDao<TrioStokunit> {
	public void saveList(List<TrioStokunit> list, String user);
}

