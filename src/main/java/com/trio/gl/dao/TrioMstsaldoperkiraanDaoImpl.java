package com.trio.gl.dao;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trio.gl.bean.TrioMstsaldoperkiraan;
import com.trio.gl.hibernate.TrioHibernateDaoSupport;
import com.trio.gl.util.TrioDateUtil;

/** @author Saifi Ahmada Apr 16, 2013 9:23:45 PM  **/

@Service
public class TrioMstsaldoperkiraanDaoImpl extends TrioHibernateDaoSupport implements TrioMstsaldoperkiraanDao {

	@Transactional(readOnly=false)
	public void saveOrUpdate(TrioMstsaldoperkiraan domain, String user) {
		// TODO , masbro
		TrioMstsaldoperkiraan objDB = (TrioMstsaldoperkiraan) getHibernateTemplate().getSessionFactory().getCurrentSession()
				.get(TrioMstsaldoperkiraan.class, domain.getTrioMstsaldoperkiraanPK());
		if (objDB == null){
			
			save(domain, user); 
		} else {
			
			update(domain, user);
		}

	}

	@Transactional(readOnly=false)
	public void save(TrioMstsaldoperkiraan domain, String user) {
		
		domain.getUserTrailing().setDcrea(TrioDateUtil.getLongSysDate());
		domain.getUserTrailing().setVcreaby(user);
		getHibernateTemplate().getSessionFactory().getCurrentSession().save(domain);
		
	}

	@Transactional(readOnly=false)
	public void update(TrioMstsaldoperkiraan domain, String user) {
		// TODO , masbro
		domain.getUserTrailing().setDmodi(TrioDateUtil.getLongSysDate());
		domain.getUserTrailing().setVmodiby(user);
		getHibernateTemplate().getSessionFactory().getCurrentSession().merge(domain);


	}

	public void delete(TrioMstsaldoperkiraan domain) {
		// TODO , masbro


	}

	public List<TrioMstsaldoperkiraan> findAll() {
		// TODO , masbro

		return null;
	}

	public List<TrioMstsaldoperkiraan> findByExample(
			TrioMstsaldoperkiraan domain) {
		// TODO , masbro

		return null;
	}

	public List<TrioMstsaldoperkiraan> findByCriteria(
			TrioMstsaldoperkiraan domain) {
		// TODO , masbro

		return null;
	}

	@Transactional(readOnly=true)
	public TrioMstsaldoperkiraan findByPrimaryKey(TrioMstsaldoperkiraan domain) {
		// TODO , masbro
		String sQ = "select s from TrioMstsaldoperkiraan s where s.trioMstsaldoperkiraanPK.idPerkiraan = :idPerkiraan and "
				+ " s.trioMstsaldoperkiraanPK.tahun = :tahun  ";
		return (TrioMstsaldoperkiraan) getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(sQ)
				.setString("idPerkiraan", domain.getTrioMstsaldoperkiraanPK().getIdPerkiraan())
				.setString("tahun", domain.getTrioMstsaldoperkiraanPK().getTahun()).uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<TrioMstsaldoperkiraan> getListSaldoPerkiraanByTahun(String tahun) {
		String sQ = "from TrioMstsaldoperkiraan s where s.trioMstsaldoperkiraanPK.tahun = :tahun and s.trioMstperkiraan.level = 4 ";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(sQ);
		q.setParameter("tahun", tahun);
		return q.list();
	}
}

