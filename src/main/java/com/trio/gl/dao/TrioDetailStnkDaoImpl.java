package com.trio.gl.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trio.gl.bean.TrioDetailStnk;
import com.trio.gl.hibernate.TrioHibernateDaoSupport;

/**
 * @author Gusti Arya 11:08:11 AM Jul 16, 2013
 */

@Service
public class TrioDetailStnkDaoImpl extends TrioHibernateDaoSupport implements TrioDetailStnkDao {

	public void saveOrUpdate(TrioDetailStnk domain, String user) {
		// TODO Auto-generated method stub

	}

	public void save(TrioDetailStnk domain, String user) {
		// TODO Auto-generated method stub

	}

	public void update(TrioDetailStnk domain, String user) {
		// TODO Auto-generated method stub

	}

	public void delete(TrioDetailStnk domain) {
		// TODO Auto-generated method stub

	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<TrioDetailStnk> findAll() {
		return (List<TrioDetailStnk>) getHibernateTemplate()
				.getSessionFactory().getCurrentSession().createQuery("from TrioDetailStnk");
	}

	public List<TrioDetailStnk> findByExample(TrioDetailStnk domain) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<TrioDetailStnk> findByCriteria(TrioDetailStnk domain) {
		// TODO Auto-generated method stub
		return null;
	}

	public TrioDetailStnk findByPrimaryKey(TrioDetailStnk domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<TrioDetailStnk> findbyStatus(String status, Date tglAwal, Date tglAkhir) {
		String hql = "select a from TrioDetailStnk a, TrioHeaderStnk b " +
				"where a.trioDetailStnkPK.noMhn = b.noMhn " +
				"and a.status = :status " +
				"and b.flag <> 'X' " +
				"and b.tgl between :tglAwal and :tglAkhir ";

		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		q.setString("status", status);
		q.setDate("tglAwal", tglAwal);
		q.setDate("tglAkhir", tglAkhir);
		return q.list();

	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<TrioDetailStnk> findByStatusX(String status, Date tglAwal,
			Date tglAkhir) {
		String hql = "select a from TrioDetailStnk a, TrioHeaderStnk b " +
				"where a.trioDetailStnkPK.noMhn = b.noMhn " +
				"and (a.status = :status or b.flag = 'X') " +
				"and b.tgl between :tglAwal and :tglAkhir ";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		q.setString("status", status);
		q.setDate("tglAwal", tglAwal);
		q.setDate("tglAkhir", tglAkhir);
		return q.list();
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<TrioDetailStnk> findByStatusA(String status, Date tglAwal,
			Date tglAkhir) {

		String hql = "select a from TrioDetailStnk a, TrioHeaderStnk b " +
				"where a.trioDetailStnkPK.noMhn = b.noMhn " +
				"and (a.status = :status or a.status is null) " +
				"and b.flag <> 'X' " +
				"and b.tgl between :tglAwal and :tglAkhir ";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		q.setString("status", status);
		q.setDate("tglAwal", tglAwal);
		q.setDate("tglAkhir", tglAkhir);
		return q.list();

	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<TrioDetailStnk> getMohonKreditBiaya(String noMesin) {
		Criteria criteria = getHibernateTemplate().getSessionFactory()
				.getCurrentSession().createCriteria(TrioDetailStnk.class);
		criteria.add(Restrictions.not(Restrictions.eq("status", "X")));
		criteria.add(Restrictions.eq("trioDetailStnkPK.noMesin", noMesin));

		return criteria.list();
	}

	//Dipanggil di Class RptHutangStnkVm
	@Transactional(readOnly=true)
	public String getNoUrus(String noMesin) {
		Criteria crit = getHibernateTemplate().getSessionFactory()
				.getCurrentSession().createCriteria(TrioDetailStnk.class);
		crit.add(Restrictions.not(Restrictions.eq("status", "X")));
		crit.add(Restrictions.eq("trioDetailStnkPK.noMesin", noMesin));
		crit.setProjection(Projections.property("trioDetailStnkPK.noMhn"));
		return (String) crit.uniqueResult();
	}

	//Dipanggil di Class RptHutangStnkVm
	@Transactional(readOnly=true)
	public Date getTglUrus(String noMesin) {
		Criteria crit = getHibernateTemplate().getSessionFactory()
				.getCurrentSession().createCriteria(TrioDetailStnk.class);
		crit.add(Restrictions.not(Restrictions.eq("status", "X")));
		crit.add(Restrictions.eq("trioDetailStnkPK.noMesin", noMesin));
		crit.setProjection(Projections.property("tglKrdt"));
		return (Date) crit.uniqueResult();
	}

	//Dipanggil di kelas RptHutangStnkVM
	@Transactional(readOnly=true)
	public BigDecimal getPengStnk(String noMesin) {
		Criteria crit = getHibernateTemplate().getSessionFactory()
				.getCurrentSession().createCriteria(TrioDetailStnk.class);
		crit.add(Restrictions.not(Restrictions.eq("status", "X")));
		crit.add(Restrictions.eq("trioDetailStnkPK.noMesin", noMesin));
		crit.setProjection(Projections.property("biaya"));
		return (BigDecimal) crit.uniqueResult();
	}

	//Di panggil di kelas RptPiutangStnkVM
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<TrioDetailStnk> findByStatusNotX(Date tglAwal,
			Date tglAkhir, boolean cb) {
		String hql;
		
		if(cb==true){
			hql = "select new TrioDetailStnk(t.tglPinj, t.trioDetailStnkPK.noMhn, t.tglFak, " 
					+"t.noFak, t.trioDetailStnkPK.noMesin, t.nama, coalesce(t.bbnkb,0), coalesce(t.pkb,0), " 
					+"coalesce(t.swdkllj,0), coalesce(t.lain,0), coalesce(t.bbnkbR,0), coalesce(t.pkbR,0), "
					+"coalesce(t.swdklljR,0), t.tglKrdt) from TrioDetailStnk t "
					+ "where t.status <> 'X' " 
					+ "and t.tglKrdt is null " 
					+ "and t.tglPinj between :tglAwal and :tglAkhir";
		}else{
			hql = "select new TrioDetailStnk(t.tglPinj, t.trioDetailStnkPK.noMhn, t.tglFak, " 
					+"t.noFak, t.trioDetailStnkPK.noMesin, t.nama, coalesce(t.bbnkb,0), coalesce(t.pkb,0), " 
					+"coalesce(t.swdkllj,0), coalesce(t.lain,0), coalesce(t.bbnkbR,0), coalesce(t.pkbR,0), "
					+"coalesce(t.swdklljR,0), t.tglKrdt) from TrioDetailStnk t "
					+ "where t.status <> 'X' " 
					+ "and t.tglPinj between :tglAwal and :tglAkhir";
		}
		
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		q.setDate("tglAwal", tglAwal);
		q.setDate("tglAkhir", tglAkhir);

		return q.list();
	}
}
