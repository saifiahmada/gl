package com.trio.gl.dao;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trio.gl.bean.TrioRmsNeracaAktivaSub;
import com.trio.gl.hibernate.TrioHibernateDaoSupport;

/**
 * @author Gusti Arya 9:42:35 AM Nov 4, 2013
 */

@Service
public class TrioRmsNeracaAktivaSubDaoImpl extends TrioHibernateDaoSupport implements TrioRmsNeracaAktivaSubDao {

	@Override
	public void saveOrUpdate(TrioRmsNeracaAktivaSub domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void save(TrioRmsNeracaAktivaSub domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(TrioRmsNeracaAktivaSub domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(TrioRmsNeracaAktivaSub domain) {
		// TODO Auto-generated method stub
		
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<TrioRmsNeracaAktivaSub> findAll() {
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery("from TrioRmsNeracaAktivaSub");
		return q.list();
	}

	@Override
	public List<TrioRmsNeracaAktivaSub> findByExample(
			TrioRmsNeracaAktivaSub domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TrioRmsNeracaAktivaSub> findByCriteria(
			TrioRmsNeracaAktivaSub domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TrioRmsNeracaAktivaSub findByPrimaryKey(TrioRmsNeracaAktivaSub domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional(readOnly=false)
	public void saveByList(List<TrioRmsNeracaAktivaSub> lists, String username) {
		String hql = "delete from TrioRmsNeracaAktivaSub";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		q.executeUpdate();
		
		for(TrioRmsNeracaAktivaSub current : lists){
			getHibernateTemplate().getSessionFactory().getCurrentSession().persist(current);
		}
		
	}

	@Override
	@Transactional(readOnly=false)
	public void saveOrUpdateByList(List<TrioRmsNeracaAktivaSub> lists,
			String username) {
		for(TrioRmsNeracaAktivaSub current : lists){
			getHibernateTemplate().getSessionFactory().getCurrentSession().saveOrUpdate(current);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<TrioRmsNeracaAktivaSub> getSumByHasil() {
		String hql = "select new TrioRmsNeracaAktivaSub(t.hasil, sum(t.nilai)) from TrioRmsNeracaAktivaSub t "
				+ "group by t.hasil";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		
		return q.list();
	}
}
