package com.trio.gl.dao;

import java.util.List;

import com.trio.gl.bean.TrioRmsNeracaAktiva;
import com.trio.gl.hibernate.TrioGenericDao;

/**
 * @author Gusti Arya 10:10:37 AM Oct 31, 2013
 */

public interface TrioRmsNeracaAktivaDao extends TrioGenericDao<TrioRmsNeracaAktiva>{
	public void saveByList(List<TrioRmsNeracaAktiva> lists, String username);
	public void saveOrUpdateByList(List<TrioRmsNeracaAktiva> lists, String userName);
	public List<TrioRmsNeracaAktiva> getSumByKdHsl();
}
