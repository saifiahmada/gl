package com.trio.gl.dao;

import java.util.List;
import java.util.Vector;

import com.trio.gl.bean.TrioStokIntransit;
import com.trio.gl.hibernate.TrioGenericDao;

/** @author Saifi Ahmada Jul 15, 2013 10:38:12 AM  **/

public interface TrioStokIntransitDao extends TrioGenericDao<TrioStokIntransit> {
	public void saveAll(List<TrioStokIntransit> list, String user);
	public String prosesUploadFile(Vector<String[]> vectorString,String kdDlr, String noCabang, String user);
}

