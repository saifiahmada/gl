package com.trio.gl.dao;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trio.gl.bean.TrioMstuserrole;
import com.trio.gl.hibernate.TrioHibernateDaoSupport;
import com.trio.gl.util.TrioDateUtil;

/** @author Saifi Ahmada Feb 8, 2013 4:14:31 PM  **/

@Service
public class TrioMstuserroleDaoImpl extends TrioHibernateDaoSupport implements TrioMstuserroleDao {

	@Transactional(readOnly=false)
	public void saveOrUpdate(TrioMstuserrole domain, String user) {
		// TODO , masbro

		TrioMstuserrole userRole = (TrioMstuserrole) getHibernateTemplate().getSessionFactory().getCurrentSession().get(TrioMstuserrole.class, domain.getTrioMstuserrolePK());
			
			if (userRole == null){
				domain.getUserTrailing().setDcrea(TrioDateUtil.getLongSysDate());
				domain.getUserTrailing().setVcreaby(user);
				getHibernateTemplate().save(domain);
				System.out.println("insert suskse");
			} else {
				domain.getUserTrailing().setDmodi(TrioDateUtil.getLongSysDate());
				domain.getUserTrailing().setVmodiby(user);
				getHibernateTemplate().update(domain);
				System.out.println("update suskses");
			}
		
	}

	@Transactional(readOnly=false)
	public void save(TrioMstuserrole domain, String user) {
		// TODO , masbro
		domain.getUserTrailing().setDcrea(TrioDateUtil.getLongSysDate());
		domain.getUserTrailing().setVcreaby(user);
		getHibernateTemplate().save(domain);
		getHibernateTemplate().flush();
	}
	@Transactional(readOnly=false)
	public void update(TrioMstuserrole domain, String user) {
		// TODO , masbro
		domain.getUserTrailing().setVmodiby(user);
		domain.getUserTrailing().setDmodi(TrioDateUtil.getLongSysDate());
		getHibernateTemplate().update(domain);
		getHibernateTemplate().flush();
	}

	public void delete(TrioMstuserrole domain) {
		// TODO , masbro
		
		
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<TrioMstuserrole> findAll() {
		// TODO , masbro
		return getHibernateTemplate().find("from TrioMstuserrole");
	}

	@Transactional(readOnly=true)
	public List<TrioMstuserrole> findByExample(TrioMstuserrole domain) {
		// TODO , masbro
		
		return null;
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<TrioMstuserrole> findByCriteria(TrioMstuserrole domain) {
		// TODO , masbro
		DetachedCriteria c = DetachedCriteria.forClass(TrioMstuserrole.class);
		if (domain.getTrioMstuserrolePK().getVroleid() != null){
			c = c.add(Restrictions.eq("trioMstuserrolePK.vroleid", domain.getTrioMstuserrolePK().getVroleid()));
		}
		if (domain.getTrioMstuserrolePK().getVusername() != null){
			c = c.add(Restrictions.eq("trioMstuserrolePK.vusername", domain.getTrioMstuserrolePK().getVusername()));
		}
		if (domain.getVstat() != null){
			c = c.add(Restrictions.eq("vstat", domain.getVstat()));
		}
		return getHibernateTemplate().findByCriteria(c);
	}

	@Transactional(readOnly=true)
	public TrioMstuserrole findByPrimaryKey(TrioMstuserrole domain) {
		// TODO , masbro
		
		return null;
	}

}

