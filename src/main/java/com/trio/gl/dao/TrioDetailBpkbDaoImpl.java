package com.trio.gl.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trio.gl.bean.TrioDetailBpkb;
import com.trio.gl.hibernate.TrioHibernateDaoSupport;

/**
 * @author Gusti Arya 1:52:38 PM Jul 17, 2013
 */

@Service
public class TrioDetailBpkbDaoImpl extends TrioHibernateDaoSupport implements TrioDetailBpkbDao {

	public void saveOrUpdate(TrioDetailBpkb domain, String user) {
		// TODO Auto-generated method stub

	}

	public void save(TrioDetailBpkb domain, String user) {
		// TODO Auto-generated method stub

	}

	public void update(TrioDetailBpkb domain, String user) {
		// TODO Auto-generated method stub

	}

	public void delete(TrioDetailBpkb domain) {
		// TODO Auto-generated method stub

	}

	public List<TrioDetailBpkb> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	public List<TrioDetailBpkb> findByExample(TrioDetailBpkb domain) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<TrioDetailBpkb> findByCriteria(TrioDetailBpkb domain) {
		// TODO Auto-generated method stub
		return null;
	}

	public TrioDetailBpkb findByPrimaryKey(TrioDetailBpkb domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<TrioDetailBpkb> findByStatusA(String status, Date tglAwal,
			Date tglAkhir) {

		String hql = "select a from TrioDetailBpkb a, TrioHeaderBpkb b " +
				"where a.trioDetailBpkbPK.noMhn = b.noMhn " +
				"and (a.status = :status or a.status is null) " +
				"and b.flag <> 'X' " +
				"and b.tgl between :tglAwal and :tglAkhir ";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		q.setString("status", status);
		q.setDate("tglAwal", tglAwal);
		q.setDate("tglAkhir", tglAkhir);
		return q.list();

	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<TrioDetailBpkb> findByStatusX(String status, Date tglAwal,
			Date tglAkhir) {
		String hql = "select a from TrioDetailBpkb a, TrioHeaderBpkb b " +
				"where a.trioDetailBpkbPK.noMhn = b.noMhn " +
				"and (a.status = :status or b.flag = 'X') " +
				"and b.tgl between :tglAwal and :tglAkhir ";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		q.setString("status", status);
		q.setDate("tglAwal", tglAwal);
		q.setDate("tglAkhir", tglAkhir);
		return q.list();
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<TrioDetailBpkb> findByStatus(String status, Date tglAwal,
			Date tglAkhir) {
		String hql = "select a from TrioDetailBpkb a, TrioHeaderBpkb b " +
				"where a.trioDetailBpkbPK.noMhn = b.noMhn " +
				"and a.status = :status " +
				"and b.flag <> 'X' " +
				"and b.tgl between :tglAwal and :tglAkhir ";

		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		q.setString("status", status);
		q.setDate("tglAwal", tglAwal);
		q.setDate("tglAkhir", tglAkhir);
		return q.list();
	}

}
