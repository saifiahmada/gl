package com.trio.gl.dao;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trio.gl.bean.TrioRumus;
import com.trio.gl.hibernate.TrioHibernateDaoSupport;

/**
 * @author Gusti Arya 11:27:36 AM Dec 14, 2013
 */

@Service
public class TrioRumusDaoImpl extends TrioHibernateDaoSupport implements TrioRumusDao {

	@Override
	public void saveOrUpdate(TrioRumus domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void save(TrioRumus domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	@Transactional(readOnly=false)
	public void update(TrioRumus domain, String user) {
		getHibernateTemplate().getSessionFactory().getCurrentSession().update(domain);
	}

	@Override
	public void delete(TrioRumus domain) {
		// TODO Auto-generated method stub
		
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<TrioRumus> findAll() {
		String hql = "from TrioRumus";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		return q.list();
	}

	@Override
	public List<TrioRumus> findByExample(TrioRumus domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TrioRumus> findByCriteria(TrioRumus domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TrioRumus findByPrimaryKey(TrioRumus domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional(readOnly=false)
	public void saveByList(List<TrioRumus> list, String user) {
		String hql = "delete from TrioRumus";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		q.executeUpdate();
		
		for(TrioRumus current : list){
			getHibernateTemplate().getSessionFactory().getCurrentSession().save(current);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<TrioRumus> listSumNilai() {
		String hql = "select new TrioRumus(t.kdHsl, sum(t.nilai)) from TrioRumus t group by t.kdHsl";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		
		return q.list();
	}

	@Override
	@Transactional(readOnly=false)
	public void saveOrUpdateByList(List<TrioRumus> list, String user) {
		for(TrioRumus current : list){
			getHibernateTemplate().getSessionFactory().getCurrentSession().saveOrUpdate(current);
		}
		
	}
}
