package com.trio.gl.dao;

import java.util.List;

import com.trio.gl.bean.TrioTarikunit;

/** @author Saifi Ahmada Mar 23, 2013 10:02:58 PM  **/

public interface TrioTarikunitDao {
	
	public List<TrioTarikunit> getListUnitOracle(String kdDlr, String tgl);
	
}

