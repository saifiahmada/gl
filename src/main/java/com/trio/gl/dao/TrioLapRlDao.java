package com.trio.gl.dao;

import java.util.List;

import com.trio.gl.bean.TrioLapRl;
import com.trio.gl.hibernate.TrioGenericDao;

public interface TrioLapRlDao extends TrioGenericDao<TrioLapRl>{
	
	public List<TrioLapRl> findByPeriode(String periode);
	public void saveByList(List<TrioLapRl> list);

}
