package com.trio.gl.dao;

import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trio.gl.bean.TrioMstkonfigcabang;
import com.trio.gl.hibernate.TrioHibernateDaoSupport;
import com.trio.gl.util.TrioDateUtil;

/** @author Saifi Ahmada Apr 3, 2013 12:05:12 PM  **/

@Service
public class TrioMstkonfigcabangDaoImpl extends TrioHibernateDaoSupport implements TrioMstkonfigcabangDao {

	@Transactional(readOnly=false)
	public void saveOrUpdate(TrioMstkonfigcabang domain, String user) {
		// TODO , masbro
		TrioMstkonfigcabang entity = (TrioMstkonfigcabang) getHibernateTemplate().getSessionFactory().getCurrentSession()
				.get(TrioMstkonfigcabang.class, domain.getTrioMstkonfigcabangPK());
		if (entity == null){
			domain.getUserTrailing().setVcreaby(user);
			domain.getUserTrailing().setDcrea(TrioDateUtil.getLongSysDate());
			getHibernateTemplate().save(domain);
			System.out.println("insert TrioMstkonfigcabang suskes");
		} else {
			domain.getUserTrailing().setVmodiby(user);
			domain.getUserTrailing().setDmodi(TrioDateUtil.getLongSysDate());
			getHibernateTemplate().merge(domain);
			System.out.println("update TrioMstkonfigcabang sukses");
		}
			
		
	}

	public void save(TrioMstkonfigcabang domain, String user) {
		// TODO , masbro
		
		
	}

	public void update(TrioMstkonfigcabang domain, String user) {
		// TODO , masbro
		
		
	}

	public void delete(TrioMstkonfigcabang domain) {
		// TODO , masbro
		
		
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<TrioMstkonfigcabang> findAll() {
		// TODO , masbro
		
		return getHibernateTemplate().find("from TrioMstkonfigcabang");
	}

	public List<TrioMstkonfigcabang> findByExample(TrioMstkonfigcabang domain) {
		// TODO , masbro
		
		return null;
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly=false)
	public List<TrioMstkonfigcabang> findByCriteria(TrioMstkonfigcabang domain) {
		// TODO , masbro
		DetachedCriteria c = DetachedCriteria.forClass(TrioMstkonfigcabang.class);
		if (domain.getTrioMstkonfigcabangPK().getKdDlr() != null){
			c = c.add(Restrictions.eq("trioMstkonfigcabangPK.kdDlr", domain.getTrioMstkonfigcabangPK().getKdDlr()));
		}
		if (domain.getTrioMstkonfigcabangPK().getContext() != null){
			c = c.add(Restrictions.eq("trioMstkonfigcabangPK.context", domain.getTrioMstkonfigcabangPK().getContext()));
		}
		if (domain.getNoCabang() != null){
			c = c.add(Restrictions.eq("noCabang", domain.getNoCabang()));
		}
		return getHibernateTemplate().findByCriteria(c); 
	}

	public TrioMstkonfigcabang findByPrimaryKey(TrioMstkonfigcabang domain) {
		// TODO , masbro
		
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true) 
	public List<TrioMstkonfigcabang> getListCabangByContext(String context){
		String sQ = "select * from trio_mstkonfigcabang where CONTEXT  = '"+context+"'";
		//Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(sQ);
		SQLQuery sQuery = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(sQ);
		sQuery.addEntity(TrioMstkonfigcabang.class);
		return sQuery.list();
	}

}

