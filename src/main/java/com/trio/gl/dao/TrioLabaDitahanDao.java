package com.trio.gl.dao;

import java.util.List;

import com.trio.gl.bean.TrioLabaDitahan;
import com.trio.gl.hibernate.TrioGenericDao;

/**
 * @author Gusti Arya 10:27:12 AM Sep 30, 2013
 */

public interface TrioLabaDitahanDao extends TrioGenericDao<TrioLabaDitahan>{
	
	public List<TrioLabaDitahan> findListbyKdDlr(String kdDlr);

}
