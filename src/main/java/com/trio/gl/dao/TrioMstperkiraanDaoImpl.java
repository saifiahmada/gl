package com.trio.gl.dao;

import java.math.BigDecimal;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.zkoss.zhtml.Messagebox;

import com.trio.gl.bean.TrioMstperkiraan;
import com.trio.gl.bean.TrioMstperkiraanPK;
import com.trio.gl.hibernate.TrioHibernateDaoSupport;
import com.trio.gl.util.TrioDateUtil;

/** @author Saifi Ahmada Mar 6, 2013 4:58:40 PM  **/

@Service
public class TrioMstperkiraanDaoImpl extends TrioHibernateDaoSupport implements TrioMstperkiraanDao {

	@Transactional(readOnly=false)
	public void saveOrUpdate(TrioMstperkiraan domain, String user) {
		// TODO , masbro
		TrioMstperkiraan entity = (TrioMstperkiraan) getHibernateTemplate().getSessionFactory().getCurrentSession().get(TrioMstperkiraan.class, domain.getTrioMstperkiraanPK());
		
		if (entity == null){
			domain.getUserTrailing().setVcreaby(user);
			domain.getUserTrailing().setDcrea(TrioDateUtil.getLongSysDate());
			getHibernateTemplate().save(domain);
			getHibernateTemplate().flush();
			System.out.println("insert TrioMstperkiraan");
		}else{
			domain.getUserTrailing().setVmodiby(user);
			domain.getUserTrailing().setDmodi(TrioDateUtil.getLongSysDate());
			getHibernateTemplate().merge(domain);
			getHibernateTemplate().flush();
			
			System.out.println("update TrioMstperkiraan");
		}
		
	}

	public void save(TrioMstperkiraan domain, String user) {
		// TODO , masbro
		
		
	}

	public void update(TrioMstperkiraan domain, String user) {
		// TODO , masbro
		
		
	}

	public void delete(TrioMstperkiraan domain) {
		// TODO , masbro
		
		
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<TrioMstperkiraan> findAll() {
		// TODO , masbro
		
		return getHibernateTemplate().find("from TrioMstperkiraan");
	}

	public List<TrioMstperkiraan> findByExample(TrioMstperkiraan domain) {
		// TODO , masbro
		
		return null;
	}

	@SuppressWarnings("unchecked")
	public List<TrioMstperkiraan> findByCriteria(TrioMstperkiraan domain) {
		// TODO , masbro
		DetachedCriteria c = DetachedCriteria.forClass(TrioMstperkiraan.class);
		if (domain.getTrioMstperkiraanPK().getIdPerkiraan() != null){
			c = c.add(Restrictions.eq("trioMstperkiraanPK.idPerkiraan", domain.getTrioMstperkiraanPK().getIdPerkiraan()));
		}
		if (domain.getNamaPerkiraan() != null){
			c = c.add(Restrictions.sqlRestriction("upper(nama_perkiraan) like ('%"+domain.getNamaPerkiraan().toUpperCase()+"%') "));
		}
		if (domain.getSub() != null){
			c = c.add(Restrictions.eq("sub", domain.getSub()));
		}
//		if (String.valueOf(domain.getLevel()) != null){
//			c = c.add(Restrictions.eq("level", domain.getLevel()));
//		}
		if (domain.getJenis() != null) {
			c = c.add(Restrictions.eq("jenis", domain.getJenis()));
		}
		return getHibernateTemplate().findByCriteria(c);
	}

	@Transactional(readOnly=true)
	public TrioMstperkiraan findByPrimaryKey(TrioMstperkiraan domain) {
		
		String sQ = "select p from TrioMstperkiraan p where p.trioMstperkiraanPK.idPerkiraan =  :idPerkiraan ";
		return (TrioMstperkiraan) getHibernateTemplate().getSessionFactory()
				.getCurrentSession().createQuery(sQ).setString("idPerkiraan", 
						domain.getTrioMstperkiraanPK().getIdPerkiraan()).uniqueResult();
	
	}

	@Transactional(readOnly=true)
	public String findNamaById(String idPerkiraan) {
		String namaPerkiraan = null;
		try {
			TrioMstperkiraan perkiraan = (TrioMstperkiraan) getHibernateTemplate().getSessionFactory().getCurrentSession().get(TrioMstperkiraan.class, new TrioMstperkiraanPK(idPerkiraan));
			namaPerkiraan =  perkiraan.getNamaPerkiraan();
		} catch (NullPointerException e) {
			Messagebox.show("ID Perkiraaan "+ idPerkiraan +" Tidak Ditemukan ");
		}
		return namaPerkiraan;
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<TrioMstperkiraan> findByLevelAndSub(String level, String sub) {
		
		String hql = "from TrioMstperkiraan where level = '4' and sub = 'Y'";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		List<TrioMstperkiraan> lists = q.list();
		
		return lists;
	}

	@Override
	@Transactional(readOnly=true)
	public BigDecimal getPenjualanSmh(String bulan, String noPerk) {
		String hql = "select " +bulan+" from TrioMstperkiraan a where a.trioMstperkiraanPK.idPerkiraan = :noPerk";
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		query.setString("noPerk", noPerk);
		
		return (BigDecimal) query.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<TrioMstperkiraan> getIdPerkiraanPiutang() {
		String hql = "select a from TrioMstperkiraan a where a.trioMstperkiraanPK.idPerkiraan between '11300' and '11399'";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		
		return q.list();
	
	}
}

