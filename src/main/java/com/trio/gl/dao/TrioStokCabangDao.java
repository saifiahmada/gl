package com.trio.gl.dao;

import com.trio.gl.bean.TrioStokCabang;
import com.trio.gl.hibernate.TrioGenericDao;

/** @author Saifi Ahmada Jul 19, 2013 4:47:42 PM  **/

public interface TrioStokCabangDao extends TrioGenericDao<TrioStokCabang> { 

}

