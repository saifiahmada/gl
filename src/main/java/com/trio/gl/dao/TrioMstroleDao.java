package com.trio.gl.dao;

import com.trio.gl.bean.TrioMstrole;
import com.trio.gl.hibernate.TrioGenericDao;

/** @author Saifi Ahmada Feb 7, 2013 4:51:36 PM  **/

public interface TrioMstroleDao extends TrioGenericDao<TrioMstrole> { 

}

