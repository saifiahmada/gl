package com.trio.gl.dao;

import java.util.List;

import com.trio.gl.bean.TrioDesignNeracaAkhirPasiva;
import com.trio.gl.hibernate.TrioGenericDao;

/**
 * @author Gusti Arya 10:12:50 AM Oct 29, 2013
 */

public interface TrioDesignNeracaAkhirPasivaDao extends TrioGenericDao<TrioDesignNeracaAkhirPasiva> {
	
	public void saveByList(List<TrioDesignNeracaAkhirPasiva> lists, String user);
	public void saveOrUpdateByList(List<TrioDesignNeracaAkhirPasiva> lists, String user);

}
