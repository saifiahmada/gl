package com.trio.gl.dao;

import java.util.List;

import com.trio.gl.bean.TrioRmsNeracaPasiva;
import com.trio.gl.hibernate.TrioGenericDao;

/**
 * @author Gusti Arya 4:54:28 PM Nov 8, 2013
 */

public interface TrioRmsNeracaPasivaDao extends TrioGenericDao<TrioRmsNeracaPasiva>{
	
	public void saveByList(List<TrioRmsNeracaPasiva> list, String username);
	public List<TrioRmsNeracaPasiva> getSumByKdHsl();

}
