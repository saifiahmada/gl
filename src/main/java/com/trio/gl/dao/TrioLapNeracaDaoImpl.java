package com.trio.gl.dao;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trio.gl.bean.TrioLapNeraca;
import com.trio.gl.hibernate.TrioHibernateDaoSupport;
import com.trio.gl.util.TrioDateUtil;

/**
 * @author Gusti Arya 4:33:45 PM Oct 25, 2013
 */

@Service
public class TrioLapNeracaDaoImpl extends TrioHibernateDaoSupport implements TrioLapNeracaDao{

	@Override
	@Transactional(readOnly=false)
	public void saveOrUpdate(TrioLapNeraca domain, String user) {
		// TODO Auto-generated method stub
		TrioLapNeraca objDB = (TrioLapNeraca) getHibernateTemplate().getSessionFactory().getCurrentSession()
				.get(TrioLapNeraca.class, domain.getTrioLapNeracaPK());
		if (objDB == null ) {
			save(domain, user); 
		} else {
			update(domain, user); 
		}
	}

	@Override
	@Transactional(readOnly=false)
	public void save(TrioLapNeraca domain, String user) {
		// TODO Auto-generated method stub
		domain.getUserTrailing().setDcrea(TrioDateUtil.getLongSysDate());
		domain.getUserTrailing().setVcreaby(user);
		getHibernateTemplate().getSessionFactory().getCurrentSession().persist(domain);
	}

	@Override
	@Transactional(readOnly=false)
	public void update(TrioLapNeraca domain, String user) {
		// TODO Auto-generated method stub
		domain.getUserTrailing().setDmodi(TrioDateUtil.getLongSysDate());
		domain.getUserTrailing().setVmodiby(user);
		getHibernateTemplate().getSessionFactory().getCurrentSession().merge(domain);
	}

	@Override
	public void delete(TrioLapNeraca domain) {
		// TODO Auto-generated method stub
		
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<TrioLapNeraca> findAll() {
		String hql = "from TrioLapNeraca";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		return q.list();
	}

	@Override
	public List<TrioLapNeraca> findByExample(TrioLapNeraca domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TrioLapNeraca> findByCriteria(TrioLapNeraca domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TrioLapNeraca findByPrimaryKey(TrioLapNeraca domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<TrioLapNeraca> findByPeriode(String periode) {
		String hql = "from TrioLapNeraca where periode = :periode";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		q.setString("periode", periode);
		return q.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<TrioLapNeraca> findBySumRl() {
		String hql = "select new TrioLapNeraca(t.trioLapNeracaPK.idPerkiraan, sum(slAwalD), sum(slAwalK), " 
				+ "sum(t.mtsD), sum(t.mtsK), sum(t.slAkhD), sum(t.slAkhK), t.rL) from TrioLapNeraca t "
				+ "group by t.trioLapNeracaPK.idPerkiraan";
		
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		return q.list();
	}
	
}
