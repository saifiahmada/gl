package com.trio.gl.dao;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trio.gl.bean.TrioLabaDitahan;
import com.trio.gl.hibernate.TrioHibernateDaoSupport;

/**
 * @author Gusti Arya 10:27:42 AM Sep 30, 2013
 */

@Service
public class TrioLabaDitahanDaoImpl extends TrioHibernateDaoSupport implements TrioLabaDitahanDao{

	@Override
	@Transactional(readOnly=false)
	public void saveOrUpdate(TrioLabaDitahan domain, String user) {
		getHibernateTemplate().getSessionFactory().getCurrentSession().saveOrUpdate(domain);
	}

	@Override
	public void save(TrioLabaDitahan domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(TrioLabaDitahan domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	@Transactional(readOnly=false)
	public void delete(TrioLabaDitahan domain) {
		getHibernateTemplate().getSessionFactory().getCurrentSession().delete(domain);
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<TrioLabaDitahan> findAll() {
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery("from TrioLabaDitahan");
		return q.list();
	}

	@Override
	public List<TrioLabaDitahan> findByExample(TrioLabaDitahan domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TrioLabaDitahan> findByCriteria(TrioLabaDitahan domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TrioLabaDitahan findByPrimaryKey(TrioLabaDitahan domain) {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<TrioLabaDitahan> findListbyKdDlr(String kdDlr) {
		String hql = "from TrioLabaDitahan a where a.trioLabaDitahanPK.kdDlr = :kdDlr";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		q.setString("kdDlr", kdDlr);
		return q.list();
	}
}
