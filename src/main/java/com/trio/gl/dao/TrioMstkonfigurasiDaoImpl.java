package com.trio.gl.dao;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trio.gl.bean.TrioMstkonfigurasi;
import com.trio.gl.bean.TrioMstkonfigurasiPK;
import com.trio.gl.hibernate.TrioHibernateDaoSupport;
import com.trio.gl.util.TrioDateUtil;

/** @author Saifi Ahmada Mar 22, 2013 9:17:53 AM  **/

@Service
public class TrioMstkonfigurasiDaoImpl extends TrioHibernateDaoSupport implements TrioMstkonfigurasiDao {

	@Transactional(readOnly=false)
	public void saveOrUpdate(TrioMstkonfigurasi domain, String user) {
		// TODO , masbro
		TrioMstkonfigurasi entity = (TrioMstkonfigurasi) getHibernateTemplate().getSessionFactory().getCurrentSession().get(TrioMstkonfigurasi.class, domain.getTrioMstkonfigurasiPK());
		if (entity == null){
			domain.getUserTrailing().setVcreaby(user);
			domain.getUserTrailing().setDcrea(TrioDateUtil.getLongSysDate());
			getHibernateTemplate().save(domain);
			System.out.println("insert sukses");
		} else {
			domain.getUserTrailing().setVmodiby(user);
			domain.getUserTrailing().setDmodi(TrioDateUtil.getLongSysDate());
			getHibernateTemplate().merge(domain);
			System.out.println("update sukses");
		}
		
	}

	public void save(TrioMstkonfigurasi domain, String user) {
		// TODO , masbro
		
		
	}

	public void update(TrioMstkonfigurasi domain, String user) {
		// TODO , masbro
		
		
	}

	public void delete(TrioMstkonfigurasi domain) {
		// TODO , masbro
		
		
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<TrioMstkonfigurasi> findAll() {
		// TODO , masbro
		
		return getHibernateTemplate().find("from TrioMstkonfigurasi");
	}

	public List<TrioMstkonfigurasi> findByExample(TrioMstkonfigurasi domain) {
		// TODO , masbro
		
		return null;
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<TrioMstkonfigurasi> findByCriteria(TrioMstkonfigurasi domain) {
		// TODO , masbro
		DetachedCriteria c = DetachedCriteria.forClass(TrioMstkonfigurasi.class);
		if (domain.getTrioMstkonfigurasiPK().getIdKonfigurasi() != null){
			c = c.add(Restrictions.eq("trioMstkonfigurasiPK.idKonfigurasi", domain.getTrioMstkonfigurasiPK().getIdKonfigurasi()));
		}
		if (domain.getNama() != null){
			c = c.add(Restrictions.sqlRestriction("upper(nama) like ('%"+domain.getNama().toUpperCase()+"%') "));
		}
		if (domain.getNilai() != null){
			c = c.add(Restrictions.eq("nilai", domain.getNilai()));
		}
		return getHibernateTemplate().findByCriteria(c);
	}

	@Transactional(readOnly=true)
	public TrioMstkonfigurasi findByPrimaryKey(TrioMstkonfigurasi domain) {
		// TODO , masbro
		TrioMstkonfigurasi entity = (TrioMstkonfigurasi) getHibernateTemplate().getSessionFactory().getCurrentSession().get(TrioMstkonfigurasi.class, domain.getTrioMstkonfigurasiPK());
		return entity;
	}
	
	@Transactional(readOnly=true)
	public TrioMstkonfigurasi findById(String idKonfig) {
		// TODO , masbro
		TrioMstkonfigurasi entity = (TrioMstkonfigurasi) getHibernateTemplate().getSessionFactory().getCurrentSession().get(TrioMstkonfigurasi.class, new TrioMstkonfigurasiPK(idKonfig)); 
		return entity;
	}
	

}

