package com.trio.gl.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.trio.gl.bean.TrioStokUnitTemp;
import com.trio.gl.hibernate.TrioGenericDao;

/**
 * @author Gusti Arya 9:21:39 AM Jun 21, 2013
 */

public interface TrioStokUnitTempDao extends TrioGenericDao<TrioStokUnitTemp>{

	public List<TrioStokUnitTemp> findByKet();
	public List<TrioStokUnitTemp> findResultByDate(Date tglAkhir, Date tgl,Date monthBefore, String formattedDate,String kdDlr);
	public List<TrioStokUnitTemp> findByNoMesinNotNull(Date tglAkhir, String kdDlr);
	public List<TrioStokUnitTemp> findVqTranMasuk(Date tglAwal, Date tglAkhir, String kdDlr);
	public List<TrioStokUnitTemp> findVqTranKeluar(Date tglAwal, Date tglAkhir, String kdDlr);
	public List<TrioStokUnitTemp> getNoMesinGroup(String noFakOut);
	public TrioStokUnitTemp getKetCekPenjualan(String noFak);
	public BigDecimal getNoFakOutCount(String noFakOut);
	
	
}
