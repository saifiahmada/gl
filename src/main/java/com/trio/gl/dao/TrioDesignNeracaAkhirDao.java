package com.trio.gl.dao;

import com.trio.gl.bean.TrioDesignNeracaAkhir;
import com.trio.gl.hibernate.TrioGenericDao;

/** @author Saifi Ahmada May 26, 2013 5:31:03 PM  **/

public interface TrioDesignNeracaAkhirDao extends TrioGenericDao<TrioDesignNeracaAkhir> {
	public void updateNewNeracaAkhir(String user);
}

