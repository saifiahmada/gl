package com.trio.gl.dao;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trio.gl.bean.TrioRmsNeracaPasiva;
import com.trio.gl.hibernate.TrioHibernateDaoSupport;

/**
 * @author Gusti Arya 4:55:00 PM Nov 8, 2013
 */

@Service
public class TrioRmsNeracaPasivaDaoImpl extends TrioHibernateDaoSupport implements TrioRmsNeracaPasivaDao{

	@Override
	public void saveOrUpdate(TrioRmsNeracaPasiva domain, String user) {
		// TODO Auto-generated method stub

	}

	@Override
	public void save(TrioRmsNeracaPasiva domain, String user) {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(TrioRmsNeracaPasiva domain, String user) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(TrioRmsNeracaPasiva domain) {
		// TODO Auto-generated method stub

	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<TrioRmsNeracaPasiva> findAll() {
		String hql = "from TrioRmsNeracaPasiva";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		return q.list();
	}

	@Override
	public List<TrioRmsNeracaPasiva> findByExample(TrioRmsNeracaPasiva domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TrioRmsNeracaPasiva> findByCriteria(TrioRmsNeracaPasiva domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TrioRmsNeracaPasiva findByPrimaryKey(TrioRmsNeracaPasiva domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional(readOnly=false)
	public void saveByList(List<TrioRmsNeracaPasiva> list, String username) {
		String hql = "delete from TrioRmsNeracaPasiva";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		q.executeUpdate();

		for(TrioRmsNeracaPasiva current : list){
			getHibernateTemplate().getSessionFactory().getCurrentSession().persist(current);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<TrioRmsNeracaPasiva> getSumByKdHsl() {
		String hql = "select new TrioRmsNeracaPasiva(t.kdHsl, sum(t.nilai)) from TrioRmsNeracaPasiva t group by t.kdHsl";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);

		return q.list();
	}
}
