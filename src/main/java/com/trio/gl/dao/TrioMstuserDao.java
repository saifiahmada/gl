package com.trio.gl.dao;

import com.trio.gl.bean.TrioMstuser;
import com.trio.gl.hibernate.TrioGenericDao;

/** @author Saifi Ahmada Feb 7, 2013 4:52:49 PM  **/

public interface TrioMstuserDao extends TrioGenericDao<TrioMstuser> { 
	public TrioMstuser findUserByUsername(String username);
	
}

