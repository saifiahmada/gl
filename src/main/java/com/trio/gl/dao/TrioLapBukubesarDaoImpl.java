package com.trio.gl.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trio.gl.bean.DKHelper;
import com.trio.gl.bean.TrioJurnal;
import com.trio.gl.bean.TrioKumpul;
import com.trio.gl.bean.TrioLapBukubesar;
import com.trio.gl.bean.TrioTarikpartsale;
import com.trio.gl.hibernate.TrioHibernateDaoSupport;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;
import com.trio.gl.util.TrioDateUtil;

/** @author Saifi Ahmada Apr 12, 2013 10:35:38 PM **/

@Service
public class TrioLapBukubesarDaoImpl extends TrioHibernateDaoSupport implements
		TrioLapBukubesarDao {

	public void saveOrUpdate(TrioLapBukubesar domain, String user) {
		// TODO , masbro

	}

	@Transactional(readOnly = false)
	public void save(TrioLapBukubesar domain, String user) {
		// TODO , masbro
		domain.getUserTrailing().setVcreaby(user);
		domain.getUserTrailing().setDcrea(TrioDateUtil.getLongSysDate());
		getHibernateTemplate().save(domain);

	}

	public void update(TrioLapBukubesar domain, String user) {
		// TODO , masbro

	}

	public void delete(TrioLapBukubesar domain) {
		// TODO , masbro

	}

	public List<TrioLapBukubesar> findAll() {
		// TODO , masbro

		return null;
	}

	public List<TrioLapBukubesar> findByExample(TrioLapBukubesar domain) {
		// TODO , masbro

		return null;
	}

	public List<TrioLapBukubesar> findByCriteria(TrioLapBukubesar domain) {
		// TODO , masbro

		return null;
	}

	public TrioLapBukubesar findByPrimaryKey(TrioLapBukubesar domain) {
		// TODO , masbro

		return null;
	}

	@Transactional(readOnly = false)
	public void saveListJurnalToBukuBesar(List<TrioJurnal> listJurnal,
			String user) {
		List<TrioLapBukubesar> listBukubesar = new ArrayList<TrioLapBukubesar>();
		for (TrioJurnal jurnal : listJurnal) {
			TrioLapBukubesar bukuBesar = new TrioLapBukubesar(jurnal
					.getTrioJurnalPK().getIdJurnal(), jurnal.getTrioJurnalPK()
					.getIdPerkiraan(), jurnal.getTrioJurnalPK().getIdSub());

			bukuBesar.setTanggal(jurnal.getTglJurnal());
			bukuBesar.setDebet(jurnal.getDebet());
			bukuBesar.setKredit(jurnal.getKredit());
			bukuBesar.setKet(jurnal.getKet());
			listBukubesar.add(bukuBesar);
			save(bukuBesar, user);
		}

		for (TrioLapBukubesar buku : listBukubesar) {
			System.out.println("id perkiraan pada buku besar "
					+ buku.getTrioMstperkiraan().getTrioMstperkiraanPK()
							.getIdPerkiraan());
		}

	}

	@Transactional(readOnly = true)
	public BigDecimal getDebetMinKreditByPeriodeAndNoPerk(String periode,
			String idPerk, int varian) {
		String hql;

		if (varian == 1) {
			hql = "select sum(kredit-debet) from TrioLapBukubesar a where a.trioBukubesarPK.idPerkiraan= :idPerkiraan "
					+ "and a.trioBukubesarPK.periode = :periode";
		} else {
			hql = "select sum(debet) from TrioLapBukubesar a where a.trioBukubesarPK.idPerkiraan= :idPerkiraan "
					+ "and a.trioBukubesarPK.periode = :periode";
		}

		Query q = getHibernateTemplate().getSessionFactory()
				.getCurrentSession().createQuery(hql);
		q.setString("idPerkiraan", idPerk);
		q.setString("periode", periode);
		BigDecimal bd = (BigDecimal) q.uniqueResult();

		return bd;
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public List<TrioLapBukubesar> getSumSaldoSub(String periode) {

		String hql = "select new TrioLapBukubesar(b.trioBukubesarPK.idPerkiraan, b.trioBukubesarPK.idSub, SUM(b.debet-b.kredit)) from TrioMstsubperkiraan a, TrioLapBukubesar b"
				+ " where b.trioBukubesarPK.idPerkiraan = a.trioMstsubperkiraanPK.idPerkiraan"
				+ " and b.trioBukubesarPK.idSub = a.trioMstsubperkiraanPK.idSub"
				+ " and b.trioBukubesarPK.idSub <> ''"
				+ " and b.trioBukubesarPK.periode = :periode"
				+ " group by b.trioBukubesarPK.idPerkiraan, b.trioBukubesarPK.idSub"
				+ " order by a.trioMstsubperkiraanPK.idPerkiraan, a.trioMstsubperkiraanPK.idSub";
		Query q = getHibernateTemplate().getSessionFactory()
				.getCurrentSession().createQuery(hql);
		q.setString("periode", periode);

		return q.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public List<TrioLapBukubesar> getListBukuBesarByPeriode(String periode) {
		String sQ = "from TrioLapBukubesar t where t.periode = :periode order by t.trioLapBukubesarPK.idPerkiraan, t.trioLapBukubesarPK.idSub";
		Query query = getHibernateTemplate().getSessionFactory()
				.getCurrentSession().createQuery(sQ);
		query.setParameter("periode", periode);
		return query.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public List<TrioKumpul> getListKumpulByPeriode(String periode) {
		String sQ = "select id_perkiraan, id_sub, dcrea, dmodi,vcreaby, vmodiby, sum(debet-kredit) total from trio_lap_bukubesar where PERIODE = :periode and LENGTH(id_sub) > 0 group by ID_PERKIRAAN, ID_SUB ";
		SQLQuery sqlQuery = getHibernateTemplate().getSessionFactory().getCurrentSession()
				.createSQLQuery(sQ);
		sqlQuery.setString("periode", periode);
		sqlQuery.addEntity(TrioKumpul.class);
		return sqlQuery.list();
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<DKHelper> getListDKHelperLapBukubesarByPeriode(String periode) {
		
		String sQ = "select t.id_Perkiraan as idPerkiraan , sum(t.debet) as debet, sum(t.kredit) as kredit from Trio_Lap_Bukubesar t where t.periode = '"+periode+"' group by t.id_Perkiraan";
		SQLQuery sqlQ = getHibernateTemplate().getSessionFactory()
				.getCurrentSession().createSQLQuery(sQ);
		sqlQ.addEntity(DKHelper.class);
		return sqlQ.list();
	}
	

}
