package com.trio.gl.dao;

import com.trio.gl.bean.TrioMstuserrole;
import com.trio.gl.hibernate.TrioGenericDao;

/** @author Saifi Ahmada Feb 8, 2013 4:13:59 PM  **/

public interface TrioMstuserroleDao extends TrioGenericDao<TrioMstuserrole> {

}

