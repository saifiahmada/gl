package com.trio.gl.dao;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trio.gl.bean.TrioLapNeracaAktiva;
import com.trio.gl.hibernate.TrioHibernateDaoSupport;

/**
 * @author Gusti Arya 4:34:48 PM Oct 25, 2013
 */

@Service
public class TrioLapNeracaAktivaDaoImpl extends TrioHibernateDaoSupport implements TrioLapNeracaAktivaDao{

	@Override
	public void saveOrUpdate(TrioLapNeracaAktiva domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	@Transactional(readOnly=false)
	public void save(TrioLapNeracaAktiva domain, String user) {
	}

	@Override
	public void update(TrioLapNeracaAktiva domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(TrioLapNeracaAktiva domain) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<TrioLapNeracaAktiva> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TrioLapNeracaAktiva> findByExample(TrioLapNeracaAktiva domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TrioLapNeracaAktiva> findByCriteria(TrioLapNeracaAktiva domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TrioLapNeracaAktiva findByPrimaryKey(TrioLapNeracaAktiva domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<TrioLapNeracaAktiva> findByPeriode(String periode) {
		String hql = "from TrioLapNeracaAktiva where periode = :periode";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		q.setString("periode", periode);

		return q.list();
	}

	@Override
	@Transactional(readOnly=false)
	public void saveFromCollection(List<TrioLapNeracaAktiva> list, String user) {
		for(TrioLapNeracaAktiva current : list){
			getHibernateTemplate().getSessionFactory().getCurrentSession().save(current);
		}
	}
}
