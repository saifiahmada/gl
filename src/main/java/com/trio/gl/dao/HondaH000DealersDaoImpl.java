package com.trio.gl.dao;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trio.gl.bean.HondaH000Dealers;
import com.trio.gl.hibernate.TrioHibernateDaoSupport;

/** @author Saifi Ahmada Mar 23, 2013 4:17:06 PM  **/

@Service
public class HondaH000DealersDaoImpl extends TrioHibernateDaoSupport implements HondaH000DealersDao {

	public void saveOrUpdate(HondaH000Dealers domain, String user) {
		// TODO , masbro
		
		
	}

	public void save(HondaH000Dealers domain, String user) {
		// TODO , masbro
		
		
	}

	public void update(HondaH000Dealers domain, String user) {
		// TODO , masbro
		
		
	}

	public void delete(HondaH000Dealers domain) {
		// TODO , masbro
		
		
	}

	@Transactional(readOnly=true)
	public List<HondaH000Dealers> findAll() {
		// TODO , masbro
		
		return getHibernateTemplate().find("from HondaH000Dealers");
	}

	public List<HondaH000Dealers> findByExample(HondaH000Dealers domain) {
		// TODO , masbro
		
		return null;
	}

	public List<HondaH000Dealers> findByCriteria(HondaH000Dealers domain) {
		// TODO , masbro
		
		return null;
	}

	public HondaH000Dealers findByPrimaryKey(HondaH000Dealers domain) {
		// TODO , masbro
		
		return null;
	}

}

