package com.trio.gl.dao;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trio.gl.bean.TrioDesignRugiLaba;
import com.trio.gl.hibernate.TrioHibernateDaoSupport;

@Service
public class TrioDesignRugiLabaDaoImpl extends TrioHibernateDaoSupport implements TrioDesignRugiLabaDao{

	@Override
	public void saveOrUpdate(TrioDesignRugiLaba domain, String user) {
		// TODO Auto-generated method stub

	}

	@Override
	public void save(TrioDesignRugiLaba domain, String user) {
		// TODO Auto-generated method stub

	}

	@Override
	@Transactional(readOnly=false)
	public void update(TrioDesignRugiLaba domain, String user) {
		getHibernateTemplate().getSessionFactory().getCurrentSession().update(domain);
	}

	@Override
	public void delete(TrioDesignRugiLaba domain) {
		// TODO Auto-generated method stub

	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<TrioDesignRugiLaba> findAll() {
		String hql = "from TrioDesignRugiLaba";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		return q.list();
	}

	@Override
	public List<TrioDesignRugiLaba> findByExample(TrioDesignRugiLaba domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TrioDesignRugiLaba> findByCriteria(TrioDesignRugiLaba domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TrioDesignRugiLaba findByPrimaryKey(TrioDesignRugiLaba domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional(readOnly=false)
	public void updateByList(List<TrioDesignRugiLaba> list, String user) {
		for(TrioDesignRugiLaba current : list){
			getHibernateTemplate().getSessionFactory().getCurrentSession().update(current);
		}
	}

	//digunakan di form designRubiLaba saat pemanggilan Button Create File excel
	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<TrioDesignRugiLaba> rlExcel() {
		String hql = "from TrioDesignRugiLaba where (namaPerkiraan is not null or (level1 + level2 + level3 + level4)> 0) or substring(namaPerkiraan,0,1)='<') and shows='*'";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		
		return q.list();
	}

	//digunakan di form designRugiLaba saat pemanggilan button Save Rugi Laba
	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<TrioDesignRugiLaba> rlSave() {
		
		String hql = "from TrioDesignRugiLaba where (namaPerkiraan is not null or (level1 +level2 + level3 + level4)<>0) or (ket2 is not null or substring(namaPerkiraan,0,1)='<') and shows='*'";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);		
		
		return q.list();
	}

	@Override
	@Transactional(readOnly=false)
	public void saveOrUpdateByList(List<TrioDesignRugiLaba> list, String user) {
		String hql = "delete from TrioDesignRugiLaba";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		q.executeUpdate();
		
		for(TrioDesignRugiLaba current : list){
			getHibernateTemplate().getSessionFactory().getCurrentSession().save(current);
		}
	}
}
