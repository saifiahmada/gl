package com.trio.gl.dao;

import java.util.List;

import com.trio.gl.bean.TrioLapNeracaPasiva;
import com.trio.gl.hibernate.TrioGenericDao;

/**
 * @author Gusti Arya 10:03:18 AM Nov 8, 2013
 */

public interface TrioLapNeracaPasivaDao extends TrioGenericDao<TrioLapNeracaPasiva> {
	
	public List<TrioLapNeracaPasiva> findByPeriode(String periode);
	public void saveFromCollection(List<TrioLapNeracaPasiva> lists, String username);

}
