package com.trio.gl.dao;

import java.util.List;

import com.trio.gl.bean.TrioLapNeracaAktiva;
import com.trio.gl.hibernate.TrioGenericDao;

/**
 * @author Gusti Arya 4:34:21 PM Oct 25, 2013
 */

public interface TrioLapNeracaAktivaDao extends TrioGenericDao<TrioLapNeracaAktiva> {
	
	public List<TrioLapNeracaAktiva> findByPeriode(String periode);
	public void saveFromCollection(List<TrioLapNeracaAktiva> list, String user);

}
