package com.trio.gl.dao;

import java.util.Date;
import java.util.List;

import com.trio.gl.bean.TrioDetailBpkb;
import com.trio.gl.hibernate.TrioGenericDao;

/**
 * @author Gusti Arya 1:52:11 PM Jul 17, 2013
 */

public interface TrioDetailBpkbDao extends TrioGenericDao<TrioDetailBpkb>{
	
	public List<TrioDetailBpkb> findByStatusA(String status, Date tglAwal, Date tglAkhir);
	public List<TrioDetailBpkb> findByStatusX(String status, Date tglAwal, Date tglAkhir);
	public List<TrioDetailBpkb> findByStatus(String status, Date tglAwal, Date tglAkhir);

}
