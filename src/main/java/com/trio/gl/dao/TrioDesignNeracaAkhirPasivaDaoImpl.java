package com.trio.gl.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trio.gl.bean.TrioDesignNeracaAkhirPasiva;
import com.trio.gl.hibernate.TrioHibernateDaoSupport;

/**
 * @author Gusti Arya 10:13:25 AM Oct 29, 2013
 */

@Service
public class TrioDesignNeracaAkhirPasivaDaoImpl extends TrioHibernateDaoSupport implements TrioDesignNeracaAkhirPasivaDao{

	@Override
	public void saveOrUpdate(TrioDesignNeracaAkhirPasiva domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void save(TrioDesignNeracaAkhirPasiva domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	@Transactional(readOnly=false)
	public void update(TrioDesignNeracaAkhirPasiva domain, String user) {
		getHibernateTemplate().getSessionFactory().getCurrentSession().merge(domain);
	}

	@Override
	public void delete(TrioDesignNeracaAkhirPasiva domain) {
		// TODO Auto-generated method stub
		
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<TrioDesignNeracaAkhirPasiva> findAll() {
		String hql = "from TrioDesignNeracaAkhirPasiva";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		return q.list();
	}

	@Override
	public List<TrioDesignNeracaAkhirPasiva> findByExample(
			TrioDesignNeracaAkhirPasiva domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TrioDesignNeracaAkhirPasiva> findByCriteria(
			TrioDesignNeracaAkhirPasiva domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TrioDesignNeracaAkhirPasiva findByPrimaryKey(
			TrioDesignNeracaAkhirPasiva domain) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	@Transactional(readOnly=false)
	public void saveByList(List<TrioDesignNeracaAkhirPasiva> lists,
			String user) {
		String nativeSql = "TRUNCATE TRIO_DESIGN_NERACA_AKHIR_PASIVA";
		SQLQuery sqlQuery = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(nativeSql);
		sqlQuery.executeUpdate();
		
		for(TrioDesignNeracaAkhirPasiva current : lists){
			getHibernateTemplate().getSessionFactory().getCurrentSession().save(current);
		}
	}

	@Override
	@Transactional(readOnly=false)
	public void saveOrUpdateByList(List<TrioDesignNeracaAkhirPasiva> lists,
			String user) {
		for(TrioDesignNeracaAkhirPasiva current : lists){
			getHibernateTemplate().getSessionFactory().getCurrentSession().saveOrUpdate(current);
		}
	}
}
