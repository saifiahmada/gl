package com.trio.gl.dao;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trio.gl.bean.TrioJurnal;
import com.trio.gl.bean.TrioStokCabang;
import com.trio.gl.bean.TrioStokIntransit;
import com.trio.gl.bean.TrioStokTerima;
import com.trio.gl.hibernate.TrioHibernateDaoSupport;
import com.trio.gl.util.TrioDateConv;
import com.trio.gl.util.TrioDateUtil;

/** @author Saifi Ahmada Jul 15, 2013 10:38:41 AM  **/

@Service
public class TrioStokIntransitDaoImpl extends TrioHibernateDaoSupport implements TrioStokIntransitDao {

	public void saveOrUpdate(TrioStokIntransit domain, String user) {
		// TODO , masbro
		
		
	}
	
	@Transactional(readOnly=false)
	public void save(TrioStokIntransit domain, String user) {
		// TODO , masbro
		domain.getUserTrailing().setDcrea(TrioDateUtil.getLongSysDate());
		domain.getUserTrailing().setVcreaby(user);
		getHibernateTemplate().getSessionFactory().getCurrentSession().save(domain);
		
		
	}

	public void update(TrioStokIntransit domain, String user) {
		// TODO , masbro
		domain.getUserTrailing().setDmodi(TrioDateUtil.getLongSysDate());
		domain.getUserTrailing().setVmodiby(user);
		getHibernateTemplate().getSessionFactory().getCurrentSession().update(domain);
		
	}

	public void delete(TrioStokIntransit domain) {
		// TODO , masbro
		
		
	}

	public List<TrioStokIntransit> findAll() {
		// TODO , masbro
		
		return null;
	}

	public List<TrioStokIntransit> findByExample(TrioStokIntransit domain) {
		// TODO , masbro
		
		return null;
	}

	public List<TrioStokIntransit> findByCriteria(TrioStokIntransit domain) {
		// TODO , masbro
		
		return null;
	}

	public TrioStokIntransit findByPrimaryKey(TrioStokIntransit domain) {
		return (TrioStokIntransit) getHibernateTemplate().getSessionFactory().getCurrentSession()
				.get(TrioStokIntransit.class, domain.getTrioStokIntransitPK());
	}
	
	@Transactional(readOnly=false)
	public void saveAll(List<TrioStokIntransit> list, String user){
		for (TrioStokIntransit stok : list){
			save(stok, user);
		}
	}
	
	@Transactional(readOnly=false)
	public String prosesUploadFile(Vector<String[]> vectorString,String kdDlr, String noCabang, String user){
		List<TrioStokIntransit> listStok = new ArrayList<TrioStokIntransit>();
		for (String [] s : vectorString){
			String noFaktur = s[0]; // no faktur
			String noTerima = s[1]; // no terima
			String tglTerima = s[2]; // tgl terima ex: 28-06-2013
			String partNum = s[3]; // partnum
			int qty = Integer.parseInt(s[4]); // qty
			String kelompokPart = "PART"; // PART | OLI
			
			Date tgl = (Date) TrioDateConv.valueOf(tglTerima);
			System.out.println("Tgl hasil konvert "+tgl);
			TrioStokIntransit stok = new TrioStokIntransit(noFaktur, partNum);
			TrioStokIntransit baru = findByPrimaryKey(stok);
			if (baru != null){
				
				TrioStokTerima terima = new TrioStokTerima(noTerima, partNum);
				terima.setKdDlr(kdDlr);
				terima.setQty(qty);
				terima.setTglTerima(tgl);
				
				// update data trioStokIntransit
				baru.setNoRef(noTerima);
				baru.setStatus("T");
				update(baru, user);
				
				// save data trioStokTerima
				getMasterFacade().getTrioStokTerimaDao().save(terima, user);
				
				TrioStokCabang sc = getMasterFacade().getTrioStokCabangDao().findByPrimaryKey(new TrioStokCabang(partNum));
				
				if (sc != null){
					int stokBaru = sc.getQty() + qty;
					sc.setQty(stokBaru);
					sc.getUserTrailing().setVmodiby(user);
					sc.getUserTrailing().setDmodi(TrioDateUtil.getLongSysDate());
				}else {
					sc = new TrioStokCabang(partNum);
					sc.setQty(qty);
					sc.getUserTrailing().setDcrea(TrioDateUtil.getLongSysDate());
					sc.getUserTrailing().setVcreaby(user);
				}
				//save data trioStokCabang
				getMasterFacade().getTrioStokCabangDao().save(sc, user);
				
				/*
				 * membuat jurnal
				 * persedian sparepart/oli
				 * 		persedian in transit sparepart/oli
				 * 
				 *  11522 => persediaan in transit sparepart CBG I, 11527 => CBG II
				 *  11523 => persediaan  in transit oli CBG I => , 11528 => CBG II
				 *  
				 *  11502 => persediaan sparepart CBG I, 11505=> CBG II
				 *  11503 => persediaan oli CBG I, 11506 => II
				 */
				String idPerk1 = null;
				String idPerk2 = null;
				if (noCabang.equalsIgnoreCase("1")){
					if (kelompokPart.equalsIgnoreCase("PART")){
						idPerk1 = "11502"; // persedian sparepart CBG I
						idPerk2 = "11522"; // persedian in transit sparepart CBG I
					}else if (kelompokPart.equalsIgnoreCase("OLI")) {
						idPerk1 = "11503"; // persedian oli CBG I
						idPerk2 = "11523"; // persedian in transit oli CBG I
					}
				} else if (noCabang.equalsIgnoreCase("2")) {
					if (kelompokPart.equalsIgnoreCase("PART")){
						idPerk1 = "11505"; // persedian sparepart CBG II
						idPerk2 = "11527"; // persedian in transit sparepart CBG II
					}else if (kelompokPart.equalsIgnoreCase("OLI")) {
						idPerk1 = "11506"; // persedian oli CBG II
						idPerk2 = "11528"; // persedian in transit oli CBG II
					}
				}
				
				BigDecimal jumlah = new BigDecimal(0);
				
				List<TrioJurnal> listJurnal = new ArrayList<TrioJurnal>();
				TrioJurnal jurnal = new TrioJurnal();
				jurnal.setTglJurnal(TrioDateUtil.getDateFromString(tglTerima)); 
				jurnal.setBlc("*");
				jurnal.getTrioJurnalPK().setIdPerkiraan(idPerk1);
				jurnal.getTrioJurnalPK().setIdSub("");
				//DatabaseContextHolder.setConnectionType(ConnectionType.setType(sessionCabang));
				String nmPerk = getMasterFacade().getTrioMstperkiraanDao().findNamaById(idPerk1);
				jurnal.setNamaPerkiraan(nmPerk);
				jurnal.setKet("NAMA TRANSAKSI"); // belom ditentuin
				jurnal.setDebet(jumlah);
				jurnal.setKredit(new BigDecimal(0));
				jurnal.setTipe("D");
				listJurnal.add(jurnal);
				
				jurnal = new TrioJurnal();
				jurnal.setTglJurnal(TrioDateUtil.getDateFromString(tglTerima)); 
				jurnal.setBlc("*");
				jurnal.getTrioJurnalPK().setIdPerkiraan(idPerk2);
				jurnal.getTrioJurnalPK().setIdSub("");
				//DatabaseContextHolder.setConnectionType(ConnectionType.setType(sessionCabang));
				nmPerk = getMasterFacade().getTrioMstperkiraanDao().findNamaById(idPerk2);
				jurnal.setNamaPerkiraan(nmPerk);
				jurnal.setKet("NAMA TRANSAKSI"); // belom ditentuin
				jurnal.setDebet(new BigDecimal(0));
				jurnal.setKredit(jumlah);
				jurnal.setTipe("K");
				listJurnal.add(jurnal);
				//DatabaseContextHolder.setConnectionType(ConnectionType.setType(sessionCabang));
				getMasterFacade().getTrioJurnalDao().saveTransaction(listJurnal, "JME", user);
				
			} else {
				return "No Faktur "+noFaktur+ ", dan no part "+partNum + "tidak ditemukan pada Stok Intransit, belum melakukan tarik Oracle";
			}
			
		}
		return "Transaksi berhasil";
	}
	
	
	
	

}

