package com.trio.gl.dao;

import java.math.BigDecimal;
import java.util.List;

import com.trio.gl.bean.DKHelper;
import com.trio.gl.bean.TrioJurnal;
import com.trio.gl.bean.TrioKumpul;
import com.trio.gl.bean.TrioLapBukubesar;
import com.trio.gl.hibernate.TrioGenericDao;

/** @author Saifi Ahmada Apr 12, 2013 10:33:07 PM  **/

public interface TrioLapBukubesarDao extends TrioGenericDao<TrioLapBukubesar>{ 
	public void saveListJurnalToBukuBesar(List<TrioJurnal> listJurnal, String user);
	public BigDecimal getDebetMinKreditByPeriodeAndNoPerk(String periode, String idPerk,int varian);
	public List<TrioLapBukubesar> getSumSaldoSub(String periode);
	public List<TrioLapBukubesar> getListBukuBesarByPeriode(String periode);
	public List<DKHelper> getListDKHelperLapBukubesarByPeriode(String periode);
	public List<TrioKumpul> getListKumpulByPeriode(String periode);
}

