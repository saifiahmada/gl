package com.trio.gl.dao;

import java.util.List;

import com.trio.gl.bean.TrioLapNeraca;
import com.trio.gl.hibernate.TrioGenericDao;

/**
 * @author Gusti Arya 4:32:53 PM Oct 25, 2013
 */

public interface TrioLapNeracaDao extends TrioGenericDao<TrioLapNeraca> {
	public List<TrioLapNeraca> findByPeriode(String periode);
	public List<TrioLapNeraca> findBySumRl();
	
}
