package com.trio.gl.dao;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trio.gl.bean.TrioLapRl;
import com.trio.gl.hibernate.TrioHibernateDaoSupport;

@Service
public class TrioLapRlDaoImpl extends TrioHibernateDaoSupport implements TrioLapRlDao {

	@Override
	public void saveOrUpdate(TrioLapRl domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void save(TrioLapRl domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(TrioLapRl domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(TrioLapRl domain) {
		// TODO Auto-generated method stub
		
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<TrioLapRl> findAll() {
		String hql = "from TrioLapRl";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		return q.list();
	}

	@Override
	public List<TrioLapRl> findByExample(TrioLapRl domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TrioLapRl> findByCriteria(TrioLapRl domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TrioLapRl findByPrimaryKey(TrioLapRl domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<TrioLapRl> findByPeriode(String periode) {
		String hql = "from TrioLapRl where periode = :periode";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		q.setString("periode", periode);
		
		return q.list();
	}

	@Override
	@Transactional(readOnly=false)
	public void saveByList(List<TrioLapRl> list) {
		for(TrioLapRl current : list){
			getHibernateTemplate().getSessionFactory().getCurrentSession().save(current);
		}
	}

}
