package com.trio.gl.dao;

import java.util.List;

import com.trio.gl.bean.TrioMstkonfigcabang;
import com.trio.gl.hibernate.TrioGenericDao;

/** @author Saifi Ahmada Apr 3, 2013 12:04:39 PM  **/

public interface TrioMstkonfigcabangDao extends TrioGenericDao<TrioMstkonfigcabang> {
	
	public List<TrioMstkonfigcabang> getListCabangByContext(String context);

}

