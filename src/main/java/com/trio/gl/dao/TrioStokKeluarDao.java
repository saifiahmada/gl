package com.trio.gl.dao;

import com.trio.gl.bean.TrioStokKeluar;
import com.trio.gl.hibernate.TrioGenericDao;

/** @author Saifi Ahmada Jul 19, 2013 4:46:09 PM  **/

public interface TrioStokKeluarDao extends TrioGenericDao<TrioStokKeluar> {

}

