package com.trio.gl.dao;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trio.gl.bean.TrioDesignNeracaAkhir;
import com.trio.gl.hibernate.TrioHibernateDaoSupport;

/** @author Saifi Ahmada May 26, 2013 5:31:30 PM  **/

@Service
public class TrioDesignNeracaAkhirDaoImpl extends TrioHibernateDaoSupport implements TrioDesignNeracaAkhirDao {

	public void saveOrUpdate(TrioDesignNeracaAkhir domain, String user) {
		// TODO , masbro
		
		
	}

	public void save(TrioDesignNeracaAkhir domain, String user) {
		// TODO , masbro
		
		
	}

	public void update(TrioDesignNeracaAkhir domain, String user) {
		// TODO , masbro
		
		
	}

	public void delete(TrioDesignNeracaAkhir domain) {
		// TODO , masbro
		
		
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<TrioDesignNeracaAkhir> findAll() {
		return getHibernateTemplate().find("from TrioDesignNeracaAkhir");
	}

	public List<TrioDesignNeracaAkhir> findByExample(
			TrioDesignNeracaAkhir domain) {
		// TODO , masbro
		
		return null;
	}

	public List<TrioDesignNeracaAkhir> findByCriteria(
			TrioDesignNeracaAkhir domain) {
		// TODO , masbro
		
		return null;
	}

	public TrioDesignNeracaAkhir findByPrimaryKey(TrioDesignNeracaAkhir domain) {
		// TODO , masbro
		
		return null;
	}
	
	@Transactional(readOnly=false)
	public void updateNewNeracaAkhir(String user){
		System.out.println("update dao");
		String sQ = "update trio_design_neraca_akhir set level1 = 0, level2 = 0, nilai = 0, vmodiby = 'saifi'";
		System.out.println("Query "+ sQ);
		getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(sQ).executeUpdate();
	}

}

