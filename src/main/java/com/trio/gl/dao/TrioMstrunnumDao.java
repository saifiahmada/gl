package com.trio.gl.dao;

import com.trio.gl.bean.TrioMstrunnum;
import com.trio.gl.hibernate.TrioGenericDao;

/** @author Saifi Ahmada Mar 14, 2013 4:39:11 PM  **/

public interface TrioMstrunnumDao extends TrioGenericDao<TrioMstrunnum> {
	
	public int getRunningNumber(String idDoc, String reseter, String user);
	
}

