package com.trio.gl.dao;

import java.math.BigDecimal;
import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trio.gl.bean.TrioKumpul;
import com.trio.gl.hibernate.TrioHibernateDaoSupport;
import com.trio.gl.util.TrioDateUtil;

/**
 * @author Gusti Arya 4:23:38 PM Sep 3, 2013
 */

@Service
public class TrioKumpulDaoImpl extends TrioHibernateDaoSupport implements TrioKumpulDao{

	@Override
	@Transactional(readOnly=false)
	public void saveOrUpdate(TrioKumpul domain, String user) {
		// TODO Auto-generated method stub
		TrioKumpul objDB = (TrioKumpul) getHibernateTemplate().getSessionFactory().getCurrentSession().get(TrioKumpul.class, domain.getTrioKumpulPK());
		if (objDB == null) {
			save(domain, user);
		} else {
			update(domain, user); 
		}
	}

	@Override
	@Transactional(readOnly=false)
	public void save(TrioKumpul domain, String user) {
		domain.getUserTrailing().setDcrea(TrioDateUtil.getLongSysDate());
		domain.getUserTrailing().setVcreaby(user); 
		getHibernateTemplate().getSessionFactory().getCurrentSession().persist(domain);
	}

	@Override
	@Transactional(readOnly=false)
	public void update(TrioKumpul domain, String user) {
		domain.getUserTrailing().setDmodi(TrioDateUtil.getLongSysDate());
		domain.getUserTrailing().setVmodiby(user); 
		getHibernateTemplate().getSessionFactory().getCurrentSession().merge(domain);
		
	}

	@Override
	public void delete(TrioKumpul domain) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<TrioKumpul> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TrioKumpul> findByExample(TrioKumpul domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TrioKumpul> findByCriteria(TrioKumpul domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TrioKumpul findByPrimaryKey(TrioKumpul domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional(readOnly=false)
	public void deleteFrom() {
		String hql = "delete from TrioKumpul";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		q.executeUpdate();
	}

	@Override
	@Transactional(readOnly=true)
	public BigDecimal getTotalByIdPerk(String idPerk, String idSub) {
		String hql = "select a.total from TrioKumpul a where a.trioKumpulPK.idPerkiraan = :idPerk " 
				+ "and a.trioKumpulPK.idSub = :idSub";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		q.setString("idPerk", idPerk);
		q.setString("idSub", idSub);
		
		return (BigDecimal) q.uniqueResult();
	}
	
	@Transactional(readOnly=false)
	public void saveAll (List<TrioKumpul> listKumpul, String user) {
		for (TrioKumpul kumpul : listKumpul) {
			saveOrUpdate(kumpul, user);
		}
	}
	
}
