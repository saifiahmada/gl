package com.trio.gl.dao;

import java.util.List;

import com.trio.gl.bean.TrioRumus;
import com.trio.gl.hibernate.TrioGenericDao;

/**
 * @author Gusti Arya 11:27:12 AM Dec 14, 2013
 */

public interface TrioRumusDao extends TrioGenericDao<TrioRumus>{
	
	public void saveByList(List<TrioRumus> list, String user);
	public void saveOrUpdateByList(List<TrioRumus> list, String user);
	public List<TrioRumus> listSumNilai();

}
