package com.trio.gl.dao;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trio.gl.bean.TrioHeaderStnk;
import com.trio.gl.hibernate.TrioHibernateDaoSupport;
import com.trio.gl.util.TrioDateUtil;

/**
 * @author Gusti Arya 11:13:15 AM Jul 16, 2013
 */

@Service
public class TrioHeaderStnkDaoImpl extends TrioHibernateDaoSupport implements TrioHeaderStnkDao {

	public void saveOrUpdate(TrioHeaderStnk domain, String user) {
		
	}
	
	@Transactional(readOnly=false)
	public void save(TrioHeaderStnk domain, String user) {
		System.out.println("No MOHON = "+domain.getNoMhn());
		domain.getUserTrailing().setVcreaby(user);
		domain.getUserTrailing().setDcrea(TrioDateUtil.getLongSysDate());
		getHibernateTemplate().getSessionFactory().getCurrentSession().save(domain);
		System.out.println("Tersimpan "+domain.getNoMhn());
		
	}

	public void update(TrioHeaderStnk domain, String user) {
		// TODO Auto-generated method stub
		
	}

	public void delete(TrioHeaderStnk domain) {
		// TODO Auto-generated method stub
		
	}

	public List<TrioHeaderStnk> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	public List<TrioHeaderStnk> findByExample(TrioHeaderStnk domain) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<TrioHeaderStnk> findByCriteria(TrioHeaderStnk domain) {
		// TODO Auto-generated method stub
		return null;
	}

	public TrioHeaderStnk findByPrimaryKey(TrioHeaderStnk domain) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Transactional(readOnly=false)
	public void saveAllCollection(List<TrioHeaderStnk> listHdrStnk, String user) {
		for (TrioHeaderStnk hdr : listHdrStnk){
			save(hdr, user);
			
		}
	}
	
	

}
