package com.trio.gl.dao;

import com.trio.gl.bean.HondaH000Dealers;
import com.trio.gl.hibernate.TrioGenericDao;

/** @author Saifi Ahmada Mar 23, 2013 4:16:44 PM  **/

public interface HondaH000DealersDao extends TrioGenericDao<HondaH000Dealers> {

}

