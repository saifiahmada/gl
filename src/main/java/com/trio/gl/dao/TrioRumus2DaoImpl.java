package com.trio.gl.dao;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trio.gl.bean.TrioRumus2;
import com.trio.gl.hibernate.TrioHibernateDaoSupport;

/**
 * @author Gusti Arya 11:34:15 AM Dec 14, 2013
 */

@Service
public class TrioRumus2DaoImpl extends TrioHibernateDaoSupport implements TrioRumus2Dao {

	@Override
	public void saveOrUpdate(TrioRumus2 domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void save(TrioRumus2 domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	@Transactional(readOnly=false)
	public void update(TrioRumus2 domain, String user) {
		getHibernateTemplate().getSessionFactory().getCurrentSession().update(domain);
	}

	@Override
	public void delete(TrioRumus2 domain) {
		// TODO Auto-generated method stub
		
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<TrioRumus2> findAll() {
		String hql = "from TrioRumus2";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		
		return q.list();
	}

	@Override
	public List<TrioRumus2> findByExample(TrioRumus2 domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TrioRumus2> findByCriteria(TrioRumus2 domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TrioRumus2 findByPrimaryKey(TrioRumus2 domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional(readOnly=false)
	public void saveByList(List<TrioRumus2> lists, String user) {
		String hql = "delete from TrioRumus2";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		q.executeUpdate();
		
		for(TrioRumus2 current : lists){
			getHibernateTemplate().getSessionFactory().getCurrentSession().save(current);
		}
		
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<TrioRumus2> listSumNilai() {
		String hql = "select new TrioRumus2(t.kdHsl, sum(t.nilai)) from TrioRumus2 t group by t.kdHsl";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		
		return q.list();
	}

	@Override
	@Transactional(readOnly=false)
	public void saveOrUpdateByList(List<TrioRumus2> list, String user) {
		for(TrioRumus2 current : list){
			getHibernateTemplate().getSessionFactory().getCurrentSession().saveOrUpdate(current);
		}
	}
}
