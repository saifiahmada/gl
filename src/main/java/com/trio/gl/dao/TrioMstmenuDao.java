package com.trio.gl.dao;

import java.util.List;

import com.trio.gl.bean.TrioMstmenu;
import com.trio.gl.hibernate.TrioGenericDao;

/** @author Saifi Ahmada Feb 7, 2013 4:47:13 PM  **/

public interface TrioMstmenuDao extends TrioGenericDao<TrioMstmenu> { 
	public List<TrioMstmenu> getListMenuByUser(String user);
}

