package com.trio.gl.dao;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trio.gl.bean.TrioMstrunnum;
import com.trio.gl.hibernate.TrioHibernateDaoSupport;
import com.trio.gl.util.TrioDateUtil;

/** @author Saifi Ahmada Mar 14, 2013 4:39:33 PM  **/

@Service
public class TrioMstrunnumDaoImpl extends TrioHibernateDaoSupport implements TrioMstrunnumDao {

	public void saveOrUpdate(TrioMstrunnum domain, String user) {
		// TODO , masbro
		
		
	}

	public void save(TrioMstrunnum domain, String user) {
		// TODO , masbro
		
		
	}

	public void update(TrioMstrunnum domain, String user) {
		// TODO , masbro
		
		
	}

	public void delete(TrioMstrunnum domain) {
		// TODO , masbro
		
		
	}

	public List<TrioMstrunnum> findAll() {
		// TODO , masbro
		
		return null;
	}

	public List<TrioMstrunnum> findByExample(TrioMstrunnum domain) {
		// TODO , masbro
		
		return null;
	}

	public List<TrioMstrunnum> findByCriteria(TrioMstrunnum domain) {
		// TODO , masbro
		
		return null;
	}

	public TrioMstrunnum findByPrimaryKey(TrioMstrunnum domain) {
		// TODO , masbro
		
		return null;
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly=false)
	public int getRunningNumber(String idDoc, String reseter, String user) {
		// TODO , masbro
		String sq = "from TrioMstrunnum r where r.trioMstrunnumPK.idDoc = :idDoc and r.trioMstrunnumPK.reseter = :reseter ";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(sq);
		q.setString("idDoc", idDoc);
		q.setString("reseter", reseter);
		List<TrioMstrunnum> list = q.list();
		TrioMstrunnum runnum;
		if (list.size() == 0){
			runnum = new TrioMstrunnum(idDoc,reseter);
			runnum.setRunnum(0);
			runnum.getUserTrailing().setVcreaby(user);
			runnum.getUserTrailing().setDcrea(TrioDateUtil.getLongSysDate());
		} else {
			runnum = list.get(0);
			runnum.getUserTrailing().setVmodiby(user);
			runnum.getUserTrailing().setDmodi(TrioDateUtil.getLongSysDate());
		}
		Integer temp = runnum.getRunnum();
		temp = temp + 1;
		runnum.setRunnum(temp);
		
		getHibernateTemplate().saveOrUpdate(runnum);
		
		return runnum.getRunnum();
	}
	
	

}

