package com.trio.gl.dao;

import com.trio.gl.bean.TrioMstkonfigurasi;
import com.trio.gl.hibernate.TrioGenericDao;

/** @author Saifi Ahmada Mar 22, 2013 9:17:11 AM  **/

public interface TrioMstkonfigurasiDao extends TrioGenericDao<TrioMstkonfigurasi> {
	public TrioMstkonfigurasi findById(String idKonfig);
}

