package com.trio.gl.dao;

import java.util.List;

import org.hibernate.SQLQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trio.gl.bean.TrioTarikpartretur;
import com.trio.gl.bean.TrioTarikpartsale;
import com.trio.gl.hibernate.TrioHibernateDaoSupport;

/** @author Saifi Ahmada Apr 2, 2013 9:35:55 PM  **/

@Service
public class TrioTarikoracleDaoImpl extends TrioHibernateDaoSupport implements TrioTarikoracleDao  {

	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<TrioTarikpartsale> getListTriotarikpartsale(String kdDlr, String tgl){
		String sQ = 
		  " SELECT "+ 
		  " distinct a.FAK_NO, a.FAK_DATE, a.PAYMENT_TYPE ket, "+ 
		  " a.TOT_PRICE jml_trm, b.KD_DLR, b.DLR_NAMA,  "+
		  " substr(a.fak_no, 1, 23)||' '||substr(b.kd_dlr, 1, 3)||' '||b.dlr_nama nm_tran, "+ 
		  " b.CABANG, a.KD_TRANS, decode(d.KELBRG_ID_KELBRG,'OIL','OIL','NONOIL') kel_brg  "+
		  " FROM "+
		  " HONDA_H000_DEALERS b, "+ 
		  " HONDA_H300_FAKTURS a,  "+
		  " HONDA_H300_DETAIL_FAKTURS c, "+ 
		  " HONDA_H300_MASTER_PARTS d "+
		  " WHERE   "+
		  " b.KD_DLR = a.DEALER_KD_DLR AND "+ 
		  " a.FAK_NO = c.FAKTUR_FAK_NO AND  "+
		  " c.PART_PART_NO = d.PART_NO AND  "+
		  " a.DEALER_KD_DLR = '"+kdDlr+"' and "+
		  " to_char(a.FAK_DATE,'YYYY-MM-DD') = '"+tgl+"' and "+
		  " (a.STATUS NOT LIKE ('B') OR  a.STATUS IS NULL) order by a.fak_no ";
		
		System.out.println("kd dlr "+kdDlr);
		System.out.println("tgl "+tgl);
		System.out.println("Squery "+sQ);
		
		SQLQuery sqlQ = getHibernateTemplate().getSessionFactory().
						getCurrentSession().createSQLQuery(sQ);
		sqlQ.addEntity(TrioTarikpartsale.class);
		List<TrioTarikpartsale> list = sqlQ.list();
		
		return list;
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<TrioTarikpartretur> getListTriotarikpartretur(String kdDlr,
			String tgl) {
		// TODO , masbro
		
		String sQ = 
				" SELECT "+ 
				" 	retur_no, retur_date, payment_type as ket, cust_code as kd_dlr, dlr_nama, kd_trans, nm_tran , cabang, kel_brg , sum(jml_trm) as jml_trm "+ 
				" FROM ( "+
				" 	SELECT  "+
				" 		retur_no, retur_date, cust_code, payment_type, cabang, e.KD_TRANS, substr(e.fak_no, 1, 23)||' '||substr(c.kd_dlr, 1, 3)||' '||c.dlr_nama nm_tran,  dlr_nama, decode(kelbrg_id_kelbrg,'OIL','OIL','NONOIL') kel_brg, SUM(b.net_price) AS jml_trm "+
				" 	FROM  "+
				" 		HONDA_H300_RETSALS a, "+ 
				" 		TRIO_H300_DETAIL_RETSALS b, "+
				" 		HONDA_H000_DEALERS c,  "+
				" 		honda_h300_master_parts f, "+
				" 		HONDA_H300_FAKTURS e "+
				" 	WHERE "+
				" 		b.fak_no=e.fak_no AND "+ 
				" 		a.RETUR_NO = b.RETSAL_RETUR_NO AND "+ 
				" 		c.KD_DLR = a.CUST_CODE AND b.part_part_no=f.part_no AND "+
				" 		to_char(a.RETUR_DATE,'YYYY-MM-DD') = '"+tgl+"' AND "+
				" 		cust_code = '"+kdDlr+"' "+
				" 	GROUP BY retur_no, retur_date, cust_code, e.KD_TRANS, payment_type, cabang,  dlr_nama, kelbrg_id_kelbrg, e.FAK_NO, c.kd_dlr "+
				" ) temp  "+
				" GROUP BY retur_no, retur_date, cust_code, payment_type, KD_TRANS, cabang, dlr_nama, kel_brg, nm_tran " ;
		
		System.out.println("kd dlr "+kdDlr);
		System.out.println("tgl "+tgl);
		System.out.println("Squery "+sQ);
		
		SQLQuery sqlQ = getHibernateTemplate().getSessionFactory().
						getCurrentSession().createSQLQuery(sQ);
		sqlQ.addEntity(TrioTarikpartretur.class);
		List<TrioTarikpartretur> list = sqlQ.list();
		return list;
	}
	
}

