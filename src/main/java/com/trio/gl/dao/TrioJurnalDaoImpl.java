package com.trio.gl.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trio.gl.bean.TrioJurnal;
import com.trio.gl.hibernate.TrioHibernateDaoSupport;
import com.trio.gl.util.TrioDateConv;
import com.trio.gl.util.TrioDateUtil;
import com.trio.gl.util.TrioStringUtil;

/** @author Saifi Ahmada Mar 14, 2013 11:05:19 AM  **/

@Service
public class TrioJurnalDaoImpl extends TrioHibernateDaoSupport implements TrioJurnalDao {
	
	@Transactional(readOnly=false)
	public String saveTransaction(List<TrioJurnal> listJurnal,String jenis, String user) {
		
		String idDoc = jenis;
		String reseter = TrioDateConv.format(new Date(), "yyyyMM");
		int no = getMasterFacade().getTrioMstrunnumDao().getRunningNumber(idDoc, reseter, user);
        String idJurnal = TrioStringUtil.getFormattedRunno(idDoc, Integer.valueOf(no));
		
        for (TrioJurnal jurnal : listJurnal){
        	jurnal.getTrioJurnalPK().setIdJurnal(idJurnal);
        	jurnal.getUserTrailing().setVcreaby(user);
        	jurnal.getUserTrailing().setDcrea(TrioDateUtil.getLongSysDate());
        	getHibernateTemplate().save(jurnal);
        }
        
		return idJurnal;
	}

	@Transactional(readOnly=false)
	public String savePelunasan(List<TrioJurnal> listJurnal,String jenis, String user) {

		String idDoc = jenis;
		String reseter = TrioDateConv.format(new Date(), "yyyyMM");
		int no = getMasterFacade().getTrioMstrunnumDao().getRunningNumber(idDoc, reseter, user);
		String idJurnal = TrioStringUtil.getFormattedRunno(idDoc, Integer.valueOf(no));

		//melakukan update status jurnal yang dilunasi
		for(TrioJurnal current : listJurnal){
			System.out.println("idperk TrioJurnalDaoImpl = " + current.getTrioJurnalPK().getIdJurnal());
			if(current.getTrioJurnalPK().getIdJurnal() != null){
				if(current.getTrioJurnalPK().getIdPerkiraan().contains("113")){
					current.setNoLunas(idJurnal);
					current.setLunas("*");
					current.setTglLunas(new Date());
					getHibernateTemplate().getSessionFactory().getCurrentSession().merge(current);
				}
			}
		}

		//Save data baru ke tabel. jika tidak ada jurnal yang dilunasi
		for (TrioJurnal jurnal : listJurnal){
			jurnal.getTrioJurnalPK().setIdJurnal(idJurnal);
			jurnal.setBlc("*");
			jurnal.setTglLunas(null);
			jurnal.setNoReff(null);
			jurnal.setNoLunas(null);
			jurnal.setLunas(null);
			jurnal.getUserTrailing().setVcreaby(user);
			jurnal.getUserTrailing().setDcrea(TrioDateUtil.getLongSysDate());
			getHibernateTemplate().getSessionFactory().getCurrentSession().save(jurnal);
		}


		return idJurnal;
	}

	@Transactional(readOnly=false)
	public void saveOrUpdateAll(List<TrioJurnal> listJurnal, String user){
		TrioJurnal entity = null;
		for (TrioJurnal jurnal : listJurnal){
			entity = (TrioJurnal) getHibernateTemplate().getSessionFactory()
					.getCurrentSession().get(TrioJurnal.class, jurnal.getTrioJurnalPK());
			if (entity == null){
				jurnal.getUserTrailing().setVcreaby(user);
				jurnal.getUserTrailing().setDcrea(TrioDateUtil.getLongSysDate());
				getHibernateTemplate().save(jurnal);
			}else{
				jurnal.getUserTrailing().setVmodiby(user);
				jurnal.getUserTrailing().setDmodi(TrioDateUtil.getLongSysDate());
				getHibernateTemplate().merge(jurnal);
			}
		}
	}

	public void saveOrUpdate(TrioJurnal domain, String user) {
		// TODO , masbro


	}

	public void save(TrioJurnal domain, String user) {
		// TODO , masbro


	}

	public void update(TrioJurnal domain, String user) {
		// TODO , masbro


	}

	public void delete(TrioJurnal domain) {
		// TODO , masbro


	}

	public List<TrioJurnal> findAll() {
		// TODO , masbro

		return null;
	}

	public List<TrioJurnal> findByExample(TrioJurnal domain) {
		// TODO , masbro

		return null;
	}

	public List<TrioJurnal> findByCriteria(TrioJurnal domain) {
		// TODO , masbro

		return null;
	}

	public TrioJurnal findByPrimaryKey(TrioJurnal domain) {
		// TODO , masbro

		return null;
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<TrioJurnal> getListJurnalByIdJurnal(String idJurnal){
		String sQ = "select * from trio_jurnal where ID_JURNAL = '"+idJurnal+"'";
		SQLQuery sqlQ = getHibernateTemplate().getSessionFactory().
				getCurrentSession().createSQLQuery(sQ);
		sqlQ.addEntity(TrioJurnal.class);
		List<TrioJurnal> list = sqlQ.list();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<TrioJurnal> getListJurnalByTanggal(Date tglJurnal){
		String sQ = "select j from TrioJurnal j where j.tglJurnal = :tglJurnal ";

		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(sQ);
		q.setDate("tglJurnal", tglJurnal);
		return q.list();
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<TrioJurnal> getListJurnalByRangeTanggal(Date tglAwal, Date tglAkhir){
		String sQ = "select j from TrioJurnal j where j.tglJurnal between :tglAwal and :tglAkhir ";

		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(sQ);
		q.setDate("tglAwal", tglAwal);
		q.setDate("tglAkhir", tglAkhir);
		System.out.println("Tgl awal "+tglAwal);
		System.out.println("Tgl akhir "+tglAkhir);
		System.out.println("list berjumlah "+q.list());
		return q.list();
	}

	//dipanggil di class JurnalMemoRefAll
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<TrioJurnal> getListJurnalMemorial() {
		String hql = "from TrioJurnal a where a.trioJurnalPK.idJurnal like 'JME%' group by a.trioJurnalPK.idJurnal";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);

		return q.list();
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<TrioJurnal> getListJurnalUmum() {
		String hql = "from TrioJurnal a where a.trioJurnalPK.idJurnal like 'JUM%'";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);

		return q.list();
	}

	//Dipanggil di class RptHutangStnkVM
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<TrioJurnal> getListJurnalForStnk(Date tglAwal, Date tglAkhir, String noPerk) {
		Criteria crit = getHibernateTemplate().getSessionFactory()
				.getCurrentSession().createCriteria(TrioJurnal.class);
		crit.add(Restrictions.like("noReff", "FAK%"));
		crit.add(Restrictions.eq("trioJurnalPK.idPerkiraan", noPerk));
		crit.add(Restrictions.gt("kredit", new BigDecimal(0)));
		crit.add(Restrictions.between("tglJurnal", tglAwal, tglAkhir));

		return crit.list();
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<TrioJurnal> getListJurnalByFketPotOff(Date tglAwal,
			Date tglAkhir, String noPerk) {
		Criteria crit = getHibernateTemplate().getSessionFactory()
				.getCurrentSession().createCriteria(TrioJurnal.class);
		crit.add(Restrictions.like("ket", "POT OFF%"));
		crit.add(Restrictions.eq("trioJurnalPK.idPerkiraan", noPerk));
		crit.add(Restrictions.between("tglJurnal", tglAwal, tglAkhir));

		return crit.list();
	}

	@Transactional(readOnly=true)
	public BigDecimal getKredit(String idJurnal, String idPerkiraan) {
		String hql = "select coalesce(t.kredit,0) from TrioJurnal t " 
				+ "where t.trioJurnalPK.idJurnal = :idJurnal " 
				+ "and t.trioJurnalPK.idPerkiraan = :idPerkiraan";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		q.setString("idJurnal", idJurnal);
		q.setString("idPerkiraan", idPerkiraan);

		return (BigDecimal) q.uniqueResult();
	}

	@Transactional(readOnly=true)
	public BigDecimal getDebit(String idJurnal, String idPerkiraan) {
		String hql = "select coalesce(t.debet) from TrioJurnal t " 
				+ "where t.trioJurnalPK.idJurnal = :idJurnal " 
				+ "and t.trioJurnalPK.idPerkiraan = :idPerkiraan";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		q.setString("idJurnal", idJurnal);
		q.setString("idPerkiraan", idPerkiraan);

		return (BigDecimal) q.uniqueResult();
	}


	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<TrioJurnal> getListJurnalByNoReff(String noReff) {
		Criteria crit = getHibernateTemplate().getSessionFactory()
				.getCurrentSession().createCriteria(TrioJurnal.class);
		crit.add(Restrictions.eq("noReff", noReff));
		crit.add(Restrictions.isNotNull("noMesin"));
		return crit.list();
	}

	@Override
	@Transactional(readOnly=true)
	public BigDecimal getOngkosAngkut(String idPerk,String tgl) {
		String hql = "select sum(debet) from TrioJurnal t "
				+ "where t.trioJurnalPK.idPerkiraan = :idPerk "
				+ "and DATE_FORMAT(tglJurnal, '%Y%m') = :tgl ";
		Query q = getHibernateTemplate().getSessionFactory()
				.getCurrentSession().createQuery(hql);
		q.setString("idPerk", idPerk);
		q.setString("tgl", tgl);

		return (BigDecimal) q.uniqueResult();
	}

	//Dipakai di PopupPelunasanVM
	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<TrioJurnal> getNotLunasJurnal(String idPerk, String idPerk2, String idPerk3) {
		String hql = "";
		Query q = null;

		if(idPerk2 == null && idPerk3 == null){
			hql = "from TrioJurnal a where lunas is null and a.trioJurnalPK.idPerkiraan = :idPerk";
			q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
			q.setString("idPerk", idPerk);
		}else if(idPerk2 != null && idPerk3 == null){
			hql = "from TrioJurnal a where lunas is null and a.trioJurnalPK.idPerkiraan in (:idPerk, :idPerk2)";
			q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
			q.setString("idPerk", idPerk);
			q.setString("idPerk2", idPerk2);
		}else if (idPerk2 == null && idPerk3 != null){
			hql = "from TrioJurnal a where lunas is null and a.trioJurnalPK.idPerkiraan in (:idPerk, :idPerk3)";
			q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
			q.setString("idPerk", idPerk);
			q.setString("idPerk3", idPerk3);
		}else{
			hql = "from TrioJurnal a where lunas is null and a.trioJurnalPK.idPerkiraan in (:idPerk, :idPerk2, :idPerk3)";
			q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
			q.setString("idPerk", idPerk);
			q.setString("idPerk2", idPerk2);
			q.setString("idPerk3", idPerk3);
		}

		return q.list();
	}

	//dipakai di report Jurnal Pelunasan
	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<TrioJurnal> getJurnalLunas(Date tglAwal, Date tglAkhir,boolean cek,boolean cek2, String idPerkiraan) {
		String hql = "";
		//ceklist pertama belum lunas tidak dicentang
		if(cek == false){
			//ceklist kedua per nomor perkiraan tidak dicentang
			if(cek2==false){
				hql = "from TrioJurnal a where a.lunas = '*' " 
						+ "and substring(a.trioJurnalPK.idJurnal,1,1) = 'J' " 
						+ "and (a.debet > 0 or a.kredit > 0) " 
						+ "and a.tglLunas between :tglAwal and :tglAkhir";
				Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
				q.setDate("tglAwal", tglAwal);
				q.setDate("tglAkhir", tglAkhir);
				return q.list();
				//ceklist kedua per nomor perkiraan dicentang
			}else{
				hql = "from TrioJurnal a where a.lunas = '*' " 
						+ "and substring(a.trioJurnalPK.idJurnal,1,1) = 'J' " 
						+ "and (a.debet > 0 or a.kredit > 0) " 
						+ "and a.trioJurnalPK.idPerkiraan = :idPerkiraan "
						+ "and a.tglLunas between :tglAwal and :tglAkhir";
				Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
				q.setDate("tglAwal", tglAwal);
				q.setDate("tglAkhir", tglAkhir);
				q.setString("idPerkiraan", idPerkiraan);
				return q.list();
			}
			//ceklist pertama belum lunas dicentang	
		}else{
			//ceklist kedua per nomor perkiraan tidak dicentang
			if(cek2 == false){
				hql = "from TrioJurnal a where a.lunas is null " 
						+ "and substring(a.trioJurnalPK.idJurnal,1,1) = 'J' " 
						+ "and substring(a.ket,1,7) <> 'PIUTANG' " 
						+ "and (a.debet > 0 or a.kredit > 0)"; 
				Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
				return q.list();

				//ceklist kedua per nomor perkiraan dicentang	
			}else{
				hql = "from TrioJurnal a where a.lunas is null " 
						+ "and substring(a.trioJurnalPK.idJurnal,1,1) = 'J' " 
						+ "and substring(a.ket,1,7) <> 'PIUTANG' " 
						+ "and a.trioJurnalPK.idPerkiraan = :idPerkiraan " 
						+ "and (a.debet > 0 or a.kredit > 0)"; 
				Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
				q.setString("idPerkiraan", idPerkiraan);
				return q.list();
			}
		}
	}

	//digunakan di form cek penjualan
	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<TrioJurnal> getPotCekPenjualan(String idPerk, String idPerk2,
			String noFak) {

		String hql = "select new TrioJurnal(sum(a.debet)) from TrioJurnal a where (a.trioJurnalPK.idPerkiraan= :idPerk or a.trioJurnalPK.idPerkiraan= :idPerk2) " 
				+ "and a.debet > 0 "
				+ "and (substring(a.ket,-15) = :noFak or a.noReff= :noFak)"
				+ "group by a.trioJurnalPK.idPerkiraan";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		q.setString("idPerk", idPerk);
		q.setString("idPerk2", idPerk2);
		q.setString("noFak", noFak);
		return q.list();
	}

	//digunakan di form cek penjualan
	@Override
	@Transactional(readOnly=true)
	public BigDecimal getNettCekPenjualan(String idPerk1, String noFak) {
		System.out.println("nofak dalam dao = " + noFak);
		String hql = "select sum(a.debet) from TrioJurnal a where a.trioJurnalPK.idPerkiraan= :idPerk "
				+ "and a.ket like '%"+noFak+"%' "
				+ "and a.debet > 0 "
				+ "group by a.trioJurnalPK.idPerkiraan";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		q.setString("idPerk", idPerk1);
		BigDecimal bd = (BigDecimal) q.uniqueResult();
		return bd;
	}

	//digunakan di form cekBalanceJurnal
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	@Override
	public List<TrioJurnal> getBalanceJurnal(String bulan, String tahun) {
		String hql = "select new TrioJurnal(a.trioJurnalPK.idJurnal,sum(a.debet), sum(a.kredit)) from TrioJurnal a "
				+ "where month(a.tglJurnal) = :bulan "
				+ "and year(a.tglJurnal) = :tahun "
				+ "group by a.trioJurnalPK.idJurnal";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		q.setString("bulan", bulan);
		q.setString("tahun", tahun);
		
		return q.list();
	}
}

