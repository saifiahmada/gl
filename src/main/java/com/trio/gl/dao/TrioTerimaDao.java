package com.trio.gl.dao;

import java.util.List;

import com.trio.gl.bean.TrioTerima;
import com.trio.gl.hibernate.TrioGenericDao;

/** @author Saifi Ahmada Aug 28, 2013 5:17:12 PM  **/

public interface TrioTerimaDao extends TrioGenericDao<TrioTerima> {
	public void prosesDataUploadLKHCabang1(List<TrioTerima> listTerima, String user);
	public void prosesDataUploadLKHCabang2(List<TrioTerima> listTerima, String user);
}

