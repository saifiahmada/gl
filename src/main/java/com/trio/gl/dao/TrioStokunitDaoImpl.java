package com.trio.gl.dao;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trio.gl.bean.TrioStokunit;
import com.trio.gl.hibernate.TrioHibernateDaoSupport;
import com.trio.gl.util.TrioDateUtil;

/** @author Saifi Ahmada Mar 23, 2013 3:49:59 PM  **/

@Service
public class TrioStokunitDaoImpl extends TrioHibernateDaoSupport implements TrioStokunitDao {

	@Transactional(readOnly=false)
	public void saveList(List<TrioStokunit> list, String user){
		for (TrioStokunit unit : list){
			unit.getUserTrailing().setVcreaby(user);
			unit.getUserTrailing().setDcrea(TrioDateUtil.getLongSysDate());
			getHibernateTemplate().save(unit);
		}
	}
	
	public void saveOrUpdate(TrioStokunit domain, String user) {
		// TODO , masbro
		
		
	}

	public void save(TrioStokunit domain, String user) {
		// TODO , masbro
		
		
	}

	public void update(TrioStokunit domain, String user) {
		// TODO , masbro
		
		
	}

	public void delete(TrioStokunit domain) {
		// TODO , masbro
		
		
	}

	public List<TrioStokunit> findAll() {
		// TODO , masbro
		
		return null;
	}

	public List<TrioStokunit> findByExample(TrioStokunit domain) {
		// TODO , masbro
		
		return null;
	}

	public List<TrioStokunit> findByCriteria(TrioStokunit domain) {
		// TODO , masbro
		
		return null;
	}

	public TrioStokunit findByPrimaryKey(TrioStokunit domain) {
		// TODO , masbro
		
		return null;
	}

}

