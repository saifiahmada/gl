package com.trio.gl.dao;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trio.gl.bean.TrioStokTerima;
import com.trio.gl.hibernate.TrioHibernateDaoSupport;
import com.trio.gl.util.TrioDateUtil;

/** @author Saifi Ahmada Jul 18, 2013 3:48:30 PM  **/

@Service
public class TrioStokTerimaDaoImpl extends TrioHibernateDaoSupport implements TrioStokTerimaDao {

	public void saveOrUpdate(TrioStokTerima domain, String user) {
		// TODO , masbro
		
		
	}

	@Transactional(readOnly=false)
	public void save(TrioStokTerima domain, String user) {
		// TODO , masbro
		domain.getUserTrailing().setDcrea(TrioDateUtil.getLongSysDate());
		domain.getUserTrailing().setVcreaby(user);
		getHibernateTemplate().save(domain);
		
	}

	public void update(TrioStokTerima domain, String user) {
		// TODO , masbro
		
		
	}

	public void delete(TrioStokTerima domain) {
		// TODO , masbro
		
		
	}

	public List<TrioStokTerima> findAll() {
		// TODO , masbro
		
		return null;
	}

	public List<TrioStokTerima> findByExample(TrioStokTerima domain) {
		// TODO , masbro
		
		return null;
	}

	public List<TrioStokTerima> findByCriteria(TrioStokTerima domain) {
		// TODO , masbro
		
		return null;
	}

	@Transactional(readOnly=true)
	public TrioStokTerima findByPrimaryKey(TrioStokTerima domain) {
		return (TrioStokTerima) getHibernateTemplate().getSessionFactory().getCurrentSession().get(TrioStokTerima.class, domain.getTrioStokTerimaPK());
	}

}

