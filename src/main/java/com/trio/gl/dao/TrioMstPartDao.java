package com.trio.gl.dao;

import com.trio.gl.bean.TrioMstPart;
import com.trio.gl.hibernate.TrioGenericDao;

/** @author Saifi Ahmada Jul 20, 2013 12:21:09 PM  **/

public interface TrioMstPartDao extends TrioGenericDao<TrioMstPart> {

}

