package com.trio.gl.dao;

import java.util.List;

import com.trio.gl.bean.TrioRmsNeracaAktivaSub;
import com.trio.gl.hibernate.TrioGenericDao;

/**
 * @author Gusti Arya 9:42:08 AM Nov 4, 2013
 */

public interface TrioRmsNeracaAktivaSubDao extends TrioGenericDao<TrioRmsNeracaAktivaSub> {
	
	public void saveByList(List<TrioRmsNeracaAktivaSub> lists, String username);
	public void saveOrUpdateByList(List<TrioRmsNeracaAktivaSub> lists, String username);
	public List<TrioRmsNeracaAktivaSub> getSumByHasil();
	
}
