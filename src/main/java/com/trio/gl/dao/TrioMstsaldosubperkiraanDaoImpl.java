package com.trio.gl.dao;

import java.math.BigDecimal;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trio.gl.bean.TrioMstsaldosubperkiraan;
import com.trio.gl.hibernate.TrioHibernateDaoSupport;
import com.trio.gl.util.TrioDateUtil;

/** @author Saifi Ahmada Apr 30, 2013 3:50:50 PM  **/

@Service
public class TrioMstsaldosubperkiraanDaoImpl extends TrioHibernateDaoSupport implements TrioMstsaldosubperkiraanDao {

	@Transactional(readOnly=false)
	public void saveOrUpdate(TrioMstsaldosubperkiraan domain, String user) {
		// TODO , masbro
		TrioMstsaldosubperkiraan objDB = (TrioMstsaldosubperkiraan) getHibernateTemplate().getSessionFactory().getCurrentSession()
				.get(TrioMstsaldosubperkiraan.class, domain.getTrioMstsaldosubperkiraanPK());
		
		if (objDB == null) {
			save(domain, user); 
		} else {
			update(domain, user); 
		}
		
	}

	@Transactional(readOnly=false)
	public void save(TrioMstsaldosubperkiraan domain, String user) {
		// TODO , masbro
		domain.getUserTrailing().setDcrea(TrioDateUtil.getLongSysDate());
		domain.getUserTrailing().setVcreaby(user);
		getHibernateTemplate().getSessionFactory().getCurrentSession().save(domain);
		
	}

	@Transactional(readOnly=false)
	public void update(TrioMstsaldosubperkiraan domain, String user) {
		// TODO , masbro
		domain.getUserTrailing().setDmodi(TrioDateUtil.getLongSysDate());
		domain.getUserTrailing().setVmodiby(user);
		getHibernateTemplate().getSessionFactory().getCurrentSession().update(domain); 
		
	}

	public void delete(TrioMstsaldosubperkiraan domain) {
		// TODO , masbro
		
		
	}

	public List<TrioMstsaldosubperkiraan> findAll() {
		// TODO , masbro
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<TrioMstsaldosubperkiraan> getAllByTahun(String tahun) {
		//select * from trio_mstsaldosubperkiraan where tahun = '2013' order by ID_PERKIRAAN, ID_SUB 
		String sQ = "from TrioMstsaldosubperkiraan s where s.trioMstsaldosubperkiraanPK.tahun = :tahun order by s.trioMstsaldosubperkiraanPK.idPerkiraan, s.trioMstsaldosubperkiraanPK.idSub  ";
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(sQ);
		query.setString("tahun", tahun);
		return query.list();
	}

	public List<TrioMstsaldosubperkiraan> findByExample(
			TrioMstsaldosubperkiraan domain) {
		// TODO , masbro
		
		return null;
	}

	public List<TrioMstsaldosubperkiraan> findByCriteria(
			TrioMstsaldosubperkiraan domain) {
		// TODO , masbro
		
		return null;
	}

	public TrioMstsaldosubperkiraan findByPrimaryKey(
			TrioMstsaldosubperkiraan domain) {
		// TODO , masbro
		
		return null;
	}
	
	@Transactional(readOnly=true)
	public BigDecimal getSaldoSubByIdPerkBulanTahun(String idPerk, String bulan, String tahun){
		String sQuery = "";
		if (bulan.equalsIgnoreCase("1")){
			sQuery = "select sum(s.bulSub0) from TrioMstsaldosubperkiraan s " +
					" where s.trioMstsaldosubperkiraanPK.idPerkiraan = :idPerk and s.trioMstsaldosubperkiraanPK.tahun = :tahun  ";
		}else if (bulan.equalsIgnoreCase("2")){
			sQuery = "select sum(s.bulSub1) from TrioMstsaldosubperkiraan s " +
					" where s.trioMstsaldosubperkiraanPK.idPerkiraan = :idPerk and s.trioMstsaldosubperkiraanPK.tahun = :tahun  ";
		}else if (bulan.equalsIgnoreCase("3")){
			sQuery = "select sum(s.bulSub2) from TrioMstsaldosubperkiraan s " +
					" where s.trioMstsaldosubperkiraanPK.idPerkiraan = :idPerk and s.trioMstsaldosubperkiraanPK.tahun = :tahun  ";
		}else if (bulan.equalsIgnoreCase("4")){
			sQuery = "select sum(s.bulSub3) from TrioMstsaldosubperkiraan s " +
					" where s.trioMstsaldosubperkiraanPK.idPerkiraan = :idPerk and s.trioMstsaldosubperkiraanPK.tahun = :tahun  ";
		}else if (bulan.equalsIgnoreCase("5")){
			sQuery = "select sum(s.bulSub4) from TrioMstsaldosubperkiraan s " +
					" where s.trioMstsaldosubperkiraanPK.idPerkiraan = :idPerk and s.trioMstsaldosubperkiraanPK.tahun = :tahun  ";
		}else if (bulan.equalsIgnoreCase("6")){
			sQuery = "select sum(s.bulSub5) from TrioMstsaldosubperkiraan s " +
					" where s.trioMstsaldosubperkiraanPK.idPerkiraan = :idPerk and s.trioMstsaldosubperkiraanPK.tahun = :tahun  ";
		}else if (bulan.equalsIgnoreCase("7")){
			sQuery = "select sum(s.bulSub6) from TrioMstsaldosubperkiraan s " +
					" where s.trioMstsaldosubperkiraanPK.idPerkiraan = :idPerk and s.trioMstsaldosubperkiraanPK.tahun = :tahun  ";
		}else if (bulan.equalsIgnoreCase("8")){
			sQuery = "select sum(s.bulSub7) from TrioMstsaldosubperkiraan s " +
					" where s.trioMstsaldosubperkiraanPK.idPerkiraan = :idPerk and s.trioMstsaldosubperkiraanPK.tahun = :tahun  ";
		}else if (bulan.equalsIgnoreCase("9")){
			sQuery = "select sum(s.bulSub8) from TrioMstsaldosubperkiraan s " +
					" where s.trioMstsaldosubperkiraanPK.idPerkiraan = :idPerk and s.trioMstsaldosubperkiraanPK.tahun = :tahun  ";
		}else if (bulan.equalsIgnoreCase("10")){
			sQuery = "select sum(s.bulSub9) from TrioMstsaldosubperkiraan s " +
					" where s.trioMstsaldosubperkiraanPK.idPerkiraan = :idPerk and s.trioMstsaldosubperkiraanPK.tahun = :tahun  ";
		}else if (bulan.equalsIgnoreCase("11")){
			sQuery = "select sum(s.bulSub10) from TrioMstsaldosubperkiraan s " +
					" where s.trioMstsaldosubperkiraanPK.idPerkiraan = :idPerk and s.trioMstsaldosubperkiraanPK.tahun = :tahun  ";
		}else if (bulan.equalsIgnoreCase("12")){
			sQuery = "select sum(s.bulSub11) from TrioMstsaldosubperkiraan s " +
					" where s.trioMstsaldosubperkiraanPK.idPerkiraan = :idPerk and s.trioMstsaldosubperkiraanPK.tahun = :tahun  ";
		}
		
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(sQuery);
		q.setString("idPerk", idPerk);
		q.setString("tahun", tahun);
		
		BigDecimal saldo = (BigDecimal) q.uniqueResult();
		return saldo == null ? new BigDecimal(0) : saldo ;
	}
	
	@Transactional(readOnly=false)
	public BigDecimal getSumSubSaldoByStatusAndPerkAndBulan(String tahun, String bulan,String idPerk, String status){
		String sQuery = "";
		if (bulan.equalsIgnoreCase("01")){
			sQuery = "select sum(s.bulSub0) from TrioMstsaldosubperkiraan s "+
					 " where s.trioMstsaldosubperkiraanPK.idPerkiraan = :idPerk and s.trioMstsaldosubperkiraanPK.tahun = :tahun " +
					 " and s.ketSub0 = :status ";
		} else if (bulan.equalsIgnoreCase("02")){
			sQuery = "select sum(s.bulSub1) from TrioMstsaldosubperkiraan s "+
					 " where s.trioMstsaldosubperkiraanPK.idPerkiraan = :idPerk and s.trioMstsaldosubperkiraanPK.tahun = :tahun " +
					 " and s.ketSub1 = :status ";
		} else if (bulan.equalsIgnoreCase("03")){
			sQuery = "select sum(s.bulSub2) from TrioMstsaldosubperkiraan s "+
					 " where s.trioMstsaldosubperkiraanPK.idPerkiraan = :idPerk and s.trioMstsaldosubperkiraanPK.tahun = :tahun " +
					 " and s.ketSub2 = :status ";
		} else if (bulan.equalsIgnoreCase("03")){
			sQuery = "select sum(s.bulSub2) from TrioMstsaldosubperkiraan s "+
					 " where s.trioMstsaldosubperkiraanPK.idPerkiraan = :idPerk and s.trioMstsaldosubperkiraanPK.tahun = :tahun " +
					 " and s.ketSub2 = :status ";
		} else if (bulan.equalsIgnoreCase("04")){
			sQuery = "select sum(s.bulSub3) from TrioMstsaldosubperkiraan s "+
					 " where s.trioMstsaldosubperkiraanPK.idPerkiraan = :idPerk and s.trioMstsaldosubperkiraanPK.tahun = :tahun " +
					 " and s.ketSub3 = :status ";
		} else if (bulan.equalsIgnoreCase("05")){
			sQuery = "select sum(s.bulSub4) from TrioMstsaldosubperkiraan s "+
					 " where s.trioMstsaldosubperkiraanPK.idPerkiraan = :idPerk and s.trioMstsaldosubperkiraanPK.tahun = :tahun " +
					 " and s.ketSub4 = :status ";
		} else if (bulan.equalsIgnoreCase("06")){
			sQuery = "select sum(s.bulSub5) from TrioMstsaldosubperkiraan s "+
					 " where s.trioMstsaldosubperkiraanPK.idPerkiraan = :idPerk and s.trioMstsaldosubperkiraanPK.tahun = :tahun " +
					 " and s.ketSub5 = :status ";
		} else if (bulan.equalsIgnoreCase("07")){
			sQuery = "select sum(s.bulSub6) from TrioMstsaldosubperkiraan s "+
					 " where s.trioMstsaldosubperkiraanPK.idPerkiraan = :idPerk and s.trioMstsaldosubperkiraanPK.tahun = :tahun " +
					 " and s.ketSub6 = :status ";
		} else if (bulan.equalsIgnoreCase("08")){
			sQuery = "select sum(s.bulSub7) from TrioMstsaldosubperkiraan s "+
					 " where s.trioMstsaldosubperkiraanPK.idPerkiraan = :idPerk and s.trioMstsaldosubperkiraanPK.tahun = :tahun " +
					 " and s.ketSub7 = :status ";
		} else if (bulan.equalsIgnoreCase("09")){
			sQuery = "select sum(s.bulSub8) from TrioMstsaldosubperkiraan s "+
					 " where s.trioMstsaldosubperkiraanPK.idPerkiraan = :idPerk and s.trioMstsaldosubperkiraanPK.tahun = :tahun " +
					 " and s.ketSub8 = :status ";
		} else if (bulan.equalsIgnoreCase("10")){
			sQuery = "select sum(s.bulSub9) from TrioMstsaldosubperkiraan s "+
					 " where s.trioMstsaldosubperkiraanPK.idPerkiraan = :idPerk and s.trioMstsaldosubperkiraanPK.tahun = :tahun " +
					 " and s.ketSub9 = :status ";
		} else if (bulan.equalsIgnoreCase("11")){
			sQuery = "select sum(s.bulSub10) from TrioMstsaldosubperkiraan s "+
					 " where s.trioMstsaldosubperkiraanPK.idPerkiraan = :idPerk and s.trioMstsaldosubperkiraanPK.tahun = :tahun " +
					 " and s.ketSub10 = :status ";
		} else if (bulan.equalsIgnoreCase("12")){
			sQuery = "select sum(s.bulSub11) from TrioMstsaldosubperkiraan s "+
					 " where s.trioMstsaldosubperkiraanPK.idPerkiraan = :idPerk and s.trioMstsaldosubperkiraanPK.tahun = :tahun " +
					 " and s.ketSub11 = :status ";
		}
		
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(sQuery);
		q.setString("idPerk", idPerk);
		q.setString("tahun", tahun);
		q.setSerializable("status", status);
		
		BigDecimal saldo = (BigDecimal) q.uniqueResult();
		return saldo == null ? new BigDecimal(0) : saldo ;
		
	}
	
	@Transactional(readOnly=false)
	public void updateSaldoByBulanAndTahun(String bulan, String tahun){
		
		String sQuery = "";
		if (bulan.equalsIgnoreCase("01")){
			sQuery = "update TrioMstsaldosubperkiraan s set s.bulSub1 = 0 where s.trioMstsaldosubperkiraanPK.tahun = :tahun "; 
		} else if (bulan.equalsIgnoreCase("02")){
			sQuery = "update TrioMstsaldosubperkiraan s set s.bulSub2 = 0 where s.trioMstsaldosubperkiraanPK.tahun = :tahun ";
		} else if (bulan.equalsIgnoreCase("03")){
			sQuery = "update TrioMstsaldosubperkiraan s set s.bulSub3 = 0 where s.trioMstsaldosubperkiraanPK.tahun = :tahun ";
		} else if (bulan.equalsIgnoreCase("04")){
			sQuery = "update TrioMstsaldosubperkiraan s set s.bulSub4 = 0 where s.trioMstsaldosubperkiraanPK.tahun = :tahun ";
		} else if (bulan.equalsIgnoreCase("05")){
			sQuery = "update TrioMstsaldosubperkiraan s set s.bulSub5 = 0 where s.trioMstsaldosubperkiraanPK.tahun = :tahun ";
		} else if (bulan.equalsIgnoreCase("06")){
			sQuery = "update TrioMstsaldosubperkiraan s set s.bulSub6 = 0 where s.trioMstsaldosubperkiraanPK.tahun = :tahun ";
		} else if (bulan.equalsIgnoreCase("07")){
			sQuery = "update TrioMstsaldosubperkiraan s set s.bulSub7 = 0 where s.trioMstsaldosubperkiraanPK.tahun = :tahun ";
		} else if (bulan.equalsIgnoreCase("08")){
			sQuery = "update TrioMstsaldosubperkiraan s set s.bulSub8 = 0 where s.trioMstsaldosubperkiraanPK.tahun = :tahun ";
		} else if (bulan.equalsIgnoreCase("09")){
			sQuery = "update TrioMstsaldosubperkiraan s set s.bulSub9 = 0 where s.trioMstsaldosubperkiraanPK.tahun = :tahun ";
		} else if (bulan.equalsIgnoreCase("10")){
			sQuery = "update TrioMstsaldosubperkiraan s set s.bulSub10 = 0 where s.trioMstsaldosubperkiraanPK.tahun = :tahun ";
		} else if (bulan.equalsIgnoreCase("11")){
			sQuery = "update TrioMstsaldosubperkiraan s set s.bulSub11 = 0 where s.trioMstsaldosubperkiraanPK.tahun = :tahun ";
		} else if (bulan.equalsIgnoreCase("12")){
			sQuery = "update TrioMstsaldosubperkiraan s set s.bulSub12 = 0 where s.trioMstsaldosubperkiraanPK.tahun = :tahun ";
		} 
		
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(sQuery);
		q.setString("tahun", tahun);
		q.executeUpdate();
				
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public void prosesSubSaldo(String bulan, String user) {
		// select s.* from trio_kumpul k
		// join trio_mstsaldosubperkiraan s on ( s.ID_PERKIRAAN = k.ID_PERKIRAAN and s.ID_SUB = k.ID_SUB )
		// order by k.ID_PERKIRAAN, k.ID_SUB
		String sQ = "select s.* from trio_kumpul k " +
				" join trio_mstsaldosubperkiraan s on ( s.ID_PERKIRAAN = k.ID_PERKIRAAN and s.ID_SUB = k.ID_SUB ) " +
				" order by k.ID_PERKIRAAN, k.ID_SUB" ;
		
		SQLQuery sqlQuery = getHibernateTemplate().getSessionFactory().getCurrentSession()
				.createSQLQuery(sQ);
		sqlQuery.addEntity(TrioMstsaldosubperkiraan.class);
		List<TrioMstsaldosubperkiraan> listSaldo = sqlQuery.list();
		String vKetAwal = "";
		for (TrioMstsaldosubperkiraan saldo : listSaldo) {
			if (saldo.getBulSub0().compareTo(BigDecimal.ZERO) != 0) {
				vKetAwal = saldo.getKetSub0();
			} else if (saldo.getBulSub1().compareTo(BigDecimal.ZERO) != 0) {
				vKetAwal = saldo.getKetSub1();
			} else if (saldo.getBulSub2().compareTo(BigDecimal.ZERO) != 0) {
				vKetAwal = saldo.getKetSub2();
			} else if (saldo.getBulSub3().compareTo(BigDecimal.ZERO) != 0) {
				vKetAwal = saldo.getKetSub3();
			} else if (saldo.getBulSub4().compareTo(BigDecimal.ZERO) != 0) {
				vKetAwal = saldo.getKetSub4();
			} else if (saldo.getBulSub5().compareTo(BigDecimal.ZERO) != 0) {
				vKetAwal = saldo.getKetSub5();
			} else if (saldo.getBulSub6().compareTo(BigDecimal.ZERO) != 0) {
				vKetAwal = saldo.getKetSub6();
			} else if (saldo.getBulSub7().compareTo(BigDecimal.ZERO) != 0) {
				vKetAwal = saldo.getKetSub7();
			} else if (saldo.getBulSub8().compareTo(BigDecimal.ZERO) != 0) {
				vKetAwal = saldo.getKetSub8();
			} else if (saldo.getBulSub9().compareTo(BigDecimal.ZERO) != 0) {
				vKetAwal = saldo.getKetSub9();
			} else if (saldo.getBulSub10().compareTo(BigDecimal.ZERO) != 0) {
				vKetAwal = saldo.getKetSub10();
			} else if (saldo.getBulSub11().compareTo(BigDecimal.ZERO) != 0) {
				vKetAwal = saldo.getKetSub11();
			} else {
				vKetAwal = "D";
			}
			
			BigDecimal kumpulTotal = getMasterFacade().getTrioKumpulDao()
					.getTotalByIdPerk(saldo.getTrioMstsaldosubperkiraanPK().getIdPerkiraan(), saldo.getTrioMstsaldosubperkiraanPK().getIdSub());
			
			if (bulan.equalsIgnoreCase("01")) {
				if (saldo != null) {
					String vKetSub = saldo.getKetSub0();
					BigDecimal nilai1 = BigDecimal.ZERO;
					if (saldo.getBulSub0().compareTo(BigDecimal.ZERO) != 0) { 
						
						if (saldo.getKetSub0().equalsIgnoreCase("D") || saldo.getKetSub0().length() == 0) {
							
							nilai1 = saldo.getBulSub0().add(kumpulTotal);
							if (nilai1.compareTo(BigDecimal.ZERO) == -1 ) {
								nilai1 = nilai1.multiply(new BigDecimal( -1 ));
								vKetSub = "K";
							}
						}
						if (saldo.getKetSub0().equalsIgnoreCase("K")) {
							BigDecimal vTotal = kumpulTotal;
							if (kumpulTotal.compareTo(BigDecimal.ZERO) == -1) {
								vTotal = kumpulTotal.multiply(new BigDecimal( -1 ));
								nilai1 = saldo.getBulSub0().add(vTotal);
							} else {
								nilai1 = saldo.getBulSub0().subtract(vTotal);
							}
							if (nilai1.compareTo(BigDecimal.ZERO) == -1){
								nilai1 = nilai1.multiply(new BigDecimal( -1 ));
								vKetSub = "D";
							}
						}
						
						saldo.setBulSub1(saldo.getBulSub1().add(nilai1));
						saldo.setKetSub1(vKetSub);
					} else {
						nilai1 = saldo.getBulSub0().add(kumpulTotal);
						if (vKetAwal.equalsIgnoreCase("D")) {
							if (nilai1.compareTo(BigDecimal.ZERO) == -1) {
								nilai1 = nilai1.multiply(new BigDecimal( -1 ));
								vKetSub = "K";
							} else {
								vKetSub = vKetAwal;
							}
						}
						if (vKetAwal.equalsIgnoreCase("K")) {
							if (nilai1.compareTo(BigDecimal.ZERO) == -1) {
								nilai1 = nilai1.multiply(new BigDecimal( -1 ));
								vKetSub = "K";
							} else {
								vKetSub = vKetAwal;
							}
						}
						saldo.setBulSub1(saldo.getBulSub1().add(nilai1));
						saldo.setKetSub1(vKetSub);
					}
				}
			} 
			//bulan 02
			if (bulan.equalsIgnoreCase("02")) {
				if (saldo != null) {
					String vKetSub = saldo.getKetSub1();
					BigDecimal nilai1 = BigDecimal.ZERO;
					if (saldo.getBulSub1().compareTo(BigDecimal.ZERO) != 0) { // bulSub1 <> 0
						
						if (saldo.getKetSub1().equalsIgnoreCase("D") || saldo.getKetSub1().length() == 0) {
							
							nilai1 = saldo.getBulSub1().add(kumpulTotal);
							if (nilai1.compareTo(BigDecimal.ZERO) == -1 ) {
								nilai1 = nilai1.multiply(new BigDecimal( -1 ));
								vKetSub = "K";
							}
						}
						if (saldo.getKetSub1().equalsIgnoreCase("K")) {
							BigDecimal vTotal = kumpulTotal;
							if (kumpulTotal.compareTo(BigDecimal.ZERO) == -1) {
								vTotal = kumpulTotal.multiply(new BigDecimal( -1 ));
								nilai1 = saldo.getBulSub1().add(vTotal);
							} else {
								nilai1 = saldo.getBulSub1().subtract(vTotal);
							}
							if (nilai1.compareTo(BigDecimal.ZERO) == -1){
								nilai1 = nilai1.multiply(new BigDecimal( -1 ));
								vKetSub = "D";
							}
						}
						
						saldo.setBulSub2(saldo.getBulSub2().add(nilai1));
						saldo.setKetSub2(vKetSub);
					} else {
						nilai1 = saldo.getBulSub1().add(kumpulTotal);
						if (vKetAwal.equalsIgnoreCase("D")) {
							if (nilai1.compareTo(BigDecimal.ZERO) == -1) {
								nilai1 = nilai1.multiply(new BigDecimal( -1 ));
								vKetSub = "K";
							} else {
								vKetSub = vKetAwal;
							}
						}
						if (vKetAwal.equalsIgnoreCase("K")) {
							if (nilai1.compareTo(BigDecimal.ZERO) == -1) {
								nilai1 = nilai1.multiply(new BigDecimal( -1 ));
								vKetSub = "K";
							} else {
								vKetSub = vKetAwal;
							}
						}
						saldo.setBulSub2(saldo.getBulSub2().add(nilai1));
						saldo.setKetSub2(vKetSub);
					}
				}
			}
			//bulan 03
			if (bulan.equalsIgnoreCase("03")) {
				if (saldo != null) {
					String vKetSub = saldo.getKetSub2();
					BigDecimal nilai1 = BigDecimal.ZERO;
					if (saldo.getBulSub2().compareTo(BigDecimal.ZERO) != 0) { 
						
						if (saldo.getKetSub2().equalsIgnoreCase("D") || saldo.getKetSub2().length() == 0) {
							
							nilai1 = saldo.getBulSub2().add(kumpulTotal);
							if (nilai1.compareTo(BigDecimal.ZERO) == -1 ) {
								nilai1 = nilai1.multiply(new BigDecimal( -1 ));
								vKetSub = "K";
							}
						}
						if (saldo.getKetSub2().equalsIgnoreCase("K")) {
							BigDecimal vTotal = kumpulTotal;
							if (kumpulTotal.compareTo(BigDecimal.ZERO) == -1) {
								vTotal = kumpulTotal.multiply(new BigDecimal( -1 ));
								nilai1 = saldo.getBulSub2().add(vTotal);
							} else {
								nilai1 = saldo.getBulSub2().subtract(vTotal);
							}
							if (nilai1.compareTo(BigDecimal.ZERO) == -1){
								nilai1 = nilai1.multiply(new BigDecimal( -1 ));
								vKetSub = "D";
							}
						}
						
						saldo.setBulSub3(saldo.getBulSub3().add(nilai1));
						saldo.setKetSub3(vKetSub);
					} else {
						nilai1 = saldo.getBulSub2().add(kumpulTotal);
						if (vKetAwal.equalsIgnoreCase("D")) {
							if (nilai1.compareTo(BigDecimal.ZERO) == -1) {
								nilai1 = nilai1.multiply(new BigDecimal( -1 ));
								vKetSub = "K";
							} else {
								vKetSub = vKetAwal;
							}
						}
						if (vKetAwal.equalsIgnoreCase("K")) {
							if (nilai1.compareTo(BigDecimal.ZERO) == -1) {
								nilai1 = nilai1.multiply(new BigDecimal( -1 ));
								vKetSub = "K";
							} else {
								vKetSub = vKetAwal;
							}
						}
						saldo.setBulSub3(saldo.getBulSub3().add(nilai1));
						saldo.setKetSub3(vKetSub);
					}
				}
			}
			//bulan 04
			if (bulan.equalsIgnoreCase("04")) {
				if (saldo != null) {
					String vKetSub = saldo.getKetSub3();
					BigDecimal nilai1 = BigDecimal.ZERO;
					if (saldo.getBulSub3().compareTo(BigDecimal.ZERO) != 0) { 
						
						if (saldo.getKetSub3().equalsIgnoreCase("D") || saldo.getKetSub3().length() == 0) {
							
							nilai1 = saldo.getBulSub3().add(kumpulTotal);
							if (nilai1.compareTo(BigDecimal.ZERO) == -1 ) {
								nilai1 = nilai1.multiply(new BigDecimal( -1 ));
								vKetSub = "K";
							}
						}
						if (saldo.getKetSub3().equalsIgnoreCase("K")) {
							BigDecimal vTotal = kumpulTotal;
							if (kumpulTotal.compareTo(BigDecimal.ZERO) == -1) {
								vTotal = kumpulTotal.multiply(new BigDecimal( -1 ));
								nilai1 = saldo.getBulSub3().add(vTotal);
							} else {
								nilai1 = saldo.getBulSub3().subtract(vTotal);
							}
							if (nilai1.compareTo(BigDecimal.ZERO) == -1){
								nilai1 = nilai1.multiply(new BigDecimal( -1 ));
								vKetSub = "D";
							}
						}
						
						saldo.setBulSub4(saldo.getBulSub4().add(nilai1));
						saldo.setKetSub4(vKetSub);
					} else {
						nilai1 = saldo.getBulSub3().add(kumpulTotal);
						if (vKetAwal.equalsIgnoreCase("D")) {
							if (nilai1.compareTo(BigDecimal.ZERO) == -1) {
								nilai1 = nilai1.multiply(new BigDecimal( -1 ));
								vKetSub = "K";
							} else {
								vKetSub = vKetAwal;
							}
						}
						if (vKetAwal.equalsIgnoreCase("K")) {
							if (nilai1.compareTo(BigDecimal.ZERO) == -1) {
								nilai1 = nilai1.multiply(new BigDecimal( -1 ));
								vKetSub = "K";
							} else {
								vKetSub = vKetAwal;
							}
						}
						saldo.setBulSub4(saldo.getBulSub4().add(nilai1));
						saldo.setKetSub4(vKetSub);
					}
				}
			}
			//bulan 05
			if (bulan.equalsIgnoreCase("05")) {
				if (saldo != null) {
					String vKetSub = saldo.getKetSub4();
					BigDecimal nilai1 = BigDecimal.ZERO;
					if (saldo.getBulSub4().compareTo(BigDecimal.ZERO) != 0) { 
						
						if (saldo.getKetSub4().equalsIgnoreCase("D") || saldo.getKetSub4().length() == 0) {
							
							nilai1 = saldo.getBulSub4().add(kumpulTotal);
							if (nilai1.compareTo(BigDecimal.ZERO) == -1 ) {
								nilai1 = nilai1.multiply(new BigDecimal( -1 ));
								vKetSub = "K";
							}
						}
						if (saldo.getKetSub4().equalsIgnoreCase("K")) {
							BigDecimal vTotal = kumpulTotal;
							if (kumpulTotal.compareTo(BigDecimal.ZERO) == -1) {
								vTotal = kumpulTotal.multiply(new BigDecimal( -1 ));
								nilai1 = saldo.getBulSub4().add(vTotal);
							} else {
								nilai1 = saldo.getBulSub4().subtract(vTotal);
							}
							if (nilai1.compareTo(BigDecimal.ZERO) == -1){
								nilai1 = nilai1.multiply(new BigDecimal( -1 ));
								vKetSub = "D";
							}
						}
						
						saldo.setBulSub5(saldo.getBulSub5().add(nilai1));
						saldo.setKetSub5(vKetSub);
					} else {
						nilai1 = saldo.getBulSub4().add(kumpulTotal);
						if (vKetAwal.equalsIgnoreCase("D")) {
							if (nilai1.compareTo(BigDecimal.ZERO) == -1) {
								nilai1 = nilai1.multiply(new BigDecimal( -1 ));
								vKetSub = "K";
							} else {
								vKetSub = vKetAwal;
							}
						}
						if (vKetAwal.equalsIgnoreCase("K")) {
							if (nilai1.compareTo(BigDecimal.ZERO) == -1) {
								nilai1 = nilai1.multiply(new BigDecimal( -1 ));
								vKetSub = "K";
							} else {
								vKetSub = vKetAwal;
							}
						}
						saldo.setBulSub5(saldo.getBulSub5().add(nilai1));
						saldo.setKetSub5(vKetSub);
					}
				}
			}
			// bulan 06
			if (bulan.equalsIgnoreCase("06")) {
				if (saldo != null) {
					String vKetSub = saldo.getKetSub5();
					BigDecimal nilai1 = BigDecimal.ZERO;
					if (saldo.getBulSub5().compareTo(BigDecimal.ZERO) != 0) { 
						
						if (saldo.getKetSub5().equalsIgnoreCase("D") || saldo.getKetSub5().length() == 0) {
							
							nilai1 = saldo.getBulSub5().add(kumpulTotal);
							if (nilai1.compareTo(BigDecimal.ZERO) == -1 ) {
								nilai1 = nilai1.multiply(new BigDecimal( -1 ));
								vKetSub = "K";
							}
						}
						if (saldo.getKetSub5().equalsIgnoreCase("K")) {
							BigDecimal vTotal = kumpulTotal;
							if (kumpulTotal.compareTo(BigDecimal.ZERO) == -1) {
								vTotal = kumpulTotal.multiply(new BigDecimal( -1 ));
								nilai1 = saldo.getBulSub5().add(vTotal);
							} else {
								nilai1 = saldo.getBulSub5().subtract(vTotal);
							}
							if (nilai1.compareTo(BigDecimal.ZERO) == -1){
								nilai1 = nilai1.multiply(new BigDecimal( -1 ));
								vKetSub = "D";
							}
						}
						
						saldo.setBulSub6(saldo.getBulSub6().add(nilai1));
						saldo.setKetSub6(vKetSub);
					} else {
						nilai1 = saldo.getBulSub5().add(kumpulTotal);
						if (vKetAwal.equalsIgnoreCase("D")) {
							if (nilai1.compareTo(BigDecimal.ZERO) == -1) {
								nilai1 = nilai1.multiply(new BigDecimal( -1 ));
								vKetSub = "K";
							} else {
								vKetSub = vKetAwal;
							}
						}
						if (vKetAwal.equalsIgnoreCase("K")) {
							if (nilai1.compareTo(BigDecimal.ZERO) == -1) {
								nilai1 = nilai1.multiply(new BigDecimal( -1 ));
								vKetSub = "K";
							} else {
								vKetSub = vKetAwal;
							}
						}
						saldo.setBulSub6(saldo.getBulSub6().add(nilai1));
						saldo.setKetSub6(vKetSub);
					}
				}
			}
			// bulan 07
			if (bulan.equalsIgnoreCase("07")) {
				if (saldo != null) {
					String vKetSub = saldo.getKetSub6();
					BigDecimal nilai1 = BigDecimal.ZERO;
					if (saldo.getBulSub6().compareTo(BigDecimal.ZERO) != 0) { 
						
						if (saldo.getKetSub6().equalsIgnoreCase("D") || saldo.getKetSub6().length() == 0) {
							
							nilai1 = saldo.getBulSub6().add(kumpulTotal);
							if (nilai1.compareTo(BigDecimal.ZERO) == -1 ) {
								nilai1 = nilai1.multiply(new BigDecimal( -1 ));
								vKetSub = "K";
							}
						}
						if (saldo.getKetSub6().equalsIgnoreCase("K")) {
							BigDecimal vTotal = kumpulTotal;
							if (kumpulTotal.compareTo(BigDecimal.ZERO) == -1) {
								vTotal = kumpulTotal.multiply(new BigDecimal( -1 ));
								nilai1 = saldo.getBulSub6().add(vTotal);
							} else {
								nilai1 = saldo.getBulSub6().subtract(vTotal);
							}
							if (nilai1.compareTo(BigDecimal.ZERO) == -1){
								nilai1 = nilai1.multiply(new BigDecimal( -1 ));
								vKetSub = "D";
							}
						}
						
						saldo.setBulSub7(saldo.getBulSub7().add(nilai1));
						saldo.setKetSub7(vKetSub);
					} else {
						nilai1 = saldo.getBulSub6().add(kumpulTotal);
						if (vKetAwal.equalsIgnoreCase("D")) {
							if (nilai1.compareTo(BigDecimal.ZERO) == -1) {
								nilai1 = nilai1.multiply(new BigDecimal( -1 ));
								vKetSub = "K";
							} else {
								vKetSub = vKetAwal;
							}
						}
						if (vKetAwal.equalsIgnoreCase("K")) {
							if (nilai1.compareTo(BigDecimal.ZERO) == -1) {
								nilai1 = nilai1.multiply(new BigDecimal( -1 ));
								vKetSub = "K";
							} else {
								vKetSub = vKetAwal;
							}
						}
						saldo.setBulSub7(saldo.getBulSub7().add(nilai1));
						saldo.setKetSub7(vKetSub);
					}
				}
			}
			// bulan 08
			if (bulan.equalsIgnoreCase("08")) {
				if (saldo != null) {
					String vKetSub = saldo.getKetSub7();
					BigDecimal nilai1 = BigDecimal.ZERO;
					if (saldo.getBulSub7().compareTo(BigDecimal.ZERO) != 0) { 
						
						if (saldo.getKetSub7().equalsIgnoreCase("D") || saldo.getKetSub7().length() == 0) {
							
							nilai1 = saldo.getBulSub7().add(kumpulTotal);
							if (nilai1.compareTo(BigDecimal.ZERO) == -1 ) {
								nilai1 = nilai1.multiply(new BigDecimal( -1 ));
								vKetSub = "K";
							}
						}
						if (saldo.getKetSub7().equalsIgnoreCase("K")) {
							BigDecimal vTotal = kumpulTotal;
							if (kumpulTotal.compareTo(BigDecimal.ZERO) == -1) {
								vTotal = kumpulTotal.multiply(new BigDecimal( -1 ));
								nilai1 = saldo.getBulSub7().add(vTotal);
							} else {
								nilai1 = saldo.getBulSub7().subtract(vTotal);
							}
							if (nilai1.compareTo(BigDecimal.ZERO) == -1){
								nilai1 = nilai1.multiply(new BigDecimal( -1 ));
								vKetSub = "D";
							}
						}
						
						saldo.setBulSub8(saldo.getBulSub8().add(nilai1));
						saldo.setKetSub8(vKetSub);
					} else {
						nilai1 = saldo.getBulSub7().add(kumpulTotal);
						if (vKetAwal.equalsIgnoreCase("D")) {
							if (nilai1.compareTo(BigDecimal.ZERO) == -1) {
								nilai1 = nilai1.multiply(new BigDecimal( -1 ));
								vKetSub = "K";
							} else {
								vKetSub = vKetAwal;
							}
						}
						if (vKetAwal.equalsIgnoreCase("K")) {
							if (nilai1.compareTo(BigDecimal.ZERO) == -1) {
								nilai1 = nilai1.multiply(new BigDecimal( -1 ));
								vKetSub = "K";
							} else {
								vKetSub = vKetAwal;
							}
						}
						saldo.setBulSub8(saldo.getBulSub8().add(nilai1));
						saldo.setKetSub8(vKetSub);
					}
				}
			}
			// bulan 09
			if (bulan.equalsIgnoreCase("09")) {
				if (saldo != null) {
					String vKetSub = saldo.getKetSub8();
					BigDecimal nilai1 = BigDecimal.ZERO;
					if (saldo.getBulSub8().compareTo(BigDecimal.ZERO) != 0) { 
						
						if (saldo.getKetSub8().equalsIgnoreCase("D") || saldo.getKetSub8().length() == 0) {
							
							nilai1 = saldo.getBulSub8().add(kumpulTotal);
							if (nilai1.compareTo(BigDecimal.ZERO) == -1 ) {
								nilai1 = nilai1.multiply(new BigDecimal( -1 ));
								vKetSub = "K";
							}
						}
						if (saldo.getKetSub8().equalsIgnoreCase("K")) {
							BigDecimal vTotal = kumpulTotal;
							if (kumpulTotal.compareTo(BigDecimal.ZERO) == -1) {
								vTotal = kumpulTotal.multiply(new BigDecimal( -1 ));
								nilai1 = saldo.getBulSub8().add(vTotal);
							} else {
								nilai1 = saldo.getBulSub8().subtract(vTotal);
							}
							if (nilai1.compareTo(BigDecimal.ZERO) == -1){
								nilai1 = nilai1.multiply(new BigDecimal( -1 ));
								vKetSub = "D";
							}
						}
						
						saldo.setBulSub9(saldo.getBulSub9().add(nilai1));
						saldo.setKetSub9(vKetSub);
					} else {
						nilai1 = saldo.getBulSub8().add(kumpulTotal);
						if (vKetAwal.equalsIgnoreCase("D")) {
							if (nilai1.compareTo(BigDecimal.ZERO) == -1) {
								nilai1 = nilai1.multiply(new BigDecimal( -1 ));
								vKetSub = "K";
							} else {
								vKetSub = vKetAwal;
							}
						}
						if (vKetAwal.equalsIgnoreCase("K")) {
							if (nilai1.compareTo(BigDecimal.ZERO) == -1) {
								nilai1 = nilai1.multiply(new BigDecimal( -1 ));
								vKetSub = "K";
							} else {
								vKetSub = vKetAwal;
							}
						}
						saldo.setBulSub9(saldo.getBulSub9().add(nilai1));
						saldo.setKetSub9(vKetSub);
					}
				}
			}
			// bulan 10
			if (bulan.equalsIgnoreCase("10")) {
				if (saldo != null) {
					String vKetSub = saldo.getKetSub9();
					BigDecimal nilai1 = BigDecimal.ZERO;
					if (saldo.getBulSub9().compareTo(BigDecimal.ZERO) != 0) { 
						
						if (saldo.getKetSub9().equalsIgnoreCase("D") || saldo.getKetSub9().length() == 0) {
							
							nilai1 = saldo.getBulSub9().add(kumpulTotal);
							if (nilai1.compareTo(BigDecimal.ZERO) == -1 ) {
								nilai1 = nilai1.multiply(new BigDecimal( -1 ));
								vKetSub = "K";
							}
						}
						if (saldo.getKetSub9().equalsIgnoreCase("K")) {
							BigDecimal vTotal = kumpulTotal;
							if (kumpulTotal.compareTo(BigDecimal.ZERO) == -1) {
								vTotal = kumpulTotal.multiply(new BigDecimal( -1 ));
								nilai1 = saldo.getBulSub9().add(vTotal);
							} else {
								nilai1 = saldo.getBulSub9().subtract(vTotal);
							}
							if (nilai1.compareTo(BigDecimal.ZERO) == -1){
								nilai1 = nilai1.multiply(new BigDecimal( -1 ));
								vKetSub = "D";
							}
						}
						
						saldo.setBulSub10(saldo.getBulSub10().add(nilai1));
						saldo.setKetSub10(vKetSub);
					} else {
						nilai1 = saldo.getBulSub9().add(kumpulTotal);
						if (vKetAwal.equalsIgnoreCase("D")) {
							if (nilai1.compareTo(BigDecimal.ZERO) == -1) {
								nilai1 = nilai1.multiply(new BigDecimal( -1 ));
								vKetSub = "K";
							} else {
								vKetSub = vKetAwal;
							}
						}
						if (vKetAwal.equalsIgnoreCase("K")) {
							if (nilai1.compareTo(BigDecimal.ZERO) == -1) {
								nilai1 = nilai1.multiply(new BigDecimal( -1 ));
								vKetSub = "K";
							} else {
								vKetSub = vKetAwal;
							}
						}
						saldo.setBulSub10(saldo.getBulSub10().add(nilai1));
						saldo.setKetSub10(vKetSub);
					}
				}
			}
			// bulan 11
			if (bulan.equalsIgnoreCase("11")) {
				if (saldo != null) {
					String vKetSub = saldo.getKetSub10();
					BigDecimal nilai1 = BigDecimal.ZERO;
					if (saldo.getBulSub10().compareTo(BigDecimal.ZERO) != 0) { 
						
						if (saldo.getKetSub10().equalsIgnoreCase("D") || saldo.getKetSub10().length() == 0) {
							
							nilai1 = saldo.getBulSub10().add(kumpulTotal);
							if (nilai1.compareTo(BigDecimal.ZERO) == -1 ) {
								nilai1 = nilai1.multiply(new BigDecimal( -1 ));
								vKetSub = "K";
							}
						}
						if (saldo.getKetSub10().equalsIgnoreCase("K")) {
							BigDecimal vTotal = kumpulTotal;
							if (kumpulTotal.compareTo(BigDecimal.ZERO) == -1) {
								vTotal = kumpulTotal.multiply(new BigDecimal( -1 ));
								nilai1 = saldo.getBulSub10().add(vTotal);
							} else {
								nilai1 = saldo.getBulSub10().subtract(vTotal);
							}
							if (nilai1.compareTo(BigDecimal.ZERO) == -1){
								nilai1 = nilai1.multiply(new BigDecimal( -1 ));
								vKetSub = "D";
							}
						}
						
						saldo.setBulSub11(saldo.getBulSub11().add(nilai1));
						saldo.setKetSub11(vKetSub);
					} else {
						nilai1 = saldo.getBulSub10().add(kumpulTotal);
						if (vKetAwal.equalsIgnoreCase("D")) {
							if (nilai1.compareTo(BigDecimal.ZERO) == -1) {
								nilai1 = nilai1.multiply(new BigDecimal( -1 ));
								vKetSub = "K";
							} else {
								vKetSub = vKetAwal;
							}
						}
						if (vKetAwal.equalsIgnoreCase("K")) {
							if (nilai1.compareTo(BigDecimal.ZERO) == -1) {
								nilai1 = nilai1.multiply(new BigDecimal( -1 ));
								vKetSub = "K";
							} else {
								vKetSub = vKetAwal;
							}
						}
						saldo.setBulSub11(saldo.getBulSub11().add(nilai1));
						saldo.setKetSub11(vKetSub);
					}
				}
			}
			//bulan 12
			if (bulan.equalsIgnoreCase("12")) {
				if (saldo != null) {
					String vKetSub = saldo.getKetSub11();
					BigDecimal nilai1 = BigDecimal.ZERO;
					if (saldo.getBulSub11().compareTo(BigDecimal.ZERO) != 0) { 
						
						if (saldo.getKetSub11().equalsIgnoreCase("D") || saldo.getKetSub11().length() == 0) {
							
							nilai1 = saldo.getBulSub11().add(kumpulTotal);
							if (nilai1.compareTo(BigDecimal.ZERO) == -1 ) {
								nilai1 = nilai1.multiply(new BigDecimal( -1 ));
								vKetSub = "K";
							}
						}
						if (saldo.getKetSub11().equalsIgnoreCase("K")) {
							BigDecimal vTotal = kumpulTotal;
							if (kumpulTotal.compareTo(BigDecimal.ZERO) == -1) {
								vTotal = kumpulTotal.multiply(new BigDecimal( -1 ));
								nilai1 = saldo.getBulSub11().add(vTotal);
							} else {
								nilai1 = saldo.getBulSub11().subtract(vTotal);
							}
							if (nilai1.compareTo(BigDecimal.ZERO) == -1){
								nilai1 = nilai1.multiply(new BigDecimal( -1 ));
								vKetSub = "D";
							}
						}
						
						saldo.setBulSub12(saldo.getBulSub12().add(nilai1));
						saldo.setKetSub12(vKetSub);
					} else {
						nilai1 = saldo.getBulSub11().add(kumpulTotal);
						if (vKetAwal.equalsIgnoreCase("D")) {
							if (nilai1.compareTo(BigDecimal.ZERO) == -1) {
								nilai1 = nilai1.multiply(new BigDecimal( -1 ));
								vKetSub = "K";
							} else {
								vKetSub = vKetAwal;
							}
						}
						if (vKetAwal.equalsIgnoreCase("K")) {
							if (nilai1.compareTo(BigDecimal.ZERO) == -1) {
								nilai1 = nilai1.multiply(new BigDecimal( -1 ));
								vKetSub = "K";
							} else {
								vKetSub = vKetAwal;
							}
						}
						saldo.setBulSub12(saldo.getBulSub12().add(nilai1));
						saldo.setKetSub12(vKetSub);
					}
				}
			}
			update(saldo,user);
			
		}
				
		
	}

}

