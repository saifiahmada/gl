package com.trio.gl.dao;

import java.math.BigDecimal;
import java.util.List;

import com.trio.gl.bean.TrioMstperkiraan;
import com.trio.gl.hibernate.TrioGenericDao;

/** @author Saifi Ahmada Mar 6, 2013 4:58:02 PM  **/

public interface TrioMstperkiraanDao extends TrioGenericDao<TrioMstperkiraan> {
	public String findNamaById(String idPerkiraan);
	public List<TrioMstperkiraan> findByLevelAndSub(String level, String sub);
	public BigDecimal getPenjualanSmh(String bulan, String noPerk);
	public List<TrioMstperkiraan> getIdPerkiraanPiutang();
}

