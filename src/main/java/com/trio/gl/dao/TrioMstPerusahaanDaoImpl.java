package com.trio.gl.dao;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trio.gl.bean.TrioMstPerusahaan;
import com.trio.gl.hibernate.TrioHibernateDaoSupport;

/**
 * @author Gusti Arya 9:57:37 AM Sep 10, 2013
 */

@Service
public class TrioMstPerusahaanDaoImpl extends TrioHibernateDaoSupport implements TrioMstPerusahaanDao{

	@Override
	public void saveOrUpdate(TrioMstPerusahaan domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void save(TrioMstPerusahaan domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(TrioMstPerusahaan domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(TrioMstPerusahaan domain) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<TrioMstPerusahaan> findAll() {
		
		return null;
	}

	@Override
	public List<TrioMstPerusahaan> findByExample(TrioMstPerusahaan domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TrioMstPerusahaan> findByCriteria(TrioMstPerusahaan domain) {
		
		return null;
	}

	@Override
	public TrioMstPerusahaan findByPrimaryKey(TrioMstPerusahaan domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional(readOnly=true)
	public TrioMstPerusahaan find() {
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery("from TrioMstPerusahaan");
		return (TrioMstPerusahaan) q.uniqueResult();
	}

}
