package com.trio.gl.dao;

import java.util.List;

import com.trio.gl.bean.TrioTarikpartretur;
import com.trio.gl.bean.TrioTarikpartsale;

/** @author Saifi Ahmada Apr 2, 2013 9:35:44 PM  **/

public interface TrioTarikoracleDao {
	
	public List<TrioTarikpartsale> getListTriotarikpartsale(String kdDlr, String tgl);
	public List<TrioTarikpartretur> getListTriotarikpartretur(String kdDlr, String tgl);

}

