package com.trio.gl.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trio.gl.bean.TrioMstsubperkiraan;
import com.trio.gl.hibernate.TrioHibernateDaoSupport;
import com.trio.gl.util.TrioDateUtil;

/** @author Saifi Ahmada Mar 7, 2013 2:01:07 PM  **/

@Service
public class TrioMstsubperkiraanDaoImpl extends TrioHibernateDaoSupport implements TrioMstsubperkiraanDao {

	@Transactional(readOnly=false)
	public void saveOrUpdate(TrioMstsubperkiraan domain, String user) {
		// TODO , masbro
		TrioMstsubperkiraan entity = (TrioMstsubperkiraan) getHibernateTemplate().getSessionFactory().getCurrentSession().get(TrioMstsubperkiraan.class, domain.getTrioMstsubperkiraanPK());
		if (entity == null){
			domain.getUserTrailing().setVcreaby(user);
			domain.getUserTrailing().setDcrea(TrioDateUtil.getLongSysDate());
			getHibernateTemplate().save(domain);
			getHibernateTemplate().flush();
			System.out.println("insert TrioMstsubperkiraan");
		}else{
			domain.getUserTrailing().setVmodiby(user);
			domain.getUserTrailing().setDmodi(TrioDateUtil.getLongSysDate());
			getHibernateTemplate().merge(domain);
			getHibernateTemplate().flush();
			System.out.println("update TrioMstsubperkirann");
		}
	}

	public void save(TrioMstsubperkiraan domain, String user) {
		// TODO , masbro
		
		
	}

	@Transactional(readOnly=false)
	public void update(TrioMstsubperkiraan domain, String user) {
		getHibernateTemplate().getSessionFactory().getCurrentSession().merge(domain);
	}

	public void delete(TrioMstsubperkiraan domain) {
		// TODO , masbro
		
		
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<TrioMstsubperkiraan> findAll() {
		// TODO , masbro
		return getHibernateTemplate().find("from TrioMstsubperkiraan");
	}

	public List<TrioMstsubperkiraan> findByExample(TrioMstsubperkiraan domain) {
		// TODO , masbro
		
		return null;
	}

	@SuppressWarnings("unchecked")
	public List<TrioMstsubperkiraan> findByCriteria(TrioMstsubperkiraan domain) {
		// TODO , masbro
		DetachedCriteria c = DetachedCriteria.forClass(TrioMstsubperkiraan.class);
		if (domain.getTrioMstsubperkiraanPK().getIdPerkiraan() != null){
			c = c.add(Restrictions.eq("trioMstsubperkiraanPK.idPerkiraan", domain.getTrioMstsubperkiraanPK().getIdPerkiraan()));
		}
		if (domain.getTrioMstsubperkiraanPK().getIdSub() != null){
			c = c.add(Restrictions.eq("trioMstsubperkiraanPK.idSub", domain.getTrioMstsubperkiraanPK().getIdSub()));
		}
		if (domain.getNamaSub() != null){
			c = c.add(Restrictions.sqlRestriction("upper(nama_sub) like ('%"+domain.getNamaSub().toUpperCase()+"%') "));
		}
		return getHibernateTemplate().findByCriteria(c);
	}

	public TrioMstsubperkiraan findByPrimaryKey(TrioMstsubperkiraan domain) {
		// TODO , masbro
		
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<TrioMstsubperkiraan> findByIdPerkiraan(String idPerkiraan){
		String sQ = "from TrioMstsubperkiraan s where s.trioMstsubperkiraanPK.idPerkiraan = :idPerkiraan order by s.trioMstsubperkiraanPK.idSub asc";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(sQ);
		q.setString("idPerkiraan", idPerkiraan);
		return q.list();
	}

	@Override
	@Transactional(readOnly=false)
	public void updateQuery(String field) {
		String hql = "update TrioMstsubperkiraan set bulSub"+field+" = 0";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		q.executeUpdate();
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<TrioMstsubperkiraan> getBulSub() {
		String hql = "select b from TrioKumpul a, TrioMstsubperkiraan b "
				+ "where b.trioMstsubperkiraanPK.idPerkiraan = a.trioKumpulPK.idPerkiraan "
				+ "and b.trioMstsubperkiraanPK.idSub = a.trioKumpulPK.idSub "
				+ "order by a.trioKumpulPK.idPerkiraan, a.trioKumpulPK.idSub";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		
		return q.list();
	}
	
	@Transactional(readOnly=true)
	public String findNamaSubByIdAndIdSub(String idPerk, String idSub){
		String namaSub = "";
		String sQ = "select s from TrioMstsubperkiraan s where s.trioMstsubperkiraanPK.idPerkiraan = :idPerk and s.trioMstsubperkiraanPK.idSub = :idSub";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(sQ);
		q.setString("idPerk", idPerk);
		q.setString("idSub", idSub);
		TrioMstsubperkiraan sub = (TrioMstsubperkiraan) q.uniqueResult();
		if (sub != null){
			namaSub = sub.getNamaSub();
		}
		return namaSub;
	}
	@Transactional(readOnly=true)
	public TrioMstsubperkiraan getSubByIdPerkAndKet2(String idPerk, String ket2){
		String sQ = "select s from TrioMstsubperkiraan s where s.trioMstsubperkiraanPK.idPerkiraan = :idPerk and s.ket2 = :ket2 order by s.trioMstsubperkiraanPK.idSub";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(sQ);
		q.setString("idPerk", idPerk);
		q.setString("ket2", ket2);
		return (TrioMstsubperkiraan) q.uniqueResult();
	}

}

