package com.trio.gl.dao;

import java.util.Date;
import java.util.List;

import com.trio.gl.bean.TrioBukubesar;
import com.trio.gl.bean.TrioJurnal;
import com.trio.gl.hibernate.TrioGenericDao;

/** @author Saifi Ahmada Apr 12, 2013 10:33:07 PM  **/

public interface TrioBukubesarDao extends TrioGenericDao<TrioBukubesar>{ 
	public void saveListJurnalToBukuBesar(List<TrioJurnal> listJurnal,Date tglAwal, Date tglAkhir, String user);
	//public void prosesBukubesar(List<TrioJurnal> listJurnal,Date tglAwal, Date tglAkhir, String user);
	
	public String prosesBukubesar(List<TrioJurnal> listJurnal,Date tglAwal, Date tglAkhir, boolean isSimpanLap, String user);
	public List<TrioBukubesar> getListBukuBesarByRangeTanggal(Date tglAwal, Date tglAkhir);
	public void saveAndProsesBukuBesar(List<TrioJurnal> listJurnal,Date tglAwal, Date tglAkhir, String user);
}

