package com.trio.gl.dao;

import java.util.List;

import com.trio.gl.bean.TrioMstsaldoperkiraan;
import com.trio.gl.hibernate.TrioGenericDao;

/** @author Saifi Ahmada Apr 16, 2013 9:23:19 PM  **/

public interface TrioMstsaldoperkiraanDao extends TrioGenericDao<TrioMstsaldoperkiraan> {
	public List<TrioMstsaldoperkiraan> getListSaldoPerkiraanByTahun(String tahun);
}

