package com.trio.gl.dao;

import java.math.BigDecimal;
import java.util.List;

import com.trio.gl.bean.TrioMstsaldosubperkiraan;
import com.trio.gl.hibernate.TrioGenericDao;

/** @author Saifi Ahmada Apr 30, 2013 3:50:17 PM  **/

public interface TrioMstsaldosubperkiraanDao extends TrioGenericDao<TrioMstsaldosubperkiraan> {
	
	public BigDecimal getSaldoSubByIdPerkBulanTahun(String idPerk, String bulan, String tahun);
	
	public BigDecimal getSumSubSaldoByStatusAndPerkAndBulan(String tahun, String bulan,String idPerk, String status);
	
	public List<TrioMstsaldosubperkiraan> getAllByTahun(String tahun);
	
	public void updateSaldoByBulanAndTahun(String bulan, String tahun);
	
	public void prosesSubSaldo(String bulan, String user);

}

