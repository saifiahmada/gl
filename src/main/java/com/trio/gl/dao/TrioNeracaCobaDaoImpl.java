package com.trio.gl.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trio.gl.bean.DKHelper;
import com.trio.gl.bean.TrioKumpul;
import com.trio.gl.bean.TrioLapBukubesar;
import com.trio.gl.bean.TrioLapNeraca;
import com.trio.gl.bean.TrioMstsaldoperkiraan;
import com.trio.gl.bean.TrioMstsubperkiraan;
import com.trio.gl.bean.TrioNeracaCoba;
import com.trio.gl.hibernate.TrioHibernateDaoSupport;
import com.trio.gl.util.TrioDateUtil;

/** @author Saifi Ahmada Jun 3, 2013 4:57:41 PM **/

@Service
public class TrioNeracaCobaDaoImpl extends TrioHibernateDaoSupport implements
		TrioNeracaCobaDao {

	@Transactional(readOnly = false)
	public void saveOrUpdate(TrioNeracaCoba domain, String user) {
		// TODO , masbro
		TrioNeracaCoba objDB = (TrioNeracaCoba) getHibernateTemplate()
				.getSessionFactory().getCurrentSession()
				.get(TrioNeracaCoba.class, domain.getTrioNeracaCobaPK());

		if (objDB == null) {
			save(domain, user);
		} else {
			update(domain, user);
		}

	}

	@Transactional(readOnly = false)
	public void save(TrioNeracaCoba domain, String user) {
		// TODO , masbro
		domain.getUserTrailing().setDcrea(TrioDateUtil.getLongSysDate());
		domain.getUserTrailing().setVcreaby(user);
		getHibernateTemplate().getSessionFactory().getCurrentSession()
				.save(domain);

	}

	@Transactional(readOnly = false)
	public void update(TrioNeracaCoba domain, String user) {
		// TODO , masbro
		domain.getUserTrailing().setDmodi(TrioDateUtil.getLongSysDate());
		domain.getUserTrailing().setVmodiby(user);
		getHibernateTemplate().getSessionFactory().getCurrentSession()
				.merge(domain);

	}

	public void delete(TrioNeracaCoba domain) {
		// TODO , masbro

	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<TrioNeracaCoba> findAll() {
		String sQ = "select n from TrioNeracaCoba n order by n.trioNeracaCobaPK.idPerkiraan";
		return getHibernateTemplate().getSessionFactory().getCurrentSession()
				.createQuery(sQ).list();
	}

	public List<TrioNeracaCoba> findByExample(TrioNeracaCoba domain) {
		// TODO , masbro

		return null;
	}

	public List<TrioNeracaCoba> findByCriteria(TrioNeracaCoba domain) {
		// TODO , masbro

		return null;
	}

	public TrioNeracaCoba findByPrimaryKey(TrioNeracaCoba domain) {
		// TODO , masbro

		return null;
	}

	@Transactional(readOnly = false)
	public String prosesNeraca(String periode, boolean saveNeraca,
			boolean prosesSub, String user) {

		// proses on foxpro -> neraca_awal_1()

		String tahun = periode.substring(0, 4);
		String bulan = periode.substring(4, 6);
		List<TrioMstsaldoperkiraan> listSaldo = getMasterFacade()
				.getTrioMstsaldoperkiraanDao().getListSaldoPerkiraanByTahun(
						tahun);

		TrioNeracaCoba nCoba = null;
		for (TrioMstsaldoperkiraan saldo : listSaldo) {

			String idPerk = saldo.getTrioMstsaldoperkiraanPK().getIdPerkiraan();
			String idSub = "";

			List<TrioMstsubperkiraan> listSub = getMasterFacade()
					.getTrioMstsubperkiraanDao().findByIdPerkiraan(idPerk);
			if (listSub.size() > 0) {
				idSub = listSub.get(0).getTrioMstsubperkiraanPK().getIdSub();
			}

			nCoba = new TrioNeracaCoba(idPerk, idSub);
			String nmPerk = getMasterFacade().getTrioMstperkiraanDao()
					.findNamaById(idPerk);
			nCoba.setNamaPerkiraan(nmPerk);

			BigDecimal saldoBul0 = saldo.getBul0() == null ? BigDecimal.ZERO
					: saldo.getBul0();
			BigDecimal saldoBul1 = saldo.getBul1() == null ? BigDecimal.ZERO
					: saldo.getBul1();
			BigDecimal saldoBul2 = saldo.getBul2() == null ? BigDecimal.ZERO
					: saldo.getBul2();
			BigDecimal saldoBul3 = saldo.getBul3() == null ? BigDecimal.ZERO
					: saldo.getBul3();
			BigDecimal saldoBul4 = saldo.getBul4() == null ? BigDecimal.ZERO
					: saldo.getBul4();
			BigDecimal saldoBul5 = saldo.getBul5() == null ? BigDecimal.ZERO
					: saldo.getBul5();
			BigDecimal saldoBul6 = saldo.getBul6() == null ? BigDecimal.ZERO
					: saldo.getBul6();
			BigDecimal saldoBul7 = saldo.getBul7() == null ? BigDecimal.ZERO
					: saldo.getBul7();
			BigDecimal saldoBul8 = saldo.getBul8() == null ? BigDecimal.ZERO
					: saldo.getBul8();
			BigDecimal saldoBul9 = saldo.getBul9() == null ? BigDecimal.ZERO
					: saldo.getBul9();
			BigDecimal saldoBul10 = saldo.getBul10() == null ? BigDecimal.ZERO
					: saldo.getBul10();
			BigDecimal saldoBul11 = saldo.getBul11() == null ? BigDecimal.ZERO
					: saldo.getBul11();

			if (bulan.equalsIgnoreCase("01")) {
				// jika saldo bul0 != 0
				if (saldoBul0.compareTo(new BigDecimal(0)) != 0) {
					if (saldo.getKet0().equalsIgnoreCase("K")) {
						nCoba.setSlAwalK(saldo.getBul0());
					} else {
						nCoba.setSlAwalD(saldo.getBul0());
					}
				} else {
					BigDecimal hasil5 = getMasterFacade()
							.getTrioMstsaldosubperkiraanDao()
							.getSumSubSaldoByStatusAndPerkAndBulan(
									tahun,
									bulan,
									saldo.getTrioMstsaldoperkiraanPK()
											.getIdPerkiraan(), "D");
					BigDecimal hasil52 = getMasterFacade()
							.getTrioMstsaldosubperkiraanDao()
							.getSumSubSaldoByStatusAndPerkAndBulan(
									tahun,
									bulan,
									saldo.getTrioMstsaldoperkiraanPK()
											.getIdPerkiraan(), "K");
					// vhasil = hasil5 - hasil52
					BigDecimal vhasil = hasil5.subtract(hasil52);
					// jika vhasil > 0
					if (vhasil.compareTo(new BigDecimal(0)) == 1) {
						nCoba.setSlAwalD(vhasil);
					} else {
						nCoba.setSlAwalK(vhasil.multiply(new BigDecimal(-1)));
					}
				}
			} else if (bulan.equals("02")) {
				// jika saldo bul1 != 0
				if (saldoBul1.compareTo(new BigDecimal(0)) != 0) {
					if (saldo.getKet1().equalsIgnoreCase("K")) {
						nCoba.setSlAwalK(saldo.getBul1());
					} else {
						nCoba.setSlAwalD(saldo.getBul1());
					}
				} else {
					BigDecimal hasil5 = getMasterFacade()
							.getTrioMstsaldosubperkiraanDao()
							.getSumSubSaldoByStatusAndPerkAndBulan(
									tahun,
									bulan,
									saldo.getTrioMstsaldoperkiraanPK()
											.getIdPerkiraan(), "D");
					BigDecimal hasil52 = getMasterFacade()
							.getTrioMstsaldosubperkiraanDao()
							.getSumSubSaldoByStatusAndPerkAndBulan(
									tahun,
									bulan,
									saldo.getTrioMstsaldoperkiraanPK()
											.getIdPerkiraan(), "K");
					// vhasil = hasil5 - hasil52
					BigDecimal vhasil = hasil5.subtract(hasil52);
					// jika vhasil > 0
					if (vhasil.compareTo(new BigDecimal(0)) == 1) {
						nCoba.setSlAwalD(vhasil);
					} else {
						nCoba.setSlAwalK(vhasil.multiply(new BigDecimal(-1)));
					}
				}
			} else if (bulan.equals("03")) {
				// jika saldo bul2 != 0
				if (saldoBul2.compareTo(new BigDecimal(0)) != 0) {
					if (saldo.getKet2().equalsIgnoreCase("K")) {
						nCoba.setSlAwalK(saldo.getBul2());
					} else {
						nCoba.setSlAwalD(saldo.getBul2());
					}
				} else {
					BigDecimal hasil5 = getMasterFacade()
							.getTrioMstsaldosubperkiraanDao()
							.getSumSubSaldoByStatusAndPerkAndBulan(
									tahun,
									bulan,
									saldo.getTrioMstsaldoperkiraanPK()
											.getIdPerkiraan(), "D");
					BigDecimal hasil52 = getMasterFacade()
							.getTrioMstsaldosubperkiraanDao()
							.getSumSubSaldoByStatusAndPerkAndBulan(
									tahun,
									bulan,
									saldo.getTrioMstsaldoperkiraanPK()
											.getIdPerkiraan(), "K");
					// vhasil = hasil5 - hasil52
					BigDecimal vhasil = hasil5.subtract(hasil52);
					// jika vhasil > 0
					if (vhasil.compareTo(new BigDecimal(0)) == 1) {
						nCoba.setSlAwalD(vhasil);
					} else {
						nCoba.setSlAwalK(vhasil.multiply(new BigDecimal(-1)));
					}
				}
			} else if (bulan.equals("04")) {
				// jika saldo bul3 != 0
				if (saldoBul3.compareTo(new BigDecimal(0)) != 0) {
					if (saldo.getKet3().equalsIgnoreCase("K")) {
						nCoba.setSlAwalK(saldo.getBul3());
					} else {
						nCoba.setSlAwalD(saldo.getBul3());
					}
				} else {
					BigDecimal hasil5 = getMasterFacade()
							.getTrioMstsaldosubperkiraanDao()
							.getSumSubSaldoByStatusAndPerkAndBulan(
									tahun,
									bulan,
									saldo.getTrioMstsaldoperkiraanPK()
											.getIdPerkiraan(), "D");
					BigDecimal hasil52 = getMasterFacade()
							.getTrioMstsaldosubperkiraanDao()
							.getSumSubSaldoByStatusAndPerkAndBulan(
									tahun,
									bulan,
									saldo.getTrioMstsaldoperkiraanPK()
											.getIdPerkiraan(), "K");
					// vhasil = hasil5 - hasil52
					BigDecimal vhasil = hasil5.subtract(hasil52);
					// jika vhasil > 0
					if (vhasil.compareTo(new BigDecimal(0)) == 1) {
						nCoba.setSlAwalD(vhasil);
					} else {
						nCoba.setSlAwalK(vhasil.multiply(new BigDecimal(-1)));
					}
				}
			} else if (bulan.equals("05")) {
				// jika saldo bul4 != 0
				if (saldoBul4.compareTo(new BigDecimal(0)) != 0) {
					if (saldo.getKet4().equalsIgnoreCase("K")) {
						nCoba.setSlAwalK(saldo.getBul4());
					} else {
						nCoba.setSlAwalD(saldo.getBul4());
					}
				} else {
					BigDecimal hasil5 = getMasterFacade()
							.getTrioMstsaldosubperkiraanDao()
							.getSumSubSaldoByStatusAndPerkAndBulan(
									tahun,
									bulan,
									saldo.getTrioMstsaldoperkiraanPK()
											.getIdPerkiraan(), "D");
					BigDecimal hasil52 = getMasterFacade()
							.getTrioMstsaldosubperkiraanDao()
							.getSumSubSaldoByStatusAndPerkAndBulan(
									tahun,
									bulan,
									saldo.getTrioMstsaldoperkiraanPK()
											.getIdPerkiraan(), "K");
					// vhasil = hasil5 - hasil52
					BigDecimal vhasil = hasil5.subtract(hasil52);
					// jika vhasil > 0
					if (vhasil.compareTo(new BigDecimal(0)) == 1) {
						nCoba.setSlAwalD(vhasil);
					} else {
						nCoba.setSlAwalK(vhasil.multiply(new BigDecimal(-1)));
					}
				}
			} else if (bulan.equals("06")) {
				// jika saldo bul5 != 0
				if (saldoBul5.compareTo(new BigDecimal(0)) != 0) {
					if (saldo.getKet5().equalsIgnoreCase("K")) {
						nCoba.setSlAwalK(saldo.getBul5());
					} else {
						nCoba.setSlAwalD(saldo.getBul5());
					}
				} else {
					BigDecimal hasil5 = getMasterFacade()
							.getTrioMstsaldosubperkiraanDao()
							.getSumSubSaldoByStatusAndPerkAndBulan(
									tahun,
									bulan,
									saldo.getTrioMstsaldoperkiraanPK()
											.getIdPerkiraan(), "D");
					BigDecimal hasil52 = getMasterFacade()
							.getTrioMstsaldosubperkiraanDao()
							.getSumSubSaldoByStatusAndPerkAndBulan(
									tahun,
									bulan,
									saldo.getTrioMstsaldoperkiraanPK()
											.getIdPerkiraan(), "K");
					// vhasil = hasil5 - hasil52
					BigDecimal vhasil = hasil5.subtract(hasil52);
					// jika vhasil > 0
					if (vhasil.compareTo(new BigDecimal(0)) == 1) {
						nCoba.setSlAwalD(vhasil);
					} else {
						nCoba.setSlAwalK(vhasil.multiply(new BigDecimal(-1)));
					}
				}
			} else if (bulan.equals("07")) {
				// jika saldo bul6 != 0
				if (saldoBul6.compareTo(new BigDecimal(0)) != 0) {
					if (saldo.getKet6().equalsIgnoreCase("K")) {
						nCoba.setSlAwalK(saldo.getBul6());
					} else {
						nCoba.setSlAwalD(saldo.getBul6());
					}
				} else {
					BigDecimal hasil5 = getMasterFacade()
							.getTrioMstsaldosubperkiraanDao()
							.getSumSubSaldoByStatusAndPerkAndBulan(
									tahun,
									bulan,
									saldo.getTrioMstsaldoperkiraanPK()
											.getIdPerkiraan(), "D");
					BigDecimal hasil52 = getMasterFacade()
							.getTrioMstsaldosubperkiraanDao()
							.getSumSubSaldoByStatusAndPerkAndBulan(
									tahun,
									bulan,
									saldo.getTrioMstsaldoperkiraanPK()
											.getIdPerkiraan(), "K");
					// vhasil = hasil5 - hasil52
					BigDecimal vhasil = hasil5.subtract(hasil52);
					// jika vhasil > 0
					if (vhasil.compareTo(new BigDecimal(0)) == 1) {
						nCoba.setSlAwalD(vhasil);
					} else {
						nCoba.setSlAwalK(vhasil.multiply(new BigDecimal(-1)));
					}
				}
			} else if (bulan.equals("08")) {
				// jika saldo bul7 != 0
				if (saldoBul7.compareTo(new BigDecimal(0)) != 0) {
					if (saldo.getKet7().equalsIgnoreCase("K")) {
						nCoba.setSlAwalK(saldo.getBul7());
					} else {
						nCoba.setSlAwalD(saldo.getBul7());
					}
				} else {
					BigDecimal hasil5 = getMasterFacade()
							.getTrioMstsaldosubperkiraanDao()
							.getSumSubSaldoByStatusAndPerkAndBulan(
									tahun,
									bulan,
									saldo.getTrioMstsaldoperkiraanPK()
											.getIdPerkiraan(), "D");
					BigDecimal hasil52 = getMasterFacade()
							.getTrioMstsaldosubperkiraanDao()
							.getSumSubSaldoByStatusAndPerkAndBulan(
									tahun,
									bulan,
									saldo.getTrioMstsaldoperkiraanPK()
											.getIdPerkiraan(), "K");
					// vhasil = hasil5 - hasil52
					BigDecimal vhasil = hasil5.subtract(hasil52);
					// jika vhasil > 0
					if (vhasil.compareTo(new BigDecimal(0)) == 1) {
						nCoba.setSlAwalD(vhasil);
					} else {
						nCoba.setSlAwalK(vhasil.multiply(new BigDecimal(-1)));
					}
				}
			} else if (bulan.equals("09")) {
				// jika saldo bul8 != 0
				if (saldoBul8.compareTo(new BigDecimal(0)) != 0) {
					if (saldo.getKet8().equalsIgnoreCase("K")) {
						nCoba.setSlAwalK(saldo.getBul8());
					} else {
						nCoba.setSlAwalD(saldo.getBul8());
					}
				} else {
					BigDecimal hasil5 = getMasterFacade()
							.getTrioMstsaldosubperkiraanDao()
							.getSumSubSaldoByStatusAndPerkAndBulan(
									tahun,
									bulan,
									saldo.getTrioMstsaldoperkiraanPK()
											.getIdPerkiraan(), "D");
					BigDecimal hasil52 = getMasterFacade()
							.getTrioMstsaldosubperkiraanDao()
							.getSumSubSaldoByStatusAndPerkAndBulan(
									tahun,
									bulan,
									saldo.getTrioMstsaldoperkiraanPK()
											.getIdPerkiraan(), "K");
					// vhasil = hasil5 - hasil52
					BigDecimal vhasil = hasil5.subtract(hasil52);
					// jika vhasil > 0
					if (vhasil.compareTo(new BigDecimal(0)) == 1) {
						nCoba.setSlAwalD(vhasil);
					} else {
						nCoba.setSlAwalK(vhasil.multiply(new BigDecimal(-1)));
					}
				}
			} else if (bulan.equals("10")) {
				// jika saldo bul9 != 0
				if (saldoBul9.compareTo(new BigDecimal(0)) != 0) {
					if (saldo.getKet9().equalsIgnoreCase("K")) {
						nCoba.setSlAwalK(saldo.getBul9());
					} else {
						nCoba.setSlAwalD(saldo.getBul9());
					}
				} else {
					BigDecimal hasil5 = getMasterFacade()
							.getTrioMstsaldosubperkiraanDao()
							.getSumSubSaldoByStatusAndPerkAndBulan(
									tahun,
									bulan,
									saldo.getTrioMstsaldoperkiraanPK()
											.getIdPerkiraan(), "D");
					BigDecimal hasil52 = getMasterFacade()
							.getTrioMstsaldosubperkiraanDao()
							.getSumSubSaldoByStatusAndPerkAndBulan(
									tahun,
									bulan,
									saldo.getTrioMstsaldoperkiraanPK()
											.getIdPerkiraan(), "K");
					// vhasil = hasil5 - hasil52
					BigDecimal vhasil = hasil5.subtract(hasil52);
					// jika vhasil > 0
					if (vhasil.compareTo(new BigDecimal(0)) == 1) {
						nCoba.setSlAwalD(vhasil);
					} else {
						nCoba.setSlAwalK(vhasil.multiply(new BigDecimal(-1)));
					}
				}
			} else if (bulan.equals("11")) {
				// jika saldo bul10 != 0
				if (saldoBul10.compareTo(new BigDecimal(0)) != 0) {
					if (saldo.getKet10().equalsIgnoreCase("K")) {
						nCoba.setSlAwalK(saldo.getBul10());
					} else {
						nCoba.setSlAwalD(saldo.getBul10());
					}
				} else {
					BigDecimal hasil5 = getMasterFacade()
							.getTrioMstsaldosubperkiraanDao()
							.getSumSubSaldoByStatusAndPerkAndBulan(
									tahun,
									bulan,
									saldo.getTrioMstsaldoperkiraanPK()
											.getIdPerkiraan(), "D");
					BigDecimal hasil52 = getMasterFacade()
							.getTrioMstsaldosubperkiraanDao()
							.getSumSubSaldoByStatusAndPerkAndBulan(
									tahun,
									bulan,
									saldo.getTrioMstsaldoperkiraanPK()
											.getIdPerkiraan(), "K");
					// vhasil = hasil5 - hasil52
					BigDecimal vhasil = hasil5.subtract(hasil52);
					// jika vhasil > 0
					if (vhasil.compareTo(new BigDecimal(0)) == 1) {
						nCoba.setSlAwalD(vhasil);
					} else {
						nCoba.setSlAwalK(vhasil.multiply(new BigDecimal(-1)));
					}
				}
			} else if (bulan.equals("12")) {
				// jika saldo bul11 != 0
				if (saldoBul11.compareTo(new BigDecimal(0)) != 0) {
					if (saldo.getKet11().equalsIgnoreCase("K")) {
						nCoba.setSlAwalK(saldo.getBul11());
					} else {
						nCoba.setSlAwalD(saldo.getBul11());
					}
				} else {
					BigDecimal hasil5 = getMasterFacade()
							.getTrioMstsaldosubperkiraanDao()
							.getSumSubSaldoByStatusAndPerkAndBulan(
									tahun,
									bulan,
									saldo.getTrioMstsaldoperkiraanPK()
											.getIdPerkiraan(), "D");
					BigDecimal hasil52 = getMasterFacade()
							.getTrioMstsaldosubperkiraanDao()
							.getSumSubSaldoByStatusAndPerkAndBulan(
									tahun,
									bulan,
									saldo.getTrioMstsaldoperkiraanPK()
											.getIdPerkiraan(), "K");
					// vhasil = hasil5 - hasil52
					BigDecimal vhasil = hasil5.subtract(hasil52);
					// jika vhasil > 0
					if (vhasil.compareTo(new BigDecimal(0)) == 1) {
						nCoba.setSlAwalD(vhasil);
					} else {
						nCoba.setSlAwalK(vhasil.multiply(new BigDecimal(-1)));
					}
				}
			}
		}
		saveOrUpdate(nCoba, user);

		// proses on foxpro -> neraca_awal_2()
		List<DKHelper> listHelper = getMasterFacade().getTrioLapBukubesarDao()
				.getListDKHelperLapBukubesarByPeriode(periode);
		for (DKHelper help : listHelper) {

			TrioNeracaCoba nrc = getTrioNeracaCobaByIdPerk(help
					.getIdPerkiraan());

			nrc.setMtsD(help.getDebet());
			nrc.setMtsK(help.getKredit());
			update(nrc, user);
		}
		// proses on foxpro -> neraca_awal_3()
		List<TrioNeracaCoba> listNeraca = findAll();

		for (TrioNeracaCoba nrc : listNeraca) {

			if (nrc.getSlAwalD() == null) {
				nrc.setSlAwalD(BigDecimal.ZERO);
			}
			if (nrc.getSlAwalK() == null) {
				nrc.setSlAwalK(BigDecimal.ZERO);
			}
			if (nrc.getMtsD() == null) {
				nrc.setMtsD(BigDecimal.ZERO);
			}
			if (nrc.getMtsK() == null) {
				nrc.setMtsK(BigDecimal.ZERO);
			}

			if (nrc.getSlAwalD().compareTo(BigDecimal.ZERO) == 1
					&& nrc.getSlAwalK().compareTo(BigDecimal.ZERO) == 0
					&& nrc.getMtsD().compareTo(BigDecimal.ZERO) == 1
					&& nrc.getMtsK().compareTo(BigDecimal.ZERO) == 0) {
				BigDecimal rumus1 = nrc.getSlAwalD().add(nrc.getMtsD());
				nrc.setSlAwalD(rumus1);
			}
			if (nrc.getSlAwalD().compareTo(BigDecimal.ZERO) == 1
					&& nrc.getSlAwalK().compareTo(BigDecimal.ZERO) == 0
					&& nrc.getMtsD().compareTo(BigDecimal.ZERO) == 1
					&& nrc.getMtsK().compareTo(BigDecimal.ZERO) == 1) {
				BigDecimal rum1 = nrc.getSlAwalD().add(nrc.getMtsD());
				BigDecimal rum2 = nrc.getSlAwalD().add(nrc.getMtsD())
						.subtract(nrc.getMtsK());

				if (rum1.compareTo(nrc.getMtsK()) == 1) {
					nrc.setSlAkhirD(rum2);
				} else {
					if (rum2.compareTo(BigDecimal.ZERO) == -1) {
						nrc.setSlAkhirK(rum2.multiply(new BigDecimal(-1)));
					} else {
						nrc.setSlAkhirK(rum2);
					}
				}

			}
			if (nrc.getSlAwalK().compareTo(BigDecimal.ZERO) == 1
					&& nrc.getSlAwalD().compareTo(BigDecimal.ZERO) == 0
					&& nrc.getMtsD().compareTo(BigDecimal.ZERO) == 1
					&& nrc.getMtsK().compareTo(BigDecimal.ZERO) == 1) {
				BigDecimal rumus1x = nrc.getSlAwalK().add(nrc.getMtsK());
				BigDecimal rumus2x = (nrc.getSlAwalK().add(nrc.getMtsK()))
						.subtract(nrc.getMtsD());

				if (nrc.getMtsD().compareTo(rumus1x) == 1) {
					nrc.setSlAkhirD(rumus2x.multiply(new BigDecimal(-1)));
				} else {
					if (rumus2x.compareTo(BigDecimal.ZERO) == -1) {
						nrc.setSlAkhirK(rumus2x.multiply(new BigDecimal(-1)));
					} else {
						nrc.setSlAkhirK(rumus2x);
					}
				}
			}
			if (nrc.getSlAwalK().compareTo(BigDecimal.ZERO) == 1
					&& nrc.getSlAwalD().compareTo(BigDecimal.ZERO) == 0
					&& nrc.getMtsD().compareTo(BigDecimal.ZERO) == 1
					&& nrc.getMtsK().compareTo(BigDecimal.ZERO) == 0) {
				if (nrc.getSlAwalK().compareTo(nrc.getMtsD()) == 1) {
					BigDecimal rumus2 = nrc.getSlAwalK()
							.subtract(nrc.getMtsD());
					nrc.setSlAkhirK(rumus2);
				} else {
					BigDecimal rumus3 = nrc.getMtsD()
							.subtract(nrc.getSlAwalK());
					nrc.setSlAkhirD(rumus3);
				}

			}
			if (nrc.getSlAwalD().compareTo(BigDecimal.ZERO) == 1
					&& nrc.getSlAwalK().compareTo(BigDecimal.ZERO) == 0
					&& nrc.getMtsK().compareTo(BigDecimal.ZERO) == 1
					&& nrc.getMtsD().compareTo(BigDecimal.ZERO) == 0) {
				if (nrc.getSlAwalD().compareTo(nrc.getMtsK()) == 1) {
					BigDecimal rumus4 = nrc.getSlAwalD()
							.subtract(nrc.getMtsK());
					nrc.setSlAkhirD(rumus4);
				} else {
					BigDecimal rumus5 = nrc.getMtsK()
							.subtract(nrc.getSlAwalD());
					nrc.setSlAkhirK(rumus5);
				}

			}
			if (nrc.getSlAwalK().compareTo(BigDecimal.ZERO) == 1
					&& nrc.getSlAwalD().compareTo(BigDecimal.ZERO) == 0
					&& nrc.getMtsK().compareTo(BigDecimal.ZERO) == 1
					&& nrc.getMtsD().compareTo(BigDecimal.ZERO) == 0) {
				BigDecimal rumus6 = nrc.getSlAwalK().add(nrc.getMtsK());
				nrc.setSlAkhirK(rumus6);
			}
			update(nrc, user);
		}
		// proses on foxpro -> neraca_awal_4()
		List<TrioNeracaCoba> listNeraca4 = findAll();
		for (TrioNeracaCoba nrc4 : listNeraca4) {
			if (nrc4.getSlAwalD().compareTo(BigDecimal.ZERO) == 0
					&& nrc4.getSlAwalK().compareTo(BigDecimal.ZERO) == 0) {
				if (nrc4.getMtsK().compareTo(nrc4.getMtsD()) == 1) {
					BigDecimal vAgus = nrc4.getMtsK().subtract(nrc4.getMtsD());
					nrc4.setSlAkhirK(vAgus);
				}
			}
			if (nrc4.getSlAwalD().compareTo(BigDecimal.ZERO) == 0
					&& nrc4.getSlAwalK().compareTo(BigDecimal.ZERO) == 0) {
				if (nrc4.getMtsD().compareTo(nrc4.getMtsK()) == 1) {
					BigDecimal vHasil1 = nrc4.getMtsD()
							.subtract(nrc4.getMtsK());
					nrc4.setSlAkhirD(vHasil1);
				} else {
					BigDecimal vHasil2 = nrc4.getMtsK()
							.subtract(nrc4.getMtsD());
				}
			}
			if (nrc4.getSlAwalD().compareTo(BigDecimal.ZERO) == 0
					&& nrc4.getSlAwalK().compareTo(BigDecimal.ZERO) == 0) {
				if (nrc4.getMtsD().compareTo(BigDecimal.ZERO) == 1
						&& nrc4.getMtsK().compareTo(BigDecimal.ZERO) == 0) {
					nrc4.setSlAkhirD(nrc4.getMtsD());
				}
				if (nrc4.getMtsD().compareTo(BigDecimal.ZERO) == 0
						&& nrc4.getMtsK().compareTo(BigDecimal.ZERO) == 1) {
					nrc4.setSlAkhirK(nrc4.getMtsK());
				}
			}
			update(nrc4, user);
		}
		// proses on foxpro -> neraca_awal_5()
		List<TrioNeracaCoba> listNeraca5 = findAll();
		for (TrioNeracaCoba nrc5 : listNeraca5) {
			if (nrc5.getSlAwalD().compareTo(BigDecimal.ZERO) == 1
					&& nrc5.getMtsD().compareTo(BigDecimal.ZERO) == 0
					&& nrc5.getMtsK().compareTo(BigDecimal.ZERO) == 0) {
				nrc5.setSlAkhirD(nrc5.getSlAwalD());
			}
			if (nrc5.getSlAwalK().compareTo(BigDecimal.ZERO) == 1
					&& nrc5.getMtsD().compareTo(BigDecimal.ZERO) == 0
					&& nrc5.getMtsK().compareTo(BigDecimal.ZERO) == 0) {
				nrc5.setSlAkhirK(nrc5.getSlAwalK());
			}
			update(nrc5, user);
		}
		// jika simpan neraca awal dicheck
		// SIMPAN_NERACA_AWAL
		// PROSES_SALDO_AWAL
		if (saveNeraca) {

			// bgn proses simpan_neraca_awal
			List<TrioNeracaCoba> listNrc = findAll();
			TrioLapNeraca lapNrc = null;

			for (TrioNeracaCoba nrcCoba : listNrc) {
				lapNrc = new TrioLapNeraca(nrcCoba.getTrioNeracaCobaPK()
						.getIdPerkiraan(), nrcCoba.getTrioNeracaCobaPK()
						.getIdSub(), periode);
				lapNrc.setTglNeraca(nrcCoba.getTglNrc());
				lapNrc.setSlAwalD(nrcCoba.getSlAwalD());
				lapNrc.setSlAwalK(nrcCoba.getSlAwalK());
				lapNrc.setMtsD(nrcCoba.getMtsD());
				lapNrc.setMtsK(nrcCoba.getMtsK());
				lapNrc.setSlAkhD(nrcCoba.getSlAkhirD());
				lapNrc.setSlAkhK(nrcCoba.getSlAkhirK());
				lapNrc.setrL(nrcCoba.getrL());

				getMasterFacade().getTrioLapNeracaDao().saveOrUpdate(lapNrc,
						user);

			}
			// end proses simpan_neraca_awal

			// bgn proses_saldo_awal
			List<TrioLapNeraca> listLapNrc = getMasterFacade()
					.getTrioLapNeracaDao().findByPeriode(periode);
			for (TrioLapNeraca laporanNrc : listLapNrc) {
				if (laporanNrc.getSlAkhD() == null) {
					laporanNrc.setSlAkhD(BigDecimal.ZERO);
				}
				if (laporanNrc.getSlAkhK() == null) {
					laporanNrc.setSlAkhK(BigDecimal.ZERO);
				}
				if (laporanNrc.getMtsD() == null) {
					laporanNrc.setMtsD(BigDecimal.ZERO);
				}
				if (laporanNrc.getMtsK() == null) {
					laporanNrc.setMtsK(BigDecimal.ZERO);
				}

				TrioMstsaldoperkiraan saldoPerk = new TrioMstsaldoperkiraan(
						laporanNrc.getTrioLapNeracaPK().getIdPerkiraan(), tahun);
				if (bulan.equalsIgnoreCase("01")) {
					if (laporanNrc.getSlAkhD().compareTo(BigDecimal.ZERO) != 0) { // jika
																					// slAkhD
																					// <>
																					// 0
						saldoPerk.setBul1(laporanNrc.getSlAkhD());
						saldoPerk.setKet1("D");
					} else {
						if (laporanNrc.getSlAkhK().compareTo(BigDecimal.ZERO) == 1) {
							saldoPerk.setBul1(laporanNrc.getSlAkhK());
							saldoPerk.setKet1("K");
						} else {
							saldoPerk.setBul1(BigDecimal.ZERO);
							saldoPerk.setKet1("D");
						}
					}

				}
				if (bulan.equalsIgnoreCase("02")) {
					if (laporanNrc.getSlAkhD().compareTo(BigDecimal.ZERO) != 0) {
						saldoPerk.setBul2(laporanNrc.getSlAkhD());
						saldoPerk.setKet2("D");
					} else {
						if (laporanNrc.getSlAkhK().compareTo(BigDecimal.ZERO) == 1) {
							saldoPerk.setBul2(laporanNrc.getSlAkhK());
							saldoPerk.setKet2("K");
						} else {
							saldoPerk.setBul2(BigDecimal.ZERO);
							saldoPerk.setKet2("D");
						}
					}
				}
				if (bulan.equalsIgnoreCase("03")) {
					if (laporanNrc.getSlAkhD().compareTo(BigDecimal.ZERO) != 0) {
						saldoPerk.setBul3(laporanNrc.getSlAkhD());
						saldoPerk.setKet3("D");
					} else {
						if (laporanNrc.getSlAkhK().compareTo(BigDecimal.ZERO) == 1) {
							saldoPerk.setBul3(laporanNrc.getSlAkhK());
							saldoPerk.setKet3("K");
						} else {
							saldoPerk.setBul3(BigDecimal.ZERO);
							saldoPerk.setKet3("D");
						}
					}
				}
				if (bulan.equalsIgnoreCase("04")) {
					if (laporanNrc.getSlAkhD().compareTo(BigDecimal.ZERO) != 0) {
						saldoPerk.setBul4(laporanNrc.getSlAkhD());
						saldoPerk.setKet4("D");
					} else {
						if (laporanNrc.getSlAkhK().compareTo(BigDecimal.ZERO) == 1) {
							saldoPerk.setBul4(laporanNrc.getSlAkhK());
							saldoPerk.setKet4("K");
						} else {
							saldoPerk.setBul4(BigDecimal.ZERO);
							saldoPerk.setKet4("D");
						}
					}
				}
				if (bulan.equalsIgnoreCase("05")) {
					if (laporanNrc.getSlAkhD().compareTo(BigDecimal.ZERO) != 0) {
						saldoPerk.setBul5(laporanNrc.getSlAkhD());
						saldoPerk.setKet5("D");
					} else {
						if (laporanNrc.getSlAkhK().compareTo(BigDecimal.ZERO) == 1) {
							saldoPerk.setBul5(laporanNrc.getSlAkhK());
							saldoPerk.setKet5("K");
						} else {
							saldoPerk.setBul5(BigDecimal.ZERO);
							saldoPerk.setKet5("D");
						}
					}
				}
				if (bulan.equalsIgnoreCase("06")) {
					if (laporanNrc.getSlAkhD().compareTo(BigDecimal.ZERO) != 0) {
						saldoPerk.setBul6(laporanNrc.getSlAkhD());
						saldoPerk.setKet6("D");
					} else {
						if (laporanNrc.getSlAkhK().compareTo(BigDecimal.ZERO) == 1) {
							saldoPerk.setBul6(laporanNrc.getSlAkhK());
							saldoPerk.setKet6("K");
						} else {
							saldoPerk.setBul6(BigDecimal.ZERO);
							saldoPerk.setKet6("D");
						}
					}
				}
				if (bulan.equalsIgnoreCase("07")) {
					if (laporanNrc.getSlAkhD().compareTo(BigDecimal.ZERO) != 0) {
						saldoPerk.setBul7(laporanNrc.getSlAkhD());
						saldoPerk.setKet7("D");
					} else {
						if (laporanNrc.getSlAkhK().compareTo(BigDecimal.ZERO) == 1) {
							saldoPerk.setBul7(laporanNrc.getSlAkhK());
							saldoPerk.setKet7("K");
						} else {
							saldoPerk.setBul7(BigDecimal.ZERO);
							saldoPerk.setKet7("D");
						}
					}
				}
				if (bulan.equalsIgnoreCase("08")) {
					if (laporanNrc.getSlAkhD().compareTo(BigDecimal.ZERO) != 0) {
						saldoPerk.setBul8(laporanNrc.getSlAkhD());
						saldoPerk.setKet8("D");
					} else {
						if (laporanNrc.getSlAkhK().compareTo(BigDecimal.ZERO) == 1) {
							saldoPerk.setBul8(laporanNrc.getSlAkhK());
							saldoPerk.setKet8("K");
						} else {
							saldoPerk.setBul8(BigDecimal.ZERO);
							saldoPerk.setKet8("D");
						}
					}
				}
				if (bulan.equalsIgnoreCase("09")) {
					if (laporanNrc.getSlAkhD().compareTo(BigDecimal.ZERO) != 0) {
						saldoPerk.setBul9(laporanNrc.getSlAkhD());
						saldoPerk.setKet9("D");
					} else {
						if (laporanNrc.getSlAkhK().compareTo(BigDecimal.ZERO) == 1) {
							saldoPerk.setBul9(laporanNrc.getSlAkhK());
							saldoPerk.setKet9("K");
						} else {
							saldoPerk.setBul9(BigDecimal.ZERO);
							saldoPerk.setKet9("D");
						}
					}
				}
				if (bulan.equalsIgnoreCase("10")) {
					if (laporanNrc.getSlAkhD().compareTo(BigDecimal.ZERO) != 0) {
						saldoPerk.setBul10(laporanNrc.getSlAkhD());
						saldoPerk.setKet10("D");
					} else {
						if (laporanNrc.getSlAkhK().compareTo(BigDecimal.ZERO) == 1) {
							saldoPerk.setBul10(laporanNrc.getSlAkhK());
							saldoPerk.setKet10("K");
						} else {
							saldoPerk.setBul10(BigDecimal.ZERO);
							saldoPerk.setKet10("D");
						}
					}
				}
				if (bulan.equalsIgnoreCase("11")) {
					if (laporanNrc.getSlAkhD().compareTo(BigDecimal.ZERO) != 0) {
						saldoPerk.setBul11(laporanNrc.getSlAkhD());
						saldoPerk.setKet11("D");
					} else {
						if (laporanNrc.getSlAkhK().compareTo(BigDecimal.ZERO) == 1) {
							saldoPerk.setBul11(laporanNrc.getSlAkhK());
							saldoPerk.setKet11("K");
						} else {
							saldoPerk.setBul11(BigDecimal.ZERO);
							saldoPerk.setKet11("D");
						}
					}
				}
				if (bulan.equalsIgnoreCase("12")) {
					if (laporanNrc.getSlAkhD().compareTo(BigDecimal.ZERO) != 0) {
						saldoPerk.setBul12(laporanNrc.getSlAkhD());
						saldoPerk.setKet12("D");
					} else {
						if (laporanNrc.getSlAkhK().compareTo(BigDecimal.ZERO) == 1) {
							saldoPerk.setBul12(laporanNrc.getSlAkhK());
							saldoPerk.setKet12("K");
						} else {
							saldoPerk.setBul12(BigDecimal.ZERO);
							saldoPerk.setKet12("D");
						}
					}
				}

				getMasterFacade().getTrioMstsaldoperkiraanDao().saveOrUpdate(
						saldoPerk, user);
			}
			// end proses_saldo_awal

		}
		// JIKA PROSES SALDO SUB DICHECK
		// PROSES_SALDO_SUB1
		// PROSES SALDO SUB2
		// PROSES SALDO SUB3

		if (prosesSub) {
			// proses_saldo_sub_1 dan proses_saldo_sub_2
			List<TrioKumpul> listKumpul = getMasterFacade()
					.getTrioLapBukubesarDao().getListKumpulByPeriode(periode);
			getMasterFacade().getTrioKumpulDao().saveAll(listKumpul, user);

			// proses_saldo_sub_3
			// update saldo sub perkiraan berdasarkan bulan dan tahun
			getMasterFacade().getTrioMstsaldosubperkiraanDao()
					.updateSaldoByBulanAndTahun(bulan, tahun);
			getMasterFacade().getTrioMstsaldosubperkiraanDao().prosesSubSaldo(
					bulan, user);

		}

		return "Sukses";
	}

	@Transactional(readOnly = true)
	public TrioNeracaCoba getTrioNeracaCobaByIdPerk(String idPerk) {
		String sQ = "from TrioNeracaCoba n where n.trioNeracaCobaPK.idPerkiraan = :idPerk ";
		Query query = getHibernateTemplate().getSessionFactory()
				.getCurrentSession().createQuery(sQ);
		query.setString("idPerk", idPerk);
		return (TrioNeracaCoba) query.uniqueResult();

	}

}
