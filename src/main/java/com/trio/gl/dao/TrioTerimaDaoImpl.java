package com.trio.gl.dao;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trio.gl.bean.TrioJurnal;
import com.trio.gl.bean.TrioMstsubperkiraan;
import com.trio.gl.bean.TrioTerima;
import com.trio.gl.hibernate.TrioHibernateDaoSupport;
import com.trio.gl.util.TrioDateConv;
import com.trio.gl.util.TrioDateUtil;
import com.trio.gl.util.TrioStringUtil;

/** @author Saifi Ahmada Aug 28, 2013 5:18:55 PM **/

@Service
public class TrioTerimaDaoImpl extends TrioHibernateDaoSupport implements
		TrioTerimaDao {

	private static final BigDecimal NOL = new BigDecimal(0);

	public void saveOrUpdate(TrioTerima domain, String user) {
		// TODO , masbro

	}

	@Transactional(readOnly = false)
	public void save(TrioTerima domain, String user) {
		// TODO , masbro
		domain.getUserTrailing().setVcreaby(user);
		domain.getUserTrailing().setDcrea(TrioDateUtil.getLongSysDate());
		getHibernateTemplate().getSessionFactory().getCurrentSession()
				.save(domain);
	}

	public void update(TrioTerima domain, String user) {
		// TODO , masbro

	}

	public void delete(TrioTerima domain) {
		// TODO , masbro

	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<TrioTerima> findAll() {
		return (List<TrioTerima>) getHibernateTemplate().getSessionFactory()
				.getCurrentSession().createQuery("from TrioTerima").list();
	}

	public List<TrioTerima> findByExample(TrioTerima domain) {
		// TODO , masbro

		return null;
	}

	public List<TrioTerima> findByCriteria(TrioTerima domain) {
		// TODO , masbro

		return null;
	}

	public TrioTerima findByPrimaryKey(TrioTerima domain) {
		// TODO , masbro

		return null;
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<TrioTerima> findByNoLKHAndNoFakAndKet(String noLKH,
			String noFak, String ket) {
		String sQuery = "select t from TrioTerima t where t.trioTerimaPK.noLKH = :noLKH and t.trioTerimaPK.noFak = :noFak and t.ket = :ket";
		Query query = getHibernateTemplate().getSessionFactory()
				.getCurrentSession().createQuery(sQuery);
		query.setString("noLKH", noLKH);
		query.setString("noFak", noFak);
		query.setString("ket", ket);
		List<TrioTerima> listTerima = query.list();
		return listTerima;
	}

	@Transactional(readOnly = true)
	public void saveList(List<TrioTerima> listTerima, String user) {
		for (TrioTerima terima : listTerima) {
			save(terima, user);
		}
	}

	@Transactional(readOnly = false)
	public void prosesDataUploadLKHCabang1(List<TrioTerima> listTerima,
			String user) {

		List<TrioTerima> listFilter = new ArrayList<TrioTerima>();
		TrioJurnal jurnal = null;
		List<TrioJurnal> listJurnal = null;

		for (TrioTerima terima : listTerima) {
			if ((terima.getTrioTerimaPK().getNoLKH().length() > 0)
					&& (terima.getNmTran().length() > 0)
					&& (terima.getTrioTerimaPK().getNoLKH()
							.equalsIgnoreCase("KWIT.TAG"))) {
				continue;
			}
			if (((terima.getNoPerk().equalsIgnoreCase("11499") && terima
					.getNoSub().equalsIgnoreCase("03")) || (terima.getNoPerk()
					.equalsIgnoreCase("11499") && terima.getNoSub()
					.equalsIgnoreCase("04")))
					&& terima.getKet().equalsIgnoreCase("3")) {
				continue;
			}

			String nmTranSub = terima.getNmTran().substring(0, 10);

			if (nmTranSub.equalsIgnoreCase("saldo awal")) {
				continue;
			}

			String nmTranSub2 = terima.getNmTran();
			if (nmTranSub2.substring(0, 4).equalsIgnoreCase("[KL]")) {
				nmTranSub2 = nmTranSub2.substring(5);
			}
			if (nmTranSub2.substring(0, 5).equalsIgnoreCase("[MNL]")) {
				nmTranSub2 = nmTranSub2.substring(6);
			}
			if (nmTranSub2.substring(0, 6).equalsIgnoreCase("[LOAD]")) {
				nmTranSub2 = nmTranSub2.substring(7);
			}
			terima.setNmTran(nmTranSub2);

			listFilter.add(terima);
		}
		// /*

		saveList(listFilter, user);

		List<TrioTerima> list = findAll();

		for (TrioTerima terima : list) {
			// vjumlah=debet+kredit
			BigDecimal vjumlah = terima.getDebet().add(terima.getKredit());
			// vjumlah2=vjumlah/1.1
			BigDecimal vjumlah2 = vjumlah.divide(new BigDecimal(1.1), 0,
					RoundingMode.HALF_EVEN);
			// vjumlah3=vjumlah-vjumlah2
			BigDecimal vjumlah3 = vjumlah.subtract(vjumlah2);

			listJurnal = new ArrayList<TrioJurnal>();
			// CASE vno_perk='21309' AND LOWER(LEFT(vnm_tran,20))<>'pembayaran
			// utk dinas'
			if (terima.getNoPerk().equalsIgnoreCase("21309")
					&& (terima.getNmTran().substring(0, 20)).toLowerCase()
							.equalsIgnoreCase("pembayaran utk dinas")) {

				System.out.println("CASEIF 1");
				// TODO TO DO membaca data jurnal
				// jika kredit > 0
				if (terima.getKredit().compareTo(NOL) == 1) {
					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan("11101");
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan("KAS");
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(NOL);
					jurnal.setKredit(terima.getKredit());
					jurnal.setBlc("*");
					jurnal.setTipe("K");
					listJurnal.add(jurnal);

				} else {
					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan("11101");
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan("KAS");
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(terima.getDebet());
					jurnal.setKredit(NOL);
					jurnal.setBlc("*");
					jurnal.setTipe("D");
					listJurnal.add(jurnal);

				}

				String jenisJurnal = getJenisJurnalByParams(terima.getKet(),
						terima.getDebet(), terima.getKredit(),
						terima.getNoPerk());
				getMasterFacade().getTrioJurnalDao().saveTransaction(
						listJurnal, jenisJurnal, user);
			}

			// CASE vno_perk='41116' AND vket='X'
			else if (terima.getNoPerk().equalsIgnoreCase("41116")
					&& terima.getKet().equalsIgnoreCase("X")) {

				System.out.println("CASEIF 2");

				String vNoPerk0 = null;
				String vNmPerk0 = null;
				// jika debet > 0
				if (terima.getDebet().compareTo(NOL) == 1) {
					String[] arrStr = getNoPerkAndNmPerk(terima.getKet2());
					vNoPerk0 = arrStr[0];
					vNmPerk0 = arrStr[0];

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk0);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk0);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(terima.getDebet());
					jurnal.setKredit(NOL);
					jurnal.setBlc("*");
					jurnal.setTipe("D");
					jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
					listJurnal.add(jurnal);

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(terima.getNoPerk());
					jurnal.getTrioJurnalPK().setIdSub("");

					String nmPerk = getMasterFacade().getTrioMstperkiraanDao()
							.findNamaById(terima.getNoPerk());
					jurnal.setNamaPerkiraan(nmPerk);

					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(NOL);
					jurnal.setKredit(vjumlah2);
					jurnal.setBlc("*");
					jurnal.setTipe("K");
					listJurnal.add(jurnal);

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan("21404");
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan("PPN KELUARAN");
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(NOL);
					jurnal.setKredit(vjumlah3);
					jurnal.setBlc("*");
					jurnal.setTipe("K");
					listJurnal.add(jurnal);

				} else { // jika debet < 0

					String[] arrStr = getNoPerkAndNmPerk(terima.getKet2());
					vNoPerk0 = arrStr[0];
					vNmPerk0 = arrStr[0];

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk0);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk0);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(NOL);
					jurnal.setKredit(terima.getKredit());
					jurnal.setBlc("*");
					jurnal.setTipe("K");
					listJurnal.add(jurnal);

					String nmPerk = getMasterFacade().getTrioMstperkiraanDao()
							.findNamaById(terima.getNoPerk());
					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(terima.getNoPerk());
					jurnal.setNamaPerkiraan(nmPerk);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(vjumlah2);
					jurnal.setKredit(NOL);
					jurnal.setBlc("*");
					jurnal.setTipe("D");
					listJurnal.add(jurnal);

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan("21404");
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan("PPN KELUARAN");
					jurnal.setDebet(vjumlah3);
					jurnal.setKredit(NOL);
					jurnal.setBlc("*");
					jurnal.setTipe("D");
					listJurnal.add(jurnal);

				}

				String jenisJurnal = getJenisJurnalByParams(terima.getKet(),
						terima.getDebet(), terima.getKredit(),
						terima.getNoPerk());
				getMasterFacade().getTrioJurnalDao().saveTransaction(
						listJurnal, jenisJurnal, user);
				// CASE vno_perk='11313' AND (vket='2' OR vket='3') AND
				// ((ALLTRIM(STR(YEAR(vtgl_trm)))+ALLTRIM(STR(MONTH(vtgl_trm))))=(ALLTRIM(STR(YEAR(vtgl_jt)))+ALLTRIM(STR(MONTH(vtgl_jt)))))
				// AND ALLTRIM(thisform.text3.value)<>'T10' &&11327 11341
			} else if (terima.getNoPerk().equalsIgnoreCase("11313")
					&& (terima.getKet().equalsIgnoreCase("2") || terima
							.getKet().equalsIgnoreCase("3"))
					&& ((TrioDateConv.format(terima.getTglTrm(), "yyyyMM"))
							.equalsIgnoreCase(TrioDateConv.format(
									terima.getTglJT(), "yyyyMM")))) {

				System.out.println("CASEIF 3");

				Date tglJt = terima.getTglJT();
				// jika debet > 0
				if (terima.getDebet().compareTo(NOL) == 1) {

					if (terima.getKet().equalsIgnoreCase("1")
							|| terima.getKet().equalsIgnoreCase("K")) {
						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(tglJt);
						jurnal.getTrioJurnalPK().setIdPerkiraan("11101");
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan("KAS");
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(terima.getDebet());
						jurnal.setKredit(NOL);
						jurnal.setBlc("*");
						jurnal.setTipe("D");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);

					}
					if (terima.getKet().equalsIgnoreCase("2")) {
						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(tglJt);
						jurnal.getTrioJurnalPK().setIdPerkiraan("11203");
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan("CEK");
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(terima.getDebet());
						jurnal.setKredit(NOL);
						jurnal.setBlc("*");
						jurnal.setTipe("D");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);

					}
					if (terima.getKet().equalsIgnoreCase("3")) {

						String vnoPerk = "11202";
						String vnoSub = terima.getKdBank();

						String vnmSub = getMasterFacade()
								.getTrioMstsubperkiraanDao()
								.findNamaSubByIdAndIdSub(vnoPerk, vnoSub);

						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(tglJt);
						jurnal.getTrioJurnalPK().setIdPerkiraan(vnoPerk);
						jurnal.getTrioJurnalPK().setIdSub(vnoSub);
						jurnal.setNamaPerkiraan(vnmSub);
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(terima.getDebet());
						jurnal.setKredit(NOL);
						jurnal.setBlc("*");
						jurnal.setTipe("D");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);
					}

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(tglJt);
					jurnal.getTrioJurnalPK().setIdPerkiraan("11313");
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan("PIUTANG PENJUALAN");
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(NOL);
					jurnal.setKredit(terima.getDebet());
					jurnal.setBlc("*");
					jurnal.setTipe("K");
					jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
					jurnal.setNoMesin(terima.getNoMesin());
					listJurnal.add(jurnal);

				} else { // jika debet < 0
					if (terima.getKet().equalsIgnoreCase("1")
							|| terima.getKet().equalsIgnoreCase("K")) {
						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(tglJt);
						jurnal.getTrioJurnalPK().setIdPerkiraan("11101");
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan("KAS");
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(NOL);
						jurnal.setKredit(terima.getKredit());
						jurnal.setBlc("*");
						jurnal.setTipe("K");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);
					}
					if (terima.getKet().equalsIgnoreCase("2")) {
						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(tglJt);
						jurnal.getTrioJurnalPK().setIdPerkiraan("11203");
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan("CEK");
						jurnal.setDebet(NOL);
						jurnal.setKredit(terima.getKredit());
						jurnal.setBlc("*");
						jurnal.setTipe("K");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);

					}
					if (terima.getKet().equalsIgnoreCase("3")) {
						String vnoPerk = "11202";
						String vnoSub = terima.getKdBank();

						String vnmSub = getMasterFacade()
								.getTrioMstsubperkiraanDao()
								.findNamaSubByIdAndIdSub(vnoPerk, vnoSub);

						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(tglJt);
						jurnal.getTrioJurnalPK().setIdPerkiraan(vnoPerk);
						jurnal.getTrioJurnalPK().setIdSub(vnoSub);
						jurnal.setNamaPerkiraan(vnmSub);
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(NOL);
						jurnal.setKredit(terima.getKredit());
						jurnal.setBlc("*");
						jurnal.setTipe("K");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);
					}

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(tglJt);
					jurnal.getTrioJurnalPK().setIdPerkiraan("11313");
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan("PIUTANG PENJUALAN");
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(terima.getKredit());
					jurnal.setKredit(NOL);
					jurnal.setBlc("*");
					jurnal.setTipe("D");
					jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
					jurnal.setNoMesin(terima.getNoMesin());
					listJurnal.add(jurnal);
					String jenisJurnal = getJenisJurnalByParams(
							terima.getKet(), terima.getDebet(),
							terima.getKredit(), terima.getNoPerk());
					getMasterFacade().getTrioJurnalDao().saveTransaction(
							listJurnal, jenisJurnal, user);

				}
				// CASE vno_perk='11499' and vket='3' AND (vno_sub='02' OR
				// vno_sub='03' OR vno_sub='04') AND vjns='XXX' AND
				// ALLTRIM(master.kd_prs)<>'T10'
			} else if (terima.getNoPerk().equalsIgnoreCase("11499")
					&& terima.getKet().equalsIgnoreCase("3")
					&& (terima.getNoSub().equalsIgnoreCase("02")
							|| terima.getNoSub().equalsIgnoreCase("03") || terima
							.getNoSub().equalsIgnoreCase("04"))
					&& terima.getJns().equalsIgnoreCase("XXX")) {

				System.out.println("CASEIF 4");

				if (terima.getDebet().compareTo(NOL) == 1) {

					String nmPerk = getMasterFacade().getTrioMstperkiraanDao()
							.findNamaById(terima.getNoPerk());

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan("21299");
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(nmPerk);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(NOL);
					jurnal.setKredit(terima.getDebet());
					jurnal.setBlc("*");
					jurnal.setTipe("K");
					listJurnal.add(jurnal);

					String noSub = terima.getNoSub();
					String nmSub = getMasterFacade()
							.getTrioMstsubperkiraanDao()
							.findNamaSubByIdAndIdSub(terima.getNoPerk(), noSub);
					if (nmSub == null) {
						nmSub = "";
						noSub = "";
					}

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan("11499");
					jurnal.getTrioJurnalPK().setIdPerkiraan(noSub);
					jurnal.setNamaPerkiraan(nmSub);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(terima.getDebet());
					jurnal.setKredit(NOL);
					jurnal.setBlc("*");
					jurnal.setTipe("D");
					listJurnal.add(jurnal);

				} else {

					String nmPerk = getMasterFacade().getTrioMstperkiraanDao()
							.findNamaById(terima.getNoPerk());

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan("21299");
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(nmPerk);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(terima.getKredit());
					jurnal.setKredit(NOL);
					jurnal.setBlc("*");
					jurnal.setTipe("D");
					listJurnal.add(jurnal);

					String noSub = terima.getNoSub();
					String nmSub = getMasterFacade()
							.getTrioMstsubperkiraanDao()
							.findNamaSubByIdAndIdSub(terima.getNoPerk(), noSub);
					if (nmSub == null) {
						nmSub = "";
						noSub = "";
					}
					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan("11499");
					jurnal.getTrioJurnalPK().setIdPerkiraan(noSub);
					jurnal.setNamaPerkiraan(nmSub);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(NOL);
					jurnal.setKredit(terima.getKredit());
					jurnal.setBlc("*");
					jurnal.setTipe("K");
					listJurnal.add(jurnal);
					String jenisJurnal = getJenisJurnalByParams(
							terima.getKet(), terima.getDebet(),
							terima.getKredit(), terima.getNoPerk());
					getMasterFacade().getTrioJurnalDao().saveTransaction(
							listJurnal, jenisJurnal, user);
				}
				// CASE vno_perk='11499' and vket='3' AND (vno_sub='02' OR
				// vno_sub='03' OR vno_sub='04') AND vjns<>'XXX' AND
				// ALLTRIM(master.kd_prs)<>'T10'
			} else if (terima.getNoPerk().equalsIgnoreCase("11499")
					&& terima.getKet().equalsIgnoreCase("3")
					&& (terima.getNoSub().equalsIgnoreCase("02")
							|| terima.getNoSub().equalsIgnoreCase("03") || terima
							.getNoSub().equalsIgnoreCase("04"))
					&& (!terima.getJns().equalsIgnoreCase("XXX"))) {

				System.out.println("CASEIF 5");

				// jika debet > 0
				if (terima.getDebet().compareTo(NOL) == 1) {

					String nmPerk = getMasterFacade().getTrioMstperkiraanDao()
							.findNamaById(terima.getNoPerk());

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan("21299");
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(nmPerk);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(terima.getDebet());
					jurnal.setKredit(NOL);
					jurnal.setBlc("*");
					jurnal.setTipe("D");
					listJurnal.add(jurnal);

					String noSub = terima.getNoSub();
					String nmSub = getMasterFacade()
							.getTrioMstsubperkiraanDao()
							.findNamaSubByIdAndIdSub(terima.getNoPerk(), noSub);
					if (nmSub == null) {
						nmSub = "";
						noSub = "";
					}

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan("11499");
					jurnal.getTrioJurnalPK().setIdPerkiraan(noSub);
					jurnal.setNamaPerkiraan(nmSub);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(NOL);
					jurnal.setKredit(terima.getDebet());
					jurnal.setBlc("*");
					jurnal.setTipe("K");
					listJurnal.add(jurnal);

				} else {
					String nmPerk = getMasterFacade().getTrioMstperkiraanDao()
							.findNamaById(terima.getNoPerk());

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan("21299");
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(nmPerk);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(NOL);
					jurnal.setKredit(terima.getKredit());
					jurnal.setBlc("*");
					jurnal.setTipe("K");
					listJurnal.add(jurnal);

					String noSub = terima.getNoSub();
					String nmSub = getMasterFacade()
							.getTrioMstsubperkiraanDao()
							.findNamaSubByIdAndIdSub(terima.getNoPerk(), noSub);
					if (nmSub == null) {
						nmSub = "";
						noSub = "";
					}

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan("11499");
					jurnal.getTrioJurnalPK().setIdPerkiraan(noSub);
					jurnal.setNamaPerkiraan(nmSub);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(terima.getKredit());
					jurnal.setKredit(NOL);
					jurnal.setBlc("*");
					jurnal.setTipe("D");
					listJurnal.add(jurnal);
					String jenisJurnal = getJenisJurnalByParams(
							terima.getKet(), terima.getDebet(),
							terima.getKredit(), terima.getNoPerk());
					getMasterFacade().getTrioJurnalDao().saveTransaction(
							listJurnal, jenisJurnal, user);
				}
				// CASE vno_perk='11499' and (vket='1' OR vket='K') AND
				// (vno_sub='02') AND ALLTRIM(master.kd_prs)<>'T10'
			} else if (terima.getNoPerk().equalsIgnoreCase("11499")
					&& (terima.getKet().equalsIgnoreCase("1") || terima
							.getKet().equalsIgnoreCase("K"))
					&& terima.getNoSub().equalsIgnoreCase("02")) {

				System.out.println("CASEIF 6");

				// jika debet > 0
				if (terima.getDebet().compareTo(NOL) == 1) {
					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan("11101");
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan("KAS");
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(terima.getDebet());
					jurnal.setKredit(NOL);
					jurnal.setBlc("*");
					jurnal.setTipe("D");
					listJurnal.add(jurnal);

					String nmPerk = getMasterFacade().getTrioMstperkiraanDao()
							.findNamaById("11498");

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan("11498");
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(nmPerk);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(NOL);
					jurnal.setKredit(terima.getDebet());
					jurnal.setBlc("*");
					jurnal.setTipe("K");
					listJurnal.add(jurnal);

				} else {
					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan("11101");
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan("KAS");
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(NOL);
					jurnal.setKredit(terima.getDebet());
					jurnal.setBlc("*");
					jurnal.setTipe("K");
					listJurnal.add(jurnal);

					String nmPerk = getMasterFacade().getTrioMstperkiraanDao()
							.findNamaById("11498");
					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan("11498");
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(nmPerk);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(terima.getKredit());
					jurnal.setKredit(NOL);
					jurnal.setBlc("*");
					jurnal.setTipe("D");
					listJurnal.add(jurnal);
					String jenisJurnal = getJenisJurnalByParams(
							terima.getKet(), terima.getDebet(),
							terima.getKredit(), terima.getNoPerk());
					getMasterFacade().getTrioJurnalDao().saveTransaction(
							listJurnal, jenisJurnal, user);

				}
				// CASE vno_perk='11499' and (vket='1' OR vket='K') AND (vno_sub='03' OR vno_sub='04')
			} else if (terima.getNoPerk().equalsIgnoreCase("11499")
					&& (terima.getKet().equalsIgnoreCase("1") || terima
							.getKet().equalsIgnoreCase("K"))
					&& (terima.getNoSub().equalsIgnoreCase("03") || terima
							.getNoSub().equalsIgnoreCase("04"))) {

				System.out.println("CASEIF 7");

				// jika debet > 0
				if (terima.getDebet().compareTo(NOL) == 1) {
					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan("11101");
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan("KAS");
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(terima.getDebet());
					jurnal.setKredit(NOL);
					jurnal.setBlc("*");
					jurnal.setTipe("D");
					listJurnal.add(jurnal);

					String idPerk = "61106";
					String idSub = "01";
					String nmSub = getMasterFacade()
							.getTrioMstsubperkiraanDao()
							.findNamaSubByIdAndIdSub(idPerk, idSub);
					if (nmSub == null) {
						nmSub = "";
					}
					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(idPerk);
					jurnal.getTrioJurnalPK().setIdSub(idSub);
					jurnal.setNamaPerkiraan(nmSub);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(NOL);
					jurnal.setKredit(terima.getDebet());
					jurnal.setBlc("*");
					jurnal.setTipe("K");
					listJurnal.add(jurnal);

				} else {
					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan("11101");
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan("KAS");
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(NOL);
					jurnal.setKredit(terima.getKredit());
					jurnal.setBlc("*");
					jurnal.setTipe("K");
					listJurnal.add(jurnal);

					String idPerk = "61106";
					String idSub = "01";
					String nmSub = getMasterFacade()
							.getTrioMstsubperkiraanDao()
							.findNamaSubByIdAndIdSub(idPerk, idSub);
					if (nmSub == null) {
						nmSub = "";
					}
					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(idPerk);
					jurnal.getTrioJurnalPK().setIdSub(idSub);
					jurnal.setNamaPerkiraan(nmSub);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(terima.getKredit());
					jurnal.setKredit(NOL);
					jurnal.setBlc("*");
					jurnal.setTipe("D");
					listJurnal.add(jurnal);
					String jenisJurnal = getJenisJurnalByParams(
							terima.getKet(), terima.getDebet(),
							terima.getKredit(), terima.getNoPerk());
					getMasterFacade().getTrioJurnalDao().saveTransaction(
							listJurnal, jenisJurnal, user);

				}
				// case (vno_perk='SUBS' OR vno_perk='CB')&& and (vket='3')
			} else if (terima.getNoPerk().equalsIgnoreCase("SUBS")
					|| terima.getNoPerk().equalsIgnoreCase("CB")) {
				if (terima.getKredit().compareTo(NOL) == 1) {

					System.out.println("CASEIF 8");

					if (terima.getKet2().equalsIgnoreCase("ADR")
							|| terima.getKet2().equalsIgnoreCase("ADS")
							|| terima.getKet2().equalsIgnoreCase("OTO")
							|| terima.getKet2().equalsIgnoreCase("WOM")) {
						if (terima.getsAHM().compareTo(NOL) == 1) {
							jurnal = new TrioJurnal();
							jurnal.setTglJurnal(terima.getTglTrm());
							jurnal.getTrioJurnalPK().setIdPerkiraan("41201");
							jurnal.getTrioJurnalPK().setIdSub("");
							jurnal.setNamaPerkiraan("POTONGAN PENJUALAN AHM");
							jurnal.setKet(terima.getNmTran());
							jurnal.setDebet(terima.getsAHM());
							jurnal.setKredit(NOL);
							jurnal.setBlc("*");
							jurnal.setTipe("D");
							listJurnal.add(jurnal);
						}

						if (terima.getsMD().compareTo(NOL) == 1) {
							jurnal = new TrioJurnal();
							jurnal.setTglJurnal(terima.getTglTrm());
							jurnal.getTrioJurnalPK().setIdPerkiraan("41201");
							jurnal.getTrioJurnalPK().setIdSub("");
							jurnal.setNamaPerkiraan("POTONGAN PENJUALAN MD");
							jurnal.setKet(terima.getNmTran());
							jurnal.setDebet(terima.getsMD());
							jurnal.setKredit(NOL);
							jurnal.setBlc("*");
							jurnal.setTipe("D");
							listJurnal.add(jurnal);
						}

						if (terima.getsSD().compareTo(NOL) == 1) {
							jurnal = new TrioJurnal();
							jurnal.setTglJurnal(terima.getTglTrm());
							jurnal.getTrioJurnalPK().setIdPerkiraan("41201");
							jurnal.getTrioJurnalPK().setIdSub("");
							jurnal.setNamaPerkiraan("POTONGAN PENJUALAN DEALER");
							jurnal.setKet(terima.getNmTran());
							jurnal.setDebet(terima.getsMD());
							jurnal.setKredit(NOL);
							jurnal.setBlc("*");
							jurnal.setTipe("D");
							listJurnal.add(jurnal);
						}

						if (terima.getsFinance().compareTo(NOL) == 1) {

							if (terima.getKet2().equalsIgnoreCase("ADR")
									|| terima.getKet2().equalsIgnoreCase("ADS")
									|| terima.getKet2().equalsIgnoreCase("OTO")) {
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK()
										.setIdPerkiraan("61106");
								jurnal.getTrioJurnalPK().setIdSub("03");
								jurnal.setNamaPerkiraan("VOUCHER PENJUALAN");
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getKredit());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								listJurnal.add(jurnal);
							}

							if (terima.getKet2().equalsIgnoreCase("WOM")) {
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK()
										.setIdPerkiraan("61106");
								jurnal.getTrioJurnalPK().setIdSub("04");
								jurnal.setNamaPerkiraan("VOUCHER PENJUALAN");
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getKredit());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								listJurnal.add(jurnal);
							}
							String vNoPerk0 = null;
							String vNmPerk0 = null;

							String[] arrStr = getNoPerkAndNmPerk(terima
									.getKet2());
							vNoPerk0 = arrStr[0];
							vNmPerk0 = arrStr[0];

							// set Debet
							BigDecimal tempDebet = NOL;

							if (terima.getKet2().equalsIgnoreCase("ADR")
									|| terima.getKet2().equalsIgnoreCase("ADS")) {
								tempDebet = terima.getsFinance().add(
										terima.getsFinance().multiply(
												new BigDecimal(0.1)));
							} else {
								if (terima.getKet2().equalsIgnoreCase("WOM")) {
									tempDebet = new BigDecimal(1100000);
								} else {
									if (terima.getKet2()
											.equalsIgnoreCase("OTO")) {
										//SELECT * FROM terima WHERE no_fak=vno_fak AND no_perk='P3' INTO CURSOR vq1
										TrioTerima tt = getTerimaByNoFakAndNoPerk(terima.getTrioTerimaPK().getNoFak(), "P3");
										tempDebet = tt.getsFinance();
									} else {
										tempDebet = terima.getsFinance();
									}
								}
							}

							jurnal = new TrioJurnal();
							jurnal.setTglJurnal(terima.getTglTrm());
							jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk0);
							jurnal.getTrioJurnalPK().setIdSub("");
							jurnal.setNamaPerkiraan(vNmPerk0);
							jurnal.setKet(terima.getNmTran());
							jurnal.setDebet(tempDebet);
							jurnal.setKredit(NOL);
							jurnal.setBlc("*");
							jurnal.setTipe("D");
							listJurnal.add(jurnal);

							String vNoSub71104 = "";

							if (terima.getKet2().equalsIgnoreCase("ADR")) {
								vNoSub71104 = "01";
							} else if (terima.getKet2().equalsIgnoreCase("ADS")) {
								vNoSub71104 = "02";
							} else if (terima.getKet2().equalsIgnoreCase("OTO")) {
								vNoSub71104 = "03";
							} else if (terima.getKet2().equalsIgnoreCase("WOM")) {
								vNoSub71104 = "04";
							}
							String vNmSub71104 = getMasterFacade()
									.getTrioMstsubperkiraanDao()
									.findNamaSubByIdAndIdSub("71104",
											vNoSub71104);

							jurnal = new TrioJurnal();
							jurnal.setTglJurnal(terima.getTglTrm());
							jurnal.getTrioJurnalPK().setIdPerkiraan("71104");
							jurnal.getTrioJurnalPK().setIdSub(vNoSub71104);
							jurnal.setNamaPerkiraan(vNmSub71104);
							jurnal.setKet(terima.getNmTran());

							if (terima.getKet2().equalsIgnoreCase("ADR")
									|| terima.getKet2().equalsIgnoreCase("ADS")) {
								tempDebet = terima.getsFinance();
							} else {
								if (terima.getKet2().equalsIgnoreCase("WOM")) {
									tempDebet = new BigDecimal(1000000);
								} else {
									if (terima.getKet2()
											.equalsIgnoreCase("OTO")) {
										tempDebet = tempDebet
												.multiply(new BigDecimal(10)
														.divide(new BigDecimal(
																11),
																0,
																RoundingMode.HALF_EVEN));
									} else {
										tempDebet = terima
												.getsFinance()
												.multiply(
														new BigDecimal(10)
																.divide(new BigDecimal(
																		11),
																		0,
																		RoundingMode.HALF_EVEN));
									}
								}
							}

							jurnal.setDebet(tempDebet);
							jurnal.setKredit(NOL);
							jurnal.setBlc("*");
							jurnal.setTipe("D");
							listJurnal.add(jurnal);

							String vNmPerk = getMasterFacade()
									.getTrioMstperkiraanDao().findNamaById(
											"21404");

							jurnal = new TrioJurnal();
							jurnal.setTglJurnal(terima.getTglTrm());
							jurnal.getTrioJurnalPK().setIdPerkiraan("21404");
							jurnal.getTrioJurnalPK().setIdSub("");
							jurnal.setNamaPerkiraan(vNmPerk);
							jurnal.setKet(terima.getNmTran());

							if (terima.getKet2().equalsIgnoreCase("ADS")
									|| terima.getKet2().equalsIgnoreCase("ADS")) {
								tempDebet = terima.getsFinance().multiply(
										new BigDecimal(0.1));
							} else {
								if (terima.getKet2().equalsIgnoreCase("WOM")) {
									tempDebet = new BigDecimal(100000);
								} else {
									if (terima.getKet2()
											.equalsIgnoreCase("OTO")) {
										tempDebet = tempDebet
												.multiply(new BigDecimal(10)
														.divide(new BigDecimal(
																11),
																0,
																RoundingMode.HALF_EVEN));
									} else {
										tempDebet = terima
												.getsFinance()
												.multiply(
														new BigDecimal(10)
																.divide(new BigDecimal(
																		11),
																		0,
																		RoundingMode.HALF_EVEN));
									}
								}
							}
							jurnal.setDebet(tempDebet);
							jurnal.setKredit(NOL);
							jurnal.setBlc("*");
							jurnal.setTipe("D");
							listJurnal.add(jurnal);

						}

						if (terima.getKet().equalsIgnoreCase("3")) {
							jurnal = new TrioJurnal();
							jurnal.setTglJurnal(terima.getTglTrm());
							jurnal.getTrioJurnalPK().setIdPerkiraan("11202");
							jurnal.getTrioJurnalPK().setIdSub(
									terima.getKdBank());
							String vNmSub = getMasterFacade()
									.getTrioMstsubperkiraanDao()
									.findNamaSubByIdAndIdSub("11202",
											terima.getKdBank());
							jurnal.setNamaPerkiraan(vNmSub);
							jurnal.setKet(terima.getNmTran());
							jurnal.setDebet(NOL);
							jurnal.setKredit(terima.getKredit());
							jurnal.setBlc("*");
							jurnal.setTipe("K");
							listJurnal.add(jurnal);
						}
						if (terima.getKet().equalsIgnoreCase("1")
								|| terima.getKet().equalsIgnoreCase("K")) {
							jurnal = new TrioJurnal();
							jurnal.setTglJurnal(terima.getTglTrm());
							jurnal.getTrioJurnalPK().setIdPerkiraan("11101");
							jurnal.getTrioJurnalPK().setIdSub("");
							jurnal.setNamaPerkiraan("KAS");
							jurnal.setKet(terima.getNmTran());
							jurnal.setDebet(NOL);
							jurnal.setKredit(terima.getKredit());
							jurnal.setBlc("*");
							jurnal.setTipe("K");
							listJurnal.add(jurnal);
						}
					} else {
						if (terima.getKet2().length() == 0) {

							if (terima.getsAHM().compareTo(NOL) == 1
									&& terima.getKet2().length() == 0) {
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK()
										.setIdPerkiraan("41201");
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan("POTONGAN PENJUALAN AHM");
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getsAHM());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								jurnal.setNoReff(terima.getTrioTerimaPK()
										.getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);
							}

							if (terima.getsMD().compareTo(NOL) == 1
									&& terima.getKet2().length() == 0) {
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK()
										.setIdPerkiraan("41201");
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan("POTONGAN PENJUALAN MD");
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getsMD());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								jurnal.setNoReff(terima.getTrioTerimaPK()
										.getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);
							}

							if (terima.getsMD().compareTo(NOL) == 1
									&& (terima.getKet2()
											.equalsIgnoreCase("SMF") || terima
											.getKet2().equalsIgnoreCase("MMF"))) {
								// vdinas='G' -> vdinas=no_perk2
								String nmPerk = null;
								String idSub = null;
								if (terima.getNoPerk2().equalsIgnoreCase("G")) {
									idSub = "03";
									nmPerk = getMasterFacade()
											.getTrioMstsubperkiraanDao()
											.findNamaSubByIdAndIdSub("11312",
													idSub);

								} else {
									idSub = "02";
									nmPerk = getMasterFacade()
											.getTrioMstsubperkiraanDao()
											.findNamaSubByIdAndIdSub("11312",
													idSub);
								}
								if (nmPerk.length() < 1) {
									idSub = "";
									nmPerk = "";
								}

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK()
										.setIdPerkiraan("11312");
								jurnal.getTrioJurnalPK().setIdSub(idSub);
								jurnal.setNamaPerkiraan(nmPerk);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getsMD());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								jurnal.setNoReff(terima.getTrioTerimaPK()
										.getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);
							}

							if (terima.getsSD().compareTo(NOL) == 1) {
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK()
										.setIdPerkiraan("41201");
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan("POTONGAN PENJUALAN DEALER");
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getsSD());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								jurnal.setNoReff(terima.getTrioTerimaPK()
										.getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);
							}
							String vNoPerk0 = null;
							String vNmPerk0 = null;

							String[] arrStr = getNoPerkAndNmPerk(terima
									.getKet2());
							vNoPerk0 = arrStr[0];
							vNmPerk0 = arrStr[0];

							BigDecimal finPlusBunga = terima.getsFinance().add(
									terima.getsBunga());
							if (finPlusBunga.compareTo(NOL) == 1) {
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(
										vNoPerk0);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk0);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(finPlusBunga);
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								listJurnal.add(jurnal);
							}

							if (terima.getsAHM().compareTo(NOL) == 1
									&& (terima.getKet2()
											.equalsIgnoreCase("SMF") || terima
											.getKet2().equalsIgnoreCase("MMF"))) {
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(
										vNoPerk0);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk0
										+ " (CASH BACK)");
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getsAHM());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								listJurnal.add(jurnal);
							}

							if (terima.getKet().equalsIgnoreCase("3")) {
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK()
										.setIdPerkiraan("11202");
								jurnal.getTrioJurnalPK().setIdSub(
										terima.getKdBank());
								String nmSubPerk = getMasterFacade()
										.getTrioMstsubperkiraanDao()
										.findNamaSubByIdAndIdSub("11202",
												terima.getKdBank());
								jurnal.setNamaPerkiraan(nmSubPerk);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getKredit());
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								listJurnal.add(jurnal);
							}
							if (terima.getKet().equalsIgnoreCase("1")
									|| terima.getKet().equalsIgnoreCase("K")) {
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK()
										.setIdPerkiraan("11101");
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan("KAS");
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getKredit());
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								listJurnal.add(jurnal);
							}
						} else {
							if (terima.getsAHM().compareTo(NOL) == 1) {
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK()
										.setIdPerkiraan("41201");
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan("POTONGAN PENJUALAN AHM");
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getsAHM());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								listJurnal.add(jurnal);
							}

							if (terima.getsMD().compareTo(NOL) == 1) {
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK()
										.setIdPerkiraan("41201");
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan("POTONGAN PENJUALAN MD");
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getsMD());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								listJurnal.add(jurnal);
							}

							if (terima.getsSD().compareTo(NOL) == 1) {
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK()
										.setIdPerkiraan("41201");
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan("POTONGAN PENJUALAN DEALER");
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getsSD());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								listJurnal.add(jurnal);
							}

							if (terima.getsFinance().compareTo(NOL) == 1) {
								String vNoPerk0 = null;
								String vNmPerk0 = null;

								String[] arrStr = getNoPerkAndNmPerk(terima
										.getKet2());
								vNoPerk0 = arrStr[0];
								vNmPerk0 = arrStr[0];

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(
										vNoPerk0);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk0);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getsFinance());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								listJurnal.add(jurnal);

							}
							if (terima.getKet().equalsIgnoreCase("3")) {
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK()
										.setIdPerkiraan("11202");
								jurnal.getTrioJurnalPK().setIdSub(
										terima.getKdBank());
								String nmSubPerk = getMasterFacade()
										.getTrioMstsubperkiraanDao()
										.findNamaSubByIdAndIdSub("11202",
												terima.getKdBank());
								jurnal.setNamaPerkiraan(nmSubPerk);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getKredit());
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								listJurnal.add(jurnal);
							}

							if (terima.getKet().equalsIgnoreCase("1")
									|| terima.getKet().equalsIgnoreCase("K")) {
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK()
										.setIdPerkiraan("11101");
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan("KAS");
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getKredit());
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								listJurnal.add(jurnal);
							}

						}

					}
					// else dari kredit > 0
				} else {
					if (terima.getKet2().equalsIgnoreCase("ADR")
							|| terima.getKet2().equalsIgnoreCase("ADS")
							|| terima.getKet2().equalsIgnoreCase("OTO")
							|| terima.getKet2().equalsIgnoreCase("WOM")) {
						if (terima.getsAHM().compareTo(NOL) == 1) {
							jurnal = new TrioJurnal();
							jurnal.setTglJurnal(terima.getTglTrm());
							jurnal.getTrioJurnalPK().setIdPerkiraan("41201");
							jurnal.getTrioJurnalPK().setIdSub("");
							jurnal.setNamaPerkiraan("POTONGAN PENJUALAN AHM");
							jurnal.setKet(terima.getNmTran());
							jurnal.setDebet(NOL);
							jurnal.setKredit(terima.getsAHM());
							jurnal.setBlc("*");
							jurnal.setTipe("K");
							listJurnal.add(jurnal);
						}
						if (terima.getsMD().compareTo(NOL) == 1) {
							jurnal = new TrioJurnal();
							jurnal.setTglJurnal(terima.getTglTrm());
							jurnal.getTrioJurnalPK().setIdPerkiraan("41201");
							jurnal.getTrioJurnalPK().setIdSub("");
							jurnal.setNamaPerkiraan("POTONGAN PENJUALAN MD");
							jurnal.setKet(terima.getNmTran());
							jurnal.setDebet(NOL);
							jurnal.setKredit(terima.getsMD());
							jurnal.setBlc("*");
							jurnal.setTipe("K");
							listJurnal.add(jurnal);
						}
						if (terima.getsSD().compareTo(NOL) == 1) {
							jurnal = new TrioJurnal();
							jurnal.setTglJurnal(terima.getTglTrm());
							jurnal.getTrioJurnalPK().setIdPerkiraan("41201");
							jurnal.getTrioJurnalPK().setIdSub("");
							jurnal.setNamaPerkiraan("POTONGAN PENJUALAN DEALER");
							jurnal.setKet(terima.getNmTran());
							jurnal.setDebet(NOL);
							jurnal.setKredit(terima.getsSD());
							jurnal.setBlc("*");
							jurnal.setTipe("K");
							listJurnal.add(jurnal);
						}
						if (terima.getsFinance().compareTo(NOL) == 1) {
							if (terima.getKet2().equalsIgnoreCase("ADR")
									|| terima.getKet2().equalsIgnoreCase("ADS")
									|| terima.getKet2().equalsIgnoreCase("OTO")) {
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK()
										.setIdPerkiraan("61106");
								jurnal.getTrioJurnalPK().setIdSub("03");
								jurnal.setNamaPerkiraan("VOUCHER PENJUALAN");
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getDebet());
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								listJurnal.add(jurnal);
							}
							if (terima.getKet2().equalsIgnoreCase("WOM")) {
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK()
										.setIdPerkiraan("61106");
								jurnal.getTrioJurnalPK().setIdSub("04");
								jurnal.setNamaPerkiraan("VOUCHER PENJUALAN");
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getDebet());
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								listJurnal.add(jurnal);
							}

							String vNoPerk0 = null;
							String vNmPerk0 = null;

							String[] arrStr = getNoPerkAndNmPerk(terima
									.getKet2());
							vNoPerk0 = arrStr[0];
							vNmPerk0 = arrStr[0];

							// set Debet
							BigDecimal tempKredit = NOL;

							if (terima.getKet2().equalsIgnoreCase("ADR")
									|| terima.getKet2().equalsIgnoreCase("ADS")) {
								tempKredit = terima.getsFinance().add(
										terima.getsFinance().multiply(
												new BigDecimal(0.1)));
							} else {
								if (terima.getKet2().equalsIgnoreCase("WOM")) {
									tempKredit = new BigDecimal(1100000);
								} else {
									if (terima.getKet2()
											.equalsIgnoreCase("OTO")) {
										tempKredit = new BigDecimal(1); // masih
																		// TODO
									} else {
										tempKredit = terima.getsFinance();
									}
								}
							}

							jurnal = new TrioJurnal();
							jurnal.setTglJurnal(terima.getTglTrm());
							jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk0);
							jurnal.getTrioJurnalPK().setIdSub("");
							jurnal.setNamaPerkiraan(vNmPerk0);
							jurnal.setKet(terima.getNmTran());
							jurnal.setDebet(NOL);
							jurnal.setKredit(tempKredit);
							jurnal.setBlc("*");
							jurnal.setTipe("K");
							listJurnal.add(jurnal);

							String vNoSub71104 = "";

							if (terima.getKet2().equalsIgnoreCase("ADR")) {
								vNoSub71104 = "01";
							} else if (terima.getKet2().equalsIgnoreCase("ADS")) {
								vNoSub71104 = "02";
							} else if (terima.getKet2().equalsIgnoreCase("OTO")) {
								vNoSub71104 = "03";
							} else if (terima.getKet2().equalsIgnoreCase("WOM")) {
								vNoSub71104 = "04";
							}
							String vNmSub71104 = getMasterFacade()
									.getTrioMstsubperkiraanDao()
									.findNamaSubByIdAndIdSub("71104",
											vNoSub71104);

							jurnal = new TrioJurnal();
							jurnal.setTglJurnal(terima.getTglTrm());
							jurnal.getTrioJurnalPK().setIdPerkiraan("71104");
							jurnal.getTrioJurnalPK().setIdSub(vNoSub71104);
							jurnal.setNamaPerkiraan(vNmSub71104);
							jurnal.setKet(terima.getNmTran());
							BigDecimal tempDebet = NOL;

							if (terima.getKet2().equalsIgnoreCase("ADR")
									|| terima.getKet2().equalsIgnoreCase("ADS")) {
								tempDebet = terima.getsFinance();
							} else {
								if (terima.getKet2().equalsIgnoreCase("WOM")) {
									tempDebet = new BigDecimal(1000000);
								} else {
									if (terima.getKet2()
											.equalsIgnoreCase("OTO")) {
										tempDebet = tempDebet
												.multiply(new BigDecimal(10)
														.divide(new BigDecimal(
																11),
																0,
																RoundingMode.HALF_EVEN));
									} else {
										tempDebet = terima
												.getsFinance()
												.multiply(
														new BigDecimal(10)
																.divide(new BigDecimal(
																		11),
																		0,
																		RoundingMode.HALF_EVEN));
									}
								}
							}

							jurnal.setDebet(tempDebet);
							jurnal.setKredit(NOL);
							jurnal.setBlc("*");
							jurnal.setTipe("D");
							listJurnal.add(jurnal);

							String vNmPerk = getMasterFacade()
									.getTrioMstperkiraanDao().findNamaById(
											"21404");

							jurnal = new TrioJurnal();
							jurnal.setTglJurnal(terima.getTglTrm());
							jurnal.getTrioJurnalPK().setIdPerkiraan("21404");
							jurnal.getTrioJurnalPK().setIdSub("");
							jurnal.setNamaPerkiraan(vNmPerk);
							jurnal.setKet(terima.getNmTran());

							if (terima.getKet2().equalsIgnoreCase("ADS")
									|| terima.getKet2().equalsIgnoreCase("ADS")) {
								tempDebet = terima.getsFinance().multiply(
										new BigDecimal(0.1));
							} else {
								if (terima.getKet2().equalsIgnoreCase("WOM")) {
									tempDebet = new BigDecimal(100000);
								} else {
									if (terima.getKet2()
											.equalsIgnoreCase("OTO")) {
										tempDebet = tempDebet
												.multiply(new BigDecimal(10)
														.divide(new BigDecimal(
																11),
																0,
																RoundingMode.HALF_EVEN));
									} else {
										tempDebet = terima
												.getsFinance()
												.multiply(
														new BigDecimal(10)
																.divide(new BigDecimal(
																		11),
																		0,
																		RoundingMode.HALF_EVEN));
									}
								}
							}
							jurnal.setDebet(tempDebet);
							jurnal.setKredit(NOL);
							jurnal.setBlc("*");
							jurnal.setTipe("D");
							listJurnal.add(jurnal);

						}

						if (terima.getKet().equalsIgnoreCase("3")) {
							String vnoPerk = "11202";
							String vnoSub = terima.getKdBank();

							String vnmSub = getMasterFacade()
									.getTrioMstsubperkiraanDao()
									.findNamaSubByIdAndIdSub(vnoPerk, vnoSub);

							jurnal = new TrioJurnal();
							jurnal.setTglJurnal(terima.getTglTrm());
							jurnal.getTrioJurnalPK().setIdPerkiraan(vnoPerk);
							jurnal.getTrioJurnalPK().setIdSub(vnoSub);
							jurnal.setNamaPerkiraan(vnmSub);
							jurnal.setKet(terima.getNmTran());
							jurnal.setDebet(terima.getDebet());
							jurnal.setKredit(NOL);
							jurnal.setBlc("*");
							jurnal.setTipe("D");
							listJurnal.add(jurnal);
						}
						if (terima.getKet().equalsIgnoreCase("1")
								|| terima.getKet().equalsIgnoreCase("K")) {
							jurnal = new TrioJurnal();
							jurnal.setTglJurnal(terima.getTglTrm());
							jurnal.getTrioJurnalPK().setIdPerkiraan("11101");
							jurnal.getTrioJurnalPK().setIdSub("");
							jurnal.setNamaPerkiraan("KAS");
							jurnal.setKet(terima.getNmTran());
							jurnal.setDebet(terima.getDebet());
							jurnal.setKredit(NOL);
							jurnal.setBlc("*");
							jurnal.setTipe("D");
							listJurnal.add(jurnal);
						}

					} else { // ini else utk ADR ADS OTO WOM
						if (terima.getKet2().length() == 0) {
							if (terima.getsMD().compareTo(NOL) == 1
									&& (terima.getKet2()
											.equalsIgnoreCase("SMF") || terima
											.getKet2().equalsIgnoreCase("MMF"))) {
								String vNmSub = null;
								String idSub = "";
								if (terima.getNoPerk2().equalsIgnoreCase("G")) {
									idSub = "03";
									vNmSub = getMasterFacade()
											.getTrioMstsubperkiraanDao()
											.findNamaSubByIdAndIdSub("11312",
													idSub);
								} else {
									idSub = "02";
									vNmSub = getMasterFacade()
											.getTrioMstsubperkiraanDao()
											.findNamaSubByIdAndIdSub("11312",
													idSub);
								}

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK()
										.setIdPerkiraan("11312");
								jurnal.getTrioJurnalPK().setIdSub(idSub);
								jurnal.setNamaPerkiraan(vNmSub);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getsMD());
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								jurnal.setNoReff(terima.getTrioTerimaPK()
										.getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);
							}

							if (terima.getsAHM().compareTo(NOL) == 1
									&& terima.getKet2().length() == 0) {
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK()
										.setIdPerkiraan("41201");
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan("POTONGAN PENJUALAN AHM");
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getsAHM());
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								jurnal.setNoReff(terima.getTrioTerimaPK()
										.getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);
							}

							if (terima.getsMD().compareTo(NOL) == 1
									&& terima.getKet2().length() == 0) {
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK()
										.setIdPerkiraan("41201");
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan("POTONGAN PENJUALAN MD");
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getsMD());
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								jurnal.setNoReff(terima.getTrioTerimaPK()
										.getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);
							}

							if (terima.getsSD().compareTo(NOL) == 1) {
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK()
										.setIdPerkiraan("41201");
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan("POTONGAN PENJUALAN DEALER");
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getsSD());
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								jurnal.setNoReff(terima.getTrioTerimaPK()
										.getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);
							}

							String vNoPerk0 = null;
							String vNmPerk0 = null;

							String[] arrStr = getNoPerkAndNmPerk(terima
									.getKet2());
							vNoPerk0 = arrStr[0];
							vNmPerk0 = arrStr[0];

							BigDecimal financePlusBunga = terima.getsFinance()
									.add(terima.getsBunga());
							if (financePlusBunga.compareTo(NOL) == 1) {
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(
										vNoPerk0);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk0);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(financePlusBunga);
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								listJurnal.add(jurnal);
							}

							if (terima.getsAHM().compareTo(NOL) == 1
									&& (terima.getKet2()
											.equalsIgnoreCase("SMF") || terima
											.getKet2().equalsIgnoreCase("MMF"))) {
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(
										vNoPerk0);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk0
										+ " (CASH BACK)");
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getsAHM());
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								listJurnal.add(jurnal);
							}

							if (terima.getKet().equalsIgnoreCase("3")) {
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK()
										.setIdPerkiraan("11202");
								jurnal.getTrioJurnalPK().setIdSub(
										terima.getKdBank());

								String vNmSub = getMasterFacade()
										.getTrioMstsubperkiraanDao()
										.findNamaSubByIdAndIdSub("11202",
												terima.getKdBank());
								jurnal.setNamaPerkiraan(vNmSub);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getDebet());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								listJurnal.add(jurnal);
							}

							if (terima.getKet().equalsIgnoreCase("1")
									|| terima.getKet().equalsIgnoreCase("K")) {
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK()
										.setIdPerkiraan("11101");
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan("KAS");
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getDebet());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								listJurnal.add(jurnal);
							}

						} else { // ini else utk terima.getKet2().length==0

							if (terima.getsAHM().compareTo(NOL) == 1) {
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK()
										.setIdPerkiraan("41201");
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan("POTONGAN PENJUALAN AHM");
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getsAHM());
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								listJurnal.add(jurnal);
							}

							if (terima.getsMD().compareTo(NOL) == 1) {

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK()
										.setIdPerkiraan("41201");
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan("POTONGAN PENJUALAN MD");
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getsMD());
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								listJurnal.add(jurnal);
							}

							if (terima.getsSD().compareTo(NOL) == 1) {

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK()
										.setIdPerkiraan("41201");
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan("POTONGAN PENJUALAN DEALER");
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getsSD());
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								listJurnal.add(jurnal);
							}

							if (terima.getsFinance().compareTo(NOL) == 1) {
								String vNoPerk0 = null;
								String vNmPerk0 = null;

								String[] arrStr = getNoPerkAndNmPerk(terima
										.getKet2());
								vNoPerk0 = arrStr[0];
								vNmPerk0 = arrStr[0];

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(
										vNoPerk0);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk0);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getsFinance());
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								listJurnal.add(jurnal);

							}

							if (terima.getKet().equalsIgnoreCase("3")) {
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK()
										.setIdPerkiraan("11202");
								jurnal.getTrioJurnalPK().setIdSub(
										terima.getKdBank());
								String vNmSub = getMasterFacade()
										.getTrioMstsubperkiraanDao()
										.findNamaSubByIdAndIdSub("11202",
												terima.getKdBank());
								jurnal.setNamaPerkiraan(vNmSub);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getDebet());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								listJurnal.add(jurnal);
							}

							if (terima.getKet().equalsIgnoreCase("1")
									|| terima.getKet().equalsIgnoreCase("K")) {
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK()
										.setIdPerkiraan("11101");
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan("KAS");
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getDebet());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								listJurnal.add(jurnal);
							}

						}
					}
					String jenisJurnal = getJenisJurnalByParams(
							terima.getKet(), terima.getDebet(),
							terima.getKredit(), terima.getNoPerk());
					getMasterFacade().getTrioJurnalDao().saveTransaction(
							listJurnal, jenisJurnal, user);
				}
				// case vno_perk='11301' and vket='2' AND ALLTRIM(master.kd_prs)<>'T10'
			} else if (terima.getNoPerk().equalsIgnoreCase("11301")
					&& terima.getKet().equalsIgnoreCase("2")) { // case

				System.out.println("CASEIF 9");

				if (terima.getDebet().compareTo(NOL) == 1) {
					TrioMstsubperkiraan subPerk = getMasterFacade()
							.getTrioMstsubperkiraanDao().getSubByIdPerkAndKet2(
									"11301", terima.getKet2());

					if (subPerk != null) {

						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan("11203");
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan("CEK");
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(terima.getDebet());
						jurnal.setKredit(NOL);
						jurnal.setBlc("*");
						jurnal.setTipe("D");
						listJurnal.add(jurnal);

						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan(
								terima.getNoPerk());
						jurnal.getTrioJurnalPK().setIdSub(
								subPerk.getTrioMstsubperkiraanPK().getIdSub());
						jurnal.setNamaPerkiraan(subPerk.getNamaSub());
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(NOL);
						jurnal.setKredit(terima.getDebet());
						jurnal.setBlc("*");
						jurnal.setTipe("K");
						listJurnal.add(jurnal);

					} else {
						System.out.println("No Mesin : " + terima.getKet2()
								+ " tidak ditemukan !");
					}

				} else {
					TrioMstsubperkiraan subPerk = getMasterFacade()
							.getTrioMstsubperkiraanDao().getSubByIdPerkAndKet2(
									"11301", terima.getKet2());

					if (subPerk != null) {

						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan("11203");
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan("CEK");
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(NOL);
						jurnal.setKredit(terima.getKredit());
						jurnal.setBlc("*");
						jurnal.setTipe("K");
						listJurnal.add(jurnal);

						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan(
								terima.getNoPerk());
						jurnal.getTrioJurnalPK().setIdSub(
								subPerk.getTrioMstsubperkiraanPK().getIdSub());
						jurnal.setNamaPerkiraan(subPerk.getNamaSub());
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(terima.getKredit());
						jurnal.setKredit(NOL);
						jurnal.setBlc("*");
						jurnal.setTipe("D");
						listJurnal.add(jurnal);

					} else {
						System.out.println("No Mesin : " + terima.getKet2()
								+ " tidak ditemukan !");
					}
				}
				String jenisJurnal = getJenisJurnalByParams(terima.getKet(),
						terima.getDebet(), terima.getKredit(),
						terima.getNoPerk());
				getMasterFacade().getTrioJurnalDao().saveTransaction(
						listJurnal, jenisJurnal, user);
				// case vno_perk='11301' and vket='3' AND ALLTRIM(master.kd_prs)<>'T10'
			} else if (terima.getNoPerk().equalsIgnoreCase("11301")
					&& terima.getKet().equalsIgnoreCase("3")) {

				System.out.println("CASEIF 10");

				if (terima.getDebet().compareTo(NOL) == 1) {

					TrioMstsubperkiraan subPerk = getMasterFacade()
							.getTrioMstsubperkiraanDao().getSubByIdPerkAndKet2(
									"11301", terima.getKet2());

					if (subPerk != null) {

						String vNoSub = subPerk.getTrioMstsubperkiraanPK()
								.getIdSub();
						String vNmPerk1 = subPerk.getNamaSub();
						String vNoPerk = "11202";

						String vNmPerk = getMasterFacade()
								.getTrioMstsubperkiraanDao()
								.findNamaSubByIdAndIdSub(vNoPerk, vNoSub);

						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan("11202");
						jurnal.getTrioJurnalPK().setIdSub(terima.getKdBank());
						jurnal.setNamaPerkiraan(vNmPerk + " " + vNmPerk1);
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(terima.getDebet());
						jurnal.setKredit(NOL);
						jurnal.setBlc("*");
						jurnal.setTipe("D");
						listJurnal.add(jurnal);

						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan(
								terima.getNoPerk());
						jurnal.getTrioJurnalPK().setIdSub(vNoSub);
						jurnal.setNamaPerkiraan(vNmPerk);
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(NOL);
						jurnal.setKredit(terima.getDebet());
						jurnal.setBlc("*");
						jurnal.setTipe("K");
						listJurnal.add(jurnal);

					} else {
						System.out.println("No Mesin : " + terima.getKet2()
								+ " tidak ditemukan !");
					}
				} else {
					TrioMstsubperkiraan subPerk = getMasterFacade()
							.getTrioMstsubperkiraanDao().getSubByIdPerkAndKet2(
									"11301", terima.getKet2());

					if (subPerk != null) {

						String vNoSub = subPerk.getTrioMstsubperkiraanPK()
								.getIdSub();
						String vNmPerk1 = subPerk.getNamaSub();
						String vNoPerk = "11202";

						String vNmPerk = getMasterFacade()
								.getTrioMstsubperkiraanDao()
								.findNamaSubByIdAndIdSub(vNoPerk, vNoSub);
						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan("11202");
						jurnal.getTrioJurnalPK().setIdSub(terima.getKdBank());
						jurnal.setNamaPerkiraan(vNmPerk + " " + vNmPerk1);
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(NOL);
						jurnal.setKredit(terima.getKredit());
						jurnal.setBlc("*");
						jurnal.setTipe("K");
						listJurnal.add(jurnal);

						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan(
								terima.getNoPerk());
						jurnal.getTrioJurnalPK().setIdSub(vNoSub);
						jurnal.setNamaPerkiraan(vNmPerk);
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(terima.getKredit());
						jurnal.setKredit(NOL);
						jurnal.setBlc("*");
						jurnal.setTipe("D");
						listJurnal.add(jurnal);

					} else {
						System.out.println("No Mesin : " + terima.getKet2()
								+ " tidak ditemukan !");
					}
				}
				String jenisJurnal = getJenisJurnalByParams(terima.getKet(),
						terima.getDebet(), terima.getKredit(),
						terima.getNoPerk());
				getMasterFacade().getTrioJurnalDao().saveTransaction(
						listJurnal, jenisJurnal, user);
				// case vno_perk='P0' and vket='1' AND vdinas='D'
			} else if (terima.getNoPerk().equalsIgnoreCase("P0")
					&& terima.getKet().equalsIgnoreCase("1")
					&& terima.getNoPerk2().equalsIgnoreCase("D")) {

				System.out.println("CASEIF 11");

				if (terima.getDebet().compareTo(NOL) == 1) {
					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan("11313");
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan("PIUTANG PENJUALAN");
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(terima.getDebet());
					jurnal.setKredit(NOL);
					jurnal.setBlc("*");
					jurnal.setTipe("D");
					jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
					jurnal.setNoMesin(terima.getNoMesin());
					listJurnal.add(jurnal);

					if (terima.getsAHM().compareTo(NOL) == 1) {
						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan("41201");
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan("POTONGAN PENJUALAN AHM");
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(terima.getsAHM());
						jurnal.setKredit(NOL);
						jurnal.setBlc("*");
						jurnal.setTipe("D");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);
					}

					if (terima.getsMD().compareTo(NOL) == 1) {
						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan("41201");
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan("POTONGAN PENJUALAN MD");
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(terima.getsMD());
						jurnal.setKredit(NOL);
						jurnal.setBlc("*");
						jurnal.setTipe("D");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);
					}

					if (terima.getsSD().compareTo(NOL) == 1) {
						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan("41201");
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan("POTONGAN PENJUALAN DEALER");
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(terima.getsSD());
						jurnal.setKredit(NOL);
						jurnal.setBlc("*");
						jurnal.setTipe("D");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);
					}
					// vfee = jml_trm on foxpro
					BigDecimal biayaGabung = terima.getpSTNK().add(
							terima.getpBPKB().add(terima.getJmlTrm()));
					BigDecimal debetPer11 = terima.getDebet().divide(
							new BigDecimal(11), 0, RoundingMode.HALF_EVEN);
					BigDecimal total = debetPer11.add(biayaGabung);
					BigDecimal allHitung = terima.getDebet().subtract(total);

					if (allHitung.compareTo(NOL) == 1) {

						String vNoPerk3 = "41101";
						String vNmPerk3 = getMasterFacade()
								.getTrioMstperkiraanDao()
								.findNamaById(vNoPerk3);
						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk3);
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan(vNmPerk3);
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(NOL);
						jurnal.setKredit(allHitung);
						jurnal.setBlc("*");
						jurnal.setTipe("K");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);
					}

					String vNoPerk4 = "21404";
					String vNmPerk4 = getMasterFacade()
							.getTrioMstperkiraanDao().findNamaById(vNoPerk4);

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk4);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk4);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(NOL);
					jurnal.setKredit(terima.getDebet().divide(
							new BigDecimal(11), 0, RoundingMode.HALF_EVEN)); // vdebet/11
																				// on
																				// foxpro
					jurnal.setBlc("*");
					jurnal.setTipe("K");
					jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
					jurnal.setNoMesin(terima.getNoMesin());
					listJurnal.add(jurnal);

					// if vs_ahm+vs_md+vs_sd>0
					BigDecimal jumlahSubsidi = terima.getsAHM().add(
							terima.getsMD().add(terima.getsSD()));
					if (jumlahSubsidi.compareTo(NOL) == 1) {

						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan("41201");
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan("POTONGAN PENJUALAN");
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(NOL);
						jurnal.setKredit(jumlahSubsidi);
						jurnal.setBlc("*");
						jurnal.setTipe("K");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);
					}

					String vNoPerk5 = "21204";
					String vNmPerk5 = getMasterFacade()
							.getTrioMstperkiraanDao().findNamaById(vNoPerk5);
					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk5);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk5);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(NOL);
					jurnal.setKredit(terima.getpSTNK());
					jurnal.setBlc("*");
					jurnal.setTipe("K");
					jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
					jurnal.setNoMesin(terima.getNoMesin());
					listJurnal.add(jurnal);

					String vNoPerk6 = "21205";
					String vNmPerk6 = getMasterFacade()
							.getTrioMstperkiraanDao().findNamaById(vNoPerk6);
					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk6);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk6);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(NOL);
					jurnal.setKredit(terima.getpBPKB());
					jurnal.setBlc("*");
					jurnal.setTipe("K");
					jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
					jurnal.setNoMesin(terima.getNoMesin());
					listJurnal.add(jurnal);

					if (terima.getJmlTrm().compareTo(NOL) == 1) {
						String vNoPerk7 = "21309";
						String vNmPerk7 = getMasterFacade()
								.getTrioMstperkiraanDao()
								.findNamaById(vNoPerk7);
						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk7);
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan(vNmPerk7);
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(NOL);
						jurnal.setKredit(terima.getJmlTrm());
						jurnal.setBlc("*");
						jurnal.setTipe("K");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);
					}

				} else {

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan("11313");
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan("PIUTANG PENJUALAN");
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(NOL);
					jurnal.setKredit(terima.getKredit());
					jurnal.setBlc("*");
					jurnal.setTipe("K");
					jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
					jurnal.setNoMesin(terima.getNoMesin());
					listJurnal.add(jurnal);

					if (terima.getsAHM().compareTo(NOL) == 1) {
						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan("41201");
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan("POTONGAN PENJUALAN AHM");
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(NOL);
						jurnal.setKredit(terima.getsAHM());
						jurnal.setBlc("*");
						jurnal.setTipe("K");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);
					}

					if (terima.getsMD().compareTo(NOL) == 1) {
						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan("41201");
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan("POTONGAN PENJUALAN MD");
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(NOL);
						jurnal.setKredit(terima.getsMD());
						jurnal.setBlc("*");
						jurnal.setTipe("K");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);
					}

					if (terima.getsSD().compareTo(NOL) == 1) {
						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan("41201");
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan("POTONGAN PENJUALAN DEALER");
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(NOL);
						jurnal.setKredit(terima.getsSD());
						jurnal.setBlc("*");
						jurnal.setTipe("K");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);
					}
					// vfee = jml_trm on foxpro
					BigDecimal biayaGabung = terima.getpSTNK().add(
							terima.getpBPKB().add(terima.getJmlTrm()));
					BigDecimal kreditPer11 = terima.getKredit().divide(
							new BigDecimal(11), 0, RoundingMode.HALF_EVEN);
					BigDecimal total = kreditPer11.add(biayaGabung);
					BigDecimal allHitung = terima.getKredit().subtract(total);

					if (allHitung.compareTo(NOL) == 1) {

						String vNoPerk3 = "41101";
						String vNmPerk3 = getMasterFacade()
								.getTrioMstperkiraanDao()
								.findNamaById(vNoPerk3);
						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk3);
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan(vNmPerk3);
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(allHitung);
						jurnal.setKredit(NOL);
						jurnal.setBlc("*");
						jurnal.setTipe("D");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);
					}

					String vNoPerk4 = "21404";
					String vNmPerk4 = getMasterFacade()
							.getTrioMstperkiraanDao().findNamaById(vNoPerk4);

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk4);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk4);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(terima.getKredit().divide(
							new BigDecimal(11), 0, RoundingMode.HALF_EVEN)); // vdebet/11
																				// on
																				// foxpro
					jurnal.setKredit(NOL);
					jurnal.setBlc("*");
					jurnal.setTipe("D");
					jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
					jurnal.setNoMesin(terima.getNoMesin());
					listJurnal.add(jurnal);

					// if vs_ahm+vs_md+vs_sd>0
					BigDecimal jumlahSubsidi = terima.getsAHM().add(
							terima.getsMD().add(terima.getsSD()));
					if (jumlahSubsidi.compareTo(NOL) == 1) {

						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan("41201");
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan("POTONGAN PENJUALAN");
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(jumlahSubsidi);
						jurnal.setKredit(NOL);
						jurnal.setBlc("*");
						jurnal.setTipe("D");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);
					}

					String vNoPerk5 = "21204";
					String vNmPerk5 = getMasterFacade()
							.getTrioMstperkiraanDao().findNamaById(vNoPerk5);
					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk5);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk5);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(terima.getpSTNK());
					jurnal.setKredit(NOL);
					jurnal.setBlc("*");
					jurnal.setTipe("D");
					jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
					jurnal.setNoMesin(terima.getNoMesin());
					listJurnal.add(jurnal);

					String vNoPerk6 = "21205";
					String vNmPerk6 = getMasterFacade()
							.getTrioMstperkiraanDao().findNamaById(vNoPerk6);
					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk6);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk6);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(terima.getpBPKB());
					jurnal.setKredit(NOL);
					jurnal.setBlc("*");
					jurnal.setTipe("D");
					jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
					jurnal.setNoMesin(terima.getNoMesin());
					listJurnal.add(jurnal);

					if (terima.getJmlTrm().compareTo(NOL) == 1) {
						String vNoPerk7 = "21309";
						String vNmPerk7 = getMasterFacade()
								.getTrioMstperkiraanDao()
								.findNamaById(vNoPerk7);
						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk7);
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan(vNmPerk7);
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(terima.getJmlTrm());
						jurnal.setKredit(NOL);
						jurnal.setBlc("*");
						jurnal.setTipe("D");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);
					}

				}
				String jenisJurnal = getJenisJurnalByParams(terima.getKet(),
						terima.getDebet(), terima.getKredit(),
						terima.getNoPerk());
				getMasterFacade().getTrioJurnalDao().saveTransaction(
						listJurnal, jenisJurnal, user);
				// case vno_perk='P1' AND vdinas='D'
			} else if (terima.getNoPerk().equalsIgnoreCase("P1")
					&& terima.getNoPerk2().equalsIgnoreCase("D")) {

				System.out.println("CASEIF 12");

				if (terima.getKet().equalsIgnoreCase("1")
						|| terima.getKet().equalsIgnoreCase("K")) {
					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan("11101");
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan("KAS");
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(terima.getDebet());
					jurnal.setKredit(NOL);
					jurnal.setBlc("*");
					jurnal.setTipe("D");
					jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
					jurnal.setNoMesin(terima.getNoMesin());
					listJurnal.add(jurnal);
				}

				if (terima.getKet().equalsIgnoreCase("2")) {
					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan("11203");
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan("CEK");
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(terima.getDebet());
					jurnal.setKredit(NOL);
					jurnal.setBlc("*");
					jurnal.setTipe("D");
					jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
					jurnal.setNoMesin(terima.getNoMesin());
					listJurnal.add(jurnal);
				}

				if (terima.getKet().equalsIgnoreCase("3")) {

					String vNoPerk = "11202";
					String vNoSub = terima.getKdBank();
					String vNmSub = "";
					if (vNoPerk.length() > 0) {
						vNmSub = getMasterFacade().getTrioMstsubperkiraanDao()
								.findNamaSubByIdAndIdSub(vNoPerk, vNoSub);
					}

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
					jurnal.getTrioJurnalPK().setIdSub(vNoSub);
					jurnal.setNamaPerkiraan(vNmSub);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(terima.getDebet());
					jurnal.setKredit(NOL);
					jurnal.setBlc("*");
					jurnal.setTipe("D");
					jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
					jurnal.setNoMesin(terima.getNoMesin());
					listJurnal.add(jurnal);

					if (terima.getsAHM().compareTo(NOL) == 1) {
						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan("41201");
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan("POTONGAN PENJUALAN AHM");
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(terima.getsAHM());
						jurnal.setKredit(NOL);
						jurnal.setBlc("*");
						jurnal.setTipe("D");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);
					}

					if (terima.getsMD().compareTo(NOL) == 1) {
						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan("41201");
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan("POTONGAN PENJUALAN MD");
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(terima.getsMD());
						jurnal.setKredit(NOL);
						jurnal.setBlc("*");
						jurnal.setTipe("D");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);
					}
					
					if (terima.getsSD().compareTo(NOL) == 1) {
						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan("41201");
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan("POTONGAN PENJUALAN DEALER");
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(terima.getsMD());
						jurnal.setKredit(NOL);
						jurnal.setBlc("*");
						jurnal.setTipe("D");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);
					}

					BigDecimal biayaGabung = terima.getpSTNK().add(
							terima.getpBPKB().add(terima.getJmlTrm()));
					BigDecimal debetPer11 = terima.getDebet().divide(
							new BigDecimal(11), 0, RoundingMode.HALF_EVEN);
					BigDecimal total = debetPer11.add(biayaGabung);
					BigDecimal allHitung = terima.getDebet().subtract(total);

					if (allHitung.compareTo(NOL) == 1) {

						String vNoPerk3 = "41101";
						String vNmPerk3 = getMasterFacade()
								.getTrioMstperkiraanDao()
								.findNamaById(vNoPerk3);

						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk3);
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan(vNmPerk3);
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(NOL);
						jurnal.setKredit(allHitung);
						jurnal.setBlc("*");
						jurnal.setTipe("K");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);

					}

					String vNoPerk4 = "21404";
					String vNmPerk4 = getMasterFacade()
							.getTrioMstperkiraanDao().findNamaById(vNoPerk4);

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk4);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk4);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(NOL);
					jurnal.setKredit(terima.getDebet().divide(
							new BigDecimal(11), 0, RoundingMode.HALF_EVEN));
					jurnal.setBlc("*");
					jurnal.setTipe("K");
					jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
					jurnal.setNoMesin(terima.getNoMesin());
					listJurnal.add(jurnal);

					// if vs_ahm+vs_md+vs_sd>0
					BigDecimal jumlahSubsidi = terima.getsAHM().add(
							terima.getsMD().add(terima.getsSD()));

					if (jumlahSubsidi.compareTo(NOL) == 1) {
						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan("41201");
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan("POTONGAN PENJUALAN");
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(NOL);
						jurnal.setKredit(jumlahSubsidi);
						jurnal.setBlc("*");
						jurnal.setTipe("K");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);
					}

					String vNoPerk5 = "21204";
					String vNmPerk5 = getMasterFacade()
							.getTrioMstperkiraanDao().findNamaById(vNoPerk5);

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk5);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk5);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(NOL);
					jurnal.setKredit(terima.getpSTNK());
					jurnal.setBlc("*");
					jurnal.setTipe("K");
					jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
					jurnal.setNoMesin(terima.getNoMesin());
					listJurnal.add(jurnal);

					String vNoPerk6 = "21205";
					String vNmPerk6 = getMasterFacade()
							.getTrioMstperkiraanDao().findNamaById(vNoPerk6);

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk6);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk6);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(NOL);
					jurnal.setKredit(terima.getpBPKB());
					jurnal.setBlc("*");
					jurnal.setTipe("K");
					jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
					jurnal.setNoMesin(terima.getNoMesin());
					listJurnal.add(jurnal);

					if (terima.getJmlTrm().compareTo(NOL) == 1) {
						String vNoPerk7 = "21309";
						String vNmPerk7 = getMasterFacade()
								.getTrioMstperkiraanDao()
								.findNamaById(vNoPerk7);

						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk7);
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan(vNmPerk7);
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(NOL);
						jurnal.setKredit(terima.getJmlTrm());
						jurnal.setBlc("*");
						jurnal.setTipe("K");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);
					}

				}
				String jenisJurnal = getJenisJurnalByParams(terima.getKet(),
						terima.getDebet(), terima.getKredit(),
						terima.getNoPerk());
				getMasterFacade().getTrioJurnalDao().saveTransaction(
						listJurnal, jenisJurnal, user);
				// case vno_perk='P0' and vket='1' AND vdinas<>'D'
			} else if (terima.getNoPerk().equalsIgnoreCase("P0")
					&& terima.getKet().equalsIgnoreCase("1")
					&& (!terima.getNoPerk2().equalsIgnoreCase("D"))) { // case
																		// vno_perk='P0'
																		// and
																		// vket='1'
																		// AND
																		// vdinas<>'D'

				System.out.println("CASEIF 13");

				if (terima.getDebet().compareTo(NOL) == 1) {
					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan("11313");
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan("PIUTANG PENJUALAN");
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(terima.getDebet());
					jurnal.setKredit(NOL);
					jurnal.setBlc("*");
					jurnal.setTipe("D");
					jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
					jurnal.setNoMesin(terima.getNoMesin());
					listJurnal.add(jurnal);

					// vdpp=round(((vharga-vb_stnk-(vs_ahm+vs_md+vs_sd+vs_finance))*10/11),0)
					// vppnx=(vharga-vb_stnk-(vs_ahm+vs_md+vs_sd+vs_finance))-vdpp
					// vpot_penjualan=vs_ahm+vs_md+vs_sd+vs_finance

					BigDecimal totalSubsidi = NOL;
					BigDecimal hitungDPP = NOL;
					BigDecimal vDPP = NOL;

					BigDecimal vPPNX = NOL;
					BigDecimal vPotPenjualan = NOL;

					if (terima.getKet2().equalsIgnoreCase("ADR")
							|| terima.getKet2().equalsIgnoreCase("ADS")
							|| terima.getKet2().equalsIgnoreCase("OTO")) {

						totalSubsidi = terima.getsAHM().add(
								terima.getsMD().add(
										terima.getsSD().add(
												terima.getsFinance())));
						hitungDPP = terima.getHarga()
								.subtract(terima.getbSTNK())
								.subtract(totalSubsidi);
						vDPP = hitungDPP.multiply(new BigDecimal(10)).divide(
								new BigDecimal(11), 0, RoundingMode.HALF_EVEN);

						vPPNX = hitungDPP.subtract(vDPP);
						vPotPenjualan = totalSubsidi;

					} else {
						totalSubsidi = terima.getsAHM().add(
								terima.getsMD().add(terima.getsSD()));
						hitungDPP = terima.getHarga()
								.subtract(terima.getbSTNK())
								.subtract(totalSubsidi);
						vDPP = hitungDPP.multiply(new BigDecimal(10)).divide(
								new BigDecimal(11), 0, RoundingMode.HALF_EVEN);

						vPPNX = hitungDPP.subtract(vDPP);
						vPotPenjualan = totalSubsidi;
					}

					String vNoPerk1 = "41101";
					String vNoPerk2 = "21404";
					String vNoPerk3 = "21204";
					String vNoPerk4 = "21205";
					String vNoPerk5 = "21228";
					String vNoPerk6 = "41201";

					String vNmPerk1 = getMasterFacade()
							.getTrioMstperkiraanDao().findNamaById(vNoPerk1);

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk1);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk1);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(NOL);
					jurnal.setKredit(vDPP);
					jurnal.setBlc("*");
					jurnal.setTipe("K");
					jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
					jurnal.setNoMesin(terima.getNoMesin());
					listJurnal.add(jurnal);

					String vNmPerk2 = getMasterFacade()
							.getTrioMstperkiraanDao().findNamaById(vNoPerk2);

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk2);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk2);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(NOL);
					jurnal.setKredit(vPPNX);
					jurnal.setBlc("*");
					jurnal.setTipe("K");
					jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
					jurnal.setNoMesin(terima.getNoMesin());
					listJurnal.add(jurnal);

					if (vPotPenjualan.compareTo(NOL) == 1) {
						String vNmPerk6 = getMasterFacade()
								.getTrioMstperkiraanDao()
								.findNamaById(vNoPerk6);

						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk6);
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan(vNmPerk6);
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(NOL);
						jurnal.setKredit(vPotPenjualan);
						jurnal.setBlc("*");
						jurnal.setTipe("K");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);
					}

					String vNmPerk3 = getMasterFacade()
							.getTrioMstperkiraanDao().findNamaById(vNoPerk3);
					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk3);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk3);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(NOL);
					jurnal.setKredit(terima.getpSTNK());
					jurnal.setBlc("*");
					jurnal.setTipe("K");
					jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
					jurnal.setNoMesin(terima.getNoMesin());
					listJurnal.add(jurnal);

					String vNmPerk4 = getMasterFacade()
							.getTrioMstperkiraanDao().findNamaById(vNoPerk4);
					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk4);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk4);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(NOL);
					jurnal.setKredit(terima.getpBPKB());
					jurnal.setBlc("*");
					jurnal.setTipe("K");
					jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
					jurnal.setNoMesin(terima.getNoMesin());
					listJurnal.add(jurnal);

					String vNmPerk5 = getMasterFacade()
							.getTrioMstperkiraanDao().findNamaById(vNoPerk5);
					// vb_stnk-vp_stnk-vp_bpkb
					BigDecimal biaya = terima.getbSTNK()
							.subtract(terima.getpSTNK())
							.subtract(terima.getpBPKB());
					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk5);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk5);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(NOL);
					jurnal.setKredit(biaya);
					jurnal.setBlc("*");
					jurnal.setTipe("K");
					jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
					jurnal.setNoMesin(terima.getNoMesin());
					listJurnal.add(jurnal);

				} else { //

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan("11313");
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan("PIUTANG PENJUALAN");
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(NOL);
					jurnal.setKredit(terima.getKredit());// bro
					jurnal.setBlc("*");
					jurnal.setTipe("K");
					jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
					jurnal.setNoMesin(terima.getNoMesin());
					listJurnal.add(jurnal);

					// vdpp=round(((vharga-vb_stnk-(vs_ahm+vs_md+vs_sd+vs_finance))*10/11),0)
					// vppnx=(vharga-vb_stnk-(vs_ahm+vs_md+vs_sd+vs_finance))-vdpp
					// vpot_penjualan=vs_ahm+vs_md+vs_sd+vs_finance

					BigDecimal totalSubsidi = NOL;
					BigDecimal hitungDPP = NOL;
					BigDecimal vDPP = NOL;

					BigDecimal vPPNX = NOL;
					BigDecimal vPotPenjualan = NOL;

					if (terima.getKet2().equalsIgnoreCase("ADR")
							|| terima.getKet2().equalsIgnoreCase("ADS")
							|| terima.getKet2().equalsIgnoreCase("OTO")) {

						totalSubsidi = terima.getsAHM().add(
								terima.getsMD().add(
										terima.getsSD().add(
												terima.getsFinance())));
						hitungDPP = terima.getHarga()
								.subtract(terima.getbSTNK())
								.subtract(totalSubsidi);
						vDPP = hitungDPP.multiply(new BigDecimal(10)).divide(
								new BigDecimal(11), 0, RoundingMode.HALF_EVEN);

						vPPNX = hitungDPP.subtract(vDPP);
						vPotPenjualan = totalSubsidi;

					} else {
						totalSubsidi = terima.getsAHM().add(
								terima.getsMD().add(terima.getsSD()));
						hitungDPP = terima.getHarga()
								.subtract(terima.getbSTNK())
								.subtract(totalSubsidi);
						vDPP = hitungDPP.multiply(new BigDecimal(10)).divide(
								new BigDecimal(11), 0, RoundingMode.HALF_EVEN);

						vPPNX = hitungDPP.subtract(vDPP);
						vPotPenjualan = totalSubsidi;
					}

					String vNoPerk1 = "41101";
					String vNoPerk2 = "21404";
					String vNoPerk3 = "21204";
					String vNoPerk4 = "21205";
					String vNoPerk5 = "21228";
					String vNoPerk6 = "41201";

					String vNmPerk1 = getMasterFacade()
							.getTrioMstperkiraanDao().findNamaById(vNoPerk1);

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk1);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk1);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(vDPP);
					jurnal.setKredit(NOL);
					jurnal.setBlc("*");
					jurnal.setTipe("D");
					jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
					jurnal.setNoMesin(terima.getNoMesin());
					listJurnal.add(jurnal);

					String vNmPerk2 = getMasterFacade()
							.getTrioMstperkiraanDao().findNamaById(vNoPerk2);

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk2);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk2);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(vPPNX);
					jurnal.setKredit(NOL);
					jurnal.setBlc("*");
					jurnal.setTipe("D");
					jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
					jurnal.setNoMesin(terima.getNoMesin());
					listJurnal.add(jurnal);

					if (vPotPenjualan.compareTo(NOL) == 1) {
						String vNmPerk6 = getMasterFacade()
								.getTrioMstperkiraanDao()
								.findNamaById(vNoPerk6);

						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk6);
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan(vNmPerk6);
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(vPotPenjualan);
						jurnal.setKredit(NOL);
						jurnal.setBlc("*");
						jurnal.setTipe("D");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);
					}

					String vNmPerk3 = getMasterFacade()
							.getTrioMstperkiraanDao().findNamaById(vNoPerk3);
					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk3);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk3);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(terima.getpSTNK());
					jurnal.setKredit(NOL);
					jurnal.setBlc("*");
					jurnal.setTipe("D");
					jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
					jurnal.setNoMesin(terima.getNoMesin());
					listJurnal.add(jurnal);

					String vNmPerk4 = getMasterFacade()
							.getTrioMstperkiraanDao().findNamaById(vNoPerk4);
					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk4);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk4);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(terima.getpBPKB());
					jurnal.setKredit(NOL);
					jurnal.setBlc("*");
					jurnal.setTipe("D");
					jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
					jurnal.setNoMesin(terima.getNoMesin());
					listJurnal.add(jurnal);

					String vNmPerk5 = getMasterFacade()
							.getTrioMstperkiraanDao().findNamaById(vNoPerk5);
					// vb_stnk-vp_stnk-vp_bpkb
					BigDecimal biaya = terima.getbSTNK()
							.subtract(terima.getpSTNK())
							.subtract(terima.getpBPKB());
					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk5);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk5);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(biaya);
					jurnal.setKredit(NOL);
					jurnal.setBlc("*");
					jurnal.setTipe("D");
					jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
					jurnal.setNoMesin(terima.getNoMesin());
					listJurnal.add(jurnal);

				}
				String jenisJurnal = getJenisJurnalByParams(terima.getKet(),
						terima.getDebet(), terima.getKredit(),
						terima.getNoPerk());
				getMasterFacade().getTrioJurnalDao().saveTransaction(
						listJurnal, jenisJurnal, user);
				// CASE (vket='1' OR vket='K') AND (vno_perk='21205')
			} else if ((terima.getKet().equalsIgnoreCase("1") || terima
					.getKet().equalsIgnoreCase("K"))
					&& terima.getNoPerk().equalsIgnoreCase("21205")) { // case

				System.out.println("CASEIF 14");

				if (terima.getDebet().compareTo(NOL) == 1) {
					String vNoPerk = "11101";

					String vNmPerk = getMasterFacade().getTrioMstperkiraanDao()
							.findNamaById(vNoPerk);
					if (vNmPerk == null) {
						vNmPerk = "";
					}

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(terima.getDebet());
					jurnal.setKredit(NOL);
					jurnal.setBlc("*");
					jurnal.setTipe("D");
					listJurnal.add(jurnal);

					vNoPerk = "11495";

					vNmPerk = getMasterFacade().getTrioMstperkiraanDao()
							.findNamaById(vNoPerk);
					if (vNmPerk == null) {
						vNmPerk = "";
					}

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(NOL);
					jurnal.setKredit(terima.getDebet());
					jurnal.setBlc("*");
					jurnal.setTipe("K");
					listJurnal.add(jurnal);

				} else { // else debet > 0
					String vNoPerk = "11495";

					String vNmPerk = getMasterFacade().getTrioMstperkiraanDao()
							.findNamaById(vNoPerk);
					if (vNmPerk == null) {
						vNmPerk = "";
					}

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(terima.getKredit());
					jurnal.setKredit(NOL);
					jurnal.setBlc("*");
					jurnal.setTipe("D");
					listJurnal.add(jurnal);

					vNoPerk = "11101";
					vNmPerk = getMasterFacade().getTrioMstperkiraanDao()
							.findNamaById(vNoPerk);
					if (vNmPerk == null) {
						vNmPerk = "";
					}

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(NOL);
					jurnal.setKredit(terima.getKredit());
					jurnal.setBlc("*");
					jurnal.setTipe("K");
					listJurnal.add(jurnal);
				}
				String jenisJurnal = getJenisJurnalByParams(terima.getKet(),
						terima.getDebet(), terima.getKredit(),
						terima.getNoPerk());
				getMasterFacade().getTrioJurnalDao().saveTransaction(
						listJurnal, jenisJurnal, user);

				// CASE (vket='1' OR vket='K') AND (vno_perk='21204')
			} else if ((terima.getKet().equalsIgnoreCase("1") || terima
					.getKet().equalsIgnoreCase("K"))	
					&& terima.getNoPerk().equalsIgnoreCase("21204")) {

				System.out.println("CASEIF 15");

				// TODO TO DO pending karena membaca tabel d_stnk

				// CASE (vket='1' OR vket='K') AND (vno_perk='21205')
			} else if ((terima.getKet().equalsIgnoreCase("1"))
					|| terima.getKet().equalsIgnoreCase("K")
					&& terima.getNoPerk().equalsIgnoreCase("21205")) {
				System.out.println("CASEIF 16");
				
				if (terima.getDebet().compareTo(NOL) == 1) {
					String vNoPerk = "11101";
					String vNmPerk = getMasterFacade().getTrioMstperkiraanDao().findNamaById(vNoPerk);
					if (vNmPerk == null || vNmPerk.length() < 1){
						vNmPerk = "";
					}
					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
					jurnal.getTrioJurnalPK().setIdSub(terima.getKdBank());
					jurnal.setNamaPerkiraan(vNmPerk);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(terima.getDebet());
					jurnal.setKredit(NOL);
					jurnal.setBlc("*");
					jurnal.setTipe("D");
					listJurnal.add(jurnal);
					
					vNoPerk = "11495";
					vNmPerk = getMasterFacade().getTrioMstperkiraanDao().findNamaById(vNoPerk);
					if (vNmPerk == null || vNmPerk.length() < 1){
						vNmPerk = "";
					}
					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(NOL);
					jurnal.setKredit(terima.getDebet());
					jurnal.setBlc("*");
					jurnal.setTipe("K");
					listJurnal.add(jurnal);
					
				} else { // else debet > 0
					String vNoPerk = "11495";
					String vNmPerk = getMasterFacade().getTrioMstperkiraanDao().findNamaById(vNoPerk);
					if (vNmPerk == null || vNmPerk.length() < 1){
						vNmPerk = "";
					}
					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
					jurnal.getTrioJurnalPK().setIdSub(terima.getKdBank());
					jurnal.setNamaPerkiraan(vNmPerk);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(terima.getKredit());
					jurnal.setKredit(NOL);
					jurnal.setBlc("*");
					jurnal.setTipe("D");
					listJurnal.add(jurnal);
					
					vNoPerk = "11101";
					vNmPerk = getMasterFacade().getTrioMstperkiraanDao().findNamaById(vNoPerk);
					if (vNmPerk == null || vNmPerk.length() < 1){
						vNmPerk = "";
					}
					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(NOL);
					jurnal.setKredit(terima.getKredit());
					jurnal.setBlc("*");
					jurnal.setTipe("K");
					listJurnal.add(jurnal);
				}

				String jenisJurnal = getJenisJurnalByParams(terima.getKet(),
						terima.getDebet(), terima.getKredit(),
						terima.getNoPerk());
				getMasterFacade().getTrioJurnalDao().saveTransaction(
						listJurnal, jenisJurnal, user);
				// CASE vket='2' AND (vno_perk='21204' OR vno_perk='21205')
			} else if (terima.getKet2().equalsIgnoreCase("2")
					&& (terima.getNoPerk().equalsIgnoreCase("21204") || terima
							.getNoPerk().equalsIgnoreCase("21205"))) {
				System.out.println("CASEIF 17");

				if (terima.getDebet().compareTo(NOL) == 1) {
					String vNoPerk = "11202";
					String vNmPerk = "";
					if (terima.getKdBank().length() > 0) {
						vNmPerk = getMasterFacade().getTrioMstsubperkiraanDao()
								.findNamaSubByIdAndIdSub(vNoPerk,
										terima.getKdBank());
					} else {
						vNmPerk = "";
					}
					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
					jurnal.getTrioJurnalPK().setIdSub(terima.getKdBank());
					jurnal.setNamaPerkiraan(vNmPerk);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(terima.getDebet());
					jurnal.setKredit(NOL);
					jurnal.setBlc("*");
					jurnal.setTipe("D");
					listJurnal.add(jurnal);

					vNoPerk = "11495";
					vNmPerk = "";
					if (terima.getKdBank().length() > 0) {
						vNmPerk = getMasterFacade().getTrioMstperkiraanDao()
								.findNamaById(vNoPerk);
					} else {
						vNmPerk = "";
					}
					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(NOL);
					jurnal.setKredit(terima.getDebet());
					jurnal.setBlc("*");
					jurnal.setTipe("K");
					listJurnal.add(jurnal);

				} else { // debet > 0

					String vNoPerk = "11495";
					String vNmPerk = "";
					if (terima.getKdBank().length() > 0) {
						vNmPerk = getMasterFacade().getTrioMstperkiraanDao()
								.findNamaById(vNoPerk);
					} else {
						vNmPerk = "";
					}
					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(terima.getKredit());
					jurnal.setKredit(NOL);
					jurnal.setBlc("*");
					jurnal.setTipe("D");
					listJurnal.add(jurnal);

					vNoPerk = "11202";
					vNmPerk = "";
					if (terima.getKdBank().length() > 0) {
						vNmPerk = getMasterFacade().getTrioMstsubperkiraanDao()
								.findNamaSubByIdAndIdSub(vNoPerk,
										terima.getKdBank());
					} else {
						vNmPerk = "";
					}
					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
					jurnal.getTrioJurnalPK().setIdSub(terima.getKdBank());
					jurnal.setNamaPerkiraan(vNmPerk);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(NOL);
					jurnal.setKredit(terima.getKredit());
					jurnal.setBlc("*");
					jurnal.setTipe("K");
					listJurnal.add(jurnal);

				}
				String jenisJurnal = getJenisJurnalByParams(terima.getKet(),
						terima.getDebet(), terima.getKredit(),
						terima.getNoPerk());
				getMasterFacade().getTrioJurnalDao().saveTransaction(
						listJurnal, jenisJurnal, user);
				// case vket='2' AND vno_perk<>'P2' AND vno_perk<>'P3' AND vno_perk<>'P1'
				 
			} else if (terima.getKet().equalsIgnoreCase("2")
					&& (!terima.getNoPerk().equalsIgnoreCase("P2"))
					&& (!terima.getNoPerk().equalsIgnoreCase("P3"))
					&& (!terima.getNoPerk().equalsIgnoreCase("P1"))) {

				System.out.println("CASEIF 18");

				String vNoPerkSTNK = terima.getNoPerk();

				if (terima.getDebet().compareTo(NOL) == 1) {
					String vNoPerk = "11203";
					String vNmPerk = getMasterFacade().getTrioMstperkiraanDao()
							.findNamaById(vNoPerk);

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(terima.getDebet());
					jurnal.setKredit(NOL);
					jurnal.setBlc("*");
					jurnal.setTipe("D");
					listJurnal.add(jurnal);

					if (vNoPerkSTNK.equalsIgnoreCase("11405")) {
						vNoPerk = "11405";
					} else {
						vNoPerk = "21208";
					}

					vNmPerk = getMasterFacade().getTrioMstperkiraanDao()
							.findNamaById(vNmPerk);
					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(NOL);
					jurnal.setKredit(terima.getDebet());
					jurnal.setBlc("*");
					jurnal.setTipe("K");
					listJurnal.add(jurnal);

				} else { // else debet > 0
					String vNoPerk = null;
					if (vNoPerkSTNK.equalsIgnoreCase("11405")) {
						vNoPerk = "11405";
					} else {
						vNoPerk = "21208";
					}

					String vNmPerk = getMasterFacade().getTrioMstperkiraanDao()
							.findNamaById(vNoPerk);
					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(terima.getKredit());
					jurnal.setKredit(NOL);
					jurnal.setBlc("*");
					jurnal.setTipe("D");
					listJurnal.add(jurnal);

					vNoPerk = "11203";
					vNmPerk = getMasterFacade().getTrioMstperkiraanDao()
							.findNamaById(vNoPerk);
					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(NOL);
					jurnal.setKredit(terima.getKredit());
					jurnal.setBlc("*");
					jurnal.setTipe("K");
					listJurnal.add(jurnal);

				}
				String jenisJurnal = getJenisJurnalByParams(terima.getKet(),
						terima.getDebet(), terima.getKredit(),
						terima.getNoPerk());
				getMasterFacade().getTrioJurnalDao().saveTransaction(
						listJurnal, jenisJurnal, user);
				// case vket='3' AND vno_perk<>'P2' AND vno_perk<>'P3' AND vno_perk<>'P1' AND vitr<>'*'
			} else if (terima.getKet().equalsIgnoreCase("3")
					&& (!terima.getNoPerk().equalsIgnoreCase("P2"))
					&& (!terima.getNoPerk().equalsIgnoreCase("P3"))
					&& (!terima.getNoPerk().equalsIgnoreCase("P1"))
					&& (!terima.getItr().equalsIgnoreCase("*"))) {

				System.out.println("CASEIF 19");

				if (terima.getDebet().compareTo(NOL) == 1) {
					String vNoPerk = "11202";
					String vNmPerk = "";
					if (terima.getKdBank().length() > 0) {
						vNmPerk = getMasterFacade().getTrioMstsubperkiraanDao()
								.findNamaSubByIdAndIdSub(vNoPerk,
										terima.getKdBank());
					}
					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
					jurnal.getTrioJurnalPK().setIdSub(terima.getKdBank());
					jurnal.setNamaPerkiraan(vNmPerk);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(terima.getDebet());
					jurnal.setKredit(NOL);
					jurnal.setBlc("*");
					jurnal.setTipe("D");
					listJurnal.add(jurnal);

					vNoPerk = "21206";
					vNmPerk = getMasterFacade().getTrioMstperkiraanDao()
							.findNamaById(vNoPerk);

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(NOL);
					jurnal.setKredit(terima.getDebet());
					jurnal.setBlc("*");
					jurnal.setTipe("K");
					listJurnal.add(jurnal);

				} else { // else debet > 0

					String vNoPerk = "21206";
					String vNmPerk = getMasterFacade().getTrioMstperkiraanDao()
							.findNamaById(vNoPerk);

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(terima.getKredit());
					jurnal.setKredit(NOL);
					jurnal.setBlc("*");
					jurnal.setTipe("D");
					listJurnal.add(jurnal);

					vNoPerk = "11202";
					vNmPerk = "";
					if (terima.getKdBank().length() > 0) {
						vNmPerk = getMasterFacade().getTrioMstsubperkiraanDao()
								.findNamaSubByIdAndIdSub(vNoPerk,
										terima.getKdBank());
					} else {
						vNmPerk = "";
					}
					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
					jurnal.getTrioJurnalPK().setIdSub(terima.getKdBank());
					jurnal.setNamaPerkiraan(vNmPerk);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(NOL);
					jurnal.setKredit(terima.getKredit());
					jurnal.setBlc("*");
					jurnal.setTipe("K");
					listJurnal.add(jurnal);

				}
				String jenisJurnal = getJenisJurnalByParams(terima.getKet(),
						terima.getDebet(), terima.getKredit(),
						terima.getNoPerk());
				getMasterFacade().getTrioJurnalDao().saveTransaction(
						listJurnal, jenisJurnal, user);
				// case vno_perk='P3'
			} else if (terima.getNoPerk().equalsIgnoreCase("P3")) {

				System.out.println("CASEIF 20");

				if (terima.getDebet().compareTo(NOL) == 1) {
					if (terima.getKet().equalsIgnoreCase("1")
							|| terima.getKet().equalsIgnoreCase("K")) {
						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan("11101");
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan("KAS");
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(terima.getDebet());
						jurnal.setKredit(NOL);
						jurnal.setBlc("*");
						jurnal.setTipe("D");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);
					}

					if (terima.getKet().equalsIgnoreCase("2")) {
						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan("11203");
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan("CEK");
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(terima.getDebet());
						jurnal.setKredit(NOL);
						jurnal.setBlc("*");
						jurnal.setTipe("D");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);
					}

					if (terima.getKet().equalsIgnoreCase("3")) {
						String vNoSub = terima.getKdBank();
						String vNoPerk = "11202";
						String vNmPerk = "";
						if (vNoSub.length() > 0) {
							vNmPerk = getMasterFacade()
									.getTrioMstsubperkiraanDao()
									.findNamaSubByIdAndIdSub(vNoPerk, vNoSub);
						}

						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
						jurnal.getTrioJurnalPK().setIdSub(vNoSub);
						jurnal.setNamaPerkiraan(vNmPerk);
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(terima.getDebet());
						jurnal.setKredit(NOL);
						jurnal.setBlc("*");
						jurnal.setTipe("D");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);
					}

					if (terima.getKet().equalsIgnoreCase("4")) {
						String vNoSub = terima.getKdPOS();
						String vNoPerk = "11120";
						String vNmPerk = "";
						if (vNoSub.length() > 0) {
							vNmPerk = getMasterFacade()
									.getTrioMstsubperkiraanDao()
									.findNamaSubByIdAndIdSub(vNoPerk, vNoSub);
						}

						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
						jurnal.getTrioJurnalPK().setIdSub(vNoSub);
						jurnal.setNamaPerkiraan(vNmPerk);
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(terima.getDebet());
						jurnal.setKredit(NOL);
						jurnal.setBlc("*");
						jurnal.setTipe("D");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);
					}
					// if vharga>vdebet
					if (terima.getHarga().compareTo(terima.getDebet()) == 1) {
						String[] arrString = getNoPerkAndNmPerk(terima
								.getKet2());
						String vNoPerk0 = arrString[0];
						String vNmPerk0 = arrString[1];

						if (terima.getKet2().equalsIgnoreCase("FIF")
								|| terima.getKet2().equalsIgnoreCase("FIS")) {
							jurnal = new TrioJurnal();
							jurnal.setTglJurnal(terima.getTglTrm());
							jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk0);
							jurnal.getTrioJurnalPK().setIdSub("");
							jurnal.setNamaPerkiraan(vNmPerk0);
							jurnal.setKet(terima.getNmTran());
							// debet with vharga-vdebet-vs_finance
							BigDecimal vHarga = terima.getHarga()
									.subtract(terima.getDebet())
									.subtract(terima.getsFinance());
							jurnal.setDebet(vHarga);
							jurnal.setKredit(NOL);
							jurnal.setBlc("*");
							jurnal.setTipe("D");
							jurnal.setNoReff(terima.getTrioTerimaPK()
									.getNoFak());
							jurnal.setNoMesin(terima.getNoMesin());
							listJurnal.add(jurnal);
						} else {
							jurnal = new TrioJurnal();
							jurnal.setTglJurnal(terima.getTglTrm());
							jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk0);
							jurnal.getTrioJurnalPK().setIdSub("");
							jurnal.setNamaPerkiraan(vNmPerk0);
							jurnal.setKet(terima.getNmTran());
							// debet with vharga-vdebet
							BigDecimal vHarga = terima.getHarga().subtract(
									terima.getDebet());
							jurnal.setDebet(vHarga);
							jurnal.setKredit(NOL);
							jurnal.setBlc("*");
							jurnal.setTipe("D");
							jurnal.setNoReff(terima.getTrioTerimaPK()
									.getNoFak());
							jurnal.setNoMesin(terima.getNoMesin());
							listJurnal.add(jurnal);
						}

						if (terima.getsFinance().compareTo(NOL) == 1
								&& (terima.getKet2().equalsIgnoreCase("FIF") || terima
										.getKet2().equalsIgnoreCase("FIS"))) {
							jurnal = new TrioJurnal();
							jurnal.setTglJurnal(terima.getTglTrm());
							jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk0);
							jurnal.getTrioJurnalPK().setIdSub("");
							jurnal.setNamaPerkiraan(vNmPerk0 + " DP");
							jurnal.setKet(terima.getNmTran());
							jurnal.setDebet(terima.getsFinance());
							jurnal.setKredit(NOL);
							jurnal.setBlc("*");
							jurnal.setTipe("D");
							jurnal.setNoReff(terima.getTrioTerimaPK()
									.getNoFak());
							jurnal.setNoMesin(terima.getNoMesin());
							listJurnal.add(jurnal);
						}
					}//

					String vNoPerk1 = "41101";
					String vNoPerk2 = "21404";
					String vNoPerk3 = "21204";
					String vNoPerk4 = "21205";
					String vNoPerk5 = "21228";
					String vNoPerk6 = "41201";

					// vdpp=round(((vharga-vb_stnk-(vs_ahm+vs_md+vs_sd))*10/11),0)
					// vppnx=(vharga-vb_stnk-(vs_ahm+vs_md+vs_sd))-vdpp
					// vpot_penjualan=vs_ahm+vs_md+vs_sd

					BigDecimal totalSubsidi = terima.getsAHM()
							.add(terima.getsMD()).add(terima.getsSD());
					BigDecimal hitungDPP = terima.getHarga()
							.subtract(terima.getbSTNK()).subtract(totalSubsidi);
					BigDecimal vDPP = hitungDPP.multiply(new BigDecimal(10))
							.divide(new BigDecimal(11), 0,
									RoundingMode.HALF_EVEN);

					BigDecimal vPPNX = hitungDPP.subtract(vDPP);
					BigDecimal vPotPenjualan = totalSubsidi;

					String vNmPerk1 = getMasterFacade()
							.getTrioMstperkiraanDao().findNamaById(vNoPerk1);

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk1);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk1);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(NOL);
					jurnal.setKredit(vDPP);
					jurnal.setBlc("*");
					jurnal.setTipe("K");
					jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
					jurnal.setNoMesin(terima.getNoMesin());
					listJurnal.add(jurnal);

					String vNmPerk2 = getMasterFacade()
							.getTrioMstperkiraanDao().findNamaById(vNoPerk2);

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk2);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk2);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(NOL);
					jurnal.setKredit(vPPNX);
					jurnal.setBlc("*");
					jurnal.setTipe("K");
					jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
					jurnal.setNoMesin(terima.getNoMesin());
					listJurnal.add(jurnal);

					if (vPotPenjualan.compareTo(NOL) == 1) {

						String vNmPerk6 = getMasterFacade()
								.getTrioMstperkiraanDao()
								.findNamaById(vNoPerk6);

						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk6);
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan(vNmPerk6);
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(NOL);
						jurnal.setKredit(vPotPenjualan);
						jurnal.setBlc("*");
						jurnal.setTipe("K");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);
					}

					if (terima.getbSTNK().compareTo(NOL) == 1) {

						String vNmPerk3 = getMasterFacade()
								.getTrioMstperkiraanDao()
								.findNamaById(vNoPerk3);
						String vNmPerk4 = getMasterFacade()
								.getTrioMstperkiraanDao()
								.findNamaById(vNoPerk4);
						String vNmPerk5 = getMasterFacade()
								.getTrioMstperkiraanDao()
								.findNamaById(vNoPerk5);

						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk3);
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan(vNmPerk3);
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(NOL);
						jurnal.setKredit(terima.getpSTNK());
						jurnal.setBlc("*");
						jurnal.setTipe("K");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);

						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk4);
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan(vNmPerk4);
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(NOL);
						jurnal.setKredit(terima.getpBPKB());
						jurnal.setBlc("*");
						jurnal.setTipe("K");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);
						// vb_stnk-vp_stnk-vp_bpkb
						BigDecimal hitungBiaya = terima.getbSTNK()
								.subtract(terima.getpSTNK())
								.subtract(terima.getpBPKB());
						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk5);
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan(vNmPerk5);
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(NOL);
						jurnal.setKredit(hitungBiaya);
						jurnal.setBlc("*");
						jurnal.setTipe("K");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);

					}// sampa sini wawwaw
						// vtamb_md+vtamb_sd+vtamb_fin>0 OR vhadiah_l='*'

					BigDecimal tambahan = terima.getTambMD()
							.add(terima.getTambSD()).add(terima.getTambFIN());

					BigDecimal tambahan2 = tambahan.divide(
							new BigDecimal(0.97), 0, RoundingMode.HALF_EVEN);
					if (tambahan.compareTo(NOL) == 1
							|| terima.getHadiah().equalsIgnoreCase("*")) {
						if (tambahan.compareTo(NOL) == 1) {

							String vNoPerkk = "61106";
							String vNoSub = "04";
							String vNmSubPerkk = getMasterFacade()
									.getTrioMstsubperkiraanDao()
									.findNamaSubByIdAndIdSub(vNoPerkk, vNoSub);
							jurnal = new TrioJurnal();
							jurnal.setTglJurnal(terima.getTglTrm());
							jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerkk);
							jurnal.getTrioJurnalPK().setIdSub(vNoSub);
							jurnal.setNamaPerkiraan(vNmSubPerkk);
							jurnal.setKet(terima.getNmTran());
							jurnal.setDebet(tambahan2);
							jurnal.setKredit(NOL);
							jurnal.setBlc("*");
							jurnal.setTipe("D");
							jurnal.setNoReff(terima.getTrioTerimaPK()
									.getNoFak());
							listJurnal.add(jurnal);
						}

						if (terima.getTambFIN().compareTo(NOL) == 1) {

							String[] arrStr = getNoPerkAndNmPerk(terima
									.getKet2());
							String vNoPerk0 = arrStr[0];
							String vNmPerk0 = arrStr[1];
							if (terima.getKet2().equalsIgnoreCase("ADR")
									|| terima.getKet2().equalsIgnoreCase("ADS")
									|| terima.getKet2().equalsIgnoreCase("OTO")
									|| terima.getKet2().equalsIgnoreCase("WOM")) {

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(
										vNoPerk0);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk0);
								jurnal.setKet(terima.getNmTran());
								BigDecimal debetTemp = NOL;
								if (terima.getKet2().equalsIgnoreCase("ADR")
										|| terima.getKet2().equalsIgnoreCase(
												"ADS")) {
									// vtamb_fin*11/10
									debetTemp = terima
											.getTambFIN()
											.multiply(new BigDecimal(11))
											.divide(new BigDecimal(10), 0,
													RoundingMode.HALF_EVEN);
								} else {
									debetTemp = terima.getTambFIN();
								}
								jurnal.setDebet(debetTemp);
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								jurnal.setNoReff(terima.getTrioTerimaPK()
										.getNoFak());
								listJurnal.add(jurnal);
								String vNoSub71104 = "";
								if (terima.getKet2().equalsIgnoreCase("ADR")) {
									vNoSub71104 = "01";
								} else if (terima.getKet2().equalsIgnoreCase(
										"ADS")) {
									vNoSub71104 = "02";
								} else if (terima.getKet2().equalsIgnoreCase(
										"OTO")) {
									vNoSub71104 = "03";
								} else if (terima.getKet2().equalsIgnoreCase(
										"WOM")) {
									vNoSub71104 = "04";
								}
								String vNmSub = getMasterFacade()
										.getTrioMstsubperkiraanDao()
										.findNamaSubByIdAndIdSub("71104",
												vNoSub71104);
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK()
										.setIdPerkiraan("71104");
								jurnal.getTrioJurnalPK().setIdSub(vNoSub71104);
								jurnal.setNamaPerkiraan(vNmSub);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								BigDecimal tempKredit = NOL;
								if (terima.getKet2().equalsIgnoreCase("ADR")
										|| terima.getKet2().equalsIgnoreCase(
												"ADS")) {
									tempKredit = terima.getTambFIN();
								} else {
									// ROUND(vtamb_fin*10/11,0)
									tempKredit = terima
											.getTambFIN()
											.multiply(new BigDecimal(10))
											.divide(new BigDecimal(11), 0,
													RoundingMode.HALF_EVEN);
								}
								jurnal.setKredit(tempKredit);
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								listJurnal.add(jurnal);

								String vNmPerk = getMasterFacade()
										.getTrioMstperkiraanDao().findNamaById(
												"21404");
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK()
										.setIdPerkiraan("21404");
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								tempKredit = NOL;
								if (terima.getKet2().equalsIgnoreCase("ADR")
										|| terima.getKet2().equalsIgnoreCase(
												"ADS")) {
									// ROUND((vtamb_fin)*0.1,0)
									tempKredit = terima
											.getTambFIN()
											.multiply(new BigDecimal(0.1))
											.setScale(0, RoundingMode.HALF_EVEN);
								} else {
									// ROUND((vtamb_fin*10/11)*0.1,0)
									tempKredit = terima
											.getTambFIN()
											.multiply(new BigDecimal(10))
											.divide(new BigDecimal(11), 0,
													RoundingMode.HALF_EVEN)
											.multiply(new BigDecimal(0.1))
											.setScale(0, RoundingMode.HALF_EVEN);
								}
								jurnal.setKredit(tempKredit);
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								listJurnal.add(jurnal);

							}// IF vket2<>'ADR' AND vket2<>'ADS' AND
								// vket2<>'OTO' AND vket2<>'WOM' AND
								// vhadiah_l='*'
							if ((!terima.getKet2().equalsIgnoreCase("ADR"))
									&& (!terima.getKet2().equalsIgnoreCase(
											"ADS"))
									&& (!terima.getKet2().equalsIgnoreCase(
											"OTO")
											&& (!terima.getKet2()
													.equalsIgnoreCase("WOM")) && terima
											.getHadiah().equalsIgnoreCase("*"))) {

								BigDecimal tambFin = terima.getTambFIN().add(
										terima.getsFinance());
								// IF vtamb_fin+vs_finance>0
								if (tambFin.compareTo(NOL) == 1) {
									String[] arrStr0 = getNoPerkAndNmPerk(terima
											.getKet2());
									String vNoPerk = arrStr0[0];
									String vNmPerk = arrStr0[1];

									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(
											vNoPerk);
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan(vNmPerk);
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(tambFin);
									jurnal.setKredit(NOL);
									jurnal.setBlc("*");
									jurnal.setTipe("D");
									jurnal.setNoReff(terima.getTrioTerimaPK()
											.getNoFak());
									listJurnal.add(jurnal);

									String vNoPerkk = "61106";
									String vNoSubk = "04";

									String vNmPerkk = getMasterFacade()
											.getTrioMstsubperkiraanDao()
											.findNamaSubByIdAndIdSub(vNoPerkk,
													vNoSubk);
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(
											vNoPerkk);
									jurnal.getTrioJurnalPK().setIdSub(vNoSubk);
									jurnal.setNamaPerkiraan(vNmPerk);
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(NOL);
									jurnal.setKredit(tambFin);
									jurnal.setBlc("*");
									jurnal.setTipe("K");
									jurnal.setNoReff(terima.getTrioTerimaPK()
											.getNoFak());
									listJurnal.add(jurnal);

								}// IF vtamb_md+vtamb_sd+vtamb_fin>0
								BigDecimal tambTriple = terima.getTambMD()
										.add(terima.getTambSD())
										.add(terima.getTambFIN());
								if (tambTriple.compareTo(NOL) == 1) {
									String vNoPerkk = "21309";
									String vNmPerkk = getMasterFacade()
											.getTrioMstperkiraanDao()
											.findNamaById(vNoPerkk);

									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(
											vNoPerkk);
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan(vNmPerkk);
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(NOL);
									jurnal.setKredit(tambTriple);
									jurnal.setBlc("*");
									jurnal.setTipe("K");
									jurnal.setNoReff(terima.getTrioTerimaPK()
											.getNoFak());
									listJurnal.add(jurnal);

									vNoPerkk = "21305";
									vNmPerkk = getMasterFacade()
											.getTrioMstperkiraanDao()
											.findNamaById(vNoPerkk);

									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(
											vNoPerkk);
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan(vNmPerkk);
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(NOL);
									// ROUND(ROUND((vtamb_md+vtamb_sd+vtamb_fin)/0.97,0)*0.03,0)
									BigDecimal credit = tambTriple
											.divide(new BigDecimal(0.97), 0,
													RoundingMode.HALF_EVEN)
											.multiply(new BigDecimal(0.03))
											.setScale(0, RoundingMode.HALF_EVEN);
									jurnal.setKredit(credit);
									jurnal.setBlc("*");
									jurnal.setTipe("K");
									jurnal.setNoReff(terima.getTrioTerimaPK()
											.getNoFak());
									listJurnal.add(jurnal);

								}
							}//
							String[] arrayPerk = getNoPerkAndNmPerk(terima
									.getKet2());
							String vNoPerk = arrayPerk[0];
							String vNmPerk = arrayPerk[1];

							List<TrioTerima> listTerima2 = findByNoLKHAndNoFakAndKet("KUPON", terima.getTrioTerimaPK().getNoFak(),"X");

							for (TrioTerima trm : listTerima2) {
								if (terima.getKet2().equalsIgnoreCase("ADR")
										|| terima.getKet2().equalsIgnoreCase(
												"ADS")
										|| terima.getKet2().equalsIgnoreCase(
												"OTO")
										|| terima.getKet2().equalsIgnoreCase(
												"WOM")) {
									if (terima.getKet2()
											.equalsIgnoreCase("OTO")) {
										jurnal = new TrioJurnal();
										jurnal.setTglJurnal(terima.getTglTrm());
										jurnal.getTrioJurnalPK()
												.setIdPerkiraan(vNoPerk0);
										jurnal.getTrioJurnalPK().setIdSub("");
										jurnal.setNamaPerkiraan(vNmPerk0);
										jurnal.setKet(trm.getNmTran() + " A.N "
												+ terima.getNmCust());
										// debet with vqcari1.debet*(10/11)
										BigDecimal dbt = trm
												.getDebet()
												.multiply(
														new BigDecimal(10)
																.divide(new BigDecimal(
																		11),
																		0,
																		RoundingMode.HALF_EVEN));
										jurnal.setDebet(dbt);
										jurnal.setKredit(NOL);
										jurnal.setBlc("*");
										jurnal.setTipe("D");
										jurnal.setNoReff(terima
												.getTrioTerimaPK().getNoFak());
										listJurnal.add(jurnal);

										jurnal = new TrioJurnal();
										jurnal.setTglJurnal(terima.getTglTrm());
										jurnal.getTrioJurnalPK()
												.setIdPerkiraan("71104");
										jurnal.getTrioJurnalPK().setIdSub("03");
										jurnal.setNamaPerkiraan("JASA PERANTARA OTO");
										jurnal.setKet(trm.getNmTran() + " A.N "
												+ terima.getNmCust());
										// debet with
										// vqcari1.debet-(vqcari1.debet*(10/11))
										dbt = trm.getDebet().subtract(dbt);
										jurnal.setDebet(dbt);
										jurnal.setKredit(NOL);
										jurnal.setBlc("*");
										jurnal.setTipe("D");
										jurnal.setNoReff(terima
												.getTrioTerimaPK().getNoFak());
										listJurnal.add(jurnal);
									} else {
										jurnal = new TrioJurnal();
										jurnal.setTglJurnal(terima.getTglTrm());
										jurnal.getTrioJurnalPK()
												.setIdPerkiraan(vNoPerk0);
										jurnal.getTrioJurnalPK().setIdSub("");
										jurnal.setNamaPerkiraan(vNmPerk);
										jurnal.setKet(trm.getNmTran() + " A.N "
												+ terima.getNmCust());
										jurnal.setDebet(trm.getDebet());
										jurnal.setKredit(NOL);
										jurnal.setBlc("*");
										jurnal.setTipe("D");
										jurnal.setNoReff(terima
												.getTrioTerimaPK().getNoFak());
										listJurnal.add(jurnal);
									}
								} else {
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(
											vNoPerk0);
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan(vNmPerk);
									jurnal.setKet(trm.getNmTran() + " A.N "
											+ terima.getNmCust());
									jurnal.setDebet(trm.getDebet());
									jurnal.setKredit(NOL);
									jurnal.setBlc("*");
									jurnal.setTipe("D");
									jurnal.setNoReff(terima.getTrioTerimaPK()
											.getNoFak());
									listJurnal.add(jurnal);
								}
								// sampai sini brorrrrr
								// IF vqcari1.no_perk='21116' AND
								// vqcari1.no_sub='01'
								if (trm.getNoPerk().equalsIgnoreCase("21116")
										&& trm.getNoSub()
												.equalsIgnoreCase("01")) {
									if (terima.getKet2()
											.equalsIgnoreCase("ADR")
											|| terima.getKet2()
													.equalsIgnoreCase("ADS")
											|| terima.getKet2()
													.equalsIgnoreCase("OTO")
											|| terima.getKet2()
													.equalsIgnoreCase("WOM")) {
										String vNoPerkk = "71105";
										String vNoSubk = trm.getNoSub();
										String vNmPerkk = "";
										if (vNoSubk.length() > 0) {
											vNmPerkk = getMasterFacade()
													.getTrioMstsubperkiraanDao()
													.findNamaSubByIdAndIdSub(
															vNoPerkk, vNoSubk);
										} else {
											vNmPerkk = getMasterFacade()
													.getTrioMstperkiraanDao()
													.findNamaById(vNoPerkk);
										}

										jurnal = new TrioJurnal();
										jurnal.setTglJurnal(terima.getTglTrm());
										jurnal.getTrioJurnalPK()
												.setIdPerkiraan(vNoPerkk);
										jurnal.getTrioJurnalPK().setIdSub(
												vNoSubk);
										jurnal.setNamaPerkiraan(vNmPerkk);
										jurnal.setKet(trm.getNmTran() + " A.N "
												+ terima.getNmCust());
										jurnal.setDebet(NOL);
										BigDecimal kredit = trm
												.getDebet()
												.multiply(
														new BigDecimal(10)
																.divide(new BigDecimal(
																		11),
																		0,
																		RoundingMode.HALF_EVEN));
										jurnal.setKredit(kredit);
										jurnal.setBlc("*");
										jurnal.setTipe("K");
										jurnal.setNoReff(terima
												.getTrioTerimaPK().getNoFak());
										listJurnal.add(jurnal);

										vNoPerkk = "21404";
										vNmPerkk = getMasterFacade()
												.getTrioMstperkiraanDao()
												.findNamaById(vNoPerkk);
										if (vNoPerk.length() < 1) {
											vNmPerkk = "";
										}

										jurnal = new TrioJurnal();
										jurnal.setTglJurnal(terima.getTglTrm());
										jurnal.getTrioJurnalPK()
												.setIdPerkiraan(vNoPerkk);
										jurnal.getTrioJurnalPK().setIdSub(
												vNoSubk);
										jurnal.setNamaPerkiraan(vNmPerkk);
										jurnal.setKet(trm.getNmTran() + " A.N "
												+ terima.getNmCust());
										jurnal.setDebet(NOL);
										// kredit with
										// ROUND((vqcari1.debet*(10/11))*0.1,0)
										kredit = trm
												.getDebet()
												.multiply(
														new BigDecimal(10)
																.divide(new BigDecimal(
																		11),
																		0,
																		RoundingMode.HALF_EVEN))
												.multiply(new BigDecimal(0.1))
												.setScale(0,
														RoundingMode.HALF_EVEN);
										jurnal.setKredit(kredit);
										jurnal.setBlc("*");
										jurnal.setTipe("K");
										jurnal.setNoReff(terima
												.getTrioTerimaPK().getNoFak());
										listJurnal.add(jurnal);

									} else {
										String vNoPerkk = "61104";
										String vNoSubk = "06";
										String vNmPerkk = getMasterFacade()
												.getTrioMstsubperkiraanDao()
												.findNamaSubByIdAndIdSub(
														vNoPerkk, vNoSubk);
										if (vNmPerkk.length() < 1) {
											vNmPerkk = "";
										}
										jurnal = new TrioJurnal();
										jurnal.setTglJurnal(terima.getTglTrm());
										jurnal.getTrioJurnalPK()
												.setIdPerkiraan(vNoPerkk);
										jurnal.getTrioJurnalPK().setIdSub(
												vNoSubk);
										jurnal.setNamaPerkiraan(vNmPerkk);
										jurnal.setKet(trm.getNmTran() + " A.N "
												+ terima.getNmCust());
										jurnal.setDebet(NOL);
										jurnal.setKredit(trm.getDebet());
										jurnal.setBlc("*");
										jurnal.setTipe("K");
										jurnal.setNoReff(terima
												.getTrioTerimaPK().getNoFak());
										listJurnal.add(jurnal);

									}
								} else { // kurang disini ternyata
									// IF (vket2='ADR' OR vket2='ADS' OR
									// vket2='OTO' OR vket2='WOM')
									if (terima.getKet2()
											.equalsIgnoreCase("ADR")
											|| terima.getKet2()
													.equalsIgnoreCase("ADS")
											|| terima.getKet2()
													.equalsIgnoreCase("OTO")
											|| terima.getKet2()
													.equalsIgnoreCase("WOM")) {
										String vNoPerkk = "71105";
										String vNoSubk = trm.getNoSub();
										String vNmPerkk = "";
										if (vNoSubk.length() > 0) {
											vNmPerkk = getMasterFacade()
													.getTrioMstsubperkiraanDao()
													.findNamaSubByIdAndIdSub(
															vNoPerkk, vNoSubk);
										} else {
											vNmPerkk = getMasterFacade()
													.getTrioMstperkiraanDao()
													.findNamaById(vNoPerkk);
										}
										jurnal = new TrioJurnal();
										jurnal.setTglJurnal(terima.getTglTrm());
										jurnal.getTrioJurnalPK()
												.setIdPerkiraan(vNoPerkk);
										jurnal.getTrioJurnalPK().setIdSub(
												vNoSubk);
										jurnal.setNamaPerkiraan(vNmPerkk);
										jurnal.setKet(trm.getNmTran() + " A.N "
												+ terima.getNmCust());
										jurnal.setDebet(NOL);
										BigDecimal temp = trm
												.getDebet()
												.multiply(
														new BigDecimal(10)
																.divide(new BigDecimal(
																		11),
																		0,
																		RoundingMode.HALF_EVEN));
										jurnal.setKredit(temp);
										jurnal.setBlc("*");
										jurnal.setTipe("K");
										jurnal.setNoReff(terima
												.getTrioTerimaPK().getNoFak());
										listJurnal.add(jurnal);

										vNoPerkk = "21404";
										vNmPerkk = getMasterFacade()
												.getTrioMstperkiraanDao()
												.findNamaById(vNoPerkk);
										if (vNmPerkk.length() < 1) {
											vNmPerkk = "";
										}
										jurnal = new TrioJurnal();
										jurnal.setTglJurnal(terima.getTglTrm());
										jurnal.getTrioJurnalPK()
												.setIdPerkiraan(vNoPerkk);
										jurnal.getTrioJurnalPK().setIdSub("");
										jurnal.setNamaPerkiraan(vNmPerkk);
										jurnal.setKet(trm.getNmTran() + " A.N "
												+ terima.getNmCust());
										jurnal.setDebet(NOL);
										// kredit with
										// ROUND((vqcari1.debet*(10/11))*0.1,0)
										temp = trm
												.getDebet()
												.multiply(
														new BigDecimal(10)
																.divide(new BigDecimal(
																		11),
																		0,
																		RoundingMode.HALF_EVEN));
										temp = temp.multiply(
												new BigDecimal(0.1)).setScale(
												0, RoundingMode.HALF_EVEN);
										jurnal.setKredit(temp);
										jurnal.setBlc("*");
										jurnal.setTipe("K");
										jurnal.setNoReff(terima
												.getTrioTerimaPK().getNoFak());
										listJurnal.add(jurnal);
									} else {
										String vNoPerkk = trm.getNoPerk();
										String vNoSubk = trm.getNoSub();
										String vNmPerkk = "";
										if (vNoSubk.length() > 0) {
											vNmPerkk = getMasterFacade()
													.getTrioMstsubperkiraanDao()
													.findNamaSubByIdAndIdSub(
															vNoPerkk, vNoSubk);
										} else {
											vNmPerkk = getMasterFacade()
													.getTrioMstperkiraanDao()
													.findNamaById(vNoPerkk);
										}
										jurnal = new TrioJurnal();
										jurnal.setTglJurnal(terima.getTglTrm());
										jurnal.getTrioJurnalPK()
												.setIdPerkiraan(vNoPerkk);
										jurnal.getTrioJurnalPK().setIdSub(
												vNoSubk);
										jurnal.setNamaPerkiraan(vNmPerkk);
										jurnal.setKet(trm.getNmTran() + " A.N "
												+ terima.getNmCust());
										jurnal.setDebet(NOL);
										jurnal.setKredit(trm.getDebet());
										jurnal.setBlc("*");
										jurnal.setTipe("K");
										jurnal.setNoReff(terima
												.getTrioTerimaPK().getNoFak());
										listJurnal.add(jurnal);

									}
								}

							}
						}
					}

				} else { // else debet > 0
					// TODO COPY di atas

					if (terima.getKet().equalsIgnoreCase("1")
							|| terima.getKet().equalsIgnoreCase("K")) {
						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan("11101");
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan("KAS");
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(NOL);
						jurnal.setKredit(terima.getKredit());
						jurnal.setBlc("*");
						jurnal.setTipe("K");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);
					}

					if (terima.getKet().equalsIgnoreCase("2")) {
						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan("11203");
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan("CEK");
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(NOL);
						jurnal.setKredit(terima.getKredit());
						jurnal.setBlc("*");
						jurnal.setTipe("K");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);
					}

					if (terima.getKet().equalsIgnoreCase("3")) {
						String vNoSub = terima.getKdBank();
						String vNoPerk = "11202";
						String vNmPerk = "";
						if (vNoSub.length() > 0) {
							vNmPerk = getMasterFacade()
									.getTrioMstsubperkiraanDao()
									.findNamaSubByIdAndIdSub(vNoPerk, vNoSub);
						}

						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
						jurnal.getTrioJurnalPK().setIdSub(vNoSub);
						jurnal.setNamaPerkiraan(vNmPerk);
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(NOL);
						jurnal.setKredit(terima.getKredit());
						jurnal.setBlc("*");
						jurnal.setTipe("K");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);
					}

					if (terima.getKet().equalsIgnoreCase("4")) {
						String vNoSub = terima.getKdPOS();
						String vNoPerk = "11120";
						String vNmPerk = "";
						if (vNoSub.length() > 0) {
							vNmPerk = getMasterFacade()
									.getTrioMstsubperkiraanDao()
									.findNamaSubByIdAndIdSub(vNoPerk, vNoSub);
						}

						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
						jurnal.getTrioJurnalPK().setIdSub(vNoSub);
						jurnal.setNamaPerkiraan(vNmPerk);
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(NOL);
						jurnal.setKredit(terima.getKredit());
						jurnal.setBlc("*");
						jurnal.setTipe("K");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);
					}
					// if vharga>vdebet
					if (terima.getHarga().compareTo(terima.getDebet()) == 1) {
						String[] arrString = getNoPerkAndNmPerk(terima
								.getKet2());
						String vNoPerk0 = arrString[0];
						String vNmPerk0 = arrString[1];

						if (terima.getKet2().equalsIgnoreCase("FIF")
								|| terima.getKet2().equalsIgnoreCase("FIS")) {
							jurnal = new TrioJurnal();
							jurnal.setTglJurnal(terima.getTglTrm());
							jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk0);
							jurnal.getTrioJurnalPK().setIdSub("");
							jurnal.setNamaPerkiraan(vNmPerk0);
							jurnal.setKet(terima.getNmTran());
							// debet with vharga-vdebet-vs_finance
							BigDecimal vHarga = terima.getHarga()
									.subtract(terima.getKredit())
									.subtract(terima.getsFinance());
							jurnal.setDebet(NOL);
							jurnal.setKredit(vHarga);
							jurnal.setBlc("*");
							jurnal.setTipe("K");
							jurnal.setNoReff(terima.getTrioTerimaPK()
									.getNoFak());
							jurnal.setNoMesin(terima.getNoMesin());
							listJurnal.add(jurnal);
						} else {
							jurnal = new TrioJurnal();
							jurnal.setTglJurnal(terima.getTglTrm());
							jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk0);
							jurnal.getTrioJurnalPK().setIdSub("");
							jurnal.setNamaPerkiraan(vNmPerk0);
							jurnal.setKet(terima.getNmTran());
							// debet with vharga-vdebet
							BigDecimal vHarga = terima.getHarga().subtract(
									terima.getKredit());
							jurnal.setDebet(NOL);
							jurnal.setKredit(vHarga);
							jurnal.setBlc("*");
							jurnal.setTipe("K");
							jurnal.setNoReff(terima.getTrioTerimaPK()
									.getNoFak());
							jurnal.setNoMesin(terima.getNoMesin());
							listJurnal.add(jurnal);
						}

						if (terima.getsFinance().compareTo(NOL) == 1
								&& (terima.getKet2().equalsIgnoreCase("FIF") || terima
										.getKet2().equalsIgnoreCase("FIS"))) {
							jurnal = new TrioJurnal();
							jurnal.setTglJurnal(terima.getTglTrm());
							jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk0);
							jurnal.getTrioJurnalPK().setIdSub("");
							jurnal.setNamaPerkiraan(vNmPerk0 + " DP");
							jurnal.setKet(terima.getNmTran());
							jurnal.setDebet(NOL);
							jurnal.setKredit(terima.getsFinance());
							jurnal.setBlc("*");
							jurnal.setTipe("K");
							jurnal.setNoReff(terima.getTrioTerimaPK()
									.getNoFak());
							jurnal.setNoMesin(terima.getNoMesin());
							listJurnal.add(jurnal);
						}
					}//

					String vNoPerk1 = "41101";
					String vNoPerk2 = "21404";
					String vNoPerk3 = "21204";
					String vNoPerk4 = "21205";
					String vNoPerk5 = "21228";
					String vNoPerk6 = "41201";

					// vdpp=round(((vharga-vb_stnk-(vs_ahm+vs_md+vs_sd))*10/11),0)
					// vppnx=(vharga-vb_stnk-(vs_ahm+vs_md+vs_sd))-vdpp
					// vpot_penjualan=vs_ahm+vs_md+vs_sd

					BigDecimal totalSubsidi = terima.getsAHM()
							.add(terima.getsMD()).add(terima.getsSD());
					BigDecimal hitungDPP = terima.getHarga()
							.subtract(terima.getbSTNK()).subtract(totalSubsidi);
					BigDecimal vDPP = hitungDPP.multiply(new BigDecimal(10))
							.divide(new BigDecimal(11), 0,
									RoundingMode.HALF_EVEN);

					BigDecimal vPPNX = hitungDPP.subtract(vDPP);
					BigDecimal vPotPenjualan = totalSubsidi;

					String vNmPerk1 = getMasterFacade()
							.getTrioMstperkiraanDao().findNamaById(vNoPerk1);

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk1);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk1);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(vDPP);
					jurnal.setKredit(NOL);
					jurnal.setBlc("*");
					jurnal.setTipe("D");
					jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
					jurnal.setNoMesin(terima.getNoMesin());
					listJurnal.add(jurnal);

					String vNmPerk2 = getMasterFacade()
							.getTrioMstperkiraanDao().findNamaById(vNoPerk2);

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk2);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk2);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(vPPNX);
					jurnal.setKredit(NOL);
					jurnal.setBlc("*");
					jurnal.setTipe("D");
					jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
					jurnal.setNoMesin(terima.getNoMesin());
					listJurnal.add(jurnal);

					if (vPotPenjualan.compareTo(NOL) == 1) {

						String vNmPerk6 = getMasterFacade()
								.getTrioMstperkiraanDao()
								.findNamaById(vNoPerk6);

						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk6);
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan(vNmPerk6);
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(vPotPenjualan);
						jurnal.setKredit(NOL);
						jurnal.setBlc("*");
						jurnal.setTipe("D");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);
					}

					if (terima.getbSTNK().compareTo(NOL) == 1) {

						String vNmPerk3 = getMasterFacade()
								.getTrioMstperkiraanDao()
								.findNamaById(vNoPerk3);
						String vNmPerk4 = getMasterFacade()
								.getTrioMstperkiraanDao()
								.findNamaById(vNoPerk4);
						String vNmPerk5 = getMasterFacade()
								.getTrioMstperkiraanDao()
								.findNamaById(vNoPerk5);

						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk3);
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan(vNmPerk3);
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(terima.getpSTNK());
						jurnal.setKredit(NOL);
						jurnal.setBlc("*");
						jurnal.setTipe("D");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);

						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk4);
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan(vNmPerk4);
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(terima.getpBPKB());
						jurnal.setKredit(NOL);
						jurnal.setBlc("*");
						jurnal.setTipe("D");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);
						// vb_stnk-vp_stnk-vp_bpkb
						BigDecimal hitungBiaya = terima.getbSTNK()
								.subtract(terima.getpSTNK())
								.subtract(terima.getpBPKB());
						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk5);
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan(vNmPerk5);
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(hitungBiaya);
						jurnal.setKredit(NOL);
						jurnal.setBlc("*");
						jurnal.setTipe("D");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);

					}// sampa sini wawwaw
						// vtamb_md+vtamb_sd+vtamb_fin>0 OR vhadiah_l='*'

					BigDecimal tambahan = terima.getTambMD()
							.add(terima.getTambSD()).add(terima.getTambFIN());

					BigDecimal tambahan2 = tambahan.divide(
							new BigDecimal(0.97), 0, RoundingMode.HALF_EVEN);
					if (tambahan.compareTo(NOL) == 1
							|| terima.getHadiah().equalsIgnoreCase("*")) {
						if (tambahan.compareTo(NOL) == 1) {

							String vNoPerkk = "61106";
							String vNoSub = "04";
							String vNmSubPerkk = getMasterFacade()
									.getTrioMstsubperkiraanDao()
									.findNamaSubByIdAndIdSub(vNoPerkk, vNoSub);
							jurnal = new TrioJurnal();
							jurnal.setTglJurnal(terima.getTglTrm());
							jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerkk);
							jurnal.getTrioJurnalPK().setIdSub(vNoSub);
							jurnal.setNamaPerkiraan(vNmSubPerkk);
							jurnal.setKet(terima.getNmTran());
							jurnal.setDebet(NOL);
							jurnal.setKredit(tambahan2);
							jurnal.setBlc("*");
							jurnal.setTipe("K");
							jurnal.setNoReff(terima.getTrioTerimaPK()
									.getNoFak());
							listJurnal.add(jurnal);
						}

						if (terima.getTambFIN().compareTo(NOL) == 1) {

							String[] arrStr = getNoPerkAndNmPerk(terima
									.getKet2());
							String vNoPerk0 = arrStr[0];
							String vNmPerk0 = arrStr[1];
							if (terima.getKet2().equalsIgnoreCase("ADR")
									|| terima.getKet2().equalsIgnoreCase("ADS")
									|| terima.getKet2().equalsIgnoreCase("OTO")) {

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(
										vNoPerk0);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk0);
								jurnal.setKet(terima.getNmTran());
								BigDecimal kreditTemp = NOL;
								if (terima.getKet2().equalsIgnoreCase("ADR")
										|| terima.getKet2().equalsIgnoreCase(
												"ADS")) {
									// vtamb_fin*11/10
									kreditTemp = terima
											.getTambFIN()
											.multiply(new BigDecimal(11))
											.divide(new BigDecimal(10), 0,
													RoundingMode.HALF_EVEN);
								} else {
									kreditTemp = terima.getTambFIN();
								}
								jurnal.setDebet(NOL);
								jurnal.setKredit(kreditTemp);
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								jurnal.setNoReff(terima.getTrioTerimaPK()
										.getNoFak());
								listJurnal.add(jurnal);
								String vNoSub71104 = "";
								if (terima.getKet2().equalsIgnoreCase("ADR")) {
									vNoSub71104 = "01";
								} else if (terima.getKet2().equalsIgnoreCase(
										"ADS")) {
									vNoSub71104 = "02";
								} else if (terima.getKet2().equalsIgnoreCase(
										"OTO")) {
									vNoSub71104 = "03";
								} else if (terima.getKet2().equalsIgnoreCase(
										"WOM")) {
									vNoSub71104 = "04";
								}
								String vNmSub = getMasterFacade()
										.getTrioMstsubperkiraanDao()
										.findNamaSubByIdAndIdSub("71104",
												vNoSub71104);
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK()
										.setIdPerkiraan("71104");
								jurnal.getTrioJurnalPK().setIdSub(vNoSub71104);
								jurnal.setNamaPerkiraan(vNmSub);
								jurnal.setKet(terima.getNmTran());

								BigDecimal tempDebet = NOL;
								if (terima.getKet2().equalsIgnoreCase("ADR")
										|| terima.getKet2().equalsIgnoreCase(
												"ADS")) {
									tempDebet = terima.getTambFIN();
								} else {
									// ROUND(vtamb_fin*10/11,0)
									tempDebet = terima
											.getTambFIN()
											.multiply(new BigDecimal(10))
											.divide(new BigDecimal(11), 0,
													RoundingMode.HALF_EVEN);
								}
								jurnal.setDebet(tempDebet);
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								listJurnal.add(jurnal);

								String vNmPerk = getMasterFacade()
										.getTrioMstperkiraanDao().findNamaById(
												"21404");
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK()
										.setIdPerkiraan("21404");
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk);
								jurnal.setKet(terima.getNmTran());

								tempDebet = NOL;
								if (terima.getKet2().equalsIgnoreCase("ADR")
										|| terima.getKet2().equalsIgnoreCase(
												"ADS")) {
									// ROUND((vtamb_fin)*0.1,0)
									tempDebet = terima
											.getTambFIN()
											.multiply(new BigDecimal(0.1))
											.setScale(0, RoundingMode.HALF_EVEN);
								} else {
									// ROUND((vtamb_fin*10/11)*0.1,0)
									tempDebet = terima
											.getTambFIN()
											.multiply(new BigDecimal(10))
											.divide(new BigDecimal(11), 0,
													RoundingMode.HALF_EVEN)
											.multiply(new BigDecimal(0.1))
											.setScale(0, RoundingMode.HALF_EVEN);
								}
								jurnal.setDebet(tempDebet);
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								listJurnal.add(jurnal);

							}// IF vket2<>'ADR' AND vket2<>'ADS' AND
								// vket2<>'OTO' AND vket2<>'WOM' AND
								// vhadiah_l='*'
							if ((!terima.getKet2().equalsIgnoreCase("ADR"))
									&& (!terima.getKet2().equalsIgnoreCase(
											"ADS"))
									&& (!terima.getKet2().equalsIgnoreCase(
											"OTO")
											&& (!terima.getKet2()
													.equalsIgnoreCase("WOM")) && terima
											.getHadiah().equalsIgnoreCase("*"))) {

								BigDecimal tambFin = terima.getTambFIN().add(
										terima.getsFinance());
								// IF vtamb_fin+vs_finance>0
								if (tambFin.compareTo(NOL) == 1) {
									String[] arrStr0 = getNoPerkAndNmPerk(terima
											.getKet2());
									String vNoPerk = arrStr0[0];
									String vNmPerk = arrStr0[1];

									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(
											vNoPerk);
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan(vNmPerk);
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(NOL);
									jurnal.setKredit(tambFin);
									jurnal.setBlc("*");
									jurnal.setTipe("K");
									jurnal.setNoReff(terima.getTrioTerimaPK()
											.getNoFak());
									listJurnal.add(jurnal);

									String vNoPerkk = "61106";
									String vNoSubk = "04";

									String vNmPerkk = getMasterFacade()
											.getTrioMstsubperkiraanDao()
											.findNamaSubByIdAndIdSub(vNoPerkk,
													vNoSubk);
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(
											vNoPerkk);
									jurnal.getTrioJurnalPK().setIdSub(vNoSubk);
									jurnal.setNamaPerkiraan(vNmPerk);
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(tambFin);
									jurnal.setKredit(NOL);
									jurnal.setBlc("*");
									jurnal.setTipe("D");
									jurnal.setNoReff(terima.getTrioTerimaPK()
											.getNoFak());
									listJurnal.add(jurnal);

								}// IF vtamb_md+vtamb_sd+vtamb_fin>0
								BigDecimal tambTriple = terima.getTambMD()
										.add(terima.getTambSD())
										.add(terima.getTambFIN());
								if (tambTriple.compareTo(NOL) == 1) {
									String vNoPerkk = "21309";
									String vNmPerkk = getMasterFacade()
											.getTrioMstperkiraanDao()
											.findNamaById(vNoPerkk);

									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(
											vNoPerkk);
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan(vNmPerkk);
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(tambTriple);
									jurnal.setKredit(NOL);
									jurnal.setBlc("*");
									jurnal.setTipe("D");
									jurnal.setNoReff(terima.getTrioTerimaPK()
											.getNoFak());
									listJurnal.add(jurnal);

									vNoPerkk = "21305";
									vNmPerkk = getMasterFacade()
											.getTrioMstperkiraanDao()
											.findNamaById(vNoPerkk);

									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(
											vNoPerkk);
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan(vNmPerkk);
									jurnal.setKet(terima.getNmTran());
									// ROUND(ROUND((vtamb_md+vtamb_sd+vtamb_fin)/0.97,0)*0.03,0)
									BigDecimal debet = tambTriple
											.divide(new BigDecimal(0.97), 0,
													RoundingMode.HALF_EVEN)
											.multiply(new BigDecimal(0.03))
											.setScale(0, RoundingMode.HALF_EVEN);
									jurnal.setDebet(debet);
									jurnal.setKredit(NOL);
									jurnal.setBlc("*");
									jurnal.setTipe("D");
									jurnal.setNoReff(terima.getTrioTerimaPK()
											.getNoFak());
									listJurnal.add(jurnal);

								}
							}//
							String[] arrayPerk = getNoPerkAndNmPerk(terima
									.getKet2());
							String vNoPerk = arrayPerk[0];
							String vNmPerk = arrayPerk[1];

							List<TrioTerima> listTerima2 = findByNoLKHAndNoFakAndKet(
									terima.getTrioTerimaPK().getNoLKH(), terima
											.getTrioTerimaPK().getNoFak(),
									terima.getKet());

							for (TrioTerima trm : listTerima2) {
								if (terima.getKet2().equalsIgnoreCase("ADR")
										|| terima.getKet2().equalsIgnoreCase(
												"ADS")
										|| terima.getKet2().equalsIgnoreCase(
												"OTO")) { // beda ||
															// terima.getKet2().equalsIgnoreCase("WOM")
									if (terima.getKet2()
											.equalsIgnoreCase("OTO")) {
										jurnal = new TrioJurnal();
										jurnal.setTglJurnal(terima.getTglTrm());
										jurnal.getTrioJurnalPK()
												.setIdPerkiraan(vNoPerk0);
										jurnal.getTrioJurnalPK().setIdSub("");
										jurnal.setNamaPerkiraan(vNmPerk0);
										jurnal.setKet(trm.getNmTran() + " A.N "
												+ terima.getNmCust());
										// debet with vqcari1.debet*(10/11)
										BigDecimal dbt = trm
												.getDebet()
												.multiply(
														new BigDecimal(10)
																.divide(new BigDecimal(
																		11),
																		0,
																		RoundingMode.HALF_EVEN));
										jurnal.setDebet(NOL);
										jurnal.setKredit(dbt);
										jurnal.setBlc("*");
										jurnal.setTipe("K");
										jurnal.setNoReff(terima
												.getTrioTerimaPK().getNoFak());
										listJurnal.add(jurnal);

										jurnal = new TrioJurnal();
										jurnal.setTglJurnal(terima.getTglTrm());
										jurnal.getTrioJurnalPK()
												.setIdPerkiraan("71104");
										jurnal.getTrioJurnalPK().setIdSub("03");
										jurnal.setNamaPerkiraan("JASA PERANTARA OTO");
										jurnal.setKet(trm.getNmTran() + " A.N "
												+ terima.getNmCust());
										// debet with
										// vqcari1.debet-(vqcari1.debet*(10/11))
										dbt = trm.getDebet().subtract(dbt);
										jurnal.setDebet(NOL);
										jurnal.setKredit(dbt);
										jurnal.setBlc("*");
										jurnal.setTipe("K");
										jurnal.setNoReff(terima
												.getTrioTerimaPK().getNoFak());
										listJurnal.add(jurnal);
									} else {
										jurnal = new TrioJurnal();
										jurnal.setTglJurnal(terima.getTglTrm());
										jurnal.getTrioJurnalPK()
												.setIdPerkiraan(vNoPerk0);
										jurnal.getTrioJurnalPK().setIdSub("");
										jurnal.setNamaPerkiraan(vNmPerk);
										jurnal.setKet(trm.getNmTran() + " A.N "
												+ terima.getNmCust());
										jurnal.setDebet(NOL);
										jurnal.setKredit(trm.getDebet());
										jurnal.setBlc("*");
										jurnal.setTipe("K");
										jurnal.setNoReff(terima
												.getTrioTerimaPK().getNoFak());
										listJurnal.add(jurnal);
									}
								} else {
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(
											vNoPerk0);
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan(vNmPerk);
									jurnal.setKet(trm.getNmTran() + " A.N "
											+ terima.getNmCust());
									jurnal.setDebet(NOL);
									jurnal.setKredit(trm.getDebet());
									jurnal.setBlc("*");
									jurnal.setTipe("K");
									jurnal.setNoReff(terima.getTrioTerimaPK()
											.getNoFak());
									listJurnal.add(jurnal);
								}
								// sampai sini brorrrrr
								// IF vqcari1.no_perk='21116' AND
								// vqcari1.no_sub='01'
								if (trm.getNoPerk().equalsIgnoreCase("21116")
										&& trm.getNoSub()
												.equalsIgnoreCase("01")) {
									if (terima.getKet2()
											.equalsIgnoreCase("ADR")
											|| terima.getKet2()
													.equalsIgnoreCase("ADS")
											|| terima.getKet2()
													.equalsIgnoreCase("OTO")) { // beda
																				// ||
																				// terima.getKet2().equalsIgnoreCase("WOM")
										String vNoPerkk = "71105";
										String vNoSubk = trm.getNoSub();
										String vNmPerkk = "";
										if (vNoSubk.length() > 0) {
											vNmPerkk = getMasterFacade()
													.getTrioMstsubperkiraanDao()
													.findNamaSubByIdAndIdSub(
															vNoPerkk, vNoSubk);
										} else {
											vNmPerkk = getMasterFacade()
													.getTrioMstperkiraanDao()
													.findNamaById(vNoPerkk);
										}

										jurnal = new TrioJurnal();
										jurnal.setTglJurnal(terima.getTglTrm());
										jurnal.getTrioJurnalPK()
												.setIdPerkiraan(vNoPerkk);
										jurnal.getTrioJurnalPK().setIdSub(
												vNoSubk);
										jurnal.setNamaPerkiraan(vNmPerkk);
										jurnal.setKet(trm.getNmTran() + " A.N "
												+ terima.getNmCust());

										BigDecimal kredit = trm
												.getDebet()
												.multiply(
														new BigDecimal(10)
																.divide(new BigDecimal(
																		11),
																		0,
																		RoundingMode.HALF_EVEN));
										jurnal.setDebet(kredit);
										jurnal.setKredit(NOL);
										jurnal.setBlc("*");
										jurnal.setTipe("D");
										jurnal.setNoReff(terima
												.getTrioTerimaPK().getNoFak());
										listJurnal.add(jurnal);

										vNoPerkk = "21404";
										vNmPerkk = getMasterFacade()
												.getTrioMstperkiraanDao()
												.findNamaById(vNoPerkk);
										if (vNoPerk.length() < 1) {
											vNmPerkk = "";
										}

										jurnal = new TrioJurnal();
										jurnal.setTglJurnal(terima.getTglTrm());
										jurnal.getTrioJurnalPK()
												.setIdPerkiraan(vNoPerkk);
										jurnal.getTrioJurnalPK().setIdSub(
												vNoSubk);
										jurnal.setNamaPerkiraan(vNmPerkk);
										jurnal.setKet(trm.getNmTran() + " A.N "
												+ terima.getNmCust());

										// kredit with
										// ROUND((vqcari1.debet*(10/11))*0.1,0)
										kredit = trm
												.getDebet()
												.multiply(
														new BigDecimal(10)
																.divide(new BigDecimal(
																		11),
																		0,
																		RoundingMode.HALF_EVEN))
												.multiply(new BigDecimal(0.1))
												.setScale(0,
														RoundingMode.HALF_EVEN);
										jurnal.setDebet(kredit);
										jurnal.setKredit(NOL);
										jurnal.setBlc("*");
										jurnal.setTipe("D");
										jurnal.setNoReff(terima
												.getTrioTerimaPK().getNoFak());
										listJurnal.add(jurnal);

									} else {
										String vNoPerkk = "61104";
										String vNoSubk = "06";
										String vNmPerkk = getMasterFacade()
												.getTrioMstsubperkiraanDao()
												.findNamaSubByIdAndIdSub(
														vNoPerkk, vNoSubk);
										if (vNmPerkk.length() < 1) {
											vNmPerkk = "";
										}
										jurnal = new TrioJurnal();
										jurnal.setTglJurnal(terima.getTglTrm());
										jurnal.getTrioJurnalPK()
												.setIdPerkiraan(vNoPerkk);
										jurnal.getTrioJurnalPK().setIdSub(
												vNoSubk);
										jurnal.setNamaPerkiraan(vNmPerkk);
										jurnal.setKet(trm.getNmTran() + " A.N "
												+ terima.getNmCust());
										jurnal.setDebet(trm.getDebet());
										jurnal.setKredit(NOL);
										jurnal.setBlc("*");
										jurnal.setTipe("D");
										jurnal.setNoReff(terima
												.getTrioTerimaPK().getNoFak());
										listJurnal.add(jurnal);

									}
								} else { // kurang disini ternyata
									// IF (vket2='ADR' OR vket2='ADS' OR
									// vket2='OTO' OR vket2='WOM')
									if (terima.getKet2()
											.equalsIgnoreCase("ADR")
											|| terima.getKet2()
													.equalsIgnoreCase("ADS")
											|| terima.getKet2()
													.equalsIgnoreCase("OTO")) { // beda
																				// ||
																				// terima.getKet2().equalsIgnoreCase("WOM")
										String vNoPerkk = "71105";
										String vNoSubk = trm.getNoSub();
										String vNmPerkk = "";
										if (vNoSubk.length() > 0) {
											vNmPerkk = getMasterFacade()
													.getTrioMstsubperkiraanDao()
													.findNamaSubByIdAndIdSub(
															vNoPerkk, vNoSubk);
										} else {
											vNmPerkk = getMasterFacade()
													.getTrioMstperkiraanDao()
													.findNamaById(vNoPerkk);
										}
										jurnal = new TrioJurnal();
										jurnal.setTglJurnal(terima.getTglTrm());
										jurnal.getTrioJurnalPK()
												.setIdPerkiraan(vNoPerkk);
										jurnal.getTrioJurnalPK().setIdSub(
												vNoSubk);
										jurnal.setNamaPerkiraan(vNmPerkk);
										jurnal.setKet(trm.getNmTran() + " A.N "
												+ terima.getNmCust());
										// debet with vqcari1.debet*(10/11)
										BigDecimal temp = trm
												.getDebet()
												.multiply(
														new BigDecimal(10)
																.divide(new BigDecimal(
																		11),
																		0,
																		RoundingMode.HALF_EVEN));
										jurnal.setDebet(temp);
										jurnal.setKredit(NOL);
										jurnal.setBlc("*");
										jurnal.setTipe("D");
										jurnal.setNoReff(terima
												.getTrioTerimaPK().getNoFak());
										listJurnal.add(jurnal);

										vNoPerkk = "21404";
										vNmPerkk = getMasterFacade()
												.getTrioMstperkiraanDao()
												.findNamaById(vNoPerkk);
										if (vNmPerkk.length() < 1) {
											vNmPerkk = "";
										}
										jurnal = new TrioJurnal();
										jurnal.setTglJurnal(terima.getTglTrm());
										jurnal.getTrioJurnalPK()
												.setIdPerkiraan(vNoPerkk);
										jurnal.getTrioJurnalPK().setIdSub("");
										jurnal.setNamaPerkiraan(vNmPerkk);
										jurnal.setKet(trm.getNmTran() + " A.N "
												+ terima.getNmCust());

										// kredit with
										// ROUND((vqcari1.debet*(10/11))*0.1,0)
										temp = trm
												.getDebet()
												.multiply(
														new BigDecimal(10)
																.divide(new BigDecimal(
																		11),
																		0,
																		RoundingMode.HALF_EVEN));
										temp = temp.multiply(
												new BigDecimal(0.1)).setScale(
												0, RoundingMode.HALF_EVEN);
										jurnal.setDebet(temp);
										jurnal.setKredit(NOL);
										jurnal.setBlc("*");
										jurnal.setTipe("D");
										jurnal.setNoReff(terima
												.getTrioTerimaPK().getNoFak());
										listJurnal.add(jurnal);
									} else {
										String vNoPerkk = trm.getNoPerk();
										String vNoSubk = trm.getNoSub();
										String vNmPerkk = "";
										if (vNoSubk.length() > 0) {
											vNmPerkk = getMasterFacade()
													.getTrioMstsubperkiraanDao()
													.findNamaSubByIdAndIdSub(
															vNoPerkk, vNoSubk);
										} else {
											vNmPerkk = getMasterFacade()
													.getTrioMstperkiraanDao()
													.findNamaById(vNoPerkk);
										}
										jurnal = new TrioJurnal();
										jurnal.setTglJurnal(terima.getTglTrm());
										jurnal.getTrioJurnalPK()
												.setIdPerkiraan(vNoPerkk);
										jurnal.getTrioJurnalPK().setIdSub(
												vNoSubk);
										jurnal.setNamaPerkiraan(vNmPerkk);
										jurnal.setKet(trm.getNmTran() + " A.N "
												+ terima.getNmCust());
										jurnal.setDebet(trm.getDebet());
										jurnal.setKredit(NOL);
										jurnal.setBlc("*");
										jurnal.setTipe("D");
										jurnal.setNoReff(terima
												.getTrioTerimaPK().getNoFak());
										listJurnal.add(jurnal);

									}
								}

							}
						}
					}
				}

				for (TrioJurnal jur : listJurnal) {
					System.out.println("Jurnal idPerk = "
							+ jur.getTrioJurnalPK().getIdPerkiraan());
					System.out.println("Jurnal idSub = "
							+ jur.getTrioJurnalPK().getIdSub());
				}

				String jenisJurnal = getJenisJurnalByParams(terima.getKet(),
						terima.getDebet(), terima.getKredit(),
						terima.getNoPerk());
				// getMasterFacade().getTrioJurnalDao().saveTransaction(listJurnal,
				// jenisJurnal, user);
				// case vno_perk='P2'
			} else if (terima.getNoPerk().equalsIgnoreCase("P2")) { 
				System.out.println("CASEIF 21");

				if (terima.getDebet().compareTo(NOL) == 1) {
					if (terima.getKet().equalsIgnoreCase("1")
							|| terima.getKet().equalsIgnoreCase("K")) {
						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan("11101");
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan("KAS");
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(terima.getDebet());
						jurnal.setKredit(NOL);
						jurnal.setBlc("*");
						jurnal.setTipe("D");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);
					}
					if (terima.getKet().equalsIgnoreCase("2")) {
						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan("11203");
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan("CEK");
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(terima.getDebet());
						jurnal.setKredit(NOL);
						jurnal.setBlc("*");
						jurnal.setTipe("D");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);
					}
					if (terima.getKet().equalsIgnoreCase("3")) {
						String vNoPerkA = "11202";
						String vNoSubA = terima.getKdBank();
						String vNmPerkA = "";
						if (vNoSubA.length() > 0) {
							vNmPerkA = getMasterFacade()
									.getTrioMstsubperkiraanDao()
									.findNamaSubByIdAndIdSub(vNoPerkA, vNoSubA);
						}
						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerkA);
						jurnal.getTrioJurnalPK().setIdSub(vNoSubA);
						jurnal.setNamaPerkiraan(vNmPerkA);
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(terima.getDebet());
						jurnal.setKredit(NOL);
						jurnal.setBlc("*");
						jurnal.setTipe("D");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);
					}
					if (terima.getKet().equalsIgnoreCase("4")) {
						String vNoPerkA = "11120";
						String vNoSubA = terima.getKdPOS();
						String vNmPerkA = "";
						if (vNoSubA.length() > 0) {
							vNmPerkA = getMasterFacade()
									.getTrioMstsubperkiraanDao()
									.findNamaSubByIdAndIdSub(vNoPerkA, vNoSubA);
						}
						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerkA);
						jurnal.getTrioJurnalPK().setIdSub(vNoSubA);
						jurnal.setNamaPerkiraan(vNmPerkA);
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(terima.getDebet());
						jurnal.setKredit(NOL);
						jurnal.setBlc("*");
						jurnal.setTipe("D");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);
					}

					String vFNoSub = "";
					boolean vSubBaru = false;
					TrioMstsubperkiraan subObject = getMasterFacade()
							.getTrioMstsubperkiraanDao().getSubByIdPerkAndKet2(
									"11301", terima.getNoMesin());
					if (subObject != null) {
						if (subObject.getTrioMstsubperkiraanPK()
								.getIdPerkiraan().length() > 0) {
							vFNoSub = subObject.getTrioMstsubperkiraanPK()
									.getIdSub();
							vSubBaru = false;
						}
					} else {
						List<TrioMstsubperkiraan> listSub = getMasterFacade()
								.getTrioMstsubperkiraanDao().findByIdPerkiraan(
										"11301");
						int jmlListSub = listSub.size();
						TrioMstsubperkiraan subObj = listSub
								.get(jmlListSub - 1); // ambil object terakhir
														// dari list
						String vNoSubAkhir = subObj.getTrioMstsubperkiraanPK()
								.getIdSub();
						int no = getMasterFacade().getTrioMstrunnumDao().getRunningNumber("IDSUB", "", user);
						String noSubBaru = TrioStringUtil.getFormattedRunno(Integer.valueOf(no));
						vFNoSub = noSubBaru;
						vSubBaru = true;
					}

					BigDecimal debetKurangBunga = terima.getDebet().subtract(
							terima.getsBunga());
					if (terima.getHarga().compareTo(debetKurangBunga) == 1) { // vharga>vdebet-vs_bunga

						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan("11301");
						jurnal.getTrioJurnalPK().setIdSub(vFNoSub);
						jurnal.setNamaPerkiraan(terima.getNmCust());
						jurnal.setKet(terima.getNmTran());
						BigDecimal jkWaktuKaliAngsuran = new BigDecimal(
								terima.getJkWaktu()).multiply(terima
								.getAngsuran());
						jurnal.setDebet(jkWaktuKaliAngsuran);
						jurnal.setKredit(NOL);
						jurnal.setBlc("*");
						jurnal.setTipe("D");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);
						if (vSubBaru) {
							TrioMstsubperkiraan subPerk = new TrioMstsubperkiraan(
									vFNoSub, "11301");
							subPerk.setNamaSub(terima.getNmCust());
							subPerk.setKet2(terima.getNoMesin());
							subPerk.setUangMuka(terima.getDebet());
							subPerk.setTglAwal(terima.getTglTrm());
							getMasterFacade().getTrioMstsubperkiraanDao().save(
									subPerk, user);
						}

					}
					// vno_perk1='41101'
					// vno_perk2='21404'
					// vno_perk3='21204'
					// vno_perk4='21205'
					// vno_perk5='21228'
					// vno_perk6='71101'
					// vno_perk7='41201'

					// vdpp=round(((vharga-vb_stnk-(vs_ahm+vs_md+vs_sd))*10/11),0)
					// vppnx= (vharga-vb_stnk-(vs_ahm+vs_md+vs_sd))-vdpp
					// vpot_penjualan=vs_ahm+vs_md+vs_sd

					BigDecimal vPotPenjualan = NOL;
					BigDecimal totalSubsidi = NOL;
					totalSubsidi = terima.getsAHM().add(terima.getsMD())
							.add(terima.getsSD())
							.setScale(0, RoundingMode.HALF_EVEN);
					BigDecimal temp = terima.getHarga()
							.subtract(terima.getbSTNK()).subtract(totalSubsidi);
					BigDecimal vdpp = temp.multiply(new BigDecimal(10)).divide(
							new BigDecimal(11), 0, RoundingMode.HALF_EVEN);
					BigDecimal vppnx = temp.subtract(vdpp);
					vPotPenjualan = totalSubsidi;

					String vNoPerk1 = "41101";
					String vNmPerk1 = getMasterFacade()
							.getTrioMstperkiraanDao().findNamaById(vNoPerk1);
					if (vNmPerk1 == null || vNmPerk1.length() < 1) {
						vNmPerk1 = "";
					}

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk1);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk1);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(NOL);
					jurnal.setKredit(vdpp);
					jurnal.setBlc("*");
					jurnal.setTipe("K");
					jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
					jurnal.setNoMesin(terima.getNoMesin());
					listJurnal.add(jurnal);

					String vNoPerk2 = "21404";
					String vNmPerk2 = getMasterFacade()
							.getTrioMstperkiraanDao().findNamaById(vNoPerk2);
					if (vNmPerk2 == null || vNmPerk2.length() < 1) {
						vNmPerk2 = "";
					}

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk2);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk2);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(NOL);
					jurnal.setKredit(vppnx);
					jurnal.setBlc("*");
					jurnal.setTipe("K");
					jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
					jurnal.setNoMesin(terima.getNoMesin());
					listJurnal.add(jurnal);

					String vNoPerk7 = "41201";
					String vNmPerk7 = getMasterFacade()
							.getTrioMstperkiraanDao().findNamaById(vNoPerk7);
					if (vNmPerk7 == null || vNmPerk7.length() < 1) {
						vNmPerk7 = "";
					}

					if (vPotPenjualan.compareTo(NOL) == 1) {

						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk7);
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan(vNmPerk7);
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(NOL);
						jurnal.setKredit(vPotPenjualan);
						jurnal.setBlc("*");
						jurnal.setTipe("K");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);

					}

					String vNoPerk3 = "21204";
					String vNmPerk3 = getMasterFacade()
							.getTrioMstperkiraanDao().findNamaById(vNoPerk3);
					if (vNmPerk3 == null || vNmPerk3.length() < 1) {
						vNmPerk3 = "";
					}

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk3);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk3);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(NOL);
					jurnal.setKredit(terima.getpSTNK());
					jurnal.setBlc("*");
					jurnal.setTipe("K");
					jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
					jurnal.setNoMesin(terima.getNoMesin());
					listJurnal.add(jurnal);

					String vNoPerk4 = "21205";
					String vNmPerk4 = getMasterFacade()
							.getTrioMstperkiraanDao().findNamaById(vNoPerk4);
					if (vNmPerk4 == null || vNmPerk4.length() < 1) {
						vNmPerk4 = "";
					}

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk4);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk4);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(NOL);
					jurnal.setKredit(terima.getpBPKB());
					jurnal.setBlc("*");
					jurnal.setTipe("K");
					jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
					jurnal.setNoMesin(terima.getNoMesin());
					listJurnal.add(jurnal);

					String vNoPerk5 = "21228";
					String vNmPerk5 = getMasterFacade()
							.getTrioMstperkiraanDao().findNamaById(vNoPerk5);
					if (vNmPerk5 == null || vNmPerk5.length() < 1) {
						vNmPerk5 = "";
					}
					// //vb_stnk-vp_stnk-vp_bpkb
					BigDecimal hitungBiaya = terima.getbSTNK()
							.subtract(terima.getpSTNK())
							.subtract(terima.getpBPKB());
					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk5);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk5);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(NOL);
					jurnal.setKredit(hitungBiaya);
					jurnal.setBlc("*");
					jurnal.setTipe("K");
					jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
					jurnal.setNoMesin(terima.getNoMesin());
					listJurnal.add(jurnal);

					// (vjk_waktu*vangsuran)-(vharga-vdebet)>0
					BigDecimal waktuKaliAngs = terima.getAngsuran()
							.multiply(new BigDecimal(terima.getJkWaktu()))
							.setScale(0, RoundingMode.HALF_EVEN);
					BigDecimal hargaKurangDebet = terima.getHarga().subtract(
							terima.getDebet());
					BigDecimal hasil = waktuKaliAngs.subtract(hargaKurangDebet);
					if (hasil.compareTo(NOL) == 1) {
						String vNoPerk6 = "71101";
						String vNmPerk6 = getMasterFacade()
								.getTrioMstperkiraanDao()
								.findNamaById(vNoPerk6);
						if (vNmPerk6 == null || vNmPerk6.length() < 1) {
							vNmPerk6 = "";
						}

						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk6);
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan(vNmPerk6);
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(NOL);
						jurnal.setKredit(hasil);
						jurnal.setBlc("*");
						jurnal.setTipe("K");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);

					} else { // else hasil.compareTo(NOL)== 1
						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk7);
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan(vNmPerk7);
						jurnal.setKet(terima.getNmTran());
						// debet with ((vjk_waktu*vangsuran)-(vharga-vdebet))*-1
						hasil = hasil.multiply(new BigDecimal(-1));
						jurnal.setDebet(hasil);
						jurnal.setKredit(NOL);
						jurnal.setBlc("*");
						jurnal.setTipe("D");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);
					}

				} else { // else debet > 0

					if (terima.getKet().equalsIgnoreCase("1")
							|| terima.getKet().equalsIgnoreCase("K")) {
						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan("11101");
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan("KAS");
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(NOL);
						jurnal.setKredit(terima.getKredit());
						jurnal.setBlc("*");
						jurnal.setTipe("K");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);
					}
					if (terima.getKet().equalsIgnoreCase("2")) {
						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan("11203");
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan("CEK");
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(NOL);
						jurnal.setKredit(terima.getKredit());
						jurnal.setBlc("*");
						jurnal.setTipe("K");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);
					}
					if (terima.getKet().equalsIgnoreCase("3")) {
						String vNoPerkA = "11202";
						String vNoSubA = terima.getKdBank();
						String vNmPerkA = "";
						if (vNoSubA.length() > 0) {
							vNmPerkA = getMasterFacade()
									.getTrioMstsubperkiraanDao()
									.findNamaSubByIdAndIdSub(vNoPerkA, vNoSubA);
						}
						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerkA);
						jurnal.getTrioJurnalPK().setIdSub(vNoSubA);
						jurnal.setNamaPerkiraan(vNmPerkA);
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(NOL);
						jurnal.setKredit(terima.getKredit());
						jurnal.setBlc("*");
						jurnal.setTipe("K");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);
					}
					if (terima.getKet().equalsIgnoreCase("4")) {
						String vNoPerkA = "11120";
						String vNoSubA = terima.getKdPOS();
						String vNmPerkA = "";
						if (vNoSubA.length() > 0) {
							vNmPerkA = getMasterFacade()
									.getTrioMstsubperkiraanDao()
									.findNamaSubByIdAndIdSub(vNoPerkA, vNoSubA);
						}
						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerkA);
						jurnal.getTrioJurnalPK().setIdSub(vNoSubA);
						jurnal.setNamaPerkiraan(vNmPerkA);
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(terima.getDebet());
						jurnal.setKredit(NOL);
						jurnal.setBlc("*");
						jurnal.setTipe("D");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);
					}

					if (terima.getHarga().compareTo(terima.getKredit()) == 1) { // vharga>vkredit

						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan("11301");
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan(terima.getNmCust());
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(NOL);
						// kredit with vjk_waktu*vangsuran &&vharga-vkredit
						BigDecimal hasil = new BigDecimal(terima.getJkWaktu())
								.multiply(terima.getAngsuran()).setScale(0,
										RoundingMode.HALF_EVEN);
						jurnal.setKredit(hasil);
						jurnal.setBlc("*");
						jurnal.setTipe("K");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);

						TrioMstsubperkiraan subPerk = getMasterFacade()
								.getTrioMstsubperkiraanDao()
								.getSubByIdPerkAndKet2("11301",
										terima.getNoMesin());
						if (subPerk != null) {
							getMasterFacade().getTrioMstsubperkiraanDao()
									.delete(subPerk);
						}
					}
					// MULAI COPY
					BigDecimal vPotPenjualan = NOL;
					BigDecimal totalSubsidi = NOL;
					totalSubsidi = terima.getsAHM().add(terima.getsMD())
							.add(terima.getsSD())
							.setScale(0, RoundingMode.HALF_EVEN);
					BigDecimal temp = terima.getHarga()
							.subtract(terima.getbSTNK()).subtract(totalSubsidi);
					BigDecimal vdpp = temp.multiply(new BigDecimal(10)).divide(
							new BigDecimal(11), 0, RoundingMode.HALF_EVEN);
					BigDecimal vppnx = temp.subtract(vdpp);
					vPotPenjualan = totalSubsidi;

					String vNoPerk1 = "41101";
					String vNmPerk1 = getMasterFacade()
							.getTrioMstperkiraanDao().findNamaById(vNoPerk1);
					if (vNmPerk1 == null || vNmPerk1.length() < 1) {
						vNmPerk1 = "";
					}

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk1);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk1);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(vdpp);
					jurnal.setKredit(NOL);
					jurnal.setBlc("*");
					jurnal.setTipe("D");
					jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
					jurnal.setNoMesin(terima.getNoMesin());
					listJurnal.add(jurnal);

					String vNoPerk2 = "21404";
					String vNmPerk2 = getMasterFacade()
							.getTrioMstperkiraanDao().findNamaById(vNoPerk2);
					if (vNmPerk2 == null || vNmPerk2.length() < 1) {
						vNmPerk2 = "";
					}

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk2);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk2);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(vppnx);
					jurnal.setKredit(NOL);
					jurnal.setBlc("*");
					jurnal.setTipe("D");
					jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
					jurnal.setNoMesin(terima.getNoMesin());
					listJurnal.add(jurnal);

					String vNoPerk7 = "41201";
					String vNmPerk7 = getMasterFacade()
							.getTrioMstperkiraanDao().findNamaById(vNoPerk7);
					if (vNmPerk7 == null || vNmPerk7.length() < 1) {
						vNmPerk7 = "";
					}

					if (vPotPenjualan.compareTo(NOL) == 1) {

						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk7);
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan(vNmPerk7);
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(NOL);
						jurnal.setKredit(vPotPenjualan);
						jurnal.setBlc("*");
						jurnal.setTipe("K");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);

					}

					String vNoPerk3 = "21204";
					String vNmPerk3 = getMasterFacade()
							.getTrioMstperkiraanDao().findNamaById(vNoPerk3);
					if (vNmPerk3 == null || vNmPerk3.length() < 1) {
						vNmPerk3 = "";
					}

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk3);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk3);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(terima.getpSTNK());
					jurnal.setKredit(NOL);
					jurnal.setBlc("*");
					jurnal.setTipe("D");
					jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
					jurnal.setNoMesin(terima.getNoMesin());
					listJurnal.add(jurnal);

					String vNoPerk4 = "21205";
					String vNmPerk4 = getMasterFacade()
							.getTrioMstperkiraanDao().findNamaById(vNoPerk4);
					if (vNmPerk4 == null || vNmPerk4.length() < 1) {
						vNmPerk4 = "";
					}

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk4);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk4);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(terima.getpBPKB());
					jurnal.setKredit(NOL);
					jurnal.setBlc("*");
					jurnal.setTipe("D");
					jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
					jurnal.setNoMesin(terima.getNoMesin());
					listJurnal.add(jurnal);

					String vNoPerk5 = "21228";
					String vNmPerk5 = getMasterFacade()
							.getTrioMstperkiraanDao().findNamaById(vNoPerk5);
					if (vNmPerk5 == null || vNmPerk5.length() < 1) {
						vNmPerk5 = "";
					}
					// //vb_stnk-vp_stnk-vp_bpkb
					BigDecimal hitungBiaya = terima.getbSTNK()
							.subtract(terima.getpSTNK())
							.subtract(terima.getpBPKB());
					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk5);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk5);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(hitungBiaya);
					jurnal.setKredit(NOL);
					jurnal.setBlc("*");
					jurnal.setTipe("D");
					jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
					jurnal.setNoMesin(terima.getNoMesin());
					listJurnal.add(jurnal);

					// (vjk_waktu*vangsuran)-(vharga-vkredit)>0
					BigDecimal waktuKaliAngs = terima.getAngsuran()
							.multiply(new BigDecimal(terima.getJkWaktu()))
							.setScale(0, RoundingMode.HALF_EVEN);
					BigDecimal hargaKurangKredit = terima.getHarga().subtract(
							terima.getKredit());
					BigDecimal hasil = waktuKaliAngs
							.subtract(hargaKurangKredit);
					if (hasil.compareTo(NOL) == 1) {
						String vNoPerk6 = "71101";
						String vNmPerk6 = getMasterFacade()
								.getTrioMstperkiraanDao()
								.findNamaById(vNoPerk6);
						if (vNmPerk6 == null || vNmPerk6.length() < 1) {
							vNmPerk6 = "";
						}

						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk6);
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan(vNmPerk6);
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(hasil);
						jurnal.setKredit(NOL);
						jurnal.setBlc("*");
						jurnal.setTipe("D");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);

					} else { // else hasil.compareTo(NOL)== 1
						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk7);
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan(vNmPerk7);
						jurnal.setKet(terima.getNmTran());
						// debet with ((vjk_waktu*vangsuran)-(vharga-vdebet))*-1
						// kredit with
						// ((vjk_waktu*vangsuran)-(vharga-vkredit))*-1
						hasil = hasil.multiply(new BigDecimal(-1));
						jurnal.setDebet(NOL);
						jurnal.setKredit(hasil);
						jurnal.setBlc("*");
						jurnal.setTipe("K");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);
					}
					// AKHIR COPY
				}
				String jenisJurnal = getJenisJurnalByParams(terima.getKet(),
						terima.getDebet(), terima.getKredit(),
						terima.getNoPerk());
				getMasterFacade().getTrioJurnalDao().saveTransaction(
						listJurnal, jenisJurnal, user);
				// end case vno_perk='P2'
				// case vno_perk='41201' and (vket='1' OR vket='K') AND ALLTRIM(vket2)='OFF TR'
			} else if (terima.getNoPerk().equalsIgnoreCase("41201")
					&& (terima.getKet().equalsIgnoreCase("1") || terima
							.getKet().equalsIgnoreCase("K"))
					&& terima.getKet2().equalsIgnoreCase("OFF TR")) { 

				System.out.println("CASEIF 22");

				TrioTerima tt = getTerimaByNoFak(terima.getTrioTerimaPK()
						.getNoFak());
				BigDecimal vbSTNK = tt.getbSTNK();
				BigDecimal vpSTNK = tt.getpSTNK();
				BigDecimal vpBPKB = tt.getpBPKB();

				if (terima.getKredit().compareTo(NOL) == 1) {
					// vno_perk3='21204'
					// vno_perk4='21205'
					// vno_perk5='21228'
					// vno_perk6='42199'
					String vNoPerk3 = "21204";
					String vNmPerk3 = getMasterFacade()
							.getTrioMstperkiraanDao().findNamaById(vNoPerk3);

					String vNoPerk4 = "21205";
					String vNmPerk4 = getMasterFacade()
							.getTrioMstperkiraanDao().findNamaById(vNoPerk4);

					String vNoPerk5 = "21228";
					String vNmPerk5 = getMasterFacade()
							.getTrioMstperkiraanDao().findNamaById(vNoPerk5);

					String vNoPerk6 = "42199";
					String vNmPerk6 = getMasterFacade()
							.getTrioMstperkiraanDao().findNamaById(vNoPerk6);

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk3);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk3);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(vpSTNK);
					jurnal.setKredit(NOL);
					jurnal.setBlc("*");
					jurnal.setTipe("D");
					jurnal.setKet2(terima.getTrioTerimaPK().getNoFak());
					listJurnal.add(jurnal);

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk4);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk4);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(vpBPKB);
					jurnal.setKredit(NOL);
					jurnal.setBlc("*");
					jurnal.setTipe("D");
					jurnal.setKet2(terima.getTrioTerimaPK().getNoFak());
					listJurnal.add(jurnal);

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk5);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk5);
					jurnal.setKet(terima.getNmTran());
					// debet with vb_stnk-vp_stnk-vp_bpkb
					BigDecimal hitung = vbSTNK.subtract(vpSTNK)
							.subtract(vpBPKB);
					jurnal.setDebet(hitung);
					jurnal.setKredit(NOL);
					jurnal.setBlc("*");
					jurnal.setTipe("D");
					jurnal.setKet2(terima.getTrioTerimaPK().getNoFak());
					listJurnal.add(jurnal);

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan("11101");
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan("KAS");
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(NOL);
					jurnal.setKredit(terima.getKredit());
					jurnal.setBlc("*");
					jurnal.setTipe("K");
					jurnal.setKet2(terima.getTrioTerimaPK().getNoFak());
					listJurnal.add(jurnal);

					// vb_stnk-vkredit>0
					BigDecimal bStnkKurangKredit = vbSTNK.subtract(terima
							.getKredit());
					if (bStnkKurangKredit.compareTo(NOL) == 1) {
						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk6);
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan(vNmPerk6);
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(NOL);
						jurnal.setKredit(bStnkKurangKredit);
						jurnal.setBlc("*");
						jurnal.setTipe("K");
						jurnal.setKet2(terima.getTrioTerimaPK().getNoFak());
						listJurnal.add(jurnal);
					}

				} else { // kredit > 0
					String vNoPerk3 = "21204";
					String vNmPerk3 = getMasterFacade()
							.getTrioMstperkiraanDao().findNamaById(vNoPerk3);

					String vNoPerk4 = "21205";
					String vNmPerk4 = getMasterFacade()
							.getTrioMstperkiraanDao().findNamaById(vNoPerk4);

					String vNoPerk5 = "21228";
					String vNmPerk5 = getMasterFacade()
							.getTrioMstperkiraanDao().findNamaById(vNoPerk5);

					String vNoPerk6 = "42199";
					String vNmPerk6 = getMasterFacade()
							.getTrioMstperkiraanDao().findNamaById(vNoPerk6);

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk3);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk3);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(NOL);
					jurnal.setKredit(vpSTNK);
					jurnal.setBlc("*");
					jurnal.setTipe("K");
					jurnal.setKet2(terima.getTrioTerimaPK().getNoFak());
					listJurnal.add(jurnal);

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk4);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk4);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(NOL);
					jurnal.setKredit(vpBPKB);
					jurnal.setBlc("*");
					jurnal.setTipe("K");
					jurnal.setKet2(terima.getTrioTerimaPK().getNoFak());
					listJurnal.add(jurnal);

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk5);
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan(vNmPerk5);
					jurnal.setKet(terima.getNmTran());
					// debet with vb_stnk-vp_stnk-vp_bpkb
					// kredit with vb_stnk-vp_stnk-vp_bpkb
					BigDecimal hitung = vbSTNK.subtract(vpSTNK)
							.subtract(vpBPKB);
					jurnal.setDebet(NOL);
					jurnal.setKredit(hitung);
					jurnal.setBlc("*");
					jurnal.setTipe("K");
					jurnal.setKet2(terima.getTrioTerimaPK().getNoFak());
					listJurnal.add(jurnal);

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan("11101");
					jurnal.getTrioJurnalPK().setIdSub("");
					jurnal.setNamaPerkiraan("KAS");
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(terima.getKredit());
					jurnal.setKredit(NOL);
					jurnal.setBlc("*");
					jurnal.setTipe("D");
					jurnal.setKet2(terima.getTrioTerimaPK().getNoFak());
					listJurnal.add(jurnal);

					// vb_stnk-vkredit>0
					BigDecimal bStnkKurangKredit = vbSTNK.subtract(terima
							.getKredit());
					if (bStnkKurangKredit.compareTo(NOL) == 1) {
						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk6);
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan(vNmPerk6);
						jurnal.setKet(terima.getNmTran());
						// debet with vb_stnk-vkredit
						jurnal.setDebet(bStnkKurangKredit);
						jurnal.setKredit(NOL);
						jurnal.setBlc("*");
						jurnal.setTipe("D");
						jurnal.setKet2(terima.getTrioTerimaPK().getNoFak());
						listJurnal.add(jurnal);
					}
				}

				String jenisJurnal = getJenisJurnalByParams(terima.getKet(),
						terima.getDebet(), terima.getKredit(),
						terima.getNoPerk());
				getMasterFacade().getTrioJurnalDao().saveTransaction(
						listJurnal, jenisJurnal, user);
				// end case 41201
				// case vno_perk='11301' and (vket='1' or vket='K') AND ALLTRIM(master.kd_prs)<>'T10'
			} else if (terima.getNoPerk().equalsIgnoreCase("11301")
					&& (terima.getNoPerk().equalsIgnoreCase("1") || terima
							.getNoPerk().equalsIgnoreCase("K"))) {

				System.out.println("CASEIF 23");

				if (terima.getDebet().compareTo(NOL) == 1) {

					TrioMstsubperkiraan subPerk = getMasterFacade()
							.getTrioMstsubperkiraanDao().getSubByIdPerkAndKet2(
									terima.getNoPerk(), terima.getKet2());
					if (subPerk != null) {
						String vNoSub = subPerk.getTrioMstsubperkiraanPK()
								.getIdSub();
						String vNmSub = subPerk.getNamaSub();
						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan("11101");
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan("KAS");
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(terima.getDebet());
						jurnal.setKredit(NOL);
						jurnal.setBlc("*");
						jurnal.setTipe("D");
						listJurnal.add(jurnal);

						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan(
								terima.getNoPerk());
						jurnal.getTrioJurnalPK().setIdSub(vNoSub);
						jurnal.setNamaPerkiraan(vNmSub);
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(NOL);
						jurnal.setKredit(terima.getDebet());
						jurnal.setBlc("*");
						jurnal.setTipe("K");
						jurnal.setKet2(terima.getKet2());
						listJurnal.add(jurnal);

					} else {
						System.out.println("No Mesin [ " + terima.getKet2()
								+ " ] tidak ditemukan");
					}

				} else { // else untuk debet > 0
					TrioMstsubperkiraan subPerk = getMasterFacade()
							.getTrioMstsubperkiraanDao().getSubByIdPerkAndKet2(
									terima.getNoPerk(), terima.getKet2());
					if (subPerk != null) {
						String vNoSub = subPerk.getTrioMstsubperkiraanPK()
								.getIdSub();
						String vNmSub = subPerk.getNamaSub();
						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan("11101");
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan("KAS");
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(NOL);
						jurnal.setKredit(terima.getKredit());
						jurnal.setBlc("*");
						jurnal.setTipe("K");
						listJurnal.add(jurnal);

						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan(
								terima.getNoPerk());
						jurnal.getTrioJurnalPK().setIdSub(vNoSub);
						jurnal.setNamaPerkiraan(vNmSub);
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(terima.getKredit());
						jurnal.setKredit(NOL);
						jurnal.setBlc("*");
						jurnal.setTipe("D");
						jurnal.setKet2(terima.getKet2());
						listJurnal.add(jurnal);

					} else {
						System.out.println("No Mesin [ " + terima.getKet2()
								+ " ] tidak ditemukan");
					}
				}

				String jenisJurnal = getJenisJurnalByParams(terima.getKet(),
						terima.getDebet(), terima.getKredit(),
						terima.getNoPerk());
				getMasterFacade().getTrioJurnalDao().saveTransaction(
						listJurnal, jenisJurnal, user);
				// end case 11301
				// case vitr='*' AND vket<>'4'
			} else if (terima.getItr().equalsIgnoreCase("*")
					&& (!terima.getKet().equalsIgnoreCase("4"))) { 

				System.out.println("CASEIF 24");

				if (terima.getDebet().compareTo(NOL) == 1) {
					if (terima.getKet().equalsIgnoreCase("1")
							|| terima.getKet().equalsIgnoreCase("K")) {
						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan("11101");
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan("KAS");
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(terima.getDebet());
						jurnal.setKredit(NOL);
						jurnal.setBlc("*");
						jurnal.setTipe("D");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);
					}
					if (terima.getKet().equalsIgnoreCase("2")) {
						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan("11203");
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan("CEK");
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(terima.getDebet());
						jurnal.setKredit(NOL);
						jurnal.setBlc("*");
						jurnal.setTipe("D");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);
					}
					if (terima.getKet().equalsIgnoreCase("3")) {
						String vNoPerk = "11202";
						String vNoSub = terima.getKdBank();
						String vNmPerk = getMasterFacade()
								.getTrioMstsubperkiraanDao()
								.findNamaSubByIdAndIdSub(vNoPerk, vNoSub);
						if (vNmPerk == null || vNmPerk.length() < 1) {
							vNmPerk = "";
						}
						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
						jurnal.getTrioJurnalPK().setIdSub(vNoSub);
						jurnal.setNamaPerkiraan(vNmPerk);
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(terima.getDebet());
						jurnal.setKredit(NOL);
						jurnal.setBlc("*");
						jurnal.setTipe("D");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);

					}

					String vNoPerkA = "11120";
					String vNoSub1 = terima.getKdPOS();
					String vNmPerkA = "";
					if (vNoSub1.length() > 0) {
						vNmPerkA = getMasterFacade()
								.getTrioMstsubperkiraanDao()
								.findNamaSubByIdAndIdSub(vNoPerkA, vNoSub1);
					}

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerkA);
					if (vNoSub1.length() > 0) {
						jurnal.getTrioJurnalPK().setIdSub(vNoSub1);
					} else {
						jurnal.getTrioJurnalPK().setIdSub("");
					}
					jurnal.setNamaPerkiraan(vNmPerkA);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(NOL);
					jurnal.setKredit(terima.getDebet());
					jurnal.setBlc("*");
					jurnal.setTipe("K");
					jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
					jurnal.setNoMesin(terima.getNoMesin());
					listJurnal.add(jurnal);

				} else { // else debet > 0

					if (terima.getKet().equalsIgnoreCase("1")
							|| terima.getKet().equalsIgnoreCase("K")) {
						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan("11101");
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan("KAS");
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(NOL);
						jurnal.setKredit(terima.getKredit());
						jurnal.setBlc("*");
						jurnal.setTipe("K");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);
					}
					if (terima.getKet().equalsIgnoreCase("2")) {
						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan("11203");
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan("CEK");
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(NOL);
						jurnal.setKredit(terima.getKredit());
						jurnal.setBlc("*");
						jurnal.setTipe("K");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);
					}
					if (terima.getKet().equalsIgnoreCase("3")) {
						String vNoPerk = "11202";
						String vNoSub = terima.getKdBank();
						String vNmPerk = getMasterFacade()
								.getTrioMstsubperkiraanDao()
								.findNamaSubByIdAndIdSub(vNoPerk, vNoSub);
						if (vNmPerk == null || vNmPerk.length() < 1) {
							vNmPerk = "";
						}
						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
						jurnal.getTrioJurnalPK().setIdSub(vNoSub);
						jurnal.setNamaPerkiraan(vNmPerk);
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(NOL);
						jurnal.setKredit(terima.getKredit());
						jurnal.setBlc("*");
						jurnal.setTipe("K");
						jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
						jurnal.setNoMesin(terima.getNoMesin());
						listJurnal.add(jurnal);

					}

					String vNoPerkA = "11120";
					String vNoSub1 = terima.getKdPOS();
					String vNmPerkA = "";
					if (vNoSub1.length() > 0) {
						vNmPerkA = getMasterFacade()
								.getTrioMstsubperkiraanDao()
								.findNamaSubByIdAndIdSub(vNoPerkA, vNoSub1);
					}

					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerkA);
					if (vNoSub1.length() > 0) {
						jurnal.getTrioJurnalPK().setIdSub(vNoSub1);
					} else {
						jurnal.getTrioJurnalPK().setIdSub("");
					}
					jurnal.setNamaPerkiraan(vNmPerkA);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(terima.getKredit());
					jurnal.setKredit(NOL);
					jurnal.setBlc("*");
					jurnal.setTipe("D");
					jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
					jurnal.setNoMesin(terima.getNoMesin());
					listJurnal.add(jurnal);

				}
				String jenisJurnal = getJenisJurnalByParams(terima.getKet(),
						terima.getDebet(), terima.getKredit(),
						terima.getNoPerk());
				getMasterFacade().getTrioJurnalDao().saveTransaction(
						listJurnal, jenisJurnal, user);
				// end case
			} else { // OTHERWISE

				System.out.println("CASEIF 25");
				String vNoPerkA = "";
				String vNoSub1 = "";
				String vNmPerkA = "";
				if (terima.getDebet().compareTo(NOL) == 1) {

					if (terima.getKet().equalsIgnoreCase("4")) {

						vNoPerkA = "11120";

						vNoSub1 = terima.getKdPOS();
						if (vNoSub1.length() > 0) {
							vNmPerkA = getMasterFacade()
									.getTrioMstsubperkiraanDao()
									.findNamaSubByIdAndIdSub(vNoPerkA, vNoSub1);
						}

					} else { // else ket = 4
						vNoPerkA = "11101";
						vNmPerkA = "KAS";
					}
					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerkA);
					if (vNoSub1.length() > 0) {
						jurnal.getTrioJurnalPK().setIdSub(vNoSub1);
					} else {
						jurnal.getTrioJurnalPK().setIdSub("");
					}
					jurnal.setNamaPerkiraan(vNmPerkA);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(terima.getDebet());
					jurnal.setKredit(NOL);
					jurnal.setBlc("*");
					jurnal.setTipe("D");
					listJurnal.add(jurnal);

					vNoSub1 = "";

					vNmPerkA = getMasterFacade().getTrioMstperkiraanDao()
							.findNamaById(terima.getNoPerk());

					if (terima.getNoSub().length() > 0) {
						vNmPerkA = getMasterFacade()
								.getTrioMstsubperkiraanDao()
								.findNamaSubByIdAndIdSub(terima.getNoPerk(),
										terima.getNoSub());
					}

					// if vno_perk='41102' or vno_perk='41103' or
					// vno_perk='41104' or vno_perk='41105' or vno_perk='41301'
					// OR vno_perk='41116'
					if (terima.getNoPerk().equalsIgnoreCase("41102")
							|| terima.getNoPerk().equalsIgnoreCase("41103")
							|| terima.getNoPerk().equalsIgnoreCase("41104")
							|| terima.getNoPerk().equalsIgnoreCase("41105")
							|| terima.getNoPerk().equalsIgnoreCase("41301")
							|| terima.getNoPerk().equalsIgnoreCase("41116")) {

						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan(
								terima.getNoPerk());
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan(vNmPerkA);
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(NOL);
						jurnal.setKredit(vjumlah2);
						jurnal.setBlc("*");
						jurnal.setTipe("K");
						listJurnal.add(jurnal);

						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan("21404");
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan("PPN KELUARAN");
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(NOL);
						jurnal.setKredit(vjumlah3);
						jurnal.setBlc("*");
						jurnal.setTipe("K");
						listJurnal.add(jurnal);

						// else if vno_perk='41102' or vno_perk='41103' or
						// vno_perk='41104' or vno_perk='41105' or
						// vno_perk='41301' OR vno_perk='41116'
					} else {
						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan(
								terima.getNoPerk());
						jurnal.getTrioJurnalPK().setIdSub(terima.getNoSub());
						jurnal.setNamaPerkiraan("PPN KELUARAN");
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(NOL);
						jurnal.setKredit(terima.getDebet());
						jurnal.setBlc("*");
						jurnal.setTipe("K");
						listJurnal.add(jurnal);
					}

				} else { // else debet > 0

					if (terima.getKet().equalsIgnoreCase("4")) {

						vNoPerkA = "11120";

						vNoSub1 = terima.getKdPOS();
						if (vNoSub1.length() > 0) {
							vNmPerkA = getMasterFacade()
									.getTrioMstsubperkiraanDao()
									.findNamaSubByIdAndIdSub(vNoPerkA, vNoSub1);
						}

					} else { // else ket = 4
						vNoPerkA = "11101";
						vNmPerkA = "KAS";
					}
					jurnal = new TrioJurnal();
					jurnal.setTglJurnal(terima.getTglTrm());
					jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerkA);
					if (vNoSub1.length() > 0) {
						jurnal.getTrioJurnalPK().setIdSub(vNoSub1);
					} else {
						jurnal.getTrioJurnalPK().setIdSub("");
					}
					jurnal.setNamaPerkiraan(vNmPerkA);
					jurnal.setKet(terima.getNmTran());
					jurnal.setDebet(NOL);
					jurnal.setKredit(terima.getKredit());
					jurnal.setBlc("*");
					jurnal.setTipe("K");
					listJurnal.add(jurnal);

					vNoSub1 = "";

					vNmPerkA = getMasterFacade().getTrioMstperkiraanDao()
							.findNamaById(terima.getNoPerk());

					if (terima.getNoSub().length() > 0) {
						vNmPerkA = getMasterFacade()
								.getTrioMstsubperkiraanDao()
								.findNamaSubByIdAndIdSub(terima.getNoPerk(),
										terima.getNoSub());
					}

					// if vno_perk='41102' or vno_perk='41103' or
					// vno_perk='41104' or vno_perk='41105' OR vno_perk='41116'
					// || beda dengan diatas
					if (terima.getNoPerk().equalsIgnoreCase("41102")
							|| terima.getNoPerk().equalsIgnoreCase("41103")
							|| terima.getNoPerk().equalsIgnoreCase("41104")
							|| terima.getNoPerk().equalsIgnoreCase("41105")
							|| terima.getNoPerk().equalsIgnoreCase("41116")) {

						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan(
								terima.getNoPerk());
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan(vNmPerkA);
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(vjumlah2);
						jurnal.setKredit(NOL);
						jurnal.setBlc("*");
						jurnal.setTipe("D");
						listJurnal.add(jurnal);

						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan("21404");
						jurnal.getTrioJurnalPK().setIdSub("");
						jurnal.setNamaPerkiraan("PPN KELUARAN");
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(vjumlah3);
						jurnal.setKredit(NOL);
						jurnal.setBlc("*");
						jurnal.setTipe("D");
						listJurnal.add(jurnal);

						// else if vno_perk='41102' or vno_perk='41103' or
						// vno_perk='41104' or vno_perk='41105' or
						// vno_perk='41301' OR vno_perk='41116'
					} else {
						jurnal = new TrioJurnal();
						jurnal.setTglJurnal(terima.getTglTrm());
						jurnal.getTrioJurnalPK().setIdPerkiraan(
								terima.getNoPerk());
						jurnal.getTrioJurnalPK().setIdSub(terima.getNoSub());
						jurnal.setNamaPerkiraan("PPN KELUARAN");
						jurnal.setKet(terima.getNmTran());
						jurnal.setDebet(terima.getKredit());
						jurnal.setKredit(NOL);
						jurnal.setBlc("*");
						jurnal.setTipe("D");
						listJurnal.add(jurnal);
					}

				}

				String jenisJurnal = getJenisJurnalByParams(terima.getKet(),
						terima.getDebet(), terima.getKredit(),
						terima.getNoPerk());
				getMasterFacade().getTrioJurnalDao().saveTransaction(
						listJurnal, jenisJurnal, user);

			}

		}

	}

	@Transactional(readOnly = false)
	public void prosesDataUploadLKHCabang2(List<TrioTerima> listTerima,
			String user) {

		List<TrioTerima> listFilter = new ArrayList<TrioTerima>();
		TrioJurnal jurnal = null;
		List<TrioJurnal> listJurnal = null;

		for (TrioTerima terima : listTerima) {
			if ((terima.getTrioTerimaPK().getNoLKH().length() > 0)
					&& (terima.getNmTran().length() > 0)
					&& (terima.getTrioTerimaPK().getNoLKH()
							.equalsIgnoreCase("KWIT.TAG"))) {
				continue;
			}
			if (((terima.getNoPerk().equalsIgnoreCase("11499") && terima
					.getNoSub().equalsIgnoreCase("03")) || (terima.getNoPerk()
					.equalsIgnoreCase("11499") && terima.getNoSub()
					.equalsIgnoreCase("04")))
					&& terima.getKet().equalsIgnoreCase("3")) {
				continue;
			}

			String nmTranSub = terima.getNmTran().substring(0, 10);

			if (nmTranSub.equalsIgnoreCase("saldo awal")) {
				continue;
			}

			String nmTranSub2 = terima.getNmTran();
			if (nmTranSub2.substring(0, 4).equalsIgnoreCase("[KL]")) {
				nmTranSub2 = nmTranSub2.substring(5);
			}
			if (nmTranSub2.substring(0, 5).equalsIgnoreCase("[MNL]")) {
				nmTranSub2 = nmTranSub2.substring(6);
			}
			if (nmTranSub2.substring(0, 6).equalsIgnoreCase("[LOAD]")) {
				nmTranSub2 = nmTranSub2.substring(7);
			}
			terima.setNmTran(nmTranSub2);

			listFilter.add(terima);
		}

		saveList(listFilter, user);

		List<TrioTerima> list = findAll();

		for (TrioTerima terima : list) {
			// vjumlah=debet+kredit
			BigDecimal vjumlah = terima.getDebet().add(terima.getKredit());
			// vjumlah2=vjumlah/1.1
			BigDecimal vjumlah2 = vjumlah.divide(new BigDecimal(1.1), 0,
					RoundingMode.HALF_EVEN);
			// vjumlah3=vjumlah-vjumlah2
			BigDecimal vjumlah3 = vjumlah.subtract(vjumlah2);

			listJurnal = new ArrayList<TrioJurnal>();

			// disini paste
			Date tglTerima = terima.getTglTrm();
			Date tglJTempo = terima.getTglJT();

			String vNoPerk = terima.getNoPerk();
			String vNmTran = terima.getNmTran();
			BigDecimal vKredit = terima.getKredit();
			BigDecimal vDebet = terima.getDebet();
			Date vTglP = terima.getTglTrm();
			String vNoFak = terima.getTrioTerimaPK().getNoFak();
			String vKet = terima.getKet();
			String vKet2 = terima.getKet2();
			String vNoMesin = terima.getNoMesin();
			String vKdBank = terima.getKdBank();
			String vNoSub = terima.getNoSub();
			String vJns = terima.getJns();
			if (vNoPerk.equalsIgnoreCase("11101")) {
				vNoPerk = "11102";
			} else if (vNoPerk.equalsIgnoreCase("11301")) {
				vNoPerk = "11315";
			} else if (vNoPerk.equalsIgnoreCase("11302")) {
				vNoPerk = "11316";
			} else if (vNoPerk.equalsIgnoreCase("11303")) {
				vNoPerk = "11317";
			} else if (vNoPerk.equalsIgnoreCase("11304")) {
				vNoPerk = "11318";
			} else if (vNoPerk.equalsIgnoreCase("11305")) {
				vNoPerk = "11319";
			} else if (vNoPerk.equalsIgnoreCase("11306")) {
				vNoPerk = "11320";
			} else if (vNoPerk.equalsIgnoreCase("11307")) {
				vNoPerk = "11321";
			} else if (vNoPerk.equalsIgnoreCase("11308")) {
				vNoPerk = "11322";
			} else if (vNoPerk.equalsIgnoreCase("11309")) {
				vNoPerk = "11323";
			} else if (vNoPerk.equalsIgnoreCase("11310")) {
				vNoPerk = "11324";
			} else if (vNoPerk.equalsIgnoreCase("11311")) {
				vNoPerk = "11325";
			} else if (vNoPerk.equalsIgnoreCase("11312")) {
				vNoPerk = "11326";
			} else if (vNoPerk.equalsIgnoreCase("11313")) {
				vNoPerk = "11327";
			} else if (vNoPerk.equalsIgnoreCase("11314")) {
				vNoPerk = "11328";
			} else if (vNoPerk.equalsIgnoreCase("11405")) {
				vNoPerk = "11406";
			} else if (vNoPerk.equalsIgnoreCase("11501")) {
				vNoPerk = "11504";
			} else if (vNoPerk.equalsIgnoreCase("11502")) {
				vNoPerk = "11505";
			} else if (vNoPerk.equalsIgnoreCase("11503")) {
				vNoPerk = "11506";
			} else if (vNoPerk.equalsIgnoreCase("11601")) {
				vNoPerk = "11603";
			} else if (vNoPerk.equalsIgnoreCase("11602")) {
				vNoPerk = "11604";
			} else if (vNoPerk.equalsIgnoreCase("21101")) {
				vNoPerk = "21108";
			} else if (vNoPerk.equalsIgnoreCase("21102")) {
				vNoPerk = "21109";
			} else if (vNoPerk.equalsIgnoreCase("21103")) {
				vNoPerk = "21110";
			} else if (vNoPerk.equalsIgnoreCase("21105")) {
				vNoPerk = "21111";
			} else if (vNoPerk.equalsIgnoreCase("21201")) {
				vNoPerk = "21210";
			} else if (vNoPerk.equalsIgnoreCase("21202")) {
				vNoPerk = "21211";
			} else if (vNoPerk.equalsIgnoreCase("21203")) {
				vNoPerk = "21212";
			} else if (vNoPerk.equalsIgnoreCase("21204")) {
				vNoPerk = "21213";
			} else if (vNoPerk.equalsIgnoreCase("21205")) {
				vNoPerk = "21214";
			} else if (vNoPerk.equalsIgnoreCase("21206")) {
				vNoPerk = "21215";
			} else if (vNoPerk.equalsIgnoreCase("21207")) {
				vNoPerk = "21216";
			} else if (vNoPerk.equalsIgnoreCase("21208")) {
				vNoPerk = "21217";
			} else if (vNoPerk.equalsIgnoreCase("21209")) {
				vNoPerk = "21218";
			} else if (vNoPerk.equalsIgnoreCase("41101")) {
				vNoPerk = "41106";
			} else if (vNoPerk.equalsIgnoreCase("41102")) {
				vNoPerk = "41107";
			} else if (vNoPerk.equalsIgnoreCase("41103")) {
				vNoPerk = "41108";
			} else if (vNoPerk.equalsIgnoreCase("41104")) {
				vNoPerk = "41109";
			} else if (vNoPerk.equalsIgnoreCase("41105")) {
				vNoPerk = "41110";
			} else if (vNoPerk.equalsIgnoreCase("41201")) {
				vNoPerk = "41204";
			} else if (vNoPerk.equalsIgnoreCase("41202")) {
				vNoPerk = "41205";
			} else if (vNoPerk.equalsIgnoreCase("41203")) {
				vNoPerk = "41206";
			} else if (vNoPerk.equalsIgnoreCase("41301")) {
				vNoPerk = "41303";
			} else if (vNoPerk.equalsIgnoreCase("41302")) {
				vNoPerk = "41304";
			} else if (vNoPerk.equalsIgnoreCase("42101")) {
				vNoPerk = "42105";
			} else if (vNoPerk.equalsIgnoreCase("42102")) {
				vNoPerk = "42106";
			} else if (vNoPerk.equalsIgnoreCase("41307")) {
				vNoPerk = "21229";
			} else if (vNoPerk.equalsIgnoreCase("42104")) {
				vNoPerk = "42108";
			} else if (vNoPerk.equalsIgnoreCase("51101")) {
				vNoPerk = "51107";
			} else if (vNoPerk.equalsIgnoreCase("51102")) {
				vNoPerk = "51108";
			} else if (vNoPerk.equalsIgnoreCase("51103")) {
				vNoPerk = "51109";
			} else if (vNoPerk.equalsIgnoreCase("51104")) {
				vNoPerk = "51110";
			} else if (vNoPerk.equalsIgnoreCase("51105")) {
				vNoPerk = "51111";
			} else if (vNoPerk.equalsIgnoreCase("51106")) {
				vNoPerk = "51112";
			} else if (vNoPerk.equalsIgnoreCase("61100")) {
				vNoPerk = "61200";
			} else if (vNoPerk.equalsIgnoreCase("61101")) {
				vNoPerk = "61201";
			} else if (vNoPerk.equalsIgnoreCase("61102")) {
				vNoPerk = "61202";
			} else if (vNoPerk.equalsIgnoreCase("61103")) {
				vNoPerk = "61203";
			} else if (vNoPerk.equalsIgnoreCase("61104")) {
				vNoPerk = "61204";
			} else if (vNoPerk.equalsIgnoreCase("61105")) {
				vNoPerk = "61205";
			} else if (vNoPerk.equalsIgnoreCase("61106")) {
				vNoPerk = "61206";
			} else if (vNoPerk.equalsIgnoreCase("61107")) {
				vNoPerk = "61207";
			} else if (vNoPerk.equalsIgnoreCase("61108")) {
				vNoPerk = "61208";
			} else if (vNoPerk.equalsIgnoreCase("61109")) {
				vNoPerk = "61209";
			} else if (vNoPerk.equalsIgnoreCase("61110")) {
				vNoPerk = "61210";
			} else if (vNoPerk.equalsIgnoreCase("61111")) {
				vNoPerk = "61211";
			} else if (vNoPerk.equalsIgnoreCase("61112")) {
				vNoPerk = "61212";
			} else if (vNoPerk.equalsIgnoreCase("61113")) {
				vNoPerk = "61213";
			} else if (vNoPerk.equalsIgnoreCase("61114")) {
				vNoPerk = "61214";
			} else if (vNoPerk.equalsIgnoreCase("61115")) {
				vNoPerk = "61215";
			} else if (vNoPerk.equalsIgnoreCase("61116")) {
				vNoPerk = "61216";
			} else if (vNoPerk.equalsIgnoreCase("61117")) {
				vNoPerk = "61217";
			} else if (vNoPerk.equalsIgnoreCase("61118")) {
				vNoPerk = "61218";
			} else if (vNoPerk.equalsIgnoreCase("61119")) {
				vNoPerk = "61219";
			} else if (vNoPerk.equalsIgnoreCase("61120")) {
				vNoPerk = "61220";
			} else if (vNoPerk.equalsIgnoreCase("61121")) {
				vNoPerk = "61221";
			} else if (vNoPerk.equalsIgnoreCase("61122")) {
				vNoPerk = "61222";
			} else if (vNoPerk.equalsIgnoreCase("61123")) {
				vNoPerk = "61223";
			} else if (vNoPerk.equalsIgnoreCase("61124")) {
				vNoPerk = "61224";
			} else if (vNoPerk.equalsIgnoreCase("61125")) {
				vNoPerk = "61225";
			} else if (vNoPerk.equalsIgnoreCase("61126")) {
				vNoPerk = "61226";
			} else if (vNoPerk.equalsIgnoreCase("61127")) {
				vNoPerk = "61227";
			} else if (vNoPerk.equalsIgnoreCase("61128")) {
				vNoPerk = "61228";
			} else if (vNoPerk.equalsIgnoreCase("61129")) {
				vNoPerk = "61229";
			} else if (vNoPerk.equalsIgnoreCase("61130")) {
				vNoPerk = "61230";
			} else if (vNoPerk.equalsIgnoreCase("61199")) {
				vNoPerk = "61299";
			} else if (vNoPerk.equalsIgnoreCase("11699")) {
				vNoPerk = "11698";
			} else if (vNoPerk.equalsIgnoreCase("21299")) {
				vNoPerk = "21298";
			} else if (vNoPerk.equalsIgnoreCase("42199")) {
				vNoPerk = "42198";
			} else if (vNoPerk.equalsIgnoreCase("11498")) {
				vNoPerk = "11497";
			} else if (vNoPerk.equalsIgnoreCase("21116")) {
				vNoPerk = "21117";
			} else if (vNoPerk.equalsIgnoreCase("41116")) {
				vNoPerk = "41117";
			} else if (vNoPerk.equalsIgnoreCase("61131")) {
				vNoPerk = "61231";
			} else if (vNoPerk.equalsIgnoreCase("71101")) {
				vNoPerk = "71201";
			} else if (vNoPerk.equalsIgnoreCase("71102")) {
				vNoPerk = "71202";
			} else if (vNoPerk.equalsIgnoreCase("71103")) {
				vNoPerk = "71203";
			} else if (vNoPerk.equalsIgnoreCase("71199")) {
				vNoPerk = "71299";
			} else if (vNoPerk.equalsIgnoreCase("81101")) {
				vNoPerk = "81201";
			} else if (vNoPerk.equalsIgnoreCase("81189")) {
				vNoPerk = "81289";
			} else if (vNoPerk.equalsIgnoreCase("81199")) {
				vNoPerk = "81299";
			} else if (vNoPerk.equalsIgnoreCase("41116")) {
				vNoPerk = "41117";
			}

			//copy paste caseif disini
			// CASE vno_perk='21309' AND LOWER(LEFT(vnm_tran,20))<>'pembayaran
						// utk dinas'
						if (terima.getNoPerk().equalsIgnoreCase("21309")
								&& (terima.getNmTran().substring(0, 20)).toLowerCase()
										.equalsIgnoreCase("pembayaran utk dinas")) {

							System.out.println("CASEIFCAB2 1");
							// TODO TO DO membaca data jurnal
							// jika kredit > 0
							if (terima.getKredit().compareTo(NOL) == 1) {
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan("11102"); // 11101
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan("KAS");
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getKredit());
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								listJurnal.add(jurnal);

							} else {
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan("11102"); //11101
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan("KAS");
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getDebet());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								listJurnal.add(jurnal);

							}

							String jenisJurnal = getJenisJurnalByParams(terima.getKet(),
									terima.getDebet(), terima.getKredit(),
									terima.getNoPerk());
							getMasterFacade().getTrioJurnalDao().saveTransaction(
									listJurnal, jenisJurnal, user);
						}

						// CASE vno_perk='41116' AND vket='X'
						else if (terima.getNoPerk().equalsIgnoreCase("41116")
								&& terima.getKet().equalsIgnoreCase("X")) {

							System.out.println("CASEIFCAB2 2");

							String vNoPerk0 = null;
							String vNmPerk0 = null;
							// jika debet > 0
							if (terima.getDebet().compareTo(NOL) == 1) {
								String[] arrStr = getNoPerkAndNmPerkForCab2(terima.getKet2());
								vNoPerk0 = arrStr[0];
								vNmPerk0 = arrStr[0];

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk0);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk0);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getDebet());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
								listJurnal.add(jurnal);

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(terima.getNoPerk());
								jurnal.getTrioJurnalPK().setIdSub("");

								String nmPerk = getMasterFacade().getTrioMstperkiraanDao()
										.findNamaById(terima.getNoPerk());
								jurnal.setNamaPerkiraan(nmPerk);

								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(vjumlah2);
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								listJurnal.add(jurnal);

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan("21404");
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan("PPN KELUARAN");
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(vjumlah3);
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								listJurnal.add(jurnal);

							} else { // jika debet < 0

								String[] arrStr = getNoPerkAndNmPerkForCab2(terima.getKet2());
								vNoPerk0 = arrStr[0];
								vNmPerk0 = arrStr[0];

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk0);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk0);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getKredit());
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								listJurnal.add(jurnal);

								String nmPerk = getMasterFacade().getTrioMstperkiraanDao()
										.findNamaById(terima.getNoPerk());
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(terima.getNoPerk());
								jurnal.setNamaPerkiraan(nmPerk);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(vjumlah2);
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								listJurnal.add(jurnal);

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan("21404");
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan("PPN KELUARAN");
								jurnal.setDebet(vjumlah3);
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								listJurnal.add(jurnal);

							}

							String jenisJurnal = getJenisJurnalByParams(terima.getKet(),
									terima.getDebet(), terima.getKredit(),
									terima.getNoPerk());
							getMasterFacade().getTrioJurnalDao().saveTransaction(
									listJurnal, jenisJurnal, user);
							// CASE vno_perk='11313' AND (vket='2' OR vket='3') AND
							// ((ALLTRIM(STR(YEAR(vtgl_trm)))+ALLTRIM(STR(MONTH(vtgl_trm))))=(ALLTRIM(STR(YEAR(vtgl_jt)))+ALLTRIM(STR(MONTH(vtgl_jt)))))
							// AND ALLTRIM(thisform.text3.value)<>'T10' &&11327 11341
						} else if (terima.getNoPerk().equalsIgnoreCase("11313")
								&& (terima.getKet().equalsIgnoreCase("2") || terima
										.getKet().equalsIgnoreCase("3"))
								&& ((TrioDateConv.format(terima.getTglTrm(), "yyyyMM"))
										.equalsIgnoreCase(TrioDateConv.format(
												terima.getTglJT(), "yyyyMM")))) {

							System.out.println("CASEIFCAB2 3");

							Date tglJt = terima.getTglJT();
							// jika debet > 0
							if (terima.getDebet().compareTo(NOL) == 1) {

								if (terima.getKet().equalsIgnoreCase("1")
										|| terima.getKet().equalsIgnoreCase("K")) {
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(tglJt);
									jurnal.getTrioJurnalPK().setIdPerkiraan("11102"); //11101
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan("KAS");
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(terima.getDebet());
									jurnal.setKredit(NOL);
									jurnal.setBlc("*");
									jurnal.setTipe("D");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);

								}
								if (terima.getKet().equalsIgnoreCase("2")) {
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(tglJt);
									jurnal.getTrioJurnalPK().setIdPerkiraan("11203");
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan("CEK");
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(terima.getDebet());
									jurnal.setKredit(NOL);
									jurnal.setBlc("*");
									jurnal.setTipe("D");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);

								}
								if (terima.getKet().equalsIgnoreCase("3")) {

									String vnoPerk = "11202";
									String vnoSub = terima.getKdBank();

									String vnmSub = getMasterFacade()
											.getTrioMstsubperkiraanDao()
											.findNamaSubByIdAndIdSub(vnoPerk, vnoSub);

									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(tglJt);
									jurnal.getTrioJurnalPK().setIdPerkiraan(vnoPerk);
									jurnal.getTrioJurnalPK().setIdSub(vnoSub);
									jurnal.setNamaPerkiraan(vnmSub);
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(terima.getDebet());
									jurnal.setKredit(NOL);
									jurnal.setBlc("*");
									jurnal.setTipe("D");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);
								}

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(tglJt);
								jurnal.getTrioJurnalPK().setIdPerkiraan("11313");
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan("PIUTANG PENJUALAN");
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getDebet());
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);

							} else { // jika debet < 0
								if (terima.getKet().equalsIgnoreCase("1")
										|| terima.getKet().equalsIgnoreCase("K")) {
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(tglJt);
									jurnal.getTrioJurnalPK().setIdPerkiraan("11102"); //11101
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan("KAS");
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(NOL);
									jurnal.setKredit(terima.getKredit());
									jurnal.setBlc("*");
									jurnal.setTipe("K");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);
								}
								if (terima.getKet().equalsIgnoreCase("2")) {
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(tglJt);
									jurnal.getTrioJurnalPK().setIdPerkiraan("11203");
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan("KAS");
									jurnal.setDebet(NOL);
									jurnal.setKredit(terima.getKredit());
									jurnal.setBlc("*");
									jurnal.setTipe("K");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);

								}
								if (terima.getKet().equalsIgnoreCase("3")) {
									String vnoPerk = "11202";
									String vnoSub = terima.getKdBank();

									String vnmSub = getMasterFacade()
											.getTrioMstsubperkiraanDao()
											.findNamaSubByIdAndIdSub(vnoPerk, vnoSub);

									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(tglJt);
									jurnal.getTrioJurnalPK().setIdPerkiraan(vnoPerk);
									jurnal.getTrioJurnalPK().setIdSub(vnoSub);
									jurnal.setNamaPerkiraan(vnmSub);
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(NOL);
									jurnal.setKredit(terima.getKredit());
									jurnal.setBlc("*");
									jurnal.setTipe("K");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);
								}

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(tglJt);
								jurnal.getTrioJurnalPK().setIdPerkiraan("11313");
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan("PIUTANG PENJUALAN");
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getKredit());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);
								String jenisJurnal = getJenisJurnalByParams(
										terima.getKet(), terima.getDebet(),
										terima.getKredit(), terima.getNoPerk());
								getMasterFacade().getTrioJurnalDao().saveTransaction(
										listJurnal, jenisJurnal, user);

							}
							// CASE vno_perk='11499' and vket='3' AND (vno_sub='02' OR
							// vno_sub='03' OR vno_sub='04') AND vjns='XXX' AND
							// ALLTRIM(master.kd_prs)<>'T10'
						} else if (terima.getNoPerk().equalsIgnoreCase("11499")
								&& terima.getKet().equalsIgnoreCase("3")
								&& (terima.getNoSub().equalsIgnoreCase("02")
										|| terima.getNoSub().equalsIgnoreCase("03") || terima
										.getNoSub().equalsIgnoreCase("04"))
								&& terima.getJns().equalsIgnoreCase("XXX")) {

							System.out.println("CASEIFCAB2 4");

							if (terima.getDebet().compareTo(NOL) == 1) {

								String nmPerk = getMasterFacade().getTrioMstperkiraanDao()
										.findNamaById(terima.getNoPerk());

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan("21299");
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(nmPerk);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getDebet());
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								listJurnal.add(jurnal);

								String noSub = terima.getNoSub();
								String nmSub = getMasterFacade()
										.getTrioMstsubperkiraanDao()
										.findNamaSubByIdAndIdSub(terima.getNoPerk(), noSub);
								if (nmSub == null) {
									nmSub = "";
									noSub = "";
								}

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan("11499");
								jurnal.getTrioJurnalPK().setIdPerkiraan(noSub);
								jurnal.setNamaPerkiraan(nmSub);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getDebet());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								listJurnal.add(jurnal);

							} else {

								String nmPerk = getMasterFacade().getTrioMstperkiraanDao()
										.findNamaById(terima.getNoPerk());

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan("21299");
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(nmPerk);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getKredit());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								listJurnal.add(jurnal);

								String noSub = terima.getNoSub();
								String nmSub = getMasterFacade()
										.getTrioMstsubperkiraanDao()
										.findNamaSubByIdAndIdSub(terima.getNoPerk(), noSub);
								if (nmSub == null) {
									nmSub = "";
									noSub = "";
								}
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan("11499");
								jurnal.getTrioJurnalPK().setIdPerkiraan(noSub);
								jurnal.setNamaPerkiraan(nmSub);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getKredit());
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								listJurnal.add(jurnal);
								String jenisJurnal = getJenisJurnalByParams(
										terima.getKet(), terima.getDebet(),
										terima.getKredit(), terima.getNoPerk());
								getMasterFacade().getTrioJurnalDao().saveTransaction(
										listJurnal, jenisJurnal, user);
							}
							// CASE vno_perk='11499' and vket='3' AND (vno_sub='02' OR
							// vno_sub='03' OR vno_sub='04') AND vjns<>'XXX' AND
							// ALLTRIM(master.kd_prs)<>'T10'
						} else if (terima.getNoPerk().equalsIgnoreCase("11499")
								&& terima.getKet().equalsIgnoreCase("3")
								&& (terima.getNoSub().equalsIgnoreCase("02")
										|| terima.getNoSub().equalsIgnoreCase("03") || terima
										.getNoSub().equalsIgnoreCase("04"))
								&& (!terima.getJns().equalsIgnoreCase("XXX"))) {

							System.out.println("CASEIFCAB2 5");

							// jika debet > 0
							if (terima.getDebet().compareTo(NOL) == 1) {

								String nmPerk = getMasterFacade().getTrioMstperkiraanDao()
										.findNamaById(terima.getNoPerk());

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan("21299");
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(nmPerk);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getDebet());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								listJurnal.add(jurnal);

								String noSub = terima.getNoSub();
								String nmSub = getMasterFacade()
										.getTrioMstsubperkiraanDao()
										.findNamaSubByIdAndIdSub(terima.getNoPerk(), noSub);
								if (nmSub == null) {
									nmSub = "";
									noSub = "";
								}

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan("11499");
								jurnal.getTrioJurnalPK().setIdPerkiraan(noSub);
								jurnal.setNamaPerkiraan(nmSub);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getDebet());
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								listJurnal.add(jurnal);

							} else {
								String nmPerk = getMasterFacade().getTrioMstperkiraanDao()
										.findNamaById(terima.getNoPerk());

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan("21299");
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(nmPerk);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getKredit());
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								listJurnal.add(jurnal);

								String noSub = terima.getNoSub();
								String nmSub = getMasterFacade()
										.getTrioMstsubperkiraanDao()
										.findNamaSubByIdAndIdSub(terima.getNoPerk(), noSub);
								if (nmSub == null) {
									nmSub = "";
									noSub = "";
								}

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan("11499");
								jurnal.getTrioJurnalPK().setIdPerkiraan(noSub);
								jurnal.setNamaPerkiraan(nmSub);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getKredit());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								listJurnal.add(jurnal);
								String jenisJurnal = getJenisJurnalByParams(
										terima.getKet(), terima.getDebet(),
										terima.getKredit(), terima.getNoPerk());
								getMasterFacade().getTrioJurnalDao().saveTransaction(
										listJurnal, jenisJurnal, user);
							}
							// CASE vno_perk='11499' and (vket='1' OR vket='K') AND
							// (vno_sub='02') AND ALLTRIM(master.kd_prs)<>'T10'
						} else if (terima.getNoPerk().equalsIgnoreCase("11499")
								&& (terima.getKet().equalsIgnoreCase("1") || terima
										.getKet().equalsIgnoreCase("K"))
								&& terima.getNoSub().equalsIgnoreCase("02")) {

							System.out.println("CASEIFCAB2 6");

							// jika debet > 0
							if (terima.getDebet().compareTo(NOL) == 1) {
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan("11102"); //11101
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan("KAS");
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getDebet());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								listJurnal.add(jurnal);

								String nmPerk = getMasterFacade().getTrioMstperkiraanDao()
										.findNamaById("11497"); //11498

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan("11497"); //11498
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(nmPerk);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getDebet());
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								listJurnal.add(jurnal);

							} else {
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan("11102"); //11101
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan("KAS");
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getDebet());
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								listJurnal.add(jurnal);

								String nmPerk = getMasterFacade().getTrioMstperkiraanDao()
										.findNamaById("11497"); //11498
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan("11497"); //11498
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(nmPerk);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getKredit());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								listJurnal.add(jurnal);
								String jenisJurnal = getJenisJurnalByParams(
										terima.getKet(), terima.getDebet(),
										terima.getKredit(), terima.getNoPerk());
								getMasterFacade().getTrioJurnalDao().saveTransaction(
										listJurnal, jenisJurnal, user);

							}
							// CASE vno_perk='11499' and (vket='1' OR vket='K') AND (vno_sub='03' OR vno_sub='04')
						} else if (terima.getNoPerk().equalsIgnoreCase("11499")
								&& (terima.getKet().equalsIgnoreCase("1") || terima
										.getKet().equalsIgnoreCase("K"))
								&& (terima.getNoSub().equalsIgnoreCase("03") || terima
										.getNoSub().equalsIgnoreCase("04"))) {

							System.out.println("CASEIFCAB2 7");

							// jika debet > 0
							if (terima.getDebet().compareTo(NOL) == 1) {
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan("11102"); //11101
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan("KAS");
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getDebet());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								listJurnal.add(jurnal);

								String idPerk = "61106";
								String idSub = "01";
								String nmSub = getMasterFacade()
										.getTrioMstsubperkiraanDao()
										.findNamaSubByIdAndIdSub(idPerk, idSub);
								if (nmSub == null) {
									nmSub = "";
								}
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(idPerk);
								jurnal.getTrioJurnalPK().setIdSub(idSub);
								jurnal.setNamaPerkiraan(nmSub);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getDebet());
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								listJurnal.add(jurnal);

							} else {
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan("11102"); //11101
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan("KAS");
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getKredit());
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								listJurnal.add(jurnal);

								String idPerk = "61106";
								String idSub = "01";
								String nmSub = getMasterFacade()
										.getTrioMstsubperkiraanDao()
										.findNamaSubByIdAndIdSub(idPerk, idSub);
								if (nmSub == null) {
									nmSub = "";
								}
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(idPerk);
								jurnal.getTrioJurnalPK().setIdSub(idSub);
								jurnal.setNamaPerkiraan(nmSub);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getKredit());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								listJurnal.add(jurnal);
								String jenisJurnal = getJenisJurnalByParams(
										terima.getKet(), terima.getDebet(),
										terima.getKredit(), terima.getNoPerk());
								getMasterFacade().getTrioJurnalDao().saveTransaction(
										listJurnal, jenisJurnal, user);

							}
							// case (vno_perk='SUBS' OR vno_perk='CB')&& and (vket='3')
							
						} else if (terima.getNoPerk().equalsIgnoreCase("SUBS")
								|| terima.getNoPerk().equalsIgnoreCase("CB")) {
							if (terima.getKredit().compareTo(NOL) == 1) {

								System.out.println("CASEIFCAB2 8");

								if (terima.getKet2().equalsIgnoreCase("ADR")
										|| terima.getKet2().equalsIgnoreCase("ADS")
										|| terima.getKet2().equalsIgnoreCase("OTO")
										|| terima.getKet2().equalsIgnoreCase("WOM")) {
									if (terima.getsAHM().compareTo(NOL) == 1) {
										jurnal = new TrioJurnal();
										jurnal.setTglJurnal(terima.getTglTrm());
										jurnal.getTrioJurnalPK().setIdPerkiraan("41204"); //41201
										jurnal.getTrioJurnalPK().setIdSub("");
										jurnal.setNamaPerkiraan("POTONGAN PENJUALAN AHM");
										jurnal.setKet(terima.getNmTran());
										jurnal.setDebet(terima.getsAHM());
										jurnal.setKredit(NOL);
										jurnal.setBlc("*");
										jurnal.setTipe("D");
										listJurnal.add(jurnal);
									}

									if (terima.getsMD().compareTo(NOL) == 1) {
										jurnal = new TrioJurnal();
										jurnal.setTglJurnal(terima.getTglTrm());
										jurnal.getTrioJurnalPK().setIdPerkiraan("41204"); //41201
										jurnal.getTrioJurnalPK().setIdSub("");
										jurnal.setNamaPerkiraan("POTONGAN PENJUALAN MD");
										jurnal.setKet(terima.getNmTran());
										jurnal.setDebet(terima.getsMD());
										jurnal.setKredit(NOL);
										jurnal.setBlc("*");
										jurnal.setTipe("D");
										listJurnal.add(jurnal);
									}

									if (terima.getsSD().compareTo(NOL) == 1) {
										jurnal = new TrioJurnal();
										jurnal.setTglJurnal(terima.getTglTrm());
										jurnal.getTrioJurnalPK().setIdPerkiraan("41204"); //41201
										jurnal.getTrioJurnalPK().setIdSub("");
										jurnal.setNamaPerkiraan("POTONGAN PENJUALAN DEALER");
										jurnal.setKet(terima.getNmTran());
										jurnal.setDebet(terima.getsMD());
										jurnal.setKredit(NOL);
										jurnal.setBlc("*");
										jurnal.setTipe("D");
										listJurnal.add(jurnal);
									}

									if (terima.getsFinance().compareTo(NOL) == 1) {

										if (terima.getKet2().equalsIgnoreCase("ADR")
												|| terima.getKet2().equalsIgnoreCase("ADS")
												|| terima.getKet2().equalsIgnoreCase("OTO")) {
											jurnal = new TrioJurnal();
											jurnal.setTglJurnal(terima.getTglTrm());
											jurnal.getTrioJurnalPK()
													.setIdPerkiraan("61206"); //61106
											jurnal.getTrioJurnalPK().setIdSub("03");
											jurnal.setNamaPerkiraan("VOUCHER PENJUALAN");
											jurnal.setKet(terima.getNmTran());
											jurnal.setDebet(terima.getKredit());
											jurnal.setKredit(NOL);
											jurnal.setBlc("*");
											jurnal.setTipe("D");
											listJurnal.add(jurnal);
										}

										if (terima.getKet2().equalsIgnoreCase("WOM")) {
											jurnal = new TrioJurnal();
											jurnal.setTglJurnal(terima.getTglTrm());
											jurnal.getTrioJurnalPK()
													.setIdPerkiraan("61206"); //61106
											jurnal.getTrioJurnalPK().setIdSub("04");
											jurnal.setNamaPerkiraan("VOUCHER PENJUALAN");
											jurnal.setKet(terima.getNmTran());
											jurnal.setDebet(terima.getKredit());
											jurnal.setKredit(NOL);
											jurnal.setBlc("*");
											jurnal.setTipe("D");
											listJurnal.add(jurnal);
										}
										String vNoPerk0 = null;
										String vNmPerk0 = null;

										String[] arrStr = getNoPerkAndNmPerkForCab2(terima
												.getKet2());
										vNoPerk0 = arrStr[0];
										vNmPerk0 = arrStr[0];

										// set Debet
										BigDecimal tempDebet = NOL;

										if (terima.getKet2().equalsIgnoreCase("ADR")
												|| terima.getKet2().equalsIgnoreCase("ADS")) {
											tempDebet = terima.getsFinance().add(
													terima.getsFinance().multiply(
															new BigDecimal(0.1)));
										} else {
											if (terima.getKet2().equalsIgnoreCase("WOM")) {
												tempDebet = new BigDecimal(1100000);
											} else {
												if (terima.getKet2()
														.equalsIgnoreCase("OTO")) {
													//SELECT * FROM terima WHERE no_fak=vno_fak AND no_perk='P3' INTO CURSOR vq1
													TrioTerima tt = getTerimaByNoFakAndNoPerk(terima.getTrioTerimaPK().getNoFak(), "P3");
													tempDebet = tt.getsFinance();
													
												} else {
													tempDebet = terima.getsFinance();
												}
											}
										}

										jurnal = new TrioJurnal();
										jurnal.setTglJurnal(terima.getTglTrm());
										jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk0);
										jurnal.getTrioJurnalPK().setIdSub("");
										jurnal.setNamaPerkiraan(vNmPerk0);
										jurnal.setKet(terima.getNmTran());
										jurnal.setDebet(tempDebet);
										jurnal.setKredit(NOL);
										jurnal.setBlc("*");
										jurnal.setTipe("D");
										listJurnal.add(jurnal);

										String vNoSub71204 = "";

										if (terima.getKet2().equalsIgnoreCase("ADR")) {
											vNoSub71204 = "01";
										} else if (terima.getKet2().equalsIgnoreCase("ADS")) {
											vNoSub71204 = "02";
										} else if (terima.getKet2().equalsIgnoreCase("OTO")) {
											vNoSub71204 = "03";
										} else if (terima.getKet2().equalsIgnoreCase("WOM")) {
											vNoSub71204 = "04";
										}
										String vNmSub71204 = getMasterFacade()
												.getTrioMstsubperkiraanDao()
												.findNamaSubByIdAndIdSub("71204",
														vNoSub71204);

										jurnal = new TrioJurnal();
										jurnal.setTglJurnal(terima.getTglTrm());
										jurnal.getTrioJurnalPK().setIdPerkiraan("71204"); //71104 
										jurnal.getTrioJurnalPK().setIdSub(vNoSub71204);
										jurnal.setNamaPerkiraan(vNmSub71204);
										jurnal.setKet(terima.getNmTran());

										if (terima.getKet2().equalsIgnoreCase("ADR")
												|| terima.getKet2().equalsIgnoreCase("ADS")) {
											tempDebet = terima.getsFinance();
										} else {
											if (terima.getKet2().equalsIgnoreCase("WOM")) {
												tempDebet = new BigDecimal(1000000);
											} else {
												if (terima.getKet2()
														.equalsIgnoreCase("OTO")) {
													tempDebet = tempDebet
															.multiply(new BigDecimal(10)
																	.divide(new BigDecimal(
																			11),
																			0,
																			RoundingMode.HALF_EVEN));
												} else {
													tempDebet = terima
															.getsFinance()
															.multiply(
																	new BigDecimal(10)
																			.divide(new BigDecimal(
																					11),
																					0,
																					RoundingMode.HALF_EVEN));
												}
											}
										}

										jurnal.setDebet(tempDebet);
										jurnal.setKredit(NOL);
										jurnal.setBlc("*");
										jurnal.setTipe("D");
										listJurnal.add(jurnal);

										String vNmPerk = getMasterFacade()
												.getTrioMstperkiraanDao().findNamaById(
														"21404");

										jurnal = new TrioJurnal();
										jurnal.setTglJurnal(terima.getTglTrm());
										jurnal.getTrioJurnalPK().setIdPerkiraan("21404");
										jurnal.getTrioJurnalPK().setIdSub("");
										jurnal.setNamaPerkiraan(vNmPerk);
										jurnal.setKet(terima.getNmTran());

										if (terima.getKet2().equalsIgnoreCase("ADS")
												|| terima.getKet2().equalsIgnoreCase("ADS")) {
											tempDebet = terima.getsFinance().multiply(
													new BigDecimal(0.1));
										} else {
											if (terima.getKet2().equalsIgnoreCase("WOM")) {
												tempDebet = new BigDecimal(100000);
											} else {
												if (terima.getKet2()
														.equalsIgnoreCase("OTO")) {
													tempDebet = tempDebet
															.multiply(new BigDecimal(10)
																	.divide(new BigDecimal(
																			11),
																			0,
																			RoundingMode.HALF_EVEN));
												} else {
													tempDebet = terima
															.getsFinance()
															.multiply(
																	new BigDecimal(10)
																			.divide(new BigDecimal(
																					11),
																					0,
																					RoundingMode.HALF_EVEN));
												}
											}
										}
										jurnal.setDebet(tempDebet);
										jurnal.setKredit(NOL);
										jurnal.setBlc("*");
										jurnal.setTipe("D");
										listJurnal.add(jurnal);

									}

									if (terima.getKet().equalsIgnoreCase("3")) {
										jurnal = new TrioJurnal();
										jurnal.setTglJurnal(terima.getTglTrm());
										jurnal.getTrioJurnalPK().setIdPerkiraan("11202");
										jurnal.getTrioJurnalPK().setIdSub(
												terima.getKdBank());
										String vNmSub = getMasterFacade()
												.getTrioMstsubperkiraanDao()
												.findNamaSubByIdAndIdSub("11202",
														terima.getKdBank());
										jurnal.setNamaPerkiraan(vNmSub);
										jurnal.setKet(terima.getNmTran());
										jurnal.setDebet(NOL);
										jurnal.setKredit(terima.getKredit());
										jurnal.setBlc("*");
										jurnal.setTipe("K");
										listJurnal.add(jurnal);
									}
									if (terima.getKet().equalsIgnoreCase("1")
											|| terima.getKet().equalsIgnoreCase("K")) {
										jurnal = new TrioJurnal();
										jurnal.setTglJurnal(terima.getTglTrm());
										jurnal.getTrioJurnalPK().setIdPerkiraan("11102"); //11101
										jurnal.getTrioJurnalPK().setIdSub("");
										jurnal.setNamaPerkiraan("KAS");
										jurnal.setKet(terima.getNmTran());
										jurnal.setDebet(NOL);
										jurnal.setKredit(terima.getKredit());
										jurnal.setBlc("*");
										jurnal.setTipe("K");
										listJurnal.add(jurnal);
									}
								} else {
									if (terima.getKet2().length() == 0) {
										//untuk cabang 1 diskip
//										BigDecimal ahmPlusMd = terima.getsAHM().add(terima.getsMD());
//										if (ahmPlusMd.compareTo(NOL) == 1 && ( ! terima.getKet().equalsIgnoreCase("SMF")) && 
//												( ! terima.getKet().equalsIgnoreCase("MMF")) && terima.getNoPerk2().equalsIgnoreCase("G")){
//											
//											String vNoSubb = getMasterFacade().getTrioMstsubperkiraanDao().findNamaSubByIdAndIdSub("11326", "03");
//											
//										}
										
										if (terima.getsMD().compareTo(NOL) == 1 && (terima.getKet2().equalsIgnoreCase("SMF") || terima.getKet2().equalsIgnoreCase("MMF")) ){
											String vNoSub111 = "";
											String vNmSub111 = "";
											if (terima.getNoPerk2().equalsIgnoreCase("G")){
												vNoSub111 = "03";
												vNmSub111 = getMasterFacade().getTrioMstsubperkiraanDao().findNamaSubByIdAndIdSub("11326", "03");
											} else {
												vNoSub111 = "02";
												vNmSub111 = getMasterFacade().getTrioMstsubperkiraanDao().findNamaSubByIdAndIdSub("11326", "02");
											}
											
											jurnal = new TrioJurnal();
											jurnal.setTglJurnal(terima.getTglTrm());
											jurnal.getTrioJurnalPK().setIdPerkiraan("11326");
											jurnal.getTrioJurnalPK().setIdSub(vNoSub111);
											jurnal.setNamaPerkiraan(vNmSub111);
											jurnal.setKet(terima.getNmTran());
											jurnal.setDebet(terima.getsMD());
											jurnal.setKredit(NOL);
											jurnal.setBlc("*");
											jurnal.setTipe("D");
											jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
											jurnal.setNoMesin(terima.getNoMesin());
											listJurnal.add(jurnal);
											
										}

										if (terima.getsAHM().compareTo(NOL) == 1
												&& terima.getKet2().length() == 0) {
											jurnal = new TrioJurnal();
											jurnal.setTglJurnal(terima.getTglTrm());
											jurnal.getTrioJurnalPK()
													.setIdPerkiraan("41204"); //41201
											jurnal.getTrioJurnalPK().setIdSub("");
											jurnal.setNamaPerkiraan("POTONGAN PENJUALAN AHM");
											jurnal.setKet(terima.getNmTran());
											jurnal.setDebet(terima.getsAHM());
											jurnal.setKredit(NOL);
											jurnal.setBlc("*");
											jurnal.setTipe("D");
											jurnal.setNoReff(terima.getTrioTerimaPK()
													.getNoFak());
											jurnal.setNoMesin(terima.getNoMesin());
											listJurnal.add(jurnal);
										}

										if (terima.getsMD().compareTo(NOL) == 1
												&& terima.getKet2().length() == 0) {
											jurnal = new TrioJurnal();
											jurnal.setTglJurnal(terima.getTglTrm());
											jurnal.getTrioJurnalPK()
													.setIdPerkiraan("41204"); //41201
											jurnal.getTrioJurnalPK().setIdSub("");
											jurnal.setNamaPerkiraan("POTONGAN PENJUALAN MD");
											jurnal.setKet(terima.getNmTran());
											jurnal.setDebet(terima.getsMD());
											jurnal.setKredit(NOL);
											jurnal.setBlc("*");
											jurnal.setTipe("D");
											jurnal.setNoReff(terima.getTrioTerimaPK()
													.getNoFak());
											jurnal.setNoMesin(terima.getNoMesin());
											listJurnal.add(jurnal);
										}

										if (terima.getsMD().compareTo(NOL) == 1
												&& (terima.getKet2()
														.equalsIgnoreCase("SMF") || terima
														.getKet2().equalsIgnoreCase("MMF"))) {
											// vdinas='G' -> vdinas=no_perk2
											String nmPerk = null;
											String idSub = null;
											if (terima.getNoPerk2().equalsIgnoreCase("G")) {
												idSub = "03";
												nmPerk = getMasterFacade()
														.getTrioMstsubperkiraanDao()
														.findNamaSubByIdAndIdSub("11326",
																idSub);

											} else {
												idSub = "02";
												nmPerk = getMasterFacade()
														.getTrioMstsubperkiraanDao()
														.findNamaSubByIdAndIdSub("11326",
																idSub);
											}
											if (nmPerk.length() < 1) {
												idSub = "";
												nmPerk = "";
											}

											jurnal = new TrioJurnal();
											jurnal.setTglJurnal(terima.getTglTrm());
											jurnal.getTrioJurnalPK()
													.setIdPerkiraan("11326"); //11312
											jurnal.getTrioJurnalPK().setIdSub(idSub);
											jurnal.setNamaPerkiraan(nmPerk);
											jurnal.setKet(terima.getNmTran());
											jurnal.setDebet(terima.getsMD());
											jurnal.setKredit(NOL);
											jurnal.setBlc("*");
											jurnal.setTipe("D");
											jurnal.setNoReff(terima.getTrioTerimaPK()
													.getNoFak());
											jurnal.setNoMesin(terima.getNoMesin());
											listJurnal.add(jurnal);
										}

										if (terima.getsSD().compareTo(NOL) == 1) {
											jurnal = new TrioJurnal();
											jurnal.setTglJurnal(terima.getTglTrm());
											jurnal.getTrioJurnalPK()
													.setIdPerkiraan("41204"); //41201
											jurnal.getTrioJurnalPK().setIdSub("");
											jurnal.setNamaPerkiraan("POTONGAN PENJUALAN DEALER");
											jurnal.setKet(terima.getNmTran());
											jurnal.setDebet(terima.getsSD());
											jurnal.setKredit(NOL);
											jurnal.setBlc("*");
											jurnal.setTipe("D");
											jurnal.setNoReff(terima.getTrioTerimaPK()
													.getNoFak());
											jurnal.setNoMesin(terima.getNoMesin());
											listJurnal.add(jurnal);
										}
										String vNoPerk0 = null;
										String vNmPerk0 = null;

										String[] arrStr = getNoPerkAndNmPerkForCab2(terima
												.getKet2());
										vNoPerk0 = arrStr[0];
										vNmPerk0 = arrStr[0];

										BigDecimal finPlusBunga = terima.getsFinance().add(
												terima.getsBunga());
										if (finPlusBunga.compareTo(NOL) == 1) {
											jurnal = new TrioJurnal();
											jurnal.setTglJurnal(terima.getTglTrm());
											jurnal.getTrioJurnalPK().setIdPerkiraan(
													vNoPerk0);
											jurnal.getTrioJurnalPK().setIdSub("");
											jurnal.setNamaPerkiraan(vNmPerk0);
											jurnal.setKet(terima.getNmTran());
											jurnal.setDebet(finPlusBunga);
											jurnal.setKredit(NOL);
											jurnal.setBlc("*");
											jurnal.setTipe("D");
											listJurnal.add(jurnal);
										}

										if (terima.getsAHM().compareTo(NOL) == 1
												&& (terima.getKet2()
														.equalsIgnoreCase("SMF") || terima
														.getKet2().equalsIgnoreCase("MMF"))) {
											jurnal = new TrioJurnal();
											jurnal.setTglJurnal(terima.getTglTrm());
											jurnal.getTrioJurnalPK().setIdPerkiraan(
													vNoPerk0);
											jurnal.getTrioJurnalPK().setIdSub("");
											jurnal.setNamaPerkiraan(vNmPerk0
													+ " (CASH BACK)");
											jurnal.setKet(terima.getNmTran());
											jurnal.setDebet(terima.getsAHM());
											jurnal.setKredit(NOL);
											jurnal.setBlc("*");
											jurnal.setTipe("D");
											listJurnal.add(jurnal);
										}

										if (terima.getKet().equalsIgnoreCase("3")) {
											jurnal = new TrioJurnal();
											jurnal.setTglJurnal(terima.getTglTrm());
											jurnal.getTrioJurnalPK()
													.setIdPerkiraan("11202");
											jurnal.getTrioJurnalPK().setIdSub(
													terima.getKdBank());
											String nmSubPerk = getMasterFacade()
													.getTrioMstsubperkiraanDao()
													.findNamaSubByIdAndIdSub("11202",
															terima.getKdBank());
											jurnal.setNamaPerkiraan(nmSubPerk);
											jurnal.setKet(terima.getNmTran());
											jurnal.setDebet(NOL);
											jurnal.setKredit(terima.getKredit());
											jurnal.setBlc("*");
											jurnal.setTipe("K");
											listJurnal.add(jurnal);
										}
										if (terima.getKet().equalsIgnoreCase("1")
												|| terima.getKet().equalsIgnoreCase("K")) {
											jurnal = new TrioJurnal();
											jurnal.setTglJurnal(terima.getTglTrm());
											jurnal.getTrioJurnalPK()
													.setIdPerkiraan("11102"); //11101
											jurnal.getTrioJurnalPK().setIdSub("");
											jurnal.setNamaPerkiraan("KAS");
											jurnal.setKet(terima.getNmTran());
											jurnal.setDebet(NOL);
											jurnal.setKredit(terima.getKredit());
											jurnal.setBlc("*");
											jurnal.setTipe("K");
											listJurnal.add(jurnal);
										}
									} else {
										if (terima.getsAHM().compareTo(NOL) == 1) {
											jurnal = new TrioJurnal();
											jurnal.setTglJurnal(terima.getTglTrm());
											jurnal.getTrioJurnalPK()
													.setIdPerkiraan("41204"); //41201
											jurnal.getTrioJurnalPK().setIdSub("");
											jurnal.setNamaPerkiraan("POTONGAN PENJUALAN AHM");
											jurnal.setKet(terima.getNmTran());
											jurnal.setDebet(terima.getsAHM());
											jurnal.setKredit(NOL);
											jurnal.setBlc("*");
											jurnal.setTipe("D");
											listJurnal.add(jurnal);
										}

										if (terima.getsMD().compareTo(NOL) == 1) {
											jurnal = new TrioJurnal();
											jurnal.setTglJurnal(terima.getTglTrm());
											jurnal.getTrioJurnalPK()
													.setIdPerkiraan("41204"); //41201
											jurnal.getTrioJurnalPK().setIdSub("");
											jurnal.setNamaPerkiraan("POTONGAN PENJUALAN MD");
											jurnal.setKet(terima.getNmTran());
											jurnal.setDebet(terima.getsMD());
											jurnal.setKredit(NOL);
											jurnal.setBlc("*");
											jurnal.setTipe("D");
											listJurnal.add(jurnal);
										}

										if (terima.getsSD().compareTo(NOL) == 1) {
											jurnal = new TrioJurnal();
											jurnal.setTglJurnal(terima.getTglTrm());
											jurnal.getTrioJurnalPK()
													.setIdPerkiraan("41204"); //41201
											jurnal.getTrioJurnalPK().setIdSub("");
											jurnal.setNamaPerkiraan("POTONGAN PENJUALAN DEALER");
											jurnal.setKet(terima.getNmTran());
											jurnal.setDebet(terima.getsSD());
											jurnal.setKredit(NOL);
											jurnal.setBlc("*");
											jurnal.setTipe("D");
											listJurnal.add(jurnal);
										}

										if (terima.getsFinance().compareTo(NOL) == 1) {
											String vNoPerk0 = null;
											String vNmPerk0 = null;

											String[] arrStr = getNoPerkAndNmPerkForCab2(terima
													.getKet2());
											vNoPerk0 = arrStr[0];
											vNmPerk0 = arrStr[0];

											jurnal = new TrioJurnal();
											jurnal.setTglJurnal(terima.getTglTrm());
											jurnal.getTrioJurnalPK().setIdPerkiraan(
													vNoPerk0);
											jurnal.getTrioJurnalPK().setIdSub("");
											jurnal.setNamaPerkiraan(vNmPerk0);
											jurnal.setKet(terima.getNmTran());
											jurnal.setDebet(terima.getsFinance());
											jurnal.setKredit(NOL);
											jurnal.setBlc("*");
											jurnal.setTipe("D");
											listJurnal.add(jurnal);

										}
										if (terima.getKet().equalsIgnoreCase("3")) {
											jurnal = new TrioJurnal();
											jurnal.setTglJurnal(terima.getTglTrm());
											jurnal.getTrioJurnalPK()
													.setIdPerkiraan("11202");
											jurnal.getTrioJurnalPK().setIdSub(
													terima.getKdBank());
											String nmSubPerk = getMasterFacade()
													.getTrioMstsubperkiraanDao()
													.findNamaSubByIdAndIdSub("11202",
															terima.getKdBank());
											jurnal.setNamaPerkiraan(nmSubPerk);
											jurnal.setKet(terima.getNmTran());
											jurnal.setDebet(NOL);
											jurnal.setKredit(terima.getKredit());
											jurnal.setBlc("*");
											jurnal.setTipe("K");
											listJurnal.add(jurnal);
										}

										if (terima.getKet().equalsIgnoreCase("1")
												|| terima.getKet().equalsIgnoreCase("K")) {
											jurnal = new TrioJurnal();
											jurnal.setTglJurnal(terima.getTglTrm());
											jurnal.getTrioJurnalPK()
													.setIdPerkiraan("11102"); //11101
											jurnal.getTrioJurnalPK().setIdSub("");
											jurnal.setNamaPerkiraan("KAS");
											jurnal.setKet(terima.getNmTran());
											jurnal.setDebet(NOL);
											jurnal.setKredit(terima.getKredit());
											jurnal.setBlc("*");
											jurnal.setTipe("K");
											listJurnal.add(jurnal);
										}

									}

								}
								// else dari kredit > 0
							} else {
								if (terima.getKet2().equalsIgnoreCase("ADR")
										|| terima.getKet2().equalsIgnoreCase("ADS")
										|| terima.getKet2().equalsIgnoreCase("OTO")
										|| terima.getKet2().equalsIgnoreCase("WOM")) {
									if (terima.getsAHM().compareTo(NOL) == 1) {
										jurnal = new TrioJurnal();
										jurnal.setTglJurnal(terima.getTglTrm());
										jurnal.getTrioJurnalPK().setIdPerkiraan("41204"); //41201
										jurnal.getTrioJurnalPK().setIdSub("");
										jurnal.setNamaPerkiraan("POTONGAN PENJUALAN AHM");
										jurnal.setKet(terima.getNmTran());
										jurnal.setDebet(NOL);
										jurnal.setKredit(terima.getsAHM());
										jurnal.setBlc("*");
										jurnal.setTipe("K");
										listJurnal.add(jurnal);
									}
									if (terima.getsMD().compareTo(NOL) == 1) {
										jurnal = new TrioJurnal();
										jurnal.setTglJurnal(terima.getTglTrm());
										jurnal.getTrioJurnalPK().setIdPerkiraan("41204"); //41201
										jurnal.getTrioJurnalPK().setIdSub("");
										jurnal.setNamaPerkiraan("POTONGAN PENJUALAN MD");
										jurnal.setKet(terima.getNmTran());
										jurnal.setDebet(NOL);
										jurnal.setKredit(terima.getsMD());
										jurnal.setBlc("*");
										jurnal.setTipe("K");
										listJurnal.add(jurnal);
									}
									if (terima.getsSD().compareTo(NOL) == 1) {
										jurnal = new TrioJurnal();
										jurnal.setTglJurnal(terima.getTglTrm());
										jurnal.getTrioJurnalPK().setIdPerkiraan("41204"); //41201
										jurnal.getTrioJurnalPK().setIdSub("");
										jurnal.setNamaPerkiraan("POTONGAN PENJUALAN DEALER");
										jurnal.setKet(terima.getNmTran());
										jurnal.setDebet(NOL);
										jurnal.setKredit(terima.getsSD());
										jurnal.setBlc("*");
										jurnal.setTipe("K");
										listJurnal.add(jurnal);
									}
									if (terima.getsFinance().compareTo(NOL) == 1) {
										if (terima.getKet2().equalsIgnoreCase("ADR")
												|| terima.getKet2().equalsIgnoreCase("ADS")
												|| terima.getKet2().equalsIgnoreCase("OTO")) {
											jurnal = new TrioJurnal();
											jurnal.setTglJurnal(terima.getTglTrm());
											jurnal.getTrioJurnalPK()
													.setIdPerkiraan("61206"); // 61106
											jurnal.getTrioJurnalPK().setIdSub("03");
											jurnal.setNamaPerkiraan("VOUCHER PENJUALAN");
											jurnal.setKet(terima.getNmTran());
											jurnal.setDebet(NOL);
											jurnal.setKredit(terima.getDebet());
											jurnal.setBlc("*");
											jurnal.setTipe("K");
											listJurnal.add(jurnal);
										}
										if (terima.getKet2().equalsIgnoreCase("WOM")) {
											jurnal = new TrioJurnal();
											jurnal.setTglJurnal(terima.getTglTrm());
											jurnal.getTrioJurnalPK()
													.setIdPerkiraan("61206"); // 61106
											jurnal.getTrioJurnalPK().setIdSub("04");
											jurnal.setNamaPerkiraan("VOUCHER PENJUALAN");
											jurnal.setKet(terima.getNmTran());
											jurnal.setDebet(NOL);
											jurnal.setKredit(terima.getDebet());
											jurnal.setBlc("*");
											jurnal.setTipe("K");
											listJurnal.add(jurnal);
										}

										String vNoPerk0 = null;
										String vNmPerk0 = null;

										String[] arrStr = getNoPerkAndNmPerkForCab2(terima
												.getKet2());
										vNoPerk0 = arrStr[0];
										vNmPerk0 = arrStr[0];

										// set Debet
										BigDecimal tempKredit = NOL;

										if (terima.getKet2().equalsIgnoreCase("ADR")
												|| terima.getKet2().equalsIgnoreCase("ADS")) {
											tempKredit = terima.getsFinance().add(
													terima.getsFinance().multiply(
															new BigDecimal(0.1)));
										} else {
											if (terima.getKet2().equalsIgnoreCase("WOM")) {
												tempKredit = new BigDecimal(1100000);
											} else {
												if (terima.getKet2()
														.equalsIgnoreCase("OTO")) {
													TrioTerima tt = getTerimaByNoFakAndNoPerk(terima.getTrioTerimaPK().getNoFak(), "P3");
													tempKredit = tt.getsFinance();
																					
												} else {
													tempKredit = terima.getsFinance();
												}
											}
										}

										jurnal = new TrioJurnal();
										jurnal.setTglJurnal(terima.getTglTrm());
										jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk0);
										jurnal.getTrioJurnalPK().setIdSub("");
										jurnal.setNamaPerkiraan(vNmPerk0);
										jurnal.setKet(terima.getNmTran());
										jurnal.setDebet(NOL);
										jurnal.setKredit(tempKredit);
										jurnal.setBlc("*");
										jurnal.setTipe("K");
										listJurnal.add(jurnal);

										String vNoSub71204 = "";

										if (terima.getKet2().equalsIgnoreCase("ADR")) {
											vNoSub71204 = "01";
										} else if (terima.getKet2().equalsIgnoreCase("ADS")) {
											vNoSub71204 = "02";
										} else if (terima.getKet2().equalsIgnoreCase("OTO")) {
											vNoSub71204 = "03";
										} else if (terima.getKet2().equalsIgnoreCase("WOM")) {
											vNoSub71204 = "04";
										}
										String vNmSub71204 = getMasterFacade()
												.getTrioMstsubperkiraanDao()
												.findNamaSubByIdAndIdSub("71204",
														vNoSub71204);

										jurnal = new TrioJurnal();
										jurnal.setTglJurnal(terima.getTglTrm());
										jurnal.getTrioJurnalPK().setIdPerkiraan("71204"); //71104
										jurnal.getTrioJurnalPK().setIdSub(vNoSub71204);
										jurnal.setNamaPerkiraan(vNmSub71204);
										jurnal.setKet(terima.getNmTran());
										BigDecimal tempDebet = NOL;

										if (terima.getKet2().equalsIgnoreCase("ADR")
												|| terima.getKet2().equalsIgnoreCase("ADS")) {
											tempDebet = terima.getsFinance();
										} else {
											if (terima.getKet2().equalsIgnoreCase("WOM")) {
												tempDebet = new BigDecimal(1000000);
											} else {
												if (terima.getKet2()
														.equalsIgnoreCase("OTO")) {
													tempDebet = tempDebet
															.multiply(new BigDecimal(10)
																	.divide(new BigDecimal(
																			11),
																			0,
																			RoundingMode.HALF_EVEN));
												} else {
													tempDebet = terima
															.getsFinance()
															.multiply(
																	new BigDecimal(10)
																			.divide(new BigDecimal(
																					11),
																					0,
																					RoundingMode.HALF_EVEN));
												}
											}
										}

										jurnal.setDebet(tempDebet);
										jurnal.setKredit(NOL);
										jurnal.setBlc("*");
										jurnal.setTipe("D");
										listJurnal.add(jurnal);

										String vNmPerk = getMasterFacade()
												.getTrioMstperkiraanDao().findNamaById(
														"21404");

										jurnal = new TrioJurnal();
										jurnal.setTglJurnal(terima.getTglTrm());
										jurnal.getTrioJurnalPK().setIdPerkiraan("21404");
										jurnal.getTrioJurnalPK().setIdSub("");
										jurnal.setNamaPerkiraan(vNmPerk);
										jurnal.setKet(terima.getNmTran());

										if (terima.getKet2().equalsIgnoreCase("ADS")
												|| terima.getKet2().equalsIgnoreCase("ADS")) {
											tempDebet = terima.getsFinance().multiply(
													new BigDecimal(0.1));
										} else {
											if (terima.getKet2().equalsIgnoreCase("WOM")) {
												tempDebet = new BigDecimal(100000);
											} else {
												if (terima.getKet2()
														.equalsIgnoreCase("OTO")) {
													tempDebet = tempDebet
															.multiply(new BigDecimal(10)
																	.divide(new BigDecimal(
																			11),
																			0,
																			RoundingMode.HALF_EVEN));
												} else {
													tempDebet = terima
															.getsFinance()
															.multiply(
																	new BigDecimal(10)
																			.divide(new BigDecimal(
																					11),
																					0,
																					RoundingMode.HALF_EVEN));
												}
											}
										}
										jurnal.setDebet(tempDebet);
										jurnal.setKredit(NOL);
										jurnal.setBlc("*");
										jurnal.setTipe("D");
										listJurnal.add(jurnal);

									}

									if (terima.getKet().equalsIgnoreCase("3")) {
										String vnoPerk = "11202";
										String vnoSub = terima.getKdBank();

										String vnmSub = getMasterFacade()
												.getTrioMstsubperkiraanDao()
												.findNamaSubByIdAndIdSub(vnoPerk, vnoSub);

										jurnal = new TrioJurnal();
										jurnal.setTglJurnal(terima.getTglTrm());
										jurnal.getTrioJurnalPK().setIdPerkiraan(vnoPerk);
										jurnal.getTrioJurnalPK().setIdSub(vnoSub);
										jurnal.setNamaPerkiraan(vnmSub);
										jurnal.setKet(terima.getNmTran());
										jurnal.setDebet(terima.getDebet());
										jurnal.setKredit(NOL);
										jurnal.setBlc("*");
										jurnal.setTipe("D");
										listJurnal.add(jurnal);
									}
									if (terima.getKet().equalsIgnoreCase("1")
											|| terima.getKet().equalsIgnoreCase("K")) {
										jurnal = new TrioJurnal();
										jurnal.setTglJurnal(terima.getTglTrm());
										jurnal.getTrioJurnalPK().setIdPerkiraan("11102"); // 11101
										jurnal.getTrioJurnalPK().setIdSub("");
										jurnal.setNamaPerkiraan("KAS");
										jurnal.setKet(terima.getNmTran());
										jurnal.setDebet(terima.getDebet());
										jurnal.setKredit(NOL);
										jurnal.setBlc("*");
										jurnal.setTipe("D");
										listJurnal.add(jurnal);
									}

								} else { // ini else utk ADR ADS OTO WOM
									if (terima.getKet2().length() == 0) {
										if (terima.getsMD().compareTo(NOL) == 1
												&& (terima.getKet2()
														.equalsIgnoreCase("SMF") || terima
														.getKet2().equalsIgnoreCase("MMF"))) {
											String vNmSub = null;
											String idSub = "";
											if (terima.getNoPerk2().equalsIgnoreCase("G")) {
												idSub = "03";
												vNmSub = getMasterFacade()
														.getTrioMstsubperkiraanDao()
														.findNamaSubByIdAndIdSub("11326",
																idSub); // 11312
											} else {
												idSub = "02";
												vNmSub = getMasterFacade()
														.getTrioMstsubperkiraanDao()
														.findNamaSubByIdAndIdSub("11326",
																idSub);// 11312
											}

											jurnal = new TrioJurnal();
											jurnal.setTglJurnal(terima.getTglTrm());
											jurnal.getTrioJurnalPK()
													.setIdPerkiraan("11326");
											jurnal.getTrioJurnalPK().setIdSub(idSub);
											jurnal.setNamaPerkiraan(vNmSub);
											jurnal.setKet(terima.getNmTran());
											jurnal.setDebet(NOL);
											jurnal.setKredit(terima.getsMD());
											jurnal.setBlc("*");
											jurnal.setTipe("K");
											jurnal.setNoReff(terima.getTrioTerimaPK()
													.getNoFak());
											jurnal.setNoMesin(terima.getNoMesin());
											listJurnal.add(jurnal);
										}

										if (terima.getsAHM().compareTo(NOL) == 1
												&& terima.getKet2().length() == 0) {
											jurnal = new TrioJurnal();
											jurnal.setTglJurnal(terima.getTglTrm());
											jurnal.getTrioJurnalPK()
													.setIdPerkiraan("41204"); //41201 
											jurnal.getTrioJurnalPK().setIdSub("");
											jurnal.setNamaPerkiraan("POTONGAN PENJUALAN AHM");
											jurnal.setKet(terima.getNmTran());
											jurnal.setDebet(NOL);
											jurnal.setKredit(terima.getsAHM());
											jurnal.setBlc("*");
											jurnal.setTipe("K");
											jurnal.setNoReff(terima.getTrioTerimaPK()
													.getNoFak());
											jurnal.setNoMesin(terima.getNoMesin());
											listJurnal.add(jurnal);
										}

										if (terima.getsMD().compareTo(NOL) == 1
												&& terima.getKet2().length() == 0) {
											jurnal = new TrioJurnal();
											jurnal.setTglJurnal(terima.getTglTrm());
											jurnal.getTrioJurnalPK()
													.setIdPerkiraan("41204"); //41201
											jurnal.getTrioJurnalPK().setIdSub("");
											jurnal.setNamaPerkiraan("POTONGAN PENJUALAN MD");
											jurnal.setKet(terima.getNmTran());
											jurnal.setDebet(NOL);
											jurnal.setKredit(terima.getsMD());
											jurnal.setBlc("*");
											jurnal.setTipe("K");
											jurnal.setNoReff(terima.getTrioTerimaPK()
													.getNoFak());
											jurnal.setNoMesin(terima.getNoMesin());
											listJurnal.add(jurnal);
										}

										if (terima.getsSD().compareTo(NOL) == 1) {
											jurnal = new TrioJurnal();
											jurnal.setTglJurnal(terima.getTglTrm());
											jurnal.getTrioJurnalPK()
													.setIdPerkiraan("41204"); //41201
											jurnal.getTrioJurnalPK().setIdSub("");
											jurnal.setNamaPerkiraan("POTONGAN PENJUALAN DEALER");
											jurnal.setKet(terima.getNmTran());
											jurnal.setDebet(NOL);
											jurnal.setKredit(terima.getsSD());
											jurnal.setBlc("*");
											jurnal.setTipe("K");
											jurnal.setNoReff(terima.getTrioTerimaPK()
													.getNoFak());
											jurnal.setNoMesin(terima.getNoMesin());
											listJurnal.add(jurnal);
										}

										String vNoPerk0 = null;
										String vNmPerk0 = null;

										String[] arrStr = getNoPerkAndNmPerkForCab2(terima
												.getKet2());
										vNoPerk0 = arrStr[0];
										vNmPerk0 = arrStr[0];

										BigDecimal financePlusBunga = terima.getsFinance()
												.add(terima.getsBunga());
										if (financePlusBunga.compareTo(NOL) == 1) {
											jurnal = new TrioJurnal();
											jurnal.setTglJurnal(terima.getTglTrm());
											jurnal.getTrioJurnalPK().setIdPerkiraan(
													vNoPerk0);
											jurnal.getTrioJurnalPK().setIdSub("");
											jurnal.setNamaPerkiraan(vNmPerk0);
											jurnal.setKet(terima.getNmTran());
											jurnal.setDebet(NOL);
											jurnal.setKredit(financePlusBunga);
											jurnal.setBlc("*");
											jurnal.setTipe("K");
											listJurnal.add(jurnal);
										}

										if (terima.getsAHM().compareTo(NOL) == 1
												&& (terima.getKet2()
														.equalsIgnoreCase("SMF") || terima
														.getKet2().equalsIgnoreCase("MMF"))) {
											jurnal = new TrioJurnal();
											jurnal.setTglJurnal(terima.getTglTrm());
											jurnal.getTrioJurnalPK().setIdPerkiraan(
													vNoPerk0);
											jurnal.getTrioJurnalPK().setIdSub("");
											jurnal.setNamaPerkiraan(vNmPerk0
													+ " (CASH BACK)");
											jurnal.setKet(terima.getNmTran());
											jurnal.setDebet(NOL);
											jurnal.setKredit(terima.getsAHM());
											jurnal.setBlc("*");
											jurnal.setTipe("K");
											listJurnal.add(jurnal);
										}

										if (terima.getKet().equalsIgnoreCase("3")) {
											jurnal = new TrioJurnal();
											jurnal.setTglJurnal(terima.getTglTrm());
											jurnal.getTrioJurnalPK()
													.setIdPerkiraan("11202");
											jurnal.getTrioJurnalPK().setIdSub(
													terima.getKdBank());

											String vNmSub = getMasterFacade()
													.getTrioMstsubperkiraanDao()
													.findNamaSubByIdAndIdSub("11202",
															terima.getKdBank());
											jurnal.setNamaPerkiraan(vNmSub);
											jurnal.setKet(terima.getNmTran());
											jurnal.setDebet(terima.getDebet());
											jurnal.setKredit(NOL);
											jurnal.setBlc("*");
											jurnal.setTipe("D");
											listJurnal.add(jurnal);
										}

										if (terima.getKet().equalsIgnoreCase("1")
												|| terima.getKet().equalsIgnoreCase("K")) {
											jurnal = new TrioJurnal();
											jurnal.setTglJurnal(terima.getTglTrm());
											jurnal.getTrioJurnalPK()
													.setIdPerkiraan("11102"); //11101
											jurnal.getTrioJurnalPK().setIdSub("");
											jurnal.setNamaPerkiraan("KAS");
											jurnal.setKet(terima.getNmTran());
											jurnal.setDebet(terima.getDebet());
											jurnal.setKredit(NOL);
											jurnal.setBlc("*");
											jurnal.setTipe("D");
											listJurnal.add(jurnal);
										}

									} else { // ini else utk terima.getKet2().length==0

										if (terima.getsAHM().compareTo(NOL) == 1) {
											jurnal = new TrioJurnal();
											jurnal.setTglJurnal(terima.getTglTrm());
											jurnal.getTrioJurnalPK()
													.setIdPerkiraan("41204"); // 41201
											jurnal.getTrioJurnalPK().setIdSub("");
											jurnal.setNamaPerkiraan("POTONGAN PENJUALAN AHM");
											jurnal.setKet(terima.getNmTran());
											jurnal.setDebet(NOL);
											jurnal.setKredit(terima.getsAHM());
											jurnal.setBlc("*");
											jurnal.setTipe("K");
											listJurnal.add(jurnal);
										}

										if (terima.getsMD().compareTo(NOL) == 1) {

											jurnal = new TrioJurnal();
											jurnal.setTglJurnal(terima.getTglTrm());
											jurnal.getTrioJurnalPK()
													.setIdPerkiraan("41204"); //41201
											jurnal.getTrioJurnalPK().setIdSub("");
											jurnal.setNamaPerkiraan("POTONGAN PENJUALAN MD");
											jurnal.setKet(terima.getNmTran());
											jurnal.setDebet(NOL);
											jurnal.setKredit(terima.getsMD());
											jurnal.setBlc("*");
											jurnal.setTipe("K");
											listJurnal.add(jurnal);
										}

										if (terima.getsSD().compareTo(NOL) == 1) {

											jurnal = new TrioJurnal();
											jurnal.setTglJurnal(terima.getTglTrm());
											jurnal.getTrioJurnalPK()
													.setIdPerkiraan("41204"); //41201
											jurnal.getTrioJurnalPK().setIdSub("");
											jurnal.setNamaPerkiraan("POTONGAN PENJUALAN DEALER");
											jurnal.setKet(terima.getNmTran());
											jurnal.setDebet(NOL);
											jurnal.setKredit(terima.getsSD());
											jurnal.setBlc("*");
											jurnal.setTipe("K");
											listJurnal.add(jurnal);
										}

										if (terima.getsFinance().compareTo(NOL) == 1) {
											String vNoPerk0 = null;
											String vNmPerk0 = null;

											String[] arrStr = getNoPerkAndNmPerkForCab2(terima
													.getKet2());
											vNoPerk0 = arrStr[0];
											vNmPerk0 = arrStr[0];

											jurnal = new TrioJurnal();
											jurnal.setTglJurnal(terima.getTglTrm());
											jurnal.getTrioJurnalPK().setIdPerkiraan(
													vNoPerk0);
											jurnal.getTrioJurnalPK().setIdSub("");
											jurnal.setNamaPerkiraan(vNmPerk0);
											jurnal.setKet(terima.getNmTran());
											jurnal.setDebet(NOL);
											jurnal.setKredit(terima.getsFinance());
											jurnal.setBlc("*");
											jurnal.setTipe("K");
											listJurnal.add(jurnal);

										}

										if (terima.getKet().equalsIgnoreCase("3")) {
											jurnal = new TrioJurnal();
											jurnal.setTglJurnal(terima.getTglTrm());
											jurnal.getTrioJurnalPK()
													.setIdPerkiraan("11202");
											jurnal.getTrioJurnalPK().setIdSub(
													terima.getKdBank());
											String vNmSub = getMasterFacade()
													.getTrioMstsubperkiraanDao()
													.findNamaSubByIdAndIdSub("11202",
															terima.getKdBank());
											jurnal.setNamaPerkiraan(vNmSub);
											jurnal.setKet(terima.getNmTran());
											jurnal.setDebet(terima.getDebet());
											jurnal.setKredit(NOL);
											jurnal.setBlc("*");
											jurnal.setTipe("D");
											listJurnal.add(jurnal);
										}

										if (terima.getKet().equalsIgnoreCase("1")
												|| terima.getKet().equalsIgnoreCase("K")) {
											jurnal = new TrioJurnal();
											jurnal.setTglJurnal(terima.getTglTrm());
											jurnal.getTrioJurnalPK()
													.setIdPerkiraan("11102"); //11101
											jurnal.getTrioJurnalPK().setIdSub("");
											jurnal.setNamaPerkiraan("KAS");
											jurnal.setKet(terima.getNmTran());
											jurnal.setDebet(terima.getDebet());
											jurnal.setKredit(NOL);
											jurnal.setBlc("*");
											jurnal.setTipe("D");
											listJurnal.add(jurnal);
										}

									}
								}
								String jenisJurnal = getJenisJurnalByParams(
										terima.getKet(), terima.getDebet(),
										terima.getKredit(), terima.getNoPerk());
								getMasterFacade().getTrioJurnalDao().saveTransaction(
										listJurnal, jenisJurnal, user);
							}
							// case vno_perk='11301' and vket='2' AND ALLTRIM(master.kd_prs)<>'T10'
						}
						
						else if (terima.getNoPerk().equalsIgnoreCase("11315")
								&& terima.getKet().equalsIgnoreCase("2")) {  //11301

							System.out.println("CASEIFCAB2 9");

							if (terima.getDebet().compareTo(NOL) == 1) {
								TrioMstsubperkiraan subPerk = getMasterFacade()
										.getTrioMstsubperkiraanDao().getSubByIdPerkAndKet2(
												"11301", terima.getKet2());

								if (subPerk != null) {

									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan("11203");
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan("CEK");
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(terima.getDebet());
									jurnal.setKredit(NOL);
									jurnal.setBlc("*");
									jurnal.setTipe("D");
									listJurnal.add(jurnal);

									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(
											terima.getNoPerk());
									jurnal.getTrioJurnalPK().setIdSub(
											subPerk.getTrioMstsubperkiraanPK().getIdSub());
									jurnal.setNamaPerkiraan(subPerk.getNamaSub());
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(NOL);
									jurnal.setKredit(terima.getDebet());
									jurnal.setBlc("*");
									jurnal.setTipe("K");
									listJurnal.add(jurnal);

								} else {
									System.out.println("No Mesin : " + terima.getKet2()
											+ " tidak ditemukan !");
								}

							} else {
								TrioMstsubperkiraan subPerk = getMasterFacade()
										.getTrioMstsubperkiraanDao().getSubByIdPerkAndKet2(
												"11301", terima.getKet2());

								if (subPerk != null) {

									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan("11203");
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan("CEK");
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(NOL);
									jurnal.setKredit(terima.getKredit());
									jurnal.setBlc("*");
									jurnal.setTipe("K");
									listJurnal.add(jurnal);

									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(
											terima.getNoPerk());
									jurnal.getTrioJurnalPK().setIdSub(
											subPerk.getTrioMstsubperkiraanPK().getIdSub());
									jurnal.setNamaPerkiraan(subPerk.getNamaSub());
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(terima.getKredit());
									jurnal.setKredit(NOL);
									jurnal.setBlc("*");
									jurnal.setTipe("D");
									listJurnal.add(jurnal);

								} else {
									System.out.println("No Mesin : " + terima.getKet2()
											+ " tidak ditemukan !");
								}
							}
							String jenisJurnal = getJenisJurnalByParams(terima.getKet(),
									terima.getDebet(), terima.getKredit(),
									terima.getNoPerk());
							getMasterFacade().getTrioJurnalDao().saveTransaction(
									listJurnal, jenisJurnal, user);
							// case vno_perk='11301' and vket='3' AND ALLTRIM(master.kd_prs)<>'T10'
						} else if (terima.getNoPerk().equalsIgnoreCase("13315")
								&& terima.getKet().equalsIgnoreCase("3")) { //11301

							System.out.println("CASEIFCAB2 10");

							if (terima.getDebet().compareTo(NOL) == 1) {

								TrioMstsubperkiraan subPerk = getMasterFacade()
										.getTrioMstsubperkiraanDao().getSubByIdPerkAndKet2(
												"11301", terima.getKet2());

								if (subPerk != null) {

									vNoSub = subPerk.getTrioMstsubperkiraanPK()
											.getIdSub();
									String vNmPerk1 = subPerk.getNamaSub();
									vNoPerk = "11202";

									String vNmPerk = getMasterFacade()
											.getTrioMstsubperkiraanDao()
											.findNamaSubByIdAndIdSub(vNoPerk, vNoSub);

									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan("11202");
									jurnal.getTrioJurnalPK().setIdSub(terima.getKdBank());
									jurnal.setNamaPerkiraan(vNmPerk + " " + vNmPerk1);
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(terima.getDebet());
									jurnal.setKredit(NOL);
									jurnal.setBlc("*");
									jurnal.setTipe("D");
									listJurnal.add(jurnal);

									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(
											terima.getNoPerk());
									jurnal.getTrioJurnalPK().setIdSub(vNoSub);
									jurnal.setNamaPerkiraan(vNmPerk);
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(NOL);
									jurnal.setKredit(terima.getDebet());
									jurnal.setBlc("*");
									jurnal.setTipe("K");
									listJurnal.add(jurnal);

								} else {
									System.out.println("No Mesin : " + terima.getKet2()
											+ " tidak ditemukan !");
								}
							} else {
								TrioMstsubperkiraan subPerk = getMasterFacade()
										.getTrioMstsubperkiraanDao().getSubByIdPerkAndKet2(
												"11301", terima.getKet2());

								if (subPerk != null) {

									vNoSub = subPerk.getTrioMstsubperkiraanPK()
											.getIdSub();
									String vNmPerk1 = subPerk.getNamaSub();
									vNoPerk = "11202";

									String vNmPerk = getMasterFacade()
											.getTrioMstsubperkiraanDao()
											.findNamaSubByIdAndIdSub(vNoPerk, vNoSub);
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan("11202");
									jurnal.getTrioJurnalPK().setIdSub(terima.getKdBank());
									jurnal.setNamaPerkiraan(vNmPerk + " " + vNmPerk1);
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(NOL);
									jurnal.setKredit(terima.getKredit());
									jurnal.setBlc("*");
									jurnal.setTipe("K");
									listJurnal.add(jurnal);

									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(
											terima.getNoPerk());
									jurnal.getTrioJurnalPK().setIdSub(vNoSub);
									jurnal.setNamaPerkiraan(vNmPerk);
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(terima.getKredit());
									jurnal.setKredit(NOL);
									jurnal.setBlc("*");
									jurnal.setTipe("D");
									listJurnal.add(jurnal);

								} else {
									System.out.println("No Mesin : " + terima.getKet2()
											+ " tidak ditemukan !");
								}
							}
							String jenisJurnal = getJenisJurnalByParams(terima.getKet(),
									terima.getDebet(), terima.getKredit(),
									terima.getNoPerk());
							getMasterFacade().getTrioJurnalDao().saveTransaction(
									listJurnal, jenisJurnal, user);
							// case vno_perk='P0' and vket='1' AND vdinas='D'
						} else if (terima.getNoPerk().equalsIgnoreCase("P0")
								&& terima.getKet().equalsIgnoreCase("1")
								&& terima.getNoPerk2().equalsIgnoreCase("D")) {

							System.out.println("CASEIFCAB2 11");

							if (terima.getDebet().compareTo(NOL) == 1) {
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan("11327"); //11313
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan("PIUTANG PENJUALAN");
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getDebet());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);

								if (terima.getsAHM().compareTo(NOL) == 1) {
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan("41204"); //41201
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan("POTONGAN PENJUALAN AHM");
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(terima.getsAHM());
									jurnal.setKredit(NOL);
									jurnal.setBlc("*");
									jurnal.setTipe("D");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);
								}

								if (terima.getsMD().compareTo(NOL) == 1) {
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan("41204"); //41201
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan("POTONGAN PENJUALAN MD");
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(terima.getsMD());
									jurnal.setKredit(NOL);
									jurnal.setBlc("*");
									jurnal.setTipe("D");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);
								}

								if (terima.getsSD().compareTo(NOL) == 1) {
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan("41204"); //41201
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan("POTONGAN PENJUALAN DEALER");
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(terima.getsSD());
									jurnal.setKredit(NOL);
									jurnal.setBlc("*");
									jurnal.setTipe("D");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);
								}
								// vfee = jml_trm on foxpro
								BigDecimal biayaGabung = terima.getpSTNK().add(
										terima.getpBPKB().add(terima.getJmlTrm()));
								BigDecimal debetPer11 = terima.getDebet().divide(
										new BigDecimal(11), 0, RoundingMode.HALF_EVEN);
								BigDecimal total = debetPer11.add(biayaGabung);
								BigDecimal allHitung = terima.getDebet().subtract(total);
								//compare sampai sini
								if (allHitung.compareTo(NOL) == 1) {

									String vNoPerk3 = "41106"; //41101
									String vNmPerk3 = getMasterFacade()
											.getTrioMstperkiraanDao()
											.findNamaById(vNoPerk3);
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk3);
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan(vNmPerk3);
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(NOL);
									jurnal.setKredit(allHitung);
									jurnal.setBlc("*");
									jurnal.setTipe("K");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);
								}

								String vNoPerk4 = "21404";
								String vNmPerk4 = getMasterFacade()
										.getTrioMstperkiraanDao().findNamaById(vNoPerk4);

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk4);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk4);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getDebet().divide(
										new BigDecimal(11), 0, RoundingMode.HALF_EVEN)); // vdebet/11
																							// on
																							// foxpro
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);

								// if vs_ahm+vs_md+vs_sd>0
								BigDecimal jumlahSubsidi = terima.getsAHM().add(
										terima.getsMD().add(terima.getsSD()));
								if (jumlahSubsidi.compareTo(NOL) == 1) {

									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan("41204"); //41201
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan("POTONGAN PENJUALAN");
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(NOL);
									jurnal.setKredit(jumlahSubsidi);
									jurnal.setBlc("*");
									jurnal.setTipe("K");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);
								}

								String vNoPerk5 = "21213"; //21204
								String vNmPerk5 = getMasterFacade()
										.getTrioMstperkiraanDao().findNamaById(vNoPerk5);
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk5);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk5);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getpSTNK());
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);

								String vNoPerk6 = "21214"; //21205
								String vNmPerk6 = getMasterFacade()
										.getTrioMstperkiraanDao().findNamaById(vNoPerk6);
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk6);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk6);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getpBPKB());
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);

								if (terima.getJmlTrm().compareTo(NOL) == 1) {
									String vNoPerk7 = "21309";
									String vNmPerk7 = getMasterFacade()
											.getTrioMstperkiraanDao()
											.findNamaById(vNoPerk7);
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk7);
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan(vNmPerk7);
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(NOL);
									jurnal.setKredit(terima.getJmlTrm());
									jurnal.setBlc("*");
									jurnal.setTipe("K");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);
								}

							} else {

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan("11327"); //11313
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan("PIUTANG PENJUALAN");
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getKredit());
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);

								if (terima.getsAHM().compareTo(NOL) == 1) {
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan("41204"); //41201
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan("POTONGAN PENJUALAN AHM");
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(NOL);
									jurnal.setKredit(terima.getsAHM());
									jurnal.setBlc("*");
									jurnal.setTipe("K");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);
								}

								if (terima.getsMD().compareTo(NOL) == 1) {
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan("41204"); //41201
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan("POTONGAN PENJUALAN MD");
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(NOL);
									jurnal.setKredit(terima.getsMD());
									jurnal.setBlc("*");
									jurnal.setTipe("K");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);
								}

								if (terima.getsSD().compareTo(NOL) == 1) {
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan("41204"); //41201
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan("POTONGAN PENJUALAN DEALER");
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(NOL);
									jurnal.setKredit(terima.getsSD());
									jurnal.setBlc("*");
									jurnal.setTipe("K");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);
								}
								// vfee = jml_trm on foxpro
								BigDecimal biayaGabung = terima.getpSTNK().add(
										terima.getpBPKB().add(terima.getJmlTrm()));
								BigDecimal kreditPer11 = terima.getKredit().divide(
										new BigDecimal(11), 0, RoundingMode.HALF_EVEN);
								BigDecimal total = kreditPer11.add(biayaGabung);
								BigDecimal allHitung = terima.getKredit().subtract(total);

								if (allHitung.compareTo(NOL) == 1) {

									String vNoPerk3 = "41106"; //41101
									String vNmPerk3 = getMasterFacade()
											.getTrioMstperkiraanDao()
											.findNamaById(vNoPerk3);
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk3);
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan(vNmPerk3);
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(allHitung);
									jurnal.setKredit(NOL);
									jurnal.setBlc("*");
									jurnal.setTipe("D");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);
								}

								String vNoPerk4 = "21404";
								String vNmPerk4 = getMasterFacade()
										.getTrioMstperkiraanDao().findNamaById(vNoPerk4);

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk4);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk4);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getKredit().divide(
										new BigDecimal(11), 0, RoundingMode.HALF_EVEN)); // vdebet/11
																							// on
																							// foxpro
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);

								// if vs_ahm+vs_md+vs_sd>0
								BigDecimal jumlahSubsidi = terima.getsAHM().add(
										terima.getsMD().add(terima.getsSD()));
								if (jumlahSubsidi.compareTo(NOL) == 1) {

									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan("41204"); //41201
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan("POTONGAN PENJUALAN");
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(jumlahSubsidi);
									jurnal.setKredit(NOL);
									jurnal.setBlc("*");
									jurnal.setTipe("D");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);
								}

								String vNoPerk5 = "21213"; //21204
								String vNmPerk5 = getMasterFacade()
										.getTrioMstperkiraanDao().findNamaById(vNoPerk5);
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk5);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk5);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getpSTNK());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);

								String vNoPerk6 = "21214"; //21205
								String vNmPerk6 = getMasterFacade()
										.getTrioMstperkiraanDao().findNamaById(vNoPerk6);
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk6);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk6);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getpBPKB());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);

								if (terima.getJmlTrm().compareTo(NOL) == 1) {
									String vNoPerk7 = "21309";
									String vNmPerk7 = getMasterFacade()
											.getTrioMstperkiraanDao()
											.findNamaById(vNoPerk7);
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk7);
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan(vNmPerk7);
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(terima.getJmlTrm());
									jurnal.setKredit(NOL);
									jurnal.setBlc("*");
									jurnal.setTipe("D");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);
								}

							}
							String jenisJurnal = getJenisJurnalByParams(terima.getKet(),
									terima.getDebet(), terima.getKredit(),
									terima.getNoPerk());
							getMasterFacade().getTrioJurnalDao().saveTransaction(
									listJurnal, jenisJurnal, user);
							// case vno_perk='P1' AND vdinas='D'
						} else if (terima.getNoPerk().equalsIgnoreCase("P1")
								&& terima.getNoPerk2().equalsIgnoreCase("D")) {

							System.out.println("CASEIFCAB2 12");

							if (terima.getKet().equalsIgnoreCase("1")
									|| terima.getKet().equalsIgnoreCase("K")) {
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan("11102"); //11101
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan("KAS");
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getDebet());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);
							}

							if (terima.getKet().equalsIgnoreCase("2")) {
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan("11203");
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan("CEK");
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getDebet());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);
							}

							if (terima.getKet().equalsIgnoreCase("3")) {

								vNoPerk = "11202";
								vNoSub = terima.getKdBank();
								String vNmSub = "";
								if (vNoPerk.length() > 0) {
									vNmSub = getMasterFacade().getTrioMstsubperkiraanDao()
											.findNamaSubByIdAndIdSub(vNoPerk, vNoSub);
								}

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
								jurnal.getTrioJurnalPK().setIdSub(vNoSub);
								jurnal.setNamaPerkiraan(vNmSub);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getDebet());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);

								if (terima.getsAHM().compareTo(NOL) == 1) {
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan("41204"); //41201
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan("POTONGAN PENJUALAN AHM");
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(terima.getsAHM());
									jurnal.setKredit(NOL);
									jurnal.setBlc("*");
									jurnal.setTipe("D");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);
								}

								if (terima.getsMD().compareTo(NOL) == 1) {
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan("41204"); //41201
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan("POTONGAN PENJUALAN MD");
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(terima.getsMD());
									jurnal.setKredit(NOL);
									jurnal.setBlc("*");
									jurnal.setTipe("D");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);
								}
								
								if (terima.getsSD().compareTo(NOL) == 1) {
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan("41204"); //41201
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan("POTONGAN PENJUALAN DEALER");
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(terima.getsSD());
									jurnal.setKredit(NOL);
									jurnal.setBlc("*");
									jurnal.setTipe("D");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);
								}

								BigDecimal biayaGabung = terima.getpSTNK().add(
										terima.getpBPKB().add(terima.getJmlTrm()));
								BigDecimal debetPer11 = terima.getDebet().divide(
										new BigDecimal(11), 0, RoundingMode.HALF_EVEN);
								BigDecimal total = debetPer11.add(biayaGabung);
								BigDecimal allHitung = terima.getDebet().subtract(total);

								if (allHitung.compareTo(NOL) == 1) {

									String vNoPerk3 = "41106"; //41101
									String vNmPerk3 = getMasterFacade()
											.getTrioMstperkiraanDao()
											.findNamaById(vNoPerk3);

									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk3);
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan(vNmPerk3);
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(NOL);
									jurnal.setKredit(allHitung);
									jurnal.setBlc("*");
									jurnal.setTipe("K");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);

								}

								String vNoPerk4 = "21404";
								String vNmPerk4 = getMasterFacade()
										.getTrioMstperkiraanDao().findNamaById(vNoPerk4);

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk4);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk4);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getDebet().divide(
										new BigDecimal(11), 0, RoundingMode.HALF_EVEN));
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);

								// if vs_ahm+vs_md+vs_sd>0
								BigDecimal jumlahSubsidi = terima.getsAHM().add(
										terima.getsMD().add(terima.getsSD()));

								if (jumlahSubsidi.compareTo(NOL) == 1) {
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan("41204"); //41201
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan("POTONGAN PENJUALAN");
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(NOL);
									jurnal.setKredit(jumlahSubsidi);
									jurnal.setBlc("*");
									jurnal.setTipe("K");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);
								}

								String vNoPerk5 = "21213"; //21204
								String vNmPerk5 = getMasterFacade()
										.getTrioMstperkiraanDao().findNamaById(vNoPerk5);

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk5);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk5);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getpSTNK());
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);

								String vNoPerk6 = "21214"; //21205
								String vNmPerk6 = getMasterFacade()
										.getTrioMstperkiraanDao().findNamaById(vNoPerk6);

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk6);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk6);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getpBPKB());
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);

								if (terima.getJmlTrm().compareTo(NOL) == 1) {
									String vNoPerk7 = "21309";
									String vNmPerk7 = getMasterFacade()
											.getTrioMstperkiraanDao()
											.findNamaById(vNoPerk7);

									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk7);
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan(vNmPerk7);
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(NOL);
									jurnal.setKredit(terima.getJmlTrm());
									jurnal.setBlc("*");
									jurnal.setTipe("K");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);
								}

							}
							String jenisJurnal = getJenisJurnalByParams(terima.getKet(),
									terima.getDebet(), terima.getKredit(),
									terima.getNoPerk());
							getMasterFacade().getTrioJurnalDao().saveTransaction(
									listJurnal, jenisJurnal, user);
							// case vno_perk='P0' and vket='1' AND vdinas<>'D'
						} else if (terima.getNoPerk().equalsIgnoreCase("P0")
								&& terima.getKet().equalsIgnoreCase("1")
								&& (!terima.getNoPerk2().equalsIgnoreCase("D"))) { 

							System.out.println("CASEIFCAB2 13");

							if (terima.getDebet().compareTo(NOL) == 1) {
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan("11327"); //11313
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan("PIUTANG PENJUALAN");
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getDebet());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);

								// vdpp=round(((vharga-vb_stnk-(vs_ahm+vs_md+vs_sd+vs_finance))*10/11),0)
								// vppnx=(vharga-vb_stnk-(vs_ahm+vs_md+vs_sd+vs_finance))-vdpp
								// vpot_penjualan=vs_ahm+vs_md+vs_sd+vs_finance

								BigDecimal totalSubsidi = NOL;
								BigDecimal hitungDPP = NOL;
								BigDecimal vDPP = NOL;

								BigDecimal vPPNX = NOL;
								BigDecimal vPotPenjualan = NOL;

								if (terima.getKet2().equalsIgnoreCase("ADR")
										|| terima.getKet2().equalsIgnoreCase("ADS")
										|| terima.getKet2().equalsIgnoreCase("OTO")) {

									totalSubsidi = terima.getsAHM().add(
											terima.getsMD().add(
													terima.getsSD().add(
															terima.getsFinance())));
									hitungDPP = terima.getHarga()
											.subtract(terima.getbSTNK())
											.subtract(totalSubsidi);
									vDPP = hitungDPP.multiply(new BigDecimal(10)).divide(
											new BigDecimal(11), 0, RoundingMode.HALF_EVEN);

									vPPNX = hitungDPP.subtract(vDPP);
									vPotPenjualan = totalSubsidi;

								} else {
									totalSubsidi = terima.getsAHM().add(
											terima.getsMD().add(terima.getsSD()));
									hitungDPP = terima.getHarga()
											.subtract(terima.getbSTNK())
											.subtract(totalSubsidi);
									vDPP = hitungDPP.multiply(new BigDecimal(10)).divide(
											new BigDecimal(11), 0, RoundingMode.HALF_EVEN);

									vPPNX = hitungDPP.subtract(vDPP);
									vPotPenjualan = totalSubsidi;
								}

								String vNoPerk1 = "41106"; //41101
								String vNoPerk2 = "21404";
								String vNoPerk3 = "21213"; //21204
								String vNoPerk4 = "21214"; //21205
								String vNoPerk5 = "21229"; //21228
								String vNoPerk6 = "41204"; //41201

								String vNmPerk1 = getMasterFacade()
										.getTrioMstperkiraanDao().findNamaById(vNoPerk1);

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk1);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk1);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(vDPP);
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);

								String vNmPerk2 = getMasterFacade()
										.getTrioMstperkiraanDao().findNamaById(vNoPerk2);

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk2);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk2);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(vPPNX);
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);

								if (vPotPenjualan.compareTo(NOL) == 1) {
									String vNmPerk6 = getMasterFacade()
											.getTrioMstperkiraanDao()
											.findNamaById(vNoPerk6);

									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk6);
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan(vNmPerk6);
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(NOL);
									jurnal.setKredit(vPotPenjualan);
									jurnal.setBlc("*");
									jurnal.setTipe("K");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);
								}

								String vNmPerk3 = getMasterFacade()
										.getTrioMstperkiraanDao().findNamaById(vNoPerk3);
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk3);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk3);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getpSTNK());
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);

								String vNmPerk4 = getMasterFacade()
										.getTrioMstperkiraanDao().findNamaById(vNoPerk4);
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk4);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk4);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getpBPKB());
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);

								String vNmPerk5 = getMasterFacade()
										.getTrioMstperkiraanDao().findNamaById(vNoPerk5);
								// vb_stnk-vp_stnk-vp_bpkb
								BigDecimal biaya = terima.getbSTNK()
										.subtract(terima.getpSTNK())
										.subtract(terima.getpBPKB());
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk5);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk5);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(biaya);
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);

							} else { //

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan("11327"); //11313
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan("PIUTANG PENJUALAN");
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getKredit());// bro
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);

								// vdpp=round(((vharga-vb_stnk-(vs_ahm+vs_md+vs_sd+vs_finance))*10/11),0)
								// vppnx=(vharga-vb_stnk-(vs_ahm+vs_md+vs_sd+vs_finance))-vdpp
								// vpot_penjualan=vs_ahm+vs_md+vs_sd+vs_finance

								BigDecimal totalSubsidi = NOL;
								BigDecimal hitungDPP = NOL;
								BigDecimal vDPP = NOL;

								BigDecimal vPPNX = NOL;
								BigDecimal vPotPenjualan = NOL;
								/*
								if (terima.getKet2().equalsIgnoreCase("ADR")
										|| terima.getKet2().equalsIgnoreCase("ADS")
										|| terima.getKet2().equalsIgnoreCase("OTO")) {

									totalSubsidi = terima.getsAHM().add(
											terima.getsMD().add(
													terima.getsSD().add(
															terima.getsFinance())));
									hitungDPP = terima.getHarga()
											.subtract(terima.getbSTNK())
											.subtract(totalSubsidi);
									vDPP = hitungDPP.multiply(new BigDecimal(10)).divide(
											new BigDecimal(11), 0, RoundingMode.HALF_EVEN);

									vPPNX = hitungDPP.subtract(vDPP);
									vPotPenjualan = totalSubsidi;

								} else {
									totalSubsidi = terima.getsAHM().add(
											terima.getsMD().add(terima.getsSD()));
									hitungDPP = terima.getHarga()
											.subtract(terima.getbSTNK())
											.subtract(totalSubsidi);
									vDPP = hitungDPP.multiply(new BigDecimal(10)).divide(
											new BigDecimal(11), 0, RoundingMode.HALF_EVEN);

									vPPNX = hitungDPP.subtract(vDPP);
									vPotPenjualan = totalSubsidi;
								}
								*/
								
								String vNoPerk1 = "41106"; //41101
								String vNoPerk2 = "21404";
								String vNoPerk3 = "21213"; //21204
								String vNoPerk4 = "21214"; //21205
								String vNoPerk5 = "21229"; //21228
								//String vNoPerk6 = "41201";
								BigDecimal hasilKurang = terima.getHarga().subtract(terima.getbSTNK());
								
								vDPP = (hasilKurang.multiply(new BigDecimal(10))).divide(new BigDecimal(11), 0, RoundingMode.HALF_EVEN);
								// hitung ( vharga-vb_stnk ) -vdpp
								BigDecimal hitung = hasilKurang.subtract(vDPP);
								String vNmPerk1 = getMasterFacade()
										.getTrioMstperkiraanDao().findNamaById(vNoPerk1);

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk1);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk1);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(vDPP);
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);

								String vNmPerk2 = getMasterFacade()
										.getTrioMstperkiraanDao().findNamaById(vNoPerk2);

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk2);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk2);
								jurnal.setKet(terima.getNmTran());
								// hitung ( vharga-vb_stnk ) -vdpp
								jurnal.setDebet(hitung);
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);
								/*
								if (vPotPenjualan.compareTo(NOL) == 1) {
									String vNmPerk6 = getMasterFacade()
											.getTrioMstperkiraanDao()
											.findNamaById(vNoPerk6);

									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk6);
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan(vNmPerk6);
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(vPotPenjualan);
									jurnal.setKredit(NOL);
									jurnal.setBlc("*");
									jurnal.setTipe("D");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);
								}
								*/
		
								String vNmPerk3 = getMasterFacade()
										.getTrioMstperkiraanDao().findNamaById(vNoPerk3);
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk3);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk3);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getpSTNK());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);

								String vNmPerk4 = getMasterFacade()
										.getTrioMstperkiraanDao().findNamaById(vNoPerk4);
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk4);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk4);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getpBPKB());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);

								String vNmPerk5 = getMasterFacade()
										.getTrioMstperkiraanDao().findNamaById(vNoPerk5);
								// vb_stnk-vp_stnk-vp_bpkb
								BigDecimal biaya = terima.getbSTNK()
										.subtract(terima.getpSTNK())
										.subtract(terima.getpBPKB());
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk5);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk5);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(biaya);
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);

							}
							String jenisJurnal = getJenisJurnalByParams(terima.getKet(),
									terima.getDebet(), terima.getKredit(),
									terima.getNoPerk());
							getMasterFacade().getTrioJurnalDao().saveTransaction(
									listJurnal, jenisJurnal, user);
							// CASE (vket='1' OR vket='K') AND (vno_perk='21205')
						} else if ((terima.getKet().equalsIgnoreCase("1") || terima
								.getKet().equalsIgnoreCase("K"))
								&& terima.getNoPerk().equalsIgnoreCase("21205")) { 

							System.out.println("CASEIFCAB2 14");

							if (terima.getDebet().compareTo(NOL) == 1) {
								vNoPerk = "11102"; //11101

								String vNmPerk = getMasterFacade().getTrioMstperkiraanDao()
										.findNamaById(vNoPerk);
								if (vNmPerk == null) {
									vNmPerk = "";
								}

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getDebet());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								listJurnal.add(jurnal);

								vNoPerk = "11494"; //11495
								
								vNmPerk = getMasterFacade().getTrioMstperkiraanDao()
										.findNamaById(vNoPerk);
								if (vNmPerk == null) {
									vNmPerk = "";
								}

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getDebet());
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								listJurnal.add(jurnal);

							} else { // else debet > 0
								vNoPerk = "11494"; //11495
								
								String vNmPerk = getMasterFacade().getTrioMstperkiraanDao()
										.findNamaById(vNoPerk);
								if (vNmPerk == null) {
									vNmPerk = "";
								}

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getKredit());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								listJurnal.add(jurnal);

								vNoPerk = "11102"; //11101
								vNmPerk = getMasterFacade().getTrioMstperkiraanDao()
										.findNamaById(vNoPerk);
								if (vNmPerk == null) {
									vNmPerk = "";
								}

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getKredit());
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								listJurnal.add(jurnal);
							}
							String jenisJurnal = getJenisJurnalByParams(terima.getKet(),
									terima.getDebet(), terima.getKredit(),
									terima.getNoPerk());
							getMasterFacade().getTrioJurnalDao().saveTransaction(
									listJurnal, jenisJurnal, user);

							// CASE (vket='1' OR vket='K') AND (vno_perk='21204')
						} else if ((terima.getKet().equalsIgnoreCase("1") || terima
								.getKet().equalsIgnoreCase("K"))	
								&& terima.getNoPerk().equalsIgnoreCase("21204")) {

							System.out.println("CASEIFCAB2 15");

							// TODO TO DO pending karena membaca tabel d_stnk

							// CASE (vket='1' OR vket='K') AND (vno_perk='21205')
						} 
						/*// caseifcab2 16 tidak ada
						else if ((terima.getKet().equalsIgnoreCase("1"))
								|| terima.getKet().equalsIgnoreCase("K")
								&& terima.getNoPerk().equalsIgnoreCase("21205")) {
							System.out.println("CASEIFCAB2 16");
							
							if (terima.getDebet().compareTo(NOL) == 1) {
								vNoPerk = "11101";
								String vNmPerk = getMasterFacade().getTrioMstperkiraanDao().findNamaById(vNoPerk);
								if (vNmPerk == null || vNmPerk.length() < 1){
									vNmPerk = "";
								}
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
								jurnal.getTrioJurnalPK().setIdSub(terima.getKdBank());
								jurnal.setNamaPerkiraan(vNmPerk);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getDebet());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								listJurnal.add(jurnal);
								
								vNoPerk = "11495";
								vNmPerk = getMasterFacade().getTrioMstperkiraanDao().findNamaById(vNoPerk);
								if (vNmPerk == null || vNmPerk.length() < 1){
									vNmPerk = "";
								}
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getDebet());
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								listJurnal.add(jurnal);
								
							} else { // else debet > 0
								vNoPerk = "11495";
								String vNmPerk = getMasterFacade().getTrioMstperkiraanDao().findNamaById(vNoPerk);
								if (vNmPerk == null || vNmPerk.length() < 1){
									vNmPerk = "";
								}
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
								jurnal.getTrioJurnalPK().setIdSub(terima.getKdBank());
								jurnal.setNamaPerkiraan(vNmPerk);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getKredit());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								listJurnal.add(jurnal);
								
								vNoPerk = "11101";
								vNmPerk = getMasterFacade().getTrioMstperkiraanDao().findNamaById(vNoPerk);
								if (vNmPerk == null || vNmPerk.length() < 1){
									vNmPerk = "";
								}
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getKredit());
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								listJurnal.add(jurnal);
							}

							String jenisJurnal = getJenisJurnalByParams(terima.getKet(),
									terima.getDebet(), terima.getKredit(),
									terima.getNoPerk());
							getMasterFacade().getTrioJurnalDao().saveTransaction(
									listJurnal, jenisJurnal, user);
							// CASE vket='2' AND (vno_perk='21204' OR vno_perk='21205')
						} 
						*/
						
						// ganti menjadi 21204-> 21213, 21205->21214
						else if (terima.getKet2().equalsIgnoreCase("2")
								&& (terima.getNoPerk().equalsIgnoreCase("21213") || terima
										.getNoPerk().equalsIgnoreCase("21214"))) {
							System.out.println("CASEIFCAB2 17");

							if (terima.getDebet().compareTo(NOL) == 1) {
								vNoPerk = "11202";
								String vNmPerk = "";
								if (terima.getKdBank().length() > 0) {
									vNmPerk = getMasterFacade().getTrioMstsubperkiraanDao()
											.findNamaSubByIdAndIdSub(vNoPerk,
													terima.getKdBank());
								} else {
									vNmPerk = "";
								}
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
								jurnal.getTrioJurnalPK().setIdSub(terima.getKdBank());
								jurnal.setNamaPerkiraan(vNmPerk);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getDebet());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								listJurnal.add(jurnal);

								vNoPerk = "11494"; //11495
								vNmPerk = "";
								if (terima.getKdBank().length() > 0) {
									vNmPerk = getMasterFacade().getTrioMstperkiraanDao()
											.findNamaById(vNoPerk);
								} else {
									vNmPerk = "";
								}
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getDebet());
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								listJurnal.add(jurnal);

							} else { // debet > 0

								vNoPerk = "11494"; //11495
								String vNmPerk = "";
								if (terima.getKdBank().length() > 0) {
									vNmPerk = getMasterFacade().getTrioMstperkiraanDao()
											.findNamaById(vNoPerk);
								} else {
									vNmPerk = "";
								}
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getKredit());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								listJurnal.add(jurnal);

								vNoPerk = "11202";
								vNmPerk = "";
								if (terima.getKdBank().length() > 0) {
									vNmPerk = getMasterFacade().getTrioMstsubperkiraanDao()
											.findNamaSubByIdAndIdSub(vNoPerk,
													terima.getKdBank());
								} else {
									vNmPerk = "";
								}
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
								jurnal.getTrioJurnalPK().setIdSub(terima.getKdBank());
								jurnal.setNamaPerkiraan(vNmPerk);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getKredit());
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								listJurnal.add(jurnal);

							}
							String jenisJurnal = getJenisJurnalByParams(terima.getKet(),
									terima.getDebet(), terima.getKredit(),
									terima.getNoPerk());
							getMasterFacade().getTrioJurnalDao().saveTransaction(
									listJurnal, jenisJurnal, user);
							// case vket='2' AND vno_perk<>'P2' AND vno_perk<>'P3' AND vno_perk<>'P1'
							 
						} else if (terima.getKet().equalsIgnoreCase("2")
								&& (!terima.getNoPerk().equalsIgnoreCase("P2"))
								&& (!terima.getNoPerk().equalsIgnoreCase("P3"))
								&& (!terima.getNoPerk().equalsIgnoreCase("P1"))) {

							System.out.println("CASEIFCAB2 18");

							String vNoPerkSTNK = terima.getNoPerk();

							if (terima.getDebet().compareTo(NOL) == 1) {
								vNoPerk = "11203";
								String vNmPerk = getMasterFacade().getTrioMstperkiraanDao()
										.findNamaById(vNoPerk);

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getDebet());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								listJurnal.add(jurnal);

								if (vNoPerkSTNK.equalsIgnoreCase("11406")) { //11405
									vNoPerk = "11406"; //11405
								} else {
									vNoPerk = "21217"; //21208
								}

								vNmPerk = getMasterFacade().getTrioMstperkiraanDao()
										.findNamaById(vNmPerk);
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getDebet());
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								listJurnal.add(jurnal);

							} else { // else debet > 0
								vNoPerk = null;
								if (vNoPerkSTNK.equalsIgnoreCase("11406")) { //11405
									vNoPerk = "11406"; //11405
								} else {
									vNoPerk = "21217"; //21208
								}

								String vNmPerk = getMasterFacade().getTrioMstperkiraanDao()
										.findNamaById(vNoPerk);
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getKredit());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								listJurnal.add(jurnal);

								vNoPerk = "11203";
								vNmPerk = getMasterFacade().getTrioMstperkiraanDao()
										.findNamaById(vNoPerk);
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getKredit());
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								listJurnal.add(jurnal);

							}
							String jenisJurnal = getJenisJurnalByParams(terima.getKet(),
									terima.getDebet(), terima.getKredit(),
									terima.getNoPerk());
							getMasterFacade().getTrioJurnalDao().saveTransaction(
									listJurnal, jenisJurnal, user);
							// cab 1
							// case vket='3' AND vno_perk<>'P2' AND vno_perk<>'P3' AND vno_perk<>'P1' AND vitr<>'*'
							// cab 2
							// case vket='3' AND vno_perk<>'P2' AND vno_perk<>'P3' AND vno_perk<>'P1'
						} else if (terima.getKet().equalsIgnoreCase("3")
								&& (!terima.getNoPerk().equalsIgnoreCase("P2"))
								&& (!terima.getNoPerk().equalsIgnoreCase("P3"))
								&& (!terima.getNoPerk().equalsIgnoreCase("P1")) ) {

							System.out.println("CASEIFCAB2 19");

							if (terima.getDebet().compareTo(NOL) == 1) {
								vNoPerk = "11202";
								String vNmPerk = "";
								if (terima.getKdBank().length() > 0) {
									vNmPerk = getMasterFacade().getTrioMstsubperkiraanDao()
											.findNamaSubByIdAndIdSub(vNoPerk,
													terima.getKdBank());
								}
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
								jurnal.getTrioJurnalPK().setIdSub(terima.getKdBank());
								jurnal.setNamaPerkiraan(vNmPerk);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getDebet());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								listJurnal.add(jurnal);

								vNoPerk = "21215"; //21206
								vNmPerk = getMasterFacade().getTrioMstperkiraanDao()
										.findNamaById(vNoPerk);

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getDebet());
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								listJurnal.add(jurnal);

							} else { // else debet > 0

								vNoPerk = "21215"; //21206
								String vNmPerk = getMasterFacade().getTrioMstperkiraanDao()
										.findNamaById(vNoPerk);

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getKredit());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								listJurnal.add(jurnal);

								vNoPerk = "11202";
								vNmPerk = "";
								if (terima.getKdBank().length() > 0) {
									vNmPerk = getMasterFacade().getTrioMstsubperkiraanDao()
											.findNamaSubByIdAndIdSub(vNoPerk,
													terima.getKdBank());
								} else {
									vNmPerk = "";
								}
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
								jurnal.getTrioJurnalPK().setIdSub(terima.getKdBank());
								jurnal.setNamaPerkiraan(vNmPerk);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getKredit());
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								listJurnal.add(jurnal);

							}
							String jenisJurnal = getJenisJurnalByParams(terima.getKet(),
									terima.getDebet(), terima.getKredit(),
									terima.getNoPerk());
							getMasterFacade().getTrioJurnalDao().saveTransaction(
									listJurnal, jenisJurnal, user);
							// case vno_perk='P3'
						} else if (terima.getNoPerk().equalsIgnoreCase("P3")) {

							System.out.println("CASEIFCAB2 20");

							if (terima.getDebet().compareTo(NOL) == 1) {
								if (terima.getKet().equalsIgnoreCase("1")
										|| terima.getKet().equalsIgnoreCase("K")) {
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan("11102"); //11101
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan("KAS");
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(terima.getDebet());
									jurnal.setKredit(NOL);
									jurnal.setBlc("*");
									jurnal.setTipe("D");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);
								}

								if (terima.getKet().equalsIgnoreCase("2")) {
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan("11203");
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan("CEK");
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(terima.getDebet());
									jurnal.setKredit(NOL);
									jurnal.setBlc("*");
									jurnal.setTipe("D");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);
								}

								if (terima.getKet().equalsIgnoreCase("3")) {
									vNoSub = terima.getKdBank();
									vNoPerk = "11202";
									String vNmPerk = "";
									if (vNoSub.length() > 0) {
										vNmPerk = getMasterFacade()
												.getTrioMstsubperkiraanDao()
												.findNamaSubByIdAndIdSub(vNoPerk, vNoSub);
									}

									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
									jurnal.getTrioJurnalPK().setIdSub(vNoSub);
									jurnal.setNamaPerkiraan(vNmPerk);
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(terima.getDebet());
									jurnal.setKredit(NOL);
									jurnal.setBlc("*");
									jurnal.setTipe("D");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);
								}

								if (terima.getKet().equalsIgnoreCase("4")) {
									vNoSub = terima.getKdPOS();
									vNoPerk = "11121"; //11120
									String vNmPerk = "";
									if (vNoSub.length() > 0) {
										vNmPerk = getMasterFacade()
												.getTrioMstsubperkiraanDao()
												.findNamaSubByIdAndIdSub(vNoPerk, vNoSub);
									}

									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
									jurnal.getTrioJurnalPK().setIdSub(vNoSub);
									jurnal.setNamaPerkiraan(vNmPerk);
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(terima.getDebet());
									jurnal.setKredit(NOL);
									jurnal.setBlc("*");
									jurnal.setTipe("D");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);
								}
								// if vharga>vdebet
								if (terima.getHarga().compareTo(terima.getDebet()) == 1) {
									String[] arrString = getNoPerkAndNmPerkForCab2(terima
											.getKet2());
									String vNoPerk0 = arrString[0];
									String vNmPerk0 = arrString[1];

									if (terima.getKet2().equalsIgnoreCase("FIF")
											|| terima.getKet2().equalsIgnoreCase("FIS")) {
										jurnal = new TrioJurnal();
										jurnal.setTglJurnal(terima.getTglTrm());
										jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk0);
										jurnal.getTrioJurnalPK().setIdSub("");
										jurnal.setNamaPerkiraan(vNmPerk0);
										jurnal.setKet(terima.getNmTran());
										// debet with vharga-vdebet-vs_finance
										BigDecimal vHarga = terima.getHarga()
												.subtract(terima.getDebet())
												.subtract(terima.getsFinance());
										jurnal.setDebet(vHarga);
										jurnal.setKredit(NOL);
										jurnal.setBlc("*");
										jurnal.setTipe("D");
										jurnal.setNoReff(terima.getTrioTerimaPK()
												.getNoFak());
										jurnal.setNoMesin(terima.getNoMesin());
										listJurnal.add(jurnal);
									} else {
										jurnal = new TrioJurnal();
										jurnal.setTglJurnal(terima.getTglTrm());
										jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk0);
										jurnal.getTrioJurnalPK().setIdSub("");
										jurnal.setNamaPerkiraan(vNmPerk0);
										jurnal.setKet(terima.getNmTran());
										// debet with vharga-vdebet
										BigDecimal vHarga = terima.getHarga().subtract(
												terima.getDebet());
										jurnal.setDebet(vHarga);
										jurnal.setKredit(NOL);
										jurnal.setBlc("*");
										jurnal.setTipe("D");
										jurnal.setNoReff(terima.getTrioTerimaPK()
												.getNoFak());
										jurnal.setNoMesin(terima.getNoMesin());
										listJurnal.add(jurnal);
									}

									if (terima.getsFinance().compareTo(NOL) == 1
											&& (terima.getKet2().equalsIgnoreCase("FIF") || terima
													.getKet2().equalsIgnoreCase("FIS"))) {
										jurnal = new TrioJurnal();
										jurnal.setTglJurnal(terima.getTglTrm());
										jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk0);
										jurnal.getTrioJurnalPK().setIdSub("");
										jurnal.setNamaPerkiraan(vNmPerk0 + " DP");
										jurnal.setKet(terima.getNmTran());
										jurnal.setDebet(terima.getsFinance());
										jurnal.setKredit(NOL);
										jurnal.setBlc("*");
										jurnal.setTipe("D");
										jurnal.setNoReff(terima.getTrioTerimaPK()
												.getNoFak());
										jurnal.setNoMesin(terima.getNoMesin());
										listJurnal.add(jurnal);
									}
								}//

								String vNoPerk1 = "41106"; //41101
								String vNoPerk2 = "21404";
								String vNoPerk3 = "21213"; //21204
								String vNoPerk4 = "21214"; //21205
								String vNoPerk5 = "21229"; //21228
								String vNoPerk6 = "41204"; //41201

								// vdpp=round(((vharga-vb_stnk-(vs_ahm+vs_md+vs_sd))*10/11),0)
								// vppnx=(vharga-vb_stnk-(vs_ahm+vs_md+vs_sd))-vdpp
								// vpot_penjualan=vs_ahm+vs_md+vs_sd

								BigDecimal totalSubsidi = terima.getsAHM()
										.add(terima.getsMD()).add(terima.getsSD());
								BigDecimal hitungDPP = terima.getHarga()
										.subtract(terima.getbSTNK()).subtract(totalSubsidi);
								BigDecimal vDPP = hitungDPP.multiply(new BigDecimal(10))
										.divide(new BigDecimal(11), 0,
												RoundingMode.HALF_EVEN);

								BigDecimal vPPNX = hitungDPP.subtract(vDPP);
								BigDecimal vPotPenjualan = totalSubsidi;

								String vNmPerk1 = getMasterFacade()
										.getTrioMstperkiraanDao().findNamaById(vNoPerk1);

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk1);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk1);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(vDPP);
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);

								String vNmPerk2 = getMasterFacade()
										.getTrioMstperkiraanDao().findNamaById(vNoPerk2);

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk2);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk2);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(vPPNX);
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);

								if (vPotPenjualan.compareTo(NOL) == 1) {

									String vNmPerk6 = getMasterFacade()
											.getTrioMstperkiraanDao()
											.findNamaById(vNoPerk6);

									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk6);
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan(vNmPerk6);
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(NOL);
									jurnal.setKredit(vPotPenjualan);
									jurnal.setBlc("*");
									jurnal.setTipe("K");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);
								}

								if (terima.getbSTNK().compareTo(NOL) == 1) {

									String vNmPerk3 = getMasterFacade()
											.getTrioMstperkiraanDao()
											.findNamaById(vNoPerk3);
									String vNmPerk4 = getMasterFacade()
											.getTrioMstperkiraanDao()
											.findNamaById(vNoPerk4);
									String vNmPerk5 = getMasterFacade()
											.getTrioMstperkiraanDao()
											.findNamaById(vNoPerk5);

									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk3);
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan(vNmPerk3);
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(NOL);
									jurnal.setKredit(terima.getpSTNK());
									jurnal.setBlc("*");
									jurnal.setTipe("K");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);

									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk4);
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan(vNmPerk4);
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(NOL);
									jurnal.setKredit(terima.getpBPKB());
									jurnal.setBlc("*");
									jurnal.setTipe("K");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);
									// vb_stnk-vp_stnk-vp_bpkb
									BigDecimal hitungBiaya = terima.getbSTNK()
											.subtract(terima.getpSTNK())
											.subtract(terima.getpBPKB());
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk5);
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan(vNmPerk5);
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(NOL);
									jurnal.setKredit(hitungBiaya);
									jurnal.setBlc("*");
									jurnal.setTipe("K");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);

								}// sampa sini wawwaw
									// vtamb_md+vtamb_sd+vtamb_fin>0 OR vhadiah_l='*'

								BigDecimal tambahan = terima.getTambMD()
										.add(terima.getTambSD()).add(terima.getTambFIN());

								BigDecimal tambahan2 = tambahan.divide(
										new BigDecimal(0.97), 0, RoundingMode.HALF_EVEN);
								if (tambahan.compareTo(NOL) == 1
										|| terima.getHadiah().equalsIgnoreCase("*")) {
									if (tambahan.compareTo(NOL) == 1) {

										String vNoPerkk = "61206"; //61106
										vNoSub = "04";
										String vNmSubPerkk = getMasterFacade()
												.getTrioMstsubperkiraanDao()
												.findNamaSubByIdAndIdSub(vNoPerkk, vNoSub);
										jurnal = new TrioJurnal();
										jurnal.setTglJurnal(terima.getTglTrm());
										jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerkk);
										jurnal.getTrioJurnalPK().setIdSub(vNoSub);
										jurnal.setNamaPerkiraan(vNmSubPerkk);
										jurnal.setKet(terima.getNmTran());
										jurnal.setDebet(tambahan2);
										jurnal.setKredit(NOL);
										jurnal.setBlc("*");
										jurnal.setTipe("D");
										jurnal.setNoReff(terima.getTrioTerimaPK()
												.getNoFak());
										listJurnal.add(jurnal);
									}

									if (terima.getTambFIN().compareTo(NOL) == 1) {

										String[] arrStr = getNoPerkAndNmPerkForCab2(terima
												.getKet2());
										String vNoPerk0 = arrStr[0];
										String vNmPerk0 = arrStr[1];
										if (terima.getKet2().equalsIgnoreCase("ADR")
												|| terima.getKet2().equalsIgnoreCase("ADS")
												|| terima.getKet2().equalsIgnoreCase("OTO")
												) { // pada cabang 1 ada || terima.getKet2().equalsIgnoreCase("WOM")

											jurnal = new TrioJurnal();
											jurnal.setTglJurnal(terima.getTglTrm());
											jurnal.getTrioJurnalPK().setIdPerkiraan(
													vNoPerk0);
											jurnal.getTrioJurnalPK().setIdSub("");
											jurnal.setNamaPerkiraan(vNmPerk0);
											jurnal.setKet(terima.getNmTran());
											BigDecimal debetTemp = NOL;
											if (terima.getKet2().equalsIgnoreCase("ADR")
													|| terima.getKet2().equalsIgnoreCase(
															"ADS")) {
												// vtamb_fin*11/10
												debetTemp = terima
														.getTambFIN()
														.multiply(new BigDecimal(11))
														.divide(new BigDecimal(10), 0,
																RoundingMode.HALF_EVEN);
											} else {
												debetTemp = terima.getTambFIN();
											}
											jurnal.setDebet(debetTemp);
											jurnal.setKredit(NOL);
											jurnal.setBlc("*");
											jurnal.setTipe("D");
											jurnal.setNoReff(terima.getTrioTerimaPK()
													.getNoFak());
											listJurnal.add(jurnal);
											String vNoSub71204 = "";
											if (terima.getKet2().equalsIgnoreCase("ADR")) {
												vNoSub71204 = "01";
											} else if (terima.getKet2().equalsIgnoreCase(
													"ADS")) {
												vNoSub71204 = "02";
											} else if (terima.getKet2().equalsIgnoreCase(
													"OTO")) {
												vNoSub71204 = "03";
											} else if (terima.getKet2().equalsIgnoreCase(
													"WOM")) {
												vNoSub71204 = "04";
											}
											String vNmSub = getMasterFacade()
													.getTrioMstsubperkiraanDao()
													.findNamaSubByIdAndIdSub("71204",
															vNoSub71204); //71104
											jurnal = new TrioJurnal();
											jurnal.setTglJurnal(terima.getTglTrm());
											jurnal.getTrioJurnalPK()
													.setIdPerkiraan("71204");
											jurnal.getTrioJurnalPK().setIdSub(vNoSub71204);
											jurnal.setNamaPerkiraan(vNmSub);
											jurnal.setKet(terima.getNmTran());
											jurnal.setDebet(NOL);
											BigDecimal tempKredit = NOL;
											if (terima.getKet2().equalsIgnoreCase("ADR")
													|| terima.getKet2().equalsIgnoreCase(
															"ADS")) {
												tempKredit = terima.getTambFIN();
											} else {
												// ROUND(vtamb_fin*10/11,0)
												tempKredit = terima
														.getTambFIN()
														.multiply(new BigDecimal(10))
														.divide(new BigDecimal(11), 0,
																RoundingMode.HALF_EVEN);
											}
											jurnal.setKredit(tempKredit);
											jurnal.setBlc("*");
											jurnal.setTipe("K");
											listJurnal.add(jurnal);

											String vNmPerk = getMasterFacade()
													.getTrioMstperkiraanDao().findNamaById(
															"21404");
											jurnal = new TrioJurnal();
											jurnal.setTglJurnal(terima.getTglTrm());
											jurnal.getTrioJurnalPK()
													.setIdPerkiraan("21404");
											jurnal.getTrioJurnalPK().setIdSub("");
											jurnal.setNamaPerkiraan(vNmPerk);
											jurnal.setKet(terima.getNmTran());
											jurnal.setDebet(NOL);
											tempKredit = NOL;
											if (terima.getKet2().equalsIgnoreCase("ADR")
													|| terima.getKet2().equalsIgnoreCase(
															"ADS")) {
												// ROUND((vtamb_fin)*0.1,0)
												tempKredit = terima
														.getTambFIN()
														.multiply(new BigDecimal(0.1))
														.setScale(0, RoundingMode.HALF_EVEN);
											} else {
												// ROUND((vtamb_fin*10/11)*0.1,0)
												tempKredit = terima
														.getTambFIN()
														.multiply(new BigDecimal(10))
														.divide(new BigDecimal(11), 0,
																RoundingMode.HALF_EVEN)
														.multiply(new BigDecimal(0.1))
														.setScale(0, RoundingMode.HALF_EVEN);
											}
											jurnal.setKredit(tempKredit);
											jurnal.setBlc("*");
											jurnal.setTipe("K");
											listJurnal.add(jurnal);

										}// IF vket2<>'ADR' AND vket2<>'ADS' AND
											// vket2<>'OTO' AND vket2<>'WOM' AND
											// vhadiah_l='*'
										BigDecimal tambFin = terima.getTambFIN().add(
												terima.getsFinance());
										
										if ((!terima.getKet2().equalsIgnoreCase("ADR"))
												&& (!terima.getKet2().equalsIgnoreCase("ADS"))
												&& (!terima.getKet2().equalsIgnoreCase("OTO")
														&& (!terima.getKet2().equalsIgnoreCase("WOM")) 
														&& (tambFin.compareTo(NOL)==1)
														&& terima.getHadiah().equalsIgnoreCase("*"))) {

											
												String[] arrStr0 = getNoPerkAndNmPerkForCab2(terima
														.getKet2());
												vNoPerk = arrStr0[0];
												String vNmPerk = arrStr0[1];

												jurnal = new TrioJurnal();
												jurnal.setTglJurnal(terima.getTglTrm());
												jurnal.getTrioJurnalPK().setIdPerkiraan(
														vNoPerk);
												jurnal.getTrioJurnalPK().setIdSub("");
												jurnal.setNamaPerkiraan(vNmPerk);
												jurnal.setKet(terima.getNmTran());
												jurnal.setDebet(tambFin);
												jurnal.setKredit(NOL);
												jurnal.setBlc("*");
												jurnal.setTipe("D");
												jurnal.setNoReff(terima.getTrioTerimaPK()
														.getNoFak());
												listJurnal.add(jurnal);

												String vNoPerkk = "61206"; //61106
												String vNoSubk = "04";

												String vNmPerkk = getMasterFacade()
														.getTrioMstsubperkiraanDao()
														.findNamaSubByIdAndIdSub(vNoPerkk,
																vNoSubk);
												jurnal = new TrioJurnal();
												jurnal.setTglJurnal(terima.getTglTrm());
												jurnal.getTrioJurnalPK().setIdPerkiraan(
														vNoPerkk);
												jurnal.getTrioJurnalPK().setIdSub(vNoSubk);
												jurnal.setNamaPerkiraan(vNmPerk);
												jurnal.setKet(terima.getNmTran());
												jurnal.setDebet(NOL);
												jurnal.setKredit(tambFin);
												jurnal.setBlc("*");
												jurnal.setTipe("K");
												jurnal.setNoReff(terima.getTrioTerimaPK()
														.getNoFak());
												listJurnal.add(jurnal);

											// IF vtamb_md+vtamb_sd+vtamb_fin>0
											BigDecimal tambTriple = terima.getTambMD()
													.add(terima.getTambSD())
													.add(terima.getTambFIN());
											if (tambTriple.compareTo(NOL) == 1) {
												vNoPerkk = "21309";
												vNmPerkk = getMasterFacade()
														.getTrioMstperkiraanDao()
														.findNamaById(vNoPerkk);

												jurnal = new TrioJurnal();
												jurnal.setTglJurnal(terima.getTglTrm());
												jurnal.getTrioJurnalPK().setIdPerkiraan(
														vNoPerkk);
												jurnal.getTrioJurnalPK().setIdSub("");
												jurnal.setNamaPerkiraan(vNmPerkk);
												jurnal.setKet(terima.getNmTran());
												jurnal.setDebet(NOL);
												jurnal.setKredit(tambTriple);
												jurnal.setBlc("*");
												jurnal.setTipe("K");
												jurnal.setNoReff(terima.getTrioTerimaPK()
														.getNoFak());
												listJurnal.add(jurnal);

												vNoPerkk = "21305";
												vNmPerkk = getMasterFacade()
														.getTrioMstperkiraanDao()
														.findNamaById(vNoPerkk);

												jurnal = new TrioJurnal();
												jurnal.setTglJurnal(terima.getTglTrm());
												jurnal.getTrioJurnalPK().setIdPerkiraan(
														vNoPerkk);
												jurnal.getTrioJurnalPK().setIdSub("");
												jurnal.setNamaPerkiraan(vNmPerkk);
												jurnal.setKet(terima.getNmTran());
												jurnal.setDebet(NOL);
												// ROUND(ROUND((vtamb_md+vtamb_sd+vtamb_fin)/0.97,0)*0.03,0)
												BigDecimal credit = tambTriple
														.divide(new BigDecimal(0.97), 0,
																RoundingMode.HALF_EVEN)
														.multiply(new BigDecimal(0.03))
														.setScale(0, RoundingMode.HALF_EVEN);
												jurnal.setKredit(credit);
												jurnal.setBlc("*");
												jurnal.setTipe("K");
												jurnal.setNoReff(terima.getTrioTerimaPK()
														.getNoFak());
												listJurnal.add(jurnal);

											}
										}//
										String[] arrayPerk = getNoPerkAndNmPerkForCab2(terima
												.getKet2());
										vNoPerk = arrayPerk[0];
										String vNmPerk = arrayPerk[1];
										
										List<TrioTerima> listTerima2 = findByNoLKHAndNoFakAndKet("KUPON", terima.getTrioTerimaPK().getNoFak(),"X");

										for (TrioTerima trm : listTerima2) {
											if (terima.getKet2().equalsIgnoreCase("ADR")
													|| terima.getKet2().equalsIgnoreCase(
															"ADS")
													|| terima.getKet2().equalsIgnoreCase(
															"OTO")
													|| terima.getKet2().equalsIgnoreCase(
															"WOM")) {
												if (terima.getKet2()
														.equalsIgnoreCase("OTO")) {
													jurnal = new TrioJurnal();
													jurnal.setTglJurnal(terima.getTglTrm());
													jurnal.getTrioJurnalPK()
															.setIdPerkiraan(vNoPerk0);
													jurnal.getTrioJurnalPK().setIdSub("");
													jurnal.setNamaPerkiraan(vNmPerk0);
													jurnal.setKet(trm.getNmTran() + " A.N "
															+ terima.getNmCust());
													// debet with vqcari1.debet*(10/11)
													BigDecimal dbt = trm
															.getDebet()
															.multiply(
																	new BigDecimal(10)
																			.divide(new BigDecimal(
																					11),
																					0,
																					RoundingMode.HALF_EVEN));
													jurnal.setDebet(dbt);
													jurnal.setKredit(NOL);
													jurnal.setBlc("*");
													jurnal.setTipe("D");
													jurnal.setNoReff(terima
															.getTrioTerimaPK().getNoFak());
													listJurnal.add(jurnal);

													jurnal = new TrioJurnal();
													jurnal.setTglJurnal(terima.getTglTrm());
													jurnal.getTrioJurnalPK()
															.setIdPerkiraan("71204"); //71104
													jurnal.getTrioJurnalPK().setIdSub("03");
													jurnal.setNamaPerkiraan("JASA PERANTARA OTO");
													jurnal.setKet(trm.getNmTran() + " A.N "
															+ terima.getNmCust());
													// debet with
													// vqcari1.debet-(vqcari1.debet*(10/11))
													dbt = trm.getDebet().subtract(dbt);
													jurnal.setDebet(dbt);
													jurnal.setKredit(NOL);
													jurnal.setBlc("*");
													jurnal.setTipe("D");
													jurnal.setNoReff(terima
															.getTrioTerimaPK().getNoFak());
													listJurnal.add(jurnal);
												} else {
													jurnal = new TrioJurnal();
													jurnal.setTglJurnal(terima.getTglTrm());
													jurnal.getTrioJurnalPK()
															.setIdPerkiraan(vNoPerk0);
													jurnal.getTrioJurnalPK().setIdSub("");
													jurnal.setNamaPerkiraan(vNmPerk);
													jurnal.setKet(trm.getNmTran() + " A.N "
															+ terima.getNmCust());
													jurnal.setDebet(trm.getDebet());
													jurnal.setKredit(NOL);
													jurnal.setBlc("*");
													jurnal.setTipe("D");
													jurnal.setNoReff(terima
															.getTrioTerimaPK().getNoFak());
													listJurnal.add(jurnal);
												}
											} else {
												jurnal = new TrioJurnal();
												jurnal.setTglJurnal(terima.getTglTrm());
												jurnal.getTrioJurnalPK().setIdPerkiraan(
														vNoPerk0);
												jurnal.getTrioJurnalPK().setIdSub("");
												jurnal.setNamaPerkiraan(vNmPerk);
												jurnal.setKet(trm.getNmTran() + " A.N "
														+ terima.getNmCust());
												jurnal.setDebet(trm.getDebet());
												jurnal.setKredit(NOL);
												jurnal.setBlc("*");
												jurnal.setTipe("D");
												jurnal.setNoReff(terima.getTrioTerimaPK()
														.getNoFak());
												listJurnal.add(jurnal);
											}
											// sampai sini brorrrrr
											// IF vqcari1.no_perk='21116' AND
											// vqcari1.no_sub='01'
											if (trm.getNoPerk().equalsIgnoreCase("21116")
													&& trm.getNoSub()
															.equalsIgnoreCase("01")) {
												if (terima.getKet2()
														.equalsIgnoreCase("ADR")
														|| terima.getKet2()
																.equalsIgnoreCase("ADS")
														|| terima.getKet2()
																.equalsIgnoreCase("OTO")
														|| terima.getKet2()
																.equalsIgnoreCase("WOM")) {
													String vNoPerkk = "71205";//71105 
													String vNoSubk = trm.getNoSub();
													String vNmPerkk = "";
													if (vNoSubk.length() > 0) {
														vNmPerkk = getMasterFacade()
																.getTrioMstsubperkiraanDao()
																.findNamaSubByIdAndIdSub(
																		vNoPerkk, vNoSubk);
													} else {
														vNmPerkk = getMasterFacade()
																.getTrioMstperkiraanDao()
																.findNamaById(vNoPerkk);
													}

													jurnal = new TrioJurnal();
													jurnal.setTglJurnal(terima.getTglTrm());
													jurnal.getTrioJurnalPK()
															.setIdPerkiraan(vNoPerkk);
													jurnal.getTrioJurnalPK().setIdSub(
															vNoSubk);
													jurnal.setNamaPerkiraan(vNmPerkk);
													jurnal.setKet(trm.getNmTran() + " A.N "
															+ terima.getNmCust());
													jurnal.setDebet(NOL);
													BigDecimal kredit = trm
															.getDebet()
															.multiply(
																	new BigDecimal(10)
																			.divide(new BigDecimal(
																					11),
																					0,
																					RoundingMode.HALF_EVEN));
													jurnal.setKredit(kredit);
													jurnal.setBlc("*");
													jurnal.setTipe("K");
													jurnal.setNoReff(terima
															.getTrioTerimaPK().getNoFak());
													listJurnal.add(jurnal);

													vNoPerkk = "21404";
													vNmPerkk = getMasterFacade()
															.getTrioMstperkiraanDao()
															.findNamaById(vNoPerkk);
													if (vNoPerk.length() < 1) {
														vNmPerkk = "";
													}

													jurnal = new TrioJurnal();
													jurnal.setTglJurnal(terima.getTglTrm());
													jurnal.getTrioJurnalPK()
															.setIdPerkiraan(vNoPerkk);
													jurnal.getTrioJurnalPK().setIdSub(
															vNoSubk);
													jurnal.setNamaPerkiraan(vNmPerkk);
													jurnal.setKet(trm.getNmTran() + " A.N "
															+ terima.getNmCust());
													jurnal.setDebet(NOL);
													// kredit with
													// ROUND((vqcari1.debet*(10/11))*0.1,0)
													kredit = trm
															.getDebet()
															.multiply(
																	new BigDecimal(10)
																			.divide(new BigDecimal(
																					11),
																					0,
																					RoundingMode.HALF_EVEN))
															.multiply(new BigDecimal(0.1))
															.setScale(0,
																	RoundingMode.HALF_EVEN);
													jurnal.setKredit(kredit);
													jurnal.setBlc("*");
													jurnal.setTipe("K");
													jurnal.setNoReff(terima
															.getTrioTerimaPK().getNoFak());
													listJurnal.add(jurnal);

												} else {
													String vNoPerkk = "61204"; //61104
													String vNoSubk = "06";
													String vNmPerkk = getMasterFacade()
															.getTrioMstsubperkiraanDao()
															.findNamaSubByIdAndIdSub(
																	vNoPerkk, vNoSubk);
													if (vNmPerkk.length() < 1) {
														vNmPerkk = "";
													}
													jurnal = new TrioJurnal();
													jurnal.setTglJurnal(terima.getTglTrm());
													jurnal.getTrioJurnalPK()
															.setIdPerkiraan(vNoPerkk);
													jurnal.getTrioJurnalPK().setIdSub(
															vNoSubk);
													jurnal.setNamaPerkiraan(vNmPerkk);
													jurnal.setKet(trm.getNmTran() + " A.N "
															+ terima.getNmCust());
													jurnal.setDebet(NOL);
													jurnal.setKredit(trm.getDebet());
													jurnal.setBlc("*");
													jurnal.setTipe("K");
													jurnal.setNoReff(terima
															.getTrioTerimaPK().getNoFak());
													listJurnal.add(jurnal);

												}
											} else { // kurang disini ternyata
												// IF (vket2='ADR' OR vket2='ADS' OR
												// vket2='OTO' OR vket2='WOM')
												if (terima.getKet2()
														.equalsIgnoreCase("ADR")
														|| terima.getKet2()
																.equalsIgnoreCase("ADS")
														|| terima.getKet2()
																.equalsIgnoreCase("OTO")
														|| terima.getKet2()
																.equalsIgnoreCase("WOM")) {
													String vNoPerkk = "71205"; //71105
													String vNoSubk = trm.getNoSub();
													String vNmPerkk = "";
													if (vNoSubk.length() > 0) {
														vNmPerkk = getMasterFacade()
																.getTrioMstsubperkiraanDao()
																.findNamaSubByIdAndIdSub(
																		vNoPerkk, vNoSubk);
													} else {
														vNmPerkk = getMasterFacade()
																.getTrioMstperkiraanDao()
																.findNamaById(vNoPerkk);
													}
													jurnal = new TrioJurnal();
													jurnal.setTglJurnal(terima.getTglTrm());
													jurnal.getTrioJurnalPK()
															.setIdPerkiraan(vNoPerkk);
													jurnal.getTrioJurnalPK().setIdSub(
															vNoSubk);
													jurnal.setNamaPerkiraan(vNmPerkk);
													jurnal.setKet(trm.getNmTran() + " A.N "
															+ terima.getNmCust());
													jurnal.setDebet(NOL);
													BigDecimal temp = trm
															.getDebet()
															.multiply(
																	new BigDecimal(10)
																			.divide(new BigDecimal(
																					11),
																					0,
																					RoundingMode.HALF_EVEN));
													jurnal.setKredit(temp);
													jurnal.setBlc("*");
													jurnal.setTipe("K");
													jurnal.setNoReff(terima
															.getTrioTerimaPK().getNoFak());
													listJurnal.add(jurnal);

													vNoPerkk = "21404";
													vNmPerkk = getMasterFacade()
															.getTrioMstperkiraanDao()
															.findNamaById(vNoPerkk);
													if (vNmPerkk.length() < 1) {
														vNmPerkk = "";
													}
													jurnal = new TrioJurnal();
													jurnal.setTglJurnal(terima.getTglTrm());
													jurnal.getTrioJurnalPK()
															.setIdPerkiraan(vNoPerkk);
													jurnal.getTrioJurnalPK().setIdSub("");
													jurnal.setNamaPerkiraan(vNmPerkk);
													jurnal.setKet(trm.getNmTran() + " A.N "
															+ terima.getNmCust());
													jurnal.setDebet(NOL);
													// kredit with
													// ROUND((vqcari1.debet*(10/11))*0.1,0)
													temp = trm
															.getDebet()
															.multiply(
																	new BigDecimal(10)
																			.divide(new BigDecimal(
																					11),
																					0,
																					RoundingMode.HALF_EVEN));
													temp = temp.multiply(
															new BigDecimal(0.1)).setScale(
															0, RoundingMode.HALF_EVEN);
													jurnal.setKredit(temp);
													jurnal.setBlc("*");
													jurnal.setTipe("K");
													jurnal.setNoReff(terima
															.getTrioTerimaPK().getNoFak());
													listJurnal.add(jurnal);
												} else {
													String vNoPerkk = "21117"; // trm.getNoPerk();
													String vNoSubk = trm.getNoSub();
													String vNmPerkk = "";
													if (vNoSubk.length() > 0) {
														vNmPerkk = getMasterFacade()
																.getTrioMstsubperkiraanDao()
																.findNamaSubByIdAndIdSub(
																		vNoPerkk, vNoSubk);
													} else {
														vNmPerkk = getMasterFacade()
																.getTrioMstperkiraanDao()
																.findNamaById(vNoPerkk);
													}
													jurnal = new TrioJurnal();
													jurnal.setTglJurnal(terima.getTglTrm());
													jurnal.getTrioJurnalPK()
															.setIdPerkiraan(vNoPerkk);
													jurnal.getTrioJurnalPK().setIdSub(
															vNoSubk);
													jurnal.setNamaPerkiraan(vNmPerkk);
													jurnal.setKet(trm.getNmTran() + " A.N "
															+ terima.getNmCust());
													jurnal.setDebet(NOL);
													jurnal.setKredit(trm.getDebet());
													jurnal.setBlc("*");
													jurnal.setTipe("K");
													jurnal.setNoReff(terima
															.getTrioTerimaPK().getNoFak());
													listJurnal.add(jurnal);

												}
											}

										}
									}
								}

							} else { // else debet > 0

								if (terima.getKet().equalsIgnoreCase("1")
										|| terima.getKet().equalsIgnoreCase("K")) {
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan("11102"); //11101
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan("KAS");
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(NOL);
									jurnal.setKredit(terima.getKredit());
									jurnal.setBlc("*");
									jurnal.setTipe("K");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);
								}

								if (terima.getKet().equalsIgnoreCase("2")) {
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan("11203");
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan("CEK");
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(NOL);
									jurnal.setKredit(terima.getKredit());
									jurnal.setBlc("*");
									jurnal.setTipe("K");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);
								}

								if (terima.getKet().equalsIgnoreCase("3")) {
									vNoSub = terima.getKdBank();
									vNoPerk = "11202";
									String vNmPerk = "";
									if (vNoSub.length() > 0) {
										vNmPerk = getMasterFacade()
												.getTrioMstsubperkiraanDao()
												.findNamaSubByIdAndIdSub(vNoPerk, vNoSub);
									}

									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
									jurnal.getTrioJurnalPK().setIdSub(vNoSub);
									jurnal.setNamaPerkiraan(vNmPerk);
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(NOL);
									jurnal.setKredit(terima.getKredit());
									jurnal.setBlc("*");
									jurnal.setTipe("K");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);
								}

								if (terima.getKet().equalsIgnoreCase("4")) {
									vNoSub = terima.getKdPOS();
									vNoPerk = "11121"; //11120
									String vNmPerk = "";
									if (vNoSub.length() > 0) {
										vNmPerk = getMasterFacade()
												.getTrioMstsubperkiraanDao()
												.findNamaSubByIdAndIdSub(vNoPerk, vNoSub);
									}

									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
									jurnal.getTrioJurnalPK().setIdSub(vNoSub);
									jurnal.setNamaPerkiraan(vNmPerk);
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(NOL);
									jurnal.setKredit(terima.getKredit());
									jurnal.setBlc("*");
									jurnal.setTipe("K");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);
								}
								// if vharga>vdebet
								
								if (terima.getHarga().compareTo(terima.getDebet()) == 1) {
									String[] arrString = getNoPerkAndNmPerkForCab2(terima
											.getKet2());
									String vNoPerk0 = arrString[0];
									String vNmPerk0 = arrString[1];

									if (terima.getKet2().equalsIgnoreCase("FIF")
											|| terima.getKet2().equalsIgnoreCase("FIS")) {
										jurnal = new TrioJurnal();
										jurnal.setTglJurnal(terima.getTglTrm());
										jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk0);
										jurnal.getTrioJurnalPK().setIdSub("");
										jurnal.setNamaPerkiraan(vNmPerk0);
										jurnal.setKet(terima.getNmTran());
										// debet with vharga-vdebet-vs_finance
										BigDecimal vHarga = terima.getHarga()
												.subtract(terima.getKredit())
												.subtract(terima.getsFinance());
										jurnal.setDebet(NOL);
										jurnal.setKredit(vHarga);
										jurnal.setBlc("*");
										jurnal.setTipe("K");
										jurnal.setNoReff(terima.getTrioTerimaPK()
												.getNoFak());
										jurnal.setNoMesin(terima.getNoMesin());
										listJurnal.add(jurnal);
									} else {
										jurnal = new TrioJurnal();
										jurnal.setTglJurnal(terima.getTglTrm());
										jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk0);
										jurnal.getTrioJurnalPK().setIdSub("");
										jurnal.setNamaPerkiraan(vNmPerk0);
										jurnal.setKet(terima.getNmTran());
										// debet with vharga-vdebet
										BigDecimal vHarga = terima.getHarga().subtract(
												terima.getKredit());
										jurnal.setDebet(NOL);
										jurnal.setKredit(vHarga);
										jurnal.setBlc("*");
										jurnal.setTipe("K");
										jurnal.setNoReff(terima.getTrioTerimaPK()
												.getNoFak());
										jurnal.setNoMesin(terima.getNoMesin());
										listJurnal.add(jurnal);
									}

									if (terima.getsFinance().compareTo(NOL) == 1
											&& (terima.getKet2().equalsIgnoreCase("FIF") || terima
													.getKet2().equalsIgnoreCase("FIS"))) {
										jurnal = new TrioJurnal();
										jurnal.setTglJurnal(terima.getTglTrm());
										jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk0);
										jurnal.getTrioJurnalPK().setIdSub("");
										jurnal.setNamaPerkiraan(vNmPerk0 + " DP");
										jurnal.setKet(terima.getNmTran());
										jurnal.setDebet(NOL);
										jurnal.setKredit(terima.getsFinance());
										jurnal.setBlc("*");
										jurnal.setTipe("K");
										jurnal.setNoReff(terima.getTrioTerimaPK()
												.getNoFak());
										jurnal.setNoMesin(terima.getNoMesin());
										listJurnal.add(jurnal);
									}
								}//

								String vNoPerk1 = "41106"; // 41101
								String vNoPerk2 = "21404";
								String vNoPerk3 = "21213"; //21204
								String vNoPerk4 = "21214"; //21205
								String vNoPerk5 = "21229"; //21228
								String vNoPerk6 = "41204"; //41201

								// vdpp=round(((vharga-vb_stnk-(vs_ahm+vs_md+vs_sd))*10/11),0)
								// vppnx=(vharga-vb_stnk-(vs_ahm+vs_md+vs_sd))-vdpp
								// vpot_penjualan=vs_ahm+vs_md+vs_sd

								BigDecimal totalSubsidi = terima.getsAHM()
										.add(terima.getsMD()).add(terima.getsSD());
								BigDecimal hitungDPP = terima.getHarga()
										.subtract(terima.getbSTNK()).subtract(totalSubsidi);
								BigDecimal vDPP = hitungDPP.multiply(new BigDecimal(10))
										.divide(new BigDecimal(11), 0,
												RoundingMode.HALF_EVEN);

								BigDecimal vPPNX = hitungDPP.subtract(vDPP);
								BigDecimal vPotPenjualan = totalSubsidi;

								String vNmPerk1 = getMasterFacade()
										.getTrioMstperkiraanDao().findNamaById(vNoPerk1);

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk1);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk1);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(vDPP);
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);

								String vNmPerk2 = getMasterFacade()
										.getTrioMstperkiraanDao().findNamaById(vNoPerk2);

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk2);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk2);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(vPPNX);
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);

								if (vPotPenjualan.compareTo(NOL) == 1) {

									String vNmPerk6 = getMasterFacade()
											.getTrioMstperkiraanDao()
											.findNamaById(vNoPerk6);

									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk6);
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan(vNmPerk6);
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(vPotPenjualan);
									jurnal.setKredit(NOL);
									jurnal.setBlc("*");
									jurnal.setTipe("D");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);
								}

								if (terima.getbSTNK().compareTo(NOL) == 1) {

									String vNmPerk3 = getMasterFacade()
											.getTrioMstperkiraanDao()
											.findNamaById(vNoPerk3);
									String vNmPerk4 = getMasterFacade()
											.getTrioMstperkiraanDao()
											.findNamaById(vNoPerk4);
									String vNmPerk5 = getMasterFacade()
											.getTrioMstperkiraanDao()
											.findNamaById(vNoPerk5);

									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk3);
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan(vNmPerk3);
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(terima.getpSTNK());
									jurnal.setKredit(NOL);
									jurnal.setBlc("*");
									jurnal.setTipe("D");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);

									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk4);
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan(vNmPerk4);
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(terima.getpBPKB());
									jurnal.setKredit(NOL);
									jurnal.setBlc("*");
									jurnal.setTipe("D");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);
									// vb_stnk-vp_stnk-vp_bpkb
									BigDecimal hitungBiaya = terima.getbSTNK()
											.subtract(terima.getpSTNK())
											.subtract(terima.getpBPKB());
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk5);
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan(vNmPerk5);
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(hitungBiaya);
									jurnal.setKredit(NOL);
									jurnal.setBlc("*");
									jurnal.setTipe("D");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);

								}// sampa sini wawwaw
									// vtamb_md+vtamb_sd+vtamb_fin>0 OR vhadiah_l='*'

								BigDecimal tambahan = terima.getTambMD()
										.add(terima.getTambSD()).add(terima.getTambFIN());

								BigDecimal tambahan2 = tambahan.divide(
										new BigDecimal(0.97), 0, RoundingMode.HALF_EVEN);
								if (tambahan.compareTo(NOL) == 1
										|| terima.getHadiah().equalsIgnoreCase("*")) {
									if (tambahan.compareTo(NOL) == 1) {

										String vNoPerkk = "61106";
										vNoSub = "04";
										String vNmSubPerkk = getMasterFacade()
												.getTrioMstsubperkiraanDao()
												.findNamaSubByIdAndIdSub(vNoPerkk, vNoSub);
										jurnal = new TrioJurnal();
										jurnal.setTglJurnal(terima.getTglTrm());
										jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerkk);
										jurnal.getTrioJurnalPK().setIdSub(vNoSub);
										jurnal.setNamaPerkiraan(vNmSubPerkk);
										jurnal.setKet(terima.getNmTran());
										jurnal.setDebet(NOL);
										jurnal.setKredit(tambahan2);
										jurnal.setBlc("*");
										jurnal.setTipe("K");
										jurnal.setNoReff(terima.getTrioTerimaPK()
												.getNoFak());
										listJurnal.add(jurnal);
									}

									if (terima.getTambFIN().compareTo(NOL) == 1) {

										String[] arrStr = getNoPerkAndNmPerkForCab2(terima
												.getKet2());
										String vNoPerk0 = arrStr[0];
										String vNmPerk0 = arrStr[1];
										if (terima.getKet2().equalsIgnoreCase("ADR")
												|| terima.getKet2().equalsIgnoreCase("ADS")
												|| terima.getKet2().equalsIgnoreCase("OTO")) {

											jurnal = new TrioJurnal();
											jurnal.setTglJurnal(terima.getTglTrm());
											jurnal.getTrioJurnalPK().setIdPerkiraan(
													vNoPerk0);
											jurnal.getTrioJurnalPK().setIdSub("");
											jurnal.setNamaPerkiraan(vNmPerk0);
											jurnal.setKet(terima.getNmTran());
											BigDecimal kreditTemp = NOL;
											if (terima.getKet2().equalsIgnoreCase("ADR")
													|| terima.getKet2().equalsIgnoreCase(
															"ADS")) {
												// vtamb_fin*11/10
												kreditTemp = terima
														.getTambFIN()
														.multiply(new BigDecimal(11))
														.divide(new BigDecimal(10), 0,
																RoundingMode.HALF_EVEN);
											} else {
												kreditTemp = terima.getTambFIN();
											}
											jurnal.setDebet(NOL);
											jurnal.setKredit(kreditTemp);
											jurnal.setBlc("*");
											jurnal.setTipe("K");
											jurnal.setNoReff(terima.getTrioTerimaPK()
													.getNoFak());
											listJurnal.add(jurnal);
											String vNoSub71204 = "";
											if (terima.getKet2().equalsIgnoreCase("ADR")) {
												vNoSub71204 = "01";
											} else if (terima.getKet2().equalsIgnoreCase(
													"ADS")) {
												vNoSub71204 = "02";
											} else if (terima.getKet2().equalsIgnoreCase(
													"OTO")) {
												vNoSub71204 = "03";
											} else if (terima.getKet2().equalsIgnoreCase(
													"WOM")) {
												vNoSub71204 = "04";
											}
											String vNmSub = getMasterFacade()
													.getTrioMstsubperkiraanDao()
													.findNamaSubByIdAndIdSub("71204",
															vNoSub71204); // 71104
											jurnal = new TrioJurnal();
											jurnal.setTglJurnal(terima.getTglTrm());
											jurnal.getTrioJurnalPK()
													.setIdPerkiraan("71204"); // 71104
											jurnal.getTrioJurnalPK().setIdSub(vNoSub71204);
											jurnal.setNamaPerkiraan(vNmSub);
											jurnal.setKet(terima.getNmTran());

											BigDecimal tempDebet = NOL;
											if (terima.getKet2().equalsIgnoreCase("ADR")
													|| terima.getKet2().equalsIgnoreCase(
															"ADS")) {
												tempDebet = terima.getTambFIN();
											} else {
												// ROUND(vtamb_fin*10/11,0)
												tempDebet = terima
														.getTambFIN()
														.multiply(new BigDecimal(10))
														.divide(new BigDecimal(11), 0,
																RoundingMode.HALF_EVEN);
											}
											jurnal.setDebet(tempDebet);
											jurnal.setKredit(NOL);
											jurnal.setBlc("*");
											jurnal.setTipe("D");
											listJurnal.add(jurnal);

											String vNmPerk = getMasterFacade()
													.getTrioMstperkiraanDao().findNamaById(
															"21404");
											jurnal = new TrioJurnal();
											jurnal.setTglJurnal(terima.getTglTrm());
											jurnal.getTrioJurnalPK()
													.setIdPerkiraan("21404");
											jurnal.getTrioJurnalPK().setIdSub("");
											jurnal.setNamaPerkiraan(vNmPerk);
											jurnal.setKet(terima.getNmTran());

											tempDebet = NOL;
											if (terima.getKet2().equalsIgnoreCase("ADR")
													|| terima.getKet2().equalsIgnoreCase(
															"ADS")) {
												// ROUND((vtamb_fin)*0.1,0)
												tempDebet = terima
														.getTambFIN()
														.multiply(new BigDecimal(0.1))
														.setScale(0, RoundingMode.HALF_EVEN);
											} else {
												// ROUND((vtamb_fin*10/11)*0.1,0)
												tempDebet = terima
														.getTambFIN()
														.multiply(new BigDecimal(10))
														.divide(new BigDecimal(11), 0,
																RoundingMode.HALF_EVEN)
														.multiply(new BigDecimal(0.1))
														.setScale(0, RoundingMode.HALF_EVEN);
											}
											jurnal.setDebet(tempDebet);
											jurnal.setKredit(NOL);
											jurnal.setBlc("*");
											jurnal.setTipe("D");
											listJurnal.add(jurnal);

										}// IF vket2<>'ADR' AND vket2<>'ADS' AND
											// vket2<>'OTO' AND vket2<>'WOM' AND
											// vhadiah_l='*'
										if ((!terima.getKet2().equalsIgnoreCase("ADR"))
												&& (!terima.getKet2().equalsIgnoreCase(
														"ADS"))
												&& (!terima.getKet2().equalsIgnoreCase(
														"OTO")
														&& (!terima.getKet2()
																.equalsIgnoreCase("WOM")) && terima
														.getHadiah().equalsIgnoreCase("*"))) {

											BigDecimal tambFin = terima.getTambFIN().add(
													terima.getsFinance());
											// IF vtamb_fin+vs_finance>0
											if (tambFin.compareTo(NOL) == 1) {
												String[] arrStr0 = getNoPerkAndNmPerkForCab2(terima
														.getKet2());
												vNoPerk = arrStr0[0];
												String vNmPerk = arrStr0[1];

												jurnal = new TrioJurnal();
												jurnal.setTglJurnal(terima.getTglTrm());
												jurnal.getTrioJurnalPK().setIdPerkiraan(
														vNoPerk);
												jurnal.getTrioJurnalPK().setIdSub("");
												jurnal.setNamaPerkiraan(vNmPerk);
												jurnal.setKet(terima.getNmTran());
												jurnal.setDebet(NOL);
												jurnal.setKredit(tambFin);
												jurnal.setBlc("*");
												jurnal.setTipe("K");
												jurnal.setNoReff(terima.getTrioTerimaPK()
														.getNoFak());
												listJurnal.add(jurnal);

												String vNoPerkk = "61206"; //61106
												String vNoSubk = "04";

												String vNmPerkk = getMasterFacade()
														.getTrioMstsubperkiraanDao()
														.findNamaSubByIdAndIdSub(vNoPerkk,
																vNoSubk);
												jurnal = new TrioJurnal();
												jurnal.setTglJurnal(terima.getTglTrm());
												jurnal.getTrioJurnalPK().setIdPerkiraan(
														vNoPerkk);
												jurnal.getTrioJurnalPK().setIdSub(vNoSubk);
												jurnal.setNamaPerkiraan(vNmPerk);
												jurnal.setKet(terima.getNmTran());
												jurnal.setDebet(tambFin);
												jurnal.setKredit(NOL);
												jurnal.setBlc("*");
												jurnal.setTipe("D");
												jurnal.setNoReff(terima.getTrioTerimaPK()
														.getNoFak());
												listJurnal.add(jurnal);

											}// IF vtamb_md+vtamb_sd+vtamb_fin>0
											BigDecimal tambTriple = terima.getTambMD()
													.add(terima.getTambSD())
													.add(terima.getTambFIN());
											if (tambTriple.compareTo(NOL) == 1) {
												String vNoPerkk = "21309";
												String vNmPerkk = getMasterFacade()
														.getTrioMstperkiraanDao()
														.findNamaById(vNoPerkk);

												jurnal = new TrioJurnal();
												jurnal.setTglJurnal(terima.getTglTrm());
												jurnal.getTrioJurnalPK().setIdPerkiraan(
														vNoPerkk);
												jurnal.getTrioJurnalPK().setIdSub("");
												jurnal.setNamaPerkiraan(vNmPerkk);
												jurnal.setKet(terima.getNmTran());
												jurnal.setDebet(tambTriple);
												jurnal.setKredit(NOL);
												jurnal.setBlc("*");
												jurnal.setTipe("D");
												jurnal.setNoReff(terima.getTrioTerimaPK()
														.getNoFak());
												listJurnal.add(jurnal);

												vNoPerkk = "21305";
												vNmPerkk = getMasterFacade()
														.getTrioMstperkiraanDao()
														.findNamaById(vNoPerkk);

												jurnal = new TrioJurnal();
												jurnal.setTglJurnal(terima.getTglTrm());
												jurnal.getTrioJurnalPK().setIdPerkiraan(
														vNoPerkk);
												jurnal.getTrioJurnalPK().setIdSub("");
												jurnal.setNamaPerkiraan(vNmPerkk);
												jurnal.setKet(terima.getNmTran());
												// ROUND(ROUND((vtamb_md+vtamb_sd+vtamb_fin)/0.97,0)*0.03,0)
												BigDecimal debet = tambTriple
														.divide(new BigDecimal(0.97), 0,
																RoundingMode.HALF_EVEN)
														.multiply(new BigDecimal(0.03))
														.setScale(0, RoundingMode.HALF_EVEN);
												jurnal.setDebet(debet);
												jurnal.setKredit(NOL);
												jurnal.setBlc("*");
												jurnal.setTipe("D");
												jurnal.setNoReff(terima.getTrioTerimaPK()
														.getNoFak());
												listJurnal.add(jurnal);

											}
										}//
										String[] arrayPerk = getNoPerkAndNmPerkForCab2(terima
												.getKet2());
										vNoPerk = arrayPerk[0];
										String vNmPerk = arrayPerk[1];

										List<TrioTerima> listTerima2 = findByNoLKHAndNoFakAndKet("KUPON", terima.getTrioTerimaPK().getNoFak(),"X");

										for (TrioTerima trm : listTerima2) {
											if (terima.getKet2().equalsIgnoreCase("ADR")
													|| terima.getKet2().equalsIgnoreCase(
															"ADS")
													|| terima.getKet2().equalsIgnoreCase(
															"OTO")) { // beda ||
																		// terima.getKet2().equalsIgnoreCase("WOM")
												if (terima.getKet2()
														.equalsIgnoreCase("OTO")) {
													jurnal = new TrioJurnal();
													jurnal.setTglJurnal(terima.getTglTrm());
													jurnal.getTrioJurnalPK()
															.setIdPerkiraan(vNoPerk0);
													jurnal.getTrioJurnalPK().setIdSub("");
													jurnal.setNamaPerkiraan(vNmPerk0);
													jurnal.setKet(trm.getNmTran() + " A.N "
															+ terima.getNmCust());
													// debet with vqcari1.debet*(10/11)
													BigDecimal dbt = trm
															.getDebet()
															.multiply(
																	new BigDecimal(10)
																			.divide(new BigDecimal(
																					11),
																					0,
																					RoundingMode.HALF_EVEN));
													jurnal.setDebet(NOL);
													jurnal.setKredit(dbt);
													jurnal.setBlc("*");
													jurnal.setTipe("K");
													jurnal.setNoReff(terima
															.getTrioTerimaPK().getNoFak());
													listJurnal.add(jurnal);

													jurnal = new TrioJurnal();
													jurnal.setTglJurnal(terima.getTglTrm());
													jurnal.getTrioJurnalPK()
															.setIdPerkiraan("71204"); //71104
													jurnal.getTrioJurnalPK().setIdSub("03");
													jurnal.setNamaPerkiraan("JASA PERANTARA OTO");
													jurnal.setKet(trm.getNmTran() + " A.N "
															+ terima.getNmCust());
													// debet with
													// vqcari1.debet-(vqcari1.debet*(10/11))
													dbt = trm.getDebet().subtract(dbt);
													jurnal.setDebet(NOL);
													jurnal.setKredit(dbt);
													jurnal.setBlc("*");
													jurnal.setTipe("K");
													jurnal.setNoReff(terima
															.getTrioTerimaPK().getNoFak());
													listJurnal.add(jurnal);
												} else {
													jurnal = new TrioJurnal();
													jurnal.setTglJurnal(terima.getTglTrm());
													jurnal.getTrioJurnalPK()
															.setIdPerkiraan(vNoPerk0);
													jurnal.getTrioJurnalPK().setIdSub("");
													jurnal.setNamaPerkiraan(vNmPerk);
													jurnal.setKet(trm.getNmTran() + " A.N "
															+ terima.getNmCust());
													jurnal.setDebet(NOL);
													jurnal.setKredit(trm.getDebet());
													jurnal.setBlc("*");
													jurnal.setTipe("K");
													jurnal.setNoReff(terima
															.getTrioTerimaPK().getNoFak());
													listJurnal.add(jurnal);
												}
											} else {
												jurnal = new TrioJurnal();
												jurnal.setTglJurnal(terima.getTglTrm());
												jurnal.getTrioJurnalPK().setIdPerkiraan(
														vNoPerk0);
												jurnal.getTrioJurnalPK().setIdSub("");
												jurnal.setNamaPerkiraan(vNmPerk);
												jurnal.setKet(trm.getNmTran() + " A.N "
														+ terima.getNmCust());
												jurnal.setDebet(NOL);
												jurnal.setKredit(trm.getDebet());
												jurnal.setBlc("*");
												jurnal.setTipe("K");
												jurnal.setNoReff(terima.getTrioTerimaPK()
														.getNoFak());
												listJurnal.add(jurnal);
											}
											// sampai sini brorrrrr
											// IF vqcari1.no_perk='21116' AND
											// vqcari1.no_sub='01'
											if (trm.getNoPerk().equalsIgnoreCase("21116")
													&& trm.getNoSub()
															.equalsIgnoreCase("01")) {
												if (terima.getKet2()
														.equalsIgnoreCase("ADR")
														|| terima.getKet2()
																.equalsIgnoreCase("ADS")
														|| terima.getKet2()
																.equalsIgnoreCase("OTO")) { // beda
																							// ||
																							// terima.getKet2().equalsIgnoreCase("WOM")
													String vNoPerkk = "71205"; //71105
													String vNoSubk = trm.getNoSub();
													String vNmPerkk = "";
													if (vNoSubk.length() > 0) {
														vNmPerkk = getMasterFacade()
																.getTrioMstsubperkiraanDao()
																.findNamaSubByIdAndIdSub(
																		vNoPerkk, vNoSubk);
													} else {
														vNmPerkk = getMasterFacade()
																.getTrioMstperkiraanDao()
																.findNamaById(vNoPerkk);
													}

													jurnal = new TrioJurnal();
													jurnal.setTglJurnal(terima.getTglTrm());
													jurnal.getTrioJurnalPK()
															.setIdPerkiraan(vNoPerkk);
													jurnal.getTrioJurnalPK().setIdSub(
															vNoSubk);
													jurnal.setNamaPerkiraan(vNmPerkk);
													jurnal.setKet(trm.getNmTran() + " A.N "
															+ terima.getNmCust());

													BigDecimal kredit = trm
															.getDebet()
															.multiply(
																	new BigDecimal(10)
																			.divide(new BigDecimal(
																					11),
																					0,
																					RoundingMode.HALF_EVEN));
													jurnal.setDebet(kredit);
													jurnal.setKredit(NOL);
													jurnal.setBlc("*");
													jurnal.setTipe("D");
													jurnal.setNoReff(terima
															.getTrioTerimaPK().getNoFak());
													listJurnal.add(jurnal);

													vNoPerkk = "21404";
													vNmPerkk = getMasterFacade()
															.getTrioMstperkiraanDao()
															.findNamaById(vNoPerkk);
													if (vNoPerk.length() < 1) {
														vNmPerkk = "";
													}

													jurnal = new TrioJurnal();
													jurnal.setTglJurnal(terima.getTglTrm());
													jurnal.getTrioJurnalPK()
															.setIdPerkiraan(vNoPerkk);
													jurnal.getTrioJurnalPK().setIdSub(
															vNoSubk);
													jurnal.setNamaPerkiraan(vNmPerkk);
													jurnal.setKet(trm.getNmTran() + " A.N "
															+ terima.getNmCust());

													// kredit with
													// ROUND((vqcari1.debet*(10/11))*0.1,0)
													kredit = trm
															.getDebet()
															.multiply(
																	new BigDecimal(10)
																			.divide(new BigDecimal(
																					11),
																					0,
																					RoundingMode.HALF_EVEN))
															.multiply(new BigDecimal(0.1))
															.setScale(0,
																	RoundingMode.HALF_EVEN);
													jurnal.setDebet(kredit);
													jurnal.setKredit(NOL);
													jurnal.setBlc("*");
													jurnal.setTipe("D");
													jurnal.setNoReff(terima
															.getTrioTerimaPK().getNoFak());
													listJurnal.add(jurnal);

												} else {
													String vNoPerkk = "61204"; //61104
													String vNoSubk = "06";
													String vNmPerkk = getMasterFacade()
															.getTrioMstsubperkiraanDao()
															.findNamaSubByIdAndIdSub(
																	vNoPerkk, vNoSubk);
													if (vNmPerkk.length() < 1) {
														vNmPerkk = "";
													}
													jurnal = new TrioJurnal();
													jurnal.setTglJurnal(terima.getTglTrm());
													jurnal.getTrioJurnalPK()
															.setIdPerkiraan(vNoPerkk);
													jurnal.getTrioJurnalPK().setIdSub(
															vNoSubk);
													jurnal.setNamaPerkiraan(vNmPerkk);
													jurnal.setKet(trm.getNmTran() + " A.N "
															+ terima.getNmCust());
													jurnal.setDebet(trm.getDebet());
													jurnal.setKredit(NOL);
													jurnal.setBlc("*");
													jurnal.setTipe("D");
													jurnal.setNoReff(terima
															.getTrioTerimaPK().getNoFak());
													listJurnal.add(jurnal);

												}
											} else { // kurang disini ternyata
												// IF (vket2='ADR' OR vket2='ADS' OR
												// vket2='OTO' OR vket2='WOM')
												if (terima.getKet2()
														.equalsIgnoreCase("ADR")
														|| terima.getKet2()
																.equalsIgnoreCase("ADS")
														|| terima.getKet2()
																.equalsIgnoreCase("OTO")) { // beda
																							// ||
																							// terima.getKet2().equalsIgnoreCase("WOM")
													String vNoPerkk = "71205"; //71105
													String vNoSubk = trm.getNoSub();
													String vNmPerkk = "";
													if (vNoSubk.length() > 0) {
														vNmPerkk = getMasterFacade()
																.getTrioMstsubperkiraanDao()
																.findNamaSubByIdAndIdSub(
																		vNoPerkk, vNoSubk);
													} else {
														vNmPerkk = getMasterFacade()
																.getTrioMstperkiraanDao()
																.findNamaById(vNoPerkk);
													}
													jurnal = new TrioJurnal();
													jurnal.setTglJurnal(terima.getTglTrm());
													jurnal.getTrioJurnalPK()
															.setIdPerkiraan(vNoPerkk);
													jurnal.getTrioJurnalPK().setIdSub(
															vNoSubk);
													jurnal.setNamaPerkiraan(vNmPerkk);
													jurnal.setKet(trm.getNmTran() + " A.N "
															+ terima.getNmCust());
													// debet with vqcari1.debet*(10/11)
													BigDecimal temp = trm
															.getDebet()
															.multiply(
																	new BigDecimal(10)
																			.divide(new BigDecimal(
																					11),
																					0,
																					RoundingMode.HALF_EVEN));
													jurnal.setDebet(temp);
													jurnal.setKredit(NOL);
													jurnal.setBlc("*");
													jurnal.setTipe("D");
													jurnal.setNoReff(terima
															.getTrioTerimaPK().getNoFak());
													listJurnal.add(jurnal);

													vNoPerkk = "21404";
													vNmPerkk = getMasterFacade()
															.getTrioMstperkiraanDao()
															.findNamaById(vNoPerkk);
													if (vNmPerkk.length() < 1) {
														vNmPerkk = "";
													}
													jurnal = new TrioJurnal();
													jurnal.setTglJurnal(terima.getTglTrm());
													jurnal.getTrioJurnalPK()
															.setIdPerkiraan(vNoPerkk);
													jurnal.getTrioJurnalPK().setIdSub("");
													jurnal.setNamaPerkiraan(vNmPerkk);
													jurnal.setKet(trm.getNmTran() + " A.N "
															+ terima.getNmCust());

													// kredit with
													// ROUND((vqcari1.debet*(10/11))*0.1,0)
													temp = trm
															.getDebet()
															.multiply(
																	new BigDecimal(10)
																			.divide(new BigDecimal(
																					11),
																					0,
																					RoundingMode.HALF_EVEN));
													temp = temp.multiply(
															new BigDecimal(0.1)).setScale(
															0, RoundingMode.HALF_EVEN);
													jurnal.setDebet(temp);
													jurnal.setKredit(NOL);
													jurnal.setBlc("*");
													jurnal.setTipe("D");
													jurnal.setNoReff(terima
															.getTrioTerimaPK().getNoFak());
													listJurnal.add(jurnal);
												} else {
													String vNoPerkk = "21117"; //trm.getNoPerk();
													String vNoSubk = trm.getNoSub();
													String vNmPerkk = "";
													if (vNoSubk.length() > 0) {
														vNmPerkk = getMasterFacade()
																.getTrioMstsubperkiraanDao()
																.findNamaSubByIdAndIdSub(
																		vNoPerkk, vNoSubk);
													} else {
														vNmPerkk = getMasterFacade()
																.getTrioMstperkiraanDao()
																.findNamaById(vNoPerkk);
													}
													jurnal = new TrioJurnal();
													jurnal.setTglJurnal(terima.getTglTrm());
													jurnal.getTrioJurnalPK()
															.setIdPerkiraan(vNoPerkk);
													jurnal.getTrioJurnalPK().setIdSub(
															vNoSubk);
													jurnal.setNamaPerkiraan(vNmPerkk);
													jurnal.setKet(trm.getNmTran() + " A.N "
															+ terima.getNmCust());
													jurnal.setDebet(trm.getDebet());
													jurnal.setKredit(NOL);
													jurnal.setBlc("*");
													jurnal.setTipe("D");
													jurnal.setNoReff(terima
															.getTrioTerimaPK().getNoFak());
													listJurnal.add(jurnal);

												}
											}

										}
									}
								}
							}

							for (TrioJurnal jur : listJurnal) {
								System.out.println("Jurnal idPerk = "
										+ jur.getTrioJurnalPK().getIdPerkiraan());
								System.out.println("Jurnal idSub = "
										+ jur.getTrioJurnalPK().getIdSub());
							}

							String jenisJurnal = getJenisJurnalByParams(terima.getKet(),
									terima.getDebet(), terima.getKredit(),
									terima.getNoPerk());
							// getMasterFacade().getTrioJurnalDao().saveTransaction(listJurnal,
							// jenisJurnal, user);
							// case vno_perk='P2'
						} else if (terima.getNoPerk().equalsIgnoreCase("P2")) { 
							System.out.println("CASEIFCAB2 21");

							if (terima.getDebet().compareTo(NOL) == 1) {
								if (terima.getKet().equalsIgnoreCase("1")
										|| terima.getKet().equalsIgnoreCase("K")) {
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan("11102"); //11101
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan("KAS");
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(terima.getDebet());
									jurnal.setKredit(NOL);
									jurnal.setBlc("*");
									jurnal.setTipe("D");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);
								}
								if (terima.getKet().equalsIgnoreCase("2")) {
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan("11203");
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan("CEK");
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(terima.getDebet());
									jurnal.setKredit(NOL);
									jurnal.setBlc("*");
									jurnal.setTipe("D");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);
								}
								if (terima.getKet().equalsIgnoreCase("3")) {
									String vNoPerkA = "11202";
									String vNoSubA = terima.getKdBank();
									String vNmPerkA = "";
									if (vNoSubA.length() > 0) {
										vNmPerkA = getMasterFacade()
												.getTrioMstsubperkiraanDao()
												.findNamaSubByIdAndIdSub(vNoPerkA, vNoSubA);
									}
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerkA);
									jurnal.getTrioJurnalPK().setIdSub(vNoSubA);
									jurnal.setNamaPerkiraan(vNmPerkA);
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(terima.getDebet());
									jurnal.setKredit(NOL);
									jurnal.setBlc("*");
									jurnal.setTipe("D");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);
								}
								if (terima.getKet().equalsIgnoreCase("4")) {
									String vNoPerkA = "11121"; //11120
									String vNoSubA = terima.getKdPOS();
									String vNmPerkA = "";
									if (vNoSubA.length() > 0) {
										vNmPerkA = getMasterFacade()
												.getTrioMstsubperkiraanDao()
												.findNamaSubByIdAndIdSub(vNoPerkA, vNoSubA);
									}
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerkA);
									jurnal.getTrioJurnalPK().setIdSub(vNoSubA);
									jurnal.setNamaPerkiraan(vNmPerkA);
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(terima.getDebet());
									jurnal.setKredit(NOL);
									jurnal.setBlc("*");
									jurnal.setTipe("D");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);
								}

								String vFNoSub = "";
								boolean vSubBaru = false;
								TrioMstsubperkiraan subObject = getMasterFacade()
										.getTrioMstsubperkiraanDao().getSubByIdPerkAndKet2(
												"11315", terima.getNoMesin()); //11301
								if (subObject != null) {
									if (subObject.getTrioMstsubperkiraanPK()
											.getIdPerkiraan().length() > 0) {
										vFNoSub = subObject.getTrioMstsubperkiraanPK()
												.getIdSub();
										vSubBaru = false;
									}
								} else {
									List<TrioMstsubperkiraan> listSub = getMasterFacade()
											.getTrioMstsubperkiraanDao().findByIdPerkiraan("11315"); //11301
									int jmlListSub = listSub.size();
									//TrioMstsubperkiraan subObj = listSub.get(jmlListSub - 1); // ambil object terakhir dari list
									TrioMstsubperkiraan subObj = listSub.get(0); // ambil object terakhir dari list
									
									String vNoSubAkhir = subObj.getTrioMstsubperkiraanPK().getIdSub();
									int no = getMasterFacade().getTrioMstrunnumDao().getRunningNumber("IDSUB", "", user);
									String noSubBaru = TrioStringUtil.getFormattedRunno(Integer.valueOf(no));
									vFNoSub = noSubBaru;
									vSubBaru = true;
								}

								BigDecimal debetKurangBunga = terima.getDebet().subtract(
										terima.getsBunga());
								if (terima.getHarga().compareTo(debetKurangBunga) == 1) { // vharga>vdebet-vs_bunga

									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan("11315"); //11301
									jurnal.getTrioJurnalPK().setIdSub(vFNoSub);
									jurnal.setNamaPerkiraan(terima.getNmCust());
									jurnal.setKet(terima.getNmTran());
									BigDecimal jkWaktuKaliAngsuran = new BigDecimal(
											terima.getJkWaktu()).multiply(terima
											.getAngsuran());
									jurnal.setDebet(jkWaktuKaliAngsuran);
									jurnal.setKredit(NOL);
									jurnal.setBlc("*");
									jurnal.setTipe("D");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);
									if (vSubBaru) {
										TrioMstsubperkiraan subPerk = new TrioMstsubperkiraan(
												vFNoSub, "11315"); //11301
										subPerk.setNamaSub(terima.getNmCust());
										subPerk.setKet2(terima.getNoMesin());
										subPerk.setUangMuka(terima.getDebet());
										subPerk.setTglAwal(terima.getTglTrm());
										getMasterFacade().getTrioMstsubperkiraanDao().save(
												subPerk, user);
									}

								}
								// vno_perk1='41101'
								// vno_perk2='21404'
								// vno_perk3='21204'
								// vno_perk4='21205'
								// vno_perk5='21228'
								// vno_perk6='71101'
								// vno_perk7='41201'

								// vdpp=round(((vharga-vb_stnk-(vs_ahm+vs_md+vs_sd))*10/11),0)
								// vppnx= (vharga-vb_stnk-(vs_ahm+vs_md+vs_sd))-vdpp
								// vpot_penjualan=vs_ahm+vs_md+vs_sd

								BigDecimal vPotPenjualan = NOL;
								BigDecimal totalSubsidi = NOL;
								totalSubsidi = terima.getsAHM().add(terima.getsMD())
										.add(terima.getsSD())
										.setScale(0, RoundingMode.HALF_EVEN);
								BigDecimal temp = terima.getHarga()
										.subtract(terima.getbSTNK()).subtract(totalSubsidi);
								BigDecimal vdpp = temp.multiply(new BigDecimal(10)).divide(
										new BigDecimal(11), 0, RoundingMode.HALF_EVEN);
								BigDecimal vppnx = temp.subtract(vdpp);
								vPotPenjualan = totalSubsidi;

								String vNoPerk1 = "41106"; //41101
								String vNmPerk1 = getMasterFacade()
										.getTrioMstperkiraanDao().findNamaById(vNoPerk1);
								if (vNmPerk1 == null || vNmPerk1.length() < 1) {
									vNmPerk1 = "";
								}

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk1);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk1);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(vdpp);
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);

								String vNoPerk2 = "21404";
								String vNmPerk2 = getMasterFacade()
										.getTrioMstperkiraanDao().findNamaById(vNoPerk2);
								if (vNmPerk2 == null || vNmPerk2.length() < 1) {
									vNmPerk2 = "";
								}

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk2);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk2);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(vppnx);
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);

								String vNoPerk7 = "41204"; //41201
								String vNmPerk7 = getMasterFacade()
										.getTrioMstperkiraanDao().findNamaById(vNoPerk7);
								if (vNmPerk7 == null || vNmPerk7.length() < 1) {
									vNmPerk7 = "";
								}

								if (vPotPenjualan.compareTo(NOL) == 1) {

									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk7);
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan(vNmPerk7);
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(NOL);
									jurnal.setKredit(vPotPenjualan);
									jurnal.setBlc("*");
									jurnal.setTipe("K");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);

								}

								String vNoPerk3 = "21213"; //21204
								String vNmPerk3 = getMasterFacade()
										.getTrioMstperkiraanDao().findNamaById(vNoPerk3);
								if (vNmPerk3 == null || vNmPerk3.length() < 1) {
									vNmPerk3 = "";
								}

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk3);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk3);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getpSTNK());
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);

								String vNoPerk4 = "21214"; //21205
								String vNmPerk4 = getMasterFacade()
										.getTrioMstperkiraanDao().findNamaById(vNoPerk4);
								if (vNmPerk4 == null || vNmPerk4.length() < 1) {
									vNmPerk4 = "";
								}

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk4);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk4);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getpBPKB());
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);

								String vNoPerk5 = "21229"; //21228
								String vNmPerk5 = getMasterFacade()
										.getTrioMstperkiraanDao().findNamaById(vNoPerk5);
								if (vNmPerk5 == null || vNmPerk5.length() < 1) {
									vNmPerk5 = "";
								}
								// //vb_stnk-vp_stnk-vp_bpkb
								BigDecimal hitungBiaya = terima.getbSTNK()
										.subtract(terima.getpSTNK())
										.subtract(terima.getpBPKB());
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk5);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk5);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(hitungBiaya);
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);

								// (vjk_waktu*vangsuran)-(vharga-vdebet)>0
								BigDecimal waktuKaliAngs = terima.getAngsuran()
										.multiply(new BigDecimal(terima.getJkWaktu()))
										.setScale(0, RoundingMode.HALF_EVEN);
								BigDecimal hargaKurangDebet = terima.getHarga().subtract(
										terima.getDebet());
								BigDecimal hasil = waktuKaliAngs.subtract(hargaKurangDebet);
								if (hasil.compareTo(NOL) == 1) {
									String vNoPerk6 = "71201"; //71101
									String vNmPerk6 = getMasterFacade()
											.getTrioMstperkiraanDao()
											.findNamaById(vNoPerk6);
									if (vNmPerk6 == null || vNmPerk6.length() < 1) {
										vNmPerk6 = "";
									}

									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk6);
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan(vNmPerk6);
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(NOL);
									jurnal.setKredit(hasil);
									jurnal.setBlc("*");
									jurnal.setTipe("K");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);

								} else { // else hasil.compareTo(NOL)== 1
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk7);
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan(vNmPerk7);
									jurnal.setKet(terima.getNmTran());
									// debet with ((vjk_waktu*vangsuran)-(vharga-vdebet))*-1
									hasil = hasil.multiply(new BigDecimal(-1));
									jurnal.setDebet(hasil);
									jurnal.setKredit(NOL);
									jurnal.setBlc("*");
									jurnal.setTipe("D");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);
								}

							} else { // else debet > 0

								if (terima.getKet().equalsIgnoreCase("1")
										|| terima.getKet().equalsIgnoreCase("K")) {
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan("11102"); //11101
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan("KAS");
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(NOL);
									jurnal.setKredit(terima.getKredit());
									jurnal.setBlc("*");
									jurnal.setTipe("K");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);
								}
								if (terima.getKet().equalsIgnoreCase("2")) {
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan("11203");
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan("CEK");
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(NOL);
									jurnal.setKredit(terima.getKredit());
									jurnal.setBlc("*");
									jurnal.setTipe("K");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);
								}
								if (terima.getKet().equalsIgnoreCase("3")) {
									String vNoPerkA = "11202";
									String vNoSubA = terima.getKdBank();
									String vNmPerkA = "";
									if (vNoSubA.length() > 0) {
										vNmPerkA = getMasterFacade()
												.getTrioMstsubperkiraanDao()
												.findNamaSubByIdAndIdSub(vNoPerkA, vNoSubA);
									}
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerkA);
									jurnal.getTrioJurnalPK().setIdSub(vNoSubA);
									jurnal.setNamaPerkiraan(vNmPerkA);
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(NOL);
									jurnal.setKredit(terima.getKredit());
									jurnal.setBlc("*");
									jurnal.setTipe("K");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);
								}
								if (terima.getKet().equalsIgnoreCase("4")) {
									String vNoPerkA = "11121"; //11120
									String vNoSubA = terima.getKdPOS();
									String vNmPerkA = "";
									if (vNoSubA.length() > 0) {
										vNmPerkA = getMasterFacade()
												.getTrioMstsubperkiraanDao()
												.findNamaSubByIdAndIdSub(vNoPerkA, vNoSubA);
									}
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerkA);
									jurnal.getTrioJurnalPK().setIdSub(vNoSubA);
									jurnal.setNamaPerkiraan(vNmPerkA);
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(terima.getDebet());
									jurnal.setKredit(NOL);
									jurnal.setBlc("*");
									jurnal.setTipe("D");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);
								}

								if (terima.getHarga().compareTo(terima.getKredit()) == 1) { // vharga>vkredit

									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan("11315"); //11301
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan(terima.getNmCust());
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(NOL);
									// kredit with vjk_waktu*vangsuran &&vharga-vkredit
									BigDecimal hasil = new BigDecimal(terima.getJkWaktu())
											.multiply(terima.getAngsuran()).setScale(0,
													RoundingMode.HALF_EVEN);
									jurnal.setKredit(hasil);
									jurnal.setBlc("*");
									jurnal.setTipe("K");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);

									TrioMstsubperkiraan subPerk = getMasterFacade()
											.getTrioMstsubperkiraanDao()
											.getSubByIdPerkAndKet2("11315",
													terima.getNoMesin()); //11301
									if (subPerk != null) {
										getMasterFacade().getTrioMstsubperkiraanDao()
												.delete(subPerk);
									}
								}
								// MULAI COPY
								BigDecimal vPotPenjualan = NOL;
								BigDecimal totalSubsidi = NOL;
								totalSubsidi = terima.getsAHM().add(terima.getsMD())
										.add(terima.getsSD())
										.setScale(0, RoundingMode.HALF_EVEN);
								BigDecimal temp = terima.getHarga()
										.subtract(terima.getbSTNK()).subtract(totalSubsidi);
								BigDecimal vdpp = temp.multiply(new BigDecimal(10)).divide(
										new BigDecimal(11), 0, RoundingMode.HALF_EVEN);
								BigDecimal vppnx = temp.subtract(vdpp);
								vPotPenjualan = totalSubsidi;

								String vNoPerk1 = "41106"; //41101
								String vNmPerk1 = getMasterFacade()
										.getTrioMstperkiraanDao().findNamaById(vNoPerk1);
								if (vNmPerk1 == null || vNmPerk1.length() < 1) {
									vNmPerk1 = "";
								}

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk1);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk1);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(vdpp);
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);

								String vNoPerk2 = "21404";
								String vNmPerk2 = getMasterFacade()
										.getTrioMstperkiraanDao().findNamaById(vNoPerk2);
								if (vNmPerk2 == null || vNmPerk2.length() < 1) {
									vNmPerk2 = "";
								}

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk2);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk2);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(vppnx);
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);

								String vNoPerk7 = "41204"; //41201
								String vNmPerk7 = getMasterFacade()
										.getTrioMstperkiraanDao().findNamaById(vNoPerk7);
								if (vNmPerk7 == null || vNmPerk7.length() < 1) {
									vNmPerk7 = "";
								}

								if (vPotPenjualan.compareTo(NOL) == 1) {

									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk7);
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan(vNmPerk7);
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(NOL);
									jurnal.setKredit(vPotPenjualan);
									jurnal.setBlc("*");
									jurnal.setTipe("K");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);

								}

								String vNoPerk3 = "21213"; //21204
								String vNmPerk3 = getMasterFacade()
										.getTrioMstperkiraanDao().findNamaById(vNoPerk3);
								if (vNmPerk3 == null || vNmPerk3.length() < 1) {
									vNmPerk3 = "";
								}

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk3);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk3);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getpSTNK());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);

								String vNoPerk4 = "21214"; //21205
								String vNmPerk4 = getMasterFacade()
										.getTrioMstperkiraanDao().findNamaById(vNoPerk4);
								if (vNmPerk4 == null || vNmPerk4.length() < 1) {
									vNmPerk4 = "";
								}

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk4);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk4);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getpBPKB());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);

								String vNoPerk5 = "21229"; //21228
								String vNmPerk5 = getMasterFacade()
										.getTrioMstperkiraanDao().findNamaById(vNoPerk5);
								if (vNmPerk5 == null || vNmPerk5.length() < 1) {
									vNmPerk5 = "";
								}
								// //vb_stnk-vp_stnk-vp_bpkb
								BigDecimal hitungBiaya = terima.getbSTNK()
										.subtract(terima.getpSTNK())
										.subtract(terima.getpBPKB());
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk5);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk5);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(hitungBiaya);
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);

								// (vjk_waktu*vangsuran)-(vharga-vkredit)>0
								BigDecimal waktuKaliAngs = terima.getAngsuran()
										.multiply(new BigDecimal(terima.getJkWaktu()))
										.setScale(0, RoundingMode.HALF_EVEN);
								BigDecimal hargaKurangKredit = terima.getHarga().subtract(
										terima.getKredit());
								BigDecimal hasil = waktuKaliAngs
										.subtract(hargaKurangKredit);
								if (hasil.compareTo(NOL) == 1) {
									String vNoPerk6 = "71201"; //71101
									String vNmPerk6 = getMasterFacade()
											.getTrioMstperkiraanDao()
											.findNamaById(vNoPerk6);
									if (vNmPerk6 == null || vNmPerk6.length() < 1) {
										vNmPerk6 = "";
									}

									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk6);
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan(vNmPerk6);
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(hasil);
									jurnal.setKredit(NOL);
									jurnal.setBlc("*");
									jurnal.setTipe("D");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);

								} else { // else hasil.compareTo(NOL)== 1
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk7);
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan(vNmPerk7);
									jurnal.setKet(terima.getNmTran());
									// debet with ((vjk_waktu*vangsuran)-(vharga-vdebet))*-1
									// kredit with
									// ((vjk_waktu*vangsuran)-(vharga-vkredit))*-1
									hasil = hasil.multiply(new BigDecimal(-1));
									jurnal.setDebet(NOL);
									jurnal.setKredit(hasil);
									jurnal.setBlc("*");
									jurnal.setTipe("K");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);
								}
								// AKHIR COPY
							}
							String jenisJurnal = getJenisJurnalByParams(terima.getKet(),
									terima.getDebet(), terima.getKredit(),
									terima.getNoPerk());
							getMasterFacade().getTrioJurnalDao().saveTransaction(
									listJurnal, jenisJurnal, user);
							// end case vno_perk='P2'
							// case vno_perk='41201' and (vket='1' OR vket='K') AND ALLTRIM(vket2)='OFF TR'
						} else if (terima.getNoPerk().equalsIgnoreCase("41204")
								&& (terima.getKet().equalsIgnoreCase("1") || terima
										.getKet().equalsIgnoreCase("K"))
								&& terima.getKet2().equalsIgnoreCase("OFF TR")) { 
								//41201
							System.out.println("CASEIFCAB2 22");

							TrioTerima tt = getTerimaByNoFak(terima.getTrioTerimaPK()
									.getNoFak());
							BigDecimal vbSTNK = tt.getbSTNK();
							BigDecimal vpSTNK = tt.getpSTNK();
							BigDecimal vpBPKB = tt.getpBPKB();

							if (terima.getKredit().compareTo(NOL) == 1) {
								// vno_perk3='21204'
								// vno_perk4='21205'
								// vno_perk5='21228'
								// vno_perk6='42199'
								String vNoPerk3 = "21213"; //21204
								String vNmPerk3 = getMasterFacade()
										.getTrioMstperkiraanDao().findNamaById(vNoPerk3);

								String vNoPerk4 = "21214"; //21205
								String vNmPerk4 = getMasterFacade()
										.getTrioMstperkiraanDao().findNamaById(vNoPerk4);

								String vNoPerk5 = "21229"; //21228
								String vNmPerk5 = getMasterFacade()
										.getTrioMstperkiraanDao().findNamaById(vNoPerk5);

								String vNoPerk6 = "42198"; //42199
								String vNmPerk6 = getMasterFacade()
										.getTrioMstperkiraanDao().findNamaById(vNoPerk6);

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk3);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk3);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(vpSTNK);
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								jurnal.setKet2(terima.getTrioTerimaPK().getNoFak());
								listJurnal.add(jurnal);

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk4);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk4);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(vpBPKB);
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								jurnal.setKet2(terima.getTrioTerimaPK().getNoFak());
								listJurnal.add(jurnal);

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk5);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk5);
								jurnal.setKet(terima.getNmTran());
								// debet with vb_stnk-vp_stnk-vp_bpkb
								BigDecimal hitung = vbSTNK.subtract(vpSTNK)
										.subtract(vpBPKB);
								jurnal.setDebet(hitung);
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								jurnal.setKet2(terima.getTrioTerimaPK().getNoFak());
								listJurnal.add(jurnal);

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan("11102"); //11101
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan("KAS");
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getKredit());
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								jurnal.setKet2(terima.getTrioTerimaPK().getNoFak());
								listJurnal.add(jurnal);

								// vb_stnk-vkredit>0
								BigDecimal bStnkKurangKredit = vbSTNK.subtract(terima
										.getKredit());
								if (bStnkKurangKredit.compareTo(NOL) == 1) {
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk6);
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan(vNmPerk6);
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(NOL);
									jurnal.setKredit(bStnkKurangKredit);
									jurnal.setBlc("*");
									jurnal.setTipe("K");
									jurnal.setKet2(terima.getTrioTerimaPK().getNoFak());
									listJurnal.add(jurnal);
								}

							} else { // kredit > 0
								String vNoPerk3 = "21213"; //21204
								String vNmPerk3 = getMasterFacade()
										.getTrioMstperkiraanDao().findNamaById(vNoPerk3);

								String vNoPerk4 = "21214"; //21205
								String vNmPerk4 = getMasterFacade()
										.getTrioMstperkiraanDao().findNamaById(vNoPerk4);

								String vNoPerk5 = "21229"; //21228
								String vNmPerk5 = getMasterFacade()
										.getTrioMstperkiraanDao().findNamaById(vNoPerk5);

								String vNoPerk6 = "42198"; //42199
								String vNmPerk6 = getMasterFacade()
										.getTrioMstperkiraanDao().findNamaById(vNoPerk6);

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk3);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk3);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(vpSTNK);
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								jurnal.setKet2(terima.getTrioTerimaPK().getNoFak());
								listJurnal.add(jurnal);

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk4);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk4);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(vpBPKB);
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								jurnal.setKet2(terima.getTrioTerimaPK().getNoFak());
								listJurnal.add(jurnal);

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk5);
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan(vNmPerk5);
								jurnal.setKet(terima.getNmTran());
								// debet with vb_stnk-vp_stnk-vp_bpkb
								// kredit with vb_stnk-vp_stnk-vp_bpkb
								BigDecimal hitung = vbSTNK.subtract(vpSTNK)
										.subtract(vpBPKB);
								jurnal.setDebet(NOL);
								jurnal.setKredit(hitung);
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								jurnal.setKet2(terima.getTrioTerimaPK().getNoFak());
								listJurnal.add(jurnal);

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan("11102"); //11101
								jurnal.getTrioJurnalPK().setIdSub("");
								jurnal.setNamaPerkiraan("KAS");
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getKredit());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								jurnal.setKet2(terima.getTrioTerimaPK().getNoFak());
								listJurnal.add(jurnal);

								// vb_stnk-vkredit>0
								BigDecimal bStnkKurangKredit = vbSTNK.subtract(terima
										.getKredit());
								if (bStnkKurangKredit.compareTo(NOL) == 1) {
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk6);
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan(vNmPerk6);
									jurnal.setKet(terima.getNmTran());
									// debet with vb_stnk-vkredit
									jurnal.setDebet(bStnkKurangKredit);
									jurnal.setKredit(NOL);
									jurnal.setBlc("*");
									jurnal.setTipe("D");
									jurnal.setKet2(terima.getTrioTerimaPK().getNoFak());
									listJurnal.add(jurnal);
								}
							}

							String jenisJurnal = getJenisJurnalByParams(terima.getKet(),
									terima.getDebet(), terima.getKredit(),
									terima.getNoPerk());
							getMasterFacade().getTrioJurnalDao().saveTransaction(
									listJurnal, jenisJurnal, user);
							// end case 41201
							// case vno_perk='11301' and (vket='1' or vket='K') AND ALLTRIM(master.kd_prs)<>'T10'
						} else if (terima.getNoPerk().equalsIgnoreCase("11301")
								&& (terima.getNoPerk().equalsIgnoreCase("1") || terima
										.getNoPerk().equalsIgnoreCase("K"))) {

							System.out.println("CASEIFCAB2 23");

							if (terima.getDebet().compareTo(NOL) == 1) {

								TrioMstsubperkiraan subPerk = getMasterFacade()
										.getTrioMstsubperkiraanDao().getSubByIdPerkAndKet2(
												terima.getNoPerk(), terima.getKet2());
								if (subPerk != null) {
									vNoSub = subPerk.getTrioMstsubperkiraanPK()
											.getIdSub();
									String vNmSub = subPerk.getNamaSub();
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan("11102"); //11101
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan("KAS");
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(terima.getDebet());
									jurnal.setKredit(NOL);
									jurnal.setBlc("*");
									jurnal.setTipe("D");
									listJurnal.add(jurnal);

									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(
											terima.getNoPerk());
									jurnal.getTrioJurnalPK().setIdSub(vNoSub);
									jurnal.setNamaPerkiraan(vNmSub);
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(NOL);
									jurnal.setKredit(terima.getDebet());
									jurnal.setBlc("*");
									jurnal.setTipe("K");
									jurnal.setKet2(terima.getKet2());
									listJurnal.add(jurnal);

								} else {
									System.out.println("No Mesin [ " + terima.getKet2()
											+ " ] tidak ditemukan");
								}

							} else { // else untuk debet > 0
								TrioMstsubperkiraan subPerk = getMasterFacade()
										.getTrioMstsubperkiraanDao().getSubByIdPerkAndKet2(
												terima.getNoPerk(), terima.getKet2());
								if (subPerk != null) {
									vNoSub = subPerk.getTrioMstsubperkiraanPK()
											.getIdSub();
									String vNmSub = subPerk.getNamaSub();
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan("11102"); //11101
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan("KAS");
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(NOL);
									jurnal.setKredit(terima.getKredit());
									jurnal.setBlc("*");
									jurnal.setTipe("K");
									listJurnal.add(jurnal);

									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(
											terima.getNoPerk());
									jurnal.getTrioJurnalPK().setIdSub(vNoSub);
									jurnal.setNamaPerkiraan(vNmSub);
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(terima.getKredit());
									jurnal.setKredit(NOL);
									jurnal.setBlc("*");
									jurnal.setTipe("D");
									jurnal.setKet2(terima.getKet2());
									listJurnal.add(jurnal);

								} else {
									System.out.println("No Mesin [ " + terima.getKet2()
											+ " ] tidak ditemukan");
								}
							}

							String jenisJurnal = getJenisJurnalByParams(terima.getKet(),
									terima.getDebet(), terima.getKredit(),
									terima.getNoPerk());
							getMasterFacade().getTrioJurnalDao().saveTransaction(
									listJurnal, jenisJurnal, user);
							// end case 11301
							// case vitr='*' AND vket<>'4'
						} else if (terima.getItr().equalsIgnoreCase("*")
								&& (!terima.getKet().equalsIgnoreCase("4"))) { 

							System.out.println("CASEIFCAB2 24");

							if (terima.getDebet().compareTo(NOL) == 1) {
								if (terima.getKet().equalsIgnoreCase("1")
										|| terima.getKet().equalsIgnoreCase("K")) {
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan("11102"); //11101
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan("KAS");
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(terima.getDebet());
									jurnal.setKredit(NOL);
									jurnal.setBlc("*");
									jurnal.setTipe("D");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);
								}
								if (terima.getKet().equalsIgnoreCase("2")) {
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan("11203");
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan("CEK");
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(terima.getDebet());
									jurnal.setKredit(NOL);
									jurnal.setBlc("*");
									jurnal.setTipe("D");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);
								}
								if (terima.getKet().equalsIgnoreCase("3")) {
									vNoPerk = "11202";
									vNoSub = terima.getKdBank();
									String vNmPerk = getMasterFacade()
											.getTrioMstsubperkiraanDao()
											.findNamaSubByIdAndIdSub(vNoPerk, vNoSub);
									if (vNmPerk == null || vNmPerk.length() < 1) {
										vNmPerk = "";
									}
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
									jurnal.getTrioJurnalPK().setIdSub(vNoSub);
									jurnal.setNamaPerkiraan(vNmPerk);
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(terima.getDebet());
									jurnal.setKredit(NOL);
									jurnal.setBlc("*");
									jurnal.setTipe("D");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);

								}

								String vNoPerkA = "11121"; //11120
								String vNoSub1 = terima.getKdPOS();
								String vNmPerkA = "";
								if (vNoSub1.length() > 0) {
									vNmPerkA = getMasterFacade()
											.getTrioMstsubperkiraanDao()
											.findNamaSubByIdAndIdSub(vNoPerkA, vNoSub1);
								}

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerkA);
								if (vNoSub1.length() > 0) {
									jurnal.getTrioJurnalPK().setIdSub(vNoSub1);
								} else {
									jurnal.getTrioJurnalPK().setIdSub("");
								}
								jurnal.setNamaPerkiraan(vNmPerkA);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getDebet());
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);

							} else { // else debet > 0

								if (terima.getKet().equalsIgnoreCase("1")
										|| terima.getKet().equalsIgnoreCase("K")) {
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan("11102"); //11101
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan("KAS");
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(NOL);
									jurnal.setKredit(terima.getKredit());
									jurnal.setBlc("*");
									jurnal.setTipe("K");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);
								}
								if (terima.getKet().equalsIgnoreCase("2")) {
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan("11203");
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan("CEK");
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(NOL);
									jurnal.setKredit(terima.getKredit());
									jurnal.setBlc("*");
									jurnal.setTipe("K");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);
								}
								if (terima.getKet().equalsIgnoreCase("3")) {
									vNoPerk = "11202";
									vNoSub = terima.getKdBank();
									String vNmPerk = getMasterFacade()
											.getTrioMstsubperkiraanDao()
											.findNamaSubByIdAndIdSub(vNoPerk, vNoSub);
									if (vNmPerk == null || vNmPerk.length() < 1) {
										vNmPerk = "";
									}
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerk);
									jurnal.getTrioJurnalPK().setIdSub(vNoSub);
									jurnal.setNamaPerkiraan(vNmPerk);
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(NOL);
									jurnal.setKredit(terima.getKredit());
									jurnal.setBlc("*");
									jurnal.setTipe("K");
									jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
									jurnal.setNoMesin(terima.getNoMesin());
									listJurnal.add(jurnal);

								}

								String vNoPerkA = "11121"; //11120
								String vNoSub1 = terima.getKdPOS();
								String vNmPerkA = "";
								if (vNoSub1.length() > 0) {
									vNmPerkA = getMasterFacade()
											.getTrioMstsubperkiraanDao()
											.findNamaSubByIdAndIdSub(vNoPerkA, vNoSub1);
								}

								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerkA);
								if (vNoSub1.length() > 0) {
									jurnal.getTrioJurnalPK().setIdSub(vNoSub1);
								} else {
									jurnal.getTrioJurnalPK().setIdSub("");
								}
								jurnal.setNamaPerkiraan(vNmPerkA);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getKredit());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								jurnal.setNoReff(terima.getTrioTerimaPK().getNoFak());
								jurnal.setNoMesin(terima.getNoMesin());
								listJurnal.add(jurnal);

							}
							String jenisJurnal = getJenisJurnalByParams(terima.getKet(),
									terima.getDebet(), terima.getKredit(),
									terima.getNoPerk());
							getMasterFacade().getTrioJurnalDao().saveTransaction(
									listJurnal, jenisJurnal, user);
							// end case
						} else { // OTHERWISE

							System.out.println("CASEIFCAB2 25");
							String vNoPerkA = "";
							String vNoSub1 = "";
							String vNmPerkA = "";
							if (terima.getDebet().compareTo(NOL) == 1) {

								if (terima.getKet().equalsIgnoreCase("4")) {

									vNoPerkA = "11120";

									vNoSub1 = terima.getKdPOS();
									if (vNoSub1.length() > 0) {
										vNmPerkA = getMasterFacade()
												.getTrioMstsubperkiraanDao()
												.findNamaSubByIdAndIdSub(vNoPerkA, vNoSub1);
									}

								} else { // else ket = 4
									vNoPerkA = "11102"; //11101
									vNmPerkA = "KAS";
								}
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerkA);
								if (vNoSub1.length() > 0) {
									jurnal.getTrioJurnalPK().setIdSub(vNoSub1);
								} else {
									jurnal.getTrioJurnalPK().setIdSub("");
								}
								jurnal.setNamaPerkiraan(vNmPerkA);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(terima.getDebet());
								jurnal.setKredit(NOL);
								jurnal.setBlc("*");
								jurnal.setTipe("D");
								listJurnal.add(jurnal);

								vNoSub1 = "";

								vNmPerkA = getMasterFacade().getTrioMstperkiraanDao()
										.findNamaById(terima.getNoPerk());

								if (terima.getNoSub().length() > 0) {
									vNmPerkA = getMasterFacade()
											.getTrioMstsubperkiraanDao()
											.findNamaSubByIdAndIdSub(terima.getNoPerk(),
													terima.getNoSub());
								}

								// if vno_perk='41102' or vno_perk='41103' or
								// vno_perk='41104' or vno_perk='41105' or vno_perk='41301'
								// OR vno_perk='41116'
								if (terima.getNoPerk().equalsIgnoreCase("41107")
										|| terima.getNoPerk().equalsIgnoreCase("41108")
										|| terima.getNoPerk().equalsIgnoreCase("41109")
										|| terima.getNoPerk().equalsIgnoreCase("41110")
										|| terima.getNoPerk().equalsIgnoreCase("41303")
										|| terima.getNoPerk().equalsIgnoreCase("41117")) {
									
									// 41102 -> 41107
									// 41103 -> 41108
									// 41104 -> 41109
									// 41105 -> 41110
									// 41301 -> 41303
									// 41116 -> 41117

									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(
											terima.getNoPerk());
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan(vNmPerkA);
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(NOL);
									jurnal.setKredit(vjumlah2);
									jurnal.setBlc("*");
									jurnal.setTipe("K");
									listJurnal.add(jurnal);

									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan("21404");
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan("PPN KELUARAN");
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(NOL);
									jurnal.setKredit(vjumlah3);
									jurnal.setBlc("*");
									jurnal.setTipe("K");
									listJurnal.add(jurnal);

									// else if vno_perk='41102' or vno_perk='41103' or
									// vno_perk='41104' or vno_perk='41105' or
									// vno_perk='41301' OR vno_perk='41116'
								} else {
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(
											terima.getNoPerk());
									jurnal.getTrioJurnalPK().setIdSub(terima.getNoSub());
									jurnal.setNamaPerkiraan("PPN KELUARAN");
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(NOL);
									jurnal.setKredit(terima.getDebet());
									jurnal.setBlc("*");
									jurnal.setTipe("K");
									listJurnal.add(jurnal);
								}

							} else { // else debet > 0

								if (terima.getKet().equalsIgnoreCase("4")) {

									vNoPerkA = "11120";

									vNoSub1 = terima.getKdPOS();
									if (vNoSub1.length() > 0) {
										vNmPerkA = getMasterFacade()
												.getTrioMstsubperkiraanDao()
												.findNamaSubByIdAndIdSub(vNoPerkA, vNoSub1);
									}

								} else { // else ket = 4
									vNoPerkA = "11102"; //11101
									vNmPerkA = "KAS";
								}
								jurnal = new TrioJurnal();
								jurnal.setTglJurnal(terima.getTglTrm());
								jurnal.getTrioJurnalPK().setIdPerkiraan(vNoPerkA);
								if (vNoSub1.length() > 0) {
									jurnal.getTrioJurnalPK().setIdSub(vNoSub1);
								} else {
									jurnal.getTrioJurnalPK().setIdSub("");
								}
								jurnal.setNamaPerkiraan(vNmPerkA);
								jurnal.setKet(terima.getNmTran());
								jurnal.setDebet(NOL);
								jurnal.setKredit(terima.getKredit());
								jurnal.setBlc("*");
								jurnal.setTipe("K");
								listJurnal.add(jurnal);

								vNoSub1 = "";

								vNmPerkA = getMasterFacade().getTrioMstperkiraanDao()
										.findNamaById(terima.getNoPerk());

								if (terima.getNoSub().length() > 0) {
									vNmPerkA = getMasterFacade()
											.getTrioMstsubperkiraanDao()
											.findNamaSubByIdAndIdSub(terima.getNoPerk(),
													terima.getNoSub());
								}

								// if vno_perk='41102' or vno_perk='41103' or
								// vno_perk='41104' or vno_perk='41105' OR vno_perk='41116'
								// || beda dengan diatas
								if (terima.getNoPerk().equalsIgnoreCase("41107")
										|| terima.getNoPerk().equalsIgnoreCase("41108")
										|| terima.getNoPerk().equalsIgnoreCase("41109")
										|| terima.getNoPerk().equalsIgnoreCase("41110")
										|| terima.getNoPerk().equalsIgnoreCase("41117")) {
									
									// 41102 -> 41107
									// 41103 -> 41108 
									// 41104 -> 41109
									// 41105 -> 41110
									// 41116 -> 41117
									
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(
											terima.getNoPerk());
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan(vNmPerkA);
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(vjumlah2);
									jurnal.setKredit(NOL);
									jurnal.setBlc("*");
									jurnal.setTipe("D");
									listJurnal.add(jurnal);

									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan("21404");
									jurnal.getTrioJurnalPK().setIdSub("");
									jurnal.setNamaPerkiraan("PPN KELUARAN");
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(vjumlah3);
									jurnal.setKredit(NOL);
									jurnal.setBlc("*");
									jurnal.setTipe("D");
									listJurnal.add(jurnal);

									// else if vno_perk='41102' or vno_perk='41103' or
									// vno_perk='41104' or vno_perk='41105' or
									// vno_perk='41301' OR vno_perk='41116'
								} else {
									jurnal = new TrioJurnal();
									jurnal.setTglJurnal(terima.getTglTrm());
									jurnal.getTrioJurnalPK().setIdPerkiraan(
											terima.getNoPerk());
									jurnal.getTrioJurnalPK().setIdSub(terima.getNoSub());
									jurnal.setNamaPerkiraan("PPN KELUARAN");
									jurnal.setKet(terima.getNmTran());
									jurnal.setDebet(terima.getKredit());
									jurnal.setKredit(NOL);
									jurnal.setBlc("*");
									jurnal.setTipe("D");
									listJurnal.add(jurnal);
								}

							}

							String jenisJurnal = getJenisJurnalByParams(terima.getKet(),
									terima.getDebet(), terima.getKredit(),
									terima.getNoPerk());
							getMasterFacade().getTrioJurnalDao().saveTransaction(
									listJurnal, jenisJurnal, user);

						}
			

		}

	}

	private String[] getNoPerkAndNmPerk(String ket2) {
		String[] arrStr = new String[2];
		String vNoPerk0 = "";
		String vNmPerk0 = "";
		if (ket2.equalsIgnoreCase("FIF") || ket2.equalsIgnoreCase("FIS")) {
			vNoPerk0 = "11302";
			vNmPerk0 = "PIUTANG SMH FIF";
		} else if (ket2.equalsIgnoreCase("ADR") || ket2.equalsIgnoreCase("ADS")) {
			vNoPerk0 = "11303";
			vNmPerk0 = "PIUTANG SMH ADIRA";
		} else if (ket2.equalsIgnoreCase("FIN")) {
			vNoPerk0 = "11304";
			vNmPerk0 = "PIUTANG SMH FINANSIA";
		} else if (ket2.equalsIgnoreCase("MCF") || ket2.equalsIgnoreCase("MCS")) {
			vNoPerk0 = "11305";
			vNmPerk0 = "PIUTANG SMH MCF";
		} else if (ket2.equalsIgnoreCase("NSC") || ket2.equalsIgnoreCase("NSS")) {
			vNoPerk0 = "11306";
			vNmPerk0 = "PIUTANG SMH NSC";
		} else if (ket2.equalsIgnoreCase("MMF")) {
			vNoPerk0 = "11307";
			vNmPerk0 = "PIUTANG SMH MMF";
		} else if (ket2.equalsIgnoreCase("SOF") || ket2.equalsIgnoreCase("OTO")) {
			vNoPerk0 = "11308";
			vNmPerk0 = "PIUTANG SMH SOF";
		} else if (ket2.equalsIgnoreCase("SOF") || ket2.equalsIgnoreCase("OTO")) {
			vNoPerk0 = "11308";
			vNmPerk0 = "PIUTANG SMH SOF";
		} else if (ket2.equalsIgnoreCase("WOM") || ket2.equalsIgnoreCase("WOS")) {
			vNoPerk0 = "11309";
			vNmPerk0 = "PIUTANG SMH WOM";
		} else if (ket2.equalsIgnoreCase("PMF") || ket2.equalsIgnoreCase("PAR")
				|| ket2.equalsIgnoreCase("MGF")) {
			vNoPerk0 = "11310";
			vNmPerk0 = "PIUTANG SMH PMF";
		} else if (ket2.equalsIgnoreCase("IMF")) {
			vNoPerk0 = "11329";
			vNmPerk0 = "PIUTANG SMH IMF";
		} else if (ket2.equalsIgnoreCase("SMF")) {
			vNoPerk0 = "11343";
			vNmPerk0 = "PIUTANG SMH SMF";
		} else if (ket2.equalsIgnoreCase("CSF")) {
			vNoPerk0 = "11345";
			vNmPerk0 = "PIUTANG SMH CSF";
		} else {
			vNoPerk0 = "11399";
			vNmPerk0 = "PIUTANG SMH LAIN-LAIN";
		}
		arrStr[0] = vNoPerk0;
		arrStr[1] = vNmPerk0;
		return arrStr;
	}
	
	private String[] getNoPerkAndNmPerkForCab2(String ket2) {
		String[] arrStr = new String[2];
		String vNoPerk0 = "";
		String vNmPerk0 = "";
		if (ket2.equalsIgnoreCase("FIF") || ket2.equalsIgnoreCase("FIS")) {
			vNoPerk0 = "11316"; //11302
			vNmPerk0 = "PIUTANG SMH FIF";
		} else if (ket2.equalsIgnoreCase("ADR") || ket2.equalsIgnoreCase("ADS")) {
			vNoPerk0 = "11317"; //11303
			vNmPerk0 = "PIUTANG SMH ADIRA";
		} else if (ket2.equalsIgnoreCase("FIN")) {
			vNoPerk0 = "11318"; //11304
			vNmPerk0 = "PIUTANG SMH FINANSIA";
		} else if (ket2.equalsIgnoreCase("MCF") || ket2.equalsIgnoreCase("MCS")) {
			vNoPerk0 = "11319"; //11305
			vNmPerk0 = "PIUTANG SMH MCF";
		} else if (ket2.equalsIgnoreCase("NSC") || ket2.equalsIgnoreCase("NSS")) {
			vNoPerk0 = "11320"; //11306
			vNmPerk0 = "PIUTANG SMH NSC";
		} else if (ket2.equalsIgnoreCase("MMF")) {
			vNoPerk0 = "11321"; //11307
			vNmPerk0 = "PIUTANG SMH MMF";
		} else if (ket2.equalsIgnoreCase("SOF") || ket2.equalsIgnoreCase("OTO")) {
			vNoPerk0 = "11322"; //11308
			vNmPerk0 = "PIUTANG SMH SOF";
		} else if (ket2.equalsIgnoreCase("WOM") || ket2.equalsIgnoreCase("WOS")) {
			vNoPerk0 = "11323"; //11309
			vNmPerk0 = "PIUTANG SMH WOM";
		} else if (ket2.equalsIgnoreCase("PMF") || ket2.equalsIgnoreCase("PAR")
				|| ket2.equalsIgnoreCase("MGF")) {
			vNoPerk0 = "11324"; //11310
			vNmPerk0 = "PIUTANG SMH PMF";
		} else if (ket2.equalsIgnoreCase("IMF")) {
			vNoPerk0 = "11330"; //11329
			vNmPerk0 = "PIUTANG SMH IMF";
		} else if (ket2.equalsIgnoreCase("SMF")) {
			vNoPerk0 = "11344"; //11343
			vNmPerk0 = "PIUTANG SMH SMF";
		} else if (ket2.equalsIgnoreCase("CSF")) {
			vNoPerk0 = "11346"; //11345
			vNmPerk0 = "PIUTANG SMH CSF";
		} else {
			vNoPerk0 = "11399";
			vNmPerk0 = "PIUTANG SMH LAIN-LAIN";
		}
		arrStr[0] = vNoPerk0;
		arrStr[1] = vNmPerk0;
		return arrStr;
	}

	private String getJenisJurnalByParams(String vket, BigDecimal debet,
			BigDecimal kredit, String noPerk) {

		// if ((vket='1' and debet>0) or (vket='K' and kredit>0)) AND
		// vno_perk<>'P0' -> JUM else JME
		String jenisJurnal = null;
		if ((vket.equalsIgnoreCase("1") && debet.compareTo(NOL) == 1)
				|| (vket.equalsIgnoreCase("K") && kredit.compareTo(NOL) == 1)
				&& (!noPerk.equalsIgnoreCase("P0"))) {
			jenisJurnal = "JUM";
		} else {
			jenisJurnal = "JME";
		}
		// System.out.println("Ket "+ vket);
		// System.out.println("Debet "+ debet);
		// System.out.println("Kredit "+ kredit);
		// System.out.println("NoPerk "+ noPerk);
		// System.out.println("Jenis Jurnal "+ jenisJurnal);

		return jenisJurnal;
	}

	private TrioTerima getTerimaByNoFak(String noFak) {
		// SELECT * FROM trio_terima WHERE (NO_PERK='P1' OR NO_PERK='P0') AND
		// no_fak='' AND no_lkh<>'SJALAN'
		String sQ = "select t from TrioTerima t where (t.noPerk = 'P1' or t.noPerk = 'P0') and t.trioTerimaPK.noFak = :noFak and t.trioTerimaPK.noLKH <> 'SJALAN' ";
		Query q = getHibernateTemplate().getSessionFactory()
				.getCurrentSession().createQuery(sQ);
		q.setString("noFak", noFak);
		return (TrioTerima) q.uniqueResult();
	}

	private TrioTerima getTerimaByNoFakAndNoPerk(String noFak, String noPerk) {
		String sQ = "select t from TrioTerima t where t.trioTerimaPK.noFak = :noFak and t.noPerk = :noPerk ";
		Query q = getHibernateTemplate().getSessionFactory()
				.getCurrentSession().createQuery(sQ);
		q.setString("noFak", noFak);
		q.setString("noPerk", noPerk);
		return (TrioTerima) q.uniqueResult();
	}

}
