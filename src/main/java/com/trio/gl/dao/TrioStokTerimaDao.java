package com.trio.gl.dao;

import com.trio.gl.bean.TrioStokTerima;
import com.trio.gl.hibernate.TrioGenericDao;

/** @author Saifi Ahmada Jul 18, 2013 3:48:37 PM  **/

public interface TrioStokTerimaDao extends TrioGenericDao<TrioStokTerima> {

}

