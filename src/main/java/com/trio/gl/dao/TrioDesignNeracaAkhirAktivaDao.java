package com.trio.gl.dao;

import java.util.List;

import com.trio.gl.bean.TrioDesignNeracaAkhirAktiva;
import com.trio.gl.hibernate.TrioGenericDao;

/** @author Saifi Ahmada May 26, 2013 2:21:56 PM  **/

public interface TrioDesignNeracaAkhirAktivaDao extends TrioGenericDao<TrioDesignNeracaAkhirAktiva>{
	
	public void saveByList(List<TrioDesignNeracaAkhirAktiva> lists, String user);
	public void saveOrUpdateByList(List<TrioDesignNeracaAkhirAktiva> lists, String user);

}

