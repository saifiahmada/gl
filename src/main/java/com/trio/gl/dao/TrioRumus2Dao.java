package com.trio.gl.dao;

import java.util.List;

import com.trio.gl.bean.TrioRumus2;
import com.trio.gl.hibernate.TrioGenericDao;

/**
 * @author Gusti Arya 11:33:07 AM Dec 14, 2013
 */

public interface TrioRumus2Dao extends TrioGenericDao<TrioRumus2>{
	
	public void saveByList(List<TrioRumus2> lists, String user);
	public void saveOrUpdateByList(List<TrioRumus2> list, String user);
	public List<TrioRumus2> listSumNilai();

}
