package com.trio.gl.dao;

import com.trio.gl.bean.TrioMstaccessrole;
import com.trio.gl.hibernate.TrioGenericDao;

/** @author Saifi Ahmada Feb 7, 2013 4:49:47 PM  **/

public interface TrioMstaccessroleDao extends TrioGenericDao<TrioMstaccessrole> {

}
 
