package com.trio.gl.dao;

import java.util.List;

import com.trio.gl.bean.TrioDesignRugiLaba;
import com.trio.gl.hibernate.TrioGenericDao;

public interface TrioDesignRugiLabaDao extends TrioGenericDao<TrioDesignRugiLaba>{
	
	public void updateByList(List<TrioDesignRugiLaba> list, String user);
	public void saveOrUpdateByList(List<TrioDesignRugiLaba> list, String user);
	public List<TrioDesignRugiLaba> rlExcel();
	public List<TrioDesignRugiLaba> rlSave();

}
