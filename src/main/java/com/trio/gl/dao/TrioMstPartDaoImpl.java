package com.trio.gl.dao;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trio.gl.bean.TrioMstPart;
import com.trio.gl.hibernate.TrioHibernateDaoSupport;

/** @author Saifi Ahmada Jul 20, 2013 12:21:50 PM  **/

@Service
public class TrioMstPartDaoImpl extends TrioHibernateDaoSupport implements TrioMstPartDao {
	
	@Transactional(readOnly=false)
	public void saveOrUpdate(TrioMstPart domain, String user) {
		// TODO , masbro
		getHibernateTemplate().getSessionFactory().getCurrentSession().saveOrUpdate(domain);
		
	}

	public void save(TrioMstPart domain, String user) {
		// TODO , masbro
		
		
	}

	public void update(TrioMstPart domain, String user) {
		// TODO , masbro
		
		
	}

	public void delete(TrioMstPart domain) {
		// TODO , masbro
		
		
	}

	public List<TrioMstPart> findAll() {
		// TODO , masbro
		
		return null;
	}

	public List<TrioMstPart> findByExample(TrioMstPart domain) {
		// TODO , masbro
		
		return null;
	}

	public List<TrioMstPart> findByCriteria(TrioMstPart domain) {
		// TODO , masbro
		
		return null;
	}

	public TrioMstPart findByPrimaryKey(TrioMstPart domain) {
		return (TrioMstPart) getHibernateTemplate().getSessionFactory().getCurrentSession().get(TrioMstPart.class, domain.getPartNum());
	}

}

