package com.trio.gl.dao;

import java.util.List;

import com.trio.gl.bean.TrioMstsubperkiraan;
import com.trio.gl.hibernate.TrioGenericDao;

/** @author Saifi Ahmada Mar 7, 2013 2:00:38 PM  **/

public interface TrioMstsubperkiraanDao extends TrioGenericDao<TrioMstsubperkiraan> {
	public List<TrioMstsubperkiraan> findByIdPerkiraan(String idPerkiraan);
	public List<TrioMstsubperkiraan> getBulSub();
	public void updateQuery(String field);
	public String findNamaSubByIdAndIdSub(String idPerk, String idSub);
	public TrioMstsubperkiraan getSubByIdPerkAndKet2(String idPerk, String ket2);
}

