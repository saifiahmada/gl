package com.trio.gl.dao;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trio.gl.bean.TrioRmsNeracaAktiva;
import com.trio.gl.hibernate.TrioHibernateDaoSupport;

/**
 * @author Gusti Arya 10:11:29 AM Oct 31, 2013
 */

@Service
public class TrioRmsNeracaAktivaDaoImpl extends TrioHibernateDaoSupport implements  TrioRmsNeracaAktivaDao{

	@Override
	public void saveOrUpdate(TrioRmsNeracaAktiva domain, String user) {
		// TODO Auto-generated method stub

	}

	@Override
	public void save(TrioRmsNeracaAktiva domain, String user) {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(TrioRmsNeracaAktiva domain, String user) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(TrioRmsNeracaAktiva domain) {
		// TODO Auto-generated method stub

	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<TrioRmsNeracaAktiva> findAll() {
		String hql = "from TrioRmsNeracaAktiva";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		return q.list();
	}

	@Override
	public List<TrioRmsNeracaAktiva> findByExample(TrioRmsNeracaAktiva domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TrioRmsNeracaAktiva> findByCriteria(TrioRmsNeracaAktiva domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TrioRmsNeracaAktiva findByPrimaryKey(TrioRmsNeracaAktiva domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional(readOnly=false)
	public void saveByList(List<TrioRmsNeracaAktiva> lists, String username) {
		String hql = "delete from TrioRmsNeracaAktiva";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		q.executeUpdate();

		for(TrioRmsNeracaAktiva current : lists){
			getHibernateTemplate().getSessionFactory().getCurrentSession().persist(current);
		}
	}

	@Override
	@Transactional(readOnly=false)
	public void saveOrUpdateByList(List<TrioRmsNeracaAktiva> lists,
			String userName) {
		
		for(TrioRmsNeracaAktiva current : lists){
			getHibernateTemplate().getSessionFactory().getCurrentSession().saveOrUpdate(current);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<TrioRmsNeracaAktiva> getSumByKdHsl() {
		String hql = "select new TrioRmsNeracaAktiva(t.kdHsl, sum(t.nilai)) from TrioRmsNeracaAktiva t "
				+"group by t.kdHsl";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		
		return q.list();
	}
}
