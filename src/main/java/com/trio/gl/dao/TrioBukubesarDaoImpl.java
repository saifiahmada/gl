package com.trio.gl.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trio.gl.bean.TrioBukubesar;
import com.trio.gl.bean.TrioJurnal;
import com.trio.gl.bean.TrioLapBukubesar;
import com.trio.gl.bean.TrioMstperkiraan;
import com.trio.gl.bean.TrioMstsaldoperkiraan;
import com.trio.gl.hibernate.TrioHibernateDaoSupport;
import com.trio.gl.util.TrioDateUtil;

/** @author Saifi Ahmada Apr 12, 2013 10:35:38 PM  **/

@Service
public class TrioBukubesarDaoImpl extends TrioHibernateDaoSupport implements TrioBukubesarDao {

	public void saveOrUpdate(TrioBukubesar domain, String user) {
		// TODO , masbro
		
		
	}

	@Transactional(readOnly=false)
	public void save(TrioBukubesar domain, String user) {
		// TODO , masbro
		domain.getUserTrailing().setVcreaby(user);
		domain.getUserTrailing().setDcrea(TrioDateUtil.getLongSysDate());
		getHibernateTemplate().getSessionFactory().getCurrentSession().save(domain);
		getHibernateTemplate().getSessionFactory().getCurrentSession().flush();
		getHibernateTemplate().getSessionFactory().getCurrentSession().refresh(domain);
		
	}
	@Transactional(readOnly=false)
	public void update(TrioBukubesar domain, String user) {
		// TODO , masbro
		domain.getUserTrailing().setVmodiby(user);
		domain.getUserTrailing().setDmodi(TrioDateUtil.getLongSysDate());
		getHibernateTemplate().getSessionFactory().getCurrentSession().merge(domain);
		getHibernateTemplate().getSessionFactory().getCurrentSession().flush();
		getHibernateTemplate().getSessionFactory().getCurrentSession().refresh(domain);
		
	}

	public void delete(TrioBukubesar domain) {
		// TODO , masbro
		
		
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<TrioBukubesar> findAll() {
		return getHibernateTemplate().find("from TrioBukubesar");
	}

	public List<TrioBukubesar> findByExample(TrioBukubesar domain) {
		// TODO , masbro
		
		return null;
	}

	public List<TrioBukubesar> findByCriteria(TrioBukubesar domain) {
		// TODO , masbro
		
		return null;
	}

	public TrioBukubesar findByPrimaryKey(TrioBukubesar domain) {
		// TODO , masbro
		
		return null;
	}
	
	@Transactional(readOnly=false)
	public void saveListJurnalToBukuBesar(List<TrioJurnal> listJurnal,Date tglAwal, Date tglAkhir, String user){
		
			int i = 0;
			for (TrioJurnal jurnal : listJurnal){
				TrioBukubesar bukuBesar = new TrioBukubesar(jurnal.getTrioJurnalPK().getIdJurnal(), 
						jurnal.getTrioJurnalPK().getIdPerkiraan(), jurnal.getTrioJurnalPK().getIdSub());
				
				bukuBesar.setTanggal(jurnal.getTglJurnal());
				bukuBesar.setDebet(jurnal.getDebet());
				bukuBesar.setKredit(jurnal.getKredit());
				bukuBesar.setKet(jurnal.getKet());
				
				save(bukuBesar, user);
				System.out.println("save ke ["+ i++ +"]"); 
			}
			
//			String sQ = "select j from TrioBukubesar j where j.tanggal between :tglAwal and :tglAkhir ";
//			
//			Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(sQ);
//			q.setDate("tglAwal", tglAwal);
//			q.setDate("tglAkhir", tglAkhir);
//			System.out.println("Tgl awal "+tglAwal);
//			System.out.println("Tgl akhir "+tglAkhir);
//			
//			
//			List<TrioBukubesar> listBuku = q.list();
//			for (TrioBukubesar buku : listBuku){
//				System.out.println("SUB "+ buku.getTrioMstperkiraan().getSub());
//			}
		
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<TrioBukubesar> getListBukuBesarByRangeTanggal(Date tglAwal, Date tglAkhir){
		String sQ = "select j from TrioBukubesar j where j.tanggal between :tglAwal and :tglAkhir ";
		
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(sQ);
		q.setDate("tglAwal", tglAwal);
		q.setDate("tglAkhir", tglAkhir);
		System.out.println("Tgl awal "+tglAwal);
		System.out.println("Tgl akhir "+tglAkhir);
		System.out.println("list berjumlah "+q.list());
		return q.list();
	}
	
	@Transactional(readOnly=false)
	public String prosesBukubesar(List<TrioJurnal> listJurnal,Date tglAwal, Date tglAkhir, boolean isSimpanLap, String user){
		
		List<TrioBukubesar> listBukubesar = new ArrayList<TrioBukubesar>();
		for (TrioJurnal jurnal : listJurnal){
			TrioBukubesar bukuBesar = new TrioBukubesar(jurnal.getTrioJurnalPK().getIdJurnal(), 
					jurnal.getTrioJurnalPK().getIdPerkiraan(), jurnal.getTrioJurnalPK().getIdSub());

			bukuBesar.setTanggal(jurnal.getTglJurnal());
			bukuBesar.setDebet(jurnal.getDebet());
			bukuBesar.setKredit(jurnal.getKredit());
			bukuBesar.setKet(jurnal.getKet());
			try {
				save(bukuBesar, user);
			} catch (ConstraintViolationException cve){
				return "Duplicate Key pada Buku_Besar " + bukuBesar.getTrioBukubesarPK() ;
			}
			
		}
		
		listBukubesar = getListBukuBesarByRangeTanggal(tglAwal, tglAkhir);
		
		int bulan = TrioDateUtil.getMonth(tglAwal);
		int tahun = TrioDateUtil.getYear(tglAwal);
			
			System.out.println("Ukuran "+listBukubesar.size());
			
			for (TrioBukubesar buku : listBukubesar){
				buku.setTrioMstperkiraan(new TrioMstperkiraan(buku.getTrioBukubesarPK().getIdPerkiraan()));
				System.out.println("id perkiraan = " + buku.getTrioBukubesarPK().getIdPerkiraan());
				TrioMstperkiraan mstPerk =getMasterFacade().getTrioMstperkiraanDao()
				.findByPrimaryKey(new TrioMstperkiraan(buku.getTrioBukubesarPK().getIdPerkiraan()));
				System.out.println("SUB = "+ mstPerk.getSub());
				System.out.println("NAMA = "+ mstPerk.getNamaPerkiraan());
				
				if (bulan == 1){
					if (mstPerk.getSub().equalsIgnoreCase("T")){
						TrioMstsaldoperkiraan saldo = getMasterFacade().getTrioMstsaldoperkiraanDao()
							.findByPrimaryKey(new TrioMstsaldoperkiraan(buku.getTrioBukubesarPK().getIdPerkiraan(), String.valueOf(tahun)));
						buku.setSaldoAwal(saldo.getBul0());
						if (saldo.getKet0().equalsIgnoreCase("D")){
							buku.setKetAwal("D");
						}else{
							buku.setKetAwal("K");
						}
					}
					if (! buku.getTrioBukubesarPK().getIdSub().equalsIgnoreCase("")){
						BigDecimal totalSaldo = getMasterFacade().getTrioMstsaldosubperkiraanDao()
								.getSaldoSubByIdPerkBulanTahun(buku.getTrioBukubesarPK().getIdPerkiraan(), String.valueOf(bulan), String.valueOf(tahun));
						buku.setSaldoAwal(totalSaldo);
						buku.setKetAwal("D");
						
					}
					System.out.println("update buku besar");
					update(buku, user);
				
				}else if (bulan == 2){
					if (mstPerk.getSub().equalsIgnoreCase("T")){
						TrioMstsaldoperkiraan saldo = getMasterFacade().getTrioMstsaldoperkiraanDao()
							.findByPrimaryKey(new TrioMstsaldoperkiraan(buku.getTrioBukubesarPK().getIdPerkiraan(), String.valueOf(tahun)));
						buku.setSaldoAwal(saldo.getBul1());
						if (saldo.getKet1().equalsIgnoreCase("D")){
							buku.setKetAwal("D");
						}else{
							buku.setKetAwal("K");
						}
					}
					if (! buku.getTrioBukubesarPK().getIdSub().equalsIgnoreCase("")){
						BigDecimal totalSaldo = getMasterFacade().getTrioMstsaldosubperkiraanDao()
								.getSaldoSubByIdPerkBulanTahun(buku.getTrioBukubesarPK().getIdPerkiraan(), String.valueOf(bulan), String.valueOf(tahun));
						buku.setSaldoAwal(totalSaldo);
						buku.setKetAwal("D");
						
					}
					System.out.println("update buku besar");
					update(buku, user);
					
				}else if (bulan == 3){
					if (mstPerk.getSub().equalsIgnoreCase("T")){
						TrioMstsaldoperkiraan saldo = getMasterFacade().getTrioMstsaldoperkiraanDao()
							.findByPrimaryKey(new TrioMstsaldoperkiraan(buku.getTrioBukubesarPK().getIdPerkiraan(), String.valueOf(tahun)));
						buku.setSaldoAwal(saldo.getBul2());
						if (saldo.getKet2().equalsIgnoreCase("D")){
							buku.setKetAwal("D");
						}else{
							buku.setKetAwal("K");
						}
						
					}
					if (! buku.getTrioBukubesarPK().getIdSub().equalsIgnoreCase("")){
						BigDecimal totalSaldo = getMasterFacade().getTrioMstsaldosubperkiraanDao()
								.getSaldoSubByIdPerkBulanTahun(buku.getTrioBukubesarPK().getIdPerkiraan(), String.valueOf(bulan), String.valueOf(tahun));
						buku.setSaldoAwal(totalSaldo);
						buku.setKetAwal("D");
						
					}
					System.out.println("update buku besar");
					update(buku, user);
					
				}else if (bulan == 4){
					if (mstPerk.getSub().equalsIgnoreCase("T")){
						TrioMstsaldoperkiraan saldo = getMasterFacade().getTrioMstsaldoperkiraanDao()
							.findByPrimaryKey(new TrioMstsaldoperkiraan(buku.getTrioBukubesarPK().getIdPerkiraan(), String.valueOf(tahun)));
						buku.setSaldoAwal(saldo.getBul3());
						if (saldo.getKet3().equalsIgnoreCase("D")){
							buku.setKetAwal("D");
						}else{
							buku.setKetAwal("K");
						}
						
					}
					if (! buku.getTrioBukubesarPK().getIdSub().equalsIgnoreCase("")){
						BigDecimal totalSaldo = getMasterFacade().getTrioMstsaldosubperkiraanDao()
								.getSaldoSubByIdPerkBulanTahun(buku.getTrioBukubesarPK().getIdPerkiraan(), String.valueOf(bulan), String.valueOf(tahun));
						buku.setSaldoAwal(totalSaldo);
						buku.setKetAwal("D");
						
					}
					System.out.println("update buku besar");
					update(buku, user);
					
				}else if (bulan == 5){
					if (mstPerk.getSub().equalsIgnoreCase("T")){
						TrioMstsaldoperkiraan saldo = getMasterFacade().getTrioMstsaldoperkiraanDao()
							.findByPrimaryKey(new TrioMstsaldoperkiraan(buku.getTrioBukubesarPK().getIdPerkiraan(), String.valueOf(tahun)));
						buku.setSaldoAwal(saldo.getBul4());
						if (saldo.getKet4().equalsIgnoreCase("D")){
							buku.setKetAwal("D");
						}else{
							buku.setKetAwal("K");
						}
						
					}
					if (! buku.getTrioBukubesarPK().getIdSub().equalsIgnoreCase("")){
						BigDecimal totalSaldo = getMasterFacade().getTrioMstsaldosubperkiraanDao()
								.getSaldoSubByIdPerkBulanTahun(buku.getTrioBukubesarPK().getIdPerkiraan(), String.valueOf(bulan), String.valueOf(tahun));
						buku.setSaldoAwal(totalSaldo);
						buku.setKetAwal("D");
						
					}
					System.out.println("update buku besar");
					update(buku, user);
					
				}else if (bulan == 6){
					if (mstPerk.getSub().equalsIgnoreCase("T")){
						TrioMstsaldoperkiraan saldo = getMasterFacade().getTrioMstsaldoperkiraanDao()
							.findByPrimaryKey(new TrioMstsaldoperkiraan(buku.getTrioBukubesarPK().getIdPerkiraan(), String.valueOf(tahun)));
						buku.setSaldoAwal(saldo.getBul5());
						if (saldo.getKet5().equalsIgnoreCase("D")){
							buku.setKetAwal("D");
						}else{
							buku.setKetAwal("K");
						}
						
					}
					if (! buku.getTrioBukubesarPK().getIdSub().equalsIgnoreCase("")){
						BigDecimal totalSaldo = getMasterFacade().getTrioMstsaldosubperkiraanDao()
								.getSaldoSubByIdPerkBulanTahun(buku.getTrioBukubesarPK().getIdPerkiraan(), String.valueOf(bulan), String.valueOf(tahun));
						buku.setSaldoAwal(totalSaldo);
						buku.setKetAwal("D");
						
					}
					System.out.println("update buku besar");
					update(buku, user);
					
				}else if (bulan == 7){
					if (mstPerk.getSub().equalsIgnoreCase("T")){
						TrioMstsaldoperkiraan saldo = getMasterFacade().getTrioMstsaldoperkiraanDao()
							.findByPrimaryKey(new TrioMstsaldoperkiraan(buku.getTrioBukubesarPK().getIdPerkiraan(), String.valueOf(tahun)));
						buku.setSaldoAwal(saldo.getBul6());
						if (saldo.getKet6().equalsIgnoreCase("D")){
							buku.setKetAwal("D");
						}else{
							buku.setKetAwal("K");
						}
						
					}
					if (! buku.getTrioBukubesarPK().getIdSub().equalsIgnoreCase("")){
						BigDecimal totalSaldo = getMasterFacade().getTrioMstsaldosubperkiraanDao()
								.getSaldoSubByIdPerkBulanTahun(buku.getTrioBukubesarPK().getIdPerkiraan(), String.valueOf(bulan), String.valueOf(tahun));
						buku.setSaldoAwal(totalSaldo);
						buku.setKetAwal("D");
						
					}
					System.out.println("update buku besar");
					update(buku, user);
					
				}else if (bulan == 8){
					if (mstPerk.getSub().equalsIgnoreCase("T")){
						TrioMstsaldoperkiraan saldo = getMasterFacade().getTrioMstsaldoperkiraanDao()
							.findByPrimaryKey(new TrioMstsaldoperkiraan(buku.getTrioBukubesarPK().getIdPerkiraan(), String.valueOf(tahun)));
						buku.setSaldoAwal(saldo.getBul7());
						if (saldo.getKet7().equalsIgnoreCase("D")){
							buku.setKetAwal("D");
						}else{
							buku.setKetAwal("K");
						}
						
					}
					if (! buku.getTrioBukubesarPK().getIdSub().equalsIgnoreCase("")){
						BigDecimal totalSaldo = getMasterFacade().getTrioMstsaldosubperkiraanDao()
								.getSaldoSubByIdPerkBulanTahun(buku.getTrioBukubesarPK().getIdPerkiraan(), String.valueOf(bulan), String.valueOf(tahun));
						buku.setSaldoAwal(totalSaldo);
						buku.setKetAwal("D");
						
					}
					System.out.println("update buku besar");
					update(buku, user);
					
				}else if (bulan == 9){
					if (mstPerk.getSub().equalsIgnoreCase("T")){
						TrioMstsaldoperkiraan saldo = getMasterFacade().getTrioMstsaldoperkiraanDao()
							.findByPrimaryKey(new TrioMstsaldoperkiraan(buku.getTrioBukubesarPK().getIdPerkiraan(), String.valueOf(tahun)));
						
						
						System.out.println("Saldo bul 8 = " + saldo.getBul8());
						System.out.println("Ket bul 8 = " + saldo.getKet8());
						buku.setSaldoAwal(saldo.getBul8());
						if (saldo.getKet8().equalsIgnoreCase("D")){
							buku.setKetAwal("D");
						}else{
							buku.setKetAwal("K");
						}
						
					}
					if (! buku.getTrioBukubesarPK().getIdSub().equalsIgnoreCase("")){
						BigDecimal totalSaldo = getMasterFacade().getTrioMstsaldosubperkiraanDao()
								.getSaldoSubByIdPerkBulanTahun(buku.getTrioBukubesarPK().getIdPerkiraan(), String.valueOf(bulan), String.valueOf(tahun));
						buku.setSaldoAwal(totalSaldo);
						buku.setKetAwal("D");
						
					}
					System.out.println("update buku besar");
					update(buku, user);
					
				}else if (bulan == 10){
					if (mstPerk.getSub().equalsIgnoreCase("T")){
						TrioMstsaldoperkiraan saldo = getMasterFacade().getTrioMstsaldoperkiraanDao()
							.findByPrimaryKey(new TrioMstsaldoperkiraan(buku.getTrioBukubesarPK().getIdPerkiraan(), String.valueOf(tahun)));
						buku.setSaldoAwal(saldo.getBul9());
						if (saldo.getKet9().equalsIgnoreCase("D")){
							buku.setKetAwal("D");
						}else{
							buku.setKetAwal("K");
						}
						
					}
					if (! buku.getTrioBukubesarPK().getIdSub().equalsIgnoreCase("")){
						BigDecimal totalSaldo = getMasterFacade().getTrioMstsaldosubperkiraanDao()
								.getSaldoSubByIdPerkBulanTahun(buku.getTrioBukubesarPK().getIdPerkiraan(), String.valueOf(bulan), String.valueOf(tahun));
						buku.setSaldoAwal(totalSaldo);
						buku.setKetAwal("D");
					}
					System.out.println("update buku besar");
					update(buku, user);
					
				}else if (bulan == 11){
					if (mstPerk.getSub().equalsIgnoreCase("T")){
						TrioMstsaldoperkiraan saldo = getMasterFacade().getTrioMstsaldoperkiraanDao()
							.findByPrimaryKey(new TrioMstsaldoperkiraan(buku.getTrioBukubesarPK().getIdPerkiraan(), String.valueOf(tahun)));
						buku.setSaldoAwal(saldo.getBul10());
						if (saldo.getKet10().equalsIgnoreCase("D")){
							buku.setKetAwal("D");
						}else{
							buku.setKetAwal("K");
						}
						
					}
					if (! buku.getTrioBukubesarPK().getIdSub().equalsIgnoreCase("")){
						BigDecimal totalSaldo = getMasterFacade().getTrioMstsaldosubperkiraanDao()
								.getSaldoSubByIdPerkBulanTahun(buku.getTrioBukubesarPK().getIdPerkiraan(), String.valueOf(bulan), String.valueOf(tahun));
						buku.setSaldoAwal(totalSaldo);
						buku.setKetAwal("D");
						
					}
					System.out.println("update buku besar");
					update(buku, user);
					
				}else if (bulan == 12){
					if (mstPerk.getSub().equalsIgnoreCase("T")){
						TrioMstsaldoperkiraan saldo = getMasterFacade().getTrioMstsaldoperkiraanDao()
							.findByPrimaryKey(new TrioMstsaldoperkiraan(buku.getTrioBukubesarPK().getIdPerkiraan(), String.valueOf(tahun)));
						buku.setSaldoAwal(saldo.getBul11());
						if (saldo.getKet11().equalsIgnoreCase("D")){
							buku.setKetAwal("D");
						}else{
							buku.setKetAwal("K");
						}
						
					}
					if (! buku.getTrioBukubesarPK().getIdSub().equalsIgnoreCase("")){
						BigDecimal totalSaldo = getMasterFacade().getTrioMstsaldosubperkiraanDao()
								.getSaldoSubByIdPerkBulanTahun(buku.getTrioBukubesarPK().getIdPerkiraan(), String.valueOf(bulan), String.valueOf(tahun));
						buku.setSaldoAwal(totalSaldo);
						buku.setKetAwal("D");
						
					}
					System.out.println("update buku besar");
					update(buku, user);
					
				}
				
			}
			
			if (isSimpanLap){
				TrioLapBukubesar lap;
				List<TrioBukubesar> newListBukuBesar = getListBukuBesarByRangeTanggal(tglAwal, tglAkhir);
				for (TrioBukubesar b : newListBukuBesar){
					lap = new TrioLapBukubesar(b.getTrioBukubesarPK().getIdJurnal(), b.getTrioBukubesarPK().getIdPerkiraan(),
							b.getTrioBukubesarPK().getIdSub());
					
					lap.setPeriode(TrioDateUtil.getPeriode(tglAwal));
					lap.setTanggal(b.getTanggal());
					lap.setDebet(b.getDebet());
					lap.setKredit(b.getKredit());
					lap.setSaldoAwal(b.getSaldoAwal());
					lap.setSaldoAkhir(new BigDecimal(0));
					lap.setKet(b.getKet());
					lap.setTipe(b.getTipe());
					System.out.println("Periode = "+ lap.getPeriode());
					getMasterFacade().getTrioLapBukubesarDao().save(lap, user); 
					
				}
			}
			
			return "Sukses";
	}

	@Transactional(readOnly=false)
	public void saveAndProsesBukuBesar(List<TrioJurnal> listJurnal,	Date tglAwal, Date tglAkhir, String user) {
		
		saveListJurnalToBukuBesar(listJurnal, tglAwal, tglAkhir, user);
		List<TrioBukubesar> listBukubesar = getListBukuBesarByRangeTanggal(tglAwal, tglAkhir);
		
		int bulan = TrioDateUtil.getMonth(tglAwal);
		int tahun = TrioDateUtil.getYear(tglAwal);
			
			for (TrioBukubesar buku : listBukubesar){
				
				
				if (bulan == 1){
					if (buku.getTrioMstperkiraan().getSub().equalsIgnoreCase("T")){
						TrioMstsaldoperkiraan saldo = getMasterFacade().getTrioMstsaldoperkiraanDao()
							.findByPrimaryKey(new TrioMstsaldoperkiraan(buku.getTrioBukubesarPK().getIdPerkiraan(), String.valueOf(tahun)));
						buku.setSaldoAwal(saldo.getBul0());
						if (saldo.getKet0().equalsIgnoreCase("D")){
							buku.setKetAwal("D");
						}else{
							buku.setKetAwal("K");
						}
					}
					if (! buku.getTrioBukubesarPK().getIdSub().equalsIgnoreCase("")){
						BigDecimal totalSaldo = getMasterFacade().getTrioMstsaldosubperkiraanDao()
								.getSaldoSubByIdPerkBulanTahun(buku.getTrioBukubesarPK().getIdPerkiraan(), String.valueOf(bulan), String.valueOf(tahun));
						buku.setSaldoAwal(totalSaldo);
						buku.setKetAwal("D");
						
					}
					System.out.println("update buku besar");
					update(buku, user);
				
				}else if (bulan == 2){
					if (buku.getTrioMstperkiraan().getSub().equalsIgnoreCase("T")){
						TrioMstsaldoperkiraan saldo = getMasterFacade().getTrioMstsaldoperkiraanDao()
							.findByPrimaryKey(new TrioMstsaldoperkiraan(buku.getTrioBukubesarPK().getIdPerkiraan(), String.valueOf(tahun)));
						buku.setSaldoAwal(saldo.getBul1());
						if (saldo.getKet1().equalsIgnoreCase("D")){
							buku.setKetAwal("D");
						}else{
							buku.setKetAwal("K");
						}
					}
					if (! buku.getTrioBukubesarPK().getIdSub().equalsIgnoreCase("")){
						BigDecimal totalSaldo = getMasterFacade().getTrioMstsaldosubperkiraanDao()
								.getSaldoSubByIdPerkBulanTahun(buku.getTrioBukubesarPK().getIdPerkiraan(), String.valueOf(bulan), String.valueOf(tahun));
						buku.setSaldoAwal(totalSaldo);
						buku.setKetAwal("D");
						
					}
					System.out.println("update buku besar");
					update(buku, user);
					
				}else if (bulan == 3){
					if (buku.getTrioMstperkiraan().getSub().equalsIgnoreCase("T")){
						TrioMstsaldoperkiraan saldo = getMasterFacade().getTrioMstsaldoperkiraanDao()
							.findByPrimaryKey(new TrioMstsaldoperkiraan(buku.getTrioBukubesarPK().getIdPerkiraan(), String.valueOf(tahun)));
						buku.setSaldoAwal(saldo.getBul2());
						if (saldo.getKet2().equalsIgnoreCase("D")){
							buku.setKetAwal("D");
						}else{
							buku.setKetAwal("K");
						}
						
					}
					if (! buku.getTrioBukubesarPK().getIdSub().equalsIgnoreCase("")){
						BigDecimal totalSaldo = getMasterFacade().getTrioMstsaldosubperkiraanDao()
								.getSaldoSubByIdPerkBulanTahun(buku.getTrioBukubesarPK().getIdPerkiraan(), String.valueOf(bulan), String.valueOf(tahun));
						buku.setSaldoAwal(totalSaldo);
						buku.setKetAwal("D");
						
					}
					System.out.println("update buku besar");
					update(buku, user);
					
				}else if (bulan == 4){
					if (buku.getTrioMstperkiraan().getSub().equalsIgnoreCase("T")){
						TrioMstsaldoperkiraan saldo = getMasterFacade().getTrioMstsaldoperkiraanDao()
							.findByPrimaryKey(new TrioMstsaldoperkiraan(buku.getTrioBukubesarPK().getIdPerkiraan(), String.valueOf(tahun)));
						buku.setSaldoAwal(saldo.getBul3());
						if (saldo.getKet3().equalsIgnoreCase("D")){
							buku.setKetAwal("D");
						}else{
							buku.setKetAwal("K");
						}
						
					}
					if (! buku.getTrioBukubesarPK().getIdSub().equalsIgnoreCase("")){
						BigDecimal totalSaldo = getMasterFacade().getTrioMstsaldosubperkiraanDao()
								.getSaldoSubByIdPerkBulanTahun(buku.getTrioBukubesarPK().getIdPerkiraan(), String.valueOf(bulan), String.valueOf(tahun));
						buku.setSaldoAwal(totalSaldo);
						buku.setKetAwal("D");
						
					}
					System.out.println("update buku besar");
					update(buku, user);
					
				}else if (bulan == 5){
					if (buku.getTrioMstperkiraan().getSub().equalsIgnoreCase("T")){
						TrioMstsaldoperkiraan saldo = getMasterFacade().getTrioMstsaldoperkiraanDao()
							.findByPrimaryKey(new TrioMstsaldoperkiraan(buku.getTrioBukubesarPK().getIdPerkiraan(), String.valueOf(tahun)));
						buku.setSaldoAwal(saldo.getBul4());
						if (saldo.getKet4().equalsIgnoreCase("D")){
							buku.setKetAwal("D");
						}else{
							buku.setKetAwal("K");
						}
						
					}
					if (! buku.getTrioBukubesarPK().getIdSub().equalsIgnoreCase("")){
						BigDecimal totalSaldo = getMasterFacade().getTrioMstsaldosubperkiraanDao()
								.getSaldoSubByIdPerkBulanTahun(buku.getTrioBukubesarPK().getIdPerkiraan(), String.valueOf(bulan), String.valueOf(tahun));
						buku.setSaldoAwal(totalSaldo);
						buku.setKetAwal("D");
						
					}
					System.out.println("update buku besar");
					update(buku, user);
					
				}else if (bulan == 6){
					if (buku.getTrioMstperkiraan().getSub().equalsIgnoreCase("T")){
						TrioMstsaldoperkiraan saldo = getMasterFacade().getTrioMstsaldoperkiraanDao()
							.findByPrimaryKey(new TrioMstsaldoperkiraan(buku.getTrioBukubesarPK().getIdPerkiraan(), String.valueOf(tahun)));
						buku.setSaldoAwal(saldo.getBul5());
						if (saldo.getKet5().equalsIgnoreCase("D")){
							buku.setKetAwal("D");
						}else{
							buku.setKetAwal("K");
						}
						
					}
					if (! buku.getTrioBukubesarPK().getIdSub().equalsIgnoreCase("")){
						BigDecimal totalSaldo = getMasterFacade().getTrioMstsaldosubperkiraanDao()
								.getSaldoSubByIdPerkBulanTahun(buku.getTrioBukubesarPK().getIdPerkiraan(), String.valueOf(bulan), String.valueOf(tahun));
						buku.setSaldoAwal(totalSaldo);
						buku.setKetAwal("D");
						
					}
					System.out.println("update buku besar");
					update(buku, user);
					
				}else if (bulan == 7){
					if (buku.getTrioMstperkiraan().getSub().equalsIgnoreCase("T")){
						TrioMstsaldoperkiraan saldo = getMasterFacade().getTrioMstsaldoperkiraanDao()
							.findByPrimaryKey(new TrioMstsaldoperkiraan(buku.getTrioBukubesarPK().getIdPerkiraan(), String.valueOf(tahun)));
						buku.setSaldoAwal(saldo.getBul6());
						if (saldo.getKet6().equalsIgnoreCase("D")){
							buku.setKetAwal("D");
						}else{
							buku.setKetAwal("K");
						}
						
					}
					if (! buku.getTrioBukubesarPK().getIdSub().equalsIgnoreCase("")){
						BigDecimal totalSaldo = getMasterFacade().getTrioMstsaldosubperkiraanDao()
								.getSaldoSubByIdPerkBulanTahun(buku.getTrioBukubesarPK().getIdPerkiraan(), String.valueOf(bulan), String.valueOf(tahun));
						buku.setSaldoAwal(totalSaldo);
						buku.setKetAwal("D");
						
					}
					System.out.println("update buku besar");
					update(buku, user);
					
				}else if (bulan == 8){
					if (buku.getTrioMstperkiraan().getSub().equalsIgnoreCase("T")){
						TrioMstsaldoperkiraan saldo = getMasterFacade().getTrioMstsaldoperkiraanDao()
							.findByPrimaryKey(new TrioMstsaldoperkiraan(buku.getTrioBukubesarPK().getIdPerkiraan(), String.valueOf(tahun)));
						buku.setSaldoAwal(saldo.getBul7());
						if (saldo.getKet7().equalsIgnoreCase("D")){
							buku.setKetAwal("D");
						}else{
							buku.setKetAwal("K");
						}
						
					}
					if (! buku.getTrioBukubesarPK().getIdSub().equalsIgnoreCase("")){
						BigDecimal totalSaldo = getMasterFacade().getTrioMstsaldosubperkiraanDao()
								.getSaldoSubByIdPerkBulanTahun(buku.getTrioBukubesarPK().getIdPerkiraan(), String.valueOf(bulan), String.valueOf(tahun));
						buku.setSaldoAwal(totalSaldo);
						buku.setKetAwal("D");
						
					}
					System.out.println("update buku besar");
					update(buku, user);
					
				}else if (bulan == 9){
					if (buku.getTrioMstperkiraan().getSub().equalsIgnoreCase("T")){
						TrioMstsaldoperkiraan saldo = getMasterFacade().getTrioMstsaldoperkiraanDao()
							.findByPrimaryKey(new TrioMstsaldoperkiraan(buku.getTrioBukubesarPK().getIdPerkiraan(), String.valueOf(tahun)));
						buku.setSaldoAwal(saldo.getBul8());
						if (saldo.getKet8().equalsIgnoreCase("D")){
							buku.setKetAwal("D");
						}else{
							buku.setKetAwal("K");
						}
						
					}
					if (! buku.getTrioBukubesarPK().getIdSub().equalsIgnoreCase("")){
						BigDecimal totalSaldo = getMasterFacade().getTrioMstsaldosubperkiraanDao()
								.getSaldoSubByIdPerkBulanTahun(buku.getTrioBukubesarPK().getIdPerkiraan(), String.valueOf(bulan), String.valueOf(tahun));
						buku.setSaldoAwal(totalSaldo);
						buku.setKetAwal("D");
						
					}
					System.out.println("update buku besar");
					update(buku, user);
					
				}else if (bulan == 10){
					if (buku.getTrioMstperkiraan().getSub().equalsIgnoreCase("T")){
						TrioMstsaldoperkiraan saldo = getMasterFacade().getTrioMstsaldoperkiraanDao()
							.findByPrimaryKey(new TrioMstsaldoperkiraan(buku.getTrioBukubesarPK().getIdPerkiraan(), String.valueOf(tahun)));
						buku.setSaldoAwal(saldo.getBul9());
						if (saldo.getKet9().equalsIgnoreCase("D")){
							buku.setKetAwal("D");
						}else{
							buku.setKetAwal("K");
						}
						
					}
					if (! buku.getTrioBukubesarPK().getIdSub().equalsIgnoreCase("")){
						BigDecimal totalSaldo = getMasterFacade().getTrioMstsaldosubperkiraanDao()
								.getSaldoSubByIdPerkBulanTahun(buku.getTrioBukubesarPK().getIdPerkiraan(), String.valueOf(bulan), String.valueOf(tahun));
						buku.setSaldoAwal(totalSaldo);
						buku.setKetAwal("D");
						
					}
					System.out.println("update buku besar");
					update(buku, user);
					
				}else if (bulan == 11){
					if (buku.getTrioMstperkiraan().getSub().equalsIgnoreCase("T")){
						TrioMstsaldoperkiraan saldo = getMasterFacade().getTrioMstsaldoperkiraanDao()
							.findByPrimaryKey(new TrioMstsaldoperkiraan(buku.getTrioBukubesarPK().getIdPerkiraan(), String.valueOf(tahun)));
						buku.setSaldoAwal(saldo.getBul10());
						if (saldo.getKet10().equalsIgnoreCase("D")){
							buku.setKetAwal("D");
						}else{
							buku.setKetAwal("K");
						}
						
					}
					if (! buku.getTrioBukubesarPK().getIdSub().equalsIgnoreCase("")){
						BigDecimal totalSaldo = getMasterFacade().getTrioMstsaldosubperkiraanDao()
								.getSaldoSubByIdPerkBulanTahun(buku.getTrioBukubesarPK().getIdPerkiraan(), String.valueOf(bulan), String.valueOf(tahun));
						buku.setSaldoAwal(totalSaldo);
						buku.setKetAwal("D");
						
					}
					System.out.println("update buku besar");
					update(buku, user);
					
				}else if (bulan == 12){
					if (buku.getTrioMstperkiraan().getSub().equalsIgnoreCase("T")){
						TrioMstsaldoperkiraan saldo = getMasterFacade().getTrioMstsaldoperkiraanDao()
							.findByPrimaryKey(new TrioMstsaldoperkiraan(buku.getTrioBukubesarPK().getIdPerkiraan(), String.valueOf(tahun)));
						buku.setSaldoAwal(saldo.getBul11());
						if (saldo.getKet11().equalsIgnoreCase("D")){
							buku.setKetAwal("D");
						}else{
							buku.setKetAwal("K");
						}
						
					}
					if (! buku.getTrioBukubesarPK().getIdSub().equalsIgnoreCase("")){
						BigDecimal totalSaldo = getMasterFacade().getTrioMstsaldosubperkiraanDao()
								.getSaldoSubByIdPerkBulanTahun(buku.getTrioBukubesarPK().getIdPerkiraan(), String.valueOf(bulan), String.valueOf(tahun));
						buku.setSaldoAwal(totalSaldo);
						buku.setKetAwal("D");
						
					}
					System.out.println("update buku besar");
					update(buku, user);
					
				}
				
				System.out.println("perkiraan "+buku.getTrioBukubesarPK().getIdPerkiraan());
				System.out.println("saldo awal "+buku.getSaldoAwal());
				System.out.println("ket awal "+buku.getKetAwal());
				
			}
		
		
		
	}
	

}



