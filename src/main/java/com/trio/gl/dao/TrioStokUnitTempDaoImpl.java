package com.trio.gl.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Projections;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trio.gl.bean.TrioStokUnitTemp;
import com.trio.gl.hibernate.TrioHibernateDaoSupport;

/**
 * @author Gusti Arya 9:22:42 AM Jun 21, 2013
 */

@Service
public class TrioStokUnitTempDaoImpl extends TrioHibernateDaoSupport implements TrioStokUnitTempDao{

	public void saveOrUpdate(TrioStokUnitTemp domain, String user) {
		// TODO Auto-generated method stub

	}

	public void save(TrioStokUnitTemp domain, String user) {
		// TODO Auto-generated method stub

	}

	public void update(TrioStokUnitTemp domain, String user) {
		// TODO Auto-generated method stub

	}

	public void delete(TrioStokUnitTemp domain) {
		// TODO Auto-generated method stub

	}

	@Transactional(readOnly=true)
	public List<TrioStokUnitTemp> findAll() {
		Criteria criteria = getHibernateTemplate().getSessionFactory().getCurrentSession()
				.createCriteria(TrioStokUnitTemp.class);
		criteria.setProjection(Projections.distinct(Projections.property("kdDlr")));
		return criteria.list();
	}

	public List<TrioStokUnitTemp> findByExample(TrioStokUnitTemp domain) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<TrioStokUnitTemp> findByCriteria(TrioStokUnitTemp domain) {
		// TODO Auto-generated method stub
		return null;
	}

	public TrioStokUnitTemp findByPrimaryKey(TrioStokUnitTemp domain) {
		// TODO Auto-generated method stub
		return null;
	}


	@Transactional(readOnly=true)
	public List<TrioStokUnitTemp> findByKet() {
		String hql = "from TrioStokUnitTemp where ket= 'S Awal'";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		return q.list();
	}

	@Transactional(readOnly=true)
	public List<TrioStokUnitTemp> findResultByDate(Date tglAkhir, Date tgl,
			Date monthBefore, String formattedDate, String kdDlr) {

		String hql = null;
		boolean check =false;

		if(tglAkhir.compareTo(tgl) <= 0 ){
			check=true;
			System.out.println("tglAkhir lebih Kecil");
			hql = "SELECT new TrioStokUnitTemp(t.kdDlr, substring(t.kdItem,1,3), t.tipe, t.harga, sum(t.masuk), 0 as masuk, 0 as keluar, 0 as sAkhir)"  
					+ " FROM TrioStokUnitTemp"
					+ " WHERE ket='S Awal'" 
					+ " AND DATE_FORMAT(tgl, '%Y%m')= :formatDate"
					+ " AND kdDlr= :kdDlr" 
					+ " GROUP BY kdDlr, kdItem, tipe, harga";
		}else {
			check=false;
			System.out.println("tglAkhirLebihBesar");

			hql = "SELECT new TrioStokUnitTemp(t.kdDlr, substring(t.kdItem,1,3), t.tipe, t.harga, sum(t.masuk-IFNULL(t.keluar,0)), 0 as masuk, 0 as keluar, 0 as sAkhir)"
					+ " FROM TrioStokUnitTemp t"
					+ " WHERE t.tgl<= :monthBefore"
					+ " AND t.kdDlr= :kdDlr"
					+ " GROUP BY t.kdDlr, substring(t.kdItem,1,3), t.tipe, t.harga";
		}

		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		
		if(check == true){
			q.setString("formatDate", formattedDate);	
		}else{
			q.setDate("monthBefore", monthBefore);
		}
		
		q.setString("kdDlr", kdDlr);
		return q.list();
	}

	@Transactional(readOnly=true)
	public List<TrioStokUnitTemp> findByNoMesinNotNull(Date tglAkhir,
			String kdDlr) {

		String hql = "from TrioStokUnitTemp where tgl <= :tglAkhir and kdDlr = :kdDlr " +
				"and noMesin is not null order by tgl, keluar";
		
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		q.setParameter("tglAkhir", tglAkhir);
		q.setParameter("kdDlr", kdDlr);
		return q.list();
	}

	@Transactional(readOnly=true)
	public List<TrioStokUnitTemp> getNoMesinGroup(String noFakOut) {
		String hql = "from TrioStokUnitTemp where noFakOut = :noFakOut";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		q.setString("noFakOut", noFakOut);
		
		return (List<TrioStokUnitTemp>)q.list();
	}

	@Transactional(readOnly=true)
	@Override
	public List<TrioStokUnitTemp> findVqTranMasuk(Date tglAwal, Date tglAkhir,
			String kdDlr) {
		
		String hql = "SELECT new TrioStokUnitTemp(t.kdDlr, substring(t.kdItem,1,3), t.tipe, t.harga, 0 as sAwal," 
				+ " SUM(t.masuk - IFNULL(t.keluar,0)), 0 as keluar, 0 as sAkhir )"
				+ " FROM TrioStokUnitTemp t"
				+ " WHERE t.kdDlr= :kdDlr"
				+ " AND (t.ket='TERIMA' OR t.ket='Batal Terima')"
				+ " AND t.tgl between :tglAwal and :tglAkhir"
				+ " GROUP BY t.kdDlr, substring(t.kdItem,1,3), t.tipe, t.harga";
		
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession()
				.createQuery(hql);
		q.setString("kdDlr", kdDlr);
		q.setDate("tglAwal", tglAwal);
		q.setDate("tglAkhir", tglAkhir);
		return q.list();
	}

	@Override
	@Transactional(readOnly=true)
	public List<TrioStokUnitTemp> findVqTranKeluar(Date tglAwal, Date tglAkhir,
			String kdDlr) {
		
		String hql = "SELECT new TrioStokUnitTemp(t.kdDlr, substring(t.kdItem,1,3), t.tipe, t.harga, 0 as sAwal," 
				+ " 0 as masuk, SUM(t.keluar - IFNULL(t.masuk,0)), 0 as sAkhir )"
				+ " FROM TrioStokUnitTemp t"
				+ " WHERE t.kdDlr= :kdDlr"
				+ " AND (t.ket='Jual' OR t.ket='Batal Jual')"
				+ " AND t.tgl between :tglAwal and :tglAkhir"
				+ " GROUP BY t.kdDlr, substring(t.kdItem,1,3), t.tipe, t.harga";
		
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession()
				.createQuery(hql);
		q.setString("kdDlr", kdDlr);
		q.setDate("tglAwal", tglAwal);
		q.setDate("tglAkhir", tglAkhir);
		
		return q.list();
	}

	//dipakai di TrnCekPenjualanVM
	@Override
	@Transactional(readOnly=true)
	public BigDecimal getNoFakOutCount(String noFakOut) {
		String hql = "select count(noFakOut) from TrioStokUnitTemp where ket='Jual' "
				+ "and noFakOut= :noFakOut";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		q.setString("noFakOut", noFakOut);
		
		BigDecimal bd = BigDecimal.valueOf((Long) q.uniqueResult()); 
				
		if(bd.compareTo(BigDecimal.ZERO) == 0){
			bd = new BigDecimal(1);
		}
		
		return bd;
	}

	//digunakan di form trnCekPenjualan
	@Override
	@Transactional(readOnly=true)
	public TrioStokUnitTemp getKetCekPenjualan(String noFak) {
		String hql = "from TrioStokUnitTemp where noFakOut = :noFak and ket = 'Jual'";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		q.setString("noFak", noFak);
		return (TrioStokUnitTemp) q.uniqueResult();
	}
}
