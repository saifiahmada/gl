package com.trio.gl.dao;

import java.util.List;

import com.trio.gl.bean.TrioHeaderStnk;
import com.trio.gl.hibernate.TrioGenericDao;

/**
 * @author Gusti Arya 11:12:34 AM Jul 16, 2013
 */

public interface TrioHeaderStnkDao extends TrioGenericDao<TrioHeaderStnk> {
	
	public void saveAllCollection(List<TrioHeaderStnk> listHdrStnk, String user);

}
