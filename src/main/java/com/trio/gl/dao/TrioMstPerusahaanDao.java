package com.trio.gl.dao;

import com.trio.gl.bean.TrioMstPerusahaan;
import com.trio.gl.hibernate.TrioGenericDao;

/**
 * @author Gusti Arya 9:56:58 AM Sep 10, 2013
 */

public interface TrioMstPerusahaanDao extends TrioGenericDao<TrioMstPerusahaan>{
	
	public TrioMstPerusahaan find();
	
}
