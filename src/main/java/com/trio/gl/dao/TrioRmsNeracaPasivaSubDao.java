package com.trio.gl.dao;

import java.util.List;

import com.trio.gl.bean.TrioRmsNeracaPasivaSub;
import com.trio.gl.hibernate.TrioGenericDao;

/**
 * @author Gusti Arya 4:55:59 PM Nov 8, 2013
 */

public interface TrioRmsNeracaPasivaSubDao extends TrioGenericDao<TrioRmsNeracaPasivaSub>{

	public void saveByCollection(List<TrioRmsNeracaPasivaSub> lists, String user);
	public List<TrioRmsNeracaPasivaSub> getSumByHasil();
	
}
