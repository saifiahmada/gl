package com.trio.gl.dao;

import com.trio.gl.bean.TrioNeracaCoba;
import com.trio.gl.hibernate.TrioGenericDao;

/** @author Saifi Ahmada Jun 3, 2013 4:57:14 PM  **/

public interface TrioNeracaCobaDao extends TrioGenericDao<TrioNeracaCoba> {
	public String prosesNeraca(String periode, boolean saveNeraca, boolean prosesSub, String user);
	public TrioNeracaCoba getTrioNeracaCobaByIdPerk (String idPerk);
}

