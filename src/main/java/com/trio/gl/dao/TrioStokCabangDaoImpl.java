package com.trio.gl.dao;

import java.util.List;

import org.springframework.stereotype.Service;

import com.trio.gl.bean.TrioStokCabang;
import com.trio.gl.hibernate.TrioHibernateDaoSupport;
import com.trio.gl.util.TrioDateUtil;

/** @author Saifi Ahmada Jul 19, 2013 4:47:33 PM  **/

@Service
public class TrioStokCabangDaoImpl extends TrioHibernateDaoSupport implements TrioStokCabangDao {

	public void saveOrUpdate(TrioStokCabang domain, String user) {
		// TODO , masbro
		
		
	}

	public void save(TrioStokCabang domain, String user) {
		// TODO , masbro
		domain.getUserTrailing().setDcrea(TrioDateUtil.getLongSysDate());
		domain.getUserTrailing().setVcreaby(user);
		getHibernateTemplate().getSessionFactory().getCurrentSession().save(domain);
	}

	public void update(TrioStokCabang domain, String user) {
		// TODO , masbro
		
		
	}

	public void delete(TrioStokCabang domain) {
		// TODO , masbro
		
		
	}

	public List<TrioStokCabang> findAll() {
		// TODO , masbro
		
		return null;
	}

	public List<TrioStokCabang> findByExample(TrioStokCabang domain) {
		// TODO , masbro
		
		return null;
	}

	public List<TrioStokCabang> findByCriteria(TrioStokCabang domain) {
		// TODO , masbro
		
		return null;
	}

	public TrioStokCabang findByPrimaryKey(TrioStokCabang domain) {
		return (TrioStokCabang) getHibernateTemplate().getSessionFactory().getCurrentSession().get(TrioStokCabang.class, domain.getPartNum());
	}

}

