package com.trio.gl.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.trio.gl.bean.TrioJurnal;
import com.trio.gl.hibernate.TrioGenericDao;

/** @author Saifi Ahmada Mar 14, 2013 11:04:42 AM  **/

public interface TrioJurnalDao extends TrioGenericDao<TrioJurnal> { 
	
	public String saveTransaction(List<TrioJurnal> listJurnal, String jenis, String user);
	
	public List<TrioJurnal> getListJurnalByIdJurnal(String idJurnal);
	
	public void saveOrUpdateAll(List<TrioJurnal> listJurnal, String user);
	
	public List<TrioJurnal> getListJurnalByTanggal(Date tglJurnal);
	
	public List<TrioJurnal> getListJurnalByRangeTanggal(Date tglAwal, Date tglAkhir);
	
	public List<TrioJurnal> getListJurnalMemorial();
	
	public List<TrioJurnal> getListJurnalUmum();
	
	public List<TrioJurnal> getListJurnalForStnk(Date tglAwal, Date tglAkhir, String noPerk);
	
	public List<TrioJurnal> getListJurnalByFketPotOff(Date tglAwal, Date tglAkhir, String noPerk);
	
	public List<TrioJurnal> getListJurnalByNoReff(String noReff);
	
	public List<TrioJurnal> getNotLunasJurnal(String idPerk, String idPerk2, String idPerk3);
	
	public List<TrioJurnal> getJurnalLunas(Date tglAwal, Date tglAkhir,boolean cek,boolean cek2, String idPerkiraan);
	
	public List<TrioJurnal> getPotCekPenjualan(String idPerk, String idPerk2, String noFak);
	
	public List<TrioJurnal> getBalanceJurnal(String bulan, String tahun);
	
	public BigDecimal getNettCekPenjualan(String idPerk1, String noFak);
	
	public BigDecimal getKredit(String idJurnal, String idPerkiraan);
	
	public BigDecimal getDebit(String idJurnal, String idPerkiraan);
	
	public BigDecimal getOngkosAngkut(String idPerk,String tgl);
	
	public String savePelunasan(List<TrioJurnal> listJurnal,String jenis, String user);
	
	

}

