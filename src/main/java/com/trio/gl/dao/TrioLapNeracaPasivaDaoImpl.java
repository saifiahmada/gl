package com.trio.gl.dao;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trio.gl.bean.TrioLapNeracaPasiva;
import com.trio.gl.hibernate.TrioHibernateDaoSupport;

/**
 * @author Gusti Arya 10:03:58 AM Nov 8, 2013
 */

@Service
public class TrioLapNeracaPasivaDaoImpl extends TrioHibernateDaoSupport implements TrioLapNeracaPasivaDao {

	@Override
	public void saveOrUpdate(TrioLapNeracaPasiva domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void save(TrioLapNeracaPasiva domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(TrioLapNeracaPasiva domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(TrioLapNeracaPasiva domain) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<TrioLapNeracaPasiva> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TrioLapNeracaPasiva> findByExample(TrioLapNeracaPasiva domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TrioLapNeracaPasiva> findByCriteria(TrioLapNeracaPasiva domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TrioLapNeracaPasiva findByPrimaryKey(TrioLapNeracaPasiva domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<TrioLapNeracaPasiva> findByPeriode(String periode) {
		String hql = "from TrioLapNeracaPasiva where periode = :periode";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		q.setString("periode", periode);
		
		return q.list();
	}

	@Override
	@Transactional(readOnly=false)
	public void saveFromCollection(List<TrioLapNeracaPasiva> lists,
			String username) {
		for(TrioLapNeracaPasiva current : lists){
			getHibernateTemplate().getSessionFactory().getCurrentSession().persist(current);
		}
	}
}
