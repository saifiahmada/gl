package com.trio.gl.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.trio.gl.bean.TrioDetailStnk;
import com.trio.gl.hibernate.TrioGenericDao;

/**
 * @author Gusti Arya 11:07:38 AM Jul 16, 2013
 */

public interface TrioDetailStnkDao extends TrioGenericDao<TrioDetailStnk> {

	public List<TrioDetailStnk> findByStatusA(String status, Date tglAwal, Date tglAkhir);
	public List<TrioDetailStnk> findByStatusX(String status, Date tglAwal, Date tglAkhir);
	public List<TrioDetailStnk> findbyStatus(String status, Date tglAwal, Date tglAkhir);
	public List<TrioDetailStnk> findByStatusNotX(Date tglAwal, Date tglAkhir, boolean cb);
	public List<TrioDetailStnk> getMohonKreditBiaya(String noMesin);
	public String getNoUrus(String noMesin);
	public Date getTglUrus(String noMesin);
	public BigDecimal getPengStnk(String noMesin);
	
	
}
