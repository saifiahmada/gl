package com.trio.gl.dao;

import java.math.BigDecimal;
import java.util.List;

import com.trio.gl.bean.TrioKumpul;
import com.trio.gl.hibernate.TrioGenericDao;

/**
 * @author Gusti Arya 4:23:27 PM Sep 3, 2013
 */

public interface TrioKumpulDao extends TrioGenericDao<TrioKumpul>{
	
	public void deleteFrom();
	public BigDecimal getTotalByIdPerk(String idPerk, String idSub);
	public void saveAll (List<TrioKumpul> listKumpul, String user);

}
