package com.trio.gl.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trio.gl.bean.TrioDesignNeracaAkhirAktiva;
import com.trio.gl.hibernate.TrioHibernateDaoSupport;

/** @author Saifi Ahmada May 26, 2013 2:22:12 PM  **/

@Service
public class TrioDesignNeracaAkhirAktivaDaoImpl extends TrioHibernateDaoSupport implements TrioDesignNeracaAkhirAktivaDao {

	public void saveOrUpdate(TrioDesignNeracaAkhirAktiva domain, String user) {
		// TODO , masbro
	}

	public void save(TrioDesignNeracaAkhirAktiva domain, String user) {
		// TODO , masbro
	}

	@Transactional(readOnly=false)
	public void update(TrioDesignNeracaAkhirAktiva domain, String user) {
		getHibernateTemplate().getSessionFactory().getCurrentSession().merge(domain);
	}

	public void delete(TrioDesignNeracaAkhirAktiva domain) {
		// TODO , masbro


	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<TrioDesignNeracaAkhirAktiva> findAll() {
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery("from TrioDesignNeracaAkhirAktiva");
		return q.list();
	}

	public List<TrioDesignNeracaAkhirAktiva> findByExample(
			TrioDesignNeracaAkhirAktiva domain) {
		// TODO , masbro

		return null;
	}

	public List<TrioDesignNeracaAkhirAktiva> findByCriteria(
			TrioDesignNeracaAkhirAktiva domain) {
		// TODO , masbro

		return null;
	}

	public TrioDesignNeracaAkhirAktiva findByPrimaryKey(
			TrioDesignNeracaAkhirAktiva domain) {
		// TODO , masbro

		return null;
	}
	
	@Override
	@Transactional(readOnly=false)
	public void saveByList(List<TrioDesignNeracaAkhirAktiva> lists, String user) {
		String nativeQuery = "TRUNCATE TRIO_DESIGN_NERACA_AKHIR_AKTIVA";
		SQLQuery sql = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(nativeQuery);
		sql.executeUpdate();
		
		for(TrioDesignNeracaAkhirAktiva current : lists){
			getHibernateTemplate().getSessionFactory().getCurrentSession().save(current);
		}
	}

	@Override
	@Transactional(readOnly=false)
	public void saveOrUpdateByList(List<TrioDesignNeracaAkhirAktiva> lists,
			String user) {
		for(TrioDesignNeracaAkhirAktiva current : lists){
			getHibernateTemplate().getSessionFactory().getCurrentSession().saveOrUpdate(current);
		}
	}

}

