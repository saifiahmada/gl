package com.trio.gl.dao;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trio.gl.bean.TrioRmsNeracaPasivaSub;
import com.trio.gl.hibernate.TrioHibernateDaoSupport;

/**
 * @author Gusti Arya 4:56:35 PM Nov 8, 2013
 */

@Service
public class TrioRmsNeracaPasivaSubDaoImpl extends TrioHibernateDaoSupport implements TrioRmsNeracaPasivaSubDao{

	@Override
	public void saveOrUpdate(TrioRmsNeracaPasivaSub domain, String user) {
		// TODO Auto-generated method stub

	}

	@Override
	public void save(TrioRmsNeracaPasivaSub domain, String user) {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(TrioRmsNeracaPasivaSub domain, String user) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(TrioRmsNeracaPasivaSub domain) {
		// TODO Auto-generated method stub

	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<TrioRmsNeracaPasivaSub> findAll() {
		String hql = "from TrioRmsNeracaPasivaSub";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession()
				.createQuery(hql);
		return q.list();
	}

	@Override
	public List<TrioRmsNeracaPasivaSub> findByExample(
			TrioRmsNeracaPasivaSub domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TrioRmsNeracaPasivaSub> findByCriteria(
			TrioRmsNeracaPasivaSub domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TrioRmsNeracaPasivaSub findByPrimaryKey(TrioRmsNeracaPasivaSub domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional(readOnly=false)
	public void saveByCollection(List<TrioRmsNeracaPasivaSub> lists, String user) {
		String hql = "delete from TrioRmsNeracaPasivaSub";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		q.executeUpdate();

		for(TrioRmsNeracaPasivaSub current : lists){
			getHibernateTemplate().getSessionFactory().getCurrentSession().save(current);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<TrioRmsNeracaPasivaSub> getSumByHasil() {
		String hql = "select new TrioRmsNeracaPasivaSub(t.hasil, sum(t.nilai)) from TrioRmsNeracaPasivaSub t group by t.hasil";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		
		return q.list();
	}
}
