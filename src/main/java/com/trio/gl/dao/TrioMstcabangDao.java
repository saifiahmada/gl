package com.trio.gl.dao;

import com.trio.gl.bean.TrioMstcabang;
import com.trio.gl.hibernate.TrioGenericDao;

/** @author Saifi Ahmada Feb 15, 2013 10:40:56 AM  **/

public interface TrioMstcabangDao extends TrioGenericDao<TrioMstcabang> {

}

