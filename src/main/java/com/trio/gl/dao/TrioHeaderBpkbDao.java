package com.trio.gl.dao;

import com.trio.gl.hibernate.TrioGenericDao;

/**
 * @author Gusti Arya 1:50:16 PM Jul 17, 2013
 */

public interface TrioHeaderBpkbDao extends TrioGenericDao<TrioHeaderBpkbDao> {

}
