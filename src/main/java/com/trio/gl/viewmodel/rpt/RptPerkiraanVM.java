package com.trio.gl.viewmodel.rpt;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zkex.zul.Jasperreport;
import org.zkoss.zul.Window;

import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioMstkonfigurasi;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;

/** @author Saifi Ahmada Mar 21, 2013 5:00:03 PM  **/

public class RptPerkiraanVM extends TrioBasePageVM {
	
	private Jasperreport report;
	
	@NotifyChange("report")
	@Command("print")
	public void print(){
		
		
		System.out.println("cabang = "+getCabangSession());
		  System.out.println("print");
		  //String reportSrc = Sessions.getCurrent().getWebApp().getRealPath("/rpt/rptPerkiraan.jasper");
		 // String reportSrc = Sessions.getCurrent().getWebApp().getRealPath("/rpt/rptSubPerkiraan.jasper");
		  //System.out.println("sumber report = "+reportSrc);
		  DatabaseContextHolder.setConnectionType(ConnectionType.DEFAULT);
		  
		  TrioMstkonfigurasi konfig = getMasterFacade().getTrioMstkonfigurasiDao().findByPrimaryKey(new TrioMstkonfigurasi("111"));
		  String pathReport = konfig.getNilai();
		  System.out.println("konfig nilai "+konfig.getNilai());
		  
		  File file = new File(pathReport+"rptPerkiraan.jasper");
		  System.out.println("file 1 nilai "+file.getAbsolutePath());
		  System.out.println("file 2 ="+file); 
		  
		  Window win = (Window) Executions.createComponents("/rpt/report.zul", null, null);
		  win.setTitle("Laporan Perkiraan");
		  DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		  Jasperreport report = (Jasperreport) win.getFellow("report");
		  report.setDataConnection(getReportConnection());
		  Map<String, Object> params = new HashMap<String, Object>();
		  params.put("CABANG", getCabangSession());
		  report.setParameters(params);
		  report.setType("pdf");
		  report.setSrc(file.getAbsolutePath());
		  //report.setSrc(reportSrc);
	}
	
	@Command("reset")
	public void reset(){
		
	}
	
	

}

