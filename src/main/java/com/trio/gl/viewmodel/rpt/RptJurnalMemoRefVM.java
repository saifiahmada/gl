package com.trio.gl.viewmodel.rpt;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zkex.zul.Jasperreport;
import org.zkoss.zul.Button;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioJurnal;
import com.trio.gl.bean.TrioMstPerusahaan;
import com.trio.gl.bean.TrioMstkonfigurasi;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;

public class RptJurnalMemoRefVM extends TrioBasePageVM{

	private TrioJurnal current;
	private Textbox tb;
	private Button butt;

	public TrioJurnal getCurrent() {
		if(current==null){
			current = new TrioJurnal();
		}
		return current;
	}

	public void setCurrent(TrioJurnal current) {
		this.current = current;
	}

	public Textbox getTb() {
		return tb;
	}

	public void setTb(Textbox tb) {
		this.tb = tb;
	}

	public Button getButt() {
		return butt;
	}

	public void setButt(Button butt) {
		this.butt = butt;
	}

	@Command
	public void openPopUpJurnal(@ContextParam(ContextType.VIEW) Component view){
		Executions.createComponents("/popup/popupJurnal.zul", view, null);
	}

	@NotifyChange("current")
	@Command
	public void sendIdJurnal(@BindingParam("idJurnal") TrioJurnal id){
		current.getTrioJurnalPK().setIdJurnal(id.getTrioJurnalPK().getIdJurnal());
	}

	@Command
	public void cetak(){

		DatabaseContextHolder.setConnectionType(ConnectionType.DEFAULT);

		TrioMstkonfigurasi konfig = getMasterFacade().getTrioMstkonfigurasiDao().findByPrimaryKey(new TrioMstkonfigurasi("111"));
		String pathReport = konfig.getNilai();

		File file = new File(pathReport+"\\Jurnal_memo_ref.jasper");

		Window win = (Window) Executions.createComponents("/rpt/report.zul", null, null);
		win.setTitle("Laporan Jurnal Memorial / Referensi");
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		//		DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);
		Jasperreport report = (Jasperreport) win.getFellow("report");
		report.setDataConnection(getReportConnection());
		
		TrioMstPerusahaan trioMstPerusahaan = getMasterFacade().getTrioMstPerusahaanDao().find();
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("CABANG", getCabangSession());
		params.put("ID_JURNAL", current.getTrioJurnalPK().getIdJurnal());
		params.put("NAMA_PERS", trioMstPerusahaan.getNamaPers());
		params.put("ALAMAT_PERS", trioMstPerusahaan.getAlamatPers());
		report.setParameters(params);
		report.setType("pdf");
		report.setSrc(file.getAbsolutePath());

	}
}
