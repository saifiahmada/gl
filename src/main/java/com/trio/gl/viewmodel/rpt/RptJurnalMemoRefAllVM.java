package com.trio.gl.viewmodel.rpt;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.annotation.Command;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zkex.zul.Jasperreport;
import org.zkoss.zul.Window;

import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioJurnal;
import com.trio.gl.bean.TrioMstPerusahaan;
import com.trio.gl.bean.TrioMstkonfigurasi;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;

public class RptJurnalMemoRefAllVM extends TrioBasePageVM{

	private TrioJurnal trioJurnal;
	private TrioJurnal trioJurnal2;
	private List<TrioJurnal> trioJurnalLists;

	public TrioJurnal getTrioJurnal() {
		if(trioJurnal == null)
			trioJurnal = new TrioJurnal();
		return trioJurnal;
	}

	public void setTrioJurnal(TrioJurnal trioJurnal) {
		this.trioJurnal = trioJurnal;
	}

	public TrioJurnal getTrioJurnal2() {
		if(trioJurnal2 ==null)
			trioJurnal2 = new TrioJurnal();
		return trioJurnal2;
	}

	public void setTrioJurnal2(TrioJurnal trioJurnal2) {
		this.trioJurnal2 = trioJurnal2;
	}

	public List<TrioJurnal> getTrioJurnalLists() {
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		return getMasterFacade().getTrioJurnalDao().getListJurnalMemorial();
	}

	public void setTrioJurnalLists(List<TrioJurnal> trioJurnalLists) {
		this.trioJurnalLists = trioJurnalLists;
	}

	@Command
	public void cetak(){

		System.out.println("ini cetak");
		DatabaseContextHolder.setConnectionType(ConnectionType.DEFAULT);

		TrioMstkonfigurasi konfig = getMasterFacade().getTrioMstkonfigurasiDao().findByPrimaryKey(new TrioMstkonfigurasi("111"));
		String pathReport = konfig.getNilai();

		File file = new File(pathReport+"\\Jurnal_memo_ref_all.jasper");

		Window win = (Window) Executions.createComponents("/rpt/report.zul", null, null);
		win.setTitle("Laporan Jurnal Memorial");
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);
		Jasperreport report = (Jasperreport) win.getFellow("report");
		report.setDataConnection(getReportConnection());

		TrioMstPerusahaan trioMstPerusahaan = getMasterFacade().getTrioMstPerusahaanDao().find();
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("CABANG", getCabangSession());
		params.put("SUBREPORT_DIR", "D:\\Report\\");
		params.put("NAMA_PERS", trioMstPerusahaan.getNamaPers());
		params.put("ALAMAT_PERS", trioMstPerusahaan.getAlamatPers());
		params.put("REF_AWAL", trioJurnal.getTrioJurnalPK().getIdJurnal().substring(7, 11) );
		params.put("REF_AKHIR", trioJurnal2.getTrioJurnalPK().getIdJurnal().substring(7,11));

		report.setParameters(params);

		report.setType("pdf");
		report.setSrc(file.getAbsolutePath());

	}
}
