package com.trio.gl.viewmodel.rpt;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zkex.zul.Jasperreport;
import org.zkoss.zul.Window;

import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioDetailBpkb;
import com.trio.gl.bean.TrioDetailStnk;
import com.trio.gl.bean.TrioMstkonfigurasi;
import com.trio.gl.helper.bean.CurStnkBpkb;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;

/**
 * @author Gusti Arya 10:17:04 AM Jul 15, 2013
 */

public class RptStnkBpkbVM extends TrioBasePageVM{

	private String comboLabel;

	private String radioLabel;

	private Date tglAwal;

	private Date tglAkhir;

	public Date getTglAwal() {
		if(tglAwal == null)
			tglAwal = new Date();
		return tglAwal;
	}

	public void setTglAwal(Date tglAwal) {
		this.tglAwal = tglAwal;
	}
	public Date getTglAkhir() {
		if(tglAkhir == null)
			tglAkhir = new Date();
		return tglAkhir;
	}

	public void setTglAkhir(Date tglAkhir) {
		this.tglAkhir = tglAkhir;
	}

	public String getcomboLabel() {
		return comboLabel;
	}

	@NotifyChange
	public void setcomboLabel(String comboLabel) {
		this.comboLabel = comboLabel;
	}

	public String getRadioLabel() {
		return radioLabel;
	}

	@NotifyChange
	public void setRadioLabel(String radioLabel) {
		this.radioLabel = radioLabel;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Command
	public void cetak(){
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		List<CurStnkBpkb> curStnkBpkbList = new ArrayList<CurStnkBpkb>();

		if(radioLabel.equalsIgnoreCase("stnk")){//STNK
			if(comboLabel.equalsIgnoreCase("A")){ // STNK dengan status A
				List<TrioDetailStnk> lists = getMasterFacade().getTrioDetailStnkDao()
						.findByStatusA(comboLabel, tglAwal, tglAwal);
				for(TrioDetailStnk current : lists){
					CurStnkBpkb curStnkBpkb = new CurStnkBpkb(current.getTrioDetailStnkPK().getNoMhn(), 
							current.getTrioHeaderStnk().getTgl(), current.getTrioDetailStnkPK().getNoMesin(), 
							current.getNoRangka(), current.getKdItem(), current.getNama(), current.getBiaya(),
							current.getStatus());
					curStnkBpkbList.add(curStnkBpkb);
				}
			}
			else if (comboLabel.equalsIgnoreCase("X")){ //STNK Dengan status X
				System.out.println("comboLabel = " + comboLabel);
				List<TrioDetailStnk> lists = getMasterFacade().getTrioDetailStnkDao()
						.findByStatusX(comboLabel, tglAwal, tglAkhir);
				for(TrioDetailStnk current : lists){
					CurStnkBpkb curStnkBpkb = new CurStnkBpkb(current.getTrioDetailStnkPK().getNoMhn(), 
							current.getTrioHeaderStnk().getTgl(), current.getTrioDetailStnkPK().getNoMesin(), 
							current.getNoRangka(), current.getKdItem(), current.getNama(), current.getBiaya(),
							current.getStatus());
					curStnkBpkbList.add(curStnkBpkb);
					System.out.println("size inside X  = " + curStnkBpkbList.size());
				}
			}
			else{//STNK dengan status selain A dan X
				List<TrioDetailStnk> lists = getMasterFacade().getTrioDetailStnkDao()
						.findbyStatus(comboLabel, tglAwal, tglAkhir);
				for(TrioDetailStnk current : lists){
					CurStnkBpkb curStnkBpkb = new CurStnkBpkb(current.getTrioDetailStnkPK().getNoMhn(), 
							current.getTrioHeaderStnk().getTgl(), current.getTrioDetailStnkPK().getNoMesin(), 
							current.getNoRangka(), current.getKdItem(), current.getNama(), current.getBiaya(),
							current.getStatus());
					curStnkBpkbList.add(curStnkBpkb);
				}
			}
		}
		
		if(radioLabel.equalsIgnoreCase("bpkb")){//BPKB
			if(comboLabel.equalsIgnoreCase("A")){//BPKB dengan status A

				List<TrioDetailBpkb> lists = getMasterFacade().getTrioDetailBpkbDao()
						.findByStatusA(comboLabel, tglAwal, tglAkhir);
				for(TrioDetailBpkb current : lists){
					CurStnkBpkb curStnkBpkb = new CurStnkBpkb(current.getTrioDetailBpkbPK().getNoMhn(), 
							current.getTrioHeaderBpkb().getTgl(), current.getTrioDetailBpkbPK().getNoMesin(), 
							current.getNoRangka(), current.getKdItem(), current.getNama(), current.getBiaya(),
							current.getStatus());
					curStnkBpkbList.add(curStnkBpkb);
				}

			}else if(comboLabel.equalsIgnoreCase("X")){//BPKB Dengan status X
				List<TrioDetailBpkb> lists = getMasterFacade().getTrioDetailBpkbDao()
						.findByStatusX(comboLabel, tglAwal, tglAkhir);
				for(TrioDetailBpkb current : lists){
					CurStnkBpkb curStnkBpkb = new CurStnkBpkb(current.getTrioDetailBpkbPK().getNoMhn(), 
							current.getTrioHeaderBpkb().getTgl(), current.getTrioDetailBpkbPK().getNoMesin(), 
							current.getNoRangka(), current.getKdItem(), current.getNama(), current.getBiaya(),
							current.getStatus());
					curStnkBpkbList.add(curStnkBpkb);
				}
			}else{//BPKB Status Selain A dan X
				List<TrioDetailBpkb> lists = getMasterFacade().getTrioDetailBpkbDao()
						.findByStatus(comboLabel, tglAwal, tglAkhir);
				for(TrioDetailBpkb current : lists){
					CurStnkBpkb curStnkBpkb = new CurStnkBpkb(current.getTrioDetailBpkbPK().getNoMhn(), 
							current.getTrioHeaderBpkb().getTgl(), current.getTrioDetailBpkbPK().getNoMesin(), 
							current.getNoRangka(), current.getKdItem(), current.getNama(), current.getBiaya(),
							current.getStatus());
					curStnkBpkbList.add(curStnkBpkb);
				}
			}
		}

		//Cetak Ke Jasper
		DatabaseContextHolder.setConnectionType(ConnectionType.DEFAULT);
		TrioMstkonfigurasi konfig = getMasterFacade().getTrioMstkonfigurasiDao()
				.findByPrimaryKey(new TrioMstkonfigurasi("111"));
		String pathReport = konfig.getNilai();
		File file = new File(pathReport + "\\laporan_stnk_bpkb.jasper");

		Window win = (Window) Executions.createComponents("/rpt/report.zul", null, null);
		win.setTitle("Laporan Stock Detail SMH");
//		DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		Jasperreport report = (Jasperreport) win.getFellow("report");
		Map params = new HashMap();
		params.put("CABANG", getCabangSession());
		params.put("PERIODE_AWAL", tglAwal);
		params.put("PERIODE_AKHIR", tglAkhir);

		report.setType("pdf");
		report.setParameters(params);
		report.setSrc(file.getAbsolutePath());
		report.setDatasource(new JRBeanCollectionDataSource(curStnkBpkbList));
	}
}