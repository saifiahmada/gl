package com.trio.gl.viewmodel.rpt;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.media.AMedia;
import org.zkoss.util.media.Media;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zkex.zul.Jasperreport;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Window;

import ar.com.fdvs.dj.core.DynamicJasperHelper;
import ar.com.fdvs.dj.core.layout.ClassicLayoutManager;
import ar.com.fdvs.dj.domain.DynamicReport;
import ar.com.fdvs.dj.domain.builders.ColumnBuilderException;
import ar.com.fdvs.dj.domain.builders.FastReportBuilder;

import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioJurnal;
import com.trio.gl.bean.TrioMstkonfigurasi;
import com.trio.gl.bean.TrioMstperkiraan;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;

/**
 * @author Gusti Arya 10:11:11 AM Sep 24, 2013
 */

public class RptJurnalPelunasanVM extends TrioBasePageVM{

	private Date dateAwal;
	private Date dateAkhir;
	private boolean cek;
	private boolean cek2;
	private List<TrioMstperkiraan> idPerkiraan;
	private TrioMstperkiraan selectedPerkiraan;

	public Date getDateAwal() {
		if(dateAwal == null)
			dateAwal = new Date();
		return dateAwal;
	}
	public void setDateAwal(Date dateAwal) {
		this.dateAwal = dateAwal;
	}
	public Date getDateAkhir() {
		if(dateAkhir==null)
			dateAkhir = new Date();
		return dateAkhir;
	}
	public void setDateAkhir(Date dateAkhir) {
		this.dateAkhir = dateAkhir;
	}

	public List<TrioMstperkiraan> getIdPerkiraan() {
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		return getMasterFacade().getTrioMstperkiraanDao().getIdPerkiraanPiutang();
	}
	public void setIdPerkiraan(List<TrioMstperkiraan> idPerkiraan) {
		this.idPerkiraan = idPerkiraan;
	}
	public boolean isCek() {
		return cek;
	}
	public void setCek(boolean cek) {
		this.cek = cek;
	}
	public boolean isCek2() {
		return cek2;
	}
	public void setCek2(boolean cek2) {
		this.cek2 = cek2;
	}
	public TrioMstperkiraan getSelectedPerkiraan() {
		return selectedPerkiraan;
	}
	public void setSelectedPerkiraan(TrioMstperkiraan selectedPerkiraan) {
		this.selectedPerkiraan = selectedPerkiraan;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Command
	@NotifyChange("cek")
	public void cetak(){
		String idPerk = "";

		//logika validasi pada bagian dua buah checkbox
		if(selectedPerkiraan==null && isCek2() == false){
			idPerk = "";
		}else{
			if(selectedPerkiraan==null){
				Messagebox.show("Kolom Id Perkiraan tidak boleh kosong");
				return;
			}else{
				idPerk = selectedPerkiraan.getTrioMstperkiraanPK().getIdPerkiraan();
			}
		}

		DatabaseContextHolder.setConnectionType(ConnectionType.DEFAULT);
		TrioMstkonfigurasi konfig = getMasterFacade().getTrioMstkonfigurasiDao().findByPrimaryKey(new TrioMstkonfigurasi("111"));
		String pathReport = konfig.getNilai();

		File file = new File("D:/Arya/jasperwms/laporan_pelunasan_piutang.jasper");

		Window win = (Window) Executions.createComponents("/rpt/report.zul", null, null);
		win.setTitle("Laporan Jurnal Pelunasan");
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));

		List<TrioJurnal> listsJurnal = getMasterFacade().getTrioJurnalDao().getJurnalLunas(dateAwal, dateAkhir, cek,cek2, idPerk);
		Map params = new HashMap();
		params.put("DATE_AWAL", dateAwal);
		params.put("DATE_AKHIR", dateAkhir);
		if(isCek()){
			if(isCek2()){
				params.put("JUDUL", "LAPORAN PIUTANG BELUM LUNAS " + selectedPerkiraan.getTrioMstperkiraanPK().getIdPerkiraan());
			}else{
				params.put("JUDUL", "LAPORAN PIUTANG BELUM LUNAS ALL");
			}
		}else{
			if(isCek2()){
				params.put("JUDUL", "LAPORAN PIUTANG LUNAS " + selectedPerkiraan.getTrioMstperkiraanPK().getIdPerkiraan());
			}else{
				params.put("JUDUL", "LAPORAN PIUTANG LUNAS ALL");
			}
		}

		Jasperreport report = (Jasperreport) win.getFellow("report");
		report.setParameters(params);
		report.setType("pdf");
		report.setSrc(file.getAbsolutePath());
		report.setDatasource(new JRBeanCollectionDataSource(listsJurnal));
	}

	@Command
	public void createFile() throws ColumnBuilderException, ClassNotFoundException, JRException, FileNotFoundException, InterruptedException{
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		String idPerk = "";

		//logika validasi pada bagian dua buah checkbox
		if(selectedPerkiraan==null && isCek2() == false){
			idPerk = "";
		}else{
			if(selectedPerkiraan==null){
				Messagebox.show("Kolom Id Perkiraan tidak boleh kosong");
				return;
			}else{
				idPerk = selectedPerkiraan.getTrioMstperkiraanPK().getIdPerkiraan();
			}
		}

		List<TrioJurnal> listJurnal = getMasterFacade().getTrioJurnalDao().getJurnalLunas(dateAwal, dateAkhir, cek,cek2,idPerk);

		FastReportBuilder drb = new FastReportBuilder();
		drb.addColumn("Tgl Lunas", "tglLunas", Date.class.getName(), 70);
		drb.addColumn("Tgl Jurnal","tglJurnal", Date.class.getName(),70);
		drb.addColumn("No Jurnal", "trioJurnalPK.idJurnal",String.class.getName(), 70);
		drb.addColumn("Keterangan", "ket",String.class.getName(), 600);
		drb.addColumn("No Jurnal", "debet",BigDecimal.class.getName(), 100,true,"###,##0.00");
		drb.setPrintColumnNames(true);
		drb.setIgnorePagination(true);
		drb.setTitle("LAPORAN JURNAL PELUNASAN");
		drb.setSubtitle("dibuat pada tanggal : " + new Date());

		DynamicReport dr = drb.build();
		JasperPrint jp = DynamicJasperHelper.generateJasperPrint(dr, new ClassicLayoutManager(), listJurnal);

		File file = new File("C:/laporan_pelunasan.xls");
		FileOutputStream fos = new FileOutputStream(file);

		JRXlsExporter export = new JRXlsExporter();
		export.setParameter(JRExporterParameter.JASPER_PRINT, jp);
		export.setParameter(JRExporterParameter.OUTPUT_STREAM, fos);
		export.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
		export.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, Boolean.TRUE);
		export.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
		export.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
		export.exportReport();

		Media media = new AMedia("excel", "xls", "application/vnd.ms-excel", file,true);
		Filedownload.save(media);

	}
}
