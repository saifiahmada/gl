package com.trio.gl.viewmodel.rpt;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.ListUtils;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.zkoss.bind.annotation.Command;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zkex.zul.Jasperreport;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Window;

import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioMstkonfigurasi;
import com.trio.gl.bean.TrioStokUnitTemp;
import com.trio.gl.helper.bean.TrioStokUnitHelper;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;
import com.trio.gl.util.TrioDateConv;
import com.trio.gl.util.TrioDateUtil;

public class RptStockSmhVM extends TrioBasePageVM {

	private Date bulan;
	private BigDecimal penjualanSmh;
	private BigDecimal ongkosAngkut;
	private List<TrioStokUnitTemp> kdDlr;
	private TrioStokUnitHelper trioStokUnitHelper;
	private TrioStokUnitTemp selectedTrioStokUnitTemp;

	public TrioStokUnitHelper getTrioStokUnitHelper() {
		if(trioStokUnitHelper==null)
			trioStokUnitHelper = new TrioStokUnitHelper();
		return trioStokUnitHelper;
	}

	public void setTrioStokUnitHelper(TrioStokUnitHelper trioStokUnitHelper) {
		this.trioStokUnitHelper = trioStokUnitHelper;
	}

	public Date getBulan() {
		return bulan;
	}

	public void setBulan(Date bulan) {
		this.bulan = bulan;
	}

	public List<TrioStokUnitTemp> getKdDlr() {
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		return getMasterFacade().getTrioStokUnitTempDao().findAll();
	}

	public void setKdDlr(List<TrioStokUnitTemp> kdDlr) {
		this.kdDlr = kdDlr;
	}

	public TrioStokUnitTemp getSelectedTrioStokUnitTemp() {
		if(selectedTrioStokUnitTemp == null)
			selectedTrioStokUnitTemp = new TrioStokUnitTemp();
		return selectedTrioStokUnitTemp;
	}

	public void setSelectedTrioStokUnitTemp(
			TrioStokUnitTemp selectedTrioStokUnitTemp) {
		this.selectedTrioStokUnitTemp = selectedTrioStokUnitTemp;
	}

	@Command
	public void cetak() throws ParseException{
		newManageList();

		DatabaseContextHolder.setConnectionType(ConnectionType.DEFAULT);
		TrioMstkonfigurasi konfig = getMasterFacade().getTrioMstkonfigurasiDao().findByPrimaryKey(new TrioMstkonfigurasi("111"));
		String pathReport = konfig.getNilai();

		File file = new File(pathReport + "\\laporan_stock_smh.jasper");
		Window win = (Window) Executions.createComponents("/rpt/report.zul", null, null);
		win.setTitle("Laporan Stock SMH");
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);
		Jasperreport report = (Jasperreport) win.getFellow("report");
		report.setDataConnection(getReportConnection());
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("CABANG", getCabangSession());
		params.put("ONGKOS_ANGKUT", ongkosAngkut);
		params.put("PENJUALAN_SMH", penjualanSmh);
		params.put("PERIODE", getBulan());

		report.setParameters(params);
		report.setType("pdf");
		report.setSrc(file.getAbsolutePath());

	}

	@Command
	public void createFile() throws IOException, SQLException{
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		newManageList();

		Connection connection = getReportConnection();
		HSSFWorkbook  xlsWorkBook = new HSSFWorkbook();
		HSSFSheet xlsSheet = xlsWorkBook.createSheet();

		int rowIndex = 0;
		String sQuery = "select KD_DLR, KD_TIPE,TIPE, HARGA, SUM(S_AWAL) as qty_awal, HARGA * SUM(S_AWAL) as nilai_awal, SUM(MASUK) as qty_masuk, HARGA * SUM(MASUK) as nilai_masuk,"
				+ " SUM(KELUAR) as qty_keluar, HARGA * SUM(KELUAR) as nilai_keluar, SUM(S_AKHIR) as qty_akhir, HARGA * SUM(S_AKHIR) as nilai_Akhir from trio_stok_unit_helper"
				+ " where (S_AWAL <> 0 OR MASUK <> 0 OR KELUAR <> 0 OR S_AKHIR <> 0)"
				+ " group by KD_DLR, KD_TIPE, TIPE, HARGA";

		PreparedStatement stmt = (PreparedStatement) connection.prepareStatement(sQuery);
		ResultSet rs = stmt.executeQuery();

		ResultSetMetaData colInfo = (ResultSetMetaData) rs.getMetaData();
		List<String> colNames = new ArrayList<String>();
		HSSFRow titleRow = xlsSheet.createRow(rowIndex++);

		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			xlsSheet.setColumnWidth((int) (i-1), (int) 4000);
		}

		// Save all the data from the database table rows
		while (rs.next()) {
			HSSFRow dataRow = xlsSheet.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				dataRow.createCell(colIndex++).setCellValue(
						new HSSFRichTextString(rs.getString(colName)));
			}
		}

		FileOutputStream fOut = new FileOutputStream("stockSmh.xls");
		xlsWorkBook.write(fOut);
		fOut.flush();
		fOut.close();
		connection.close();

		File file = new File("stockSmh.xls");
		Filedownload.save(file, "XLS");

		System.out.println("done");

	}

	@SuppressWarnings("unchecked")
	private void newManageList(){
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		getMasterFacade().getTrioStokUnitHelperDao().delete(getTrioStokUnitHelper());

		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
		String formattedDate =  sdf.format(getBulan());
		String bul = formattedDate.substring(5);

		String noPerk1 = "";
		String noPerk2 = "";
		if(selectedTrioStokUnitTemp.getKdDlr().equals("T13")){
			noPerk1 = "41101";
			noPerk2 = "51104";
			penjualanSmh = getMasterFacade().getTrioMstperkiraanDao()
					.getPenjualanSmh("bul"+bul, noPerk1);
			ongkosAngkut = getMasterFacade().getTrioJurnalDao()
					.getOngkosAngkut(noPerk2, formattedDate);

			if(penjualanSmh == null)
				penjualanSmh =  penjualanSmh.ZERO;
			if(ongkosAngkut == null)
				ongkosAngkut = ongkosAngkut.ZERO;

		}else{
			noPerk1 = "41106";
			noPerk2 = "51110";

			penjualanSmh = getMasterFacade().getTrioMstperkiraanDao()
					.getPenjualanSmh("bul"+bul, noPerk1);
			ongkosAngkut = getMasterFacade().getTrioJurnalDao()
					.getOngkosAngkut(noPerk2, formattedDate);

			if(penjualanSmh == null)
				penjualanSmh =  penjualanSmh.ZERO;
			if(ongkosAngkut == null)
				ongkosAngkut = ongkosAngkut.ZERO;
		}

		//mengambil bulan, misal juli = 07
		String periode = formattedDate.substring(4);

		BigDecimal vqCariJual = getMasterFacade().getTrioLapBukubesarDao()
				.getDebetMinKreditByPeriodeAndNoPerk(periode, noPerk1, 1);
		if(vqCariJual == null)
			vqCariJual = vqCariJual.ZERO;

		BigDecimal vOngkosAngkut = getMasterFacade().getTrioLapBukubesarDao()
				.getDebetMinKreditByPeriodeAndNoPerk(periode, noPerk2, 2);
		if(vOngkosAngkut == null)
			vOngkosAngkut = BigDecimal.ZERO;

		//tglAwal dibulan yang dipilih
		Date tglAwal = TrioDateConv.valueOf(TrioDateUtil.getYear(getBulan()), 
				TrioDateUtil.getMonth(getBulan()), 1);

		//tgl terakhir di bulan yang dipiih
		Date tglAkhir = TrioDateConv.valueOf(TrioDateUtil.getYear(getBulan()), 
				TrioDateUtil.getMonth(getBulan()), TrioDateUtil.lastDayOf(getBulan()));

		//		Date bulanAkhir = bulan sebelum bulan yang dipilih
		Date bulAkhir = TrioDateConv.valueOf(TrioDateUtil.getYear(getBulan()), 
				TrioDateUtil.getMonth(getBulan()) -1, TrioDateUtil.lastDayOf(getBulan()));

		Date dateFromList = new Date();
		List<TrioStokUnitTemp> vqsAwal = new ArrayList<TrioStokUnitTemp>();

		List<TrioStokUnitTemp> listStokTemp = getMasterFacade().getTrioStokUnitTempDao().findByKet();
		for(TrioStokUnitTemp current : listStokTemp){
			dateFromList = current.getTgl();

			// vqsAwal
			vqsAwal = getMasterFacade().getTrioStokUnitTempDao()
					.findResultByDate(tglAkhir, dateFromList, bulAkhir, formattedDate,selectedTrioStokUnitTemp.getKdDlr());
		}


		//vqtran_masuk
		List<TrioStokUnitTemp> vqTranMasuk = getMasterFacade().getTrioStokUnitTempDao()
				.findVqTranMasuk(tglAwal, tglAkhir, selectedTrioStokUnitTemp.getKdDlr());

		//vqTranKeluar
		List<TrioStokUnitTemp> vqTranKeluar = getMasterFacade().getTrioStokUnitTempDao()
				.findVqTranKeluar(tglAwal, tglAkhir, selectedTrioStokUnitTemp.getKdDlr());

		System.out.println("vqTranKeluar = " + vqTranKeluar.size());

		//List gabungan antara vqTranMasuk dengan vqTranKeluar
		List<TrioStokUnitTemp> vqTranGab = ListUtils.union(vqTranMasuk, vqTranKeluar);
		List<TrioStokUnitTemp> vqGab1 = ListUtils.union(vqsAwal, vqTranGab);

		System.out.println("size vqsawal = " + vqsAwal.size());
		System.out.println("size vqTranMasuk = " + vqTranMasuk.size());
		System.out.println("vqTranKeluar = " + vqTranKeluar.size());
		System.out.println("vqTranGab = " + vqTranGab.size());
		System.out.println("vqGab1 = " + vqGab1.size());

		TrioStokUnitHelper trioStokUnitHelper = new TrioStokUnitHelper();
		for(TrioStokUnitTemp curr : vqGab1){
			trioStokUnitHelper.setKdDlr(curr.getKdDlr());
			trioStokUnitHelper.setKdTipe(curr.getKdItem());
			trioStokUnitHelper.setTipe(curr.getTipe());
			trioStokUnitHelper.setHarga(curr.getHarga());
			trioStokUnitHelper.setsAwal(curr.getsAwal());
			trioStokUnitHelper.setMasuk(curr.getMasuk());
			trioStokUnitHelper.setKeluar(curr.getKeluar());
			trioStokUnitHelper.setsAkhir(curr.getsAkhir());
			getMasterFacade().getTrioStokUnitHelperDao().save(trioStokUnitHelper, getUserSession());
		} 
	}
}
