package com.trio.gl.viewmodel.rpt;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zkex.zul.Jasperreport;
import org.zkoss.zul.Window;

import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioJurnal;
import com.trio.gl.bean.TrioMstkonfigurasi;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;

public class RptJurnalUmumRefVM extends TrioBasePageVM {

	private Date tglAwal;
	private Date tglAkhir;
	private boolean cb;
	private File file;
	private List<TrioJurnal> trioJurnalLists;
	private TrioJurnal trioJurnalAwal;
	private TrioJurnal trioJurnalAkhir;

	public List<TrioJurnal> getTrioJurnalLists() {
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		return getMasterFacade().getTrioJurnalDao().getListJurnalUmum();
	}
	public void setTrioJurnalLists(List<TrioJurnal> trioJurnalLists) {
		this.trioJurnalLists = trioJurnalLists;
	}

	public TrioJurnal getTrioJurnalAwal() {
		if(trioJurnalAwal==null)
			trioJurnalAwal = new TrioJurnal();
		return trioJurnalAwal;
	}

	public void setTrioJurnalAwal(TrioJurnal trioJurnalAwal) {
		this.trioJurnalAwal = trioJurnalAwal;
	}

	public TrioJurnal getTrioJurnalAkhir() {
		if(trioJurnalAkhir==null)
			trioJurnalAkhir = new TrioJurnal();
		return trioJurnalAkhir;
	}

	public void setTrioJurnalAkhir(TrioJurnal trioJurnalAkhir) {
		this.trioJurnalAkhir = trioJurnalAkhir;
	}

	public Date getTglAwal() {
		if(tglAwal==null)
			tglAwal = new Date();
		return tglAwal;
	}
	public void setTglAwal(Date tglAwal) {
		this.tglAwal = tglAwal;
	}
	public Date getTglAkhir() {
		if(tglAkhir==null)
			tglAkhir = new Date();
		return tglAkhir;
	}
	public void setTglAkhir(Date tglAkhir) {
		this.tglAkhir = tglAkhir;
	}
	public boolean isCb() {
		return cb;
	}
	public void setCb(boolean cb) {
		this.cb = cb;
	}

	@NotifyChange("cb")
	@Command
	public void cetak(){

		System.out.println("ini cetak");
		System.out.println("ini cb =  "+cb);

		DatabaseContextHolder.setConnectionType(ConnectionType.DEFAULT);

		TrioMstkonfigurasi konfig = getMasterFacade().getTrioMstkonfigurasiDao().findByPrimaryKey(new TrioMstkonfigurasi("111"));
		String pathReport = konfig.getNilai();

		if(cb==true){
			file = new File(pathReport+"\\Jurnal_umum_ref_ket.jasper");
		}else{
			file = new File(pathReport+"\\Jurnal_umum_ref.jasper");
		}

		Window win = (Window) Executions.createComponents("/rpt/report.zul", null, null);
		win.setTitle("Laporan Jurnal Umum");
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		//		DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);
		Jasperreport report = (Jasperreport) win.getFellow("report");
		report.setDataConnection(getReportConnection());

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("CABANG", getCabangSession());
		params.put("TGL_AWAL", tglAwal);
		params.put("TGL_AKHIR", tglAkhir);
		params.put("REF_AWAL", getTrioJurnalAwal().getTrioJurnalPK().getIdJurnal().substring(7, 11).trim());
		params.put("REF_AKHIR", getTrioJurnalAkhir().getTrioJurnalPK().getIdJurnal().substring(7,11).trim());
		
		System.out.println("refAwal = " + getTrioJurnalAwal().getTrioJurnalPK().getIdJurnal().substring(7, 11));
		System.out.println("refAwal = " + getTrioJurnalAkhir().getTrioJurnalPK().getIdJurnal().substring(7, 11));
		
		report.setParameters(params);
		report.setType("pdf");
		report.setSrc(file.getAbsolutePath());

	}
}
