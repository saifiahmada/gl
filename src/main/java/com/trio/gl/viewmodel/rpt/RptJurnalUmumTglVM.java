package com.trio.gl.viewmodel.rpt;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zkex.zul.Jasperreport;
import org.zkoss.zul.Window;

import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioMstkonfigurasi;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;

public class RptJurnalUmumTglVM extends TrioBasePageVM {

	private Date tglAwal;
	private Date tglAkhir;
	private boolean cb;
	private File file;

	public Date getTglAwal() {
		if(tglAwal==null)
			tglAwal = new Date();
		return tglAwal;
	}
	public void setTglAwal(Date tglAwal) {
		this.tglAwal = tglAwal;
	}
	public Date getTglAkhir() {
		if(tglAkhir==null)
			tglAkhir = new Date();
		return tglAkhir;
	}
	public void setTglAkhir(Date tglAkhir) {
		this.tglAkhir = tglAkhir;
	}

	public boolean isCb() {
		return cb;
	}
	public void setCb(boolean cb) {
		this.cb = cb;
	}

	@NotifyChange({"cb", "report"})
	@Command
	public void cetak(){
		System.out.println("ini cetak");
		System.out.println("ini cb =  "+cb);

		DatabaseContextHolder.setConnectionType(ConnectionType.DEFAULT);
		TrioMstkonfigurasi konfig = getMasterFacade().getTrioMstkonfigurasiDao().findByPrimaryKey(new TrioMstkonfigurasi("111"));
		String pathReport = konfig.getNilai();

		if(cb==true){
			file = new File(pathReport+"\\Jurnal_umum_har_ket.jasper");
		}else{
			file = new File(pathReport+"\\Jurnal_umum_har.jasper");
		}


		Window win = (Window) Executions.createComponents("/rpt/report.zul", null, null);
		win.setTitle("Laporan Jurnal Umum");
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		//		DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);
		Jasperreport report = (Jasperreport) win.getFellow("report");
		report.setDataConnection(getReportConnection());

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("CABANG", getCabangSession());

		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		String beginDate = sdf.format(tglAwal);
		System.out.println("begin date  = " + beginDate);
		params.put("TGL_AWAL", beginDate);

		String endDate = sdf.format(tglAkhir);
		System.out.println("end date" + endDate );
		params.put("TGL_AKHIR", endDate);
		report.setParameters(params);

		report.setType("pdf");
		report.setSrc(file.getAbsolutePath());
	}

}

