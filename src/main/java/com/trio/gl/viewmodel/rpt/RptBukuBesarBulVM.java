package com.trio.gl.viewmodel.rpt;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zkex.zul.Jasperreport;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Window;

import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.ResultSetMetaData;
import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioMstPerusahaan;
import com.trio.gl.bean.TrioMstkonfigurasi;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;

public class RptBukuBesarBulVM extends TrioBasePageVM{

	private Date bulan;
	private Jasperreport report;
	private File file;

	public Date getBulan() {
		if(bulan==null || bulan.equals(null)){
			bulan=new Date();
		}
		return bulan;
	}

	public void setBulan(Date bulan) {
		this.bulan = bulan;
	}

	@NotifyChange({"report"})
	@Command
	public void cetak(){
		System.out.println("ini cetak");
		System.out.println("get bulan = " + getBulan());

		DatabaseContextHolder.setConnectionType(ConnectionType.DEFAULT);

		TrioMstkonfigurasi konfig = getMasterFacade().getTrioMstkonfigurasiDao().findByPrimaryKey(new TrioMstkonfigurasi("111"));
		String pathReport = konfig.getNilai();

		file = new File(pathReport+"\\Buku_besar_bul_lama.jasper");	

		Window win = (Window) Executions.createComponents("/rpt/report.zul", null, null);
		win.setTitle("Laporan Jurnal Umum");
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		//		DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);
		Jasperreport report = (Jasperreport) win.getFellow("report");
		report.setDataConnection(getReportConnection());
		Map<String, Object> params = new HashMap<String, Object>();
		
		TrioMstPerusahaan trioMstPerusahaan = getMasterFacade().getTrioMstPerusahaanDao().find();
		
		params.put("CABANG", getCabangSession());
		params.put("BULAN", getBulan());
		params.put("NAMA_PERS", trioMstPerusahaan.getNamaPers());
		params.put("ALAMAT_PERS", trioMstPerusahaan.getAlamatPers());
		params.put("TELP_1", trioMstPerusahaan.getTelp1());
		params.put("TELP_2", trioMstPerusahaan.getTelp2());
		params.put("FAX", trioMstPerusahaan.getFax());

		SimpleDateFormat sdf = new SimpleDateFormat("MM");
		String month = sdf.format(bulan);
		System.out.println( "monnth = " + month);

		params.put("PERIODE", month);
		report.setParameters(params);
		report.setType("pdf");
		report.setSrc(file.getAbsolutePath());
	}

	@Command
	public void createFile() throws SQLException, IOException{
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		SimpleDateFormat sdf = new SimpleDateFormat("MM");
		String month = sdf.format(bulan);
		
		Connection connection = getReportConnection();
		HSSFWorkbook  xlsWorkBook = new HSSFWorkbook();
		HSSFSheet xlsSheet = xlsWorkBook.createSheet();

		int rowIndex = 0;
		String sQuery = "select a.TANGGAL, a.ID_JURNAL, a.ID_PERKIRAAN, a.ID_SUB, a.KET,"
				+ " a.SALDO_AWAL, a.KET_AWAL, a.DEBET, a.KREDIT from trio_bukubesar a," 
				+ " trio_mstperkiraan b"
				+ " where a.ID_PERKIRAAN = b.ID_PERKIRAAN"
				+ " and DATE_FORMAT(a.TANGGAL, '%m') = " + month
				+ " and YEAR(CURDATE())"
				+ " order by a.ID_PERKIRAAN, a.TANGGAL, a.ID_JURNAL asc ";
				
		PreparedStatement stmt = (PreparedStatement) connection.prepareStatement(sQuery);
		ResultSet rs = stmt.executeQuery();

		ResultSetMetaData colInfo = (ResultSetMetaData) rs.getMetaData();
		List<String> colNames = new ArrayList<String>();
		HSSFRow titleRow = xlsSheet.createRow(rowIndex++);


		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			xlsSheet.setColumnWidth((int) (i-1), (int) 4000);
		}

		// Save all the data from the database table rows
		while (rs.next()) {
			HSSFRow dataRow = xlsSheet.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				dataRow.createCell(colIndex++).setCellValue(
						new HSSFRichTextString(rs.getString(colName)));
			}
		}

		FileOutputStream fOut = new FileOutputStream("bukuBesar.xls");
		xlsWorkBook.write(fOut);
		fOut.flush();
		fOut.close();
		connection.close();

		File file = new File("bukuBesar.xls");
		Filedownload.save(file, "XLS");

		System.out.println("done");

	}
	
	@Command
	public void createTahunan() throws SQLException, IOException{
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		
		Connection connection = getReportConnection();
		HSSFWorkbook  xlsWorkBook = new HSSFWorkbook();
		HSSFSheet xlsSheet = xlsWorkBook.createSheet();

		int rowIndex = 0;
		String sQuery = "select a.TANGGAL, a.ID_JURNAL, a.ID_PERKIRAAN, a.ID_SUB, a.KET,"
				+ " a.SALDO_AWAL, a.KET_AWAL, a.DEBET, a.KREDIT from trio_bukubesar a," 
				+ " trio_mstperkiraan b"
				+ " where a.ID_PERKIRAAN = b.ID_PERKIRAAN"
				+ " and YEAR(CURDATE())"
				+ " order by a.ID_PERKIRAAN, a.TANGGAL, a.ID_JURNAL asc ";
				
		PreparedStatement stmt = (PreparedStatement) connection.prepareStatement(sQuery);
		ResultSet rs = stmt.executeQuery();

		ResultSetMetaData colInfo = (ResultSetMetaData) rs.getMetaData();
		List<String> colNames = new ArrayList<String>();
		HSSFRow titleRow = xlsSheet.createRow(rowIndex++);


		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			xlsSheet.setColumnWidth((int) (i-1), (int) 4000);
		}

		// Save all the data from the database table rows
		while (rs.next()) {
			HSSFRow dataRow = xlsSheet.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				dataRow.createCell(colIndex++).setCellValue(
						new HSSFRichTextString(rs.getString(colName)));
			}
		}

		FileOutputStream fOut = new FileOutputStream("bukuBesarTahunan.xls");
		xlsWorkBook.write(fOut);
		fOut.flush();
		fOut.close();
		connection.close();

		File file = new File("bukuBesarTahunan.xls");
		Filedownload.save(file, "XLS");

		System.out.println("done");
		
	}
}
