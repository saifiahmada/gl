package com.trio.gl.viewmodel.rpt;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.zkoss.bind.annotation.Command;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zkex.zul.Jasperreport;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Window;

import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.ResultSetMetaData;
import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioMstkonfigurasi;
import com.trio.gl.bean.TrioStokUnitTemp;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;

/**
 * @author Gusti Arya 9:21:30 AM Jul 10, 2013
 */

public class RptStockPenjualanSmhVM extends TrioBasePageVM{

	private List<TrioStokUnitTemp> kdDlr;
	private TrioStokUnitTemp selectedTrioStokUnitTemp;
	private Date bulan;


	public List<TrioStokUnitTemp> getKdDlr() {
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		//		DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);
		return getMasterFacade().getTrioStokUnitTempDao().findAll();
	}
	public void setKdDlr(List<TrioStokUnitTemp> kdDlr) {
		this.kdDlr = kdDlr;
	}

	public TrioStokUnitTemp getSelectedTrioStokUnitTemp() {
		if (selectedTrioStokUnitTemp == null) {
			selectedTrioStokUnitTemp = new TrioStokUnitTemp();
		}
		return selectedTrioStokUnitTemp;
	}

	public void setSelectedTrioStokUnitTemp(
			TrioStokUnitTemp selectedTrioStokUnitTemp) {
		this.selectedTrioStokUnitTemp = selectedTrioStokUnitTemp;
	}
	public Date getBulan() {
		return bulan;
	}
	public void setBulan(Date bulan) {
		this.bulan = bulan;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Command
	public void cetak(){
		SimpleDateFormat sdf = new SimpleDateFormat("MMyyyy");
		String sdfDate = sdf.format(getBulan());
		System.out.println(sdfDate);

		DatabaseContextHolder.setConnectionType(ConnectionType.DEFAULT);
		TrioMstkonfigurasi config = getMasterFacade().getTrioMstkonfigurasiDao().findById("111");
		String reportPath = config.getNilai();
		File file = new File(reportPath + "\\laporan_stock_penj_smh.jasper");

		Window win = (Window) Executions.createComponents("/rpt/report.zul", null, null);
		win.setTitle("Laporan Stock Penjualan SMH");
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		//		DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);
		Jasperreport report = (Jasperreport) win.getFellow("report");
		report.setDataConnection(getReportConnection());

		Map params = new HashMap();
		params.put("PERIODE", sdfDate);
		sdf = new SimpleDateFormat("MM-yyyy");
		String dateStr = sdf.format(getBulan());
		params.put("PERIODE_STR", dateStr);
		params.put("KD_DLR", selectedTrioStokUnitTemp.getKdDlr());

		report.setParameters(params);
		report.setType("pdf");
		report.setSrc(file.getAbsolutePath());
	}


	@Command
	public void createFile() throws Exception{
		SimpleDateFormat sdf = new SimpleDateFormat("MMyyyy");
		String sdfDate = sdf.format(getBulan());

		String fileName = "Laporan Penjualan SMH " ;
		generateXls(selectedTrioStokUnitTemp.getKdDlr(), sdfDate, fileName);
	}

	public void generateXls(String kdDlr, String sdf, String fileName) throws Exception{

		Connection connection = getReportConnection();

		HSSFWorkbook xlsWorkBook = new HSSFWorkbook();
		HSSFSheet xlsSheet = xlsWorkBook.createSheet();

		int rowIndex = 0;

		String sql = "select KD_DLR, KET, TGL, TIPE, NO_MESIN, NO_RANGKA, " 
				+ "KD_ITEM, NO_FAK_OUT, NO_SJ_OUT, NM_CUST, HARGA_J, " 
				+ "DISCOUNT, S_AHM, S_MD, S_SD, S_FINANCE, S_BUNGA " 
				+ "from trio_stokunit_temp "
				+ "where "
				+ "NO_MESIN is not  null "
				+ "and DATE_FORMAT(TGL, '%m%Y') =   ? "
				+ "and KD_DLR = ? "
				+ "and ket in ('Jual', 'Batal Jual') "
				+ "order by tgl, NO_FAK_OUT";

		PreparedStatement stmt = (PreparedStatement) connection.prepareStatement(sql);
		stmt.setString(1, sdf);
		stmt.setString(2, kdDlr);
		ResultSet rs = stmt.executeQuery();

		ResultSetMetaData colInfo = (ResultSetMetaData) rs.getMetaData();
		List<String> colNames = new ArrayList<String>();
		HSSFRow titleRow = xlsSheet.createRow(rowIndex++);

		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			xlsSheet.setColumnWidth((int) (i-1), (int) 4000);
		}

		// Save all the data from the database table rows
		while (rs.next()) {
			HSSFRow dataRow = xlsSheet.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				dataRow.createCell(colIndex++).setCellValue(
						new HSSFRichTextString(rs.getString(colName)));
			}
		}

		FileOutputStream fOut = new FileOutputStream(fileName);
		xlsWorkBook.write(fOut);
		fOut.flush();
		fOut.close();
		connection.close();

		File file = new File(fileName);
		Filedownload.save(file, "XLS");
	}
}
