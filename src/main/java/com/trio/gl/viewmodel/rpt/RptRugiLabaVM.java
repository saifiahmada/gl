package com.trio.gl.viewmodel.rpt;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.zkoss.bind.annotation.Command;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zkex.zul.Jasperreport;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Window;

import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.ResultSetMetaData;
import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioMstPerusahaan;
import com.trio.gl.bean.TrioMstkonfigurasi;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;

public class RptRugiLabaVM extends TrioBasePageVM {

	private Date bulan;
	private boolean cb;
	String periode;


	public Date getBulan() {
		return bulan;
	}
	public void setBulan(Date bulan) {
		this.bulan = bulan;
	}
	public boolean isCb() {
		return cb;
	}
	public void setCb(boolean cb) {
		this.cb = cb;
	}

	
	@Command
	public void cetak(){
		System.out.println("ini cetak");

		DatabaseContextHolder.setConnectionType(ConnectionType.DEFAULT);

		TrioMstkonfigurasi konfig = getMasterFacade().getTrioMstkonfigurasiDao().findByPrimaryKey(new TrioMstkonfigurasi("111"));
		String pathReport = konfig.getNilai();

		File file = new File(pathReport+"\\laporan_rugi_laba.jasper");

		Window win = (Window) Executions.createComponents("/rpt/report.zul", null, null);
		win.setTitle("Laporan Buku Besar Per Tanggal");
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		//		DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);
		Jasperreport report = (Jasperreport) win.getFellow("report");
		report.setDataConnection(getReportConnection());

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("CABANG", getCabangSession());
		
		TrioMstPerusahaan trioMstPerusahaan = getMasterFacade().getTrioMstPerusahaanDao().find();
		
		SimpleDateFormat sdf = new SimpleDateFormat("MM");
		periode = sdf.format(bulan);
		System.out.println("begin date  = " + periode);
		params.put("PERIODE", periode);
		params.put("NAMA_PERS", trioMstPerusahaan.getNamaPers());
		params.put("ALAMAT_PERS", trioMstPerusahaan.getAlamatPers());
		
		report.setParameters(params);

		report.setType("pdf");
		report.setSrc(file.getAbsolutePath());
	}
	
	@Command
	public void createFile() throws FileNotFoundException, IOException, SQLException {
		
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		SimpleDateFormat sdf = new SimpleDateFormat("MMM-yyyy");
		String strBulan = sdf.format(getBulan());
		
		SimpleDateFormat sdf2 = new SimpleDateFormat("MM");
		periode = sdf2.format(bulan);
		
		generateXls("laprl "+ getCabangSession() + " " +strBulan);
		System.out.println(periode);
	}
	
	public void generateXls(String fileName) throws FileNotFoundException, IOException, SQLException{
		
		Connection connection = getReportConnection();
		
		HSSFWorkbook  xlsWorkBook = new HSSFWorkbook();
		HSSFSheet xlsSheet = xlsWorkBook.createSheet();
		
		int rowIndex = 0;
		
		String sQuery = "select A.NAMA_PERKIRAAN, a.LEVEL_1, a.LEVEL_2, a.LEVEL_3, a.LEVEL_4, a.KET2 from trio_lap_rl a where periode ="+periode;
		PreparedStatement stmt = (PreparedStatement) connection.prepareStatement(sQuery);
		ResultSet rs = stmt.executeQuery();
		
		ResultSetMetaData colInfo = (ResultSetMetaData) rs.getMetaData();
		List<String> colNames = new ArrayList<String>();
		HSSFRow titleRow = xlsSheet.createRow(rowIndex++);
		
		
	    for (int i = 1; i <= colInfo.getColumnCount(); i++) {
	        colNames.add(colInfo.getColumnName(i));
	        titleRow.createCell((int) (i-1)).setCellValue(
	          new HSSFRichTextString(colInfo.getColumnName(i)));
	        xlsSheet.setColumnWidth((int) (i-1), (int) 4000);
	      }
	   
	      // Save all the data from the database table rows
	      while (rs.next()) {
	        HSSFRow dataRow = xlsSheet.createRow(rowIndex++);
	        int colIndex = 0;
	        for (String colName : colNames) {
	          dataRow.createCell(colIndex++).setCellValue(
	            new HSSFRichTextString(rs.getString(colName)));
	        }
	      }
		
		FileOutputStream fOut = new FileOutputStream(fileName);
		xlsWorkBook.write(fOut);
		fOut.flush();
		fOut.close();
		connection.close();
		
		File file = new File(fileName);
		Filedownload.save(file, "XLS");
		
		System.out.println("done");
	}
}
