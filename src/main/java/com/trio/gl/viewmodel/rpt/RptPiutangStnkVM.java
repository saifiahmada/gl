package com.trio.gl.viewmodel.rpt;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.media.AMedia;
import org.zkoss.util.media.Media;
import org.zkoss.zul.Filedownload;

import ar.com.fdvs.dj.core.DynamicJasperHelper;
import ar.com.fdvs.dj.core.layout.ClassicLayoutManager;
import ar.com.fdvs.dj.domain.DynamicReport;
import ar.com.fdvs.dj.domain.builders.ColumnBuilderException;
import ar.com.fdvs.dj.domain.builders.FastReportBuilder;

import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioDetailStnk;
import com.trio.gl.helper.bean.CurTmpLap2;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;

/**
 * @author Gusti Arya 2:42:09 PM July 26, 2013
 */

public class RptPiutangStnkVM extends TrioBasePageVM{
	
	private Date tglAwal;
	private Date tglAkhir;
	private boolean cb;
	
	public boolean isCb() {
		return cb;
	}
	public void setCb(boolean cb) {
		this.cb = cb;
	}
	public Date getTglAwal() {
		if(tglAwal == null)
			tglAwal = new Date();
		return tglAwal;
		
	}
	public void setTglAwal(Date tglAwal) {
		this.tglAwal = tglAwal;
	}
	public Date getTglAkhir() {
		if(tglAkhir == null)
			tglAkhir = new Date();
		return tglAkhir;
	}
	public void setTglAkhir(Date tglAkhir) {
		this.tglAkhir = tglAkhir;
	}

	@Command
	public void createFile() throws ColumnBuilderException, ClassNotFoundException, JRException, FileNotFoundException{
		
		List<CurTmpLap2> curLists = new ArrayList<CurTmpLap2>();
		
		BigDecimal totPinj;
		BigDecimal totReal;
		BigDecimal selisih;
		BigDecimal saldo;
		
		DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);
		List<TrioDetailStnk> lists = getMasterFacade().getTrioDetailStnkDao().findByStatusNotX(tglAwal, tglAkhir, cb);
		
		for (TrioDetailStnk currentRow : lists) {
			
			totPinj = currentRow.getBbnkb().add(currentRow.getPkb()).add(currentRow.getSwdkllj());
			totReal = currentRow.getBbnkbR().add(currentRow.getPkbR()).add(currentRow.getLain());
			selisih = totPinj.subtract(totReal);
			
			if(currentRow.getTglKrdt() == null){
				saldo = totPinj.subtract(totReal);
			}else{
				saldo =  new BigDecimal(0);
			}
			
			CurTmpLap2 curTmpLap2 = new CurTmpLap2(currentRow.getTglPinj(), currentRow.getTglKrdt(), 
					currentRow.getTglFak(), currentRow.getTrioDetailStnkPK().getNoMhn(), 
					currentRow.getNoFak(), currentRow.getTrioDetailStnkPK().getNoMesin(), 
					currentRow.getNama(), currentRow.getBbnkb(), currentRow.getBbnkbR(), 
					currentRow.getPkb(), currentRow.getPkbR(), currentRow.getSwdkllj(), 
					currentRow.getSwdklljR(), currentRow.getLain(), currentRow.getLain(),
					totPinj, totReal, selisih, saldo);
			curLists.add(curTmpLap2);
		}
		
		
		FastReportBuilder drb = new FastReportBuilder();
		drb.addColumn("Tgl. Pinjam", "tglPinj", Date.class.getName(), 70);
		drb.addColumn("No. Urus", "noUrus", String.class.getName(), 80);
		drb.addColumn("Tgl. Faktur", "tglFak", Date.class.getName(), 70);
		drb.addColumn("No. Faktur", "noFak", String.class.getName(), 110);
		drb.addColumn("Nama Konsumen", "nama", String.class.getName(), 200);
		drb.addColumn("BBNKB", "bbnkb", BigDecimal.class.getName(), 90,true, "##,##0.00");
		drb.addColumn("PKB", "pkb", BigDecimal.class.getName(),90, true, "##,##0.00");
		drb.addColumn("SWDKLLJ", "swdkllj", BigDecimal.class.getName(), 90,true, "##,##0.00");
		drb.addColumn("LAIN", "lain", BigDecimal.class.getName(), 90,true, "##,##0.00");
		drb.addColumn("Total Pinjaman", "totPinj", BigDecimal.class.getName(),90,true, "##,##0.00");
		drb.addColumn("BBKNB_R", "bbnkbR", BigDecimal.class.getName(), 90,true, "##,##0.00");
		drb.addColumn("PKB_R", "pkbR", BigDecimal.class.getName(), 90,true, "##,##0.00");
		drb.addColumn("SWDKLLJ_R", "swdklljR", BigDecimal.class.getName(), 90,true, "##,##0.00");
		drb.addColumn("LAIN_R", "lainR", BigDecimal.class.getName(), 90,true, "##,##0.00");
		drb.addColumn("TOTAL_REAL", "totReal", BigDecimal.class.getName(),90,true, "##,##0.00");
		drb.addColumn("SELISIH", "selisih", BigDecimal.class.getName(),90,true, "##,##0.00");
		drb.addColumn("Tgl. Kredit", "tglKredit", Date.class.getName(),70);
		drb.addColumn("SALDO", "saldo", BigDecimal.class.getName(),90,true, "##,##0.00");
		drb.setPrintColumnNames(true);
		drb.setIgnorePagination(true);
		drb.setTitle("LAPORAN HUTANG STNK");
		drb.setSubtitle("Dibuat pada tanggal : " + new Date());
		
		DynamicReport dr = drb.build();
		JasperPrint jp = DynamicJasperHelper.generateJasperPrint(dr, new ClassicLayoutManager(), curLists);
		
		File file = new File("D:/Arya/piutang.xls");
		FileOutputStream fos = new FileOutputStream(file);
		
		JRXlsExporter exporter = new JRXlsExporter();
		exporter.setParameter(JRExporterParameter.JASPER_PRINT, jp);
		exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, fos);
		
		exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, Boolean.TRUE);
		exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
		exporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
		exporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
		exporter.exportReport();
		
		Media media = new AMedia("piutangStnk", "xls", "application/vnd.ms-excel", file,true);
		Filedownload.save(media);
		
	}
	
	@NotifyChange("cb")
	@Command
	public void bersihkan(){
		System.out.println(cb);
	}
}
