package com.trio.gl.viewmodel.rpt;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.Validator;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.validator.AbstractValidator;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zkex.zul.Jasperreport;
import org.zkoss.zul.Window;

import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioJurnal;
import com.trio.gl.bean.TrioMstkonfigurasi;
import com.trio.gl.bean.TrioMstperkiraan;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;

public class RptPerincianPiutangVM extends TrioBasePageVM{

	private Date bulan;

	private TrioJurnal current;

	private TrioMstperkiraan selectedPerkiraan;

	private boolean isFocus;

	public Date getBulan() {
		return bulan;
	}

	public void setBulan(Date bulan) {
		this.bulan = bulan;
	}

	public boolean getFocus(){
		return this.isFocus;
	}

	public TrioMstperkiraan getSelectedPerkiraan() {
		if (selectedPerkiraan == null) selectedPerkiraan = new TrioMstperkiraan();
		return selectedPerkiraan;
	}

	public void setSelectedPerkiraan(TrioMstperkiraan selectedPerkiraan) {
		this.selectedPerkiraan = selectedPerkiraan;
	}

	public TrioJurnal getCurrent() {
		if (current == null) current = new TrioJurnal();
		return current;
	}

	public void setCurrent(TrioJurnal current) {
		this.current = current;
	}


	public List<TrioMstperkiraan> getPerkiraans(){
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		//		DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);
		List<TrioMstperkiraan> lists = getMasterFacade().getTrioMstperkiraanDao().findByLevelAndSub("4", "Y");
		return lists;
	}



	@NotifyChange({"subPerkiraans","selectedPerkiraan","kredit","debet"})
	@Command("showSelectedPerkiraan")
	public void showSelectedPerkiraan(){
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		//		DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);
		String idPerk = getSelectedPerkiraan().getTrioMstperkiraanPK().getIdPerkiraan();
		System.out.println("Id Perkiraan show "+idPerk);
	}


	@Command
	public void cetak(){
		System.out.println("ini cetak");

		DatabaseContextHolder.setConnectionType(ConnectionType.DEFAULT);

		TrioMstkonfigurasi konfig = getMasterFacade().getTrioMstkonfigurasiDao().findByPrimaryKey(new TrioMstkonfigurasi("111"));
		String pathReport = konfig.getNilai();

		File file = new File(pathReport+"\\lap_perincian_piutang.jasper");

		Window win = (Window) Executions.createComponents("/rpt/report.zul", null, null);
		win.setTitle("Laporan Perincian Piutang");
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		//		DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);
		Jasperreport report = (Jasperreport) win.getFellow("report");
		report.setDataConnection(getReportConnection());

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("CABANG", getCabangSession());

		SimpleDateFormat sdf = new SimpleDateFormat("MM");
		String beginDate = sdf.format(bulan);
		
		System.out.println("begin date  = " + beginDate);
		System.out.println("getSelectedPerkiraan  = " + getSelectedPerkiraan());

		params.put("ID_PERKIRAAN", getSelectedPerkiraan().getTrioMstperkiraanPK().getIdPerkiraan());
		params.put("BULAN", beginDate);

		report.setParameters(params);
		report.setType("pdf");
		report.setSrc(file.getAbsolutePath());
	}


	public Validator getFormValidator(){
		return new AbstractValidator() {

			public void validate(ValidationContext arg0) {
				// TODO , masbro
			}
		};

	}

}

