package com.trio.gl.viewmodel.rpt;

import java.io.File;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.zkoss.bind.annotation.Command;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zkex.zul.Jasperreport;
import org.zkoss.zul.Window;

import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioMstkonfigurasi;
import com.trio.gl.bean.TrioStokUnitTemp;
import com.trio.gl.helper.bean.CurCariStok;
import com.trio.gl.helper.bean.CurLapStok;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;
import com.trio.gl.util.TrioDateConv;
import com.trio.gl.util.TrioDateUtil;

/**
 * @author Gusti Arya 10:59:23 AM Jul 2, 2013
 */

public class RptStockDetSmhVM extends TrioBasePageVM{

	private Date bulan;
	private TrioStokUnitTemp selectedTrioStokUnitTemp;
	private List<TrioStokUnitTemp> kdDlr;
	private String pilih;
	

	public String getPilih() {
		return pilih;
	}

	public void setPilih(String pilih) {
		this.pilih = pilih;
	}

	public Date getBulan() {
		return bulan;
	}

	public void setBulan(Date bulan) {
		this.bulan = bulan;
	}
	
	public List<TrioStokUnitTemp> getKdDlr() {
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
//		DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);
		return getMasterFacade().getTrioStokUnitTempDao().findAll();
	}
	

	public TrioStokUnitTemp getSelectedTrioStokUnitTemp() {
		if (selectedTrioStokUnitTemp == null) {
			selectedTrioStokUnitTemp = new TrioStokUnitTemp();
		}
		return selectedTrioStokUnitTemp;
	}

	public void setSelectedTrioStokUnitTemp(
			TrioStokUnitTemp selectedTrioStokUnitTemp) {
		this.selectedTrioStokUnitTemp = selectedTrioStokUnitTemp;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Command
	public void cetak() throws ParseException, JRException{
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
//		DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);

		Date tglAkhir = TrioDateConv.valueOf(TrioDateUtil.getYear(getBulan()), TrioDateUtil.getMonth(getBulan())
				, TrioDateUtil.lastDayOf(getBulan()));

		System.out.println("tgl Akhir = " +  tglAkhir);
		
		List<TrioStokUnitTemp> lists = getMasterFacade().getTrioStokUnitTempDao()
				.findByNoMesinNotNull(tglAkhir, selectedTrioStokUnitTemp.getKdDlr());
		List<CurCariStok> curCariStokLists = new ArrayList<CurCariStok>();
		List<CurLapStok> curLapStokLists = new ArrayList<CurLapStok>();
		System.out.println("lists size trioStokUnitTemp = " + lists.size());
		
		for(TrioStokUnitTemp curr : lists){//outer for
			CurCariStok curCariStok = new CurCariStok(curr.getTgl(),//dari trioStokunit di masukkan ke dalam cursor cari stok
					curr.getNoMesin(), curr.getNoRangka(), curr.getKdItem(), 
					curr.getNoFakIn(), curr.getNoSjIn(), curr.getNoFakOut()
					,curr.getMasuk(), curr.getKeluar());
			curCariStokLists.add(curCariStok);//dari class cursorCariStok di masukkan lagi ke dalam
		}
		
		for(CurCariStok currStok : curCariStokLists){//inner for
			CurLapStok curLapStok = new CurLapStok(currStok.getNoFakIn(), 
					currStok.getNoSjIn(), currStok.getNoMesin(), 
					currStok.getNoMesin(), currStok.getNoMesin(), currStok.getTgl());
			if(currStok.getMasuk() == 1){//jika dari cariStok nilai masuk = 1, insert data ke POJO curLapStok
				curLapStokLists.add(curLapStok);
			}
			
			if(currStok.getKeluar() == 1){//jika dari cariStok nilai keluar = 1, hapus row yang ada di lapStok
				
				Iterator<CurLapStok> iterator = curLapStokLists.iterator();
				while (iterator.hasNext()) {
					CurLapStok lapStok = iterator.next();
					if (lapStok.getNoMesin().equalsIgnoreCase(currStok.getNoMesin())) {
						iterator.remove();
					}
				}
			}
		}
		
		//cetak ke jasper report
		System.out.println("curCariStok size  = " + curCariStokLists.size());
		System.out.println("curLapstok size = " + curLapStokLists.size());
		
		DatabaseContextHolder.setConnectionType(ConnectionType.DEFAULT);
		TrioMstkonfigurasi konfig = getMasterFacade().getTrioMstkonfigurasiDao()
				.findByPrimaryKey(new TrioMstkonfigurasi("111"));
		String pathReport = konfig.getNilai();
		File file = new File(pathReport + "\\laporan_stock_det_smh.jasper");
		
		Window win = (Window) Executions.createComponents("/rpt/report.zul", null, null);
		win.setTitle("Laporan Stock Detail SMH");
		DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);
		Jasperreport report = (Jasperreport) win.getFellow("report");
		Map params = new HashMap();
		params.put("CABANG", getKdDlr().get(0));
		params.put("PERIODE", bulan);
		
		report.setType("pdf");
		report.setParameters(params);
		report.setSrc(file.getAbsolutePath());
		report.setDatasource(new JRBeanCollectionDataSource(curLapStokLists));
	}
	
	@Command
	public void bersihkan(){
		DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);
	}
}
