package com.trio.gl.viewmodel.rpt;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zkex.zul.Jasperreport;
import org.zkoss.zul.Window;

import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioMstkonfigurasi;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;

public class RptJurnalMemoBulVM extends TrioBasePageVM{


	private Date bulan;
	private boolean cb;
	private Jasperreport report;
	private File file;
	
	public Date getBulan() {
		if(bulan==null || bulan.equals(null)){
			bulan=new Date();
		}
		return bulan;
	}

	public void setBulan(Date bulan) {
		this.bulan = bulan;
	}
	
	public boolean isCb() {
		return cb;
	}

	public void setCb(boolean cb) {
		this.cb = cb;
	}

	@NotifyChange({"report" , "cb"})
	@Command
	public void cetak(){
		DatabaseContextHolder.setConnectionType(ConnectionType.DEFAULT);
		
		TrioMstkonfigurasi konfig = getMasterFacade().getTrioMstkonfigurasiDao().findByPrimaryKey(new TrioMstkonfigurasi("111"));
		String pathReport = konfig.getNilai();
		
		if(cb==true){
			file = new File(pathReport+"\\Jurnal_memo_bul_ket.jasper");
		}else{
			file = new File(pathReport+"\\Jurnal_memo_bul.jasper");	
		}
		
		Window win = (Window) Executions.createComponents("/rpt/report.zul", null, null);
		win.setTitle("Laporan Jurnal Memorial");
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
//		DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);
		Jasperreport report = (Jasperreport) win.getFellow("report");
		report.setDataConnection(getReportConnection());
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("CABANG", getCabangSession());
		
		SimpleDateFormat sdf = new SimpleDateFormat("MM");
		String month = sdf.format(bulan);
		System.out.println( "monnth = " + month);
		
		params.put("BULAN", month);
		report.setParameters(params);
		report.setType("pdf");
		report.setSrc(file.getAbsolutePath());
		
	}
}

