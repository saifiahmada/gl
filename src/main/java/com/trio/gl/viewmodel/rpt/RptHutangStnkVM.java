package com.trio.gl.viewmodel.rpt;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.media.AMedia;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zkex.zul.Jasperreport;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Window;

import ar.com.fdvs.dj.core.DynamicJasperHelper;
import ar.com.fdvs.dj.core.layout.ClassicLayoutManager;
import ar.com.fdvs.dj.domain.DynamicReport;
import ar.com.fdvs.dj.domain.builders.ColumnBuilderException;
import ar.com.fdvs.dj.domain.builders.FastReportBuilder;

import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioJurnal;
import com.trio.gl.bean.TrioMstkonfigurasi;
import com.trio.gl.bean.TrioStokUnitTemp;
import com.trio.gl.helper.bean.CurTmpLap;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;

/**
 * @author Gusti Arya 2:41:07 PM Jul 19, 2013
 */

public class RptHutangStnkVM extends TrioBasePageVM {

	private String selected;
	private Date tglAwal;
	private Date tglAkhir;

	public String getSelected() {
		return selected;
	}

	public void setSelected(String selected) {
		this.selected = selected;
	}

	public Date getTglAwal() {
		if(tglAwal==null)
			tglAwal = new Date();
		return tglAwal;
	}

	public void setTglAwal(Date tglAwal) {
		this.tglAwal = tglAwal;
	}

	public Date getTglAkhir() {
		if(tglAkhir == null)
			tglAkhir = new Date();
		return tglAkhir;
	}

	public void setTglAkhir(Date tglAkhir) {
		this.tglAkhir = tglAkhir;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@NotifyChange("selected")
	@Command
	public void cetak(){
		String noPerk1 = "";
		String noPerk2 = "";

		if(selected.equalsIgnoreCase("cabang1")){
			noPerk1 = "41101";
			noPerk2 = "21204";
		}

		if(selected.equalsIgnoreCase("cabang2")){
			noPerk1 = "41106";
			noPerk2 = "21213";
		}

		System.out.println("noperk1 = " + noPerk1);
		System.out.println("noPerk2 = " + noPerk2);

		List<CurTmpLap> listsCur = getPrintOrCreate(noPerk1, noPerk2);

		DatabaseContextHolder.setConnectionType(ConnectionType.DEFAULT);
		TrioMstkonfigurasi config = getMasterFacade().getTrioMstkonfigurasiDao().findById("111");
		String reportPath = config.getNilai();
		File file = new File( reportPath + "\\laporan_hutang_stnk.jasper");

		Window  win = (Window) Executions.createComponents("/rpt/report.zul", null, null);
		win.setTitle("Laporan Hutang STNK");

		//		DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		Jasperreport report = (Jasperreport) win.getFellow("report");
		Map params = new HashMap();
		params.put("PERIODE_AWAL", tglAwal);
		params.put("PERIODE_AKHIR", tglAkhir);

		report.setType("pdf");
		report.setParameters(params);
		report.setSrc(file.getAbsolutePath());
		report.setDatasource(new JRBeanCollectionDataSource(listsCur));
	}

	@Command
	public void createFile() throws ColumnBuilderException, ClassNotFoundException, JRException, FileNotFoundException {

		String noPerk1 = "";
		String noPerk2 = "";

		if(selected.equalsIgnoreCase("cabang1")){
			noPerk1 = "41101";
			noPerk2 = "21204";
		}else{
			noPerk1 = "41106";
			noPerk2 = "21213";
		}

		List<CurTmpLap> listCur = getPrintOrCreate(noPerk1, noPerk2);

		//Membuat Column di excel
		FastReportBuilder drb = new FastReportBuilder();
		drb.addColumn("No. Jurnal", "no", String.class.getName(), 70);
		drb.addColumn("Tanggal", "tgl", Date.class.getName(),  70);
		drb.addColumn("No Mesin","noMesin",String.class.getName(),100);
		drb.addColumn("Nama", "nama", String.class.getName(), 200);
		drb.addColumn("Hutang STNK", "hStnk", BigDecimal.class.getName(),120,true, "##,##0.00");
		drb.addColumn("No. Urus", "noUrus", String.class.getName(),70);
		drb.addColumn("Tgl. Kredit", "tglKredit", Date.class.getName(),70);
		drb.addColumn("Pengurus Stnk", "pengStnk", BigDecimal.class.getName(),120,true, "##,##0.00");
		drb.addColumn("Selisih", "selisih", BigDecimal.class.getName(),120,true, "##,##0.00");
		drb.addColumn("Saldo", "saldo", BigDecimal.class.getName(),120, true, "##,##0.00");
		drb.setPrintColumnNames(true);
		drb.setIgnorePagination(true);
		drb.setTitle("LAPORAN HUTANG STNK");
		drb.setSubtitle("dibuat pada tanggal : " + new Date());

		DynamicReport dr = drb.build();
		JasperPrint jp = DynamicJasperHelper.generateJasperPrint(dr, new ClassicLayoutManager(), listCur);

		File file = new File("C:/hutang_stnk");
		FileOutputStream fos = new FileOutputStream(file);

		JRXlsExporter export = new JRXlsExporter();
		export.setParameter(JRExporterParameter.JASPER_PRINT, jp);
		export.setParameter(JRExporterParameter.OUTPUT_STREAM, fos);
		export.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
		export.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, Boolean.TRUE);
		export.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
		export.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
		export.exportReport();

		Media media = new AMedia("HutangStnk", "xls", "application/vnd.ms-excel", file,true);
		Filedownload.save(media);
	}

	public List<CurTmpLap> getPrintOrCreate(String noPerk1, String noPerk2){
		List<CurTmpLap> listsCur = new ArrayList<CurTmpLap>();

		//		DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));

		System.out.println("ini cetak");
		List<TrioJurnal> lists = getMasterFacade().getTrioJurnalDao().getListJurnalForStnk(tglAwal, tglAkhir, noPerk1);

		for(TrioJurnal current : lists){
			String no;
			String noMesin = "";
			String nama;
			String noUrus = "";
			String noFak= "";
			Date tgl;
			Date tglUrus = null;
			BigDecimal hStnk;
			BigDecimal pengStnk = new BigDecimal(0);
			BigDecimal selisih = new BigDecimal(0);
			BigDecimal saldo = new BigDecimal(0);
			BigDecimal kredit = new BigDecimal(0);
			BigDecimal debet = new BigDecimal(0);


			System.out.println( "noJurnal = "+  current.getTrioJurnalPK().getIdJurnal()  +  " noMesin = " + current.getNoMesin());
			if(current.getNoMesin()=="GROUP" || current.getNoMesin().equalsIgnoreCase("GROUP")){
				System.out.println("noreffnya = " + current.getNoReff());
				List<TrioStokUnitTemp> stokList = getMasterFacade().getTrioStokUnitTempDao()
						.getNoMesinGroup(current.getNoReff());

				for(TrioStokUnitTemp  listStoktemp : stokList){

					noMesin = listStoktemp.getNoMesin();
					no = current.getTrioJurnalPK().getIdJurnal();
					tgl = current.getTglJurnal();
					nama = current.getKet();
					noFak = current.getNoReff();

					kredit = getMasterFacade().getTrioJurnalDao().getKredit(no, noPerk2);
					debet = getMasterFacade().getTrioJurnalDao().getDebit(no, noPerk2);

					hStnk = kredit.subtract(debet);

					if(hStnk.compareTo(BigDecimal.ZERO) > 0){
						noUrus = getMasterFacade().getTrioDetailStnkDao().getNoUrus(noMesin);
						tglUrus = getMasterFacade().getTrioDetailStnkDao().getTglUrus(noMesin);
						pengStnk = getMasterFacade().getTrioDetailStnkDao().getPengStnk(noMesin);
						if(noUrus == null || noUrus == ""){
							saldo = hStnk;
							selisih = new BigDecimal(0);
						}else{
							selisih = hStnk.subtract(pengStnk);
						}
					}else{
						noUrus = getMasterFacade().getTrioDetailStnkDao().getNoUrus(noMesin);
						tglUrus = getMasterFacade().getTrioDetailStnkDao().getTglUrus(noMesin);
						pengStnk = getMasterFacade().getTrioDetailStnkDao().getPengStnk(noMesin);
					}


					CurTmpLap curTmpLap = new CurTmpLap(no, tgl, noMesin, nama, hStnk, noUrus, tglUrus, pengStnk, selisih, saldo,noFak);
					listsCur.add(curTmpLap);
				}

			}else{
				System.out.println("diElse");
				no = current.getTrioJurnalPK().getIdJurnal();
				tgl = current.getTglJurnal();
				noMesin = current.getNoMesin();
				nama = current.getKet();
				noFak = current.getNoReff();

				kredit = getMasterFacade().getTrioJurnalDao().getKredit(no, noPerk2);
				debet = getMasterFacade().getTrioJurnalDao().getDebit(no, noPerk2);
				System.out.println("kredit = " + kredit);
				System.out.println("debet = " + debet);
				
				hStnk = kredit.subtract(debet);
				System.out.println("Bigdecimal = " + hStnk);

				if(hStnk.compareTo(BigDecimal.ZERO) > 0){
					noUrus = getMasterFacade().getTrioDetailStnkDao().getNoUrus(noMesin);
					tglUrus = getMasterFacade().getTrioDetailStnkDao().getTglUrus(noMesin);
					pengStnk = getMasterFacade().getTrioDetailStnkDao().getPengStnk(noMesin);
					if(noUrus == null || noUrus == ""){
						saldo = hStnk;
						selisih = new BigDecimal(0);
					}else{
						selisih = hStnk.subtract(pengStnk);
					}
				}else{
					noUrus = getMasterFacade().getTrioDetailStnkDao().getNoUrus(noMesin);
					tglUrus = getMasterFacade().getTrioDetailStnkDao().getTglUrus(noMesin);
					pengStnk = getMasterFacade().getTrioDetailStnkDao().getPengStnk(noMesin);
				}

				CurTmpLap curTmpLap = new CurTmpLap(no, tgl, noMesin, nama, hStnk, noUrus, tglUrus, pengStnk, selisih, saldo, noFak);
				listsCur.add(curTmpLap);
			}
		}

		List<TrioJurnal> potOffLists = getMasterFacade().getTrioJurnalDao()
				.getListJurnalByFketPotOff(tglAwal, tglAkhir, noPerk2);

		for(TrioJurnal current : potOffLists){
			Date tgl;
			Date tglUrus;
			String no;
			String nama;
			String noReff;
			String noMesin = "";
			String noUrus;
			BigDecimal hStnk;
			BigDecimal pengStnk;
			BigDecimal debit = current.getDebet();
			BigDecimal kredit = current.getKredit();

			no = current.getTrioJurnalPK().getIdJurnal();
			tgl = current.getTglJurnal();
			nama = current.getKet();
			noReff = (current.getKet()).substring(nama.length()-15);
			hStnk = kredit.subtract(debit);

			List<TrioJurnal> listToNoMesin = getMasterFacade().getTrioJurnalDao()
					.getListJurnalByNoReff(noReff);
			for(TrioJurnal noMes : listToNoMesin){
				noMesin = noMes.getNoMesin();
			}
			noUrus = getMasterFacade().getTrioDetailStnkDao().getNoUrus(noMesin);
			tglUrus = getMasterFacade().getTrioDetailStnkDao().getTglUrus(noMesin);
			pengStnk = getMasterFacade().getTrioDetailStnkDao().getPengStnk(noMesin);

			CurTmpLap curTmpLap = new CurTmpLap(no, tgl, noMesin, nama, hStnk, noUrus, tglUrus, pengStnk, new BigDecimal(0), hStnk, noReff);
			listsCur.add(curTmpLap);
		}
		
		Iterator<CurTmpLap> iterator = listsCur.iterator();
		while(iterator.hasNext()){
			CurTmpLap curTmpLap = iterator.next();
			if (curTmpLap.gethStnk().compareTo(BigDecimal.ZERO) < 0) {
				iterator.remove();
			}
		}
		
		return listsCur;
	}
}
