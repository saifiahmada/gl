package com.trio.gl.viewmodel.rpt;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.Validator;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.validator.AbstractValidator;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zkex.zul.Jasperreport;
import org.zkoss.zul.Window;

import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioJurnal;
import com.trio.gl.bean.TrioMstPerusahaan;
import com.trio.gl.bean.TrioMstkonfigurasi;
import com.trio.gl.bean.TrioMstperkiraan;
import com.trio.gl.bean.TrioMstsubperkiraan;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;

public class RptBukuBesarPemPerTanggalVM extends TrioBasePageVM{

	private Date dateAwal;

	private Date dateAkhir;

	private TrioJurnal current;

	private TrioMstperkiraan selectedPerkiraan;

	private TrioMstsubperkiraan selectedSubPerkiraan;

	private List<TrioMstsubperkiraan> subPerkiraans;

	private boolean isFocus;

	public Date getDateAwal() {
		if(dateAwal==null)
			dateAwal = new Date();
		return dateAwal;
	}

	public void setDateAwal(Date dateAwal) {
		this.dateAwal = dateAwal;
	}

	public Date getDateAkhir() {
		if(dateAkhir==null)
			dateAkhir = new Date();
		return dateAkhir;
	}

	public void setDateAkhir(Date dateAkhir) {
		this.dateAkhir = dateAkhir;
	}

	public boolean getFocus(){
		return this.isFocus;
	}

	public TrioMstsubperkiraan getSelectedSubPerkiraan() {
		if (selectedSubPerkiraan == null) selectedSubPerkiraan = new TrioMstsubperkiraan();
		return selectedSubPerkiraan;
	}

	public void setSelectedSubPerkiraan(TrioMstsubperkiraan selectedSubPerkiraan) {
		this.selectedSubPerkiraan = selectedSubPerkiraan;
	}

	public TrioMstperkiraan getSelectedPerkiraan() {
		if (selectedPerkiraan == null) selectedPerkiraan = new TrioMstperkiraan();
		return selectedPerkiraan;
	}

	public void setSelectedPerkiraan(TrioMstperkiraan selectedPerkiraan) {
		this.selectedPerkiraan = selectedPerkiraan;
	}

	public TrioJurnal getCurrent() {
		if (current == null) current = new TrioJurnal();
		return current;
	}

	public void setCurrent(TrioJurnal current) {
		this.current = current;
	}


	public List<TrioMstperkiraan> getPerkiraans(){
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		//		DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);

		return getMasterFacade().getTrioMstperkiraanDao().findAll();
	}

	public List<TrioMstsubperkiraan> getSubPerkiraans(){
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		//		DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);
		if (getSelectedPerkiraan().getTrioMstperkiraanPK().getIdPerkiraan() != null){
			System.out.println("dan id = "+getSelectedPerkiraan().getTrioMstperkiraanPK().getIdPerkiraan());
			subPerkiraans = getMasterFacade().getTrioMstsubperkiraanDao()
					.findByIdPerkiraan(getSelectedPerkiraan().getTrioMstperkiraanPK().getIdPerkiraan());
		}else{
			subPerkiraans = new ArrayList<TrioMstsubperkiraan>();
		}
		return subPerkiraans;
	}

	public void setSubPerkiraans(List<TrioMstsubperkiraan> subPerkiraans) {
		this.subPerkiraans = subPerkiraans;
	}

	@NotifyChange({"subPerkiraans","selectedPerkiraan"})
	@Command("showSelectedPerkiraan")
	public void showSelectedPerkiraan(){
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		//		DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);
		String idPerk = getSelectedPerkiraan().getTrioMstperkiraanPK().getIdPerkiraan();
		System.out.println("Id Perkiraan show "+idPerk);
		subPerkiraans = getMasterFacade().getTrioMstsubperkiraanDao().findByIdPerkiraan(idPerk);
	}


	@Command
	public void cetak(){
		System.out.println("ini cetak");

		DatabaseContextHolder.setConnectionType(ConnectionType.DEFAULT);

		TrioMstkonfigurasi konfig = getMasterFacade().getTrioMstkonfigurasiDao().findByPrimaryKey(new TrioMstkonfigurasi("111"));
		String pathReport = konfig.getNilai();

		File file = new File(pathReport+"\\Buku_besar_pem_pertanggal.jasper");

		Window win = (Window) Executions.createComponents("/rpt/report.zul", null, null);
		win.setTitle("Laporan Buku Besar Per Tanggal");
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		//		DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);
		Jasperreport report = (Jasperreport) win.getFellow("report");
		report.setDataConnection(getReportConnection());

		TrioMstPerusahaan trioMstPerusahaan = getMasterFacade().getTrioMstPerusahaanDao().find();
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("CABANG", getCabangSession());
		params.put("NAMA_PERS", trioMstPerusahaan.getNamaPers());
		params.put("ALAMAT_PERS", trioMstPerusahaan.getAlamatPers());
		params.put("TELP_1", trioMstPerusahaan.getTelp1());
		params.put("TELP_2", trioMstPerusahaan.getTelp2());
		params.put("FAX", trioMstPerusahaan.getFax());
		params.put("TGL_AWAL", dateAwal);
		params.put("TGL_AKHIR", dateAkhir);
		params.put("ID_PERKIRAAN", getSelectedPerkiraan().getTrioMstperkiraanPK().getIdPerkiraan());
		params.put("ID_SUB", getSelectedSubPerkiraan().getTrioMstsubperkiraanPK().getIdSub());

		report.setParameters(params);
		report.setType("pdf");
		report.setSrc(file.getAbsolutePath());
	}


	public Validator getFormValidator(){
		return new AbstractValidator() {

			public void validate(ValidationContext arg0) {
				// TODO , masbro
			}
		};

	}

}

