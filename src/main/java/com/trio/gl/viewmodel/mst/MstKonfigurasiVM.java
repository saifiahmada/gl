package com.trio.gl.viewmodel.mst;

import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.Validator;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.validator.AbstractValidator;
import org.zkoss.zul.ListModelList;

import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioMstkonfigurasi;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;

/** @author Saifi Ahmada Mar 22, 2013 10:55:55 AM  **/

public class MstKonfigurasiVM extends TrioBasePageVM {
	
	private TrioMstkonfigurasi current;
	
	private ListModelList<TrioMstkonfigurasi> listModel;
	
	private boolean isFocus;
	
	@Init
	public void init(){
		isFocus = true;
	}
	
	public boolean getFocus(){
		return isFocus;
	}
	
	public TrioMstkonfigurasi getCurrent() {
		if (current == null) current = new TrioMstkonfigurasi();
		return current;
	}

	public void setCurrent(TrioMstkonfigurasi current) {
		this.current = current;
	}

	public ListModelList<TrioMstkonfigurasi> getListModel() {
		DatabaseContextHolder.setConnectionType(ConnectionType.DEFAULT);
		if (listModel == null){
			listModel = new ListModelList<TrioMstkonfigurasi>();
			listModel.addAll(getMasterFacade().getTrioMstkonfigurasiDao().findAll());
		}
		return listModel;
	}

	public void setListModel(ListModelList<TrioMstkonfigurasi> listModel) {
		this.listModel = listModel;
	}
	
	@NotifyChange({"listModel","current","focus"})
	@Command
	public void save(){
		DatabaseContextHolder.setConnectionType(ConnectionType.DEFAULT);
		getMasterFacade().getTrioMstkonfigurasiDao().saveOrUpdate(getCurrent(), getUserSession());
		isFocus = true;
		reset();
	}
	
	@NotifyChange({"listModel","current","focus"})
	@Command
	public void reset(){
		DatabaseContextHolder.setConnectionType(ConnectionType.DEFAULT);
		current = new TrioMstkonfigurasi();
		listModel = new ListModelList<TrioMstkonfigurasi>();
		listModel.addAll(getMasterFacade().getTrioMstkonfigurasiDao().findAll());
		isFocus = true;
	}
	
	@NotifyChange({"listModel","focus"})
	@Command
	public void search(){
		DatabaseContextHolder.setConnectionType(ConnectionType.DEFAULT);
		listModel = new ListModelList<TrioMstkonfigurasi>();
		listModel.addAll(getMasterFacade().getTrioMstkonfigurasiDao().findByCriteria(getCurrent()));
		isFocus = true;
	}

	public Validator getFormValidator(){
		return new AbstractValidator() {
			
			public void validate(ValidationContext ctx) {
				// TODO , masbro
				if (ctx.getCommand().equals("search")){
					System.out.println("Numpang lewat bro");
				}else {
					String field = (String) ctx.getProperties("idKonfigurasi")[0].getValue();
					if (field == null || field.equalsIgnoreCase("")){
						addInvalidMessage(ctx,"fkey1", "*isi");
					}
					field = (String) ctx.getProperties("nama")[0].getValue();
					if (field == null || field.equalsIgnoreCase("")){
						addInvalidMessage(ctx,"fkey2", "*isi");
					}
					field = (String) ctx.getProperties("nilai")[0].getValue();
					if (field == null || field.equalsIgnoreCase("")){
						addInvalidMessage(ctx,"fkey3", "*isi");
					}
				}
				
				
			}
		};
	}

}

