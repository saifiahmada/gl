package com.trio.gl.viewmodel.mst;

import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.Validator;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.validator.AbstractValidator;
import org.zkoss.zul.ListModelList;

import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioMstperkiraan;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;

/** @author Saifi Ahmada Feb 15, 2013 11:01:04 AM  **/

public class MstPerkiraanVM extends TrioBasePageVM {
	
	//data object
	private TrioMstperkiraan current;
	
	//data component
	private ListModelList<TrioMstperkiraan> listModel;

	public TrioMstperkiraan getCurrent() {
		if (current == null) current = new TrioMstperkiraan();
		return current;
	}

	public void setCurrent(TrioMstperkiraan current) {
		this.current = current;
	}

	public ListModelList<TrioMstperkiraan> getListModel() {
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		if (listModel == null){
			listModel = new ListModelList<TrioMstperkiraan>();
			listModel.addAll(getMasterFacade().getTrioMstperkiraanDao().findAll());
		}
		return listModel;
	}

	public void setListModel(ListModelList<TrioMstperkiraan> listModel) {
		this.listModel = listModel;
	}
	
	@NotifyChange({"listModel", "current"})
	@Command("save")
	public void save(){
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		getMasterFacade().getTrioMstperkiraanDao().saveOrUpdate(current, getUserSession());
		current = new TrioMstperkiraan();
		listModel = new ListModelList<TrioMstperkiraan>();
		listModel.addAll(getMasterFacade().getTrioMstperkiraanDao().findAll());
	}
	
	@NotifyChange({"listModel", "current"})
	@Command("reset")
	public void reset(){
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		current = new TrioMstperkiraan();
		listModel = new ListModelList<TrioMstperkiraan>();
		listModel.addAll(getMasterFacade().getTrioMstperkiraanDao().findAll());
	}
	
	@NotifyChange("listModel")
	@Command("search")
	public void search(){
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		listModel = new ListModelList<TrioMstperkiraan>();
		listModel.addAll(getMasterFacade().getTrioMstperkiraanDao().findByCriteria(current));
	}
	
	public Validator getFormValidator(){
		return new AbstractValidator() {
			
			public void validate(ValidationContext ctx) {
				// TODO , masbro
				if (ctx.getCommand().equals("search")){
					System.out.println("lewat bro");
				}else {
					String field = (String) ctx.getProperties("idPerkiraan")[0].getValue();
					if (field == null || field.equalsIgnoreCase("")){
						addInvalidMessage(ctx, "fkey1", "*isi");
					}
					field = (String) ctx.getProperties("namaPerkiraan")[0].getValue();
					if (field == null || field.equalsIgnoreCase("")){
						addInvalidMessage(ctx, "fkey2", "*isi");
					}
					field = (String) ctx.getProperties("sub")[0].getValue();
					if (field == null || field.equalsIgnoreCase("")){
						addInvalidMessage(ctx, "fkey3", "*isi");
					}
//					field = (String) ctx.getProperties("level")[0].getValue();
//					if (field == null || field.equalsIgnoreCase("")){
//						addInvalidMessage(ctx, "fkey4", "Level harus diisi");
//					}
				}	
			}
		};
	}

}

