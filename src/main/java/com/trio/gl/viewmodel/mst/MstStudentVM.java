package com.trio.gl.viewmodel.mst;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.Validator;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.validator.AbstractValidator;
import org.zkoss.zul.ListModelList;

import com.trio.gl.bean.Student;

/** @author Saifi Ahmada Feb 27, 2013 11:13:41 AM  **/

public class MstStudentVM {
	
	private Student current,search;
	
	private ListModelList<Student> listModel;
	
	@Init
	public void init(){
		List<Student> list = new ArrayList<Student>();
		list.add(new Student("1", "Student 1"));
		list.add(new Student("2", "Student 2"));
		list.add(new Student("3", "Student 3"));
		list.add(new Student("4", "Student 4"));
		list.add(new Student("5", "Student 5"));
		list.add(new Student("6", "Student 6"));
		listModel = new ListModelList<Student>();
		listModel.addAll(list);
	}

	public Student getSearch() {
		if (search == null) search = new Student();
		return search;
	}

	public void setSearch(Student search) {
		this.search = search;
	}

	public Student getCurrent() {
		if (current == null) current = new Student();
		return current;
	}

	public void setCurrent(Student current) {
		this.current = current;
	}

	public ListModelList<Student> getListModel() {
		if (listModel == null){
			listModel = new ListModelList<Student>();
		}
		return listModel;
	}

	public void setListModel(ListModelList<Student> listModel) {
		this.listModel = listModel;
	}
	
	@NotifyChange("listModel")
	@Command("save")
	public void save(){
		System.out.println("obj current "+current);
		listModel.add(current);
		
	}
	
	//@NotifyChange({"listModel","current"}) 
	@Command("search")
	public void search(){
		
			System.out.println("current id "+current.getId());
			System.out.println("current nama "+current.getNama());
	}
	
	@NotifyChange("current")
	@Command("reset")
	public void reset(){
		current = null;
	}
	
	public Validator getFormValidator(){
		return new AbstractValidator() {
			
			public void validate(ValidationContext ctx) {
				// TODO , masbro
				
				if (ctx.getCommand().equals("search")){
					System.out.println("monggo lewat");
				}else{
					
				String id = (String) ctx.getProperties("id")[0].getValue();
				if (id == null || id.equalsIgnoreCase("")){
					addInvalidMessage(ctx, "fkey1", "id is empty bro");
				}
				String nama = (String) ctx.getProperties("nama")[0].getValue();
				if (nama == null || nama.equalsIgnoreCase("")){
					addInvalidMessage(ctx, "fkey2", "nama is empty bro");
				}
				}
			}
		};
	}

}

