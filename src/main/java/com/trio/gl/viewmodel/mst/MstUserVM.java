package com.trio.gl.viewmodel.mst;

import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.Validator;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.validator.AbstractValidator;
import org.zkoss.zul.ListModelList;

import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioMstuser;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;
import com.trio.gl.util.PasswdUtil;

/** @author Saifi Ahmada Feb 11, 2013 3:34:44 PM  **/

public class MstUserVM extends TrioBasePageVM {
	
	//date object
	private TrioMstuser current,search;
	
	//data component
	private ListModelList<TrioMstuser> listModel;

	public TrioMstuser getCurrent() {
		if (current == null) current = new TrioMstuser();
		return current;
	}
	
	public TrioMstuser getSearch() {
		if (search == null) search = new TrioMstuser();
		return search;
	}

	public void setSearch(TrioMstuser search) {
		this.search = search;
	}

	@NotifyChange({"listModel","current"})
	@Command("save")
	public void save() {

		DatabaseContextHolder.setConnectionType(ConnectionType.DEFAULT); 
		String passwd = PasswdUtil.getMD5DigestFromString(current.getTrioMstuserPK().getVusername()+current.getVpassword());
		current.setVpassword(passwd);
		
		getMasterFacade().getTrioMstuserDao().saveOrUpdate(current, getUserSession());
		current = new TrioMstuser();
		
		listModel = new ListModelList<TrioMstuser>();
		listModel.addAll(getMasterFacade().getTrioMstuserDao().findAll());
				
	}
	
	@NotifyChange({"current", "listModel"})
	@Command("reset")
	public void reset(){
		DatabaseContextHolder.setConnectionType(ConnectionType.DEFAULT);
		current = new TrioMstuser();
		listModel = new ListModelList<TrioMstuser>();
		listModel.addAll(getMasterFacade().getTrioMstuserDao().findAll());
	}
	
	@NotifyChange("listModel")
	@Command("search")
	public void search(){
		DatabaseContextHolder.setConnectionType(ConnectionType.DEFAULT);
		listModel = new ListModelList<TrioMstuser>();
		listModel.addAll(getMasterFacade().getTrioMstuserDao().findByCriteria(current)); 
	}

	public void setCurrent(TrioMstuser current) {
		this.current = current;
	}

	public ListModelList<TrioMstuser> getListModel() {
		DatabaseContextHolder.setConnectionType(ConnectionType.DEFAULT);
		if (listModel == null){
		listModel = new ListModelList<TrioMstuser>();	
		listModel.addAll(getMasterFacade().getTrioMstuserDao().findAll());
		}
		return listModel;
	}
	
	public Validator getFormValidator(){
		return new AbstractValidator() {
			
			public void validate(ValidationContext ctx) {
				// TODO , masbro
				
				if (ctx.getCommand().equals("search")){
					System.out.println("lewat bro");
				}else {
					
					String vusername = (String) ctx.getProperties("vusername")[0].getValue();
					if (vusername == null || vusername.equalsIgnoreCase("")) addInvalidMessage(ctx, "fkey1","Username tidak boleh kosong");
					
					String vpassword = (String) ctx.getProperties("vpassword")[0].getValue();
					if (vpassword == null || vpassword.equalsIgnoreCase("")) addInvalidMessage(ctx, "fkey2", "Password tidak boleh kosong");
					
					String vstat = (String) ctx.getProperties("vstat")[0].getValue();
					if (vstat == null || vstat.equalsIgnoreCase("")) addInvalidMessage(ctx, "fkey3", "Status tidak boleh kosong");
				}
			}
		};
	}

}

