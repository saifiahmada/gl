package com.trio.gl.viewmodel.mst;

import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.Validator;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.validator.AbstractValidator;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zul.ListModelList;

import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioMstperkiraan;
import com.trio.gl.bean.TrioMstsubperkiraan;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;

/** @author Saifi Ahmada Mar 07, 2013 14:24:01 AM  **/

public class MstSubPerkiraanVM extends TrioBasePageVM {
	
	//data object
	private TrioMstsubperkiraan current;
	
	//data component
	private ListModelList<TrioMstsubperkiraan> listModel;

	public TrioMstsubperkiraan getCurrent() {
		if (current == null) current = new TrioMstsubperkiraan();
		return current;
	}

	public void setCurrent(TrioMstsubperkiraan current) {
		this.current = current;
	}

	public ListModelList<TrioMstsubperkiraan> getListModel() {
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		if (listModel == null){
			listModel = new ListModelList<TrioMstsubperkiraan>();
			listModel.addAll(getMasterFacade().getTrioMstsubperkiraanDao().findAll());
		}
		return listModel;
	}

	public void setListModel(ListModelList<TrioMstsubperkiraan> listModel) {
		this.listModel = listModel;
	}
	
	@NotifyChange({"listModel", "current"})
	@Command("save")
	public void save(){
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		getMasterFacade().getTrioMstsubperkiraanDao().saveOrUpdate(current, getUserSession());
		current = new TrioMstsubperkiraan();
		listModel = new ListModelList<TrioMstsubperkiraan>();
		listModel.addAll(getMasterFacade().getTrioMstsubperkiraanDao().findAll());
	}
	
	@NotifyChange({"listModel", "current"})
	@Command("reset")
	public void reset(){
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		current = new TrioMstsubperkiraan();
		listModel = new ListModelList<TrioMstsubperkiraan>();
		listModel.addAll(getMasterFacade().getTrioMstsubperkiraanDao().findAll());
	}
	
	@NotifyChange("listModel")
	@Command("search")
	public void search(){
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		listModel = new ListModelList<TrioMstsubperkiraan>();
		listModel.addAll(getMasterFacade().getTrioMstsubperkiraanDao().findByCriteria(current));
	}
	
	@Command("openPopPerkiraan") 
	public void openPopPerkiraan(@ContextParam (ContextType.VIEW) Component view){
		Executions.getCurrent().createComponents("/popup/popupPerkiraan.zul", view, null);
	}
	
	@Command
    @NotifyChange("current")
    public void sendParamPerkiraan(@BindingParam("perkiraanParam") TrioMstperkiraan perkiraan) {
    	current.getTrioMstsubperkiraanPK().setIdPerkiraan(perkiraan.getTrioMstperkiraanPK().getIdPerkiraan());
    }
	
	public Validator getFormValidator(){
		return new AbstractValidator() {
			
			public void validate(ValidationContext ctx) {
				// TODO , masbro
				if (ctx.getCommand().equals("search")){
					System.out.println("lewat bro");
				}else {
					String field = (String) ctx.getProperties("idPerkiraan")[0].getValue();
					if (field == null || field.equalsIgnoreCase("")){
						addInvalidMessage(ctx, "fkey1", "*isi");
					}
					field = (String) ctx.getProperties("idSub")[0].getValue();
					if (field == null || field.equalsIgnoreCase("")){
						addInvalidMessage(ctx, "fkey2", "*isi");
					}
					field = (String) ctx.getProperties("namaSub")[0].getValue();
					if (field == null || field.equalsIgnoreCase("")){
						addInvalidMessage(ctx, "fkey3", "*isi");
					}
//					field = (String) ctx.getProperties("level")[0].getValue();
//					if (field == null || field.equalsIgnoreCase("")){
//						addInvalidMessage(ctx, "fkey4", "Level harus diisi");
//					}
				}	
			}
		};
	}

}

