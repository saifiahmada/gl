package com.trio.gl.viewmodel.mst;

import java.math.BigDecimal;
import java.util.List;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zul.ListModelList;

import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioLabaDitahan;
import com.trio.gl.bean.TrioMstPerusahaan;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;

/**
 * @author Gusti Arya 10:43:15 AM Sep 30, 2013
 */

public class MstLabaDitahanVM extends TrioBasePageVM{

	private ListModelList<TrioLabaDitahan> listModel;
	private TrioLabaDitahan selectedLaba;
	private TrioMstPerusahaan mstPerusahaan;
	private String selectedPerusahaan;
	private String deleteMessage;

	public String getDeleteMessage() {
		return deleteMessage;
	}

	public void setDeleteMessage(String deleteMessage) {
		this.deleteMessage = deleteMessage;
	}

	public String getSelectedPerusahaan() {
		return selectedPerusahaan;
	}

	public void setSelectedPerusahaan(String selectedPerusahaan) {
		this.selectedPerusahaan = selectedPerusahaan;
	}

	public TrioMstPerusahaan getMstPerusahaan() {
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		if(mstPerusahaan ==null)
			mstPerusahaan = new TrioMstPerusahaan();
		mstPerusahaan = getMasterFacade().getTrioMstPerusahaanDao().find();
		return mstPerusahaan;
	}

	public void setMstPerusahaan(TrioMstPerusahaan mstPerusahaan) {
		this.mstPerusahaan = mstPerusahaan;
	}

	public TrioLabaDitahan getSelectedLaba() {
		return selectedLaba;
	}

	@NotifyChange("selectedLaba")
	public void setSelectedLaba(TrioLabaDitahan selectedLaba) {
		this.selectedLaba = selectedLaba;
	}

	public ListModelList<TrioLabaDitahan> getListModel() {
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		return listModel;
	}

	public void setListModel(ListModelList<TrioLabaDitahan> listModel) {
		this.listModel = listModel;
	}


	@Command("showList")
	@NotifyChange({"listModel","selectedLaba"})
	public void showList(){
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		List<TrioLabaDitahan> lists = getMasterFacade().getTrioLabaDitahanDao().findListbyKdDlr(selectedPerusahaan);
		System.out.println(lists.size());
		ListModelList<TrioLabaDitahan> iList = new ListModelList<TrioLabaDitahan>();
		iList.addAll(lists);
		setListModel(iList);

		selectedLaba = null;
	}


	@Command("save")
	@NotifyChange({"listModel","selectedLaba"})
	public void save(){
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		if(selectedLaba.getTrioLabaDitahanPK().getKdLaba() == null || selectedLaba.getTrioLabaDitahanPK().getKdLaba().equals("")){
			Messagebox.show("Kode Laba Tidak Boleh Kosong");
			return;
		}
		
		TrioLabaDitahan trioLabaDitahan = new TrioLabaDitahan();
		String kdDlr = getSelectedPerusahaan();
		String kdLaba = selectedLaba.getTrioLabaDitahanPK().getKdLaba();
		String ket = selectedLaba.getKet();
		BigDecimal total = selectedLaba.getTotal();

		trioLabaDitahan.getTrioLabaDitahanPK().setKdDlr(kdDlr);
		trioLabaDitahan.getTrioLabaDitahanPK().setKdLaba(kdLaba);
		trioLabaDitahan.setKet(ket);
		trioLabaDitahan.setTotal(total);
		getMasterFacade().getTrioLabaDitahanDao().saveOrUpdate(trioLabaDitahan, getUserSession());
		showList();
	}

	@Command("labaBaru")
	@NotifyChange("selectedLaba")
	public void labaBaru(){
		if(selectedPerusahaan == null){
			Messagebox.show("Silahkan pilih cabang terlebih dahulu");
			return;
		}
		selectedLaba = new TrioLabaDitahan();
	}

	@Command("deletePrompt")
	@NotifyChange("deleteMessage")
	public void deletePromt(){
		setDeleteMessage("Are you sure to delete this data ?");

	}

	@Command("deleteLaba")
	@NotifyChange({"selectedLaba", "listModel","deleteMessage"})
	public void deleteLaba(){
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		getMasterFacade().getTrioLabaDitahanDao().delete(selectedLaba);
		showList();
		setDeleteMessage(null);
		setSelectedLaba(null);
	}

	@Command("cancelDelete")
	@NotifyChange("deleteMessage")
	public void cancelDelete(){
		setDeleteMessage(null);
	}
}
