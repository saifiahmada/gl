package com.trio.gl.viewmodel.mst;

import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.Validator;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.validator.AbstractValidator;
import org.zkoss.zul.ListModelList;

import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioMstkonfigcabang;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;

/** @author Saifi Ahmada Apr 3, 2013 2:42:28 PM  **/

public class MstConfigCabangVM extends TrioBasePageVM {
	
	private TrioMstkonfigcabang current;
	
	private ListModelList<TrioMstkonfigcabang> listModel;

	public TrioMstkonfigcabang getCurrent() {
		if (current == null) current = new TrioMstkonfigcabang();
		return current;
	}

	public void setCurrent(TrioMstkonfigcabang current) {
		this.current = current;
	}

	public ListModelList<TrioMstkonfigcabang> getListModel() {
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		if (listModel == null){
			listModel = new ListModelList<TrioMstkonfigcabang>();
			listModel.addAll(getMasterFacade().getTrioMstkonfigcabangDao().findAll());
		}
		return listModel;
	}

	@NotifyChange({"listModel","current"})
	@Command("save")
	public void save(){
		System.out.println("save");
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		getMasterFacade().getTrioMstkonfigcabangDao().saveOrUpdate(getCurrent(), getUserSession());
		reset();
	}
	
	@NotifyChange({"listModel","current"})
	@Command("reset")
	public void reset(){
		System.out.println("reset");
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		current = new TrioMstkonfigcabang();
		listModel = new ListModelList<TrioMstkonfigcabang>();
		listModel.addAll(getMasterFacade().getTrioMstkonfigcabangDao().findAll());
	}
	
	@NotifyChange({"listModel"})
	@Command("search")
	public void search(){
		System.out.println("search");
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		listModel = new ListModelList<TrioMstkonfigcabang>();
		listModel.addAll(getMasterFacade().getTrioMstkonfigcabangDao().findByCriteria(getCurrent()));
	}
	
	public Validator getFormValidator(){
		return new AbstractValidator() {
			
			public void validate(ValidationContext ctx) {
				// TODO , masbro
				if (ctx.getCommand().equals("search")){
					System.out.println("tombol search di form validator");
				}else{
					String field = (String) ctx.getProperties("kdDlr")[0].getValue();
					if (field == null || field.equalsIgnoreCase("")){
						addInvalidMessage(ctx,"fkey1", "*isi");
					}
					field = (String) ctx.getProperties("context")[0].getValue();
					if (field == null || field.equalsIgnoreCase("")){
						addInvalidMessage(ctx,"fkey2", "*isi");
					}
					field = (String) ctx.getProperties("noCabang")[0].getValue();
					if (field == null || field.equalsIgnoreCase("")){
						addInvalidMessage(ctx,"fkey3", "*isi");
					}
					
				}
			}
		};
	}
	
	

}

