package com.trio.gl.viewmodel.utl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.zkoss.bind.annotation.Command;
import org.zkoss.zhtml.Messagebox;

import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioJurnal;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;

/**
 * @author Gusti Arya 9:57:12 AM Oct 23, 2013
 */

public class UtlCekBalanceJurnalVM extends TrioBasePageVM{

	private Date periode;
	
	public Date getPeriode() {
		if(periode == null)
			periode = new Date();
		return periode;
	}

	public void setPeriode(Date periode) {
		this.periode = periode;
	}

	@Command
	public void prosesCek(){
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		System.out.println("Proses Cek button");
		System.out.println(getPeriode());
		
		SimpleDateFormat sdf = new SimpleDateFormat("MM");
		String bulan = sdf.format(getPeriode());
		System.out.println("bulan  = " + bulan);
		
		sdf= new SimpleDateFormat("yyyy");
		String tahun = sdf.format(getPeriode());
		System.out.println("tahun " + tahun);
		
		List<TrioJurnal> lists = getMasterFacade().getTrioJurnalDao().getBalanceJurnal(bulan, tahun);
		
		for(TrioJurnal current : lists){
			if(current.getDebet().compareTo(current.getKredit()) != 0 ){
				Messagebox.show("No. Ref ["  + current.getTrioJurnalPK().getIdJurnal()+ "] Tidak Balance ");
				return;
			}
		}
		
		Messagebox.show("Proses Selesai");
	}
}
