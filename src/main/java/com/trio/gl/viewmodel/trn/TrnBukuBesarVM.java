package com.trio.gl.viewmodel.trn;

import java.util.Date;
import java.util.List;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Messagebox;

import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioJurnal;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;
import com.trio.gl.util.TrioDateConv;
import com.trio.gl.util.TrioDateUtil;

/** @author Saifi Ahmada Apr 12, 2013 10:56:13 PM **/

public class TrnBukuBesarVM extends TrioBasePageVM {

	private Date monthYear;

	private boolean pilih;

	public boolean isPilih() {
		return pilih;
	}

	public void setPilih(boolean pilih) {
		this.pilih = pilih;
	}

	public Date getMonthYear() {
		if (monthYear == null)
			monthYear = new Date();
		return monthYear;
	}

	public void setMonthYear(Date monthYear) {
		this.monthYear = monthYear;
	}

	@NotifyChange({ "" })
	@Command
	public void proses() {
		System.out.println("proses");
		Date tglAwal = TrioDateConv.valueOf(TrioDateUtil.getYear(monthYear),
				TrioDateUtil.getMonth(monthYear), 1);
		System.out.println("Tahun = " + TrioDateUtil.getYear(monthYear));
		Date tglAkhir = TrioDateConv.valueOf(TrioDateUtil.getYear(monthYear),
				TrioDateUtil.getMonth(monthYear),
				TrioDateUtil.lastDayOf(tglAwal));

		System.out.println("tgl awal " + tglAwal);
		System.out.println("tgl akhir " + tglAkhir);

		DatabaseContextHolder.setConnectionType(ConnectionType
				.setType(getCabangSession()));
		System.out.println("Cabang === " + getCabangSession());
		List<TrioJurnal> list = getMasterFacade().getTrioJurnalDao().getListJurnalByRangeTanggal(tglAwal, tglAkhir);
		System.out.println("ukuran " + list.size());

		//getMasterFacade().getTrioBukubesarDao().saveJurnalToBukuBesar(list, getUserSession());
		String pesan = getMasterFacade().getTrioBukubesarDao().prosesBukubesar(list, tglAwal, tglAkhir, pilih, getUserSession());
		
		Messagebox.show("Proses : " + pesan);
		
	}

	@Command
	public void pilih(@BindingParam("checked") boolean isPicked) {
		pilih = isPicked;
	}

}