package com.trio.gl.viewmodel.trn;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;

import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioJurnal;
import com.trio.gl.bean.TrioMstkonfigcabang;
import com.trio.gl.bean.TrioTarikpartretur;
import com.trio.gl.bean.TrioTarikpartsale;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;
import com.trio.gl.util.TrioDateConv;

/** @author Saifi Ahmada Mar 23, 2013 11:47:15 PM  **/

public class TrnTarikPartVM extends TrioBasePageVM {
	
	private String kodeCabang;
	
	private Date tglTarik;
	
	private ListModelList<TrioTarikpartsale> listModel;
	
	private ListModelList<TrioMstkonfigcabang> configCabangList;
	
	private TrioMstkonfigcabang config;
	
	public TrioMstkonfigcabang getConfig() {
		if (config == null) config = new TrioMstkonfigcabang();
		return config;
	}

	public void setConfig(TrioMstkonfigcabang config) {
		this.config = config;
	}

	public ListModelList<TrioMstkonfigcabang> getConfigCabangList(){
		if (configCabangList == null){
			configCabangList = new ListModelList<TrioMstkonfigcabang>();
			DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
			configCabangList.addAll(getMasterFacade().getTrioMstkonfigcabangDao().getListCabangByContext("PART"));
		}
		return configCabangList;
	}
	
	public ListModelList<TrioTarikpartsale> getListModel(){
		if (listModel == null){
			listModel = new ListModelList<TrioTarikpartsale>();
		}
		return listModel;
	}
	
	@NotifyChange({"config","tglTarik","listModel"}) 
	@Command
	public void save(){
		
		if (getConfig().getTrioMstkonfigcabangPK().getKdDlr() == null || 
				getConfig().getTrioMstkonfigcabangPK().getKdDlr().equalsIgnoreCase("")){
			Messagebox.show("Kode Cabang belum diisi");
			return;
		}
		if (getTglTarik() == null){
			Messagebox.show("Tanggal belum diisi");
			return;
		}
		
		String tgl = TrioDateConv.format(getTglTarik(), "YYYY-MM-dd");
		String kdDlr = getConfig().getTrioMstkonfigcabangPK().getKdDlr();
		String noCabang = getConfig().getNoCabang();
		
		//bgn membuat jurnal untuk sale
		DatabaseContextHolder.setConnectionType(ConnectionType.ORACLEPART);
		List<TrioTarikpartsale> listSale = getMasterFacade().getTrioTarikoracleDao()
				.getListTriotarikpartsale(kdDlr, tgl);
		
		for (TrioTarikpartsale sale : listSale){
				String idPerk1 = null;
				String idPerk2 = null;
				String idPerk3 = null;
			if (noCabang.equalsIgnoreCase("1") && sale.getKelBrg().equalsIgnoreCase("NONOIL")){
				idPerk1 = "11502";
				if (sale.getKdTrans().equalsIgnoreCase("AKS")){
					idPerk1 = "11510";
				}
				idPerk2 = "11701";
				idPerk3 = "21102";
			}
			if (noCabang.equalsIgnoreCase("2") && sale.getKelBrg().equalsIgnoreCase("NONOIL")){
				idPerk1 = "11505";
				if (sale.getKdTrans().equalsIgnoreCase("AKS")){
					idPerk1 = "11511";
				}
				idPerk2 = "11701";
				idPerk3 = "21109";
			}
			if (noCabang.equalsIgnoreCase("3") && sale.getKelBrg().equalsIgnoreCase("NONOIL")){
				idPerk1 = "11508";
				if (sale.getKdTrans().equalsIgnoreCase("AKS")){
					idPerk1 = "11512";
				}
				idPerk2 = "11701";
				idPerk3 = "21113";
			}
			if (noCabang.equalsIgnoreCase("1") && sale.getKelBrg().equalsIgnoreCase("OIL")){
				idPerk1="11503";
				idPerk2="11701";
				idPerk3="21102";
			}
			if (noCabang.equalsIgnoreCase("2") && sale.getKelBrg().equalsIgnoreCase("OIL")){
				idPerk1="11506";
				idPerk2="11701";
				idPerk3="21109";
			}
			if (noCabang.equalsIgnoreCase("3") && sale.getKelBrg().equalsIgnoreCase("OIL")){
				idPerk1="11509";
				idPerk2="11701";
				idPerk3="21113";
			}
			
			BigDecimal jumlah = sale.getJmlTrm();
			BigDecimal jumlah2 = jumlah.divideToIntegralValue(new BigDecimal(1.1));
			BigDecimal jumlah3 = jumlah.subtract(jumlah2);
			BigDecimal jumlah4 = jumlah.multiply(new BigDecimal(0.1));
			
			List<TrioJurnal> listJurnal = new ArrayList<TrioJurnal>();
			TrioJurnal jurnal = new TrioJurnal();
			jurnal.setTglJurnal(sale.getFakDate());
			jurnal.setBlc("*");
			jurnal.getTrioJurnalPK().setIdPerkiraan(idPerk1);
			jurnal.getTrioJurnalPK().setIdSub("");
			DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
			String nmPerk = getMasterFacade().getTrioMstperkiraanDao().findNamaById(idPerk1);
			jurnal.setNamaPerkiraan(nmPerk);
			jurnal.setKet(sale.getNmTran());
			jurnal.setDebet(jumlah2);
			jurnal.setKredit(new BigDecimal(0));
			jurnal.setTipe("D");
			listJurnal.add(jurnal);
			
			jurnal = new TrioJurnal();
			jurnal.setTglJurnal(sale.getFakDate());
			jurnal.setBlc("*");
			jurnal.getTrioJurnalPK().setIdPerkiraan(idPerk2);
			jurnal.getTrioJurnalPK().setIdSub("");
			DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
			nmPerk = getMasterFacade().getTrioMstperkiraanDao().findNamaById(idPerk2);
			jurnal.setNamaPerkiraan(nmPerk);
			jurnal.setKet(sale.getNmTran());
			jurnal.setDebet(jumlah3);
			jurnal.setKredit(new BigDecimal(0));
			jurnal.setTipe("D");
			listJurnal.add(jurnal);
			
			jurnal = new TrioJurnal();
			jurnal.setTglJurnal(sale.getFakDate());
			jurnal.setBlc("*");
			jurnal.getTrioJurnalPK().setIdPerkiraan(idPerk3);
			jurnal.getTrioJurnalPK().setIdSub("");
			DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
			nmPerk = getMasterFacade().getTrioMstperkiraanDao().findNamaById(idPerk3);
			jurnal.setNamaPerkiraan(nmPerk);
			jurnal.setKet(sale.getNmTran());
			jurnal.setDebet(new BigDecimal(0));
			jurnal.setKredit(jumlah);
			jurnal.setTipe("K");
			listJurnal.add(jurnal);
			
			getMasterFacade().getTrioJurnalDao().saveTransaction(listJurnal, "JME", getUserSession());
		}
		
		//end membuat jurnal untuk sale
		
		//bgn membuat jurnal untuk retur
		DatabaseContextHolder.setConnectionType(ConnectionType.ORACLEPART);
		List<TrioTarikpartretur> listRetur = getMasterFacade().getTrioTarikoracleDao()
				.getListTriotarikpartretur(kdDlr, tgl);
		
		for (TrioTarikpartretur retur : listRetur){
			
			String idPerk1 = null;
			String idPerk2 = null;
			String idPerk3 = null;
			if (noCabang.equalsIgnoreCase("1") && retur.getKelBrg().equalsIgnoreCase("NONOIL")){
				idPerk1 = "11502";
				if (retur.getKdTrans().equalsIgnoreCase("AKS")){
					idPerk1 = "11510";
				}
				idPerk2 = "11701";
				idPerk3 = "21102";
			}
			if (noCabang.equalsIgnoreCase("2") && retur.getKelBrg().equalsIgnoreCase("NONOIL")){
				idPerk1 = "11505";
				if (retur.getKdTrans().equalsIgnoreCase("AKS")){
					idPerk1 = "11511";
				}
				idPerk2 = "11701";
				idPerk3 = "21109";
			}
			if (noCabang.equalsIgnoreCase("3") && retur.getKelBrg().equalsIgnoreCase("NONOIL")){
				idPerk1 = "11508";
				if (retur.getKdTrans().equalsIgnoreCase("AKS")){
					idPerk1 = "11512";
				}
				idPerk2 = "11701";
				idPerk3 = "21113";
			}
			if (noCabang.equalsIgnoreCase("1") && retur.getKelBrg().equalsIgnoreCase("OIL")){
				idPerk1="11503";
				idPerk2="11701";
				idPerk3="21102";
			}
			if (noCabang.equalsIgnoreCase("2") && retur.getKelBrg().equalsIgnoreCase("OIL")){
				idPerk1="11506";
				idPerk2="11701";
				idPerk3="21109";
			}
			if (noCabang.equalsIgnoreCase("3") && retur.getKelBrg().equalsIgnoreCase("OIL")){
				idPerk1="11509";
				idPerk2="11701";
				idPerk3="21113";
			}
			
			BigDecimal jumlah = retur.getJmlTrm();
			BigDecimal jumlah2 = jumlah.divideToIntegralValue(new BigDecimal(1.1));
			BigDecimal jumlah3 = jumlah.subtract(jumlah2);
			BigDecimal jumla4 = jumlah.multiply(new BigDecimal(0.1));
			
			List<TrioJurnal> listJurnal = new ArrayList<TrioJurnal>();
			TrioJurnal jurnal = new TrioJurnal();
			jurnal.setTglJurnal(retur.getReturDate());
			jurnal.setBlc("*");
			jurnal.getTrioJurnalPK().setIdPerkiraan(idPerk1);
			jurnal.getTrioJurnalPK().setIdSub("");
			DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
			String nmPerk = getMasterFacade().getTrioMstperkiraanDao().findNamaById(idPerk1);
			jurnal.setNamaPerkiraan(nmPerk);
			jurnal.setKet(retur.getNmTran());
			jurnal.setDebet(new BigDecimal(0));
			jurnal.setKredit(jumlah2);
			jurnal.setTipe("K");
			listJurnal.add(jurnal);
			
			jurnal = new TrioJurnal();
			jurnal.setTglJurnal(retur.getReturDate());
			jurnal.setBlc("*");
			jurnal.getTrioJurnalPK().setIdPerkiraan(idPerk2);
			jurnal.getTrioJurnalPK().setIdSub("");
			DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
			nmPerk = getMasterFacade().getTrioMstperkiraanDao().findNamaById(idPerk2);
			jurnal.setNamaPerkiraan(nmPerk);
			jurnal.setKet(retur.getNmTran());
			jurnal.setDebet(new BigDecimal(0));
			jurnal.setKredit(jumlah3);
			jurnal.setTipe("K");
			listJurnal.add(jurnal);
			
			jurnal = new TrioJurnal();
			jurnal.setTglJurnal(retur.getReturDate());
			jurnal.setBlc("*");
			jurnal.getTrioJurnalPK().setIdPerkiraan(idPerk3);
			jurnal.getTrioJurnalPK().setIdSub("");
			DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
			nmPerk = getMasterFacade().getTrioMstperkiraanDao().findNamaById(idPerk3);
			jurnal.setNamaPerkiraan(nmPerk);
			jurnal.setKet(retur.getNmTran());
			jurnal.setDebet(jumlah);
			jurnal.setKredit(new BigDecimal(0));
			jurnal.setTipe("D");
			listJurnal.add(jurnal);
			
			getMasterFacade().getTrioJurnalDao().saveTransaction(listJurnal, "JME", getUserSession());
			
		}
		//end membuat jurnal untuk retur
		
		Messagebox.show("Proses Jurnal Memorial untuk Sale : "+listSale.size()+"\nProses Jurnal Memorial untuk Retur : "+listRetur.size());
		
	}
	
	@NotifyChange({"kodeCabang","tglTarik","listModel"})
	public void reset(){
		setKodeCabang(null);
		setTglTarik(null);
		listModel = new ListModelList<TrioTarikpartsale>();
	}

	public String getKodeCabang() {
		return kodeCabang;
	}

	public void setKodeCabang(String kodeCabang) {
		this.kodeCabang = kodeCabang;
	}

	public Date getTglTarik() {
		return tglTarik;
	}

	public void setTglTarik(Date tglTarik) {
		this.tglTarik = tglTarik;
	}

}

