package com.trio.gl.viewmodel.trn;

import java.util.Date;
import java.util.List;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;

import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioLapBukubesar;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;
import com.trio.gl.util.TrioDateConv;
import com.trio.gl.util.TrioDateUtil;

/** @author Saifi Ahmada May 31, 2013 8:56:57 AM  **/

public class TrnNeracaCoba extends TrioBasePageVM {
	
	private Date monthYear;
	
	private boolean saveNeraca;
	
	private boolean prosesSub;

	public Date getMonthYear() {
		return monthYear;
	}

	public void setMonthYear(Date monthYear) {
		this.monthYear = monthYear;
	}
	
	@NotifyChange("saveNeraca")
	@Command
	public void cek(@BindingParam("checked") boolean isCek){
		if (isCek){
			saveNeraca = true;
		}else{
			saveNeraca = false;
		}
	}
	
	@NotifyChange("prosesSub")
	@Command
	public void cek2(@BindingParam("checked") boolean isCek){
		if (isCek){
			prosesSub = true;
		}else{
			prosesSub = false;
		}
	}
	
	@NotifyChange({"monthYear"})
	@Command
	public void proses(){
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		Date tglAwal = TrioDateConv.valueOf(TrioDateUtil.getYear(monthYear), TrioDateUtil.getMonth(monthYear), 1);
		Date tglAkhir = TrioDateConv.valueOf(TrioDateUtil.getYear(monthYear), TrioDateUtil.getMonth(monthYear), TrioDateUtil.lastDayOf(tglAwal));
		
		if (monthYear == null){
			Messagebox.show("Periode Bulan Kosong");
			return;
		}
		String periode = TrioDateConv.format(monthYear, "YYYYMM");
		List<TrioLapBukubesar> list = getMasterFacade().getTrioLapBukubesarDao().getListBukuBesarByPeriode(periode);
		String tahun = periode.substring(0, 4);
		String bulan = periode.substring(4, 6);
		if (list.size() < 1){
			Messagebox.show("Laporan buku besar dengan periode [ "+periode+" ] tidak ditemukan" );
			return;
		}
		String pesan = getMasterFacade().getTrioNeracaCobaDao().prosesNeraca(periode, saveNeraca, prosesSub, getUserSession());
		Messagebox.show("Proses : " + pesan);
				
	}
	
	

}

