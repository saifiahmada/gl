package com.trio.gl.viewmodel.trn;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.zkoss.bind.annotation.Command;

import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioKumpul;
import com.trio.gl.bean.TrioLapBukubesar;
import com.trio.gl.bean.TrioMstsubperkiraan;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;

/**
 * @author Gusti Arya 9:56:39 AM Sep 3, 2013
 */

public class TrnSaldoSubPerkVM  extends TrioBasePageVM{


	private Date bulan;

	public Date getBulan() {
		if(bulan==null)
			bulan = new Date();
		return bulan;
	}

	public void setBulan(Date bulan) {
		this.bulan = bulan;
	}

	@Command
	public void proses(){
		DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);

		//clear table trioKumpul
		getMasterFacade().getTrioKumpulDao().deleteFrom();
		
		SimpleDateFormat sdf = new SimpleDateFormat("MM");
		String periode = sdf.format(getBulan());

		List<TrioLapBukubesar> lists = getMasterFacade().getTrioLapBukubesarDao().getSumSaldoSub(periode);

		TrioKumpul trioKumpul = new TrioKumpul();
		for(TrioLapBukubesar current : lists){
			trioKumpul.getTrioKumpulPK().setIdPerkiraan(current.getTrioLapBukubesarPK().getIdPerkiraan());
			trioKumpul.getTrioKumpulPK().setIdSub(current.getTrioLapBukubesarPK().getIdSub());
			trioKumpul.setTotal(current.getSaldoAwal());
			getMasterFacade().getTrioKumpulDao().save(trioKumpul, getUserSession());
		}	

		//update b_sub 
		System.out.println("substr = " + periode.substring(1));
		getMasterFacade().getTrioMstsubperkiraanDao().updateQuery(periode.substring(1));

		List<TrioMstsubperkiraan> listsBulSub = getMasterFacade().getTrioMstsubperkiraanDao()
				.getBulSub();

		String ketAwal= "";
		for(TrioMstsubperkiraan current : listsBulSub){
			
			//mark01
			if(nullConverter(current.getBulSub0()).compareTo(BigDecimal.ZERO) != 0){
				ketAwal = current.getKetSub0();
			}else if(nullConverter(current.getBulSub1()).compareTo(BigDecimal.ZERO) != 0) {
				ketAwal = current.getKetSub1();
			}else if(nullConverter(current.getBulSub2()).compareTo(BigDecimal.ZERO) != 0){
				ketAwal = current.getKetSub2();
			}else if(nullConverter(current.getBulSub3()).compareTo(BigDecimal.ZERO) != 0){
				ketAwal = current.getKetSub3();
			}else if(nullConverter(current.getBulSub4()).compareTo(BigDecimal.ZERO) != 0){
				ketAwal = current.getKetSub4();
			}else if(nullConverter(current.getBulSub5()).compareTo(BigDecimal.ZERO) != 0){
				ketAwal = current.getKetSub5();
			}else if(nullConverter(current.getBulSub6()).compareTo(BigDecimal.ZERO) != 0){
				ketAwal = current.getKetSub6();
			}else if(nullConverter(current.getBulSub7()).compareTo(BigDecimal.ZERO) != 0){
				ketAwal = current.getKetSub7();
			}else if(nullConverter(current.getBulSub8()).compareTo(BigDecimal.ZERO) != 0){
				ketAwal = current.getKetSub8();
			}else if(nullConverter(current.getBulSub9()).compareTo(BigDecimal.ZERO) != 0){
				ketAwal = current.getKetSub9();
			}else if(nullConverter(current.getBulSub10()).compareTo(BigDecimal.ZERO) != 0){
				ketAwal = current.getKetSub10();
			}else if(nullConverter(current.getBulSub11()).compareTo(BigDecimal.ZERO) != 0){
				ketAwal = current.getKetSub11();
			}

			String ketSub="";
			BigDecimal nilai1= new BigDecimal(0);
//			BigDecimal total = new BigDecimal(0);
			
			if(periode.equals("01")){
				for(TrioMstsubperkiraan current2 : listsBulSub){
					ketSub = current.getKetSub0();
					BigDecimal total = getMasterFacade().getTrioKumpulDao()
							.getTotalByIdPerk(current2.getTrioMstsubperkiraanPK().getIdPerkiraan(),current2.getTrioMstsubperkiraanPK().getIdSub());
					if(nullConverter(current2.getBulSub0()).compareTo(BigDecimal.ZERO) !=0){
						//mark03
						if(current2.getKetSub0().equalsIgnoreCase("D")){
							nilai1 = current2.getBulSub0().add(total);
							if(nilai1.compareTo(BigDecimal.ZERO) < 0){
								nilai1 = nilai1.multiply(new BigDecimal(-1));
								ketSub = "K";
							}
						}
						//mark04
						if(current2.getKetSub0().equalsIgnoreCase("K")){
							if(total.compareTo(BigDecimal.ZERO) < 0){
								total = total.multiply(new BigDecimal(-1));
								nilai1 = current2.getBulSub0().add(total);
							}else{
								nilai1 = current2.getBulSub0().subtract(total);
							}
							if(nilai1.compareTo(BigDecimal.ZERO) < 0){
								nilai1 = nilai1.multiply(new BigDecimal(-1));
								ketSub = "D";
							}
						}
						//mark05
						System.out.println("nilai1 akhir = " + nilai1);
						current2.setBulSub1(nilai1);
						current2.setKetSub1(ketSub);
						getMasterFacade().getTrioMstsubperkiraanDao().update(current2,getUserSession());
						//mark06
					}else{
						nilai1 = current2.getBulSub0().add(total);
						if(ketAwal.equalsIgnoreCase("D")){
							if(nilai1.compareTo(BigDecimal.ZERO) <0){
								nilai1 = nilai1.multiply(new BigDecimal(-1));
								ketSub="K";
							}else{
								ketSub = ketAwal;
							}
						}
						if(ketAwal.equalsIgnoreCase("K")){
							if(nilai1.compareTo(BigDecimal.ZERO) < 0){
								nilai1 = nilai1.multiply(new BigDecimal(-1));
								ketSub="K";
							}else{
								ketSub=ketAwal;
							}
						}
						current2.setBulSub1(nilai1);
						current2.setKetSub1(ketSub);
						getMasterFacade().getTrioMstsubperkiraanDao().update(current2, getUserSession());
					}
				}
			}
			
			if(periode.equals("02")){
				for(TrioMstsubperkiraan current2 : listsBulSub){
					ketSub = current.getKetSub1();
					BigDecimal total = getMasterFacade().getTrioKumpulDao()
							.getTotalByIdPerk(current2.getTrioMstsubperkiraanPK().getIdPerkiraan(),current2.getTrioMstsubperkiraanPK().getIdSub());
					if(nullConverter(current2.getBulSub1()).compareTo(BigDecimal.ZERO) !=0){
						//mark03
						if(current2.getKetSub1().equalsIgnoreCase("D")){
							nilai1 = current2.getBulSub1().add(total);
							if(nilai1.compareTo(BigDecimal.ZERO) < 0){
								nilai1 = nilai1.multiply(new BigDecimal(-1));
								ketSub = "K";
							}
						}
						//mark04
						if(current2.getKetSub1().equalsIgnoreCase("K")){
							if(total.compareTo(BigDecimal.ZERO) < 0){
								total = total.multiply(new BigDecimal(-1));
								nilai1 = current2.getBulSub1().add(total);
							}else{
								nilai1 = current2.getBulSub1().subtract(total);
							}
							if(nilai1.compareTo(BigDecimal.ZERO) < 0){
								nilai1 = nilai1.multiply(new BigDecimal(-1));
								ketSub = "D";
							}
						}
						//mark05
						System.out.println("nilai1 akhir = " + nilai1);
						current2.setBulSub2(nilai1);
						current2.setKetSub2(ketSub);
						getMasterFacade().getTrioMstsubperkiraanDao().update(current2, getUserSession());
						//mark06
					}else{
						nilai1 = current2.getBulSub1().add(total);
						if(ketAwal.equalsIgnoreCase("D")){
							if(nilai1.compareTo(BigDecimal.ZERO) <0){
								nilai1 = nilai1.multiply(new BigDecimal(-1));
								ketSub="K";
							}else{
								ketSub = ketAwal;
							}
						}
						if(ketAwal.equalsIgnoreCase("K")){
							if(nilai1.compareTo(BigDecimal.ZERO) < 0){
								nilai1 = nilai1.multiply(new BigDecimal(-1));
								ketSub="K";
							}else{
								ketSub=ketAwal;
							}
						}
						current2.setBulSub2(nilai1);
						current2.setKetSub2(ketSub);
						getMasterFacade().getTrioMstsubperkiraanDao().update(current2, getUserSession());
					}
				}
			}
			
			if(periode.equals("03")){
				for(TrioMstsubperkiraan current2 : listsBulSub){
					ketSub = current.getKetSub2();
					BigDecimal total = getMasterFacade().getTrioKumpulDao()
							.getTotalByIdPerk(current2.getTrioMstsubperkiraanPK().getIdPerkiraan(),current2.getTrioMstsubperkiraanPK().getIdSub());
					if(nullConverter(current2.getBulSub2()).compareTo(BigDecimal.ZERO) !=0){
						//mark03
						if(current2.getKetSub2().equalsIgnoreCase("D")){
							nilai1 = current2.getBulSub2().add(total);
							if(nilai1.compareTo(BigDecimal.ZERO) < 0){
								nilai1 = nilai1.multiply(new BigDecimal(-1));
								ketSub = "K";
							}
						}
						//mark04
						if(current2.getKetSub2().equalsIgnoreCase("K")){
							if(total.compareTo(BigDecimal.ZERO) < 0){
								total = total.multiply(new BigDecimal(-1));
								nilai1 = current2.getBulSub2().add(total);
							}else{
								nilai1 = current2.getBulSub2().subtract(total);
							}
							if(nilai1.compareTo(BigDecimal.ZERO) < 0){
								nilai1 = nilai1.multiply(new BigDecimal(-1));
								ketSub = "D";
							}
						}
						//mark05
						System.out.println("nilai1 akhir = " + nilai1);
						current2.setBulSub3(nilai1);
						current2.setKetSub3(ketSub);
						getMasterFacade().getTrioMstsubperkiraanDao().update(current2, getUserSession());
						//mark06
					}else{
						nilai1 = current2.getBulSub2().add(total);
						if(ketAwal.equalsIgnoreCase("D")){
							if(nilai1.compareTo(BigDecimal.ZERO) <0){
								nilai1 = nilai1.multiply(new BigDecimal(-1));
								ketSub="K";
							}else{
								ketSub = ketAwal;
							}
						}
						if(ketAwal.equalsIgnoreCase("K")){
							if(nilai1.compareTo(BigDecimal.ZERO) < 0){
								nilai1 = nilai1.multiply(new BigDecimal(-1));
								ketSub="K";
							}else{
								ketSub=ketAwal;
							}
						}
						current2.setBulSub3(nilai1);
						current2.setKetSub3(ketSub);
						getMasterFacade().getTrioMstsubperkiraanDao().update(current2, getUserSession());
					}
				}
			}
			
			if(periode.equals("04")){
				for(TrioMstsubperkiraan current2 : listsBulSub){
					ketSub = current.getKetSub3();
					BigDecimal total = getMasterFacade().getTrioKumpulDao()
							.getTotalByIdPerk(current2.getTrioMstsubperkiraanPK().getIdPerkiraan(),current2.getTrioMstsubperkiraanPK().getIdSub());
					if(nullConverter(current2.getBulSub3()).compareTo(BigDecimal.ZERO) !=0){
						//mark03
						if(current2.getKetSub3().equalsIgnoreCase("D")){
							nilai1 = current2.getBulSub3().add(total);
							if(nilai1.compareTo(BigDecimal.ZERO) < 0){
								nilai1 = nilai1.multiply(new BigDecimal(-1));
								ketSub = "K";
							}
						}
						//mark04
						if(current2.getKetSub3().equalsIgnoreCase("K")){
							if(total.compareTo(BigDecimal.ZERO) < 0){
								total = total.multiply(new BigDecimal(-1));
								nilai1 = current2.getBulSub3().add(total);
							}else{
								nilai1 = current2.getBulSub3().subtract(total);
							}
							if(nilai1.compareTo(BigDecimal.ZERO) < 0){
								nilai1 = nilai1.multiply(new BigDecimal(-1));
								ketSub = "D";
							}
						}
						//mark05
						System.out.println("nilai1 akhir = " + nilai1);
						current2.setBulSub4(nilai1);
						current2.setKetSub4(ketSub);
						getMasterFacade().getTrioMstsubperkiraanDao().update(current2, getUserSession());
						//mark06
					}else{
						nilai1 = current2.getBulSub3().add(total);
						if(ketAwal.equalsIgnoreCase("D")){
							if(nilai1.compareTo(BigDecimal.ZERO) <0){
								nilai1 = nilai1.multiply(new BigDecimal(-1));
								ketSub="K";
							}else{
								ketSub = ketAwal;
							}
						}
						if(ketAwal.equalsIgnoreCase("K")){
							if(nilai1.compareTo(BigDecimal.ZERO) < 0){
								nilai1 = nilai1.multiply(new BigDecimal(-1));
								ketSub="K";
							}else{
								ketSub=ketAwal;
							}
						}
						current2.setBulSub4(nilai1);
						current2.setKetSub4(ketSub);
						getMasterFacade().getTrioMstsubperkiraanDao().update(current2, getUserSession());
					}
				}
			}
			
			if(periode.equals("05")){
				for(TrioMstsubperkiraan current2 : listsBulSub){
					ketSub = current.getKetSub4();
					BigDecimal total = getMasterFacade().getTrioKumpulDao()
							.getTotalByIdPerk(current2.getTrioMstsubperkiraanPK().getIdPerkiraan(),current2.getTrioMstsubperkiraanPK().getIdSub());
					if(nullConverter(current2.getBulSub4()).compareTo(BigDecimal.ZERO) !=0){
						//mark03
						if(current2.getKetSub4().equalsIgnoreCase("D")){
							nilai1 = current2.getBulSub4().add(total);
							if(nilai1.compareTo(BigDecimal.ZERO) < 0){
								nilai1 = nilai1.multiply(new BigDecimal(-1));
								ketSub = "K";
							}
						}
						//mark04
						if(current2.getKetSub4().equalsIgnoreCase("K")){
							if(total.compareTo(BigDecimal.ZERO) < 0){
								total = total.multiply(new BigDecimal(-1));
								nilai1 = current2.getBulSub4().add(total);
							}else{
								nilai1 = current2.getBulSub4().subtract(total);
							}
							if(nilai1.compareTo(BigDecimal.ZERO) < 0){
								nilai1 = nilai1.multiply(new BigDecimal(-1));
								ketSub = "D";
							}
						}
						//mark05
						System.out.println("nilai1 akhir = " + nilai1);
						current2.setBulSub5(nilai1);
						current2.setKetSub5(ketSub);
						getMasterFacade().getTrioMstsubperkiraanDao().update(current2, getUserSession());
						//mark06
					}else{
						nilai1 = current2.getBulSub4().add(total);
						if(ketAwal.equalsIgnoreCase("D")){
							if(nilai1.compareTo(BigDecimal.ZERO) <0){
								nilai1 = nilai1.multiply(new BigDecimal(-1));
								ketSub="K";
							}else{
								ketSub = ketAwal;
							}
						}
						if(ketAwal.equalsIgnoreCase("K")){
							if(nilai1.compareTo(BigDecimal.ZERO) < 0){
								nilai1 = nilai1.multiply(new BigDecimal(-1));
								ketSub="K";
							}else{
								ketSub=ketAwal;
							}
						}
						current2.setBulSub5(nilai1);
						current2.setKetSub5(ketSub);
						getMasterFacade().getTrioMstsubperkiraanDao().update(current2, getUserSession());
					}
				}
			}
			
			if(periode.equals("06")){
				for(TrioMstsubperkiraan current2 : listsBulSub){
					ketSub = current.getKetSub5();
					BigDecimal total = getMasterFacade().getTrioKumpulDao()
							.getTotalByIdPerk(current2.getTrioMstsubperkiraanPK().getIdPerkiraan(),current2.getTrioMstsubperkiraanPK().getIdSub());
					if(nullConverter(current2.getBulSub5()).compareTo(BigDecimal.ZERO) !=0){
						//mark03
						if(current2.getKetSub5().equalsIgnoreCase("D")){
							nilai1 = current2.getBulSub5().add(total);
							if(nilai1.compareTo(BigDecimal.ZERO) < 0){
								nilai1 = nilai1.multiply(new BigDecimal(-1));
								ketSub = "K";
							}
						}
						//mark04
						if(current2.getKetSub5().equalsIgnoreCase("K")){
							if(total.compareTo(BigDecimal.ZERO) < 0){
								total = total.multiply(new BigDecimal(-1));
								nilai1 = current2.getBulSub5().add(total);
							}else{
								nilai1 = current2.getBulSub5().subtract(total);
							}
							if(nilai1.compareTo(BigDecimal.ZERO) < 0){
								nilai1 = nilai1.multiply(new BigDecimal(-1));
								ketSub = "D";
							}
						}
						//mark05
						System.out.println("nilai1 akhir = " + nilai1);
						current2.setBulSub6(nilai1);
						current2.setKetSub6(ketSub);
						getMasterFacade().getTrioMstsubperkiraanDao().update(current2, getUserSession());
						//mark06
					}else{
						nilai1 = current2.getBulSub5().add(total);
						if(ketAwal.equalsIgnoreCase("D")){
							if(nilai1.compareTo(BigDecimal.ZERO) <0){
								nilai1 = nilai1.multiply(new BigDecimal(-1));
								ketSub="K";
							}else{
								ketSub = ketAwal;
							}
						}
						if(ketAwal.equalsIgnoreCase("K")){
							if(nilai1.compareTo(BigDecimal.ZERO) < 0){
								nilai1 = nilai1.multiply(new BigDecimal(-1));
								ketSub="K";
							}else{
								ketSub=ketAwal;
							}
						}
						current2.setBulSub6(nilai1);
						current2.setKetSub6(ketSub);
						getMasterFacade().getTrioMstsubperkiraanDao().update(current2, getUserSession());
					}
				}
			}
			
			//mark02
			if(periode.equals("07")){
				for(TrioMstsubperkiraan current2 : listsBulSub){
					ketSub = current.getKetSub6();
					BigDecimal total = getMasterFacade().getTrioKumpulDao()
							.getTotalByIdPerk(current2.getTrioMstsubperkiraanPK().getIdPerkiraan(),current2.getTrioMstsubperkiraanPK().getIdSub());
					if(nullConverter(current2.getBulSub6()).compareTo(BigDecimal.ZERO) !=0){
						//mark03
						if(current2.getKetSub6().equalsIgnoreCase("D")){
							nilai1 = current2.getBulSub6().add(total);
							if(nilai1.compareTo(BigDecimal.ZERO) < 0){
								nilai1 = nilai1.multiply(new BigDecimal(-1));
								ketSub = "K";
							}
						}
						//mark04
						if(current2.getKetSub6().equalsIgnoreCase("K")){
							if(total.compareTo(BigDecimal.ZERO) < 0){
								total = total.multiply(new BigDecimal(-1));
								nilai1 = current2.getBulSub6().add(total);
							}else{
								nilai1 = current2.getBulSub6().subtract(total);
							}
							if(nilai1.compareTo(BigDecimal.ZERO) < 0){
								nilai1 = nilai1.multiply(new BigDecimal(-1));
								ketSub = "D";
							}
						}
						//mark05
						System.out.println("nilai1 akhir = " + nilai1);
						current2.setBulSub7(nilai1);
						current2.setKetSub7(ketSub);
						getMasterFacade().getTrioMstsubperkiraanDao().update(current2, getUserSession());
						//mark06
					}else{
						nilai1 = current2.getBulSub6().add(total);
						if(ketAwal.equalsIgnoreCase("D")){
							if(nilai1.compareTo(BigDecimal.ZERO) <0){
								nilai1 = nilai1.multiply(new BigDecimal(-1));
								ketSub="K";
							}else{
								ketSub = ketAwal;
							}
						}
						if(ketAwal.equalsIgnoreCase("K")){
							if(nilai1.compareTo(BigDecimal.ZERO) < 0){
								nilai1 = nilai1.multiply(new BigDecimal(-1));
								ketSub="K";
							}else{
								ketSub=ketAwal;
							}
						}
						current2.setBulSub7(nilai1);
						current2.setKetSub7(ketSub);
						getMasterFacade().getTrioMstsubperkiraanDao().update(current2, getUserSession());
					}
				}
			}
			
			
			if(periode.equals("08")){
				for(TrioMstsubperkiraan current2 : listsBulSub){
					ketSub = current.getKetSub7();
					BigDecimal total = getMasterFacade().getTrioKumpulDao()
							.getTotalByIdPerk(current2.getTrioMstsubperkiraanPK().getIdPerkiraan(),current2.getTrioMstsubperkiraanPK().getIdSub());
					if(nullConverter(current2.getBulSub7()).compareTo(BigDecimal.ZERO) !=0){
						//mark03
						if(current2.getKetSub7().equalsIgnoreCase("D")){
							nilai1 = current2.getBulSub7().add(total);
							if(nilai1.compareTo(BigDecimal.ZERO) < 0){
								nilai1 = nilai1.multiply(new BigDecimal(-1));
								ketSub = "K";
							}
						}
						//mark04
						if(current2.getKetSub7().equalsIgnoreCase("K")){
							if(total.compareTo(BigDecimal.ZERO) < 0){
								total = total.multiply(new BigDecimal(-1));
								nilai1 = current2.getBulSub7().add(total);
							}else{
								nilai1 = current2.getBulSub7().subtract(total);
							}
							if(nilai1.compareTo(BigDecimal.ZERO) < 0){
								nilai1 = nilai1.multiply(new BigDecimal(-1));
								ketSub = "D";
							}
						}
						//mark05
						System.out.println("nilai1 akhir = " + nilai1);
						current2.setBulSub8(nilai1);
						current2.setKetSub8(ketSub);
						getMasterFacade().getTrioMstsubperkiraanDao().update(current2, getUserSession());
						//mark06
					}else{
						nilai1 = current2.getBulSub7().add(total);
						if(ketAwal.equalsIgnoreCase("D")){
							if(nilai1.compareTo(BigDecimal.ZERO) <0){
								nilai1 = nilai1.multiply(new BigDecimal(-1));
								ketSub="K";
							}else{
								ketSub = ketAwal;
							}
						}
						if(ketAwal.equalsIgnoreCase("K")){
							if(nilai1.compareTo(BigDecimal.ZERO) < 0){
								nilai1 = nilai1.multiply(new BigDecimal(-1));
								ketSub="K";
							}else{
								ketSub=ketAwal;
							}
						}
						current2.setBulSub8(nilai1);
						current2.setKetSub8(ketSub);
						getMasterFacade().getTrioMstsubperkiraanDao().update(current2, getUserSession());
					}
				}
			}
			
			if(periode.equals("09")){
				for(TrioMstsubperkiraan current2 : listsBulSub){
					ketSub = current.getKetSub8();
					BigDecimal total = getMasterFacade().getTrioKumpulDao()
							.getTotalByIdPerk(current2.getTrioMstsubperkiraanPK().getIdPerkiraan(),current2.getTrioMstsubperkiraanPK().getIdSub());
					if(nullConverter(current2.getBulSub8()).compareTo(BigDecimal.ZERO) !=0){
						//mark03
						if(current2.getKetSub8().equalsIgnoreCase("D")){
							nilai1 = current2.getBulSub8().add(total);
							if(nilai1.compareTo(BigDecimal.ZERO) < 0){
								nilai1 = nilai1.multiply(new BigDecimal(-1));
								ketSub = "K";
							}
						}
						//mark04
						if(current2.getKetSub8().equalsIgnoreCase("K")){
							if(total.compareTo(BigDecimal.ZERO) < 0){
								total = total.multiply(new BigDecimal(-1));
								nilai1 = current2.getBulSub8().add(total);
							}else{
								nilai1 = current2.getBulSub8().subtract(total);
							}
							if(nilai1.compareTo(BigDecimal.ZERO) < 0){
								nilai1 = nilai1.multiply(new BigDecimal(-1));
								ketSub = "D";
							}
						}
						//mark05
						System.out.println("nilai1 akhir = " + nilai1);
						current2.setBulSub9(nilai1);
						current2.setKetSub9(ketSub);
						getMasterFacade().getTrioMstsubperkiraanDao().update(current2, getUserSession());
						//mark06
					}else{
						nilai1 = current2.getBulSub8().add(total);
						if(ketAwal.equalsIgnoreCase("D")){
							if(nilai1.compareTo(BigDecimal.ZERO) <0){
								nilai1 = nilai1.multiply(new BigDecimal(-1));
								ketSub="K";
							}else{
								ketSub = ketAwal;
							}
						}
						if(ketAwal.equalsIgnoreCase("K")){
							if(nilai1.compareTo(BigDecimal.ZERO) < 0){
								nilai1 = nilai1.multiply(new BigDecimal(-1));
								ketSub="K";
							}else{
								ketSub=ketAwal;
							}
						}
						current2.setBulSub9(nilai1);
						current2.setKetSub9(ketSub);
						getMasterFacade().getTrioMstsubperkiraanDao().update(current2, getUserSession());
					}
				}
			}
			
			if(periode.equals("10")){
				for(TrioMstsubperkiraan current2 : listsBulSub){
					ketSub = current.getKetSub9();
					BigDecimal total = getMasterFacade().getTrioKumpulDao()
							.getTotalByIdPerk(current2.getTrioMstsubperkiraanPK().getIdPerkiraan(),current2.getTrioMstsubperkiraanPK().getIdSub());
					if(nullConverter(current2.getBulSub9()).compareTo(BigDecimal.ZERO) !=0){
						//mark03
						if(current2.getKetSub9().equalsIgnoreCase("D")){
							nilai1 = current2.getBulSub9().add(total);
							if(nilai1.compareTo(BigDecimal.ZERO) < 0){
								nilai1 = nilai1.multiply(new BigDecimal(-1));
								ketSub = "K";
							}
						}
						//mark04
						if(current2.getKetSub9().equalsIgnoreCase("K")){
							if(total.compareTo(BigDecimal.ZERO) < 0){
								total = total.multiply(new BigDecimal(-1));
								nilai1 = current2.getBulSub9().add(total);
							}else{
								nilai1 = current2.getBulSub9().subtract(total);
							}
							if(nilai1.compareTo(BigDecimal.ZERO) < 0){
								nilai1 = nilai1.multiply(new BigDecimal(-1));
								ketSub = "D";
							}
						}
						//mark05
						System.out.println("nilai1 akhir = " + nilai1);
						current2.setBulSub10(nilai1);
						current2.setKetSub10(ketSub);
						getMasterFacade().getTrioMstsubperkiraanDao().update(current2, getUserSession());
						//mark06
					}else{
						nilai1 = current2.getBulSub9().add(total);
						if(ketAwal.equalsIgnoreCase("D")){
							if(nilai1.compareTo(BigDecimal.ZERO) <0){
								nilai1 = nilai1.multiply(new BigDecimal(-1));
								ketSub="K";
							}else{
								ketSub = ketAwal;
							}
						}
						if(ketAwal.equalsIgnoreCase("K")){
							if(nilai1.compareTo(BigDecimal.ZERO) < 0){
								nilai1 = nilai1.multiply(new BigDecimal(-1));
								ketSub="K";
							}else{
								ketSub=ketAwal;
							}
						}
						current2.setBulSub10(nilai1);
						current2.setKetSub10(ketSub);
						getMasterFacade().getTrioMstsubperkiraanDao().update(current2, getUserSession());
					}
				}
			}
			
			if(periode.equals("11")){
				for(TrioMstsubperkiraan current2 : listsBulSub){
					ketSub = current.getKetSub10();
					BigDecimal total = getMasterFacade().getTrioKumpulDao()
							.getTotalByIdPerk(current2.getTrioMstsubperkiraanPK().getIdPerkiraan(),current2.getTrioMstsubperkiraanPK().getIdSub());
					if(nullConverter(current2.getBulSub10()).compareTo(BigDecimal.ZERO) !=0){
						//mark03
						if(current2.getKetSub10().equalsIgnoreCase("D")){
							nilai1 = current2.getBulSub10().add(total);
							if(nilai1.compareTo(BigDecimal.ZERO) < 0){
								nilai1 = nilai1.multiply(new BigDecimal(-1));
								ketSub = "K";
							}
						}
						//mark04
						if(current2.getKetSub10().equalsIgnoreCase("K")){
							if(total.compareTo(BigDecimal.ZERO) < 0){
								total = total.multiply(new BigDecimal(-1));
								nilai1 = current2.getBulSub10().add(total);
							}else{
								nilai1 = current2.getBulSub10().subtract(total);
							}
							if(nilai1.compareTo(BigDecimal.ZERO) < 0){
								nilai1 = nilai1.multiply(new BigDecimal(-1));
								ketSub = "D";
							}
						}
						//mark05
						System.out.println("nilai1 akhir = " + nilai1);
						current2.setBulSub11(nilai1);
						current2.setKetSub11(ketSub);
						getMasterFacade().getTrioMstsubperkiraanDao().update(current2, getUserSession());
						//mark06
					}else{
						nilai1 = current2.getBulSub10().add(total);
						if(ketAwal.equalsIgnoreCase("D")){
							if(nilai1.compareTo(BigDecimal.ZERO) <0){
								nilai1 = nilai1.multiply(new BigDecimal(-1));
								ketSub="K";
							}else{
								ketSub = ketAwal;
							}
						}
						if(ketAwal.equalsIgnoreCase("K")){
							if(nilai1.compareTo(BigDecimal.ZERO) < 0){
								nilai1 = nilai1.multiply(new BigDecimal(-1));
								ketSub="K";
							}else{
								ketSub=ketAwal;
							}
						}
						current2.setBulSub11(nilai1);
						current2.setKetSub11(ketSub);
						getMasterFacade().getTrioMstsubperkiraanDao().update(current2, getUserSession());
					}
				}
			}
			
			
			if(periode.equals("12")){
				for(TrioMstsubperkiraan current2 : listsBulSub){
					ketSub = current.getKetSub11();
					BigDecimal total = getMasterFacade().getTrioKumpulDao()
							.getTotalByIdPerk(current2.getTrioMstsubperkiraanPK().getIdPerkiraan(),current2.getTrioMstsubperkiraanPK().getIdSub());
					if(nullConverter(current2.getBulSub11()).compareTo(BigDecimal.ZERO) !=0){
						//mark03
						if(current2.getKetSub11().equalsIgnoreCase("D")){
							nilai1 = current2.getBulSub11().add(total);
							if(nilai1.compareTo(BigDecimal.ZERO) < 0){
								nilai1 = nilai1.multiply(new BigDecimal(-1));
								ketSub = "K";
							}
						}
						//mark04
						if(current2.getKetSub11().equalsIgnoreCase("K")){
							if(total.compareTo(BigDecimal.ZERO) < 0){
								total = total.multiply(new BigDecimal(-1));
								nilai1 = current2.getBulSub11().add(total);
							}else{
								nilai1 = current2.getBulSub11().subtract(total);
							}
							if(nilai1.compareTo(BigDecimal.ZERO) < 0){
								nilai1 = nilai1.multiply(new BigDecimal(-1));
								ketSub = "D";
							}
						}
						//mark05
						System.out.println("nilai1 akhir = " + nilai1);
						current2.setBulSub12(nilai1);
						current2.setKetSub12(ketSub);
						getMasterFacade().getTrioMstsubperkiraanDao().update(current2, getUserSession());
						//mark06
					}else{
						nilai1 = current2.getBulSub11().add(total);
						if(ketAwal.equalsIgnoreCase("D")){
							if(nilai1.compareTo(BigDecimal.ZERO) <0){
								nilai1 = nilai1.multiply(new BigDecimal(-1));
								ketSub="K";
							}else{
								ketSub = ketAwal;
							}
						}
						if(ketAwal.equalsIgnoreCase("K")){
							if(nilai1.compareTo(BigDecimal.ZERO) < 0){
								nilai1 = nilai1.multiply(new BigDecimal(-1));
								ketSub="K";
							}else{
								ketSub=ketAwal;
							}
						}
						current2.setBulSub12(nilai1);
						current2.setKetSub12(ketSub);
						getMasterFacade().getTrioMstsubperkiraanDao().update(current2, getUserSession());
					}
				}
			}
		}
	}

	public BigDecimal nullConverter(BigDecimal bd){
		if(bd == null){
			return new BigDecimal(0);
		}else{
			return bd;
		}
	}
}
