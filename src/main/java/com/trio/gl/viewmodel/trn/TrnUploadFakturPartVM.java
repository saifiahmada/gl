package com.trio.gl.viewmodel.trn;

import java.io.File;
import java.io.IOException;
import java.util.Vector;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.io.Files;
import org.zkoss.util.media.Media;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;

import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioMstkonfigcabang;
import com.trio.gl.bean.TrioMstkonfigurasi;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;
import com.trio.gl.util.TrioFileConv;

/** @author Saifi Ahmada Jul 10, 2013 10:28:40 AM  **/

public class TrnUploadFakturPartVM extends TrioBasePageVM {
	
	private TrioMstkonfigcabang config;
	
	private ListModelList<TrioMstkonfigcabang> configCabangList;
	
	public TrioMstkonfigcabang getConfig() {
		if (config == null) config = new TrioMstkonfigcabang();
		return config;
	}

	public void setConfig(TrioMstkonfigcabang config) {
		this.config = config;
	}

	public ListModelList<TrioMstkonfigcabang> getConfigCabangList() {
		if (configCabangList == null) {
			configCabangList = new ListModelList<TrioMstkonfigcabang>();
			DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
			configCabangList.addAll(getMasterFacade().getTrioMstkonfigcabangDao().getListCabangByContext("PART"));
		}
		return configCabangList;
	}

	public void setConfigCabangList(
			ListModelList<TrioMstkonfigcabang> configCabangList) {
		this.configCabangList = configCabangList;
	}

	@Command
	public void getUploadedFile(@BindingParam("media") Media media){
		String pesan = "";
		File fileCSV = null;
		File destCSV = null;
		
		if (config.getTrioMstkonfigcabangPK().getKdDlr() == null ||
				config.getTrioMstkonfigcabangPK().getKdDlr().equalsIgnoreCase("")){
			Messagebox.show("Pilih Kode Cabang");
			return;
		}
		
		TrioMstkonfigurasi csv = getMasterFacade().getTrioMstkonfigurasiDao().findById("114");
		if (csv == null){
			Messagebox.show("TrioMstkonfigurasi @DIR_GL_CSV tidak ada");
			return;
		}
		
		if (media != null) {
			String fileName = media.getName();
			int le = fileName.length();
			fileName = fileName.substring(le-4, le);
			
			if (fileName.equalsIgnoreCase("RSPF")){
				
				destCSV = new File(csv.getNilai()+media.getName());
				try {
					Files.copy(destCSV, media.getStreamData());
					fileCSV = new File(csv.getNilai()+media.getName());
					
					Vector<String> rows = TrioFileConv.castToStringVector(fileCSV);
					
					Vector<String[]> vectorString = new Vector<String[]>();
					String baris = null;
					String[] tempString = null;
					for (String row : rows){
						baris = row;
						String tmpBaris = new String();
						tmpBaris = baris + "E";
						String[] contents = null;
						contents = tmpBaris.split(";");
						
						tempString = new String[contents.length - 1];
						
						for (int i = 0; i< contents.length - 1; i++){
							tempString[i] = contents[i];
						}
						
						vectorString.add(tempString);
					}
					String kdDlr = config.getTrioMstkonfigcabangPK().getKdDlr();
					String noCabang = config.getNoCabang();
					DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
					pesan = getMasterFacade().getTrioStokIntransitDao().prosesUploadFile(vectorString, kdDlr, noCabang, getUserSession());
					
				} catch (IOException e) { 
					pesan = "Error Copy File";
					e.printStackTrace();
				}
				
			}else{
				pesan = "File Tidak Cocok";
			}
			
		}else{
			pesan = "File Kosong"; 
			
		}
		
		Messagebox.show(""+pesan);
	}
	

}

