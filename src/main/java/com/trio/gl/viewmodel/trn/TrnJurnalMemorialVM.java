package com.trio.gl.viewmodel.trn;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.Validator;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.validator.AbstractValidator;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;

import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioJurnal;
import com.trio.gl.bean.TrioMstperkiraan;
import com.trio.gl.bean.TrioMstsubperkiraan;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;

/** @author Saifi Ahmada Mar 14, 2013 3:03:23 PM  **/

public class TrnJurnalMemorialVM extends TrioBasePageVM {
	
	
	private Date todayDate;
	
	private String ketJurnal,ketJurnal2;
	
	private TrioJurnal current;
	
	private ListModelList<TrioJurnal> listModel = new ListModelList<TrioJurnal>();
	
	private TrioMstperkiraan selectedPerkiraan;
	
	private TrioMstsubperkiraan selectedSubPerkiraan;
	
	private List<TrioMstsubperkiraan> subPerkiraans;
	
	private BigDecimal debet,kredit,totalKredit, totalDebet, selisih;
	
	private ListModelList<TrioJurnal> listModelChecked = new ListModelList<TrioJurnal>();
	
	private boolean isFocus;
	
	public boolean getFocus(){
		return this.isFocus;
	}
	
	
	public BigDecimal getDebet() {
		if (debet == null) debet = new BigDecimal(0);
		return debet;
	}

	public void setDebet(BigDecimal debet) {
		this.debet = debet;
	}

	public BigDecimal getKredit() {
		if (kredit == null) kredit = new BigDecimal(0);
		return kredit;
	}

	public void setKredit(BigDecimal kredit) {
		this.kredit = kredit;
	}

	public BigDecimal getSelisih() {
		if (selisih == null) selisih = new BigDecimal(0);
		return selisih;
	}

	public void setSelisih(BigDecimal selisih) {
		this.selisih = selisih;
	}

	public BigDecimal getTotalKredit() {
		if (totalKredit == null) totalKredit = new BigDecimal(0);
		return totalKredit;
	}

	public void setTotalKredit(BigDecimal totalKredit) {
		this.totalKredit = totalKredit;
	}

	public BigDecimal getTotalDebet() {
		if (totalDebet == null) totalDebet = new BigDecimal(0);
		return totalDebet;
	}

	public void setTotalDebet(BigDecimal totalDebet) {
		this.totalDebet = totalDebet;
	}

	public String getKetJurnal() {
		return ketJurnal;
	}

	public void setKetJurnal(String ketJurnal) {
		this.ketJurnal = ketJurnal;
	}

	public String getKetJurnal2() {
		return ketJurnal2;
	}

	public void setKetJurnal2(String ketJurnal2) {
		this.ketJurnal2 = ketJurnal2;
	}

	public Date getTodayDate() {
		if (todayDate == null) todayDate = new Date();
		return todayDate;
	}

	public void setTodayDate(Date todayDate) {
		this.todayDate = todayDate;
	}

	public TrioMstsubperkiraan getSelectedSubPerkiraan() {
		if (selectedSubPerkiraan == null) selectedSubPerkiraan = new TrioMstsubperkiraan();
		return selectedSubPerkiraan;
	}

	public void setSelectedSubPerkiraan(TrioMstsubperkiraan selectedSubPerkiraan) {
		this.selectedSubPerkiraan = selectedSubPerkiraan;
	}

	public TrioMstperkiraan getSelectedPerkiraan() {
		if (selectedPerkiraan == null) selectedPerkiraan = new TrioMstperkiraan();
		return selectedPerkiraan;
	}

	public void setSelectedPerkiraan(TrioMstperkiraan selectedPerkiraan) {
		this.selectedPerkiraan = selectedPerkiraan;
	}

	public TrioJurnal getCurrent() {
		if (current == null) current = new TrioJurnal();
		return current;
	}

	public void setCurrent(TrioJurnal current) {
		this.current = current;
	}

	public ListModelList<TrioJurnal> getListModel() {
		return listModel;
	}

	public void setListModel(ListModelList<TrioJurnal> listModel) {
		this.listModel = listModel;
	}
	
	public ListModelList<TrioJurnal> getListModelChecked() {
		return listModelChecked;
	}

	public void setListModelChecked(ListModelList<TrioJurnal> listModelChecked) {
		this.listModelChecked = listModelChecked;
	}

	public List<TrioMstperkiraan> getPerkiraans(){
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		return getMasterFacade().getTrioMstperkiraanDao().findAll();
	}
	
	public List<TrioMstsubperkiraan> getSubPerkiraans(){
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		
//		DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);
		if (getSelectedPerkiraan().getTrioMstperkiraanPK().getIdPerkiraan() != null){
			System.out.println("dan id = "+getSelectedPerkiraan().getTrioMstperkiraanPK().getIdPerkiraan());
			subPerkiraans = getMasterFacade().getTrioMstsubperkiraanDao()
					.findByIdPerkiraan(getSelectedPerkiraan().getTrioMstperkiraanPK().getIdPerkiraan());
		}else{
			subPerkiraans = new ArrayList<TrioMstsubperkiraan>();
		}
		return subPerkiraans;
	}
	
	public void setSubPerkiraans(List<TrioMstsubperkiraan> subPerkiraans) {
		this.subPerkiraans = subPerkiraans;
	}
	
	@NotifyChange({"subPerkiraans","selectedPerkiraan","kredit","debet"})
	@Command("showSelectedPerkiraan")
	public void showSelectedPerkiraan(){
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		String idPerk = getSelectedPerkiraan().getTrioMstperkiraanPK().getIdPerkiraan();
		System.out.println("Id Perkiraan show "+idPerk);
		subPerkiraans = getMasterFacade().getTrioMstsubperkiraanDao().findByIdPerkiraan(idPerk);
		if (getSelisih().compareTo(new BigDecimal(0)) == 1){
			// transaksi sebelumnya debet,
			kredit = getSelisih();
		}else if (getSelisih().compareTo(new BigDecimal(0)) == -1) {
			// transaksi sebelumnya kredit,
			debet = getSelisih().abs();
		}else {
			kredit = new BigDecimal(0);
			debet = new BigDecimal(0);
		}
	}
	
	@NotifyChange({"listModel","current","selectedSubPerkiraan","subPerkiraans","totalDebet","totalKredit","selisih","debet","kredit","focus"})
	@Command("addJurnal")
	public void addJurnal(){
		
		getCurrent().setDebet(getDebet());
		getCurrent().setKredit(getKredit());
		
		if (getSelectedPerkiraan().getNamaPerkiraan() != null){
			getCurrent().setNamaPerkiraan(getSelectedPerkiraan().getNamaPerkiraan());
		}
		if (getSelectedPerkiraan().getTrioMstperkiraanPK().getIdPerkiraan() != null){
			getCurrent().getTrioJurnalPK().setIdPerkiraan(getSelectedPerkiraan().getTrioMstperkiraanPK().getIdPerkiraan());
		}
		if (getSelectedSubPerkiraan().getTrioMstsubperkiraanPK().getIdSub() != null){
			getCurrent().getTrioJurnalPK().setIdSub(getSelectedSubPerkiraan().getTrioMstsubperkiraanPK().getIdSub());
		}else {
			getCurrent().getTrioJurnalPK().setIdSub("");
		}
		
		if (getCurrent().getTrioJurnalPK().getIdPerkiraan() == null || getCurrent().getTrioJurnalPK().getIdPerkiraan().equalsIgnoreCase("") ){
			Messagebox.show("No Perkiraan kosong");
			return;
		}
		if (getCurrent().getKredit() == null && getCurrent().getDebet() == null){
			Messagebox.show("Debet atau Kredit belum diisi");
			return;
		}
		
		TrioJurnal j = new TrioJurnal();
		
		if (getCurrent().getDebet() == null){
			getCurrent().setDebet(new BigDecimal(0));
		}
		if (getCurrent().getKredit() == null){
			getCurrent().setKredit(new BigDecimal(0));
		}
		
		if (getCurrent().getKredit().compareTo(new BigDecimal(0)) == 0 && getCurrent().getDebet().compareTo(new BigDecimal(0)) == 0){
			Messagebox.show("Debet atau Kredit tidak valid");
			return;
		}
		
		totalDebet = totalDebet.add(getCurrent().getDebet());
		totalKredit = totalKredit.add(getCurrent().getKredit());
		selisih = totalDebet.subtract(totalKredit);
		j = getCurrent();
		j.setTglJurnal(getTodayDate());
		j.setKet(getKetJurnal());
		j.setKet2(getKetJurnal2());
		listModel.add(j);
		isFocus = true;
		reset();
	}
	
	@NotifyChange({"current","selectedSubPerkiraan","subPerkiraans","debet","kredit"})
	@Command("reset")
	public void reset(){
		current = new TrioJurnal();
		selectedSubPerkiraan = new TrioMstsubperkiraan();
		subPerkiraans = new ArrayList<TrioMstsubperkiraan>();
		debet = new BigDecimal(0);
		kredit = new BigDecimal(0);
	}
	
	@NotifyChange({"current","listModel","todayDate","ketJurnal","ketJurnal2",
					"selectedPerkiraan","selectedSubPerkiraan","subPerkiraans","totalKredit",
					"totalDebet","selisih","debet","kredit"})
	@Command("resetAll")
	public void resetAll(){
		current = new TrioJurnal();
		listModel = new ListModelList<TrioJurnal>();
		todayDate = new Date();
		ketJurnal = null;
		ketJurnal2 = null;
		selectedSubPerkiraan = new TrioMstsubperkiraan();
		selectedPerkiraan = new TrioMstperkiraan();
		subPerkiraans = new ArrayList<TrioMstsubperkiraan>();
		totalDebet = new BigDecimal(0);
		totalKredit = new BigDecimal(0);
		selisih = new BigDecimal(0);
		debet = new BigDecimal(0);
		kredit = new BigDecimal(0);
		
	}
	
	@NotifyChange("ketJurnal2")
	@Command("copyData")
	public void copyData(){
		ketJurnal2 = getKetJurnal();
	}
	
	@NotifyChange({"current","listModel","todayDate","ketJurnal","ketJurnal2","selectedPerkiraan","selectedSubPerkiraan","subPerkiraans","totalKredit","totalDebet","selisih","debet","kredit"})
	@Command("save")
	public void save(){
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		
		BigDecimal jmlDebet = new BigDecimal(0);
		BigDecimal jmlKredit = new BigDecimal(0);
		List<TrioJurnal> listJurnal = new ArrayList<TrioJurnal>();
		for (TrioJurnal jurnal : listModel){
			if (jurnal.getDebet().compareTo(new BigDecimal(0)) == 1){
				jmlDebet = jmlDebet.add(jurnal.getDebet());
				System.out.println("jml debet = " + jmlDebet);
			}
			if (jurnal.getKredit().compareTo(new BigDecimal(0)) == 1){
				jmlKredit = jmlKredit.add(jurnal.getKredit());
				System.out.println("jml kredit = " + jmlKredit);
			}
			listJurnal.add(jurnal);
		}
		
		System.out.println("list dalam listbox = " + listJurnal.size());
		
		if (jmlDebet.compareTo(jmlKredit) != 0){
			Messagebox.show("Jumlah Debet dan Kredit tidak sama");
			return;
		}
		
		if (listJurnal.size() < 1 ){
			Messagebox.show("Tidak ada jurnal");
			return;
		}
		
		
		String idJurnal = getMasterFacade().getTrioJurnalDao().savePelunasan(listJurnal, "JME", getUserSession());
		Messagebox.show("Transaksi Sukses \nNo Jurnal "+idJurnal);
		resetAll();
	}
	
	@NotifyChange({"listModelChecked"})
	@Command("cek")
	public void cek(@BindingParam("checked") boolean isPicked, @BindingParam("picked") TrioJurnal jurnal){
		
		System.out.println("cecked = "+jurnal.getTrioJurnalPK().getIdPerkiraan());
		listModelChecked.add(jurnal);
	}
	
	@NotifyChange({"listModel","current","listModel","todayDate","ketJurnal","ketJurnal2","selectedPerkiraan","selectedSubPerkiraan","subPerkiraans","totalKredit","totalDebet","selisih","debet","kredit"})
	@Command("delete")
	public void delete(){
		listModel.removeAll(getListModelChecked());
		resetAll();
		
		totalDebet = totalDebet.add(getCurrent().getDebet());
		totalKredit = totalKredit.add(getCurrent().getKredit());
		selisih = totalDebet.subtract(totalKredit);
	}
	
	//buka popUp
	@Command
	public void openPopUp(@ContextParam(ContextType.VIEW) Component view){
		Executions.createComponents("/popup/popupPelunasan.zul", view, null);
	}
	
	//mengambil listmodel yang dikirim dari popup
	@NotifyChange({"listModel","current","selectedSubPerkiraan","subPerkiraans","totalDebet","totalKredit","selisih","debet","kredit","focus"})
	@Command
	public void sendListModel(@BindingParam("iModel") ListModelList<TrioJurnal> list){
		setListModel(list);
		for(TrioJurnal tj : list){
			totalDebet = totalDebet.add(tj.getDebet());
			totalKredit = totalKredit.add(tj.getKredit());
			selisih = totalDebet.subtract(totalKredit);
			reset();
		}
	}
	
	public Validator getFormValidator(){
		return new AbstractValidator() {
			
			public void validate(ValidationContext arg0) {
				// TODO , masbro
				
			}
		};
	}
	
}

