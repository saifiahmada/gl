package com.trio.gl.viewmodel.trn;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.ListModelList;

import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioDesignNeracaAkhir;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;

/** @author Saifi Ahmada May 26, 2013 6:10:40 PM  **/

public class TrnDesignNeraca extends TrioBasePageVM {
	
	private TrioDesignNeracaAkhir current;
	
	private ListModelList<TrioDesignNeracaAkhir> listModel;

	public TrioDesignNeracaAkhir getCurrent() {
		if (current == null) current = new TrioDesignNeracaAkhir();
		return current;
	}

	public void setCurrent(TrioDesignNeracaAkhir current) {
		this.current = current;
	}

	public ListModelList<TrioDesignNeracaAkhir> getListModel() {
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		
		if (listModel == null){
			listModel = new ListModelList<TrioDesignNeracaAkhir>();
			listModel.addAll(getMasterFacade().getTrioDesignNeracaAkhirDao().findAll());
		}
		return listModel;
	}

	public void setListModel(ListModelList<TrioDesignNeracaAkhir> listModel) {
		this.listModel = listModel;
	}
	
	@NotifyChange({"listModel"})
	@Command
	public void newFile(){
		
		Messagebox.show("Question is pressed. Are you sure?", "Question",
				Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION, new EventListener<Event>() {
					
					public void onEvent(Event event) throws Exception {
						// TODO , masbro
						if (event.getName().equals(Messagebox.ON_OK)){
							DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
							getMasterFacade().getTrioDesignNeracaAkhirDao().updateNewNeracaAkhir(getUserSession());
							System.out.println("Sukses Update");
							listModel = new ListModelList<TrioDesignNeracaAkhir>();
							listModel.addAll(getMasterFacade().getTrioDesignNeracaAkhirDao().findAll());
							Messagebox.show("Proses Selesai");
						}else if (event.getName().equals(Messagebox.ON_CANCEL)){
							Messagebox.show("Proses Dibatalkan");
						}
						
					}
				}); 
		
	}
	
	@NotifyChange({"listModel"})
	@Command
	public void loadFile(){
	
	}
	
	@NotifyChange({"listModel"})
	@Command
	public void preview(){
	
	}
	
	@NotifyChange({"listModel"})
	@Command
	public void save(){
	
	}
	

}

