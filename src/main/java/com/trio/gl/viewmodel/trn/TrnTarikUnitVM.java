package com.trio.gl.viewmodel.trn;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;

import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioJurnal;
import com.trio.gl.bean.TrioStokunit;
import com.trio.gl.bean.TrioTarikunit;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;
import com.trio.gl.util.TrioDateConv;

/** @author Saifi Ahmada Mar 23, 2013 11:47:15 PM  **/

public class TrnTarikUnitVM extends TrioBasePageVM {
	
	private String kodeCabang;
	
	private Date tglTarik;
	
	private ListModelList<TrioTarikunit> listModel;
	
	public ListModelList<TrioTarikunit> getListModel(){
		if (listModel == null){
			listModel = new ListModelList<TrioTarikunit>();
		}
		return listModel;
	}
	
	@NotifyChange({"kodeCabang","tglTarik","listModel"}) 
	@Command
	public void tarik(){
		
		String tgl = TrioDateConv.format(getTglTarik(), "YYYY-MM-dd");
		DatabaseContextHolder.setConnectionType(ConnectionType.ORACLEUNIT);
		List<TrioTarikunit> list = getMasterFacade().getTrioTarikunitDao().getListUnitOracle(getKodeCabang(), tgl);
		listModel = new ListModelList<TrioTarikunit>();
		listModel.addAll(list);
	}
	
	@NotifyChange("listModel")
	@Command
	public void save(){
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		List<TrioStokunit> listStokUnit = new ArrayList<TrioStokunit>();
		TrioStokunit unit;
		if (listModel.size() < 1){
			Messagebox.show("Tidak ada data");
			return;
		}
		
		BigDecimal jumlah = new BigDecimal(0);
		BigDecimal jumlahPotongan = new BigDecimal(0);
		BigDecimal ppn = new BigDecimal(0);
		BigDecimal hutang = new BigDecimal(0);
		Date tglJurnal = new Date();
		String noFaktur = null;
		String namaPerkiraan = null;
		List<TrioJurnal> listJurnal = new ArrayList<TrioJurnal>();
		
		for (TrioTarikunit tarik : listModel){
			unit = new TrioStokunit(tarik.getNoMesin(), tarik.getKdItem());
			unit.setKdDlr(tarik.getKdDlr());
			unit.setNoSjIn(tarik.getNoSj());
			unit.setNoFakIn(tarik.getNoFaktur());
			unit.setNoRangka(tarik.getNoRangka());
			unit.setTipe(tarik.getKet1());
			jumlah = jumlah.add(tarik.getHargaStd());
			jumlahPotongan = jumlahPotongan.add(tarik.getDiscount());
			BigDecimal hargaStd = tarik.getHargaStd().subtract(tarik.getDiscount());
			
			unit.setHarga(hargaStd);
			unit.setMasuk(1);
			listStokUnit.add(unit);
			noFaktur = tarik.getNoFaktur();
			tglJurnal = tarik.getTglSj();
			
		}
		TrioJurnal jurnal = new TrioJurnal();
		jurnal.getTrioJurnalPK().setIdPerkiraan("11501");
		jurnal.getTrioJurnalPK().setIdSub("");
		namaPerkiraan = getMasterFacade().getTrioMstperkiraanDao().findNamaById("11501");
		jurnal.setNamaPerkiraan(namaPerkiraan);
		jurnal.setTglJurnal(tglJurnal);
		jurnal.setBlc("*");
		jurnal.setKet("PEMB. SMH NO : "+noFaktur);
		jurnal.setDebet(jumlah.subtract(jumlahPotongan));
		jurnal.setKredit(new BigDecimal(0));
		jurnal.setTipe("D");
		jurnal.setNoReff(noFaktur);
		
		listJurnal.add(jurnal);
		
		jurnal = new TrioJurnal();
		jurnal.getTrioJurnalPK().setIdPerkiraan("11701");
		jurnal.getTrioJurnalPK().setIdSub("");
		namaPerkiraan = getMasterFacade().getTrioMstperkiraanDao().findNamaById("11701");
		jurnal.setNamaPerkiraan(namaPerkiraan);
		jurnal.setTglJurnal(tglJurnal);
		jurnal.setBlc("*");
		jurnal.setKet("PEMB. SMH NO : "+noFaktur);
		
		jumlah = jumlah.subtract(jumlahPotongan);
		ppn = jumlah.multiply(new BigDecimal(0.1));
		jurnal.setDebet(ppn);
		jurnal.setKredit(new BigDecimal(0));
		jurnal.setTipe("D");
		jurnal.setNoReff(noFaktur);
		listJurnal.add(jurnal);
		
		jurnal = new TrioJurnal();
		jurnal.getTrioJurnalPK().setIdPerkiraan("21101");
		jurnal.getTrioJurnalPK().setIdSub("");
		namaPerkiraan = getMasterFacade().getTrioMstperkiraanDao().findNamaById("21101");
		jurnal.setNamaPerkiraan(namaPerkiraan);
		jurnal.setTglJurnal(tglJurnal);
		jurnal.setBlc("*");
		jurnal.setKet("PEMB. SMH NO : "+noFaktur);
		jurnal.setDebet(new BigDecimal(0));
		
		hutang = jumlah.add(ppn);
		
		jurnal.setKredit(hutang);
		jurnal.setTipe("K");
		jurnal.setNoReff(noFaktur);
		listJurnal.add(jurnal);
		
		getMasterFacade().getTrioJurnalDao().saveTransaction(listJurnal, "JME", getUserSession());
		getMasterFacade().getTrioStokunitDao().saveList(listStokUnit,getUserSession());
		listModel = new ListModelList<TrioTarikunit>();
	}

	public String getKodeCabang() {
		return kodeCabang;
	}

	public void setKodeCabang(String kodeCabang) {
		this.kodeCabang = kodeCabang;
	}

	public Date getTglTarik() {
		return tglTarik;
	}

	public void setTglTarik(Date tglTarik) {
		this.tglTarik = tglTarik;
	}

}

