package com.trio.gl.viewmodel.trn;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.io.Files;
import org.zkoss.util.media.Media;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.event.UploadEvent;

import ar.com.fdvs.dj.core.DynamicJasperHelper;
import ar.com.fdvs.dj.core.layout.ClassicLayoutManager;
import ar.com.fdvs.dj.domain.DynamicReport;
import ar.com.fdvs.dj.domain.builders.ColumnBuilderException;
import ar.com.fdvs.dj.domain.builders.FastReportBuilder;

import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioJurnal;
import com.trio.gl.bean.TrioStokUnitTemp;
import com.trio.gl.helper.bean.CurCekPenjualan;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;

/**
 * @author Gusti Arya 9:35:09 AM Oct 4, 2013
 */

public class TrnCekPenjualanVM extends TrioBasePageVM{

	private String filePath;
	private String noPerk1;
	private String noPerk2;
	private String kas;
	private String sheet;
	private boolean cb;
	private boolean radCabang1;
	private boolean radCabang2;


	public String getSheet() {
		return sheet;
	}

	public void setSheet(String sheet) {
		this.sheet = sheet;
	}

	public boolean isCb() {
		return cb;
	}

	@NotifyChange({"cb","sheet"})
	public void setCb(boolean cb) {
		this.cb = cb;
		this.sheet = "";
	}

	public boolean isRadCabang1() {
		return radCabang1;
	}

	@NotifyChange({"noPerk1", "noPerk2"})
	public void setRadCabang1(boolean radCabang1) {
		this.noPerk1 = "41201";
		this.noPerk2 = "11312";
		this.kas = "11101";
		this.radCabang1 = radCabang1;
	}

	public boolean isRadCabang2() {
		return radCabang2;
	}

	@NotifyChange({"noPerk1", "noPerk2"})
	public void setRadCabang2(boolean radCabang2) {
		this.noPerk1 = "41204";
		this.noPerk2 = "11326";
		this.kas = "11102";
		this.radCabang2 = radCabang2;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}


	//Button Upload Browse File
	@Command
	@NotifyChange("filePath")
	public void browse(@BindingParam("upEvent") UploadEvent event) throws IOException{

		Media media = event.getMedia();
		File file = new File(media.getName());
		File files = new File(file.getAbsolutePath());

		if(!media.getName().contains(".xls")){
			Messagebox.show("File yang diupload harus berformat .xls");
			return;
		}else{
			setFilePath(files.getAbsolutePath());
			Files.copy(files, media.getStreamData());
		}

	}

	//create excel file
	@Command("create")
	public void create() throws IOException, ColumnBuilderException, ClassNotFoundException, JRException{
		if(noPerk1 == null || noPerk2 == null){
			Messagebox.show("Pilih Cabang Terlebih Dahulu");
			return;
		}

		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));

		List<CurCekPenjualan> listCekPenjualan = new ArrayList<CurCekPenjualan>();
		CurCekPenjualan curCekPenjualan = null;

		FileInputStream file = new FileInputStream(new File(getFilePath()));

		//create workbook
		HSSFWorkbook workbook = new HSSFWorkbook(file);

		//get desired sheet from workbook
		HSSFSheet sheet = workbook.getSheetAt(0);
		if(isCb()){
			sheet = workbook.getSheet(getSheet());
			if(sheet == null){
				Messagebox.show("Sheet tidak ditemukan");
				return;
			}
		}

		Row row;
		for(int i = 1 ; i <= sheet.getLastRowNum(); i++){
			row = sheet.getRow(i);

			//cek jika pada cell yang ke 0 hasilnya = 0 maka stop perulangan
			if(row.getCell(0).getNumericCellValue() == 0){
				break;
			}

			double no = row.getCell(0).getNumericCellValue();
			Date tglSj = row.getCell(1).getDateCellValue();
			String noMesin = row.getCell(2).getStringCellValue();
			String noRangka = row.getCell(3).getStringCellValue();
			String kdItem = row.getCell(4).getStringCellValue();
			String noFak = row.getCell(5).getStringCellValue();
			String nmCust = row.getCell(6).getStringCellValue();

			curCekPenjualan = new CurCekPenjualan();
			curCekPenjualan.setNo(no);
			curCekPenjualan.setTglSj(tglSj);
			curCekPenjualan.setNoMesin(noMesin);
			curCekPenjualan.setNoRangka(noRangka);
			curCekPenjualan.setKdItem(kdItem);
			curCekPenjualan.setNoFak(noFak);
			curCekPenjualan.setNmCust(nmCust);
			curCekPenjualan.setPot(new BigDecimal(0));
			curCekPenjualan.setNett(new BigDecimal(0));
			curCekPenjualan.setKet("");

			listCekPenjualan.add(curCekPenjualan);
		}
		file.close();

		//mencari nilai pot
		BigDecimal pot = new BigDecimal(0);
		BigDecimal hit = new BigDecimal(0);

		ListIterator<CurCekPenjualan> listIterator = listCekPenjualan.listIterator();
		while(listIterator.hasNext()){
			CurCekPenjualan current = listIterator.next();
			hit = getMasterFacade().getTrioStokUnitTempDao()
					.getNoFakOutCount(current.getNoFak());

			List<TrioJurnal> listsJurnal = getMasterFacade().getTrioJurnalDao()
					.getPotCekPenjualan(noPerk1, noPerk2, current.getNoFak());
			System.out.println("size = " + listsJurnal.size());
			if(listsJurnal.size() > 0){
				BigDecimal iDebet = new BigDecimal(0);

				for(TrioJurnal tjCurrent : listsJurnal){
					iDebet = iDebet.add(tjCurrent.getDebet());
				}

				pot = pot.add(iDebet);
				pot = pot.divide(hit);
			}
			current.setPot(pot);


			//mencari nilai nett
			BigDecimal bdNett = new BigDecimal(0);
			BigDecimal nett = getMasterFacade().getTrioJurnalDao().getNettCekPenjualan(kas, current.getNoFak());
			if(nett != null){
				bdNett = nett.divide(hit);
				bdNett = bdNett.subtract(pot);
				current.setNett(bdNett);
			}

			//mencari nilai ket
			BigDecimal hargaFull = new BigDecimal(0);
			TrioStokUnitTemp harga = getMasterFacade().getTrioStokUnitTempDao().getKetCekPenjualan(current.getNoFak());

			if(harga != null){
				System.out.println("masuk if");
				hargaFull = (harga.getHargaJ()).add(harga.getHargaJ().multiply(new BigDecimal(0.1))).add(harga.getbSTNK());
			}

			hargaFull.setScale(0, BigDecimal.ROUND_CEILING);

			BigDecimal bds = current.getPot().add(current.getNett());

			if((hargaFull.subtract(bds)).compareTo(BigDecimal.ZERO) > 0){
				current.setKet("Belum Lunas");
			}

			//update list 
			listIterator.set(current);
		}

		//mencetak dan mebuat kolom di excel
		FastReportBuilder frb = new FastReportBuilder();
		frb.addColumn("No", "no", Double.class.getName(), 20,true,"#");
		frb.addColumn("Tgl Sj","tglSj", Date.class.getName(),70 );
		frb.addColumn("No Mesin","noMesin", String.class.getName(),120);
		frb.addColumn("No. Rangka","noRangka",String.class.getName(),120);
		frb.addColumn("Kd. Item", "kdItem", String.class.getName(),120);
		frb.addColumn("No Faktur", "noFak", String.class.getName(),150);
		frb.addColumn("Nama Customer", "nmCust", String.class.getName(),200);
		frb.addColumn("Pot", "pot", BigDecimal.class.getName(),120,true,"###,##0.0");
		frb.addColumn("Nett", "nett", BigDecimal.class.getName(),120,true,"###,##0.0");
		frb.addColumn("Ket", "ket", String.class.getName(),120);

		DynamicReport dr = frb.build();
		JasperPrint jp = DynamicJasperHelper.generateJasperPrint(dr, new ClassicLayoutManager(), listCekPenjualan);

		File files = new File("C:/cekPenjualan.xls");
		FileOutputStream fos = new FileOutputStream(files);

		JRXlsExporter exporter = new JRXlsExporter();
		exporter.setParameter(JRExporterParameter.JASPER_PRINT, jp);
		exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, fos);
		exporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
		exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, Boolean.TRUE);
		exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
		exporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND,Boolean.FALSE);

		exporter.exportReport();

		Messagebox.show("File berhasil dibuat di C:/cekPenjualan.xls");

	}
}
