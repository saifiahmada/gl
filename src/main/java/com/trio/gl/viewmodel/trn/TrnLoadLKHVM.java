package com.trio.gl.viewmodel.trn;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.io.Files;
import org.zkoss.util.media.Media;
import org.zkoss.zul.Messagebox;

import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioDetailStnk;
import com.trio.gl.bean.TrioDetailStnkPK;
import com.trio.gl.bean.TrioHeaderStnk;
import com.trio.gl.bean.TrioMstkonfigurasi;
import com.trio.gl.bean.TrioTerima;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;
import com.trio.gl.util.TrioDateConv;
import com.trio.gl.util.TrioFileConv;

/** @author Saifi Ahmada Mar 25, 2013 9:01:05 AM  **/

public class TrnLoadLKHVM extends TrioBasePageVM {
	
	private String idCabang;
	
	public String getIdCabang() {
		return idCabang;
	}

	public void setIdCabang(String idCabang) {
		this.idCabang = idCabang;
	}
	
	@Command
	public void getUploadedFile(@BindingParam("media") Media media)  {
		
		if (getIdCabang().equalsIgnoreCase("") || getIdCabang() == null){
			Messagebox.show("Id Cabang Kosong");
			return;
		}
		
		if (media == null){
			Messagebox.show("File belum dipilih");
			return;
		}
		
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		TrioMstkonfigurasi zip = getMasterFacade().getTrioMstkonfigurasiDao().findById("112");
		TrioMstkonfigurasi extracted = getMasterFacade().getTrioMstkonfigurasiDao().findById("113");
		
		List<TrioTerima> listTerima = new ArrayList<TrioTerima>();
		
		if (media != null){
			try {
				File dst = new File(zip.getNilai()+media.getName());
				
				String ctype = media.getContentType();
				
				Files.copy(dst, media.getStreamData());
				if (ctype.equalsIgnoreCase("application/zip")){

					try{
						
						ZipFile file = new ZipFile(dst);
						if (file.isEncrypted()){
							
							String folderKirim = getNamaFileLKH(file.getFile().getName(), false);
							String fileLKH = getNamaFileLKH(file.getFile().getName(), true);
							
							file.setPassword("DBKasirTr10");
							file.extractAll(extracted.getNilai()+folderKirim);
							
							System.out.println("Nama file "+ file.getFile().getName());
							Messagebox.show("File \n"+file.getFile().getAbsoluteFile()+"\nExtract "+extracted.getNilai()+folderKirim);
							
							File fileExtract = new File(extracted.getNilai()+folderKirim+"/"+"kirim/"+fileLKH);
							File fileDtlStnk = new File(extracted.getNilai()+folderKirim+"/"+"kirim/"+"d_stnk.dst");
							File fileHdrStnk = new File(extracted.getNilai()+folderKirim+"/"+"kirim/"+"h_stnk.hst");
							
							// awal proses baca file h_stnk
							/*
							List<TrioHeaderStnk> listHdrStnk = new ArrayList<TrioHeaderStnk>();
							TrioHeaderStnk hdrStnk = null;
							if (fileHdrStnk.isFile()){
								Vector<String[]> vcStringHdrStnk = TrioFileConv.castToStringArrayVector(fileHdrStnk);
								
								for (String [] arrStr : vcStringHdrStnk){
									String noMhn = arrStr[0];
									String nmWil = arrStr[1];
									String tgl = arrStr[2];
									String jumlah = arrStr[3];
									String qty = arrStr[4];
									String pengurus = arrStr[5];
									String flag = arrStr[6];
									String jnsKlr = arrStr[7];
									String stnk = arrStr[8];
									String lain = arrStr[9];
									String kStnk = arrStr[10];
									String kLain = arrStr[11];
									
									hdrStnk = new TrioHeaderStnk();
									hdrStnk.setNoMhn(noMhn);
									hdrStnk.setNmWil(nmWil);
									hdrStnk.setTgl(parsingToDateByString(tgl));
									hdrStnk.setJumlah(new BigDecimal(jumlah)); 
									hdrStnk.setQty(new BigDecimal(qty));
									hdrStnk.setPengurus(pengurus);
									hdrStnk.setFlag(flag);
									hdrStnk.setJnsKlr(jnsKlr);
									hdrStnk.setStnk(new BigDecimal(stnk));
									hdrStnk.setLain(new BigDecimal(lain));
									hdrStnk.setkStnk(new BigDecimal(kStnk));
									hdrStnk.setkLain(new BigDecimal(kLain));
									listHdrStnk.add(hdrStnk);
								}
								
							}
							List<TrioDetailStnk> listDtlStnk = new ArrayList<TrioDetailStnk>();
							TrioDetailStnk dtlStnk = null;
							// akhir proses baca file h_stnk
							
							// awal proses baca file d_stnk
							if (fileDtlStnk.isFile()){
								Vector<String[]> vcStringDtlStnk = TrioFileConv.castToStringArrayVector(fileDtlStnk);
								for (String [] arrStr : vcStringDtlStnk){
									String noMhn = arrStr[0];
									String noMesin = arrStr[1];
									String noRangka = arrStr[2];
									String kdItem = arrStr[3];
									String nama = arrStr[4];
									String biaya = arrStr[5];
									biaya = biaya == null || biaya.equalsIgnoreCase("") || biaya.equalsIgnoreCase(" ") ? biaya = "0" : biaya;
									String check = arrStr[6];
									String status = arrStr[7];
									String stnk = arrStr[8];
									String lain = arrStr[9];
									String kStnk = arrStr[10];
									String kLain = arrStr[11];
									String noBuktiS = arrStr[12];
									String noBuktiL = arrStr[13];
									String bbnkb = arrStr[14];
									String pkb = arrStr[15];
									String swdkllj = arrStr[16];
									String totLain = arrStr[17];
									String bbnkbR = arrStr[18];
									String pkbR = arrStr[19];
									String swdklljR = arrStr[20];
									String sama = arrStr[21];
									String tglPinj = arrStr[22];
									String tglKmbl = arrStr[23];
									String tglKrdt = arrStr[24];
									String noFak = arrStr[25];
									String tglFak = arrStr[26];
									
									dtlStnk = new TrioDetailStnk();
									TrioDetailStnkPK pk = new TrioDetailStnkPK(noMesin, noMhn);
									dtlStnk.setTrioDetailStnkPK(pk);
									dtlStnk.setNoRangka(noRangka);
									dtlStnk.setKdItem(kdItem);
									dtlStnk.setNama(nama);
									dtlStnk.setBiaya(new BigDecimal(biaya));
									dtlStnk.setCek(new BigDecimal(check)); 
									dtlStnk.setStatus(status);
									dtlStnk.setStnk(new BigDecimal(stnk));
									dtlStnk.setLain(new BigDecimal(lain));
									dtlStnk.setkStnk(new BigDecimal(kStnk));
									dtlStnk.setkLain(new BigDecimal(kLain));
									dtlStnk.setNoBuktiS(noBuktiS);
									dtlStnk.setNoBuktiL(noBuktiL);
									dtlStnk.setBbnkb(new BigDecimal(bbnkb));
									dtlStnk.setPkb(new BigDecimal(pkb));
									dtlStnk.setSwdkllj(new BigDecimal(swdkllj));
									dtlStnk.setTotLain(new BigDecimal(totLain));
									dtlStnk.setBbnkbR(new BigDecimal(bbnkbR));
									dtlStnk.setPkbR(new BigDecimal(pkbR));
									dtlStnk.setSwdklljR(new BigDecimal(swdklljR));
									dtlStnk.setSama(new BigDecimal(sama));
									dtlStnk.setTglPinj(parsingToDateByString(tglPinj));
									dtlStnk.setTglKmbl(parsingToDateByString(tglKmbl));
									dtlStnk.setTglKrdt(parsingToDateByString(tglKrdt));
									dtlStnk.setNoFak(noFak);
									dtlStnk.setTglFak(parsingToDateByString(tglFak)); 
									listDtlStnk.add(dtlStnk);
								}
							}
							// akhir proses baca file d_stnk
							 
							 
							List<TrioHeaderStnk> listHeader = new ArrayList<TrioHeaderStnk>();
							Set<TrioDetailStnk> trioDetailStnkSet = null;
							TrioHeaderStnk objHeader = null;
							int y = 0;
							for (TrioHeaderStnk header : listHdrStnk){
								y++;
								
								objHeader = new TrioHeaderStnk();
								trioDetailStnkSet = new HashSet<TrioDetailStnk>();
								for (TrioDetailStnk detail : listDtlStnk){
									
									if (header.getNoMhn().equalsIgnoreCase(detail.getTrioDetailStnkPK().getNoMhn())){
										trioDetailStnkSet.add(detail);
									}
									header.setTrioDetailStnkSet(trioDetailStnkSet);
									listHeader.add(header);
								}
								
							}
							
							getMasterFacade().getTrioHeaderStnkDao().saveAllCollection(listHeader, getUserSession());
							
							if (true){
								System.out.println("return");
								return;
							}
							*/
							int i = 0;
					        String baris = null;
					       
					        Vector<String[]> vcString = TrioFileConv.castToStringArrayVector(fileExtract);
					        
					        Vector<String[]> vectorString = new Vector<String[]>();
					        String[] tempString = null;
					       
					        int b = 0;
					        int brs = 0;
					        for (String [] st : vcString){
					        	b++;
					        	if (b < 3){
					        		System.out.println("continue saat baris ke "+b);
					        		continue;
					        	}else {
					        		brs++;
					        		int stLen = st.length;
					        		String noLKH = st[0];
					        		String noBukti = st[1];
					        		String noKWT = st[2];
					        		String tglTrm = st[3];
					        		String jns = st[4];
					        		String ket = st[5];
					        		String nmKet = st[6];
					        		String nmTran = st[7];
					        		String jmlTrm = st[8];
					        		String debet = st[9];
					        		debet = debet.length() < 1 || debet.equalsIgnoreCase("") || debet.equalsIgnoreCase(" ") ? "0" : debet;
					        		String kredit = st[10];
					        		kredit = kredit.length() < 1 || kredit.equalsIgnoreCase("") || kredit.equalsIgnoreCase(" ") ? "0" : kredit;
					        		String tutup = st[11];
					        		String user = st[12];
					        		String tglUser = st[13];
					        		String bpkb = st[14];
					        		bpkb = bpkb.length() < 1 || bpkb.equalsIgnoreCase("") || bpkb.equalsIgnoreCase(" ") ? "0" : bpkb;
					        		String ketAng = st[15];
					        		String reg = st[16];
					        		String noMesin = st[17];
					        		String noRangka = st[18];
					        		String kdPrs = st[19];
					        		String noRec = st[20];
					        		String noCekBg = st[21];
					        		String namaBank = st[22];
					        		String tglJT = st[23];
					        		String noPerk = st[24];
					        		String noPerk2 = st[25];
					        		String noSub = st[26];
					        		String ket2 = st[27];
					        		String noFak = st[28];
					        		noFak = noFak == null ? "" : noFak;
					        		String sAHM = st[29];
					        		sAHM = sAHM.length() < 1 || sAHM.equalsIgnoreCase("") || sAHM.equalsIgnoreCase(" ") ? "0" : sAHM;
					        		String sMD = st[30];
					        		sMD = sMD.length() < 1 || sMD.equalsIgnoreCase("") || sMD.equalsIgnoreCase(" ") ? "0" : sMD;
					        		String sSD = st[31];
					        		sSD = sSD.length() < 1 || sSD.equalsIgnoreCase("") || sSD.equalsIgnoreCase(" ") ? "0" : sSD;
					        		String sFinance = st[32];
					        		sFinance = sFinance.length() < 1 || sFinance.equalsIgnoreCase("") || sFinance.equalsIgnoreCase(" ") ? "0" : sFinance;
					        		String sBunga = st[33];
					        		sBunga = sBunga.length() < 1 || sBunga.equalsIgnoreCase("") || sBunga.equalsIgnoreCase(" ") ? "0" : sBunga;
					        		String kdItem = st[34];
					        		String harga = st[35];
					        		String bSTNK = st[36];
					        		bSTNK = bSTNK.length() < 1 || bSTNK.equalsIgnoreCase("") || bSTNK.equalsIgnoreCase(" ") ? "0" : bSTNK;
					        		String pSTNK = st[37];
					        		String pBPKB = st[38];
					        		String nmCust = st[39];
					        		String batalJual = st[40];
					        		
					        		String kdBank = st[41];
					        		String kdBank2 = st[42];
					        		String noBP = st[43];
					        		String alamat = st[44];
					        		String jkWaktu = st[45];
					        		String angsuran = st[46];
					        		angsuran = angsuran.length() < 1 || angsuran.equalsIgnoreCase("") || angsuran.equalsIgnoreCase(" ") ? "0" : angsuran;
					        		String noSJ = st[47];
					        		String kdPOS = st[48];
					        		String itr = st[49];
					        		String noLKH2 = st[50];
					        		String tglTrm2 = st[51];
					        		String noKirim = st[52];
					        		String tglKirim = st[53];
					        		String tblB = st[54];
					        		String hadiah = st[55];
					        		
					        		String tambMD = st[56];
					        		tambMD = tambMD.equalsIgnoreCase("") || tambMD.length() < 1 || tambMD.equalsIgnoreCase(" ") ? "0" : tambMD;
					        		
					        		String tambSD = st[57];
					        		tambSD = tambSD.equalsIgnoreCase("") || tambSD.length() < 1 || tambSD.equalsIgnoreCase(" ") ? "0" : tambSD;
					        		String tambFIN = st[58];
					        		tambFIN = tambFIN.equalsIgnoreCase("") || tambFIN.length() < 1 || tambFIN.equalsIgnoreCase(" ") ? "0" : tambFIN;
					        		
					        		TrioTerima terima = new TrioTerima(noLKH, noFak);
					        		terima.setAlamat(alamat);
					        		terima.setAngsuran(new BigDecimal(angsuran));
					        		terima.setBatalJual(batalJual);
					        		terima.setBPKB(Integer.parseInt(bpkb));
					        		terima.setbSTNK(new BigDecimal(bSTNK));
					        		terima.setDebet(new BigDecimal(debet));
					        		terima.setHarga(new BigDecimal(harga));
					        		terima.setItr(itr);
					        		terima.setJkWaktu(Integer.parseInt(jkWaktu));
					        		terima.setJmlTrm(new BigDecimal(jmlTrm));
					        		terima.setJns(jns);
					        		terima.setHadiah(hadiah);
					        		terima.setKdBank(kdBank);
					        		terima.setKdBank2(kdBank2);
					        		terima.setKdItem(kdItem);
					        		terima.setKdPOS(kdPOS);
					        		terima.setKdPrs(kdPrs);
					        		terima.setKet(ket);
					        		terima.setKet2(ket2);
					        		terima.setKetAng(Integer.parseInt(ketAng));
					        		terima.setKredit(new BigDecimal(kredit));
					        		terima.setNamaBank(namaBank);
					        		terima.setNmCust(nmCust);
					        		terima.setNmKet(nmKet);
					        		terima.setNmTran(nmTran);
					        		terima.setNoBP(noBP);
					        		terima.setNoBukti(noBukti);
					        		terima.setNoCekBg(noCekBg);
					        		
					        		terima.setNoKirim(noKirim);
					        		terima.setNoKWT(noKWT);
					        		terima.setNoLKH2(noLKH2);
					        		terima.setNoMesin(noMesin);
					        		terima.setNoPerk(noPerk);
					        		terima.setNoPerk2(noPerk2);
					        		terima.setNoRangka(noRangka);
					        		terima.setNoRec(noRec);
					        		terima.setNoSJ(noSJ);
					        		terima.setNoSub(noSub);
					        		terima.setpBPKB(new BigDecimal(pBPKB));
					        		terima.setpSTNK(new BigDecimal(pSTNK));
					        		terima.setReg(reg);
					        		terima.setsAHM(new BigDecimal(sAHM));
					        		terima.setsBunga(new BigDecimal(sBunga));
					        		terima.setsFinance(new BigDecimal(sFinance));
					        		terima.setsMD(new BigDecimal(sMD));
					        		terima.setsSD(new BigDecimal(sSD));
					        		
					        		terima.setTambFIN(new BigDecimal(tambFIN));
					        		terima.setTambMD(new BigDecimal(tambMD));
					        		terima.setTambSD(new BigDecimal(tambSD));
					        		
					        		terima.setTglJT(parsingToDateByString(tglJT));
					        		terima.setTglKirim(parsingToDateByString(tglKirim));
					        		terima.setTglTrm(parsingToDateByString(tglTrm)); 
					        		terima.setTglTrm2(parsingToDateByString(tglTrm2)); 
					        		terima.setTglUser(parsingToDateByString(tglUser)); 
					        		terima.setTutup(tutup);
					        		terima.setUser(user);
					        		
					        		listTerima.add(terima);
					        	}
					        }
					        
					        if (getIdCabang().equalsIgnoreCase("cabang1")){
					        	System.out.println("Proses Cabang 1"); 
					        	getMasterFacade().getTrioTerimaDao().prosesDataUploadLKHCabang1(listTerima, getUserSession());
					        	
					        } else {
					        	System.out.println("Proses Cabang 2");
					        	getMasterFacade().getTrioTerimaDao().prosesDataUploadLKHCabang2(listTerima, getUserSession());
					        }
					        
						}else{
							Messagebox.show("File zip invalid");
						}
						
					}catch (Exception e) {
						Messagebox.show("Error "+e);
						e.printStackTrace();
					}
					
				} else {
					Messagebox.show("Format file salah, file yang dianjurkan .zip");
					return;
				}
				
			} catch (IOException e) { 
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else{
			Messagebox.show("tidak ada");
			
		}

	}
	
	private String getNamaFileLKH(String sumber, boolean pakaiExtensionDotLKH){
		
		String [] pisahTitik = sumber.split("\\.");
		sumber = pisahTitik[0];
		
		String [] pisahGarisBawah = sumber.split("\\_");
		String kdDlr = pisahGarisBawah[0];
		String tanggalLengkap = pisahGarisBawah[1];
		String [] pisahTanggal = tanggalLengkap.split("\\-");
		
		String tanggal = pisahTanggal[0];
		String bulan = pisahTanggal[1];
		String tahun = pisahTanggal[2];
		
		String hasil = null;
		if (pakaiExtensionDotLKH){
			hasil = tahun+bulan+tanggal+kdDlr+".lkh";
		} else {
			hasil = tahun+bulan+tanggal+kdDlr;
		}
		return hasil;
	}
	
	private Date parsingToDateByString(String tglString){
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		try {
			return sdf.parse(tglString); 
		}catch (Exception e){
			return null;
		}
	}
	

}
