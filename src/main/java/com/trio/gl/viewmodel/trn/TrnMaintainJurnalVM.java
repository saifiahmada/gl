package com.trio.gl.viewmodel.trn;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;

import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioJurnal;
import com.trio.gl.bean.TrioMstperkiraan;
import com.trio.gl.bean.TrioMstsubperkiraan;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;

/** @author Saifi Ahmada Apr 5, 2013 3:46:33 PM  **/

public class TrnMaintainJurnalVM extends TrioBasePageVM {
	
	private String idJurnal;
	
	private Date tglJurnal;
	
	private String ket;
	
	private TrioJurnal current;
	
	private ListModelList<TrioJurnal> listModel;
	
	private ListModelList<TrioJurnal> listModelChecked = new ListModelList<TrioJurnal>();
	
	private TrioMstperkiraan selectedPerkiraan;
	
	private TrioMstsubperkiraan selectedSubPerkiraan;
	
	private List<TrioMstsubperkiraan> subPerkiraans;
	
	private BigDecimal debet,kredit,totalKredit, totalDebet, selisih;
	
	private boolean isFocus;
	
	public boolean getFocus(){
		return isFocus;
	}
	
	public TrioMstsubperkiraan getSelectedSubPerkiraan() {
		if (selectedSubPerkiraan == null) selectedSubPerkiraan = new TrioMstsubperkiraan();
		return selectedSubPerkiraan;
	}

	public void setSelectedSubPerkiraan(TrioMstsubperkiraan selectedSubPerkiraan) {
		this.selectedSubPerkiraan = selectedSubPerkiraan;
	}
	
	public BigDecimal getDebet() {
		if (debet == null) debet = new BigDecimal(0);
		return debet;
	}

	public void setDebet(BigDecimal debet) {
		this.debet = debet;
	}

	public BigDecimal getKredit() {
		if (kredit == null) kredit = new BigDecimal(0);
		return kredit;
	}

	public void setKredit(BigDecimal kredit) {
		this.kredit = kredit;
	}

	public BigDecimal getSelisih() {
		if (selisih == null) selisih = new BigDecimal(0);
		return selisih;
	}

	public void setSelisih(BigDecimal selisih) {
		this.selisih = selisih;
	}

	public BigDecimal getTotalKredit() {
		if (totalKredit == null) totalKredit = new BigDecimal(0);
		return totalKredit;
	}

	public void setTotalKredit(BigDecimal totalKredit) {
		this.totalKredit = totalKredit;
	}

	public BigDecimal getTotalDebet() {
		if (totalDebet == null) totalDebet = new BigDecimal(0);
		return totalDebet;
	}

	public void setTotalDebet(BigDecimal totalDebet) {
		this.totalDebet = totalDebet;
	}
	
	public List<TrioMstsubperkiraan> getSubPerkiraans(){
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		
		if (getSelectedPerkiraan().getTrioMstperkiraanPK().getIdPerkiraan() != null){
			System.out.println("dan id = "+getSelectedPerkiraan().getTrioMstperkiraanPK().getIdPerkiraan());
			subPerkiraans = getMasterFacade().getTrioMstsubperkiraanDao()
					.findByIdPerkiraan(getSelectedPerkiraan().getTrioMstperkiraanPK().getIdPerkiraan());
		}else{
			subPerkiraans = new ArrayList<TrioMstsubperkiraan>();
		}
		return subPerkiraans;
	}
	
	public void setSubPerkiraans(List<TrioMstsubperkiraan> subPerkiraans) {
		this.subPerkiraans = subPerkiraans;
	}
	
	public TrioMstperkiraan getSelectedPerkiraan() {
		if (selectedPerkiraan == null) selectedPerkiraan = new TrioMstperkiraan();
		return selectedPerkiraan;
	}
	
	public void setSelectedPerkiraan(TrioMstperkiraan selectedPerkiraan) {
		this.selectedPerkiraan = selectedPerkiraan;
	}
	
	public ListModelList<TrioJurnal> getListModelChecked() {
		return listModelChecked;
	}

	public void setListModelChecked(ListModelList<TrioJurnal> listModelChecked) {
		this.listModelChecked = listModelChecked;
	}

	public TrioJurnal getCurrent() {
		if (current == null) current = new TrioJurnal();
		return current;
	}

	public void setCurrent(TrioJurnal current) {
		this.current = current;
	}

	public ListModelList<TrioJurnal> getListModel() {
		if (listModel == null){
			listModel = new ListModelList<TrioJurnal>();
		}
		return listModel;
	}

	public void setListModel(ListModelList<TrioJurnal> listModel) {
		this.listModel = listModel;
	}

	public Date getTglJurnal() {
		return tglJurnal;
	}

	public void setTglJurnal(Date tglJurnal) {
		this.tglJurnal = tglJurnal;
	}

	public String getKet() {
		return ket;
	}

	public void setKet(String ket) {
		this.ket = ket;
	}

	public String getIdJurnal() {
		return idJurnal;
	}

	public void setIdJurnal(String idJurnal) {
		this.idJurnal = idJurnal;
	}
	
	public List<TrioMstperkiraan> getPerkiraans(){
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		return getMasterFacade().getTrioMstperkiraanDao().findAll();
	}
	
	@NotifyChange({"subPerkiraans","selectedPerkiraan","kredit","debet"})
	@Command("showSelectedPerkiraan")
	public void showSelectedPerkiraan(){
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		String idPerk = getSelectedPerkiraan().getTrioMstperkiraanPK().getIdPerkiraan();
		System.out.println("Id Perkiraan show "+idPerk);
		subPerkiraans = getMasterFacade().getTrioMstsubperkiraanDao().findByIdPerkiraan(idPerk);
		if (getSelisih().compareTo(new BigDecimal(0)) == 1){
			// transaksi sebelumnya debet,
			kredit = getSelisih();
		}else if (getSelisih().compareTo(new BigDecimal(0)) == -1) {
			// transaksi sebelumnya kredit,
			debet = getSelisih().abs();
		}else {
			kredit = new BigDecimal(0);
			debet = new BigDecimal(0);
		}
	}
	
	@NotifyChange({"listModel","current","selectedSubPerkiraan","subPerkiraans","totalDebet","totalKredit","selisih","debet","kredit","focus"})
	@Command
	public void addJurnal(){
		System.out.println("addJurnal ");
		
		getCurrent().getTrioJurnalPK().setIdJurnal(getIdJurnal());
		getCurrent().setDebet(getDebet());
		getCurrent().setKredit(getKredit());
		
		if (getSelectedPerkiraan().getNamaPerkiraan() != null){
			getCurrent().setNamaPerkiraan(getSelectedPerkiraan().getNamaPerkiraan());
		}
		if (getSelectedPerkiraan().getTrioMstperkiraanPK().getIdPerkiraan() != null){
			getCurrent().getTrioJurnalPK().setIdPerkiraan(getSelectedPerkiraan().getTrioMstperkiraanPK().getIdPerkiraan());
		}
		if (getSelectedSubPerkiraan().getTrioMstsubperkiraanPK().getIdSub() != null){
			getCurrent().getTrioJurnalPK().setIdSub(getSelectedSubPerkiraan().getTrioMstsubperkiraanPK().getIdSub());
		}else {
			getCurrent().getTrioJurnalPK().setIdSub("");
		}
		
		if (getCurrent().getTrioJurnalPK().getIdPerkiraan() == null || getCurrent().getTrioJurnalPK().getIdPerkiraan().equalsIgnoreCase("") ){
			Messagebox.show("No Perkiraan kosong");
			return;
		}
		if (getCurrent().getKredit() == null && getCurrent().getDebet() == null){
			Messagebox.show("Debet atau Kredit belum diisi");
			return;
		}
		
		TrioJurnal j = new TrioJurnal();
		
		if (getCurrent().getDebet() == null){
			getCurrent().setDebet(new BigDecimal(0));
		}
		if (getCurrent().getKredit() == null){
			getCurrent().setKredit(new BigDecimal(0));
		}
		
		if (getCurrent().getKredit().compareTo(new BigDecimal(0)) == 0 && getCurrent().getDebet().compareTo(new BigDecimal(0)) == 0){
			Messagebox.show("Debet atau Kredit tidak valid");
			return;
		}
		
		totalDebet = totalDebet.add(getCurrent().getDebet());
		totalKredit = totalKredit.add(getCurrent().getKredit());
		selisih = totalDebet.subtract(totalKredit);
		j = getCurrent();
		j.setTglJurnal(getTglJurnal());
		j.setKet(getKet());
		
		listModel.add(j);
		isFocus = true;
		reset();
		
	}
	
	@NotifyChange({"tglJurnal","ket","listModel","totalDebet","totalKredit","selisih"})
	@Command
	public void searchJurnal(){
		System.out.println("id jurnal == "+getIdJurnal());
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getCabangSession()));
		List<TrioJurnal> list = getMasterFacade().getTrioJurnalDao().getListJurnalByIdJurnal(getIdJurnal());
		TrioJurnal jurnal = list.get(0);
		setTglJurnal(jurnal.getTglJurnal());
		setKet(jurnal.getKet());
		listModel = new ListModelList<TrioJurnal>();
		listModel.addAll(list);
		BigDecimal totKredit = new BigDecimal(0);
		BigDecimal totDebet = new BigDecimal(0);
		for (TrioJurnal j : list){
			totKredit = totKredit.add(j.getKredit());
			totDebet = totDebet.add(j.getDebet());
			
		}
		totalDebet = totDebet;
		totalKredit = totKredit;
		selisih = totalDebet.subtract(totalKredit);
	}
	
	@NotifyChange({"listModelChecked"})
	@Command
	public void cek(@BindingParam("checked") boolean isPicked, @BindingParam("picked") TrioJurnal jurnalPick){
		
		if (isPicked){
			System.out.println("dicek");
			listModelChecked.add(jurnalPick);
		}else{
			System.out.println("tidak dicek");
			listModelChecked.remove(jurnalPick);
		}
	}
	
	@Command
	public void save(){
		
		if (listModel.size() < 1){
			Messagebox.show("Jurnal masih kosong");
			return;
		}
		List<TrioJurnal> listJurnal = new ArrayList<TrioJurnal>();
		for (TrioJurnal jurnal : listModel){
			listJurnal.add(jurnal);
		}
		getMasterFacade().getTrioJurnalDao().saveOrUpdateAll(listJurnal, getUserSession());
	}
	
	@NotifyChange({"current","selectedSubPerkiraan","subPerkiraans","debet","kredit"})
	@Command("reset")
	public void reset(){
		current = new TrioJurnal();
		selectedSubPerkiraan = new TrioMstsubperkiraan();
		subPerkiraans = new ArrayList<TrioMstsubperkiraan>();
		debet = new BigDecimal(0);
		kredit = new BigDecimal(0);
	}
	
	@NotifyChange("listModel")
	@Command
	public void delete(){
		System.out.println("delete");
		System.out.println("size delete "+listModelChecked.size());
		listModel.removeAll(listModelChecked);
	}
	
	
	
	

}

