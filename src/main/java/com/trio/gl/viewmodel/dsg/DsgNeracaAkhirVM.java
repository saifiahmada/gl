package com.trio.gl.viewmodel.dsg;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zkex.zul.Jasperreport;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Window;

import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioDesignNeracaAkhirAktiva;
import com.trio.gl.bean.TrioDesignNeracaAkhirPasiva;
import com.trio.gl.bean.TrioLabaDitahan;
import com.trio.gl.bean.TrioMstPerusahaan;
import com.trio.gl.bean.TrioMstkonfigurasi;
import com.trio.gl.bean.TrioRmsNeracaAktiva;
import com.trio.gl.bean.TrioRmsNeracaAktivaSub;
import com.trio.gl.bean.TrioRmsNeracaPasiva;
import com.trio.gl.bean.TrioRmsNeracaPasivaSub;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;

/**
 * @author Gusti Arya 9:45:52 AM Oct 25, 2013
 */

public class DsgNeracaAkhirVM extends TrioBasePageVM {

	private TrioDesignNeracaAkhirAktiva selectedDesignAktiva;
	private TrioDesignNeracaAkhirPasiva selectedDesignPasiva;
	private ListModelList<TrioDesignNeracaAkhirAktiva> listModelAktiva;
	private ListModelList<TrioDesignNeracaAkhirPasiva> listModelPasiva;

	public TrioDesignNeracaAkhirAktiva getSelectedDesignAktiva() {
		return selectedDesignAktiva;
	}

	public void setSelectedDesignAktiva(
			TrioDesignNeracaAkhirAktiva selectedDesignAktiva) {
		this.selectedDesignAktiva = selectedDesignAktiva;
	}

	public TrioDesignNeracaAkhirPasiva getSelectedDesignPasiva() {
		return selectedDesignPasiva;
	}

	public void setSelectedDesignPasiva(
			TrioDesignNeracaAkhirPasiva selectedDesignPasiva) {
		this.selectedDesignPasiva = selectedDesignPasiva;
	}

	public ListModelList<TrioDesignNeracaAkhirAktiva> getListModelAktiva() {
		DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);
		if(listModelAktiva == null){
			listModelAktiva = new ListModelList<TrioDesignNeracaAkhirAktiva>();
			List<TrioDesignNeracaAkhirAktiva> list = getMasterFacade().getTrioDesignNeracaAkhirAktivaDao().findAll();
			listModelAktiva.addAll(list);
		}
		return listModelAktiva;
	}

	public void setListModelAktiva(
			ListModelList<TrioDesignNeracaAkhirAktiva> listModelAktiva) {
		this.listModelAktiva = listModelAktiva;
	}

	public ListModelList<TrioDesignNeracaAkhirPasiva> getListModelPasiva() {
		DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);
		if(listModelPasiva == null){
			listModelPasiva = new ListModelList<TrioDesignNeracaAkhirPasiva>();
			List<TrioDesignNeracaAkhirPasiva> list = getMasterFacade().getTrioDesignNeracaAkhirPasivaDao().findAll();
			listModelPasiva.addAll(list);
		}
		return listModelPasiva;
	}

	public void setListModelPasiva(
			ListModelList<TrioDesignNeracaAkhirPasiva> listModelPasiva) {
		this.listModelPasiva = listModelPasiva;
	}

	//newFile Aktiva
	@Command
	@NotifyChange("listModelAktiva")
	public void newFileAktiva(){
		DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);

		List<TrioDesignNeracaAkhirAktiva> lists = getMasterFacade().getTrioDesignNeracaAkhirAktivaDao().findAll();
		for(TrioDesignNeracaAkhirAktiva current : lists){
			current.setLevel1(new BigDecimal(0));
			current.setLevel2(new BigDecimal(0));
			current.setNilai(new BigDecimal(0));
			getMasterFacade().getTrioDesignNeracaAkhirAktivaDao().update(current, getUserSession());
		}


		List<TrioDesignNeracaAkhirAktiva> lists2 = getMasterFacade().getTrioDesignNeracaAkhirAktivaDao().findAll();

		ListModelList<TrioDesignNeracaAkhirAktiva> newListModel = new ListModelList<TrioDesignNeracaAkhirAktiva>();
		newListModel.addAll(lists2);
		setListModelAktiva(newListModel);
	}

	//newFilePasiva
	@Command
	@NotifyChange("listModelPasiva")
	public void newFilePasiva(){
		DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);

		List<TrioDesignNeracaAkhirPasiva> lists = getMasterFacade().getTrioDesignNeracaAkhirPasivaDao().findAll();
		for(TrioDesignNeracaAkhirPasiva current : lists){
			current.setLevel1(new BigDecimal(0));
			current.setLevel2(new BigDecimal(0));
			current.setNilai(new BigDecimal(0));
			getMasterFacade().getTrioDesignNeracaAkhirPasivaDao().update(current, getUserSession());
		}

		List<TrioDesignNeracaAkhirPasiva> list2 = getMasterFacade().getTrioDesignNeracaAkhirPasivaDao().findAll();
		ListModelList<TrioDesignNeracaAkhirPasiva> newListModel = new ListModelList<TrioDesignNeracaAkhirPasiva>();
		newListModel.addAll(list2);
		setListModelPasiva(newListModel);
	}

	//deleteBaris Aktiva
	@Command
	@NotifyChange("listModelAktiva")
	public void deleteBarisAktiva(){
		getListModelAktiva().remove(getSelectedDesignAktiva());
	}
	
	//insertBaris Aktiva
	@Command
	@NotifyChange("listModelAktiva")
	public void addBarisAktiva(){
		TrioDesignNeracaAkhirAktiva dsgPasiva = 
				new TrioDesignNeracaAkhirAktiva("", "", new Date(), "", "", "", new BigDecimal(0), new BigDecimal(0), new BigDecimal(0));
		getListModelAktiva().add(dsgPasiva);
	}

	//saveBarisAktiva
	@Command
	@NotifyChange("listModelAktiva")
	public void updateBarisAktiva(){
		getMasterFacade().getTrioDesignNeracaAkhirAktivaDao().saveByList(getListModelAktiva(), getUserSession());
		Messagebox.show("Berhasil Update Baris Aktiva");
	}

	//deleteBaris pasiva
	@Command
	@NotifyChange("listModelPasiva")
	public void deleteBarisPasiva(){
		getListModelPasiva().remove(getSelectedDesignPasiva());
	}
	
	//addBaris Pasiva
	@Command
	@NotifyChange("listModelPasiva")
	public void addBarisPasiva(){
		TrioDesignNeracaAkhirPasiva pasiva = 
				new TrioDesignNeracaAkhirPasiva("", "", new Date(), "", "", "", new BigDecimal(0), new BigDecimal(0), new BigDecimal(0));
		getListModelPasiva().add(pasiva);
	}
	
	//Update Baris Pasiva
	@Command
	@NotifyChange("listModelPasiva")
	public void updateBarisPasiva(){
		getMasterFacade().getTrioDesignNeracaAkhirPasivaDao().saveByList(getListModelPasiva(), getUserSession());
		Messagebox.show("Berhasil Update Baris Pasiva");
	}

	//membuka popUp loadFile tab aktiva
	@Command
	public void loadFileAktiva(@ContextParam(ContextType.VIEW) Component view){
		Executions.createComponents("/popup/popupLoadFileNeracaAktiva.zul", view, null);
	}

	//popup preview aktiva
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Command
	public void previewAktiva(){

		DatabaseContextHolder.setConnectionType(ConnectionType.DEFAULT);

		TrioMstkonfigurasi konfig = getMasterFacade().getTrioMstkonfigurasiDao().findByPrimaryKey(new TrioMstkonfigurasi("111"));
		String pathReport = konfig.getNilai();

		File file = new File(pathReport+"\\previewAktiva.jasper");


		Window win = (Window) Executions.createComponents("/rpt/report.zul", null, null);
		win.setTitle("Preview Neraca Aktiva");
		DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);
		TrioMstPerusahaan perusahaan = getMasterFacade().getTrioMstPerusahaanDao().find();
		Map params = new HashMap();
		params.put("NAMA_PERSH", perusahaan.getNamaPers());
		params.put("ALAMAT_PERSH", perusahaan.getAlamatPers());

		Jasperreport report =  (Jasperreport) win.getFellow("report");
		report.setDataConnection(getReportConnection());
		report.setParameters(params);
		report.setType("pdf");
		report.setSrc(file.getAbsolutePath());
	}

	//popup review pasiva
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Command
	public void previewPasiva(){
		DatabaseContextHolder.setConnectionType(ConnectionType.DEFAULT);

		TrioMstkonfigurasi konfig = getMasterFacade().getTrioMstkonfigurasiDao().findByPrimaryKey(new TrioMstkonfigurasi("111"));
		String pathReport = konfig.getNilai();

		File file = new File(pathReport+"\\previewPasiva.jasper");


		Window win = (Window) Executions.createComponents("/rpt/report.zul", null, null);
		win.setTitle("Preview Neraca Aktiva");
		DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);
		TrioMstPerusahaan perusahaan = getMasterFacade().getTrioMstPerusahaanDao().find();
		Map params = new HashMap();
		params.put("NAMA_PERSH", perusahaan.getNamaPers());
		params.put("ALAMAT_PERSH", perusahaan.getAlamatPers());

		Jasperreport report =  (Jasperreport) win.getFellow("report");
		report.setDataConnection(getReportConnection());
		report.setParameters(params);
		report.setType("pdf");
		report.setSrc(file.getAbsolutePath());
	}

	//membuka popup saveFileAktiva
	@Command
	public void saveFileAktiva(@ContextParam(ContextType.VIEW) Component view){
		Executions.createComponents("/popup/popupSaveFileAktiva.zul", view, null);
	}

	//membuka popup saveFilePasiva
	@Command
	public void saveFilePasiva(@ContextParam(ContextType.VIEW) Component view){
		Executions.createComponents("/popup/popupSaveFilePasiva.zul", view, null);
	}

	//membuka popUpRumusTotalAktiva
	@Command
	public void rumusTotalAktiva(@ContextParam(ContextType.VIEW) Component view){
		Executions.createComponents("/popup/popupRumusTotalAktiva.zul", view, null);
	}

	//membuka popUpRumusTotalPasiva
	@Command
	public void rumusTotalPasiva(@ContextParam(ContextType.VIEW) Component view){
		Executions.createComponents("/popup/popupRumusTotalPasiva.zul", view, null);
	}

	//membuka popupRumusTotalAktivaSub
	@Command
	public void rumusTotalAktivaSub(@ContextParam(ContextType.VIEW) Component view){
		Executions.createComponents("/popup/popupRumusTotalAktivaSub.zul", view, null);
	}

	//membuka popupRumusTotalAktivaSub
	@Command
	public void rumusTotalPasivaSub(@ContextParam(ContextType.VIEW) Component view){
		Executions.createComponents("/popup/popupRumusTotalPasivaSub.zul", view, null);
	}

	
	//load laba ditahan pasiva
	@Command
	@NotifyChange("listModelPasiva")
	public void loadLabaDitahan(){
		List<TrioLabaDitahan> listLaba = getMasterFacade().getTrioLabaDitahanDao().findAll();
		List<TrioDesignNeracaAkhirPasiva> listDsgPasiva = getMasterFacade().getTrioDesignNeracaAkhirPasivaDao().findAll();
		List<TrioDesignNeracaAkhirPasiva> listTosave = new ArrayList<TrioDesignNeracaAkhirPasiva>();

		for(TrioLabaDitahan laba : listLaba){
			for(TrioDesignNeracaAkhirPasiva dsgPasiva : listDsgPasiva){
				if(dsgPasiva.getKdJd().equalsIgnoreCase(laba.getTrioLabaDitahanPK().getKdLaba())){
					if(dsgPasiva.getKet().equalsIgnoreCase("1")){
						dsgPasiva.setLevel1(laba.getTotal());
					}else{
						dsgPasiva.setLevel2(laba.getTotal());
					}
					dsgPasiva.setNilai(laba.getTotal());
					listTosave.add(dsgPasiva);
				}
			}
		}
		getMasterFacade().getTrioDesignNeracaAkhirPasivaDao().saveOrUpdateByList(listTosave, getUserSession());
		Messagebox.show("Proses Selesai");
	}

	//prosesHitung di Tab Aktiva
	@Command
	@NotifyChange("listModelAktiva")
	public void prosesHitung(){
		DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);
		tarikTotalAktiva();
		hitungTotalAktiva();
		tarikSubAktiva();
		hitungSubAktiva();
	}


	public void tarikTotalAktiva(){
		List<TrioDesignNeracaAkhirAktiva> listDsg = getMasterFacade().getTrioDesignNeracaAkhirAktivaDao().findAll();
		List<TrioRmsNeracaAktiva> listRmsAktiva = getMasterFacade().getTrioRmsNeracaAktivaDao().findAll();

		for(TrioDesignNeracaAkhirAktiva dsg : listDsg){
			for(TrioRmsNeracaAktiva rms : listRmsAktiva){
				if(rms.getIdPerkiraan().equalsIgnoreCase(dsg.getIdPerkiraan())){
					int x = listRmsAktiva.indexOf(rms);
					rms.setNilai(dsg.getNilai());
					listRmsAktiva.set(x, rms);
				}
			}
		}
		getMasterFacade().getTrioRmsNeracaAktivaDao().saveOrUpdateByList(listRmsAktiva, getUserSession());

	}

	public void hitungTotalAktiva(){
		//ambil object yang sudah di save/update tadi
		List<TrioRmsNeracaAktiva> listAfterUpdate = getMasterFacade().getTrioRmsNeracaAktivaDao().getSumByKdHsl();
		List<TrioDesignNeracaAkhirAktiva> listDsg = getMasterFacade().getTrioDesignNeracaAkhirAktivaDao().findAll();

		//Collection Baru untuk menampung update TrioDesignNeracaAkhirAktiva
		List<TrioDesignNeracaAkhirAktiva> listDsgBaru = new ArrayList<TrioDesignNeracaAkhirAktiva>();

		for(TrioRmsNeracaAktiva nrc : listAfterUpdate){
			for(TrioDesignNeracaAkhirAktiva dsg : listDsg){
				if(dsg.getKdJd().equalsIgnoreCase(nrc.getKdHsl())){
					//if 1
					if(dsg.getKet().equalsIgnoreCase("1")){
						dsg.setLevel1(nrc.getNilai());
					}
					//if 2
					if(dsg.getKet().equalsIgnoreCase("2")){
						dsg.setLevel2(nrc.getNilai());
					}
					dsg.setNilai(nrc.getNilai());
				}
				//insert hasil perubahan ke collection listDsgBaru
				listDsgBaru.add(dsg);
			}
		}
		//execute method saveOrUpdate untuk TrioDesignNeracaAktivaDao
		getMasterFacade().getTrioDesignNeracaAkhirAktivaDao().saveOrUpdateByList(listDsgBaru, getUserSession());

		List<TrioDesignNeracaAkhirAktiva> newListModel = getMasterFacade().getTrioDesignNeracaAkhirAktivaDao().findAll();
		ListModelList<TrioDesignNeracaAkhirAktiva> listModelList = new ListModelList<TrioDesignNeracaAkhirAktiva>();
		listModelList.addAll(newListModel);
		setListModelAktiva(listModelList);
	}

	public void tarikSubAktiva(){
		List<TrioDesignNeracaAkhirAktiva> listDsg = getMasterFacade().getTrioDesignNeracaAkhirAktivaDao().findAll();
		List<TrioRmsNeracaAktivaSub> listRmsNeracaAktivaSub = getMasterFacade().getTrioRmsNeracaAktivaSubDao().findAll();

		for(TrioDesignNeracaAkhirAktiva dsg : listDsg){
			for(TrioRmsNeracaAktivaSub nrc : listRmsNeracaAktivaSub){
				if(nrc.getKdHsl().equalsIgnoreCase(dsg.getKdJd())){
					int index = listRmsNeracaAktivaSub.indexOf(nrc);
					nrc.setNilai(dsg.getNilai());
					listRmsNeracaAktivaSub.set(index, nrc);
				}
			}
		}

		getMasterFacade().getTrioRmsNeracaAktivaSubDao().saveByList(listRmsNeracaAktivaSub, getUserSession());

	}

	public void hitungSubAktiva(){
		List<TrioRmsNeracaAktivaSub> listRmsAktivaSub = getMasterFacade().getTrioRmsNeracaAktivaSubDao().getSumByHasil();
		List<TrioDesignNeracaAkhirAktiva> listDsgAktiva = getMasterFacade().getTrioDesignNeracaAkhirAktivaDao().findAll();
		List<TrioDesignNeracaAkhirAktiva> listDsgBaru = new ArrayList<TrioDesignNeracaAkhirAktiva>();

		for(TrioRmsNeracaAktivaSub nrc : listRmsAktivaSub){
			for(TrioDesignNeracaAkhirAktiva dsg : listDsgAktiva){
				if(dsg.getKdJd().equalsIgnoreCase(nrc.getHasil())){
					if(dsg.getKet().equalsIgnoreCase("1")){
						dsg.setLevel1(nrc.getNilai());
					}
					if(dsg.getKet().equalsIgnoreCase("2")){
						dsg.setLevel2(nrc.getNilai());
					}
					dsg.setNilai(nrc.getNilai());
				}
				listDsgBaru.add(dsg);
			}
		}

		getMasterFacade().getTrioDesignNeracaAkhirAktivaDao().saveOrUpdateByList(listDsgBaru, getUserSession());

		List<TrioDesignNeracaAkhirAktiva> newListDsg = getMasterFacade().getTrioDesignNeracaAkhirAktivaDao().findAll();
		ListModelList<TrioDesignNeracaAkhirAktiva> newListModel = new ListModelList<TrioDesignNeracaAkhirAktiva>();
		newListModel.addAll(newListDsg);
		setListModelAktiva(newListModel);
	}


	//prosesHitung di Tab Pasiva
	@Command
	@NotifyChange("listModelPasiva")
	public void prosesHitungPasiva(){
		System.out.println("Hitung Pasiva");
		tarikTotalPasiva();
		hitungTotalpasiva();
		tarikSubPasiva();
		hitungSubPasiva();

	}

	public void tarikTotalPasiva(){
		List<TrioDesignNeracaAkhirPasiva> listDsgPasiva = getMasterFacade().getTrioDesignNeracaAkhirPasivaDao().findAll();
		List<TrioRmsNeracaPasiva> listNrcPasiva = getMasterFacade().getTrioRmsNeracaPasivaDao().findAll();

		for(TrioDesignNeracaAkhirPasiva dsg : listDsgPasiva){
			for(TrioRmsNeracaPasiva nrc : listNrcPasiva){
				if(nrc.getIdPerkiraan().equalsIgnoreCase(dsg.getIdPerkiraan())){
					int x = listNrcPasiva.indexOf(nrc);
					nrc.setNilai(dsg.getNilai());
					listNrcPasiva.set(x, nrc);
				}
			}
		}
		getMasterFacade().getTrioRmsNeracaPasivaDao().saveByList(listNrcPasiva, getUserSession());
	}

	public void hitungTotalpasiva(){
		List<TrioRmsNeracaPasiva> listRmsPasiva = getMasterFacade().getTrioRmsNeracaPasivaDao().getSumByKdHsl();
		List<TrioDesignNeracaAkhirPasiva> listDsgPasiva = getMasterFacade().getTrioDesignNeracaAkhirPasivaDao().findAll();
		List<TrioDesignNeracaAkhirPasiva> listDsgBaru = new ArrayList<TrioDesignNeracaAkhirPasiva>();

		for(TrioRmsNeracaPasiva nrc : listRmsPasiva){
			for(TrioDesignNeracaAkhirPasiva dsg : listDsgPasiva){
				if(dsg.getKdJd().equalsIgnoreCase(nrc.getKdHsl())){
					if(dsg.getKet().equalsIgnoreCase("1")){
						dsg.setLevel1(nrc.getNilai());
					}
					if(dsg.getKet().equalsIgnoreCase("2")){
						dsg.setLevel2(nrc.getNilai());
					}
					dsg.setNilai(nrc.getNilai());
				}
				listDsgBaru.add(dsg);
			}
		}

		getMasterFacade().getTrioDesignNeracaAkhirPasivaDao().saveOrUpdateByList(listDsgBaru, getUserSession());

		List<TrioDesignNeracaAkhirPasiva> newListToAdd = getMasterFacade().getTrioDesignNeracaAkhirPasivaDao().findAll();
		ListModelList<TrioDesignNeracaAkhirPasiva> newListModelToAdd = new ListModelList<TrioDesignNeracaAkhirPasiva>();
		newListModelToAdd.addAll(newListToAdd);
		setListModelPasiva(newListModelToAdd);

	}

	public void tarikSubPasiva(){
		List<TrioDesignNeracaAkhirPasiva> listDsgPasiva = getMasterFacade().getTrioDesignNeracaAkhirPasivaDao().findAll();
		List<TrioRmsNeracaPasivaSub> listNrcPasivaSub = getMasterFacade().getTrioRmsNeracaPasivaSubDao().findAll();

		for(TrioDesignNeracaAkhirPasiva dsg : listDsgPasiva){
			for(TrioRmsNeracaPasivaSub nrc : listNrcPasivaSub){
				if(nrc.getKdHsl().equalsIgnoreCase(dsg.getKdJd())){
					int x = listNrcPasivaSub.indexOf(nrc);
					nrc.setNilai(dsg.getNilai());
					listNrcPasivaSub.set(x, nrc);
				}
			}
		}
		getMasterFacade().getTrioRmsNeracaPasivaSubDao().saveByCollection(listNrcPasivaSub, getUserSession());
	}

	public void hitungSubPasiva(){
		List<TrioRmsNeracaPasivaSub> listsRmsPasivaSub = getMasterFacade().getTrioRmsNeracaPasivaSubDao().getSumByHasil();
		List<TrioDesignNeracaAkhirPasiva> listDsgPasiva = getMasterFacade().getTrioDesignNeracaAkhirPasivaDao().findAll();
		List<TrioDesignNeracaAkhirPasiva> listDsgToSave = new ArrayList<TrioDesignNeracaAkhirPasiva>();

		for(TrioRmsNeracaPasivaSub nrc : listsRmsPasivaSub){
			for(TrioDesignNeracaAkhirPasiva dsg : listDsgPasiva){
				if(dsg.getKdJd().equalsIgnoreCase(nrc.getHasil())){
					if(dsg.getKet().equalsIgnoreCase("1")){
						dsg.setLevel1(nrc.getNilai());
					}
					if(dsg.getKet().equalsIgnoreCase("2")){
						dsg.setLevel2(nrc.getNilai());
					}
					dsg.setNilai(nrc.getNilai());
				}
				listDsgToSave.add(dsg);
			}
		}

		getMasterFacade().getTrioDesignNeracaAkhirPasivaDao().saveOrUpdateByList(listDsgToSave, getUserSession());

		List<TrioDesignNeracaAkhirPasiva> newListToAdd = getMasterFacade().getTrioDesignNeracaAkhirPasivaDao().findAll();
		ListModelList<TrioDesignNeracaAkhirPasiva> newListModelToAdd = new ListModelList<TrioDesignNeracaAkhirPasiva>();
		newListModelToAdd.addAll(newListToAdd);
		setListModelPasiva(newListModelToAdd);
	}

	//menangkap data yang dikirim oleh popupLoadFile
	@NotifyChange({"listModelAktiva","listModelPasiva"})
	@Command
	public void sendListModel(@BindingParam("aktivaModel") List<TrioDesignNeracaAkhirAktiva> listAktiva,
			@BindingParam("pasivaModel") List<TrioDesignNeracaAkhirPasiva> listPasiva){

		//Set Aktiva ListModel
		ListModelList<TrioDesignNeracaAkhirAktiva> newAktivaModel = new ListModelList<TrioDesignNeracaAkhirAktiva>();
		newAktivaModel.addAll(listAktiva);
		setListModelAktiva(newAktivaModel);

		//Set Pasiva ListModel
		ListModelList<TrioDesignNeracaAkhirPasiva> newPasivaModel = new ListModelList<TrioDesignNeracaAkhirPasiva>();
		newPasivaModel.addAll(listPasiva);
		setListModelPasiva(newPasivaModel);
	}
}
