package com.trio.gl.viewmodel.dsg;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;

import ar.com.fdvs.dj.core.DynamicJasperHelper;
import ar.com.fdvs.dj.core.layout.ClassicLayoutManager;
import ar.com.fdvs.dj.domain.DynamicReport;
import ar.com.fdvs.dj.domain.builders.ColumnBuilderException;
import ar.com.fdvs.dj.domain.builders.FastReportBuilder;

import com.trio.gl.base.TrioBasePageVM;
import com.trio.gl.bean.TrioDesignRugiLaba;
import com.trio.gl.bean.TrioRumus;
import com.trio.gl.bean.TrioRumus2;
import com.trio.gl.helper.bean.CurTmpRl;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;

public class DsgRugiLabaVM extends TrioBasePageVM {

	private ListModelList<TrioDesignRugiLaba> listRugiLaba;

	public ListModelList<TrioDesignRugiLaba> getListRugiLaba() {
		DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);
		if(listRugiLaba==null){
			listRugiLaba = new ListModelList<TrioDesignRugiLaba>();
			List<TrioDesignRugiLaba> list = getMasterFacade().getTrioDesignRugiLabaDao().findAll();
			listRugiLaba.addAll(list);
		}

		return listRugiLaba;
	}

	@NotifyChange("listRugiLaba")
	public void setListRugiLaba(ListModelList<TrioDesignRugiLaba> listRugiLaba) {
		this.listRugiLaba = listRugiLaba;
	}


	@Command
	@NotifyChange("listRugiLaba")
	public void newRugiLaba(){
		for(TrioDesignRugiLaba current : getListRugiLaba()){
			current.setLevel1(new BigDecimal(0));
			current.setLevel2(new BigDecimal(0));
			current.setLevel3(new BigDecimal(0));
			current.setLevel4(new BigDecimal(0));
			current.setNilai(new BigDecimal(0));
		}
		getMasterFacade().getTrioDesignRugiLabaDao().updateByList(getListRugiLaba(), getUserSession());
	}

	//loadNilai
	@Command
	public void loadNilai(@ContextParam(ContextType.VIEW) Component view){
		Executions.createComponents("/popup/popupLoadNilaiRL.zul", view, null);
	}

	//saveFile rugiLaba
	@Command
	public void saveRugiLaba(@ContextParam(ContextType.VIEW) Component view){
		Executions.createComponents("/popup/popupSaveRugiLaba.zul", view, null);
	}

	//createFile excel
	@Command
	public void createFile() throws ColumnBuilderException, ClassNotFoundException, JRException, FileNotFoundException{
		List<TrioDesignRugiLaba> lists = getMasterFacade().getTrioDesignRugiLabaDao().rlExcel();
		List<CurTmpRl> listTmpRl = new ArrayList<CurTmpRl>();

		String dash = "-";
		String ket1 = "1";
		String ket2 = "2";
		String ket3 = "3";
		String ket4 = "4";

		for(TrioDesignRugiLaba current : lists){
			CurTmpRl cur = new CurTmpRl();

			if(current.getNamaPerkiraan().equalsIgnoreCase("-")){
				cur.setKet(dash);

				if(ket1.equals(current.getKet())){
					cur.setKet1(current.getKet2());
				}

				if(ket2.equals(current.getKet())){
					cur.setKet2(current.getKet2());
				}

				if(ket3.equals(current.getKet())){
					cur.setKet3(current.getKet2());
				}

				if(ket4.equals(current.getKet())){
					cur.setKet4(current.getKet2());
				}
			}else{
				cur.setKet(current.getNamaPerkiraan());
			}
			cur.setNilai1(current.getLevel1());
			cur.setNilai2(current.getLevel2());
			cur.setNilai3(current.getLevel3());
			cur.setNilai4(current.getLevel4());

			listTmpRl.add(cur);
		}



		FastReportBuilder drb = new FastReportBuilder();
		drb.addColumn("Ket", "ket", String.class.getName(),200);
		drb.addColumn("Nilai 2", "nilai2", BigDecimal.class.getName(),130,true,"###,##0.00");
		drb.addColumn("Ket 2", "ket2", String.class.getName(),70);
		drb.addColumn("Nilai 3", "nilai3", BigDecimal.class.getName(),130,true,"###,##0.00");
		drb.addColumn("Ket 3", "ket3", String.class.getName(),70);
		drb.addColumn("Nilai 4","nilai4", BigDecimal.class.getName(),130,true,"###,##0.00" );
		drb.addColumn("Ket 4", "ket4", String.class.getName(),70);
		drb.setPrintColumnNames(true);
		drb.setIgnorePagination(true);
		drb.setTitle("Designer Rugi Laba");

		DynamicReport dr = drb.build();
		JasperPrint jp = DynamicJasperHelper.generateJasperPrint(dr, new ClassicLayoutManager(), listTmpRl);

		File file = new File("C:/lap_rl_new.xls");
		FileOutputStream fos = new FileOutputStream(file);

		JRXlsExporter export = new JRXlsExporter();
		export.setParameter(JRExporterParameter.JASPER_PRINT, jp);
		export.setParameter(JRExporterParameter.OUTPUT_STREAM, fos);
		export.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
		export.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, Boolean.TRUE);
		export.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
		export.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
		export.exportReport();

		Messagebox.show("File berhahasil dibuat di " + file.getAbsolutePath());
	}

	//popup Rumus1
	@Command
	public void rumus1(@ContextParam(ContextType.VIEW) Component view){
		Executions.createComponents("/popup/popupRumus1RugiLaba.zul/", view, null);
	}

	//popup Rumus2
	@Command
	public void rumus2(@ContextParam(ContextType.VIEW) Component view){
		Executions.createComponents("/popup/popupRumus2RugiLaba.zul/", view, null);
	}

	//popup prosesHitung
	@Command
	@NotifyChange("listRugiLaba")
	public void prosesHitung(){
		List<TrioDesignRugiLaba> listRl = getMasterFacade().getTrioDesignRugiLabaDao().findAll();
		List<TrioDesignRugiLaba> listRlTosave = new ArrayList<TrioDesignRugiLaba>();

		for(TrioDesignRugiLaba rl : listRl){
			rl.setLevel1(rl.getTmp1());
			rl.setLevel2(rl.getTmp2());
			rl.setLevel3(rl.getTmp3());
			rl.setLevel4(rl.getTmp4());
			listRlTosave.add(rl);
		}
		getMasterFacade().getTrioDesignRugiLabaDao().saveOrUpdateByList(listRlTosave, getUserSession());

		tarik1();
		hitung1();
		tarik2();
		hitung2();
		
		
		List<TrioDesignRugiLaba> listSaveTmp = getMasterFacade().getTrioDesignRugiLabaDao().findAll();
		for(TrioDesignRugiLaba rl : listSaveTmp){
			rl.setTmp1(rl.getLevel1());
			rl.setTmp2(rl.getLevel2());
			rl.setTmp3(rl.getLevel3());
			rl.setTmp4(rl.getLevel4());
			rl.setShowDtl(new BigDecimal(0));
			rl.setShows("*");
			listRlTosave.add(rl);
		}
		getMasterFacade().getTrioDesignRugiLabaDao().saveOrUpdateByList(listSaveTmp, getUserSession());
		
		
		ListModelList<TrioDesignRugiLaba> lm = new ListModelList<TrioDesignRugiLaba>();
		lm.addAll(getMasterFacade().getTrioDesignRugiLabaDao().findAll());
		setListRugiLaba(lm);
	}

	public void tarik1(){
		List<TrioDesignRugiLaba> listRl = getMasterFacade().getTrioDesignRugiLabaDao().findAll();
		List<TrioRumus> listRumus = getMasterFacade().getTrioRumusDao().findAll();
		List<TrioDesignRugiLaba> listRlToSave = new ArrayList<TrioDesignRugiLaba>();
		List<TrioRumus> listRmsToSave = new ArrayList<TrioRumus>();

		for(TrioDesignRugiLaba rl : listRl){
			if(rl.getLevel1().compareTo(new BigDecimal(0)) != 0){
				rl.setNilai(rl.getLevel1());
			}

			if(rl.getLevel2().compareTo(new BigDecimal(0)) != 0){
				rl.setNilai(rl.getLevel2());
			}

			if(rl.getLevel3().compareTo(new BigDecimal(0)) != 0){
				rl.setNilai(rl.getLevel3());
			}

			if(rl.getLevel4().compareTo(new BigDecimal(0)) != 0){
				rl.setNilai(rl.getLevel4());
			}
			
			listRlToSave.add(rl);
		}
		getMasterFacade().getTrioDesignRugiLabaDao().updateByList(listRlToSave, getUserSession());
		
		
		for(TrioDesignRugiLaba rl : listRl){
			for(TrioRumus rms : listRumus){
				if(rms.getIdPerkiraan().equalsIgnoreCase(rl.getIdPerkiraan())){
					if(rms.getJenis() != '-'){
						rms.setNilai(rl.getNilai());
					}else{
						rms.setNilai(rl.getNilai().multiply(new BigDecimal(-1)));
					}
				}
				listRmsToSave.add(rms);
			}
		}
		
		getMasterFacade().getTrioRumusDao().saveOrUpdateByList(listRmsToSave, getUserSession());
	}

	public void tarik2(){
		List<TrioDesignRugiLaba> listRl = getMasterFacade().getTrioDesignRugiLabaDao().findAll();
		List<TrioRumus2> listRumus2 = getMasterFacade().getTrioRumus2Dao().findAll();
		List<TrioDesignRugiLaba> listRlToSave = new ArrayList<TrioDesignRugiLaba>();
		List<TrioRumus2> listRms2ToSave = new ArrayList<TrioRumus2>();

		for(TrioDesignRugiLaba current : listRl){
			if(current.getLevel1().compareTo(new BigDecimal(0)) != 0){
				current.setNilai(current.getLevel1());
			}

			if(current.getLevel2().compareTo(new BigDecimal(0)) != 0){
				current.setNilai(current.getLevel2());
			}

			if(current.getLevel3().compareTo(new BigDecimal(0)) != 0){
				current.setNilai(current.getLevel3());
			}

			if(current.getLevel4().compareTo(new BigDecimal(0)) != 0){
				current.setNilai(current.getLevel4());
			}
			listRlToSave.add(current);
		}
		getMasterFacade().getTrioDesignRugiLabaDao().updateByList(listRlToSave, getUserSession());
		

		for(TrioDesignRugiLaba current : listRl){
			for(TrioRumus2 rms2 : listRumus2){
				if(rms2.getIdPerkiraan().equalsIgnoreCase(current.getKdJD())){
					if(rms2.getJenis() != '-'){
						rms2.setNilai(current.getNilai());
					}else{
						rms2.setNilai(current.getNilai().multiply(new BigDecimal(-1)));
					}
				}
				
				listRms2ToSave.add(rms2);
			}
		}
		
		getMasterFacade().getTrioRumus2Dao().saveOrUpdateByList(listRms2ToSave, getUserSession());
	}

	public void hitung1(){
		List<TrioRumus> listsRms = getMasterFacade().getTrioRumusDao().listSumNilai();
		List<TrioDesignRugiLaba> listRl = getMasterFacade().getTrioDesignRugiLabaDao().findAll();
		List<TrioDesignRugiLaba> listRlToSave = new ArrayList<TrioDesignRugiLaba>();
		
		for(TrioRumus rms : listsRms){
			for(TrioDesignRugiLaba rl : listRl){
				if(rl.getKdJD().equalsIgnoreCase(rms.getKdHsl())){

					if(rl.getKet().equalsIgnoreCase("1")){
						rl.setLevel1(rms.getNilai());
					}

					if(rl.getKet().equalsIgnoreCase("2")){
						rl.setLevel2(rms.getNilai());
					}

					if(rl.getKet().equalsIgnoreCase("3")){
						rl.setLevel3(rms.getNilai());
					}

					if(rl.getKet().equalsIgnoreCase("4")){
						rl.setLevel4(rms.getNilai());
					}

				}
				listRlToSave.add(rl);
			}
		}
		
		getMasterFacade().getTrioDesignRugiLabaDao().updateByList(listRlToSave, getUserSession());
	}

	public void hitung2(){
		List<TrioRumus2> listRms = getMasterFacade().getTrioRumus2Dao().listSumNilai();
		List<TrioDesignRugiLaba> listsRl = getMasterFacade().getTrioDesignRugiLabaDao().findAll();
		List<TrioDesignRugiLaba> listRlToSave = new ArrayList<TrioDesignRugiLaba>();

		for(TrioRumus2 rms2 : listRms){
			for(TrioDesignRugiLaba rl : listsRl){
				if(rl.getKdJD().equalsIgnoreCase(rms2.getKdHsl())){

					if(rl.getKet().equalsIgnoreCase("1")){
						rl.setLevel1(rms2.getNilai());
					}

					if(rl.getKet().equalsIgnoreCase("2")){
						rl.setLevel2(rms2.getNilai());
					}

					if(rl.getKet().equalsIgnoreCase("3")){
						rl.setLevel3(rms2.getNilai());
					}

					if(rl.getKet().equalsIgnoreCase("4")){
						rl.setLevel4(rms2.getNilai());
					}
				}
				listRlToSave.add(rl);
			}
		}

		getMasterFacade().getTrioDesignRugiLabaDao().updateByList(listRlToSave, getUserSession());
	}

	@Command
	public void showLabaDitahan(@ContextParam(ContextType.VIEW) Component view){
		Executions.createComponents("/popup/popupLabaDitahanRl.zul/", view, null);
	}


	//menangkap nilai dari hasil popupLoadNilai
	@NotifyChange("listRugiLaba")
	@Command
	public void sendListModel(@BindingParam("rugiLabaModel") List<TrioDesignRugiLaba> listRugiLaba){
		ListModelList<TrioDesignRugiLaba> listModelRugiLaba = new ListModelList<TrioDesignRugiLaba>();
		listModelRugiLaba.addAll(listRugiLaba);
		setListRugiLaba(listModelRugiLaba);
	}
}
