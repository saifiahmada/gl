package com.trio.gl.test;

/** @author Saifi Ahmada Mar 21, 2013 2:07:23 PM  **/

public class CellCnt {
	private int _index;
	private boolean _checked;
	public CellCnt (int index, boolean checked) {
		_index = index;
		_checked = checked;
	}
	public void setIndex (int index) {
		_index = index;
	}
	public int getIndex () {
		return _index;
	}
	public void setChecked (boolean checked) {
		_checked = checked;
	}
	public boolean getChecked () {
		return _checked;
	}
}

