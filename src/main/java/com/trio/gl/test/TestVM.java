package com.trio.gl.test;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.event.CheckEvent;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listcell;

/** @author Saifi Ahmada Mar 21, 2013 2:05:58 PM  **/

public class TestVM {
	@SuppressWarnings("rawtypes")
	private ListModelList _model;
	private int _currentIndex = 0; // increase forever
	private String _msg = "";

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ListModelList getModel () {
		if (_model == null) {
			List l = new ArrayList();
			_model = new ListModelList(l);
		}
		return _model;
	}
	public String getMsg () {
		return _msg;
	}
	@SuppressWarnings("unchecked")
	@Command
	@NotifyChange({"model", "msg"})
	public void addRow () {
		_currentIndex += 1;
		_model.add(new CellCnt(_currentIndex, false));
		_msg = "Row " + _currentIndex + " is created";
	}
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Command
	@NotifyChange({"model", "msg"})
	public void removeSelected () {
		List tmp = new ArrayList();
		for (int i = 0; i < _model.getSize(); i++) {
			if (!((CellCnt)_model.getElementAt(i)).getChecked()) {
				tmp.add(_model.getElementAt(i));
			}
		}
		_model = new ListModelList(tmp);
		_msg = "Checked rows are deleted.";
	}
	@Command
	@NotifyChange({"msg"})
	public void updateCheckedMsg (@ContextParam(ContextType.TRIGGER_EVENT) CheckEvent event) {
		String label = ((Listcell)event.getTarget().getParent().getPreviousSibling()).getLabel();
		_msg = "checkbox " + label + " is "
				+ (event.isChecked()? "checked" : "unchecked");
	}
}

