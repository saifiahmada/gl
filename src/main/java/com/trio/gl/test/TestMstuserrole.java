package com.trio.gl.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.DataIntegrityViolationException;

import com.trio.gl.bean.TrioMstuserrole;
import com.trio.gl.dao.TrioMstuserroleDao;
import com.trio.gl.facade.MasterFacade;

/** @author Saifi Ahmada Feb 8, 2013 3:35:57 PM  **/

public class TestMstuserrole {
	
	public static void main(String[] args) {
		System.out.println("Mulai");
		
		ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml");
		MasterFacade mf = (MasterFacade) ac.getBean("masterFacade");
		
		TrioMstuserroleDao dao = mf.getTrioMstuserroleDao();
		
		TrioMstuserrole userrole = new TrioMstuserrole("arya", "ROLE01");
		userrole.setVstat("A");
		try {
			dao.save(userrole, "saifi");
		}catch (DataIntegrityViolationException	cve) {
			System.out.println("ada data yg sama bos");
			//cve.printStackTrace();
		}
		
	}
}

