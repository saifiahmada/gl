package com.trio.gl.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.trio.gl.bean.TrioMstmenu;
import com.trio.gl.dao.TrioMstmenuDao;
import com.trio.gl.facade.MasterFacade;

/** @author Saifi Ahmada Feb 7, 2013 2:59:01 PM  **/

public class TestMenu {
	
	public static void main(String[] args) {
		
		System.out.println("Mulai");
		
		//ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml");
		ApplicationContext ac = new FileSystemXmlApplicationContext("/applicationContext.xml");
		
		
		MasterFacade mf = (MasterFacade) ac.getBean("masterFacade");
		
		TrioMstmenuDao dao =  mf.getTrioMstmenuDao();
		
		TrioMstmenu menu = new TrioMstmenu("T10GLMST002");
		
		menu.setNorderer(2);
		menu.setVimage("/image/image.jpg");
		menu.setVlocation("mstCabang.zul");
		menu.setVparent("parent");
		menu.setVtitle("Master Perkiraan");
		
		dao.save(menu, "saifi");
		
		System.out.println("sukses");
	}

}

