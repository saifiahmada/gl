package com.trio.gl.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.criteria.CriteriaBuilder.In;
import javax.print.attribute.HashAttributeSet;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.trio.gl.bean.DKHelper;
import com.trio.gl.bean.TrioKumpul;
import com.trio.gl.bean.TrioLapNeraca;
import com.trio.gl.bean.TrioMstsaldosubperkiraan;
import com.trio.gl.bean.TrioMstsubperkiraan;
import com.trio.gl.bean.TrioNeracaCoba;
import com.trio.gl.facade.MasterFacade;
import com.trio.gl.spring.ConnectionType;
import com.trio.gl.spring.DatabaseContextHolder;

public class TestDaoObject {

	public static void main(String[] args) {

		DatabaseContextHolder.setConnectionType(ConnectionType
				.setType("PERINTIS"));

		// ApplicationContext ac = new
		// FileSystemXmlApplicationContext("file:/home/saifi/workspace/gl/src/applicationContext.xml");
		// MasterFacade mf = (MasterFacade) ac.getBean("masterFacade");
		// System.out.println("Perintis");
		// String periode = "201309";
		// List<TrioKumpul> listKumpul =
		// mf.getTrioLapBukubesarDao().getListKumpulByPeriode(periode);
		// mf.getTrioKumpulDao().saveAll(listKumpul, "saifi");

		List<DKHelper> list = new ArrayList<DKHelper>();

		list.add(new DKHelper("001"));
		list.add(new DKHelper("001"));
		list.add(new DKHelper("002"));
		list.add(new DKHelper("003"));
		list.add(new DKHelper("004"));
		list.add(new DKHelper("004"));
		list.add(new DKHelper("004"));

		Map<String, Integer> map = new HashMap<String, Integer>();
		for (DKHelper a : list) {
			if (map.containsKey(a.getIdPerkiraan())) {
				map.put(a.getIdPerkiraan(), map.get(a.getIdPerkiraan()) + 1);
			} else {
				map.put(a.getIdPerkiraan(), 1);
			}
		}

		for (String key : map.keySet()) {
			System.out.println("key = " + key + " berjumlah  " + map.get(key));
		}

	}

}
