package com.trio.gl.view.ctrl;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zul.Button;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;

import com.trio.gl.base.TrioBasePage;
import com.trio.gl.util.LoginManager;
import com.trio.gl.util.PasswdUtil;

public class LoginCtrl extends TrioBasePage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	Textbox nameTxb, passwordTxb;
	
	Button confirmBtn, cancelBtn;
	
	Combobox cbCabang;
	
	@SuppressWarnings("unchecked")
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		nameTxb.setFocus(true);
	
	}
	
	public void onClick$confirmBtn() throws InterruptedException{ 
		/*
		 * validation empty 
		 */
		
		if (nameTxb.getValue().equalsIgnoreCase("") || nameTxb.getValue() == null){
			Messagebox.show("Username harus diisi");
			return;
		}
		if (passwordTxb.getValue().equalsIgnoreCase("") || passwordTxb.getValue() == null){
			Messagebox.show("Password harus diisi");
			return;
		}
		doLogin();
	}
	
	public void onClick$cancelBtn(){
		nameTxb.setValue(null);
		passwordTxb.setValue(null);
		nameTxb.setFocus(true);
	}

		
//	public void onOK(){
//		
//		doLogin();
//	}
	
	public void doLogin(){
		
		LoginManager lm = LoginManager.getIntance(Sessions.getCurrent());
		/*
		 * mendaftarkan username ke session
		 * @param username
		 * @return nameTxb.getValue()
		 */
		Sessions.getCurrent().setAttribute("username", nameTxb.getValue());
		Sessions.getCurrent().setAttribute("cabang", cbCabang.getSelectedItem().getValue());
		
		lm.logIn(nameTxb.getValue(),getEnkripsiByUserAndPass(nameTxb.getValue(), passwordTxb.getValue()));
		if(lm.isAuthenticated()){
			execution.sendRedirect("index.zul");
			//DatabaseContextHolder.setConnectionType(ConnectionType.PERINTIS);
			//System.out.println("Koneksi :: "+DatabaseContextHolder.getConnectionType()); 
			
		}else {
				Messagebox.show("Username/Password anda salah", "Warning", Messagebox.OK,
						Messagebox.EXCLAMATION);
		}
	}
	
	public void onOK(){
		doLogin();
	}
	
	public String getEnkripsiByUserAndPass(String userName, String password){
		String plain = userName+password;
		String chiper = PasswdUtil.getMD5DigestFromString(plain);
		return chiper;
	}
	
}
