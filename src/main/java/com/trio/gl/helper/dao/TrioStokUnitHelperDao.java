package com.trio.gl.helper.dao;

import com.trio.gl.helper.bean.TrioStokUnitHelper;
import com.trio.gl.hibernate.TrioGenericDao;

/**
 * @author Gusti Arya 4:16:21 PM Jun 24, 2013
 */

public interface TrioStokUnitHelperDao extends TrioGenericDao<TrioStokUnitHelper>{
	
	
}
