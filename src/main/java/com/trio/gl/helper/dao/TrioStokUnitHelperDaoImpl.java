package com.trio.gl.helper.dao;

import java.util.List;

import org.hibernate.SQLQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trio.gl.helper.bean.TrioStokUnitHelper;
import com.trio.gl.hibernate.TrioHibernateDaoSupport;

/**
 * @author Gusti Arya 4:17:09 PM Jun 24, 2013
 */

@Service
public class TrioStokUnitHelperDaoImpl extends TrioHibernateDaoSupport implements TrioStokUnitHelperDao{

	public void saveOrUpdate(TrioStokUnitHelper domain, String user) {
		// TODO Auto-generated method stub

	}

	@Transactional(readOnly=false)
	public void save(TrioStokUnitHelper domain, String user) {
		getHibernateTemplate().getSessionFactory().getCurrentSession().save(domain);
	}

	public void update(TrioStokUnitHelper domain, String user) {
		// TODO Auto-generated method stub

	}

	@Transactional(readOnly=false)
	public void delete(TrioStokUnitHelper domain) {
		String hql = "DELETE FROM TRIO_STOK_UNIT_HELPER";
		SQLQuery sQuery = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(hql);
		sQuery.executeUpdate();
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<TrioStokUnitHelper> findAll() {
		// TODO Auto-generated method stub
		return getHibernateTemplate().find("from TrioStokUnitHelper");
	}

	public List<TrioStokUnitHelper> findByExample(TrioStokUnitHelper domain) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<TrioStokUnitHelper> findByCriteria(TrioStokUnitHelper domain) {
		// TODO Auto-generated method stub
		return null;
	}

	public TrioStokUnitHelper findByPrimaryKey(TrioStokUnitHelper domain) {
		// TODO Auto-generated method stub
		return null;
	}

}
