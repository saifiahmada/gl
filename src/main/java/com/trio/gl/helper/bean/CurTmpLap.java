package com.trio.gl.helper.bean;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Gusti Arya 4:57:55 PM Jul 19, 2013
 */

public class CurTmpLap {

	private String no;
	private Date tgl;
	private String noMesin;
	private String nama;
	private BigDecimal hStnk ;
	private String noUrus;
	private Date tglKredit;
	private BigDecimal pengStnk;
	private BigDecimal selisih;
	private BigDecimal saldo;
	private String noFak;
	
	public CurTmpLap() {
		// TODO Auto-generated constructor stub
	}
	
	public CurTmpLap(String no, Date tgl, String noMesin, String nama,
			BigDecimal hStnk, String noUrus, Date tglKredit,
			BigDecimal pengStnk, BigDecimal selisih, BigDecimal saldo, String noFak) {
		super();
		this.no = no;
		this.tgl = tgl;
		this.noMesin = noMesin;
		this.nama = nama;
		this.hStnk = hStnk;
		this.noUrus = noUrus;
		this.tglKredit = tglKredit;
		this.pengStnk = pengStnk;
		this.selisih = selisih;
		this.saldo = saldo;
		this.noFak = noFak;
	}
	

	public String getNo() {
		return no;
	}
	public void setNo(String no) {
		this.no = no;
	}
	public Date getTgl() {
		return tgl;
	}
	public void setTgl(Date tgl) {
		this.tgl = tgl;
	}
	public String getNoMesin() {
		return noMesin;
	}
	public void setNoMesin(String noMesin) {
		this.noMesin = noMesin;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public BigDecimal gethStnk() {
		return hStnk;
	}
	public void sethStnk(BigDecimal hStnk) {
		this.hStnk = hStnk;
	}
	public String getNoUrus() {
		return noUrus;
	}
	public void setNoUrus(String noUrus) {
		this.noUrus = noUrus;
	}
	public Date getTglKredit() {
		return tglKredit;
	}
	public void setTglKredit(Date tglKredit) {
		this.tglKredit = tglKredit;
	}
	public BigDecimal getPengStnk() {
		return pengStnk;
	}
	public void setPengStnk(BigDecimal pengStnk) {
		this.pengStnk = pengStnk;
	}
	public BigDecimal getSelisih() {
		return selisih;
	}
	public void setSelisih(BigDecimal selisih) {
		this.selisih = selisih;
	}
	public BigDecimal getSaldo() {
		return saldo;
	}
	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}
	
	
	
}
