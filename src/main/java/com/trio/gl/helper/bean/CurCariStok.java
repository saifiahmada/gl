package com.trio.gl.helper.bean;

import java.util.Date;

/**
 * @author Gusti Arya 4:36:54 PM Jul 5, 2013
 */

public class CurCariStok {

	private Date tgl;
	private String noMesin;
	private String noRangka;
	private String kdItem;
	private String noFakIn;
	private String noSjIn;
	private String noFakOut;
	private int masuk;
	private int keluar;
	
	public CurCariStok() {
		// TODO Auto-generated constructor stub
	}
	

	public CurCariStok(Date tgl, String noMesin, String noRangka,
			String kdItem, String noFakIn, String noSjIn, String noFakOut,
			int masuk, int keluar) {
		super();
		this.tgl = tgl;
		this.noMesin = noMesin;
		this.noRangka = noRangka;
		this.kdItem = kdItem;
		this.noFakIn = noFakIn;
		this.noSjIn = noSjIn;
		this.noFakOut = noFakOut;
		this.masuk = masuk;
		this.keluar = keluar;
	}



	public Date getTgl() {
		return tgl;
	}
	public void setTgl(Date tgl) {
		this.tgl = tgl;
	}
	public String getNoMesin() {
		return noMesin;
	}
	public void setNoMesin(String noMesin) {
		this.noMesin = noMesin;
	}
	public String getNoRangka() {
		return noRangka;
	}
	public void setNoRangka(String noRangka) {
		this.noRangka = noRangka;
	}
	public String getKdItem() {
		return kdItem;
	}
	public void setKdItem(String kdItem) {
		this.kdItem = kdItem;
	}
	public String getNoFakIn() {
		return noFakIn;
	}
	public void setNoFakIn(String noFakIn) {
		this.noFakIn = noFakIn;
	}
	public String getNoFakOut() {
		return noFakOut;
	}
	public void setNoFakOut(String noFakOut) {
		this.noFakOut = noFakOut;
	}
	public int getMasuk() {
		return masuk;
	}
	public void setMasuk(int masuk) {
		this.masuk = masuk;
	}
	public int getKeluar() {
		return keluar;
	}
	public void setKeluar(int keluar) {
		this.keluar = keluar;
	}

	public String getNoSjIn() {
		return noSjIn;
	}

	public void setNoSjIn(String noSjIn) {
		this.noSjIn = noSjIn;
	}
	
}
