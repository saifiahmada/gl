package com.trio.gl.helper.bean;

import java.math.BigDecimal;

public class CurTmpRl {
	
	private String ket;
	private String ket1;
	private String ket2;
	private String ket3;
	private String ket4;
	private BigDecimal nilai1;
	private BigDecimal nilai2;
	private BigDecimal nilai3;
	private BigDecimal nilai4;
	public String getKet() {
		return ket;
	}
	public void setKet(String ket) {
		this.ket = ket;
	}
	public String getKet1() {
		return ket1;
	}
	public void setKet1(String ket1) {
		this.ket1 = ket1;
	}
	public String getKet2() {
		return ket2;
	}
	public void setKet2(String ket2) {
		this.ket2 = ket2;
	}
	public String getKet3() {
		return ket3;
	}
	public void setKet3(String ket3) {
		this.ket3 = ket3;
	}
	public String getKet4() {
		return ket4;
	}
	public void setKet4(String ket4) {
		this.ket4 = ket4;
	}
	public BigDecimal getNilai1() {
		return nilai1;
	}
	public void setNilai1(BigDecimal nilai1) {
		this.nilai1 = nilai1;
	}
	public BigDecimal getNilai2() {
		return nilai2;
	}
	public void setNilai2(BigDecimal nilai2) {
		this.nilai2 = nilai2;
	}
	public BigDecimal getNilai3() {
		return nilai3;
	}
	public void setNilai3(BigDecimal nilai3) {
		this.nilai3 = nilai3;
	}
	public BigDecimal getNilai4() {
		return nilai4;
	}
	public void setNilai4(BigDecimal nilai4) {
		this.nilai4 = nilai4;
	}
	

}
