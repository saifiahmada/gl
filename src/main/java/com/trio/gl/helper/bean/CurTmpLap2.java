package com.trio.gl.helper.bean;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Gusti Arya 9:38:28 AM Jul 29, 2013
 */

public class CurTmpLap2 {

	private Date tglPinj;
	private Date tglKredit;
	private Date tglFak;
	private String noUrus;
	private String noFak;
	private String noMesin;
	private String nama;
	private BigDecimal bbnkb;
	private BigDecimal bbnkbR;
	private BigDecimal pkb;
	private BigDecimal pkbR;
	private BigDecimal swdkllj;
	private BigDecimal swdklljR;
	private BigDecimal lain;
	private BigDecimal lainR;
	private BigDecimal totPinj;
	private BigDecimal totReal;
	private BigDecimal selisih;
	private BigDecimal saldo;
	
	public CurTmpLap2() {
		// TODO Auto-generated constructor stub
	}

	public CurTmpLap2(Date tglPinj, Date tglKredit, Date tglFak, String noUrus,
			String noFak, String noMesin, String nama, BigDecimal bbnkb,
			BigDecimal bbnkbR, BigDecimal pkb, BigDecimal pkbR,
			BigDecimal swdkllj, BigDecimal swdklljR, BigDecimal lain,
			BigDecimal lainR, BigDecimal totPinj, BigDecimal totReal,
			BigDecimal selisih, BigDecimal saldo) {
		super();
		this.tglPinj = tglPinj;
		this.tglKredit = tglKredit;
		this.tglFak = tglFak;
		this.noUrus = noUrus;
		this.noFak = noFak;
		this.noMesin = noMesin;
		this.nama = nama;
		this.bbnkb = bbnkb;
		this.bbnkbR = bbnkbR;
		this.pkb = pkb;
		this.pkbR = pkbR;
		this.swdkllj = swdkllj;
		this.swdklljR = swdklljR;
		this.lain = lain;
		this.lainR = lainR;
		this.totPinj = totPinj;
		this.totReal = totReal;
		this.selisih = selisih;
		this.saldo = saldo;
	}

	public Date getTglPinj() {
		return tglPinj;
	}

	public void setTglPinj(Date tglPinj) {
		this.tglPinj = tglPinj;
	}

	public Date getTglKredit() {
		return tglKredit;
	}

	public void setTglKredit(Date tglKredit) {
		this.tglKredit = tglKredit;
	}

	public Date getTglFak() {
		return tglFak;
	}

	public void setTglFak(Date tglFak) {
		this.tglFak = tglFak;
	}

	public String getNoUrus() {
		return noUrus;
	}

	public void setNoUrus(String noUrus) {
		this.noUrus = noUrus;
	}

	public String getNoFak() {
		return noFak;
	}

	public void setNoFak(String noFak) {
		this.noFak = noFak;
	}

	public String getNoMesin() {
		return noMesin;
	}

	public void setNoMesin(String noMesin) {
		this.noMesin = noMesin;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public BigDecimal getBbnkb() {
		return bbnkb;
	}

	public void setBbnkb(BigDecimal bbnkb) {
		this.bbnkb = bbnkb;
	}

	public BigDecimal getBbnkbR() {
		return bbnkbR;
	}

	public void setBbnkbR(BigDecimal bbnkbR) {
		this.bbnkbR = bbnkbR;
	}

	public BigDecimal getPkb() {
		return pkb;
	}

	public void setPkb(BigDecimal pkb) {
		this.pkb = pkb;
	}

	public BigDecimal getPkbR() {
		return pkbR;
	}

	public void setPkbR(BigDecimal pkbR) {
		this.pkbR = pkbR;
	}

	public BigDecimal getSwdkllj() {
		return swdkllj;
	}

	public void setSwdkllj(BigDecimal swdkllj) {
		this.swdkllj = swdkllj;
	}

	public BigDecimal getSwdklljR() {
		return swdklljR;
	}

	public void setSwdklljR(BigDecimal swdklljR) {
		this.swdklljR = swdklljR;
	}

	public BigDecimal getLain() {
		return lain;
	}

	public void setLain(BigDecimal lain) {
		this.lain = lain;
	}

	public BigDecimal getLainR() {
		return lainR;
	}

	public void setLainR(BigDecimal lainR) {
		this.lainR = lainR;
	}

	public BigDecimal getTotPinj() {
		return totPinj;
	}

	public void setTotPinj(BigDecimal totPinj) {
		this.totPinj = totPinj;
	}

	public BigDecimal getTotReal() {
		return totReal;
	}

	public void setTotReal(BigDecimal totReal) {
		this.totReal = totReal;
	}

	public BigDecimal getSelisih() {
		return selisih;
	}

	public void setSelisih(BigDecimal selisih) {
		this.selisih = selisih;
	}

	public BigDecimal getSaldo() {
		return saldo;
	}

	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}
	
}
