package com.trio.gl.helper.bean;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Gusti Arya 3:19:38 PM Oct 7, 2013
 */

public class CurCekPenjualan {
	
	private double no;
	private BigDecimal pot;
	private BigDecimal nett;
	private String noMesin;
	private String noRangka;
	private String kdItem;
	private String noFak;
	private String nmCust;
	private String ket;
	private Date tglSj;
	
	public CurCekPenjualan() {
		// TODO Auto-generated constructor stub
	}
	
	public double getNo() {
		return no;
	}
	public void setNo(double no) {
		this.no = no;
	}
	
	public BigDecimal getPot() {
		return pot;
	}

	public void setPot(BigDecimal pot) {
		this.pot = pot;
	}

	public BigDecimal getNett() {
		return nett;
	}

	public void setNett(BigDecimal nett) {
		this.nett = nett;
	}

	public String getNoMesin() {
		return noMesin;
	}
	public void setNoMesin(String noMesin) {
		this.noMesin = noMesin;
	}
	public String getNoRangka() {
		return noRangka;
	}
	public void setNoRangka(String noRangka) {
		this.noRangka = noRangka;
	}
	public String getKdItem() {
		return kdItem;
	}
	public void setKdItem(String kdItem) {
		this.kdItem = kdItem;
	}
	public String getNoFak() {
		return noFak;
	}
	public void setNoFak(String noFak) {
		this.noFak = noFak;
	}
	public String getNmCust() {
		return nmCust;
	}
	public void setNmCust(String nmCust) {
		this.nmCust = nmCust;
	}
	public String getKet() {
		return ket;
	}
	public void setKet(String ket) {
		this.ket = ket;
	}
	public Date getTglSj() {
		return tglSj;
	}
	public void setTglSj(Date tglSj) {
		this.tglSj = tglSj;
	}
	
	
}
