package com.trio.gl.helper.bean;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Gusti Arya 1:19:41 PM Jul 16, 2013
 */

public class CurStnkBpkb {

	private String noMhn;
	private Date tgl;
	private String noMesin;
	private String noRangka;
	private String kdItem;
	private String nama;
	private BigDecimal biaya;
	private String status;
	
	public CurStnkBpkb() {
		// TODO Auto-generated constructor stub
	}
	
	
	public CurStnkBpkb(String noMhn, Date tgl, String noMesin, String noRangka,
			String kdItem, String nama, BigDecimal biaya, String status) {
		super();
		this.noMhn = noMhn;
		this.tgl = tgl;
		this.noMesin = noMesin;
		this.noRangka = noRangka;
		this.kdItem = kdItem;
		this.nama = nama;
		this.biaya = biaya;
		this.status = status;
	}

	public String getNoMhn() {
		return noMhn;
	}
	public void setNoMhn(String noMhn) {
		this.noMhn = noMhn;
	}
	public Date getTgl() {
		return tgl;
	}
	public void setTgl(Date tgl) {
		this.tgl = tgl;
	}
	public String getNoMesin() {
		return noMesin;
	}
	public void setNoMesin(String noMesin) {
		this.noMesin = noMesin;
	}
	public String getNoRangka() {
		return noRangka;
	}
	public void setNoRangka(String noRangka) {
		this.noRangka = noRangka;
	}
	public String getKdItem() {
		return kdItem;
	}
	public void setKdItem(String kdItem) {
		this.kdItem = kdItem;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public BigDecimal getBiaya() {
		return biaya;
	}
	public void setBiaya(BigDecimal biaya) {
		this.biaya = biaya;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
}
