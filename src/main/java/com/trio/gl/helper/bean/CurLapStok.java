package com.trio.gl.helper.bean;

import java.util.Date;

/**
 * @author Gusti Arya 11:52:38 AM Jul 5, 2013
 */

public class CurLapStok {

	private String noFak;
	private String noSj;
	private String noMesin;
	private String noRangka;
	private String kdItem;
	private Date tglSj;
	public String getNoFak() {
		return noFak;
	}
	public void setNoFak(String noFak) {
		this.noFak = noFak;
	}
	public String getNoSj() {
		return noSj;
	}
	public void setNoSj(String noSj) {
		this.noSj = noSj;
	}
	public String getNoMesin() {
		return noMesin;
	}
	public void setNoMesin(String noMesin) {
		this.noMesin = noMesin;
	}
	public String getNoRangka() {
		return noRangka;
	}
	public void setNoRangka(String noRangka) {
		this.noRangka = noRangka;
	}
	public String getKdItem() {
		return kdItem;
	}
	public void setKdItem(String kdItem) {
		this.kdItem = kdItem;
	}
	public Date getTglSj() {
		return tglSj;
	}
	public void setTglSj(Date tglSj) {
		this.tglSj = tglSj;
	}
	
	public CurLapStok() {
		// TODO Auto-generated constructor stub
	}
	
	
	public CurLapStok(String noFak, String noSj, String noMesin,
			String noRangka, String kdItem, Date tglSj) {
		super();
		this.noFak = noFak;
		this.noSj = noSj;
		this.noMesin = noMesin;
		this.noRangka = noRangka;
		this.kdItem = kdItem;
		this.tglSj = tglSj;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((kdItem == null) ? 0 : kdItem.hashCode());
		result = prime * result + ((noFak == null) ? 0 : noFak.hashCode());
		result = prime * result + ((noMesin == null) ? 0 : noMesin.hashCode());
		result = prime * result
				+ ((noRangka == null) ? 0 : noRangka.hashCode());
		result = prime * result + ((noSj == null) ? 0 : noSj.hashCode());
		result = prime * result + ((tglSj == null) ? 0 : tglSj.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CurLapStok other = (CurLapStok) obj;
		if (kdItem == null) {
			if (other.kdItem != null)
				return false;
		} else if (!kdItem.equals(other.kdItem))
			return false;
		if (noFak == null) {
			if (other.noFak != null)
				return false;
		} else if (!noFak.equals(other.noFak))
			return false;
		if (noMesin == null) {
			if (other.noMesin != null)
				return false;
		} else if (!noMesin.equals(other.noMesin))
			return false;
		if (noRangka == null) {
			if (other.noRangka != null)
				return false;
		} else if (!noRangka.equals(other.noRangka))
			return false;
		if (noSj == null) {
			if (other.noSj != null)
				return false;
		} else if (!noSj.equals(other.noSj))
			return false;
		if (tglSj == null) {
			if (other.tglSj != null)
				return false;
		} else if (!tglSj.equals(other.tglSj))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "CurLapStok [noFak=" + noFak + ", noSj=" + noSj + ", noMesin="
				+ noMesin + ", noRangka=" + noRangka + ", kdItem=" + kdItem
				+ ", tglSj=" + tglSj + "]";
	}



}
