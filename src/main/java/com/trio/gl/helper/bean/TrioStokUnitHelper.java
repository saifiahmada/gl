package com.trio.gl.helper.bean;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.trio.gl.usertrail.TrioEntityUserTrail;

/**
 * @author Gusti Arya 2:49:41 PM Jun 24, 2013
 */

@Entity
@Table(name="TRIO_STOK_UNIT_HELPER")
public class TrioStokUnitHelper extends TrioEntityUserTrail implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="BARIS", nullable=false)
	private int baris;

	@Column(name="KD_DLR",length=10)
	private String kdDlr;

	@Column(name="KD_TIPE", length=6)
	private String kdTipe;
	
	@Column(name="TIPE", length=50)
	private String tipe;
	
	@Column(name="HARGA", precision=10, scale=2)
	private BigDecimal harga;
	
	@Column(name="S_AWAL", precision=10, scale=2)
	private BigDecimal sAwal;
	
	@Column(name="MASUK", length=3)
	private Integer masuk;
	
	@Column(name="KELUAR", length=3)
	private Integer keluar;
	
	@Column(name="S_AKHIR", precision=10, scale=2)
	private BigDecimal sAkhir;
	
	public TrioStokUnitHelper() {
		// TODO Auto-generated constructor stub
	}

	public int getBaris() {
		return baris;
	}

	public void setBaris(int baris) {
		this.baris = baris;
	}

	public String getKdDlr() {
		return kdDlr;
	}

	public void setKdDlr(String kdDlr) {
		this.kdDlr = kdDlr;
	}

	public String getKdTipe() {
		return kdTipe;
	}

	public void setKdTipe(String kdTipe) {
		this.kdTipe = kdTipe;
	}

	public String getTipe() {
		return tipe;
	}

	public void setTipe(String tipe) {
		this.tipe = tipe;
	}

	public BigDecimal getHarga() {
		return harga;
	}

	public void setHarga(BigDecimal harga) {
		this.harga = harga;
	}

	public BigDecimal getsAwal() {
		return sAwal;
	}

	public void setsAwal(BigDecimal sAwal) {
		this.sAwal = sAwal;
	}

	public Integer getMasuk() {
		return masuk;
	}

	public void setMasuk(Integer masuk) {
		this.masuk = masuk;
	}

	public Integer getKeluar() {
		return keluar;
	}

	public void setKeluar(Integer keluar) {
		this.keluar = keluar;
	}

	public BigDecimal getsAkhir() {
		return sAkhir;
	}

	public void setsAkhir(BigDecimal sAkhir) {
		this.sAkhir = sAkhir;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + baris;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioStokUnitHelper other = (TrioStokUnitHelper) obj;
		if (baris != other.baris)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioStokUnitHelper [baris=" + baris + "]";
	}
	
	
}
