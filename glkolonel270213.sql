-- MySQL dump 10.13  Distrib 5.5.28, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: glkolonel
-- ------------------------------------------------------
-- Server version	5.5.28-0ubuntu0.12.04.3

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `glkolonel`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `glkolonel` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `glkolonel`;

--
-- Table structure for table `trio_mstaccessrole`
--

DROP TABLE IF EXISTS `trio_mstaccessrole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trio_mstaccessrole` (
  `VMENUID` varchar(20) NOT NULL,
  `VROLEID` varchar(20) NOT NULL,
  `DCREA` datetime DEFAULT NULL,
  `DMODI` datetime DEFAULT NULL,
  `VCREABY` varchar(20) DEFAULT NULL,
  `VMODIBY` varchar(20) DEFAULT NULL,
  `VSTAT` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`VMENUID`,`VROLEID`),
  KEY `FK24D538EDCC051BD5` (`VROLEID`),
  KEY `FK24D538EDC2EEAF67` (`VMENUID`),
  CONSTRAINT `FK24D538EDC2EEAF67` FOREIGN KEY (`VMENUID`) REFERENCES `trio_mstmenu` (`VMENUID`),
  CONSTRAINT `FK24D538EDCC051BD5` FOREIGN KEY (`VROLEID`) REFERENCES `trio_mstrole` (`VROLEID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trio_mstaccessrole`
--

LOCK TABLES `trio_mstaccessrole` WRITE;
/*!40000 ALTER TABLE `trio_mstaccessrole` DISABLE KEYS */;
/*!40000 ALTER TABLE `trio_mstaccessrole` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trio_mstcabang`
--

DROP TABLE IF EXISTS `trio_mstcabang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trio_mstcabang` (
  `KD_PRS` varchar(3) NOT NULL,
  `DCREA` datetime DEFAULT NULL,
  `DMODI` datetime DEFAULT NULL,
  `VCREABY` varchar(20) DEFAULT NULL,
  `VMODIBY` varchar(20) DEFAULT NULL,
  `ALM_PRS` varchar(40) DEFAULT NULL,
  `FAX` varchar(11) DEFAULT NULL,
  `KD_PRS2` varchar(3) DEFAULT NULL,
  `NM_PRS` varchar(35) DEFAULT NULL,
  `TELP1` varchar(11) DEFAULT NULL,
  `TELP2` varchar(11) DEFAULT NULL,
  `TELP3` varchar(11) DEFAULT NULL,
  PRIMARY KEY (`KD_PRS`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trio_mstcabang`
--

LOCK TABLES `trio_mstcabang` WRITE;
/*!40000 ALTER TABLE `trio_mstcabang` DISABLE KEYS */;
/*!40000 ALTER TABLE `trio_mstcabang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trio_mstmenu`
--

DROP TABLE IF EXISTS `trio_mstmenu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trio_mstmenu` (
  `VMENUID` varchar(11) NOT NULL,
  `DCREA` datetime DEFAULT NULL,
  `DMODI` datetime DEFAULT NULL,
  `VCREABY` varchar(20) DEFAULT NULL,
  `VMODIBY` varchar(20) DEFAULT NULL,
  `NORDERER` int(11) DEFAULT NULL,
  `VIMAGE` varchar(30) DEFAULT NULL,
  `VLOCATION` varchar(30) DEFAULT NULL,
  `VPARENT` varchar(6) DEFAULT NULL,
  `VTITLE` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`VMENUID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trio_mstmenu`
--

LOCK TABLES `trio_mstmenu` WRITE;
/*!40000 ALTER TABLE `trio_mstmenu` DISABLE KEYS */;
/*!40000 ALTER TABLE `trio_mstmenu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trio_mstrole`
--

DROP TABLE IF EXISTS `trio_mstrole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trio_mstrole` (
  `VROLEID` varchar(20) NOT NULL,
  `DCREA` datetime DEFAULT NULL,
  `DMODI` datetime DEFAULT NULL,
  `VCREABY` varchar(20) DEFAULT NULL,
  `VMODIBY` varchar(20) DEFAULT NULL,
  `VROLENAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`VROLEID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trio_mstrole`
--

LOCK TABLES `trio_mstrole` WRITE;
/*!40000 ALTER TABLE `trio_mstrole` DISABLE KEYS */;
/*!40000 ALTER TABLE `trio_mstrole` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trio_mstuser`
--

DROP TABLE IF EXISTS `trio_mstuser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trio_mstuser` (
  `VUSERNAME` varchar(20) NOT NULL,
  `DCREA` datetime DEFAULT NULL,
  `DMODI` datetime DEFAULT NULL,
  `VCREABY` varchar(20) DEFAULT NULL,
  `VMODIBY` varchar(20) DEFAULT NULL,
  `VPASSWORD` varchar(64) NOT NULL,
  `VSTAT` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`VUSERNAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trio_mstuser`
--

LOCK TABLES `trio_mstuser` WRITE;
/*!40000 ALTER TABLE `trio_mstuser` DISABLE KEYS */;
/*!40000 ALTER TABLE `trio_mstuser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trio_mstuserrole`
--

DROP TABLE IF EXISTS `trio_mstuserrole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trio_mstuserrole` (
  `VROLEID` varchar(20) NOT NULL,
  `VUSERNAME` varchar(20) NOT NULL,
  `DCREA` datetime DEFAULT NULL,
  `DMODI` datetime DEFAULT NULL,
  `VCREABY` varchar(20) DEFAULT NULL,
  `VMODIBY` varchar(20) DEFAULT NULL,
  `VSTAT` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`VROLEID`,`VUSERNAME`),
  KEY `FK6B9E1094E8C4A26F` (`VUSERNAME`),
  KEY `FK6B9E1094CC051BD5` (`VROLEID`),
  CONSTRAINT `FK6B9E1094CC051BD5` FOREIGN KEY (`VROLEID`) REFERENCES `trio_mstrole` (`VROLEID`),
  CONSTRAINT `FK6B9E1094E8C4A26F` FOREIGN KEY (`VUSERNAME`) REFERENCES `trio_mstuser` (`VUSERNAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trio_mstuserrole`
--

LOCK TABLES `trio_mstuserrole` WRITE;
/*!40000 ALTER TABLE `trio_mstuserrole` DISABLE KEYS */;
/*!40000 ALTER TABLE `trio_mstuserrole` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-02-27 17:08:32
